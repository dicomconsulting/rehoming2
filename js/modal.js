
// Инициализация модальных окон
$(function () {
    $('.modal-dialog').find('button.close').on('click', function(event, obj) {
        $(event.target).closest('.modal').hide();
    });
});

function showDialog(id)
{
    $('#'+id).modal('show');
}

function hideDialog(id)
{
    $('#'+id).modal('hide');
}