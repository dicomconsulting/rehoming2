/**
 * Created by Дамир on 11.11.13.
 */
$(function(){
    $('textarea').autosize();

    //заменяем все селекты на выбранные значения
    $.each($("select"), function (i, el){
        var text = $('option:selected', el).text();
        $(el).replaceWith($("<span style='border-bottom: 1px dotted #808080'>"+text+"</span>"))
    });

});