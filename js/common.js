/**
 * Created by Дамир on 19.11.13.
 */
$(function () {
    attachDatepickerToFields($('body'));

    //timepicker для тех браузеров, которые не поддерживают input[type=time]
    var input = document.createElement('input');
    input.setAttribute('type', 'time');

    if (input.type == 'text') {
        $('input[type=time]').timepicker({
            hourText: 'Часы',// Define the locale text for "Hours"
            minuteText: 'Минуты'
        });
    }

    //обработка ввода значений в поля с плавающей запятой. запрет ввода всего, кроме [0-9,]
    //такие поля должны помечаться классом decimal_field
    $(document).on("keypress", "input.decimal_field", function (e) {
        var code = e.charCode || e.keyCode;
        var char = String.fromCharCode(code);
        var reg = /[^0-9,]/i;

        if (char.match(reg)) {
            e.preventDefault();
        }
    });
});

/**
 * Проверяет поддерживает ли браузер input[type=date]
 * @returns {boolean}
 */
function isDateFieldAviable() {
    var input = document.createElement('input');
    input.setAttribute('type', 'date');
    if (input.type == 'text') {
        return false;
    } else {
        return true;
    }
}

/**
 * Прикручивает datepicker к полям с типом date если браузер не поддерживает поле date
 * @param el
 */
function attachDatepickerToFields(el) {

    var language = $("meta[name=language]").attr("content");
    $.datepicker.setDefaults($.datepicker.regional[language]);

    $.each($(el).find('input[type=date]'), function (i, el) {
        var jqEl = $(el);
        var oldName = jqEl.attr("name"),
            oldId = jqEl.attr("id");

        jqEl.attr("name", oldName + "_datepicker");
        jqEl.attr("id", oldId + "_datepicker");

        var oldVal = $(jqEl).val();
        var newInput = $('<input type="hidden" id="' + oldId + '" name="' + oldName + '" value="' + oldVal + '">');

        var parsedDate = $.datepicker.parseDate("yy-mm-dd", oldVal);
        var formattedDate = $.datepicker.formatDate('dd-mm-yy', parsedDate);

        jqEl.after(newInput);
        jqEl.attr("type", "text").datepicker(
            {
                dateFormat: "dd-mm-yy",
                altFormat: "yy-mm-dd",
                altField: "#" + oldId,
                changeMonth: true,
                changeYear: true
            }
        );
        jqEl.datepicker( "setDate", formattedDate);
    })

}

// Показываем фреймы с деперсонализированными данными
window.onmessage = function(e){
    if (e.data == 'depersonalizationFrameLoaded') {
        console.log(e);
        $('.depersonalizationFrame').css("visibility", "visible");
    }
};

