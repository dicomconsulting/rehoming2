<?php

Yii::app()->getModule("admin");

class UserController extends Controller
{

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return [
            [
                'deny',
                'users' => ['?'],
            ],
        ];
    }

    public function actionProfile()
    {
        if (Yii::app()->user->isGuest) {
            $this->redirect($this->createUrl("/admin/backend/user/login", ""));
        }

        /** @var CWebUser $user */
        $webuser = Yii::app()->user;
        $user    = User::model()->findByPk($webuser->getId());

        if (array_key_exists('settings', $_POST)) {
            $user->setSettings($_POST['settings']);
            $user->save();
            Yii::app()->user->setFlash('success', Yii::t('general', 'Данные успешно сохранены'));
        }

        $this->render('profile', ['user' => $user]);
    }

}
