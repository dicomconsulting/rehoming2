<?php


class StatisticsController extends Controller
{

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return [
            [
                'deny',
                'users' => ['?'],
            ],
        ];
    }

    public function actionIndex()
    {
        $this->render("index");
    }

}
