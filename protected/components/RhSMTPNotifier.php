<?php

class RhSMTPNotifier extends CComponent
{
    public $smtpAuth = true;

    public $smtpSecure = 'ssl';

    public $smtpHost;

    public $smtpPort;

    public $username;

    public $password;

    public $adminAddresses = [];

    /**
     * @var PHPMailer
     */
    private $mailer;

    public function init()
    {
        $this->mailer = $this->_createMailer();
    }

    public function send($message, $subject = 'Системное уведомление')
    {
        if (!empty($this->adminAddresses) && is_array($this->adminAddresses)) {
            $mailer = $this->_getMailer();
            $mailer->Subject = $subject;
            $mailer->AltBody = strip_tags($message);
            $mailer->MsgHTML($message);
            foreach ($this->adminAddresses as $address) {
                $mailer->addAddress($address);
            }
            $mailer->send();
        }
    }

    private function _getMailer()
    {
        return $this->mailer;
    }

    private function _createMailer()
    {
        $mailer = new PHPMailer();
        $mailer->IsSMTP();
        $mailer->SMTPAuth = $this->smtpAuth;
        $mailer->SMTPSecure = $this->smtpSecure;
        $mailer->Host = $this->smtpHost;
        $mailer->Port = $this->smtpPort;
        $mailer->Username = $this->username;
        $mailer->Password = $this->password;
        $mailer->CharSet = 'utf-8';
        return $mailer;
    }
}
