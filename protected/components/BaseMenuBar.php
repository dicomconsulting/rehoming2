<?php

abstract class BaseMenuBar extends CMenu
{
    /**
     * html-обертки вокруг подменю
     */
    public $submenuHtmlBefore;
    public $submenuHtmlAfter;

    public $activateParents = true;

    abstract public function _init();

    public function init()
    {
        $this->_init();
        parent::init();
    }

    /**
     * Renders the menu items.
     * @param array $items menu items. Each menu item will be an array with at least two elements: 'label' and 'active'.
     * It may have three other optional elements: 'items', 'linkOptions' and 'itemOptions'.
     */
    protected function renderMenu($items)
    {
        if (count($items)) {
            $subMenu = "";
            echo CHtml::openTag('ul', $this->htmlOptions)."\n";
            $this->renderMenuWithSubs($items, $subMenu);
            echo CHtml::closeTag('ul');
            echo $subMenu;
        }
    }

    /**
     * Новый метод т.к. нам необходимо выводить субменю только для текущего активного элемента верхнего меню,
     * плюс у нас отличается вывод - подменю выводится не внутри меню верхнего уровня, а под ним
     *
     * @param array $items the menu items to be rendered recursively
     * @param string $submenu returns submenu contents if exists
     */
    protected function renderMenuWithSubs($items, &$submenu)
    {
        $count=0;
        $n=count($items);

        foreach ($items as $item) {
            $count++;
            $options=isset($item['itemOptions']) ? $item['itemOptions'] : array();
            $class=array();
            if ($item['active'] && $this->activeCssClass!='') {
                $class[]=$this->activeCssClass;
            }
            if ($count===1 && $this->firstItemCssClass!==null) {
                $class[]=$this->firstItemCssClass;
            }
            if ($count===$n && $this->lastItemCssClass!==null) {
                $class[]=$this->lastItemCssClass;
            }
            if ($this->itemCssClass!==null) {
                $class[]=$this->itemCssClass;
            }
            if ($class!==array()) {
                if (empty($options['class'])) {
                    $options['class']=implode(' ', $class);
                } else {
                    $options['class'].=' '.implode(' ', $class);
                }

            }

            echo CHtml::openTag('li', $options);

            $menu=$this->renderMenuItem($item);
            if (isset($this->itemTemplate) || isset($item['template'])) {
                $template=isset($item['template']) ? $item['template'] : $this->itemTemplate;
                echo strtr($template, array('{menu}'=>$menu));
            } else {
                echo $menu;
            }

            echo CHtml::closeTag('li')."\n";

            if (isset($item['items']) && count($item['items']) && $item['active']) {
                ob_start();

                echo $this->submenuHtmlBefore;
                echo "\n".CHtml::openTag(
                    'ul',
                    isset($item['submenuOptions']) ? $item['submenuOptions'] : $this->submenuHtmlOptions
                )
                ."\n";
                $this->renderMenuWithSubs($item['items'], $submenu);
                echo CHtml::closeTag('ul')."\n";
                echo $this->submenuHtmlAfter;

                $submenu = ob_get_clean();
            }
        }
    }
}
