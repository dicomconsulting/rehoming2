<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Damir Garifullin
 * Date: 07.10.13
 * Time: 17:40
 *
 * Виджет верхнего фиксированного меню
 *
 */

class TopMenu extends CMenu {

    public function init()
    {
        $this->items = array(
            array('label'=>'Главная', 'url'=>array('/site/index')),
//            array('label'=>'Секретная', 'url'=>array('/site/secret')),
            array('label'=>'Войти', 'url'=>array('/admin/backend/user/login'), 'visible'=>Yii::app()->user->isGuest),
        );

        parent::init();
    }
}