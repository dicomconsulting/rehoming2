<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Damir Garifullin
 * Date: 07.10.13
 * Time: 17:40
 *
 * Виджет левого меню
 *
 */

class LeftSideMenu extends CMenu
{

    public $activateParents = true;

    public $encodeLabel = false;

    public function init()
    {
        Yii::app()->getModule("patient");
        Yii::app()->getModule("info");
        $totalCnt = Patient::model()->onlyAssigned()->count();

        $patient = new Patient("search");
        $patient->acknowledge_search = 1;
        $provider = $patient->onlyAssigned()->search();
        $countUnacknowleged = $provider->totalItemCount;

        $moduleId = Yii::app()->controller->module ? Yii::app()->controller->module->id : "";
        $controllerId = Yii::app()->controller->id;
        $id = Yii::app()->getRequest()->getQuery('id');

        $toolsItems = $this->getToolsMenu($moduleId, $controllerId, $id);

        $this->items = array(
            array(
                'label' => Yii::t('general', 'Пациенты'),
                'visible' => Yii::app()->user->checkAccess('viewPatientCard'),
                'items' => array(
                    array(
                        'label' => Yii::t('general', 'Все') . ' (' . $totalCnt . ')',
                        'url' => array('/patient/list/index'),
                        'visible' => Yii::app()->user->checkAccess('viewPatientCard')
                    ),
//                    array(
//                        'label' => Yii::t('general', 'Требуют внимания') . ' (' . $countUnacknowleged . ')',
//                        'url' => array('/patient/list/unacknowledged'),
//                        'visible' => Yii::app()->user->checkAccess('viewPatientCard')
//                    ),
                )
            ),/*
            array(
                'label' => Yii::t('general', 'Статистика (Пред)'),
                'url' => array('/statistics/view/index'),
                'visible' => Yii::app()->user->checkAccess('viewStatistics')
            ),*/
            array(
                'label' => Yii::t('general', 'Статистика'),
                'url' => array('/statisticsNew/view/index'),
                'visible' => Yii::app()->user->checkAccess('viewStatistics')
            ),
            array(
                'label' => Yii::t('general', 'Статистика (нов.)'),
                'url' => array('/statisticsUpdate/view/index'),
                'visible' => Yii::app()->user->checkAccess('viewStatistics')
            ),
            array(
                'label' => Yii::t('general', 'Статистика 2018'),
                'url' => array('/statistics2018/view/index'),
                'visible' => Yii::app()->user->checkAccess('viewStatistics')
            ),
            array(
                'label' => Yii::t('general', 'Администрирование'),
                'url' => array('/admin/backend/enter/index'),
                'visible' => Yii::app()->user->checkAccess('adminEnter')
            ),
            array(
                'label' => Yii::t('general', 'Инструменты'),
                'visible' => !Yii::app()->user->isGuest,
                'items' => $toolsItems
            ),
        );

        parent::init();
    }

    public function getInfopageItems($moduleId, $controllerId, $id)
    {
        $result = [];

        /** @var Infopage[] $pages */
        $pages = Infopage::getPagesToShow();

        foreach ($pages as $page) {
            $result[] = [
                'label' => $page->name,
                'url' => array('/info/view/index/id/' . $page->uid),
                'active' => $moduleId == "info" && $controllerId == "view" && $id == $page->uid
            ];
        }

        return $result;
    }

    /**
     * @param $moduleId
     * @param $controllerId
     * @param $id
     * @return array
     */
    protected function getToolsMenu($moduleId, $controllerId, $id)
    {
        $toolsItems = array(
            array(
                'label' => Yii::t('general', 'Профиль'),
                'url' => array('/user/profile'),
                'visible' => !Yii::app()->user->isGuest
            ),
        );

        $infoPageItems = $this->getInfopageItems($moduleId, $controllerId, $id);

        if ($infoPageItems) {
            $toolsItems = array_merge($toolsItems, $infoPageItems);
        }

        $toolsItems[] = array(
            'label' => Yii::t('general', 'Выйти'),
            'url' => array('/admin/backend/user/logout'),
            'visible' => !Yii::app()->user->isGuest
        );
        return $toolsItems;
    }
}