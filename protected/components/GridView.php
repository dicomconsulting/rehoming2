<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 11.12.13
 * Time: 16:34
 */

Yii::import("zii.widgets.grid.CGridView");

class GridView extends CGridView
{
    public function renderKeys()
    {
        echo CHtml::openTag(
            'div',
            array(
                'class' => 'keys',
                'style' => 'display:none',
                'title' => Yii::app()->getRequest()->getOriginalUrl(),
            )
        );
        foreach ($this->dataProvider->getKeys() as $key) {
            echo "<span>" . CHtml::encode($key) . "</span>";
        }
        echo "</div>\n";
    }
}
