<?php

/**
 * @author Ruslan Fadeev
 * created: 17.04.14 14:22
 * @method Controller getController()
 */
class Modal extends \TbModal
{
    public $fade = false;
    public $renderAfter = false;
    public $viewForm = "";
    public $saveButton = false;

    public function init()
    {
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->getBaseUrl(true) . '/js/modal.js',
            CClientScript::POS_END
        );

        if ($this->saveButton) {
            $htmlOptions = array_merge(['class' => 'pull-left'], isset($this->saveButton['htmlOptions']) ? $this->saveButton['htmlOptions'] : []);
            $this->footer .= TbHtml::button(
                isset($this->saveButton['label']) ? $this->saveButton['label'] : \CoreModule::t('Сохранить'),
                $htmlOptions
            );
        }
        if ($this->renderAfter) {
            $controller = $this->getController();
            if (method_exists($controller, 'onAfterRenderEvent')) {
                $controller->attachEventHandler('onAfterRenderEvent', [$this, 'afterRender']);
                ob_start();
            }
        }
        parent::init();
    }


    /**
     * Кешируем вывод, и выводим модальное окно в конце страницы
     */
    public function run()
    {
        if ($this->renderAfter) {
            $controller = $this->getController();
            if (method_exists($controller, 'afterRender')) {
                parent::run();
                $this->viewForm = ob_get_clean();
            }
        } else {
            parent::run();
        }
    }

    public function afterRender($event)
    {
        $event->params['output'] .= $this->viewForm;
    }

    public static function buttonShow($modalId, $text, $htmlOptions = [])
    {
        $htmlOptions = array_merge(['data-toggle' => 'modal', 'data-target' => '#' . $modalId], $htmlOptions);
        return TbHtml::button($text, $htmlOptions);
    }
}
