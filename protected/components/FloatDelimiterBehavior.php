<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 13.11.13
 * Time: 14:55
 */

class FloatDelimiterBehavior extends CActiveRecordBehavior
{

    /**
     * @param CModelEvent $event
     * @return bool
     */
    public function beforeSave($event)
    {
        /** @var CActiveRecord $model */
        $model = $this->getOwner();

        foreach ($model->attributes as $key => $value) {
            if ($value && $this->isAttrDecimal($key)) {
                $model->$key = str_replace(",", ".", $value);
            }
        }
        return true;
    }

    /**
     * @param CModelEvent $event
     * @return bool
     */
    public function afterSave($event)
    {
        /** @var CActiveRecord $model */
        $model = $this->getOwner();

        foreach ($model->attributes as $key => $value) {
            if ($value && $this->isAttrDecimal($key)) {
                $model->$key = str_replace(".", ",", $value);
            }
        }
        return true;
    }

    /**
     * @param CModelEvent $event
     * @return bool
     */
    public function afterFind($event)
    {
        /** @var CActiveRecord $model */
        $model = $this->getOwner();

        foreach ($model->attributes as $key => $value) {
            if ($value && $this->isAttrDecimal($key)) {
                $model->$key = str_replace(".", ",", $value);
            }
        }
        return true;
    }

    protected function isAttrDecimal($attr)
    {
        /** @var CActiveRecord $model */
        $model = $this->getOwner();
        /** @var CMysqlColumnSchema $colData */
        $colData = $model->getMetaData()->columns[$attr];
        return (strrpos($colData->dbType, "decimal") !== false);
    }
}
