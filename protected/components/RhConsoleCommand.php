<?php

class RhConsoleCommand extends CConsoleCommand
{
    public function run($args)
    {
        echo 'Запуск консольной команды ' . date(RhDateTime::RH_DATETIME . " \n");
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $exitCode = parent::run($args);
            $transaction->commit();
            return $exitCode;
        } catch (Exception $e) {
            $transaction->rollback();
            if (defined('YII_CRON') && YII_CRON) {
                $this->redirectExceptionToStderr($e);
                return 1;
            } else {
                throw $e;
            }
        } finally {
            echo 'Завершение работы консольной команды ' . date(RhDateTime::RH_DATETIME . " \n");
        }
    }

    protected function redirectExceptionToStderr(Exception $e)
    {
        $errorHandler = fopen('php://stderr', 'a');
        $dateTime = new RhDateTime();
        fwrite($errorHandler, $dateTime->format('d.m.Y H:i:s '));
        fwrite($errorHandler, "Исключение с сообщением '{$e->getMessage()}' в файле {$e->getFile()}:{$e->getLine()}\n");
        fwrite($errorHandler, $e->getTraceAsString());
        fclose($errorHandler);
    }
}
