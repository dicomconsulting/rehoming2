<?php
Yii::import('admin.AdminModule');

class AdminMenu extends BaseMenuBar
{
    public $encodeLabel = false;

    public function _init()
    {
        $moduleId = Yii::app()->controller->module->id;
        $controllerId = Yii::app()->controller->id;

        $this->items = [
            [
                'label' => CHtml::tag('i', ['class' => 'icon-book'], ' ') . Yii::t(
                    'AdminModule.messages',
                    'Справочники'
                ),
                'url' => ['/directory/backend/country/index'],
                'active' => $moduleId == 'directory',
                'items' => [
                    [
                        'label' => Yii::t('AdminModule.messages', 'Страны'),
                        'url' => ['/directory/backend/country/index'],
                        'active' => $moduleId == 'directory' && $controllerId == 'backend/country',
                    ],
                    [
                        'label' => Yii::t('AdminModule.messages', 'Города'),
                        'url' => ['/directory/backend/city/index'],
                        'active' => $moduleId == 'directory' && $controllerId == 'backend/city',
                    ],
                    [
                        'label' => Yii::t('AdminModule.messages', 'Мед. центры'),
                        'url' => ['/directory/backend/medicalCenter/index'],
                        'active' => $moduleId == 'directory' && $controllerId == 'backend/medicalCenter',
                    ],
                    [
                        'label' => Yii::t('AdminModule.messages', 'Исходы НЯ'),
                        'url' => ['/directory/backend/adverseResult/index'],
                        'active' => $moduleId == 'directory' && $controllerId == 'backend/adverseResult',
                    ],
                    [
                        'label' => Yii::t('AdminModule.messages', 'Купирование аритмии'),
                        'url' => ['/directory/backend/arrCupping/index'],
                        'active' => $moduleId == 'directory' && $controllerId == 'backend/arrCupping',
                    ],
                    [
                        'label' => Yii::t('AdminModule.messages', 'Опции сознания при приступе'),
                        'url' => ['/directory/backend/attackConsciousness/index'],
                        'active' => $moduleId == 'directory' && $controllerId == 'backend/attackConsciousness',
                    ],
                    [
                        'label' => Yii::t('AdminModule.messages', 'Контроль захвата'),
                        'url' => ['/directory/backend/captureControl/index'],
                        'active' => $moduleId == 'directory' && $controllerId == 'backend/captureControl',
                    ],
                    [
                        'label' => Yii::t('AdminModule.messages', 'Полярность устройства'),
                        'url' => ['/directory/backend/devicePolarity/index'],
                        'active' => $moduleId == 'directory' && $controllerId == 'backend/devicePolarity',
                    ],
                    [
                        'label' => Yii::t('AdminModule.messages', 'Типы устройств'),
                        'url' => ['/directory/backend/deviceType/index'],
                        'active' => $moduleId == 'directory' && $controllerId == 'backend/deviceType',
                    ],
                    [
                        'label' => Yii::t('AdminModule.messages', 'Модели устройств'),
                        'url' => ['/directory/backend/deviceModel/index'],
                        'active' => $moduleId == 'directory' && $controllerId == 'backend/deviceModel',
                    ],
                    [
                        'label' => Yii::t('AdminModule.messages', 'Положение электрода'),
                        'url' => ['/directory/backend/electrodePlacement/index'],
                        'active' => $moduleId == 'directory' && $controllerId == 'backend/electrodePlacement',
                    ],
                    [
                        'label' => Yii::t('AdminModule.messages', 'Единицы измерения'),
                        'url' => ['/directory/backend/measurementUnit/index'],
                        'active' => $moduleId == 'directory' && $controllerId == 'backend/measurementUnit',
                    ],
                    [
                        'label' => Yii::t('AdminModule.messages', 'Сообщения Red и Yellow'),
                        'url' => ['/directory/backend/deviceAlterMessage/index'],
                        'active' => $moduleId == 'directory' && $controllerId == 'backend/deviceAlterMessage',
                    ],
                ],
                'visible' => Yii::app()->user->checkAccess("dictManagement")
            ],
            [
                'label' => CHtml::tag('i', ['class' => 'icon-user'], ' ')
                    . Yii::t('AdminModule.messages', 'Пользователи'),
                'url' => ['/admin/backend/doctor/index'],
                'active' => $moduleId == 'admin'
                    && in_array(
                        $controllerId,
                        [
                            'backend/doctor',
                            'backend/roles',
                            'backend/user',
                            'backend/userGroup',
                            'backend/patientGroup',
                            'backend/patient',
                        ]
                    ),
                'items' => [
                    [
                        'label' => Yii::t('AdminModule.messages', 'Врачи'),
                        'url' => ['/admin/backend/doctor/index'],
                        'active' => $moduleId == 'admin' && $controllerId == 'backend/doctor',
                    ],
                    [
                        'label' => Yii::t('AdminModule.messages', 'Пользователи'),
                        'url' => ['/admin/backend/user/index'],
                        'active' => $moduleId == 'admin' && $controllerId == 'backend/user',
                    ],
                    [
                        'label' => Yii::t('AdminModule.messages', 'Группы пользователей'),
                        'url' => ['/admin/backend/userGroup/index'],
                        'active' => $moduleId == 'admin' && $controllerId == 'backend/userGroup',
                    ],
                    [
                        'label' => Yii::t('AdminModule.messages', 'Роли'),
                        'url' => ['/admin/backend/roles/index'],
                        'active' => $moduleId == 'admin' && $controllerId == 'backend/roles',
                    ],
                    [
                        'label' => Yii::t('AdminModule.messages', 'Группы пациентов'),
                        'url' => ['/admin/backend/patientGroup/index'],
                        'active' => $moduleId == 'admin' && $controllerId == 'backend/patientGroup',
                    ],
                    [
                        'label' => Yii::t('AdminModule.messages', 'Пациенты'),
                        'url' => ['/admin/backend/patient/index'],
                        'active' => $moduleId == 'admin' && $controllerId == 'backend/patient',
                    ],
                ],
                'visible' => Yii::app()->user->checkAccess("userManagement")
            ],
            [
                'label' => CHtml::tag('i', ['class' => 'icon-hdd'], ' ') . Yii::t('AdminModule.messages', 'Устройство'),
                'url' => ['/device/backend/synonym/index'],
                'active' => $moduleId == 'device' && $controllerId == 'backend/synonym',
                'items' => [
                    [
                        'label' => Yii::t('AdminModule.messages', 'Синонимы'),
                        'url' => ['/device/backend/synonym/index'],
                    ]
                ],
                'visible' => Yii::app()->user->checkAccess("deviceManagement")
            ],
            [
                'label' => CHtml::tag('i', ['class' => 'icon-wrench'], ' ') . Yii::t('AdminModule.messages', 'Настройки'),
                'url' => ['/admin/backend/setting/index'],
                'active' => $moduleId == 'admin' && $controllerId == 'backend/setting',
                'items' => [
                    [
                        'label' => Yii::t('AdminModule.messages', 'Обзор'),
                        'url' => ['/admin/backend/setting/index'],
                    ],
                    [
                        'label' => Yii::t('AdminModule.messages', 'Редактирование'),
                        'url' => ['/admin/backend/setting/edit'],
                    ],
                ],
                'visible' => Yii::app()->user->checkAccess("settingsManagement")
            ],
            [
                'label' => CHtml::tag('i', ['class' => 'icon-tasks'], ' ') . Yii::t('AdminModule.messages', 'Задачи'),
                'url' => ['/task/backend/setting/index'],
                'active' => $moduleId == 'task' && $controllerId == 'backend/setting',
                'items' => [
                    [
                        'label' => Yii::t('AdminModule.messages', 'Обзор'),
                        'url' => ['/task/backend/setting/index'],
                    ],
                ],
                'visible' => Yii::app()->user->checkAccess("taskManagement")
            ],
            [
                'label' => CHtml::tag('i', ['class' => 'icon-file'], ' ') . Yii::t(
                    'AdminModule.messages',
                    'Инфо. страницы'
                ),
                'url' => ['/info/backend/infoPageAdmin/index'],
                'active' => $moduleId == 'info' && $controllerId == 'backend/infoPageAdmin',
                'items' => [
                    [
                        'label' => Yii::t('AdminModule.messages', 'Обзор'),
                        'url' => ['/info/backend/infoPageAdmin/index'],
                    ],
                ],
                'visible' => Yii::app()->user->checkAccess("infopagesManagement")
            ],
            [
                'label' => CHtml::tag('i', ['class' => 'icon-file'], ' ') . Yii::t(
                        'AdminModule.messages',
                        'Статистика'
                    ),
                'url' => ['/admin/backend/statistics/index'],
                'active' => $moduleId == 'admin' && $controllerId == 'backend/statistics',
                'items' => [
                    [
                        'label' => Yii::t('AdminModule.messages', 'Обзор'),
                        'url' => ['/admin/backend/statistics/index'],
                    ],
                ],
                'visible' => Yii::app()->user->checkAccess('viewStatistics')
            ]
        ];
    }
}
