<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 30.10.13
 * Time: 9:57
 */

class DateIntervalHelper
{
    /**
     * Создает экземпляр DateInterval из секунд, разибрая их на временные отрезки
     *
     * @param $seconds
     * @return DateInterval
     */
    public static function createDateIntervalFromSeconds($seconds)
    {
        $years = floor($seconds / 60 / 60 / 24 / 365);
        $seconds -= $years * 31536000;
        $months = floor($seconds / 60 / 60 / 24 / 30);
        $seconds -= $months * 2592000;
        $days = floor($seconds / 60 / 60 / 24);
        $seconds -= $days * 86400;
        $hours = floor($seconds / 60 / 60);
        $seconds -= $hours * 3600;
        $minutes = floor($seconds / 60);
        $seconds -= $minutes * 60;

        $intervalInText = "P" . sprintf("%1$04d-%2$02d-%3$02d", $years, $months, $days) . "T" . sprintf(
            "%1$02d:%2$02d:%3$02d",
            $hours,
            $minutes,
            $seconds
        );

        return new DateInterval($intervalInText);
    }

    public static function toSeconds(DateInterval $interval)
    {
        return ($interval->y * 365 * 24 * 60 * 60) +
        ($interval->m * 30 * 24 * 60 * 60) +
        ($interval->d * 24 * 60 * 60) +
        ($interval->h * 60 * 60) +
        ($interval->i * 60) +
        $interval->s;
    }
}
