<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 13.11.13
 * Time: 14:55
 */

class NullableAttrsBehavior extends CActiveRecordBehavior
{

    /**
     * @param CModelEvent $event
     * @return bool
     */
    public function beforeSave($event)
    {
        /** @var CActiveRecord $model */
        $model = $this->getOwner();

        foreach ($model->attributes as $key => $value) {
            if ($value === '') {
                $model->$key = null;
            }
        }

        return true;
    }
}
