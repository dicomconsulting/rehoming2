<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Damir Garifullin
 * Date: 07.10.13
 * Time: 17:40
 *
 * Виджет внутреннего двухуровнего меню
 *
 */

class InnerMenu extends BaseMenuBar
{

    /**
     * @var null|Patient
     */
    public $patient;

    public function _init()
    {
        Yii::import("protocols.models.Research");

        $moduleId = Yii::app()->controller->module->id;
        $controllerId = Yii::app()->controller->id;
        $patientId = Yii::app()->getRequest()->getQuery('id');

        $researchExists = Research::model()->isAnyExists($this->patient);

        $existDeviceData = $this->patient->existDeviceData();

        $this->items = array(
            array(
                'label' => Yii::t('PatientModule.patient', 'Статус'),
                'url'=>array('/patient/status/summary'),
                'visible' => $existDeviceData && $this->patient->device->isActiveTab('status'),
                'items' => array(
                    array(
                        'label' => Yii::t('PatientModule.patient', 'Общий'),
                        'url' => array('/patient/status/summary'),
                        'visible' => $this->patient->device->isActiveTab('status.summary'),
                        'itemOptions' => array(
                            'class' => $this->getClassByAlertLevel(
                                Message::getAlertLevelBySection('status.summary', $this->patient)
                            )
                        )
                    ),
                    array(
                        'label' => Yii::t('PatientModule.patient', 'Устройство'),
                        'url' => array('/patient/status/device'),
                        'visible' => $this->patient->device->isActiveTab('status.device'),
                        'itemOptions' => array(
                            'class' => $this->getClassByAlertLevel(
                                Message::getAlertLevelBySection('status.device', $this->patient)
                            )
                        )
                    ),
                    array(
                        'label' => Yii::t('PatientModule.device', 'Электрод'),
                        'url' => array('/patient/status/lead'),
                        'visible' => $this->patient->device->isActiveTab('status.lead'),
                        'itemOptions' => array(
                            'class' => $this->getClassByAlertLevel(
                                Message::getAlertLevelBySection('status.lead', $this->patient)
                            )
                        )
                    ),
                    array(
                        'label' => Yii::t('PatientModule.patient', 'Брад./РТС'),
                        'url' => array('/patient/status/bradycardia'),
                        'visible' => $this->patient->device->isActiveTab('status.bradycardia'),
                        'itemOptions' => array(
                            'class' => $this->getClassByAlertLevel(
                                Message::getAlertLevelBySection('status.bradycardia', $this->patient)
                            )
                        )
                    ),
                    array(
                        'label' => Yii::t('PatientModule.patient', 'Пред. аритмия'),
                        'url' => array('/patient/status/atrial'),
                        'visible' => $this->patient->device->isActiveTab('status.atrial'),
                        'itemOptions' => array(
                            'class' => $this->getClassByAlertLevel(
                                Message::getAlertLevelBySection('status.atrial', $this->patient)
                            )
                        )
                    ),
                    array(
                        'label' => Yii::t('PatientModule.patient', 'Жел. аритмия'),
                        'url' => array('/patient/status/ventricular'),
                        'visible' => $this->patient->device->isActiveTab('status.ventricular'),
                        'itemOptions' => array(
                            'class' => $this->getClassByAlertLevel(
                                Message::getAlertLevelBySection('status.ventricular', $this->patient)
                            )
                        )
                    ),
                    array(
                        'label' => Yii::t('PatientModule.patient', 'Физ. параметры'),
                        'url' => array('/patient/status/physiologic'),
                        'visible' => $this->patient->device->isActiveTab('status.physiologic'),
                        'itemOptions' => array(
                            'class' => $this->getClassByAlertLevel(
                                Message::getAlertLevelBySection('status.physiologic', $this->patient)
                            )
                        )
                    ),
                    array(
                        'label' => Yii::t('PatientModule.patient', 'HFmonitor'),
                        'url' => array('/patient/status/hf'),
                        'visible' => $this->patient->device->isActiveTab('status.hf'),
                        'itemOptions' => array(
                            'class' => $this->getClassByAlertLevel(
                                Message::getAlertLevelBySection('status.hf', $this->patient)
                            )
                        )
                    ),
                )
            ),
            array(
                'label' => Yii::t('PatientModule.patient', 'Параметры устройства'),
                'url'=>array('/patient/device/overview'),
                'visible' => $existDeviceData && $this->patient->device->isActiveTab('device'),
                'items' => array(
                    array(
                        'label' => Yii::t('PatientModule.patient', 'Обзор'),
                        'url' => array('/patient/device/overview'),
                        'visible' => $this->patient->device->isActiveTab('device.overview'),
                        'itemOptions' => array(
                            'class' => $this->getClassByAlertLevel(
                                Message::getAlertLevelBySection('device.overview', $this->patient)
                            )
                        )
                    ),
                    array(
                        'label' => Yii::t('PatientModule.device', 'Электрод'),
                        'url' => array('/patient/device/lead'),
                        'visible' => $this->patient->device->isActiveTab('device.lead'),
                        'itemOptions' => array(
                            'class' => $this->getClassByAlertLevel(
                                Message::getAlertLevelBySection('device.lead', $this->patient)
                            )
                        )
                    ),
                    array(
                        'label' => Yii::t('PatientModule.patient', 'Брад./РТС'),
                        'url' => array('/patient/device/bradycardia'),
                        'visible' => $this->patient->device->isActiveTab('device.bradycardia'),
                        'itemOptions' => array(
                            'class' => $this->getClassByAlertLevel(
                                Message::getAlertLevelBySection('device.bradycardia', $this->patient)
                            )
                        )
                    ),
                    array(
                        'label' => Yii::t('PatientModule.patient', 'Пред. аритмия'),
                        'url' => array('/patient/device/atrial'),
                        'visible' => $this->patient->device->isActiveTab('device.atrial'),
                        'itemOptions' => array(
                            'class' => $this->getClassByAlertLevel(
                                Message::getAlertLevelBySection('device.atrial', $this->patient)
                            )
                        )
                    ),
                    array(
                        'label' => Yii::t('PatientModule.patient', 'Жел. аритмия'),
                        'url' => array('/patient/device/ventricular'),
                        'visible' => $this->patient->device->isActiveTab('device.ventricular'),
                        'itemOptions' => array(
                            'class' => $this->getClassByAlertLevel(
                                Message::getAlertLevelBySection('device.ventricular', $this->patient)
                            )
                        )
                    ),
                    array(
                        'label' => Yii::t('PatientModule.patient', 'Домашний мониторинг'),
                        'url' => array('/patient/device/hm'),
                        'visible' => $this->patient->device->isActiveTab('device.hm'),
                        'itemOptions' => array(
                            'class' => $this->getClassByAlertLevel(
                                Message::getAlertLevelBySection('device.hm', $this->patient)
                            )
                        )
                    ),
                )
            ),
            array(
                'label'=> Yii::t('PatientModule.patient', 'Записи'),
                'url'=>array('/patient/recording/list'),
                'active' => $moduleId == 'patient' && $controllerId == 'recording',
                'itemOptions' => array(
                    'class' => $this->getClassByAlertLevel(
                        Message::getAlertLevelBySection('recording.list', $this->patient)
                    )
                )
            ),
            array(
                'label'=> Yii::t('PatientModule.patient', 'История'),
                'url'=>array('/patient/history/list')
            ),
            array(
                'label' => Yii::t('PatientModule.patient', 'Профиль пациента'),
                'url' => array('/patient/profile/view/index'),
                'items' => array(
                    array(
                        'label' => Yii::t('PatientModule.patient', 'Общие данные'),
                        'url' => array('/patient/profile/view/index'),
                    ),
                    array(
                        'label' => Yii::t('PatientModule.patient', 'Клинические данные'),
                        'url' => array('/patient/profile/clinical/index'),
                        'active' => $moduleId == 'patient' && $controllerId == 'profile/clinical'
                    ),
                    array(
                        'label' => Yii::t('PatientModule.patient', 'Эхокардиография'),
                        'url' => array('/patient/profile/ekg/index'),
                        'active' => $moduleId == 'patient' && $controllerId == 'profile/ekg'
                    ),
                    array(
                        'label' => Yii::t('PatientModule.patient', 'Холтер'),
                        'url' => array('/patient/profile/holter/index'),
                        'active' => $moduleId == 'patient' && $controllerId == 'profile/holter'
                    ),
                )
            ),
            array(
                'label'=> Yii::t('PatientModule.patient', 'Опции'),
                'url'=>array('/patient/options/index'),
            ),
            array(
                'label'=> Yii::t('PatientModule.patient', 'Документы'),
                'url'=>array('/protocols/list/index'),
                'active' => $moduleId == "protocols",
                'items' => array(
                    array(
                        'label' => 'Включение',
                        'url' => array('/protocols/enrolment'),
                        'active' => $moduleId == 'protocols' && $controllerId == 'enrolment'
                    ),
                    array('label' => 'Инициализация HM',
                        'url' => array('/protocols/inithm'),
                        "visible" => $researchExists,
                        'active' => $moduleId == 'protocols' && $controllerId == 'inithm'
                    ),
                    array(
                        'label' => 'Оценка HM',
                        'url' => array('/protocols/evolution'),
                        "visible" => $researchExists,
                        'active' => $moduleId == 'protocols' && $controllerId == 'evolution'
                    ),
                    array('label' => 'Обследование через 12 месяцев',
                        'url' => array('/protocols/annual'),
                        "visible" => $researchExists,
                        'active' => $moduleId == 'protocols' && $controllerId == 'annual'
                    ),
                    array(
                        'label' => 'Нежелательное явление',
                        'url' => array('/protocols/adverse'),
                        "visible" => $researchExists,
                        'active' => $moduleId == 'protocols' && $controllerId == 'adverse'
                    ),
                    array(
                        'label' => 'Регулярное обследование',
                        'url' => array('/protocols/regular'),
                        "visible" => $researchExists,
                        'active' => $moduleId == 'protocols' && $controllerId == 'regular'
                    ),
                    array(
                        'label' => 'Окончание',
                        'url' => array('/protocols/finish'),
                        "visible" => $researchExists,
                        'active' => $moduleId == 'protocols' && $controllerId == 'finish'
                    ),
                ),
                'visible' => Yii::app()->user->checkAccess("viewProtocols")
            ),
        );

        $this->addParamRecursive($this->items, array("id" => $patientId));
    }

    protected function addParamRecursive(&$items, $params)
    {

        foreach ($items as &$item) {
            if (isset($item['url']) && is_array($item['url'])) {
                foreach ($params as $param => $val) {
                    $item['url'][$param] = $val;
                }
            }

            if (isset($item['items']) && is_array($item['items'])) {
                $this->addParamRecursive($item['items'], $params);
            }
        }

    }

    protected function getClassByAlertLevel($level)
    {
        if ($level == AbstractOption::ALERT_LEVEL_RED_WITH_NOTICE) {
            return "red_alert";
        } elseif ($level == AbstractOption::ALERT_LEVEL_YELLOW
            || $level == AbstractOption::ALERT_LEVEL_YELLOW_WITH_NOTICE) {
            return "yellow_alert";
        }
        return "";
    }
}
