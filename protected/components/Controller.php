<?php
Yii::import('patient.models.Patient');
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return [
            [
                'allow',
                'roles' => ['viewPatientCard']
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/main',
     * meaning using a single column layout. See 'protected/views/layouts/main.php'.
     */
    public $layout='//layouts/main';
    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu=array();

    /**
     * @var Patient
     */
    public $patient = null;

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param  integer        $id the ID of the model to be loaded
     * @return Patient        the loaded model
     * @throws CHttpException
     */
    public function loadPatientModel($id)
    {
        $model=Patient::model()->onlyAssigned()->findByPk($id);
        if ($model===null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        $this->patient = $model;
        $this->patient->updateLastViewTime();
        return $model;
    }
}
