<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h3>Главная страница проекта "<i><?php echo CHtml::encode(Yii::app()->name); ?></i>"</h3>

<p>Инструмент для врачей-кардиологов, помогающий в мониторинге людей с импантированым кардиостимлятором.</p>
