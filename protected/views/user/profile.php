<?php
/* @var $this UserController */
/* @var $user User */

$this->pageTitle = Yii::app()->name;
?>

<div class="margin30">
    <table class="table table-condensed rh-main-table">
        <col width="30%" valign="middle">
        <col width="70%" valign="middle">
        <thead>
        <tr>
            <th colspan="2">
                <?= Yii::t('general', 'Общие данные'); ?>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <?= Yii::t('general', 'Логин'); ?>
            </td>
            <td>
                <?= $user->username; ?>
            </td>
        </tr>
        <tr>
            <td>
                <?= Yii::t('general', 'Имя'); ?>
            </td>
            <td>
                <?= $user->getDisplayName(); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?= Yii::t('general', 'Тип учетной записи'); ?>
            </td>
            <td>
                <?= $user->authentication_type; ?>
            </td>
        </tr>
        </tbody>
    </table>
</div>

<?= TbHtml::form() ?>
<div class="margin30">
    <?php if (Yii::app()->user->hasFlash('success')) {
        echo \TbHtml::alert(
            \TbHtml::ALERT_COLOR_SUCCESS,
            Yii::app()->user->getFlash('success')
        );
    } ?>
    <?php if (Yii::app()->user->hasFlash('error')) {
        echo \TbHtml::alert(
            \TbHtml::ALERT_COLOR_DANGER,
            Yii::app()->user->getFlash('error')
        );
    } ?>
    <table class="table table-condensed rh-main-table">
        <col width="30%" valign="middle">
        <col width="70%" valign="middle">
        <thead>
        <tr>
            <th colspan="2">
                <?= Yii::t('general', 'Персональные данные пациентов'); ?>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <?= Yii::t('general', 'Ссылка для получения персоальных данных'); ?>
            </td>
            <td>
                <?php echo TbHtml::textField('settings[depersonalizationUrl]', $user->settings['depersonalizationUrl']); ?>
            </td>
        </tr>
        </tbody>
    </table>
    <?php echo TbHtml::submitButton(Yii::t('general', 'Сохранить')); ?>
</div>
</form>