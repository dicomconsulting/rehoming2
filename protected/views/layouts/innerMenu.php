<?php /* @var $this Controller */

Yii::app()->clientScript->registerCoreScript('jquery');
Yii::app()->clientScript->registerCoreScript('bootstrap');
Yii::app()->clientScript->registerCoreScript('jquery.ui');
Yii::app()->clientScript->registerCoreScript('prettify');


Yii::app()->clientScript->registerScriptFile(
    Yii::app()->request->getBaseUrl(true) . '/js/jquery.ui.datepicker-ru.js',
    CClientScript::POS_END
);

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->request->getBaseUrl(true) . '/js/common.js',
    CClientScript::POS_END
);

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->request->getBaseUrl(true) . '/js/timepicker/jquery.ui.timepicker.js',
    CClientScript::POS_END
);

Yii::app()->clientScript->registerCssFile(
    Yii::app()->request->getBaseUrl(true) . '/js/timepicker/jquery.ui.timepicker.css'
);

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->request->getBaseUrl(true) . '/js/autosize/jquery.autosize.js',
    CClientScript::POS_END
);
?>
<!DOCTYPE html>
<html lang="<?=Yii::app()->language?>"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="<?=Yii::app()->language?>" />

    <!-- styles -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/custom-theme/jquery-ui-1.10.0.custom.css" rel="stylesheet" />
    <link type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css" rel="stylesheet" />
    <!--[if IE 7]>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome-ie7.min.css">
    <![endif]-->
    <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/custom-theme/jquery.ui.1.10.0.ie.css"/>
    <![endif]-->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/docs.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/js/google-code-prettify/prettify.css" rel="stylesheet">

    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap-override.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


    <!-- fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo Yii::app()->request->baseUrl; ?>/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo Yii::app()->request->baseUrl; ?>/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo Yii::app()->request->baseUrl; ?>/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo Yii::app()->request->baseUrl; ?>/ico/apple-touch-icon-57-precomposed.png">

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body data-spy="scroll" data-target=".bs-docs-sidebar" data-twttr-rendered="true">

<header class="jumbotron subhead" id="overview">
    <div class="container">
        <h1><?php echo CHtml::encode(Yii::app()->name); ?></h1>
        <p class="lead"><?=Yii::t("general", "Система мониторинга показателей кардиоимплантатов");?></p>
        <?php if(!Yii::app()->user->isGuest):?>
            <div style="float: right;">
                <?=CHtml::link(Yii::app()->user->name, $this->createUrl('/user/profile'), ['title' => Yii::t("general", "Текущий пользователь")])?>
            </div>
        <?php endif;?>
        <?php $this->widget('LanguageSwitcherWidget');?>
    </div>
</header>

<div class="container">
<div class="row">
    <div class="span2 bs-docs-sidebar">

        <?php $this->widget('LeftSideMenu', array(
            "htmlOptions" => array(
                "class" => "nav nav-list",
            ),
            "submenuHtmlOptions" => array('class' => 'nav nav-list'),
            ));?>
    </div>
    <div class="span10">
    <?php
    if (!empty($this->patient)) { ?>
        <div>
            <?php $this->widget('application.modules.patient.components.PatientInfoWidget', array('patient' => $this->patient));?>
        </div>
    <?php
    } ?>
        <div id="page">

            <?php $this->widget('InnerMenu', array(
                "htmlOptions" => array(
                    "class" => "nav nav-tabs navbar-toplevel",
                ),
                "submenuHtmlOptions" => array('class' => 'nav'),
                "submenuHtmlBefore" => '<div class="navbar navbar-bottomlevel"><div class="navbar-inner">',
                "submenuHtmlAfter" => '</div></div>',
                "patient" => $this->patient

            ));?>

            <?php echo $content; ?>

            <div class="clear"></div>


        </div><!-- page -->
    </div>
</div>
</div>

<footer class="footer">
    <div class="container">
        <p>
            ReHoming &copy; <?php echo date('Y'); ?> by CIR.<br/>
        </p>
        <ul class="footer-links">
            <li><a href="http://www.dicoming.ru/" target="_blank">DC</a></li>
            <li class="muted">&middot;</li>
            <li><a href="http://kirkazan.ru/" target="_blank">CIR</a></li>
            <li class="muted">&middot;</li>
            <li><a href="https://www.biotronik.com/" target="_blank">Biotronik</a></li>
        </ul>
    </div>
</footer>

</body>
</html>
