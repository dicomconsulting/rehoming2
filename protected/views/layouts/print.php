<?php /* @var $this Controller */

Yii::app()->clientScript->registerScriptFile(Yii::app()->request->getBaseUrl(true) . '/js/jquery-1.9.0.min.js',
    CClientScript::POS_HEAD
);

Yii::app()->clientScript->registerScriptFile(Yii::app()->request->getBaseUrl(true) . '/js/jquery-ui-1.10.0.custom.min.js',
    CClientScript::POS_BEGIN
);

Yii::app()->clientScript->registerScriptFile(Yii::app()->request->getBaseUrl(true) . '/js/bootstrap.min.js',
    CClientScript::POS_BEGIN
);

Yii::app()->clientScript->registerScriptFile(Yii::app()->request->getBaseUrl(true) . '/js/google-code-prettify/prettify.js',
    CClientScript::POS_END
);

Yii::app()->clientScript->registerScriptFile(Yii::app()->request->getBaseUrl(true) . '/js/autosize/jquery.autosize.js',
    CClientScript::POS_END
);
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->getBaseUrl(true) . '/js/print.js',
    CClientScript::POS_END
);
?>
<!DOCTYPE html>
<html lang="<?=Yii::app()->language?>"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="<?=Yii::app()->language?>" />

    <!-- styles -->
    <link href="<?=Yii::app()->request->getBaseUrl(true);?>/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="<?=Yii::app()->request->getBaseUrl(true);?>/css/custom-theme/jquery-ui-1.10.0.custom.css" rel="stylesheet" />
    <link type="text/css" href="<?=Yii::app()->request->getBaseUrl(true);?>/css/font-awesome.min.css" rel="stylesheet" />
    <!--[if IE 7]>
    <link rel="stylesheet" href="<?=Yii::app()->request->getBaseUrl(true);?>/css/font-awesome-ie7.min.css">
    <![endif]-->
    <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="<?=Yii::app()->request->getBaseUrl(true);?>/css/custom-theme/jquery.ui.1.10.0.ie.css"/>
    <![endif]-->
    <link href="<?=Yii::app()->request->getBaseUrl(true);?>/css/docs.css" rel="stylesheet">
    <link href="<?=Yii::app()->request->getBaseUrl(true);?>/js/google-code-prettify/prettify.css" rel="stylesheet">

    <link href="<?=Yii::app()->request->getBaseUrl(true);?>/css/bootstrap-override.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=Yii::app()->request->getBaseUrl(true);?>/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=Yii::app()->request->getBaseUrl(true);?>/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=Yii::app()->request->getBaseUrl(true);?>/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?=Yii::app()->request->getBaseUrl(true);?>/ico/apple-touch-icon-57-precomposed.png">

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body data-spy="scroll" data-target=".bs-docs-sidebar" data-twttr-rendered="true">

<div class="container">
<div class="row">
    <div class="span10">
        <div id="page"
            <?php if(isset(Yii::app()->params['pageContainerWidth'])):?> style="width: <?=Yii::app()->params['pageContainerWidth']?>"<?php endif;?>>

            <?php echo $content; ?>

            <div class="clear"></div>


        </div><!-- page -->
    </div>
</div>
</div>


</body>
</html>
