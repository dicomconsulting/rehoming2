<?php
$ds = DIRECTORY_SEPARATOR;
//Composer autoloader
require_once(dirname(__FILE__) . $ds . '..' . $ds .'vendor' . $ds . 'autoload.php');
define('YII_CRON', false);

// change the following paths if necessary
$yiic = dirname(__FILE__) . $ds . '..' . $ds .'vendor' . $ds . 'yiisoft' . $ds . 'yii' . $ds . 'framework' . $ds . 'yiic.php';
$config = dirname(__FILE__) . $ds . 'config' . $ds . 'console.php';

require_once(dirname(__FILE__) . $ds . '..' . $ds .'vendor' . $ds . 'yiisoft' . $ds . 'yii' . $ds . 'framework' . $ds . 'yii.php');
require_once(dirname(__FILE__) . '/modules/admin/components/SettingManager.php');
$settingManager = new SettingManager(dirname(__FILE__) . $ds . '..');
$config = $settingManager->getConfigArray($config);

require_once($yiic);
