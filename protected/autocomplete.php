<?php
/**
 * @property CAuthManager $authManager The authorization manager component.
 * @property CAssetManager $assetManager The asset manager component.
 * @property CFormatter $format provides a set of commonly used data formatting methods.
 * @property CHttpSession $session The session component.
 * @property RhWebUser $user The user session information.
 * @property UserGroupManager $userGroupManager The user group information.
 * @property TaskRunner $taskRunner
 * @property BiotronikSpider $biotronikSpider The user group information.
 * @property IViewRenderer $viewRenderer The view renderer.
 * @property CClientScript $clientScript The client script manager.
 * @property CWidgetFactory $widgetFactory The widget factory.
 * @property CThemeManager $themeManager The theme manager.
 * @property CTheme $theme The theme used currently. Null if no theme is being used.
 * @property Controller $controller The currently active controller.
 * @property string $controllerPath The directory that contains the controller classes. Defaults to 'protected/controllers'.
 * @property string $viewPath The root directory of view files. Defaults to 'protected/views'.
 * @property string $systemViewPath The root directory of system view files. Defaults to 'protected/views/system'.
 * @property string $layoutPath The root directory of layout files. Defaults to 'protected/views/layouts'.
 *
 * @method CClientScript getClientScript()
 * @method CAssetManager getAssetManager()
 * @method RhWebUser getUser()
 * @method AdminModule|I18nModule|PatientModule|ProtocolsModule|RightsModule|PdfableModule getModule()
 *
 */
class CApplication {}