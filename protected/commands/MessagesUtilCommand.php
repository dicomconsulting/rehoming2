<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 28.10.13
 * Time: 17:04
 *
 * Generates CSV file from messages file and converts source strings to cp1251
 */

class MessagesUtilCommand extends CConsoleCommand
{

    public function actionMakeCsv($messageFile = false, $outputFile = false)
    {
        if (!$messageFile || !$outputFile) {
            echo $this->getMakeCsvHelp();
            return;
        }

        if (!file_exists($messageFile)) {
            echo "Messages file $messageFile doesnt exists";
            return;
        }

        $rows = require($messageFile);

        $fp = fopen($outputFile, 'w');

        foreach ($rows as $key => $row) {
            $str = iconv("utf-8", "CP1251", $key);
            fputcsv($fp, array($str, $row), ";");
        }

        fclose($fp);

        echo "done";
    }

    public function getMakeCsvHelp()
    {
        return <<<HELP
You need to specify 'messageFile' and 'outputFile' variables.
HELP;
    }

    public function actionMakeMessages($csvFile = false, $outputFile = false, $appendData = false)
    {
        if (!$csvFile || !$outputFile) {
            echo $this->getMakeMessagesHelp();
            return;
        }

        if (!file_exists($csvFile)) {
            echo "Csv file $csvFile doesnt exists";
            return;
        }

        $source = fopen($csvFile, "r");

        $result = array();

        $existingData = require($outputFile);


        while ($row = fgetcsv($source)) {
            if (!$appendData && !isset($existingData[$row[0]])) {
                continue;
            }
            $existingData[$row[0]] = $row[1];
        }

        $data = "<?php \nreturn " . var_export($existingData, true) . ";";
        file_put_contents($outputFile, $data);

        echo "done";
    }

    public function getMakeMessagesHelp()
    {
        return <<<HELP
Updates existing messages file with translated data. If 'appendData' variable specified, it will create new rows if existing not found.
You need to specify 'csvFile' and 'outputFile' variables.
HELP;
    }

    public function actionUpdateOriginalPhrases($csvFile = false, $module = false)
    {
        if (!$csvFile) {
            echo $this->getReplaceOriginalHelp();
            return;
        }

        if (!file_exists($csvFile)) {
            echo "Csv file $csvFile doesnt exists";
            return;
        }

        $source = fopen($csvFile, "r");

        $dataToReplace = [];

        while ($row = fgetcsv($source)) {
            if ($row[0] != $row[2]) {
                $dataToReplace[trim($row[2])] = trim($row[0]);
            }
        }

        if ($module) {
            $sourcePath = Yii::getPathOfAlias($module);
        } else {
            $sourcePath = Yii::getPathOfAlias("application");
        }

        $options = [];
        $options['fileTypes'] = array('php', 'phtml');

        $files = CFileHelper::findFiles(realpath($sourcePath), $options);

        $messages = array();
        foreach ($files as $file) {
            $this->replacePhrasesInFile($file, $dataToReplace);
        }

        var_export($dataToReplace);

        echo "done";
    }

    public function getReplaceOriginalHelp()
    {
        return <<<HELP
Updates existing phrases in all files with new.
HELP;
    }

    protected function replacePhrasesInFile($fileName, &$data)
    {
        $subject = file_get_contents($fileName);

        $currentTranslator = "Yii::t";
        $reg = '/\b'
            . $currentTranslator
            . '\s*\(\s*(\'[\w.\/]*?(?<!\.)\'|"[\w.]*?(?<!\.)")\s*,\s*(\'.*?(?<!\\\\)\'|".*?(?<!\\\\)")\s*[,\)]/s';

        $n = preg_match_all($reg, $subject, $matches, PREG_SET_ORDER);

        $changed = false;

        if ($n) {
            for ($i = 0; $i < $n; ++$i) {
                if (($pos = strpos($matches[$i][1], '.')) !== false) {
                    $category = substr($matches[$i][1], $pos + 1, -1);
                } else {
                    $category = substr($matches[$i][1], 1, -1);
                }

                $message = eval("return {$matches[$i][2]};");

                if (isset($data[trim($message)])) {
                    $subject = str_replace(
                        $matches[$i][0],
                        "Yii::t('$category','{$data[trim($message)]}')",
                        $subject,
                        $count
                    );

                    echo $fileName . " " . $message . " - $count\n";
                    $changed = true;
                }
            }
        }

        if ($changed) {
            file_put_contents($fileName, $subject);
        }
    }
}
