<?php
return [
    'Система мониторинга показателей кардиоимплантатов' => 'Monitoring system of the cardiac implants\' indicators',
    'Пациенты' => 'Patients',
    'Профиль' => 'Profile',
    'Все' => 'All',
    'Требуют внимания' => 'Demand attention',
    'Статистика' => 'Statistics',
    'Администрирование' => 'Administrator panel',
    'Инструменты' => 'Tools',
    'Выйти' => 'Sign out',
    'Статус на' => 'Status on',
    'Значение по измерению "{param_name}" вышло за пределы допустимых' => 'Value measurement "{param_name}" has gone beyond the acceptable',
    '{n} дня|{n} дней|{n} дней|{n} дня' => '{n} day|{n} days|{n} days|{n} days',
    'Начало события - ' => 'Start of event - ',
    'Уведомлен' => 'Acknowledge',
    'Мед. центр' => 'Medical center',
    'Код пациента' => 'Patient code',
    'Пользовательский режим' => 'User mode',
    'Выход из панели администрирования в пользовательский режим' => 'Exit from the administration panel in user mode',
    'Общие данные' => 'General data',
    'Логин' => 'Login',
    'Имя' => 'Full name',
    'Тип учетной записи' => 'Account type',
];