<?php
set_time_limit(1800);

defined('YII_CRON') or define('YII_CRON', true);
$ds = DIRECTORY_SEPARATOR;
//Composer autoloader
require_once(dirname(__FILE__) . $ds . '..' . $ds .'vendor' . $ds . 'autoload.php');

// change the following paths if necessary
$yiic = dirname(__FILE__) . $ds . '..' . $ds .'vendor' . $ds . 'yiisoft' . $ds . 'yii' . $ds . 'framework' . $ds . 'yiic.php';
$config = dirname(__FILE__) . $ds . 'config' . $ds . 'cron.php';

require_once(dirname(__FILE__) . $ds . '..' . $ds .'vendor' . $ds . 'yiisoft' . $ds . 'yii' . $ds . 'framework' . $ds . 'yii.php');
require_once(dirname(__FILE__) . '/modules/admin/components/SettingManager.php');
$settingManager = new SettingManager(dirname(__FILE__) . $ds . '..');
$config = $settingManager->getConfigArray($config);

require_once($yiic);
