<?php
/**
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * ПОСЛЕ ИЗМЕНЕНИЯ ПОЧИСТИТЬ runtime/settingsCache
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */

return CMap::mergeArray(
    require(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'console.php'),
    [
        'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
        'name' => 'Cron',
        'preload' => ['log'],
        'components' => [
            'log' => [
                'class' => 'CLogRouter',
                'routes' => [
                    [
                        'class'=>'CFileLogRoute',
                        'logFile'=>'cron.log',
                        'levels'=>'error, warning',
                    ],
                    [
                        'class'=>'CFileLogRoute',
                        'logFile'=>'cron_info.log',
                        'levels'=>'info',
                    ],
                    [
                        'class'=>'CFileLogRoute',
                        'logFile'=>'cron_trace.log',
                        'levels'=>'trace',
                    ],
                ],
            ],
        ],
    ]
);
