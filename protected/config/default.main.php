<?php
/**
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * ПОСЛЕ ИЗМЕНЕНИЯ ПОЧИСТИТЬ runtime/settingsCache
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */

mb_internal_encoding("UTF-8");

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath'          => __DIR__ . DIRECTORY_SEPARATOR . '..',
    'homeUrl'           => ['/patient/list/index'],
    'defaultController' => 'patient/list',
    'name'              => '%webName%',
    'aliases'           => array(
        'extvendors' => __DIR__ . DIRECTORY_SEPARATOR . '..'
            . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor',
    ),
    // preloading 'log' component
    'preload'           => array('log'),
    // autoloading model and component classes
    'import'            => array(
        'application.models.*',
        'application.components.*',
        'application.modules.i18n.components.*',
        'application.modules.admin.components.*',
        'zii.widgets.*',
        'ext.*',
    ),
    'modules'           => array(
        // uncomment the following to enable the Gii tool

        'gii'     => array(
            'class'     => 'system.gii.GiiModule',
            'password'  => 'pass',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '10.1.1.51', '10.1.1.53', '::1'),
        ),
        'admin',
        'i18n'    => [
            'translatedLanguages' => '%translatedLanguages%',
            'defaultLanguage'     => '%defaultLanguage%',
        ],
        'directory',
        'device',
        'protocols',
        'dataSource',
        'patient'=> [
            'depersonalization' => true,
        ],
        'info',
        'option',
        'statistics',
        'statisticsNew',
        'statisticsUpdate',
        'statistics2018',
        'pdfable' => array('class' => 'ext.pdfable.pdfable.PdfableModule'),
        'rights'  => array('class' => 'ext.rights.RightsModule'),
        'task'
    ),
    'language'          => 'ru',
    'sourceLanguage'    => 'ru',
    'localeDataPath'    => __DIR__ . DIRECTORY_SEPARATOR . '..'
        . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . 'i18n'
        . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'locale',
    'components'        => array(
        'biotronikSpider'  => array(
            'class'      => 'dataSource.components.BiotronikSpider',
            'domainName' => '%biotronikDomainName%',
        ),
        'user'             => array(
            'class'          => 'admin.components.RhWebUser',
            'allowAutoLogin' => true,
            'loginUrl'       => array('/admin/backend/user/login'),
            'authTimeout'    => '3600'
        ),
        'userGroupManager' => array('class' => 'admin.components.UserGroupManager'),
        'request'          => array('class' => 'LanguageHttpRequest'),
        'urlManager'       => array(
            'class'          => 'LanguageUrlManager',
            'urlFormat'      => 'path',
            'showScriptName' => false,
            'rules'          => array(
                '<controller:\w+>/<id:\d+>'              => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>'          => '<controller>/<action>',
            ),
        ),
        'db'               => array(
            'connectionString' => 'mysql:host=localhost;dbname=rehoming',
            'emulatePrepare'   => true,
            'username'         => 'root',
            'password'         => '',
            'charset'          => 'utf8',
            'tablePrefix'      => 'rh_',
        ),
        'authManager'      => array(
            'class'           => 'CDbAuthManager',
            'connectionID'    => 'db',
            'itemTable'       => '{{auth_item}}',
            'itemChildTable'  => '{{auth_item_child}}',
            'assignmentTable' => '{{auth_assignment}}',
        ),
        'errorHandler'     => array('errorAction' => 'site/error'),
        'log'              => array(
            'class'  => 'CLogRouter',
            'routes' => array(
                array(
                    'class'  => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),
        'mustache'         => array(
            'class'             => 'application.extensions.MustacheApplicationComponent',
            // Default settings (not needed)
            'templatePathAlias' => 'application.templates',
            'templateExtension' => 'mustache',
            'extension'         => true,
        ),
        'mailer'           => array(
            'class'          => 'application.components.RhSMTPNotifier',
            'smtpHost'       => '%smtpHost%',
            'smtpPort'       => '%smtpPort%',
            'username'       => '%mailUsername%',
            'password'       => '%mailPassword%',
            'adminAddresses' => '%adminAddresses%'
        ),
        //переопределения для ЖС встроенных в YII
        'clientScript'     => array(
            'packages' => array(
                'jquery'    => array(
                    'baseUrl'            => '/js/',
                    'js'                 => array('jquery-1.9.0.min.js'),
                    'coreScriptPosition' => CClientScript::POS_HEAD
                ),
                'jquery.ui' => array(
                    'baseUrl'            => '/js/',
                    'js'                 => array('jquery-ui-1.10.0.custom.min.js'),
                    'depends'            => array('jquery'),
                    'coreScriptPosition' => CClientScript::POS_BEGIN
                ),
                'bootstrap' => array(
                    'baseUrl'            => '/js/',
                    'js'                 => array('bootstrap.min.js'),
                    'depends'            => array('jquery'),
                    'coreScriptPosition' => CClientScript::POS_BEGIN
                ),
                'prettify'  => array(
                    'baseUrl'            => '/js/google-code-prettify/',
                    'js'                 => array('prettify.js'),
                    'depends'            => array('jquery'),
                    'coreScriptPosition' => CClientScript::POS_BEGIN
                )
            ),
        ),
        'widgetFactory'    => array(
            'widgets' => array(
                //отключение подключения встроенного в YII jQuery UI поскольку он конфиликтует с используемым на проекте
                //это необходимо прописать для всех виджетов CJui*, которые используюся на проекте
                'CJuiWidget'       => array('scriptFile' => false),
                'CJuiInputWidget'  => array('scriptFile' => false),
                'CJuiDialog'       => array('scriptFile' => false),
                'CJuiAutoComplete' => array('scriptFile' => false),
            ),
        )
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'            => [
        'wkhtmltopdf' => '/usr/bin/wkhtmltopdf',
    ],
);
