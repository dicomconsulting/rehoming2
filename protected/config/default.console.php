<?php
/**
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * ПОСЛЕ ИЗМЕНЕНИЯ ПОЧИСТИТЬ runtime/settingsCache
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */

mb_internal_encoding("UTF-8");

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
    'basePath'=>__DIR__.DIRECTORY_SEPARATOR.'..',
    'name'=>'%consoleName%',

    'aliases'=>array(
        'extvendors' => __DIR__ . DIRECTORY_SEPARATOR . '..'
            . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor',
    ),

    'modules' => array(
        'admin',
        'patient',
        'dataSource',
        'install',
        'directory',
        'device',
        'protocols',
        'i18n',
        'info',
        'option',
        'statistics',
        'statisticsNew',
        'statisticsUpdate',
        'statistics2018',
        'task'
    ),

    'language'=>'ru',
    'sourceLanguage'=>'ru',
    'localeDataPath' => __DIR__ . DIRECTORY_SEPARATOR . '..'
        . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . 'i18n'
        . DIRECTORY_SEPARATOR . 'data'. DIRECTORY_SEPARATOR . 'locale',

    // preloading 'log' component
    'preload'=>array('log'),

    'import'=>array(
        'application.modules.i18n.components.I18nDbMigration',
        'application.modules.dataSource.models.*',
        'application.modules.dataSource.components.*',
        'ext.*',
        'application.components.*'
    ),

    // application components
    'components'=>array(
        'biotronikSpider' => array(
            'class' => 'dataSource.components.BiotronikSpider',
            'domainName' => '%biotronikDomainName%',
        ),
        'userGroupManager'=>array(
            'class'=>'admin.components.UserGroupManager',
        ),
        'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=rehoming',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
            'tablePrefix' => 'rh_',
        ),
        'authManager'=>array(
            'class'=>'CDbAuthManager',
            'connectionID'=>'db',
            'itemTable' =>'{{auth_item}}',
            'itemChildTable' =>'{{auth_item_child}}',
            'assignmentTable' =>'{{auth_assignment}}',
        ),
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning',
                ),
            ),
        ),
        'mailer' => array(
            'class' => 'application.components.RhSMTPNotifier',
            'smtpHost' => '%smtpHost%',
            'smtpPort' => '%smtpPort%',
            'username' => '%mailUsername%',
            'password' => '%mailPassword%',
            'adminAddresses' => '%adminAddresses%'
        ),
        'taskRunner' => array('class' => 'task.components.TaskRunner'),
    ),
    'commandMap' => array(
        'migrate' => array(
            'class' => 'extvendors.yiiext.migrate-command.EMigrateCommand',
            'migrationPath' => 'application.db.migrations',
            'migrationTable' => '{{migration}}',
            'applicationModuleName' => 'core',
            'modulePaths' => array(
                'admin' => 'application.modules.admin.migrations',
                'directory' => 'application.modules.directory.migrations',
                'device' => 'application.modules.device.migrations',
                'data' => 'application.modules.dataSource.migrations',
                'patient' => 'application.modules.patient.migrations',
                'protocols' => 'application.modules.protocols.migrations',
                'info' => 'application.modules.info.migrations',
                'option' => 'application.modules.option.migrations',
                'statistics' => 'application.modules.statistics.migrations',
                'statisticsNew' => 'application.modules.statisticsNew.migrations',
                'tasks' => 'application.modules.task.migrations'
            ),
            'migrationSubPath' => 'migrations',
            'disabledModules' => array(),
            'connectionID'=>'db',
            'templateFile'=>'application.modules.i18n.data.migration_template',
        ),
        'spider' => array('class' => 'application.modules.dataSource.commands.BiotronikCommand'),
        'mail' => array(
            'class' => 'application.modules.dataSource.commands.MailCommand',
            'biotronikEmail' => '%biotronikEmail%'
        ),
        'install' => array('class' => 'application.modules.install.commands.InstallCommand'),
        'authorization' => array('class' => 'application.modules.admin.commands.AuthorizationCommand'),
        'task-manager' => array('class' => 'application.modules.task.commands.TaskManagerCommand')
    ),
    'params' => array(
        'i18n' => array(
            'tableSuffix' => '_i18n',
            'defaultLocale' => 'ru'
        )
    ),
);
