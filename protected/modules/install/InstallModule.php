<?php

class InstallModule extends CWebModule
{
    public function init()
    {
        parent::init();
        Yii::setPathOfAlias('install', dirname(__FILE__));

        Yii::app()->getModule("device");

        $this->setImport(
            'install.models.*'
        );
    }
}
