<?php
Yii::import('application.modules.install.models.*');

class InstallCommand extends CConsoleCommand
{
    public function actionIndex()
    {
        $measurementUnitDirectory = new MeasurementUnitDirectory();
        $measurementUnitDirectory->fill();

        $deviceParameterDirectory = new DeviceParameterDirectory();
        $deviceParameterDirectory->fill();

        $protocolsDirectory = new ProtocolDirectory();
        $protocolsDirectory->fill();

        $surgeryDirectory = new SurgeryDirectory;
        $surgeryDirectory->fill();

        $icd10Directory = new Icd10Directory;
        $icd10Directory->fill();

        $pharmClinicGroupDirectory = new PharmClinicGroupDirectory;
        $pharmClinicGroupDirectory->fill();

        $pharmMeasureDirectory = new PharmMeasureDirectory;
        $pharmMeasureDirectory->fill();

        $pharmProductDirectory = new PharmProductDirectory;
        $pharmProductDirectory->fill();

        $pharmProductRelDirectory = new PharmProductClinicGroupRel;
        $pharmProductRelDirectory->fill();

        $regionDirectory = new RegionDirectory;
        $regionDirectory->fill();

        $countryDirectory = new CountryDirectory();
        $countryDirectory->fill();

        $cityDirectory = new CityDirectory();
        $cityDirectory->fill();

        $medCenterDirectory = new MedicalCenterDirectory();
        $medCenterDirectory->fill();

        $nyhaDirectory = new NyhaClassDirectory();
        $nyhaDirectory->fill();

        $electrodePlacementDirectory = new ElectrodePlacementDirectory();
        $electrodePlacementDirectory->fill();

        $sectionDirectory = new SectionDirectory();
        $sectionDirectory->fill();

        $optionTypeDirectory = new OptionTypeDirectory();
        $optionTypeDirectory->fill();

        $optionDirectory = new OptionDirectory();
        $optionDirectory->fill();

        $bgDirectory = new BlockadeGradeDirectory();
        $bgDirectory->fill();

        $trDirectory = new TransmitterDirectory();
        $trDirectory->fill();

        $arrDict = new ArrCuppingDictionary();
        $arrDict->fill();

        $ttDict = new TachyTypeDictionary();
        $ttDict->fill();

        $acDict = new AttackConsciousnessDictionary();
        $acDict->fill();

        $dpDict = new DevicePolarityDictionary();
        $dpDict->fill();

        $ccDict = new CaptureControlDictionary();
        $ccDict->fill();

        $arDict = new AdverseResultDictionary();
        $arDict->fill();

        $settingDirectory = new MainSettingDirectory();
        $settingDirectory->fill();

        $ssDict = new StatisticsSectionDirectory();
        $ssDict->fill();

        $statDict = new StatisticsDirectory();
        $statDict->fill();

        $modelTypeDict = new DeviceTypeDictionary();
        $modelTypeDict->fill();

        $modelModelDict = new DeviceModelDictionary();
        $modelModelDict->fill();
    }

    public function actionSetDisableTabSettings()
    {
        $rows = [
            ['device', 'Cylos DR-T'],
            ['device', 'Philos II DR-T'],
            ['device.atrial', 'Lumax 300 VR-T'],
            ['device.atrial', 'Lumax 340 VR-T (XL)'],
            ['status.hf', 'Cylos DR-T'],
            ['status.hf', 'Philos II DR-T']
        ];
        $db = Yii::app()->db;
        /** @var $db CDbConnection*/
        $transaction = $db->beginTransaction();
        try {
            $command = $db->createCommand();
            foreach ($rows as $row) {
                $command->insert(
                    '{{disable_tab}}',
                    ['tab_name' => $row[0], 'device_model_name' => $row[1]]
                );
            }
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();
        }
    }
}
