<?php
Yii::import('directory.models.*');

class Icd10Directory implements DirectoryInterface
{
    public function fill()
    {
        $fileName = __DIR__
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR . 'Icd10Dictionary';

        $resource = fopen($fileName, 'r');
        //пропускаем заголовок
        $header = fgets($resource);

        while ($row = fgets($resource)) {

            $vals = explode(';', $row);
            $dictRow = new Icd10();

            $dictRow->uid = $vals[0];
            $dictRow->code = $vals[1];
            $dictRow->name = $vals[2];
            $dictRow->parent_id = $vals[3];
            $dictRow->is_injury = $vals[4];
            $dictRow->record_id = $vals[5];
            $dictRow->order = $vals[6];
            $dictRow->type = $vals[7];
            $dictRow->from = $vals[8];
            $dictRow->to = $vals[9];

            $dictRow->save();
        }

        fclose($resource);
    }
}
