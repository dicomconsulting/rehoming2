<?php
Yii::import('directory.models.*');

class RegionDirectory implements DirectoryInterface
{
    public function fill()
    {
        $fileName = __DIR__
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR . 'RegionDictionary';

        $resource = fopen($fileName, 'r');
        //пропускаем заголовок
        $header = fgets($resource);

        while ($row = fgets($resource)) {

            $vals = explode(';', $row);

            $dictRow = Region::model()->withI18n()->findByPk($vals[0]);

            if (!$dictRow) {
                $dictRow = new Region();
                $dictRow->uid = $vals[0];
            }

            $dictRow->setLocale("ru");
            $dictRow->name = $vals[1];
            $dictRow->full_name = $vals[2];

            $dictRow->save();
        }

        fclose($resource);
    }
}
