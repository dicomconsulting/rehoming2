<?php
Yii::import('directory.models.*');

class MedicalCenterDirectory implements DirectoryInterface
{
    public function fill()
    {
        $fileName = __DIR__
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR . 'MedicalCenterDictionary';

        $resource = fopen($fileName, 'r');
        //пропускаем заголовок
        $header = fgets($resource);

        while ($row = fgets($resource)) {

            $vals = explode(';', $row);

            $dictRow = MedicalCenter::model()->findByPk($vals[0]);

            if (!$dictRow) {
                $dictRow = new MedicalCenter();
                $dictRow->uid = (int) $vals[0];
            }

            /** @var MedicalCenter $dictRow */
            $dictRow->city_id = (int) $vals[1];
            $dictRow->code = $vals[2];
            $dictRow->inn = $vals[3];

            $dictRow->setLocale("ru");
            $dictRow->name = $vals[4];

            $dictRow->save(false);
        }

        fclose($resource);
    }
}
