<?php

class ElectrodePlacementDirectory implements DirectoryInterface
{
    public function fill()
    {
        Yii::app()->getModule("directory");

        $fileName = __DIR__
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR . 'ElectrodePlacementDictionary';

        $resource = fopen($fileName, 'r');
        //пропускаем заголовок
        $header = fgets($resource);

        while ($row = fgets($resource)) {

            $vals = explode(';', $row);
            $dictRow = new ElectrodePlacement();

            $dictRow->uid = $vals[0];

            $dictRow->setLocale("ru");
            $dictRow->name = $vals[1];

            $dictRow->save(false);
        }

        fclose($resource);
    }
}
