<?php

class CountryDirectory implements DirectoryInterface
{
    public function fill()
    {
        Yii::app()->getModule("directory");

        $fileName = __DIR__
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR . 'CountryDictionary';

        $resource = fopen($fileName, 'r');
        //пропускаем заголовок
        $header = fgets($resource);

        while ($row = fgets($resource)) {

            $vals = explode(';', $row);

            $dictRow = Country::model()->findByPk($vals[0]);

            if (!$dictRow) {
                $dictRow = new Country();
                $dictRow->uid = $vals[0];
            }

            /** @var Country $dictRow */
            $dictRow->alpha2_code = $vals[1];
            $dictRow->alpha3_code = $vals[2];
            $dictRow->iso_code = $vals[3];
            $dictRow->region_id = $vals[4];

            $dictRow->setLocale("ru");
            $dictRow->name = $vals[5];
            $dictRow->full_name = $vals[6];

            $dictRow->save();
        }

        fclose($resource);
    }
}
