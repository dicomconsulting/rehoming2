<?php
Yii::import('directory.models.*');

class SurgeryDirectory implements DirectoryInterface
{
    public function fill()
    {
        $fileName = __DIR__
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR . 'SurgeryDictionary';

        $resource = fopen($fileName, 'r');
        //пропускаем заголовок
        $header = fgets($resource);

        while ($row = fgets($resource)) {

            $vals = explode(';', $row);
            $dictRow = new Surgery();

            $dictRow->uid = $vals[0];

            $dictRow->setLocale("ru");
            $dictRow->name = $vals[1];

            $dictRow->save();
        }

        fclose($resource);
    }
}
