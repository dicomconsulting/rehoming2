<?php
Yii::import('directory.models.*');

class PharmClinicGroupDirectory implements DirectoryInterface
{

    public function fill()
    {
        $fileName = __DIR__
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR . 'PharmClinicGroupDictionary';

        $delimiter = "%%";

        $resource = fopen($fileName, 'r');
        //пропускаем заголовок
        $header = fgets($resource);

        while ($row = fgets($resource)) {

            $vals = explode($delimiter, $row);
            $dictRow = new PharmClinicGroup();

            $dictRow->uid = $vals[0];
            $dictRow->code = $vals[1];

            $dictRow->setLocale("ru");
            $dictRow->name = $vals[2];

            $dictRow->save();
        }

        fclose($resource);
    }
}
