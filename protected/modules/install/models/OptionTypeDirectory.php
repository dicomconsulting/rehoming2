<?php


class OptionTypeDirectory implements DirectoryInterface
{
    public function fill()
    {
        Yii::app()->getModule("option");

        $fileName = __DIR__
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR . 'OptionTypeDirectory';

        $resource = fopen($fileName, 'r');
        //пропускаем заголовок
        $header = fgets($resource);

        while ($row = fgets($resource)) {

            $vals = explode(';', $row);

            $dictRow = OptionType::model()->findByPk($vals[0]);

            if (!$dictRow) {
                $dictRow = new OptionType();
                $dictRow->uid = (int) $vals[0];
            }

            /** @var OptionType $dictRow */
            $dictRow->name = trim($vals[1]);

            $dictRow->save(false);
        }

        fclose($resource);
    }
}
