<?php


class OptionDirectory implements DirectoryInterface
{
    public function fill()
    {
        Yii::app()->getModule("option");

        $fileName = __DIR__
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR . 'OptionDirectory.csv';

        $resource = fopen($fileName, 'r');
        //пропускаем заголовок
        $header = fgets($resource);
        $header = $this->clearupArray(explode(';', $header));

        while ($row = fgets($resource)) {

            $vals = array_combine($header, $this->clearupArray(explode(';', $row)));

            $dictRow = Option::model()->findByAttributes(array('source_text' => $vals['source_text']));

            if (!$dictRow) {
                $dictRow = new Option();
            }

            /** @var OptionType $optionType */
            $optionType = OptionType::model()->findByAttributes(array("name" => $vals['type']));
            /** @var Section $section */
            $section = Section::model()->findByAttributes(array("alias" => $vals['section']));

            $dictRow->option_type_uid = $optionType->uid;
            $dictRow->section_uid = $section->uid;
            $dictRow->source_text = $vals['source_text'];

            /** @var Option $dictRow */
            $dictRow->setLocale("ru");
            $dictRow->name = $vals['name_ru'];
            $dictRow->setLocale("en");
            $dictRow->name = $vals['source_text'];
            $dictRow->save(false);

            $className = $vals['type'];
            $className::install($dictRow, $vals);

        }

        fclose($resource);
    }

    public function clearupArray($array)
    {
        return array_map('trim', $array);
    }
}
