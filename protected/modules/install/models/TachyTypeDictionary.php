<?php


class TachyTypeDictionary implements DirectoryInterface
{
    public function fill()
    {
        Yii::app()->getModule("directory");

        $fileName = __DIR__
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR . 'TachyTypeDictionary';

        $resource = fopen($fileName, 'r');
        //пропускаем заголовок
        $header = fgets($resource);

        while ($row = fgets($resource)) {

            $vals = explode(';', $row);

            $dictRow = TachyType::model()->withI18n()->findByPk($vals[0]);

            if (!$dictRow) {
                $dictRow = new TachyType();
                $dictRow->uid = (int) $vals[0];
            }

            /** @var Section $dictRow */
            $dictRow->setLocale("ru");
            $dictRow->name = $vals[1];

            $dictRow->setLocale("en");
            $dictRow->name = trim($vals[2]);

            $dictRow->save(false);
        }

        fclose($resource);
    }
}
