<?php


class TransmitterDirectory implements DirectoryInterface
{
    public function fill()
    {
        Yii::import('device.models.*');

        $fileName = __DIR__
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR . 'TransmitterDictionary';

        $resource = fopen($fileName, 'r');
        //пропускаем заголовок
        $header = fgets($resource);

        while ($row = fgets($resource)) {

            $vals = explode(';', $row);

            $dictRow = Transmitter::model()->findByPk($vals[0]);

            if (!$dictRow) {
                $dictRow = new Transmitter();
                $dictRow->uid = (int) $vals[0];
            }

            /** @var Transmitter $dictRow */
            $dictRow->name = $vals[1];

            $dictRow->save(false);
        }

        fclose($resource);
    }
}
