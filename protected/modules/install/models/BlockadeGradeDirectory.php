<?php


class BlockadeGradeDirectory implements DirectoryInterface
{
    public function fill()
    {
        Yii::import('directory.models.*');

        $fileName = __DIR__
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR . 'BlockadeGradeDictionary';

        $resource = fopen($fileName, 'r');
        //пропускаем заголовок
        $header = fgets($resource);

        while ($row = fgets($resource)) {

            $vals = explode(';', $row);

            $dictRow = BlockadeDegree::model()->findByPk($vals[0]);

            if (!$dictRow) {
                $dictRow = new BlockadeDegree();
                $dictRow->uid = (int) $vals[0];
            }

            /** @var BlockadeDegree $dictRow */
            $dictRow->name = $vals[1];

            $dictRow->save(false);
        }

        fclose($resource);
    }
}
