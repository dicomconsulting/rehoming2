<?php


class NyhaClassDirectory implements DirectoryInterface
{
    public function fill()
    {
        Yii::import('directory.models.*');

        $fileName = __DIR__
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR . 'NyhaClassDictionary';

        $resource = fopen($fileName, 'r');
        //пропускаем заголовок
        $header = fgets($resource);

        while ($row = fgets($resource)) {

            $vals = explode(';', $row);

            $dictRow = NyhaClass::model()->findByPk($vals[0]);

            if (!$dictRow) {
                $dictRow = new NyhaClass();
                $dictRow->uid = (int) $vals[0];
            }

            /** @var NyhaClass $dictRow */
            $dictRow->setLocale("ru");
            $dictRow->name = $vals[1];
            $dictRow->setLocale("en");
            $dictRow->name = $vals[2];

            $dictRow->save(false);
        }

        fclose($resource);
    }
}
