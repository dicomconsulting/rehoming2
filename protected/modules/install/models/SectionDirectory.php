<?php


class SectionDirectory implements DirectoryInterface
{
    public function fill()
    {
        Yii::app()->getModule("directory");

        $fileName = __DIR__
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR . 'SectionDictionary';

        $resource = fopen($fileName, 'r');
        //пропускаем заголовок
        $header = fgets($resource);

        while ($row = fgets($resource)) {

            $vals = explode(';', $row);

            $dictRow = Section::model()->withI18n()->findByPk($vals[0]);

            if (!$dictRow) {
                $dictRow = new Section();
                $dictRow->uid = (int) $vals[0];
            }

            /** @var Section $dictRow */
            $dictRow->alias = $vals[1];
            $dictRow->order = $vals[3];
            $dictRow->setLocale("ru");
            $dictRow->name = $vals[2];

            $dictRow->setLocale("en");
            $dictRow->name = trim($vals[4]);

            $dictRow->save(false);
        }

        fclose($resource);
    }
}
