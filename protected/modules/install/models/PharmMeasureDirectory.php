<?php
Yii::import('directory.models.*');

class PharmMeasureDirectory implements DirectoryInterface
{
    public function fill()
    {
        $fileName = __DIR__
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR . 'PharmMeasure';

        $delimiter = "%%";

        $resource = fopen($fileName, 'r');
        //пропускаем заголовок
        $header = fgets($resource);

        while ($row = fgets($resource)) {

            $vals = explode($delimiter, $row);

            $dictRow = new PharmMeasure();
            $dictRow->measurement_unit_uid = $vals[0];
            $dictRow->save();
        }

        fclose($resource);
    }
}
