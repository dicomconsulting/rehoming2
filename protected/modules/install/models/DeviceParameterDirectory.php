<?php
Yii::import('device.models.*');
Yii::import('directory.models.*');

class DeviceParameterDirectory implements DirectoryInterface
{
    public function fill()
    {
        $filePath = __DIR__
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR;
        $this->_fillMainTable($filePath);
        $this->_fillSynonymTable($filePath);
        $this->_fillDeviceStateTransformers();
    }

    private function _fillMainTable($filePath)
    {
        $resource = fopen($filePath . 'ParameterNameList', 'r');
        $translations = require_once $filePath . 'ParameterNameTranslation.php';
        while ($csvParameterName = fgets($resource)) {
            $csvParameterNameParts = explode(';', $csvParameterName);
            $csvParameterName = preg_replace('/[\r|\n]/', '', $csvParameterNameParts[0]);
            $parameterType = preg_replace('/[\r|\n]/', '', $csvParameterNameParts[1]);
            $parts = explode('[', $csvParameterName);
            $parameterEnName = $parts[0];
            $deviceParameter = new DeviceParameter();
            $deviceParameter->csv_name = $csvParameterName;
            if (isset($parts[1])) {
                $measurementUnitEnName = preg_replace('/\].*/s', '', $parts[1]);
                $measurementUnit = MeasurementUnit::model()->withI18n('en')->find('name = :name', [':name' => $measurementUnitEnName]);
                $deviceParameter->measurement_unit_id = $measurementUnit->uid;
            }
            $deviceParameter->system_name = $this->_generateSystemName($csvParameterName);
            $parameterEnName = trim($parameterEnName);
            $deviceParameter->setLocale('en')->name = $parameterEnName;
            $deviceParameter->setLocale('ru')->name = trim(
                isset($translations[$parameterEnName]) ? $translations[$parameterEnName] : $parameterEnName
            );
            $deviceParameter->type = $parameterType;
            $deviceParameter->save();
        }
        fclose($resource);
    }

    private function _fillSynonymTable($filePath)
    {
        $db = Yii::app()->db;
        /** @var $db CDbConnection*/
        $transaction = $db->beginTransaction();
        try {
            $command = $db->createCommand();
            $resource = fopen($filePath . 'ParameterNameSynonym', 'r');
            while ($csvParameterName = fgets($resource)) {
                list($originCsvName, $synonymCsvName, $deviceModel) = explode(';', trim($csvParameterName));
                $parameter = DeviceParameter::model()->findByAttributes(['csv_name' => $originCsvName]);
                if ($parameter instanceof DeviceParameter) {
                    $command->insert(
                        '{{parameter_synonym}}',
                        [
                            'parameter_id' => $parameter->uid,
                            'csv_synonym' => $synonymCsvName,
                            'device_model_name' => $deviceModel
                        ]
                    );
                }
            }
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();
            throw $e;
        } finally {
            fclose($resource);
        }
    }

    private function _fillDeviceStateTransformers()
    {
        $rows = [
            ['Evia DR-T', 'episode_number', 'NullNotUniqueByDevice', 'normalize'],
            ['Evia DR-T', 'episode_type', 'NormalizeNullByEpisodeNumber', 'normalize'],
            ['Evia SR-T', 'episode_number', 'NullNotUniqueByDevice', 'normalize'],
            ['Evia SR-T', 'episode_type', 'NormalizeNullByEpisodeNumber', 'normalize']
        ];
        $db = Yii::app()->db;
        /** @var $db CDbConnection*/
        $transaction = $db->beginTransaction();
        try {
            $command = $db->createCommand();
            foreach ($rows as $row) {
                $command->insert(
                    '{{device_state_transformer}}',
                    [
                        'device_model' => $row[0],
                        'parameter_system_name' => $row[1],
                        'class_name' => $row[2],
                        'method_name' => $row[3]
                    ]
                );
            }
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();
            throw $e;
        }
    }

    private function _generateSystemName($csvParameterName)
    {
        $stringWithReplace = preg_replace(
            ['/\s+/', '/\[.*\]/s', '/[^a-zA-Z0-9_]/', '/_+/'],
            [' ', '', '_', '_'],
            $csvParameterName
        );
        return trim(strtolower($stringWithReplace), '_ ');
    }
}
