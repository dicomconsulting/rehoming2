<?php
Yii::import('protocols.models.*');

class ProtocolDirectory implements DirectoryInterface
{
    public function fill()
    {
        $fileName = __DIR__
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR . 'ProtocolsDictionary';

        $resource = fopen($fileName, 'r');
        //пропускаем заголовок
        $header = fgets($resource);

        while ($row = fgets($resource)) {

            $vals = explode(';', $row);
            $protocolDictionary = new ProtocolDictionary();

            $protocolDictionary->uid = $vals[0];
            $protocolDictionary->alias = $vals[1];

            $protocolDictionary->setLocale("ru");
            $protocolDictionary->name = $vals[2];

            $protocolDictionary->setLocale("en");
            $protocolDictionary->name = $vals[3];

            $protocolDictionary->save();
        }

        fclose($resource);
    }
}
