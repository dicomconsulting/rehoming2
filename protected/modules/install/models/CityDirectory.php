<?php
Yii::import('directory.models.*');

class CityDirectory implements DirectoryInterface
{
    public function fill()
    {
        $fileName = __DIR__
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR . 'CityDictionary';

        $resource = fopen($fileName, 'r');
        //пропускаем заголовок
        $header = fgets($resource);

        while ($row = fgets($resource)) {

            $vals = explode(';', $row);

            $dictRow = City::model()->findByPk($vals[0]);

            if (!$dictRow) {
                $dictRow = new City();
                $dictRow->uid = $vals[0];
            }

            /** @var City $dictRow */
            $dictRow->country_id = $vals[1];

            $dictRow->setLocale("ru");
            $dictRow->name = $vals[2];

            $dictRow->save();
        }

        fclose($resource);
    }
}
