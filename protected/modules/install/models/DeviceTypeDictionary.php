<?php


class DeviceTypeDictionary implements DirectoryInterface
{
    public function fill()
    {
        Yii::app()->getModule("directory");

        $fileName = __DIR__
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR . 'DeviceTypeDictionary';

        $resource = fopen($fileName, 'r');
        //пропускаем заголовок
        $header = fgets($resource);

        while ($row = fgets($resource)) {

            $vals = explode(';', $row);

            $dictRow = CActiveRecord::model("DeviceType")->findByPk($vals[0]);

            if (!$dictRow) {
                $dictRow = new DeviceType();
                $dictRow->uid = (int) $vals[0];
            }

            $dictRow->alias = trim($vals[5]);

            /** @var DeviceType $dictRow */
            $dictRow->setLocale("ru");
            $dictRow->name = trim($vals[1]);
            $dictRow->name_full = trim($vals[2]);

            $dictRow->setLocale("en");
            $dictRow->name = trim($vals[3]);
            $dictRow->name_full = trim($vals[4]);

            $dictRow->save(false);
        }

        fclose($resource);
    }
}
