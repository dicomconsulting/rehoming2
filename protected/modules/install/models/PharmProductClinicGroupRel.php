<?php
Yii::import('directory.models.*');

class PharmProductClinicGroupRel implements DirectoryInterface
{
    public function fill()
    {
        $fileName = __DIR__
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR . 'PharmProductGroupRel';

        $delimiter = '%%';

        $resource = fopen($fileName, 'r');

        //пропускаем заголовок
        $header = fgets($resource);

        while ($row = fgets($resource)) {

            $vals = explode($delimiter, trim($row));
            $dictRow = new PharmProductClinicGroupRelation();

            $dictRow->pharm_product_uid = $vals[0];
            $dictRow->pharm_group_uid = $vals[1];

            $dictRow->save();
        }

        fclose($resource);
    }
}
