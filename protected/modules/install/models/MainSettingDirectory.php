<?php
Yii::import('admin.models.*');
Yii::import('directory.models.*');

class MainSettingDirectory implements DirectoryInterface
{
    public function fill()
    {
        $filePath = __DIR__ . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR;
        $resource = fopen($filePath . 'MainSettingList', 'r');
        while ($csvParameterName = fgets($resource)) {
            list($name, $value, $serialized, $description) = explode(';', $csvParameterName);

            $newSetting = new Setting();
            $newSetting->name = $name;
            $newSetting->value = $value;
            $newSetting->serialized = $serialized;
            $newSetting->description = preg_replace('/[\r|\n]/', '', $description);
            $newSetting->save();
        }
        fclose($resource);
    }
}
