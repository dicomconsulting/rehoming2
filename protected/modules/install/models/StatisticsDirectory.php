<?php


class StatisticsDirectory implements DirectoryInterface
{
    public function fill()
    {
        Yii::app()->getModule("statistics");

        $fileName = __DIR__
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR . 'StatisticsDirectory';

        $resource = fopen($fileName, 'r');
        //пропускаем заголовок
        $header = fgetcsv($resource);
        $header = $this->clearupArray($header);

        StatisticsRow::model()->deleteAll();

        while ($row = fgetcsv($resource)) {

            $vals = array_combine($header, $this->clearupArray($row));

            $dictRow = StatisticsRow::model()->findByAttributes(
                array(
                    'source' => $vals['source'],
                    'source_field' => $vals['source_field'],
                    'data_processor' => $vals['data_processor'],
                )
            );

            if (!$dictRow) {
                $dictRow = new StatisticsRow();
            }

            /** @var StatisticsSection $section */
            $section = StatisticsSection::model()->findByAttributes(array("alias" => $vals['section']));

            $dictRow->statistics_section_uid = $section->uid;
            $dictRow->source = $vals['source'];
            $dictRow->source_field = $vals['source_field'];
            $dictRow->value_transformer = $vals['value_transformer'];
            $dictRow->value_transformer_param = $vals['value_transformer_param'];
            $dictRow->data_processor = $vals['data_processor'];
            $dictRow->renderer = $vals['renderer'];

            /** @var StatisticsRow $dictRow */
            $dictRow->setLocale("ru");
            $dictRow->name = $vals['name'];
            $dictRow->setLocale("en");
            $dictRow->name = $vals['name_en'];
            $dictRow->save(false);

        }

        fclose($resource);
    }

    public function clearupArray($array)
    {
        return array_map('trim', $array);
    }
}
