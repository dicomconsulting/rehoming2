<?php


class ArrCuppingDictionary implements DirectoryInterface
{
    public function fill()
    {
        Yii::app()->getModule("directory");

        $fileName = __DIR__
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR . 'ArrCuppingDictionary';

        $resource = fopen($fileName, 'r');
        //пропускаем заголовок
        $header = fgets($resource);

        while ($row = fgets($resource)) {

            $vals = explode(';', $row);

            $dictRow = ArrCupping::model()->withI18n()->findByPk($vals[0]);

            if (!$dictRow) {
                $dictRow = new ArrCupping();
                $dictRow->uid = (int) $vals[0];
            }

            /** @var Section $dictRow */
            $dictRow->alias = $vals[1];
            $dictRow->setLocale("ru");
            $dictRow->name = $vals[2];

            $dictRow->setLocale("en");
            $dictRow->name = trim($vals[3]);

            $dictRow->save(false);
        }

        fclose($resource);
    }
}
