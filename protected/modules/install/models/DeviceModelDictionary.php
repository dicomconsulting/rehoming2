<?php

class DeviceModelDictionary implements DirectoryInterface
{
    public function fill()
    {
        Yii::app()->getModule("directory");

        $fileName = __DIR__
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR . 'DeviceModelDictionary';

        $resource = fopen($fileName, 'r');
        //пропускаем заголовок
        $header = fgets($resource);

        while ($row = fgets($resource)) {

            $vals = explode(';', $row);

            $dictRow = CActiveRecord::model("DeviceModel")->findByPk($vals[0]);

            if (!$dictRow) {
                $dictRow = new DeviceModel();
                $dictRow->name = $vals[0];
            }

            $dictRow->device_type_uid = $this->getTypeIdByName(trim($vals[1]));

            $dictRow->save(false);
        }

        fclose($resource);
    }

    protected function getTypeIdByName($name)
    {
        static $types = [];

        if (empty($types)) {
            $types = CHtml::listData(DeviceType::model()->withI18n('ru')->findAll(), 'name', "uid");
        }

        return $types[$name];
    }
}
