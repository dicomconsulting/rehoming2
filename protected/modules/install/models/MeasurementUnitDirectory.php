<?php
Yii::import('directory.models.*');

class MeasurementUnitDirectory implements DirectoryInterface
{
    public function fill()
    {
        $fileName = __DIR__
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR.'MeasurementUnit';
        $resource = fopen($fileName, 'r');
        $header = fgets($resource);
        $localeArray = explode(';', $header);
        while ($row = fgets($resource)) {
            $names = explode(';', $row);
            $measurementUnit = new MeasurementUnit();
            foreach ($localeArray as $key => $localeName) {
                $measurementUnit->setLocale($localeName);
                $measurementUnit->name = preg_replace('/[\r|\n]/', '', $names[$key]);
            }
            $measurementUnit->save();
        }
        fclose($resource);
    }
}
