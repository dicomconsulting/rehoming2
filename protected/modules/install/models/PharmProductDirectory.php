<?php
Yii::import('directory.models.*');

class PharmProductDirectory implements DirectoryInterface
{

    public function fill()
    {
        $fileName = __DIR__
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR . 'PharmProduct';

        $delimiter = "%%";

        $resource = fopen($fileName, 'r');
        //пропускаем заголовок
        $header = fgets($resource);

        while ($row = fgets($resource)) {

            $vals = explode($delimiter, $row);
            $dictRow = new PharmProduct();

            $dictRow->uid = $vals[0];
            $dictRow->name_latin = $vals[1];

            $dictRow->setLocale("ru");
            $dictRow->name = $vals[2];
            $dictRow->zip_info = $vals[3];
            $dictRow->composition = $vals[4];

            $dictRow->save();
        }

        fclose($resource);
    }
}
