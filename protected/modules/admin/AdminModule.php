<?php

class AdminModule extends CWebModule
{
    /**
     * Initializes the module.
     */
    public function init()
    {
        parent::init();
        Yii::setPathOfAlias('admin', dirname(__FILE__));
        $this->setImport(
            array(
                'admin.components.*',
                'admin.models.*',
                'admin.helpers.ViewFragment',
            )
        );
    }
}
