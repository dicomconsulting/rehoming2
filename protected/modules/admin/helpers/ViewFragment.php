<?php

/**
 * @author Ruslan Fadeev
 * created: 15.04.14 10:38
 */
class ViewFragment
{
    private static $moduleId;

    /**
     * @return string
     */
    public static function getModuleId()
    {
        if (isset(self::$moduleId)) {
            return self::$moduleId;
        }
        return self::$moduleId = Yii::app()->getController()->getModule()->getId();
    }

    /**
     * @param string $moduleId
     */
    public static function setModule($moduleId)
    {
        self::$moduleId = $moduleId;
    }

    public static function unsetModule()
    {
        self::$moduleId = null;
    }

    public static function registerScriptAndCssFile($filePath, $scriptPosition = CClientScript::POS_END, $cssMedia = '')
    {
        self::registerScriptFile($filePath, $scriptPosition);
        self::registerCssFile($filePath, $cssMedia);
    }

    /**
     * Регистрирует CSS файл в папке assets/css текущего модуля
     * @param string $filePath
     * @param string $media
     */
    public static function registerCssFile($filePath = 'index', $media = '')
    {
        Yii::app()->getClientScript()->registerCssFile(
            Yii::app()->getAssetManager()->publish(
                Yii::getPathOfAlias(self::getModuleId()) . '/assets/css/' . $filePath . '.css'
            ),
            $media
        );
    }

    /**
     * Регистрирует JS файл в папке assets/js текущего модуля
     * @param string $filePath
     * @param int $position
     */
    public static function registerScriptFile($filePath = 'index', $position = CClientScript::POS_END)
    {
        Yii::app()->getClientScript()->registerScriptFile(
            Yii::app()->getAssetManager()->publish(
                Yii::getPathOfAlias(self::getModuleId()) . '/assets/js/' . $filePath . '.js'
            ),
            $position
        );
    }

    public static function registerStickyTableHeaders($selector = 'table')
    {
        Yii::app()->getClientScript()->registerScriptFile(
            Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('vendor.jmosbech.StickyTableHeaders.js')) . '/jquery.stickytableheaders.min.js',
            CClientScript::POS_HEAD
        );
        self::registerScript('$("' . $selector . '").stickyTableHeaders();');
    }

    public static function stickClearInputButton($elementId)
    {
        $assets = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('vendor.skorecky.Add-Clear') . '/addclear.js');
        Yii::app()->getClientScript()->registerScriptFile($assets . '/addclear.js', CClientScript::POS_HEAD);
        self::registerScript("$('#{$elementId}').addClear();", CClientScript::POS_LOAD);
    }

    /**
     * Передает переменные в JS кодом:
     * {$variable} = new {$className}({$options});
     *
     * Пример JS для работы совместимости с данным методом:
     * function {$className}({$options}) {
     *    var self = this;
     *    self.defaults = {
     *            modalId  : 'upload-modal',
     *            buttonId : 'upload-modal-button'
     *        };
     *    var opts = $.extend({}, self.defaults, {$options});
     * ...........
     *
     * @param string $variable
     * @param string $className
     * @param array $options
     * @param int $position
     */
    public static function registerScriptOptions($variable, $className, array $options, $position = CClientScript::POS_READY)
    {
        $optionsString = CJavaScript::encode($options);
        Yii::app()->getClientScript()->registerScript($variable, "{$variable} = new {$className}({$optionsString});", $position);
    }

    /**
     * Регистрирует JS файл в папке assets/js текущего модуля
     * @param string $script
     * @param int $position
     */
    public static function registerScript($script, $position = CClientScript::POS_END)
    {
        Yii::app()->getClientScript()->registerScript('script' . rand(0, 99999), $script, $position);
    }

    /**
     * Регистрирует CSS в папке assets/css текущего модуля
     * @param string $css
     * @param string $media
     */
    public static function registerCss($css, $media = '')
    {
        Yii::app()->getClientScript()->registerCss(rand(0, 99999), $css, $media);
    }

    public static function registerMaskedInput()
    {
        return Yii::app()->getClientScript()->registerScriptFile(Yii::app()->getRequest()->getBaseUrl() . '/js/jquery.maskedinput.min.js', CClientScript::POS_END);
    }

    /**
     * Кнопка поиск в Grid
     * @param string $gridId
     * @param array $htmlOptions
     * @return string
     */
    public static function searchPanel($gridId = null, $htmlOptions = [])
    {
        if ($gridId) {
            Yii::app()->getClientScript()->registerScript(
                'search',
                "$('.search-button').click(function(){ $('.search-form').toggle();return false;});"
                . "$('.search-form form').submit(function(){ $('#{$gridId}').yiiGridView('update', {data: $(this).serialize()});return false;});"
                . "$('.search-form form').attr('gridId', '{$gridId}');"
            );
        }

        $html = <<<HTML
<div class="panel-heading">
    <h3 class="panel-title">
HTML;
        $html .= \TbHtml::button(
            AdminModule::t('Поиск'),
            array_merge(
                $htmlOptions,
                [
                    'class' => 'search-button',
                    'icon'  => \TbHtml::ICON_SEARCH,
                    'color' => \TbHtml::BUTTON_COLOR_PRIMARY,
                ]
            )
        );
        $html .= <<<HTML
    </h3>
</div>
HTML;
        return $html;
    }

    /**
     * Возвращает блок о напоминиания заполнения обязательных полей
     * @param string $tag HTML
     * @param string $class HTML
     * @return string HTML
     */
    public static function attributesRequired($tag = 'p', $class = 'help-block')
    {
        return CHtml::tag(
            $tag,
            ['class' => $class],
            AdminModule::t(
                'Поля, обозначенные символом {required}, обязательны для заполнения',
                'messages',
                ['{required}' => '<span class="required">*</span>']
            )
        );
    }

    /**
     * Возвращает кнопки Создать/Сохранить в зависимости от параметра $isNewRecord
     * @param bool $isNewRecord
     * @param array|string $additionalAction
     * @param array $options
     * @return string
     */
    public static function formActions($isNewRecord = false, $additionalAction = null, $options = [])
    {
        $submitLabel = \TbArray::popValue('submitLabel', $options, ($isNewRecord ? AdminModule::t('Создать') : AdminModule::t('Сохранить')));
        $actions[] = \TbHtml::submitButton($submitLabel, ['color' => \TbHtml::BUTTON_COLOR_PRIMARY]);

        if ($additionalAction) {
            if (!is_array($additionalAction)) {
                $actions[] = $additionalAction;
            } else {
                $actions = array_merge($actions, $additionalAction);
            }
        }
        return \TbHtml::formActions($actions);
    }

    /**
     * Кнопки Поиск и Сброс для _search.php
     * @param string $tag
     * @param string $class
     * @param string $additionalJs
     * @return string
     */
    public static function searchFormActions($tag = 'div', $class = 'form-actions', $additionalJs = '')
    {
        return CHtml::tag(
            $tag,
            ['class' => $class],
            \TbHtml::submitButton(\AdminModule::t('Искать'), ['color' => \TbHtml::BUTTON_COLOR_PRIMARY])
            . \TbHtml::resetButton(
                AdminModule::t('Сбросить'),
                [
                    'color'   => \TbHtml::BUTTON_COLOR_DEFAULT,
                    'onClick' => $additionalJs . ';$(this).closest("form").clearForm(true);return true;'
                ]
            )
        );
    }

    public static function bsLabelInput($labelHtml, $inputHtml, $labelWidth = 6, $layout = \TbHtml::FORM_LAYOUT_HORIZONTAL)
    {
        $inputWidth = 12 - $labelWidth;
        return "<div class='row form-group clear-both'><div class='col-lg-{$labelWidth}'>{$labelHtml}</div><div class='col-lg-{$inputWidth}'>{$inputHtml}</div></div>";
    }

    public static function ucFirst($str, $enc = 'utf-8')
    {
        return mb_strtoupper(mb_substr($str, 0, 1, $enc), $enc) . mb_substr($str, 1, mb_strlen($str, $enc), $enc);
    }

    /**
     * Выводит кнопку для сохранения данных формы по умолчанию
     * @param $label
     * @param $htmlOptions
     * @return string
     */
    public static function setAsDefaultButton($model, $label, $htmlOptions = [])
    {
        $htmlOptions['id'] = 'setAsDefaultValButton';
        $htmlOptions['data-toggle'] = 'tooltip';
        $htmlOptions['data-placement'] = 'top';
        $htmlOptions['title'] = AdminModule::t($label);
        Yii::app()->clientScript->registerScript(
            'setAsDefaultVal',
            '$("#setAsDefaultValButton").click(function () {
                var data = $(this).closest("form").serialize();
                $.ajax({
                    url: "' . Yii::app()->createUrl('defaultval/saveAsDefault', ['modelName' => get_class($model)]) . '",
                    type: "POST",
                    data: data,
                });
            });',
            CClientScript::POS_END
        );
        return \TbHtml::button('<span class="glyphicon glyphicon-floppy-disk"></span>', $htmlOptions);
    }

    /**
     * Поле Показывать заблокированные записи
     * @param CActiveRecord $model
     * @param TbActiveForm $form
     * @return string mixed
     */
    public static function getBlockSearchField($model, $form)
    {
        $horizontal = $form->layout == \TbHtml::FORM_LAYOUT_HORIZONTAL;
        $name = \CHtml::modelName($model) . '[showBlocked]';
        $data = [
            '0' => AdminModule::t('Нет'),
            '1' => AdminModule::t('Да'),
        ];
        $return = '<div class="form-group">';
        $return .= \TbHtml::label(\AdminModule::t('Показывать заблокированные записи'), $name, ['class' => $horizontal ? 'col-sm-2 control-label' : '']);
        $return .= $horizontal ? '<div class="col-sm-10">' : '<div>';
        $return .= \TbHtml::dropDownList($name, 0, $data);
        $return .= '</div>';
        $return .= '</div>';

        return $return;
    }

    /**
     * Кнопка "добавить"
     * @param array $htmlOptions
     * @return string
     */
    public static function buttonAdd($htmlOptions = [])
    {
        $htmlOptions = array_merge(['class' => 'btn btn-default'], $htmlOptions);
        return \TbHtml::button(\TbHtml::icon('glyphicon-plus'), $htmlOptions);
    }

    /**
     * Кнопка вызова дерева
     * @param array $htmlOptions
     * @return string
     */
    public static function buttonTree($htmlOptions = [])
    {
        $htmlOptions = array_merge(['class' => 'btn btn-default'], $htmlOptions);
        $img = '<img src="/images/flow2.png"/>';
        return \TbHtml::button($img, $htmlOptions);
    }
}
