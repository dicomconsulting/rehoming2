function SavedFilterClass(options) {
    var self = this;
    self.modalId = options.modalId;               // Id элемента модального окна
    self.elementId = options.elementId;           // Id елемента селекта с выбором текущего фильтра
    self.inputPrefix = options.inputPrefix;       // Префикс элементов с данными фильтров
    self.filterValues = options.filterValues;     // Описания существующих фильтров c их именами
    self.saveUrl = options.saveUrl;               // Ссылка для сохранения нового фильтра
    self.deleteUrl = options.deleteUrl;           // Ссылка для сохранения нового фильтра
    self.placement = options.placement;           // Размещение сохраненного фильтра
    self.urlPrefix = options.urlPrefix;           // Префикс текущей ссылки для предачи на сервер в методе getFilterUrl
    self.getFilterUrl = options.getFilterUrl;     // Префикс текущей ссылки
    self.maxUrlLen = 4048;
    self.init();
}

SavedFilterClass.prototype.init = function () {
    var self = this;
};

SavedFilterClass.prototype.getSelectedValue = function () {
    var self = this;
    return $('#' + self.elementId).val();
};

SavedFilterClass.prototype.getCurrentFilterProperties = function () {
    var self = this;
    var selectedValue = this.getSelectedValue();
    if (self.filterValues[selectedValue]) {
        return self.filterValues[selectedValue];
    } else {
        return false;
    }

};

SavedFilterClass.prototype.applyFilterId = function (filterId) {
    var self = this;
    var filterProperties = self.filterValues[filterId];
    if (filterProperties) {
        url = self.fixSavedFilterInUrl(filterProperties.url, filterId);
        window.location.href = url;
    }
};

SavedFilterClass.prototype.addNewFilterToSelect = function (filterId, filterName, filterUrl) {
    var self = this;
    var selectControl = $('#' + self.elementId);
    if (typeof self.filterValues[filterId] == 'undefined') {
        selectControl.append($("<option></option>").attr("value", filterId).text(filterName));
    }
    selectControl.val(filterId);
    filterUrl = self.fixSavedFilterInUrl(filterUrl, filterId);
    history.pushState({}, "", filterUrl);
    self.filterValues[filterId] = {
        name: filterName,
        url: filterUrl
    }
};

SavedFilterClass.prototype.deleteFilterFromSelect = function (filterId) {
    var self = this;
    $("#" + self.elementId + " option[value='" + filterId + "']").remove();
};

SavedFilterClass.prototype.handleClickFilter = function () {
    var self = this;
    var filterId = this.getSelectedValue();
    self.applyFilterId(filterId);
};

/**
 * Сформировать массив текущих данных фильтра
 *
 * @returns {{comment: string}}
 */
SavedFilterClass.prototype.formFiltersValues = function () {
    var self = this;
    var params = {};
    $("[name ^= " + self.inputPrefix + "]").each(function (index, inputParameter) {
        var fullName = $(inputParameter).attr('name');
        //console.log(fullName);
        var shortName;
        // Проверяем на закрывающуюся скобку
        if (fullName.substr(self.inputPrefix.length, 1) == '[') {
            // Многомерный массив
            var brPos = fullName.indexOf(']');
            shortName = fullName.substr(self.inputPrefix.length + 1, brPos - (self.inputPrefix.length + 1)) + fullName.substr(brPos + 1, self.maxUrlLen);

        } else {
            shortName = fullName.substr(self.inputPrefix.length + 1, self.maxUrlLen);
            shortName = shortName.substr(0, shortName.length - 1);
        }
        var val;
        if ($(inputParameter).is(':checkbox')) {
            if ($(inputParameter).is(':checked')) {
                val = $(inputParameter).val();
            } else {
                // Пропускаем параметр
                return;
            }
        } else {
            val = $(inputParameter).val();
        }
        if (typeof params[shortName] == 'undefined') {
            params[shortName] = val;
        } else {
            // Значение уже задано
            if (typeof params[shortName] == 'string') {
                // Приводим значение к массиву
                params[shortName] = [
                    params[shortName]
                ];
            }
            params[shortName].push(val);
        }
    });
    return params;
};

/**
 * Получить значения параметров не входящих по имени в основной префикс
 * в дальнейшем может быть переведен в callBack
 *
 * @returns {{}}
 */
SavedFilterClass.prototype.getAdditionalFilterValues = function () {
    var self = this;
    var params = {};

    $("[name ^= AdditionalFilter").each(function (index, inputParameter) {
        var fullName = $(inputParameter).attr('name');
        //console.log(fullName);
        var shortName = fullName.substr(17, self.maxUrlLen);
        shortName = shortName.substr(0, shortName.length - 1);
        params[shortName] = $(inputParameter).val();
    });
    //console.log(params);
    return params;
};


SavedFilterClass.prototype.ajaxSaveFilters = function (filtersValues, additionalFilterValues) {
    var self = this;
    var formId = $('#' + self.modalId).closest('.modal').attr('id');

    newName = $('#newFilterName').val();
    // Приводим filterValues к виду для POST запроса
    var resultFilterValues = {};
    for (var key in filtersValues) {
        val = filtersValues[key];
        key = key.replace("[", "%5B");
        key = key.replace("]", "%5D");
        resultFilterValues[key] = val;
    }
    $.ajax({
        type: 'POST',
        url: self.saveUrl,
        dataType: 'json',
        data: {
            'filterPlacement': self.placement,
            'filterName': newName,
            'filterParams': resultFilterValues,
            'additionalFilter': additionalFilterValues
        },
        success: function (data) {
            var newFilterId = data.id;
            var filterName = data.name;
            // Запрашиваем с сервера ссылку по которой должны будем перейти при клике на элемент селекта
            $.ajax({
                type: 'POST',
                url: self.getFilterUrl,
                dataType: 'json',
                data: {
                    'filterId': newFilterId,
                    'parameterPrefix': self.inputPrefix,
                    'urlPrefix': self.urlPrefix
                },
                success: function (data) {
                    // Перезагружаем страницу с текущим фильтром
                    var filterUrl = data.result;
                    self.addNewFilterToSelect(newFilterId, filterName, filterUrl);
                }
            });
        },
        error: function () {
            console.log('Ajax save error.');
        }
    });
    hideDialog(formId);
};

/**
 * Показать диалог сохранения нового фильтра
 *
 * @param filterId
 * @param currentName
 * @param filtersValues
 * @param additionalFilterValues
 */
SavedFilterClass.prototype.showSaveDialog = function (filterId, currentName, filtersValues, additionalFilterValues) {
    var self = this;
    var formId = $('#' + self.modalId).closest('.modal').attr('id');

    // Устанавливаем текущее имя
    $('#newFilterName').val(currentName);


    // Подвязываем каллбекы на сохранение
    $('#saveFilterButton').off('click'); // Защита от двойного срабатывания
    $('#saveFilterButton').on('click', function (event) {
        self.ajaxSaveFilters(filtersValues, additionalFilterValues);
    });

    $('#cancelSaveFilterButton').off('click');
    $('#cancelSaveFilterButton').on('click', function (event) {
        hideDialog(formId);
    });
    showDialog(formId);
};

/**
 *  Нажата кнопка сохранения фильтра
 */
SavedFilterClass.prototype.handleSaveFilter = function () {
    var self = this;
    var selectedValue = self.getSelectedValue();
    var filtersValues = self.formFiltersValues();
    var additionalFilterValues = self.getAdditionalFilterValues();

    selectedValue = self.getSelectedValue();
    if (self.filterValues[selectedValue]) {
        filterName = self.filterValues[selectedValue].name;
    } else {
        filterName = ''
    }
    self.showSaveDialog(selectedValue, filterName, filtersValues, additionalFilterValues);
};

/**
 *  Нажата кнопка удаления фильтра
 */
SavedFilterClass.prototype.handleDeleteFilter = function () {
    var self = this;
    filterId = this.getSelectedValue();
    $.ajax({
        type: 'POST',
        url: self.deleteUrl,
        data: {
            'filterId': filterId
        },
        success: function (data) {
            // Убираем элемент из выбора
            self.deleteFilterFromSelect(filterId);
            // window.location.href = self.fixSavedFilterInUrl(window.location.href, 0);
        },
        error: function () {
            console.log('Ajax save error.');
        }
    });
};

/**
 * Фиксируем адрес текущего сохранного фильтра в ссылке
 */
SavedFilterClass.prototype.fixSavedFilterInUrl = function (url, filterId) {
    var self = this;
    if (url.indexOf('savedFilterId=') == -1) {
        p1 = url.indexOf('?');
        if (p1 == -1) {
            // Нет параметров вообще
            url = url + '?savedFilterId=' + filterId;
        } else {
            if (url.substr(url.length - 1, 1) != '&') {
                url = url + '&';
            }
            url = url + 'savedFilterId=' + filterId;
        }
    } else {
        pos = url.indexOf('savedFilterId=');
        pos2 = url.indexOf('&', pos + 1);
        if (pos2 == -1) {
            pos2 = self.maxUrlLen;
        }
        url = url.substr(0, pos) + 'savedFilterId=' + filterId + url.substr(pos2, self.maxUrlLen);
    }
    return url;
}


