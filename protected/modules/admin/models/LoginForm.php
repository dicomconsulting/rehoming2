<?php

class LoginForm extends CFormModel
{
    /**
     * 30 days in seconds
     */
    const REMEMBER_ME_TIME = 2592000;

    public $userGroup;

    public $username;

    public $password;

    private $_identity;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            array('username, password', 'required'),
            array('username, password', 'checkAlreadyAuthenticated'),
            array('userGroup', 'checkUserGroupRequired'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'userGroup' => Yii::t('user', Yii::t('AdminModule.messages', 'Группа')),
            'username'  => Yii::t('user', Yii::t('AdminModule.messages', 'Имя пользователя')),
            'password'  => Yii::t('user', Yii::t('AdminModule.messages', 'Пароль')),
        );
    }

    public function checkUserGroupRequired()
    {
        if ($this->_getUserIdentity()->isUserGroupRequired() && empty($this->userGroup)) {
            $this->addError(
                'userGroup',
                Yii::t('AdminModule.messages', 'Поле группа является обязательным для данного пользователя')
            );
        }
    }

    public function checkAlreadyAuthenticated()
    {
        $lastRequestTime = new RhDateTime();
        $lastRequestTime->setTimestamp(time() - Yii::app()->user->authTimeout);
        /*
         *  Проверка на вход с разных устройст отключена
         *
        if (User::model()->exists(
            'username = :username AND last_request_time > :time',
            [
                ':username' => $this->username,
                ':time'     => $lastRequestTime->format('Y-m-d H:i:s')
            ]
        )
        ) {
            $this->addError(
                'username',
                Yii::t(
                    'AdminModule.messages',
                    'Ваша учетная запись используется на другом компьютере. Попробуйте зайти в другое время.'
                )
            );
        }
        */
    }

    /**
     * Logs in the user using the given username and password in the model.
     * @return boolean whether login is successful
     */
    public function login()
    {
        $this->_getUserIdentity()->authenticate();

        if ($this->_getUserIdentity()->errorCode === UserIdentity::ERROR_NONE) {
            Yii::app()->user->login($this->_getUserIdentity(), self::REMEMBER_ME_TIME);
            return true;
        } elseif ($this->_getUserIdentity()->errorCode === UserIdentity::ERROR_PASSWORD_INVALID) {
            $this->addError('password', Yii::t('user', 'Неправильный пароль'));
        } elseif ($this->_getUserIdentity()->errorCode === UserIdentity::ERROR_USERNAME_INVALID) {
            $this->addError('username', Yii::t('user', 'Пользователь отсутствует в системе'));
        } else {
            $this->addError('password', Yii::t('user', 'Некорректные данные пользователя'));
        }

        return false;
    }

    /**
     * @return UserIdentity
     */
    private function _getUserIdentity()
    {
        if (!$this->_issetUserIdentity()) {
            $this->_identity = new UserIdentity(
                $this->userGroup,
                $this->username,
                $this->password
            );
        }
        return $this->_identity;
    }

    private function _issetUserIdentity()
    {
        return $this->_identity instanceof UserIdentity;
    }
}
