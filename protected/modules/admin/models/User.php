<?php

/**
 * This is the model class for table "{{user}}".
 *
 * The followings are the available columns in table '{{user}}':
 * @property integer $uid
 * @property string $username
 * @property string $password
 * @property string $email
 * @property RhDateTime $create_time
 * @property RhDateTime $update_time
 * @property integer $is_deleted
 * @property integer $doctor_id Primary key of linked doctor.
 * @property string $authentication_type
 * @property RhDateTime $last_request_time
 * @property integer $medical_center_id Primary key of linked medical center.
 * @property integer $user_group_id
 * @property Doctor $doctor Link to doctor entity
 * @property PatientGroupAccess[] $patientGroupRel
 * @property PatientGroup[] $patientGroup
 */
class User extends CActiveRecord
{
    /**
     * Type for users from Biotronik Home Monitoring
     */
    const USER_IDENTITY = 'Local';
    const ROOT_NAME = 'root';

    public $password_repeat;
    public $new_password;
    public $settings;

    public function init()
    {
        $this->initDefaultSettings();
    }

    protected function initDefaultSettings()
    {
        // Настройки по умолчанию
        $this->settings = [
            'depersonalizationUrl' => 'http://127.0.0.1:4412/patientInfo?'
        ];
    }

    public function updateLastRequestTime($reset = false)
    {
        if ($reset) {
            $lastRequestTime = null;
        } else {
            $lastRequestTime = new RhDateTime();
            $lastRequestTime = $lastRequestTime->format('Y-m-d H:i:s');
        }
        $this->updateByPk(
            $this->uid,
            ['last_request_time' => $lastRequestTime]
        );
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{user}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('username', 'required'),
            array('new_password', 'required', 'on' => 'create'),
            array('create_time, update_time, is_deleted, password_repeat, new_password', 'safe'),
            array('username, email', 'length', 'max' => 128),
            array('password, new_password', 'length', 'max' => 255),
            array('new_password', 'compare', 'compareAttribute' => 'password_repeat'),
            array('email, username', 'unique'),
            array('email', 'email'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('uid, username, password, email, create_time, update_time, is_deleted', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'doctor'          => array(self::BELONGS_TO, 'Doctor', 'doctor_id'),
            'patientGroupRel' => array(
                self::HAS_MANY,
                'PatientGroupAccess',
                array("uid" => 'doctor_id'),
                'through' => 'doctor'
            ),
            'patientGroup'    => array(
                self::HAS_MANY,
                'PatientGroup',
                array('patient_group_id' => "uid"),
                'through' => 'patientGroupRel'
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid'             => 'ID',
            'username'        => 'Логин',
            'password'        => 'Пароль',
            'new_password'    => 'Пароль',
            'email'           => 'E-mail',
            'create_time'     => 'Время создания',
            'update_time'     => 'Время обновления',
            'is_deleted'      => 'Удален?',
            'password_repeat' => 'Повторите пароль',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('uid', $this->uid);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('create_time', $this->create_time);
        $criteria->compare('update_time', $this->update_time);
        $criteria->compare('is_deleted', $this->is_deleted);

        return new CActiveDataProvider(
            $this,
            array(
                'criteria'   => $criteria,
                'pagination' => [
                    'pageSize' => Yii::app()->user->getState('pageSize')
                ]
            )
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param  string $className active record class name.
     * @return User  the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        return [
            'RhDateTimeBehavior'  => [
                'class' => 'application.extensions.RhDateTimeBehavior'
            ],
            'EAdvancedArBehavior' => [
                'class' => 'application.extensions.EAdvancedArBehavior'
            ]
        ];
    }

    protected function beforeSave()
    {

        if ($this->isNewRecord) {
            $this->is_deleted          = 0;
            $this->authentication_type = static::USER_IDENTITY;
        }
        $this->settings_serialized = json_encode($this->settings, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        return parent::beforeSave();
    }

    protected function afterFind()
    {
        $dbSettings     = json_decode($this->settings_serialized, true);
        if (is_array($dbSettings)) {
            $this->settings = array_merge($this->settings, $dbSettings);
        }
        parent::afterFind();
    }

    /**
     * apply a hash on the password before we store it in the database
     */
    protected function afterValidate()
    {
        parent::afterValidate();
        if (!$this->hasErrors()) {
            if ($this->new_password) {
                $this->password = $this->hashPassword($this->new_password);
            }
        }
    }

    /**
     * Generates the password hash.
     * @param string $password
     * @return string hash
     */
    public function hashPassword($password)
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    /**
     * Verify password with hash.
     * @param string $password
     * @param string $hash
     * @return boolean
     */
    public function verifyPassword($password, $hash)
    {
        return password_verify($password, $hash);
    }

    public function primaryKey()
    {
        return 'uid';
    }

    /**
     * Возвращает отображаемое имя пользователя
     */
    public function getDisplayName()
    {
        if ($this->doctor_id) {
            $parts = array_filter(
                [$this->doctor->title, $this->doctor->surname, $this->doctor->name],
                "strlen"
            );
            return implode('&nbsp;', $parts);
        } else {
            return $this->username;
        }
    }

    /**
     * @return CAuthItem[]
     */
    public function getRoles()
    {
        /** @var CDbAuthManager $am */
        $am    = Yii::app()->authManager;
        $roles = $am->getRoles($this->uid);
        return $roles;
    }

    public function isEditable()
    {
        return $this->authentication_type == self::USER_IDENTITY
        && $this->username != self::ROOT_NAME || $this->getIsNewRecord();
    }

    /**
     * Возвращает пользователей, которые можно назначить на врача
     *
     * @return CActiveRecord[]
     */
    public static function getAssignable()
    {
        $criteria = new CDbCriteria([
            'condition' => 'doctor_id IS NULL AND authentication_type = :auth_type',
            'params'    => [':auth_type' => User::USER_IDENTITY]
        ]);

        return self::model()->findAllByAttributes(
            [],
            $criteria
        );
    }

    public function updateGroup($groupId)
    {
        $this->user_group_id = $groupId;
        $this->save();
    }

    public function setSettings($settings)
    {
        foreach ($settings as $key=>$value) {
            if (array_key_exists($key, $this->settings)) {
                $this->settings[$key] = $value;
            }
        }
    }
}
