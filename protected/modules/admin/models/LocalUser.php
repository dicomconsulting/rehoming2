<?php
Yii::import('admin.models.User');

class LocalUser extends User
{
    /**
     * Type for local users.
     */
    const USER_IDENTITY = 'Local';
}
