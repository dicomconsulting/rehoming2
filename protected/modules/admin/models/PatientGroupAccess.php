<?php

/**
 * This is the model class for table "{{doctor_patient_group_relation}}".
 *
 * The followings are the available columns in table '{{doctor_patient_group_relation}}':
 *
 * @property integer $doctor_id
 * @property integer $patient_group_id
 * @property integer $access_level
 */
class PatientGroupAccess extends CActiveRecord
{
    /**
     * No access patient group level.
     */
    const ACCESS_NO = 0;

    /**
     * Read access patient group level.
     */
    const ACCESS_READ = 1;

    /**
     * Full access patient group level.
     */
    const ACCESS_FULL = 2;

    /**
     * @var integer Access level property.
     */
    public $access_level = self::ACCESS_NO;

    /**
     * The associated database table name.
     *
     * @return string
     */
    public function tableName()
    {
        return '{{doctor_patient_group_relation}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param  string  $className active record class name.
     * @return Patient the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        return array(
            'EAdvancedArBehavior' => array(
                'class' => 'application.extensions.EAdvancedArBehavior'
            )
        );
    }

    /**
     * @property integer $groupId
     * @return PatientGroupAccess[]
     */
    public function getAllIndexByDoctor($groupId)
    {
        $items = $this->findAllByAttributes(['patient_group_id' => $groupId]);
        $returnArray = [];
        while ($item = array_pop($items)) {
            $returnArray[$item->doctor_id] = $item;
        }
        return $returnArray;
    }

    /**
     * @property integer $doctorId
     * @return PatientGroupAccess[]
     */
    public function getAllIndexByPatientGroup($doctorId)
    {
        $items = $this->findAllByAttributes(['doctor_id' => $doctorId]);
        $returnArray = [];
        while ($item = array_pop($items)) {
            $returnArray[$item->patient_group_id] = $item;
        }
        return $returnArray;
    }

    public static function getAccessLevelList()
    {
        return [
            self::ACCESS_FULL => 'Full access',
            self::ACCESS_READ => 'Read access',
            self::ACCESS_NO => 'No access'
        ];
    }
}
