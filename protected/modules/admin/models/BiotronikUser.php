<?php
Yii::import('admin.models.User');
Yii::import('admin.models.Doctor');

class BiotronikUser extends User
{
    /**
     * Type for users from Biotronik Home Monitoring
     */
    const USER_IDENTITY = 'Biotronik';

    protected function afterSave()
    {
        if ($this->getIsNewRecord()) {
            //прописываем роли по-умолчанию
            /** @var CAuthManager $authManager */
            $authManager = Yii::app()->getComponent('authManager');

            if ($this->doctor->external_role == Doctor::ROLE_ADMIN) {
                $authManager->assign(Doctor::LOCAL_ROLE_ADMIN, $this->uid);
            } else {
                $authManager->assign(Doctor::LOCAL_ROLE_PHYSICIAN, $this->uid);
            }

            $authManager->save();
        }
    }
}
