<?php
Yii::import('admin.models.User');
Yii::import('admin.models.BiotronikUser');

/**
 * This is the model class for table "{{doctor}}".
 *
 * The followings are the available columns in table '{{doctor}}':
 * @property integer $uid
 * @property string $title
 * @property string $name
 * @property string $surname
 * @property string $external_role
 */
class Doctor extends CActiveRecord
{

    const ROLE_ADMIN = "User administrator";
    const LOCAL_ROLE_ADMIN = "biotronikAdmin";
    const ROLE_PHYSICIAN = "Physician";
    const LOCAL_ROLE_PHYSICIAN = "biotronikUser";

    public $linkedUser;

    public function relations()
    {
        return [
            'linkedUser' => [
                self::HAS_ONE,
                'User',
                'doctor_id'
            ],
            'patientGroupRel' => array(
                self::HAS_MANY,
                'PatientGroupAccess',
                array("doctor_id" => 'uid'),
            ),
            'patientGroup' => array(
                self::HAS_MANY,
                'PatientGroup',
                array('patient_group_id' => "uid"),
                'through'=>'patientGroupRel'
            ),        ];
    }


    public function attributeLabels()
    {
        return [
            "title" => "Обращение",
            "name" => "Имя",
            "surname" => "Фамилия",
            "linkedUser" => "Связанный пользователь",
        ];
    }


    public function isEditable()
    {
        $user = $this->getLinkedUser();
        return empty($user)
            || $user->authentication_type != BiotronikUser::USER_IDENTITY;
    }

    /**
     * @return User | null
     */
    public function getLinkedUser()
    {
        if ($this->linkedUser === null) {
            $user = User::model()->findByAttributes(['doctor_id' => $this->uid]);
            if ($user instanceof User) {
                $this->linkedUser = $user;
            } else {
                $this->linkedUser = false;
            }
        } elseif (is_numeric($this->linkedUser)) {
            $this->linkedUser = User::model()->findByPk($this->linkedUser);
        }

        if ($this->linkedUser === false) {
            return null;
        }
        return $this->linkedUser;
    }

    public function rules()
    {
        return [
            ['name', 'checkAllowEditable'],
            ['name, surname, linkedUser', 'required'],
            ['title', 'safe']
        ];
    }

    public function checkAllowEditable()
    {
        if (!$this->isEditable()) {
            $this->addError(
                'summary',
                Yii::t(
                    'AdminModule.messages',
                    'Изменение данного объекта запрещено'
                )
            );
        }
    }

    /**
     * The associated database table name.
     *
     * @return string
     */
    public function tableName()
    {
        return '{{doctor}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param  string  $className active record class name.
     * @return Patient the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        return array(
            'EAdvancedArBehavior' => array(
                'class' => 'application.extensions.EAdvancedArBehavior'
            )
        );
    }

    public function search()
    {
        return new CActiveDataProvider(
            $this,
            array(
                'criteria' => new CDbCriteria,
                'pagination'=> [
                    'pageSize' => Yii::app()->user->getState('pageSize')
                ]
            )
        );
    }

    public function formatName()
    {
        return ($this->title ? $this->title . " " : "") . $this->name . " " . $this->surname;
    }

    protected function beforeSave()
    {
        if (!$this->getIsNewRecord() && $this->linkedUser) {
            //надо очистить связь с пользователе, иначе при перепривязке случится ошибка неуникальности
            User::model()->updateAll(['doctor_id' => null], "doctor_id = :doctor_id", [':doctor_id' => $this->uid]);
        }
        return parent::beforeSave(); // TODO: Change the autogenerated stub
    }
}
