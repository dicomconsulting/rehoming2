<?php

/**
 * This is the model class for table "{{savedFilter}}".
 *
 * The followings are the available columns in table '{{savedFilter}}':
 * @property integer $id
 * @property string $name
 * @property integer $user_id
 * @property string $placement
 * @property string $condition
 *
 * The followings are the available model relations:
 * @property User $user
 */
class SavedFilter extends CActiveRecord
{
    const PLACEMENT_PATIENT = 'patient';
    const PLACEMENT_STATISTICS = 'statistics';

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{saved_filter}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, placement, condition', 'required'),
            array('user_id', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 255),
            array('placement', 'length', 'max' => 128),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, user_id, placement, condition', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'        => 'ID',
            'name'      => 'Name',
            'user_id'   => 'User',
            'placement' => 'Placement',
            'condition' => 'Condition',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('placement', $this->placement, true);
        $criteria->compare('condition', $this->condition, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SavedFilter the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    /**
     * Сформировать ссылку для сохраненного фильтра
     *
     * @param $parameters
     * @param $urlPrefix
     * @param $parameterPrefix
     * @return string
     */
    public static function formUrlForSavedFilter($parameters, $parameterPrefix, $urlPrefix)
    {
        $resultUrl = $urlPrefix . '?';
        if ($parameters) {
            if (array_key_exists('mainFilter', $parameters) && $parameters['mainFilter']) {
                foreach ($parameters['mainFilter'] as $parameterName => $parameterValue) {
                    if (is_array($parameterValue)) {
                        foreach ($parameterValue as $oneValue) {
                            if (substr($parameterName, -6, 6) == '%5B%5D') {
                                $parameterName = substr($parameterName, 0, -6);
                            }
                            $resultUrl .= $parameterPrefix . '%5B' . $parameterName . '%5D%5B%5D=' . $oneValue . '&';
                        }
                    } else {
                        if (substr($parameterName, -6, 6) == '%5B%5D') {
                            $parameterName = substr($parameterName, 0, -6);
                            $resultUrl .= $parameterPrefix . '%5B' . $parameterName . '%5D%5B%5D=' . $parameterValue . '&';
                        } else {
                            $resultUrl .= $parameterPrefix . '%5B' . $parameterName . '%5D=' . $parameterValue . '&';
                        }
                    }
                }
            }
            if (array_key_exists('additional', $parameters) && $parameters['additional']) {
                foreach ($parameters['additional'] as $parameterName => $parameterValue) {
                    $resultUrl .= 'AdditionalFilter' . '%5B' . $parameterName . '%5D=' . $parameterValue . '&';
                }
            }
        }
        return $resultUrl;
    }

    /**
     * Получить данные по фильтрам в виде пригодном для передачи в js объект
     * TODO: Перенести метод полностью на фронт
     *
     * @param $placement
     * @param $urlPrefix
     * @param $parameterPrefix
     * @return array
     */
    public static function getFiltersUrls($placement, $parameterPrefix, $urlPrefix)
    {
        $userId       = Yii::app()->getUser()->id;
        $savedFilters = SavedFilter::model()->findAllByAttributes(['placement' => $placement, 'user_id' => $userId]);
        $returnValue  = [];
        foreach ($savedFilters as $oneFilter) {
            $condition                   = json_decode($oneFilter->condition, true);
            $returnValue[$oneFilter->id] = [
                'name' => $oneFilter->name,
                'url'  => self::formUrlForSavedFilter($condition, $parameterPrefix, $urlPrefix)
            ];
        }
        return $returnValue;
    }

    /**
     * Получить список фильтр для определенного месторасположения
     *
     * @param $placement
     * @return array
     */
    public static function getFiltersList($placement)
    {
        $userId       = Yii::app()->getUser()->id;
        $savedFilters = SavedFilter::model()->findAllByAttributes(['placement' => $placement, 'user_id' => $userId]);
        $savedFilters = ['' => ''] + $savedFilters;
        return CHtml::listData($savedFilters, 'id', 'name');
    }
}
