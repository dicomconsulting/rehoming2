<?php

/**
 * This is the model class for table "{{user_group}}".
 *
 * The followings are the available columns in table '{{user_group}}':
 * @property integer $uid
 * @property string $name
 * @property string $admin_name
 * @property string $admin_pass
 * @property string $mail_server
 * @property string $mail_user
 * @property string $mail_pass
 */
class UserGroup extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{user_group}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, admin_name, admin_pass, mail_server, mail_user, mail_pass', 'required'),
            array('name, admin_name, admin_pass, mail_server, mail_user, mail_pass', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('uid, name, admin_name, admin_pass, mail_server, mail_user, mail_pass', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid'          => 'ID',
            'name'        => 'Название',
            'admin_name'  => 'Название административного аккаунта',
            'admin_pass'  => 'Пароль административного аккаунта',
            'mail_server' => 'Адрес почтового сервера',
            'mail_user'   => 'Имя пользователя электронной почты',
            'mail_pass'   => 'Пароль пользователя электронной почты',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('uid', $this->uid);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('admin_name', $this->admin_name, true);
        $criteria->compare('admin_pass', $this->admin_pass, true);
        $criteria->compare('mail_server', $this->mail_server, true);
        $criteria->compare('mail_user', $this->mail_user, true);
        $criteria->compare('mail_pass', $this->mail_pass, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return static the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
