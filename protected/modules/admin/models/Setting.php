<?php

/**
 * This is the model class for table "{{setting}}".
 *
 * The followings are the available columns in table '{{setting}}':
 * @property integer $uid
 * @property string $name
 * @property string $value
 * @property string $description
 * @property integer $serialized
 */
class Setting extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{setting}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('value', 'required'),
            array('value', 'length', 'max'=>255),
            array('uid, name, value, serialized', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid' => Yii::t("AdminModule.messages", 'Номер'),
            'name' => Yii::t("AdminModule.messages", 'Название'),
            'value' => Yii::t("AdminModule.messages", 'Значение'),
            'description' => Yii::t("AdminModule.messages", 'Описание')
        );
    }

    public function search()
    {
        return new CActiveDataProvider(
            $this,
            array(
                'criteria' => new CDbCriteria,
                'pagination'=> [
                    'pageSize' => Yii::app()->user->getState('pageSize')
                ]
            )
        );
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
