<?php

class m131220_075406_add_field_to_user_table extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{user}}', 'last_request_time', 'DATETIME');
    }

    public function safeDown()
    {
        $this->dropColumn('{{user}}', 'last_request_time');
    }
}
