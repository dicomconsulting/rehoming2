<?php

class m131012_174054_create_doctor_patient_group_relation extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{doctor_patient_group_relation}}',
            array(
                'doctor_id' => 'INTEGER(11)',
                'patient_group_id' => 'INTEGER(11)',
                'access_level' => 'TINYINT(1) NOT NULL',
                'PRIMARY KEY ( `doctor_id` , `patient_group_id` )'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );
        
        $this->addForeignKey(
            'fk_doctor_patient_group_relation_doctor',
            '{{doctor_patient_group_relation}}',
            'doctor_id',
            '{{doctor}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_fk_doctor_patient_group_relation_patient_group',
            '{{doctor_patient_group_relation}}',
            'patient_group_id',
            '{{patient_group}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_doctor_patient_group_relation_doctor',
            '{{doctor_patient_group_relation}}'
        );
        $this->dropForeignKey(
            'fk_fk_doctor_patient_group_relation_patient_group',
            '{{doctor_patient_group_relation}}'
        );
        
        $this->dropTable('{{doctor_patient_group_relation}}');
    }
}
