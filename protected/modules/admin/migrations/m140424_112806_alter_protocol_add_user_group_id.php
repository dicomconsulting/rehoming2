<?php

class m140424_112806_alter_protocol_add_user_group_id extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn(
            '{{protocol}}',
            'user_group_id',
            'INT(11) DEFAULT NULL COMMENT "Связь с группой пользователей"'
        );
        $this->addForeignKey(
            'fk_protocol_user_group_id',
            '{{protocol}}',
            'user_group_id',
            '{{user_group}}',
            'uid',
            'SET NULL',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_protocol_user_group_id',
            '{{protocol}}'
        );
        $this->dropColumn(
            '{{protocol}}',
            'user_group_id'
        );
    }
}
