<?php

class m140428_105214_update_patients_groups extends I18nDbMigration
{
    public function safeUp()
    {
        $this->execute(
            <<<SQL
UPDATE {{patient}}, {{patient_group}} SET {{patient}}.user_group_id = {{patient_group}}.user_group_id
WHERE {{patient}}.group_id = {{patient_group}}.uid
SQL
        );

        $this->execute(
            <<<SQL
UPDATE {{research}}, {{patient}} SET {{research}}.user_group_id = {{patient}}.user_group_id
WHERE {{patient}}.uid = {{research}}.patient_uid
SQL
        );
        $this->execute(
            <<<SQL
UPDATE {{research}}, {{protocol}} SET {{protocol}}.user_group_id = {{research}}.user_group_id
WHERE {{research}}.uid = {{protocol}}.research_uid
SQL
        );
    }

    public function safeDown()
    {
        return true;
    }
}
