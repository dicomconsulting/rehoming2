<?php

class m140424_113423_alter_reaserch_drop_unique_screening_number_index extends I18nDbMigration
{
    public function safeUp()
    {
        $this->dropIndex('unique_screening_number', '{{research}}');
    }

    public function safeDown()
    {
        $this->createIndex('unique_screening_number', '{{research}}', 'screening_number', true);
    }
}
