<?php

class m131002_085324_add_user_to_medical_center_relation extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{user}}', 'medical_center_id', 'INTEGER(11)');
        $this->addForeignKey(
            'fk_user_medical_center_id_medical_center_uid',
            '{{user}}',
            'medical_center_id',
            '{{medical_center}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_user_medical_center_id_medical_center_uid',
            '{{user}}'
        );
        $this->dropColumn('{{user}}', 'medical_center_id');
    }
}
