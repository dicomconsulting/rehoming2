<?php

class m160120_150801_create_saved_filters_table extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{saved_filter}}',
            [
                'id'        => 'pk',
                'name'      => 'VARCHAR(255) NOT NULL',
                'user_id'   => 'INTEGER',
                'placement' => 'VARCHAR(128) NOT NULL',
                'condition' => 'TEXT NOT NULL',
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );
        $this->addForeignKey(
            'fk_saved_filter_user_id',
            '{{saved_filter}}',
            'user_id',
            '{{user}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
        return true;
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_saved_filter_user_id', '{{saved_filter}}');
        $this->dropTable('{{saved_filter}}');
        return true;
    }
}
