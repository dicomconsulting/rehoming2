<?php

class m131012_162857_update_user_table extends I18nDbMigration
{
    public function safeUp()
    {
        $this->dropForeignKey(
            "fk_auth_assignment_userid",
            "{{auth_assignment}}"
        );
        $this->dropForeignKey(
            'fk_user_id_protocol',
            '{{protocol}}'
        );
        
        $this->alterColumn('{{user}}', 'id', 'INTEGER(11)');
        $this->dropPrimaryKey('id', '{{user}}');
        $this->renameColumn('{{user}}', 'id', 'uid');
        $this->alterColumn('{{user}}', 'uid', 'pk');
        
        $this->alterColumn('{{user}}', 'password', 'VARCHAR(255)');
        $this->alterColumn('{{user}}', 'email', 'VARCHAR(128)');
        $this->alterColumn('{{user}}', 'create_time', 'DATETIME');
        $this->alterColumn('{{user}}', 'update_time', 'DATETIME');
        
        $this->addColumn('{{user}}', 'doctor_id', 'INTEGER(11)');
        $this->addColumn('{{user}}', 'authentication_type', 'VARCHAR(128)');
        
        $this->createIndex('doctor_id', '{{user}}', 'doctor_id', true);
        
        $this->addForeignKey(
            "fk_auth_assignment_userid_relation_user_uid",
            "{{auth_assignment}}",
            "userid",
            "{{user}}",
            "uid",
            "CASCADE",
            "CASCADE"
        );
        $this->addForeignKey(
            'fk_user_id_protocol',
            '{{protocol}}',
            'user_id',
            '{{user}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
        
        $this->dropForeignKey(
            'fk_user_medical_center_id_medical_center_uid',
            '{{user}}'
        );
        $this->addForeignKey(
            'fk_user_medical_center_id_relation_medical_center_uid',
            '{{user}}',
            'medical_center_id',
            '{{medical_center}}',
            'uid',
            'RESTRICT',
            'CASCADE'
        );
        
        $this->addForeignKey(
            'fk_user_doctor_id_relation_doctor_uid',
            '{{user}}',
            'doctor_id',
            '{{doctor}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            "fk_auth_assignment_userid_relation_user_uid",
            "{{auth_assignment}}"
        );
        
        $this->dropForeignKey(
            'fk_user_medical_center_id_relation_medical_center_uid',
            '{{user}}'
        );
        $this->dropForeignKey(
            'fk_user_doctor_id_relation_doctor_uid',
            '{{user}}'
        );
        $this->dropForeignKey(
            'fk_user_id_protocol',
            '{{protocol}}'
        );
        
        $this->alterColumn('{{user}}', 'uid', 'INTEGER(11)');
        $this->dropPrimaryKey('uid', '{{user}}');
        $this->renameColumn('{{user}}', 'uid', 'id');
        $this->alterColumn('{{user}}', 'id', 'pk');
        
        $this->alterColumn('{{user}}', 'password', 'VARCHAR(255) NOT NULL');
        $this->alterColumn('{{user}}', 'email', 'VARCHAR(128) NOT NULL');
        $this->alterColumn('{{user}}', 'create_time', 'INTEGER(11)');
        $this->alterColumn('{{user}}', 'update_time', 'INTEGER(11)');

        $this->dropIndex('doctor_id', '{{user}}');
        
        $this->dropColumn('{{user}}', 'doctor_id');
        $this->dropColumn('{{user}}', 'authentication_type');
        
        

        $this->addForeignKey(
            "fk_auth_assignment_userid",
            "{{auth_assignment}}",
            "userid",
            "{{user}}",
            "id",
            "CASCADE",
            "CASCADE"
        );
        $this->addForeignKey(
            'fk_user_id_protocol',
            '{{protocol}}',
            'user_id',
            '{{user}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        
        $this->addForeignKey(
            'fk_user_medical_center_id_medical_center_uid',
            '{{user}}',
            'medical_center_id',
            '{{medical_center}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }
}
