<?php

class m140423_092117_create_user_group extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{user_group}}',
            array(
                'uid' => 'pk',
                'name' => 'VARCHAR(255) NOT NULL COMMENT "Название группы пользователей"',
                'admin_name' => 'VARCHAR(255) NOT NULL COMMENT "Название административного аккаунта"',
                'admin_pass' => 'VARCHAR(255) NOT NULL COMMENT "Пароль административного аккаунта"',
                'mail_server' => 'VARCHAR(255) NOT NULL COMMENT "Адрес почтового сервера"',
                'mail_user' => 'VARCHAR(255) NOT NULL COMMENT "Имя пользователя электронной почты"',
                'mail_pass' => 'VARCHAR(255) NOT NULL COMMENT "Пароль пользователя электронной почты"',
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT="Таблица настроек групп пользователей"'
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{user_group}}');
    }
}
