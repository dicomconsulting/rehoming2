<?php

class m130930_131938_create_user_table extends EDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{user}}',
            array(
                'id' => 'pk',
                'username' => 'VARCHAR(128) NOT NULL',
                'password' => 'VARCHAR(255) NOT NULL',
                'email' => 'VARCHAR(128) NOT NULL',
                'create_time' => 'INTEGER',
                'update_time' => 'INTEGER',
                'is_deleted' => 'TINYINT(1) DEFAULT 0'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{user}}');
    }
}
