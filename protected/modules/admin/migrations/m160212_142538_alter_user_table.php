<?php

class m160212_142538_alter_user_table extends EDbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{user}}', 'settings_serialized', 'TEXT NOT NULL');
    }

    public function safeDown()
    {
        $this->dropColumn('{{user}}', 'settings_serialized');
    }
}
