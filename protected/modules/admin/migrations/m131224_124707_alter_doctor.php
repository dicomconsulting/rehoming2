<?php

class m131224_124707_alter_doctor extends I18nDbMigration
{
    public function safeUp()
    {
        $this->alterColumn("{{doctor}}", "external_role", "varchar(100) DEFAULT NULL");
    }

    public function safeDown()
    {
        $this->alterColumn("{{doctor}}", "external_role", "varchar(100) NOT NULL");
    }
}
