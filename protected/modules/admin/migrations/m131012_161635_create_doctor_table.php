<?php

class m131012_161635_create_doctor_table extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{doctor}}',
            array(
                'uid' => 'pk',
                'title' => 'VARCHAR(100)',
                'name' => 'VARCHAR(100) NOT NULL',
                'surname' => 'VARCHAR(100) NOT NULL',
                'external_role' => 'VARCHAR(100) NOT NULL',
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{doctor}}');
    }
}
