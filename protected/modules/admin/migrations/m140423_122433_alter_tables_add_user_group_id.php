<?php

class m140423_122433_alter_tables_add_user_group_id extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn(
            '{{user}}',
            'user_group_id',
            'INT(11) DEFAULT NULL COMMENT "Связь с группой пользователей"'
        );
        $this->addForeignKey(
            'fk_user_user_group_id',
            '{{user}}',
            'user_group_id',
            '{{user_group}}',
            'uid',
            'SET NULL',
            'CASCADE'
        );

        $this->addColumn(
            '{{patient}}',
            'user_group_id',
            'INT(11) DEFAULT NULL COMMENT "Связь с группой пользователей"'
        );
        $this->addForeignKey(
            'fk_patient_user_group_id',
            '{{patient}}',
            'user_group_id',
            '{{user_group}}',
            'uid',
            'SET NULL',
            'CASCADE'
        );

        $this->addColumn(
            '{{research}}',
            'user_group_id',
            'INT(11) DEFAULT NULL COMMENT "Связь с группой пользователей"'
        );
        $this->addForeignKey(
            'fk_research_user_group_id',
            '{{research}}',
            'user_group_id',
            '{{user_group}}',
            'uid',
            'SET NULL',
            'CASCADE'
        );

        $this->addColumn(
            '{{patient_group}}',
            'user_group_id',
            'INT(11) DEFAULT NULL COMMENT "Связь с группой пользователей"'
        );
        $this->addForeignKey(
            'fk_patient_group_user_group_id',
            '{{patient_group}}',
            'user_group_id',
            '{{user_group}}',
            'uid',
            'SET NULL',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_user_user_group_id',
            '{{user}}'
        );
        $this->dropColumn(
            '{{user}}',
            'user_group_id'
        );

        $this->dropForeignKey(
            'fk_patient_user_group_id',
            '{{patient}}'
        );
        $this->dropColumn(
            '{{patient}}',
            'user_group_id'
        );

        $this->dropForeignKey(
            'fk_research_user_group_id',
            '{{research}}'
        );
        $this->dropColumn(
            '{{research}}',
            'user_group_id'
        );

        $this->dropForeignKey(
            'fk_patient_group_user_group_id',
            '{{patient_group}}'
        );
        $this->dropColumn(
            '{{patient_group}}',
            'user_group_id'
        );
    }
}
