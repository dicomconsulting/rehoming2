<?php

class m131219_121053_create_setting_table extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{setting}}',
            array(
                'uid' => 'pk',
                'name' => 'VARCHAR(100) NOT NULL',
                'value' => 'VARCHAR(255)',
                'description' => 'VARCHAR(255)',
                'serialized' => 'TINYINT(1) DEFAULT 0'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{setting}}');
    }
}
