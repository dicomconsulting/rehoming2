<?php

/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 23.04.14
 * Time: 16:54
 */
class UserGroupSeparationBehavior extends CActiveRecordBehavior
{
    public static $enabled = true;

    public static function disableDataSeparation()
    {
        $prev = self::$enabled;
        self::$enabled = false;
        return $prev;
    }

    public static function setState($state)
    {
        self::$enabled = $state;
    }

    public function beforeFind($event)
    {
        $this->modifyCriteria($event);
    }

    public function beforeCount($event)
    {
        $this->modifyCriteria($event);
    }

    protected function modifyCriteria($event)
    {
        if (self::$enabled) {
            $currentGroupId = Yii::app()->userGroupManager->getCurrentGroupId();
            if (!$currentGroupId) {
                $currentGroupId = 0;
            }
            /** @var CActiveRecord $ar */
            $ar = $this->getOwner();
            $alias = $ar->getTableAlias(false, false);

            $criteria = $ar->getDbCriteria();

            $criteria->addCondition($alias . '.user_group_id = :user_group_id');
            $criteria->params[':user_group_id'] = $currentGroupId;
        }
    }

    public function beforeSave($event)
    {
        /** @var CActiveRecord $ar */
        $ar = $this->getOwner();
        $ar->user_group_id = Yii::app()->userGroupManager->getCurrentGroupId();
    }
}
