<?php
Yii::import('admin.components.UserIdentity');
Yii::import('admin.components.UserIdentityStrategy.AbstractUserIdentityStrategy');
Yii::import('admin.models.User');
Yii::import('dataSource.models.Tasks.*');
Yii::import('dataSource.components.BiotronikSpiderException');

class BiotronikUserIdentityStrategy extends AbstractUserIdentityStrategy
{
    public function authenticate()
    {
        $spider = Yii::app()->biotronikSpider;
        $spider->backupAuthenticationCredentials();
        try {
            $spider->setUserGroup($this->getOwner()->userGroup);
            $spider->setUserName($this->getOwner()->username);
            $spider->setUserPassword($this->getOwner()->password);
            $result = $spider->authenticate();
            $spider->restoreAuthenticationCredentials();
            if ($result && !$this->getUser() instanceof User) {
                /*
                 * @todo Перенести создание сущностей в фоноые задачи
                 * Сделать когда будут реализованы очередь и менеджер задач.
                 */
                $updateDoctorTask = new UpdateDoctorTask();
                $updateDoctorTask->run(['doctorLogin' => $this->getOwner()->username]);
                $updateDoctorAccessTask = new UpdateDoctorAccessTask($updateDoctorTask);
                $updateDoctorAccessTask->run(['doctorLogin' => $this->getOwner()->username]);

                /** @var User $user */
                $user = User::model()->findByAttributes(['username' => $this->getOwner()->username]);
                $this->setUser($user);
            }
            if ($result) {
                $this->setUserIdentityData();
                $this->getUser()->updateGroup(
                    Yii::app()->userGroupManager->getGroupIdByName($this->getOwner()->userGroup)
                );
                //устанавливаем текущую группу
                Yii::app()->userGroupManager->setCurrentGroupId($this->getUser()->user_group_id);

            }
            return $result;
        } catch (BiotronikSpiderException $e) {
            switch ($e->getCode()) {
                case 204:
                    $this->getOwner()->errorCode = UserIdentity::ERROR_PASSWORD_INVALID;
                    break;
                case 205:
                    $this->getOwner()->errorCode = UserIdentity::ERROR_USERNAME_INVALID;
                    break;
                default:
                    $this->getOwner()->errorCode = UserIdentity::ERROR_UNKNOWN_IDENTITY;
                    break;
            }
        }
    }

    public function isUserGroupRequired()
    {
        return true;
    }
}
