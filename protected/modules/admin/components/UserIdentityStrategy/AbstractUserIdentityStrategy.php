<?php
Yii::import('admin.components.UserIdentity');
Yii::import('admin.models.User');

abstract class AbstractUserIdentityStrategy
{
    private $owner;

    /** @var User */
    private $user;

    public function __construct(UserIdentity $owner)
    {
        $this->owner = $owner;
    }

    /**
     *
     * @return UserIdentity
     */
    public function getOwner()
    {
        return $this->owner;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUserIdentityData()
    {
        $this->getOwner()->setId($this->getUser()->uid);
        $this->getOwner()->setState('username', $this->getUser()->username);
        $this->getOwner()->setState('email', $this->getUser()->email);
        $this->getOwner()->errorCode = UserIdentity::ERROR_NONE;
    }

    public function isUserGroupRequired()
    {
        return false;
    }

    abstract public function authenticate();
}
