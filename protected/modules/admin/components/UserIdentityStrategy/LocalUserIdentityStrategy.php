<?php
Yii::import('admin.components.UserIdentity');
Yii::import('admin.components.UserIdentityStrategy.AbstractUserIdentityStrategy');
Yii::import('admin.models.LocalUser');

class LocalUserIdentityStrategy extends AbstractUserIdentityStrategy
{
    public function authenticate()
    {
        if ($this->getUser() === null) {
            $this->getOwner()->errorCode = UserIdentity::ERROR_USERNAME_INVALID;
        } elseif (!LocalUser::model()->verifyPassword(
            $this->getOwner()->password,
            $this->getUser()->password
        )) {
            $this->getOwner()->errorCode = UserIdentity::ERROR_PASSWORD_INVALID;
        } else {
            $this->setUserIdentityData();
        }
    }
}
