<?php
/**
 * Class DeactivationDataTrait
 *
 * @mixin CActiveRecord
 */
trait DeactivationDataTrait
{
    protected static $onlyActive = false;

    public static function enableOnlyActive()
    {
        self::$onlyActive = true;
    }

    public static function disableOnlyActive()
    {
        self::$onlyActive = false;
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(false, false);

        $defaultScope = [];
        if (self::$onlyActive) {
            $defaultScope['condition'] = " {$alias}.is_delete = 0";
        }
        return array_merge(parent::defaultScope(), $defaultScope);
    }

    public function delete()
    {
        if (!$this->getIsNewRecord()) {
            if ($this->beforeDelete()) {
                $result = $this->updateByPk($this->getPrimaryKey(), ['is_delete' => 1]) == 1;
                $this->afterDelete();
            } else {
                $result = false;
            }
        } else {
            $result = false;
        }
        return $result;
    }

    public function restore()
    {
        if (!$this->getIsNewRecord()) {
            return $this->updateByPk($this->getPrimaryKey(), ['is_delete' => 0]) == 1;
        }
    }

    public function isActive()
    {
        return $this->is_delete == 0;
    }
}
