<?php
Yii::import("admin.models.*");

/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 23.04.14
 * Time: 16:55
 */
class UserGroupManager extends CComponent
{

    public function init()
    {
        if (Yii::app()->getComponent('user')) {
            $this->currentGroupId = Yii::app()->user->getState('user_group_id', null);
        }
    }

    /**
     * Идентификатор текущей группы пользователей
     *
     * @var integer
     */
    protected $currentGroupId;

    /**
     * @return int
     */
    public function getCurrentGroupId()
    {
        return $this->currentGroupId;
    }

    /**
     * @param int $currentGroup
     */
    public function setCurrentGroupId($currentGroup)
    {
        if (Yii::app()->getComponent('user')) {
            Yii::app()->user->setState('user_group_id', $currentGroup);
        }
        $this->currentGroupId = $currentGroup;
    }

    /**
     * Возвращает идентификатор группы по её названию
     *
     * @param $name
     * @return int
     * @throws CException
     */
    public function getGroupIdByName($name)
    {
        /** @var UserGroup $group */
        $group = UserGroup::model()->findByAttributes(['name' => $name]);
        if (!$group) {
            throw new CException(Yii::t(
                'AdminModule.messages',
                'Не найдено ни одной группы пользователей с переданным названием: ' . $name
            ));
        }

        return $group->uid;
    }

    /**
     * Проверка на то что разрешен логин в эту группу
     *
     * @param $name
     * @return bool
     */
    public function isAllowedUserGroup($name)
    {
        try {
            $this->getGroupIdByName($name);
            return true;
        } catch (CException $e) {
            return false;
        }
    }

    /**
     * @return UserGroup[]
     */
    public function getAvailableGroups()
    {
        return UserGroup::model()->findAll();
    }

    /**
     * @return UserGroup
     */
    public function getCurrentGroup()
    {
        return UserGroup::model()->findByPk($this->getCurrentGroupId());
    }
}
