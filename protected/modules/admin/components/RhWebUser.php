<?php
Yii::import('admin.AdminModule');
Yii::import('admin.models.User');

class RhWebUser extends CWebUser
{
    protected $userIdBeforeLogout;

    const AUTOMATIC_LOGOUT_KEY = 'automaticLogout';

    protected function updateAuthStatus()
    {
        parent::updateAuthStatus();
        if (!$this->getIsGuest()) {
            $user = User::model()->findByPk($this->getId());
            $user->updateLastRequestTime();
            $this->setState('user_group_id', $user->user_group_id);
        }
    }

    protected function beforeLogout()
    {
        $this->userIdBeforeLogout = $this->getId();
        return true;
    }

    protected function afterLogout()
    {
        if ($this->authTimeout !== null && $this->getState(self::AUTH_TIMEOUT_VAR) == null) {
            Yii::app()->getUser()->setFlash(
                self::AUTOMATIC_LOGOUT_KEY,
                Yii::t('AdminModule.messages', 'Был достигнут предел неактивности пользователя в системе.')
            );
        }

        $user = User::model()->findByPk($this->userIdBeforeLogout);
        if ($user) {
            $user->updateLastRequestTime(true);
        }
    }

    protected function afterLogin($fromCookie)
    {
        $user = User::model()->findByPk($this->getId());
        $user->updateLastRequestTime();
    }

    public function getUser()
    {
        $user = User::model()->findByPk($this->getId());
        return $user;
    }
}
