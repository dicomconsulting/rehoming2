<?php
Yii::import('admin.components.UserIdentityStrategy.*');

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    private $_id;

    /** @var  AbstractUserIdentityStrategy */
    private $strategy;

    public $userGroup;

    private $name;

    public function __construct($userGroup, $username, $password)
    {
        $this->userGroup = $userGroup;
        $this->_calculateStrategy($userGroup, $username);
        parent::__construct($username, $password);
    }

    /**
     * Authenticates a user.
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        $this->strategy->authenticate();
        $user = $this->strategy->getUser();
        if ($user) {
            $this->name = $user->getDisplayName();
        }
        return empty($this->errorCode) || $this->errorCode == UserIdentity::ERROR_NONE;
    }

    public function isUserGroupRequired()
    {
        return $this->strategy->isUserGroupRequired();
    }

    public function setId($id)
    {
        $this->_id = $id;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function setStrategy(AbstractUserIdentityStrategy $strategy)
    {
        $this->strategy = $strategy;
    }

    /**
     *
     * @return AbstractUserIdentityStrategy
     */
    public function getStrategy()
    {
        return $this->strategy;
    }

    private function _calculateStrategy($userGroup, $username)
    {
        /** @var User $user */
        $user = User::model()->findByAttributes(array('username' => $username));
        if (!empty($user)) {
            $userIdentityStrategyClassName = ucfirst(
                    strtolower($user->authentication_type)
                ) . "UserIdentityStrategy";
            $this->setStrategy(new $userIdentityStrategyClassName($this));
            $this->getStrategy()->setUser($user);
        } elseif (Yii::app()->userGroupManager->isAllowedUserGroup($userGroup)) {
            $this->setStrategy(new BiotronikUserIdentityStrategy($this));
        } else {
            $this->setStrategy(new LocalUserIdentityStrategy($this));
        }
    }

    public function getName()
    {
        return $this->name;
    }
}
