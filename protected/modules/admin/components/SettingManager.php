<?php

class SettingManager
{
    private $debug;

    private $rootPath;

    private $cacheObject;

    public function __construct($rootPath)
    {
        $this->debug = (boolean) YII_DEBUG;
        $this->rootPath = $rootPath;
    }

    public function getConfigArray($configPath)
    {
        if ($this->debug || ($configArray = $this->_getCacheObject()->get($configPath)) == false) {
            $configArray = require($configPath);
            array_walk_recursive(
                $configArray,
                [$this, 'assignConfigValues'],
                $this->_getAllSettings($configArray['components']['db'])
            );
            $this->_getCacheObject()->set($configPath, $configArray);
        }
        return $configArray;
    }

    public function assignConfigValues(&$item, $key, $settings)
    {
        if (is_string($item) && preg_match('/^%.*%$/', $item)) {
            $settingName = trim($item, '%');
            if (array_key_exists($settingName, $settings)) {
                $item = $settings[$settingName];
            } else {
                $item = null;
            }
        }
    }

    public function purgeCache()
    {
        $this->_getCacheObject()->flush();
    }

    private function _getCacheObject()
    {
        if (!$this->cacheObject instanceof CCache) {
            $this->cacheObject = new CFileCache();
            $this->cacheObject->keyPrefix = get_class($this);
            $this->cacheObject->cachePath = $this->rootPath . DIRECTORY_SEPARATOR . 'protected'
                . DIRECTORY_SEPARATOR . 'runtime' . DIRECTORY_SEPARATOR . 'settingsCache';
            $this->cacheObject->init();
        }
        return $this->cacheObject;
    }

    private function _getAllSettings($dbConfig)
    {
        $normalizeArray = [];
        $dbConnection = new PDO(
            $dbConfig['connectionString'],
            $dbConfig['username'],
            $dbConfig['password']
        );
        $statement = $dbConnection->prepare('SELECT `name`, `value`, `serialized` FROM `' . $dbConfig['tablePrefix'] . 'setting`');
        $statement->execute();
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            if ($row['serialized']) {
                $normalizeArray[$row['name']] = array_map(
                    function ($item) {
                        return trim($item);
                    },
                    explode(',', $row['value'])
                );
            } else {
                $normalizeArray[$row['name']] = $row['value'];
            }
        }
        return $normalizeArray;
    }
}
