<?php

/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.01.16
 * Time: 15:18
 */
class SavedFilterController extends CController
{
    public function init()
    {
        parent::init();
    }

    public function accessRules()
    {
        $accessRules = parent::accessRules();

        array_unshift(
            $accessRules,
            [
                'allow',
                'actions' => ['saveSavedFilterParams'],
                'users'   => ['*'],
            ]
        );

        return $accessRules;
    }

    /**
     * Сохранить пользовательский фильтр
     *
     * @internal param int $filterId идентификатор фильтра
     * @internal param string $filterName имя фильтра
     * @internal param array $filterParams значения отфильтрованных параметров
     */
    public function actionSaveSavedFilterParams()
    {

        $user_id = Yii::app()->getUser()->id;
        // $filterId     = intval($_POST['filterId']);
        $filterName   = $_POST['filterName'];
        $filterParams = json_encode([
            'mainFilter' => $_POST['filterParams'],
            'additional' => array_key_exists('additionalFilter', $_POST) ? $_POST['additionalFilter'] : []
        ], JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

        $filterPlacement = $_POST['filterPlacement'];

        $savedFilter = SavedFilter::model()->findByAttributes(['name' => $filterName, 'user_id' => $user_id, 'placement' => $filterPlacement]);
        if ($savedFilter) {
            // Сохраненный фильтр уже существует
            $savedFilter->condition = $filterParams;
            $savedFilter->save();
        } else {
            $savedFilter            = new SavedFilter();
            $savedFilter->user_id   = $user_id;
            $savedFilter->name      = $filterName;
            $savedFilter->placement = $filterPlacement;
            $savedFilter->condition = $filterParams;
            $savedFilter->save();
        }

        header('Content-Type: application/json; charset=utf-8');
        echo json_encode([
            'success' => 'true',
            'id'      => $savedFilter->id,
            'name'    => $savedFilter->name,
        ], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }

    /**
     *  Удалить существующий фильтр
     *
     * @internal param int $filterId идентификатор фильтра
     */
    public function actionDeleteSavedFilter()
    {
        $user_id  = Yii::app()->getUser()->id;
        $filterId = intval($_POST['filterId']);
        SavedFilter::model()->deleteAllByAttributes(['id' => $filterId, 'user_id' => $user_id]);
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode([
            'success' => 'true',
            'result'  => $filterId
        ], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }

    /**
     * Получить ссылку для определенного фильтра
     *
     * @internal param int $filterId идентификатор фильтра
     * @internal param string $parameterPrefix имя фильтра
     * @internal param array $urlPrefix значения отфильтрованных параметров
     */
    public function actionGetFilterUrl()
    {

        $user_id         = Yii::app()->getUser()->id;
        $filterId        = intval($_POST['filterId']);
        $parameterPrefix = $_POST['parameterPrefix'];
        $urlPrefix       = $_POST['urlPrefix'];

        /** @var SavedFilter $savedFilter */
        $savedFilter = SavedFilter::model()->findByPk($filterId);

        $url         = SavedFilter::formUrlForSavedFilter(json_decode($savedFilter->condition, true), $parameterPrefix, $urlPrefix );
        echo json_encode([
            'success' => 'true',
            'result'  => $url
        ], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }

}
