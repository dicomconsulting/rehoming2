<?php

class UserController extends BaseAdminController
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    /**
     * @return string
     */
    public function getModelClassName()
    {
        return "user";
    }


    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => ['login'],
                'users' => ['*']
            ],
            [
                'allow',
                'actions' => ['logout'],
                'users' => ['@']
            ],
            [
                'allow',
                'actions' => ['index', 'view', 'create', 'update', 'delete', 'admin'],
                'roles' => ['userManagement'],
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

    public function actionLogin()
    {
        if (!Yii::app()->user->getIsGuest()) {
            $this->redirect(Yii::app()->homeUrl);
        }

        $model = new LoginForm();

        if (isset($_POST['ajax']) && $_POST['ajax']==='login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['LoginForm'])) {
            $model->attributes=$_POST['LoginForm'];
            if ($model->validate() && $model->login()) {
                $returnUrl = Yii::app()->user->returnUrl;
                if ($returnUrl == Yii::app()->user->loginUrl) {
                    $returnUrl = Yii::app()->homeUrl;
                }
                $this->redirect($returnUrl);
            }
        }
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

        /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model=new User;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['User'])) {
            $model->attributes=$_POST['User'];
            if ($model->save()) {

                if (isset($_POST['roles'])) {
                    $this->saveRoles($model, $_POST['roles']);
                }

                $this->redirect(array('view','id'=>$model->uid));
            }
        }

        $this->render('create', ['model'=>$model]);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     * @return mixed
     */
    public function actionUpdate($id)
    {
        /** @var User $model */
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['User'])) {
            $model->attributes=$_POST['User'];
            if ($model->save()) {

                if (isset($_POST['roles'])) {
                    $this->saveRoles($model, $_POST['roles']);
                }
                $this->redirect(array('view', 'id'=>$model->uid));
            }
        }

        $model->password = "";
        $model->password_repeat = "";

        $this->render('update', ['model'=>$model]);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     * @throws CHttpException
     * @return mixed
     */
    public function actionDelete($id)
    {
        $user = $this->loadModel($id);

        if (!$user->isEditable()) {
            throw new CHttpException(500, Yii::t("AdminModule.messages", "Данный пользователь не может быть удален"));
        }

        $user->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }


    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new User('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['User'])) {
            $model->attributes=$_GET['User'];
        }

        $this->render('admin', ['model'=>$model]);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param  integer        $id the ID of the model to be loaded
     * @return User           the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=User::model()->findByPk($id);
        if ($model===null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param User $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax']==='user-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }


    public function getSelectedRoles(User $user)
    {
        $result = [];
        /** @var CDbAuthManager $am */
        $am = Yii::app()->authManager;
        $roles = $am->getRoles($user->uid);
        foreach ($roles as $role) {
            $result[] = $role->name;
        }
        return $result;
    }

    public function getAviableRoles()
    {
        /** @var CDbAuthManager $am */
        $am = Yii::app()->authManager;
        return $am->getRoles();
    }

    protected function saveRoles(User $user, $roles)
    {
        /** @var CDbAuthManager $am */
        $am = Yii::app()->authManager;

        $exRoles = $am->getRoles($user->uid);

        foreach ($exRoles as $role) {
            $am->revoke($role->name, $user->uid);
        }

        foreach ($roles as $newRole) {
            $am->assign($newRole, $user->uid);
        }

        $am->save();
    }
}
