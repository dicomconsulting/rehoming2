<?php
Yii::import('patient.models.PatientGroup');
Yii::import('admin.models.Doctor');
Yii::import('admin.models.PatientGroupAccess');

class DoctorController extends BaseAdminController
{
    protected $pageTitle = 'Врачи';

    public function accessRules()
    {
        return [
            [
                'allow',
                'roles' => ['userManagement'],
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

    public function getModelClassName()
    {
        return 'Doctor';
    }

    public function actionPatientAccess($id)
    {
        $doctor = $this->loadModel($id);
        $patientGroupArray = PatientGroup::model()->findAll();
        $groupAccessLevelArray = PatientGroupAccess::model()->getAllIndexByPatientGroup($id);
        $entityForSave = [];
        if (isset($_POST['PatientGroupAccess'])) {
            $valid = true;
            foreach ($patientGroupArray as $patientGroup) {
                if(isset($_POST['PatientGroupAccess'][$patientGroup->uid]) && $doctor->isEditable()) {
                    if (isset($groupAccessLevelArray[$patientGroup->uid])) {
                        $patientGroupAccess = $groupAccessLevelArray[$patientGroup->uid];
                        $patientGroupAccess->access_level = $_POST['PatientGroupAccess'][$patientGroup->uid]['access_level'];
                        $valid = $patientGroupAccess->validate() && $valid;
                        $entityForSave[$patientGroup->uid] = $patientGroupAccess;
                    } else {
                        $patientGroupAccess = new PatientGroupAccess();
                        $patientGroupAccess->patient_group_id = $patientGroup->uid;
                        $patientGroupAccess->doctor_id = $id;
                        $patientGroupAccess->access_level = $_POST['PatientGroupAccess'][$patientGroup->uid]['access_level'];
                        $valid = $patientGroupAccess->validate() && $valid;
                        $entityForSave[$patientGroup->uid] = $patientGroupAccess;
                    }
                }
            }

            if ($valid) {
                foreach ($entityForSave as $entity) {
                    $entity->save();
                }
                $this->redirect(['index']);
            }
        }

        $this->render(
            'patientAccess',
            [
                'doctor' => $doctor,
                'patientGroupArray' => $patientGroupArray,
                'groupAccessLevelArray' => $groupAccessLevelArray + $entityForSave,
                'newGroupAccessLevel' => new PatientGroupAccess(),
                'accessLevelList' => PatientGroupAccess::getAccessLevelList()
            ]
        );
    }

    public function getUsersToLink(Doctor $model)
    {
        if ($model->linkedUser) {
            return array_merge([$model->linkedUser], User::getAssignable());
        } else {
            return User::getAssignable();
        }
    }
}
