<?php

Yii::import('statisticsNew.models.StatisticsTemplateFile');
Yii::import('statisticsUpdate.models.StatisticsTemplateFile');
Yii::import('statistics2018.models.StatisticsTemplateFile');

class StatisticsController extends CController
{

    public $layout = '//layouts/admin';


    /**
     * @return array action filters
     */
    public function filters()
    {
        return array('accessControl');
    }

    public function accessRules()
    {
        return [
            [
                'allow',
                'roles' => ['viewStatistics']
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

    public function actionDownloadDefaultTemplate()
    {
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename=defaultStatisticsTemplate.xlsx');
        $path = Yii::getPathOfAlias('application') . '/../uploads/defaultStatisticsTemplate.xlsx';
        readfile($path);
    }

    public function actionDownloadTemplate()
    {
        $currentGroupId = Yii::app()->user->user_group_id;
        if (!$currentGroupId) {
            echo 'Unknown user';
            Yii::app()->end();
        }
        $statisticsTemplateFile = StatisticsTemplateFile::model()->findByAttributes(['group_id' => $currentGroupId]);
        if (!$statisticsTemplateFile) {
            echo 'Template not exists';
            Yii::app()->end();
        }
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename=' . $statisticsTemplateFile->name);
        $data = base64_decode($statisticsTemplateFile->fileData);
        echo $data;
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $currentGroupId = Yii::app()->user->user_group_id;
        if ($currentGroupId) {
            $statisticsTemplateFile = StatisticsTemplateFile::model()->findByAttributes(['group_id' => $currentGroupId]);
        } else {
            $statisticsTemplateFile = null;
        }

        if (array_key_exists('xlsTemplate', $_FILES)) {
            $name         = $_FILES['xlsTemplate']['name'];
            $fileContents = file_get_contents($_FILES['xlsTemplate']['tmp_name']);
            if ($currentGroupId) {
                if (!$statisticsTemplateFile) {
                    $statisticsTemplateFile           = new StatisticsTemplateFile();
                    $statisticsTemplateFile->group_id = $currentGroupId;
                }
                $statisticsTemplateFile->name     = $name;
                $statisticsTemplateFile->fileData = base64_encode($fileContents);
                if ($statisticsTemplateFile->save()) {
                    Yii::app()->user->setFlash('successSave', Yii::t('AdminModule.statistics', 'Шаблон успешно сохранен'));
                } else {
                    $errors       = $statisticsTemplateFile->getErrors();
                    $errorsString = '';
                    array_walk_recursive($errors, function ($value, $key) use (&$errorsString) {
                        $errorsString .= "<br>\n" . $value;
                    });
                    Yii::app()->user->setFlash('errorSave',
                        Yii::t('AdminModule.statistics', 'Ошибка сохранения шаблона: ') . $errorsString);
                }
            }
        }
        $this->render('index', ['currentTemplate' => $statisticsTemplateFile]);
    }
}
