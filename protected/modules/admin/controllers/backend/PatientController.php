<?php
Yii::import('admin.models.Doctor');
Yii::import('patient.models.Patient');
Yii::import('patient.models.PatientGroup');

class PatientController extends BaseAdminController
{
    protected $pageTitle = 'Список пациентов';

    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => ['delete'],
                'roles' => ['patientDeletion'],
            ],
            [
                'deny',
                'actions' => ['delete'],
                'users' => ['*'],
            ],
            [
                'allow',
                'roles' => ['userManagement'],
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

    public function getModelClassName()
    {
        return 'Patient';
    }

    public function actionView($id)
    {
        throw new CException('Действие недоступно.');
    }

    public function actionCreate()
    {
        throw new CException('Действие недоступно.');
    }

    public function actionUpdate($id)
    {
        throw new CException('Действие недоступно.');
    }

    protected function edit(CActiveRecord $model)
    {
        throw new CException('Действие недоступно.');
    }

    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();
        $this->redirect($this->createUrl($this->id . "/index"));
    }

    public function customizeDataProvider(CDataProvider $dataProvider)
    {
        $dataProvider->sort = array(
            "defaultOrder" => "t.uid");
        return $dataProvider;
    }
}
