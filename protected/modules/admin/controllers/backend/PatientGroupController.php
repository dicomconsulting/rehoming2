<?php
Yii::import('patient.models.PatientGroup');
Yii::import('admin.models.Doctor');
Yii::import('admin.models.PatientGroupAccess');

class PatientGroupController extends BaseAdminController
{
    protected $pageTitle = 'Группы пациентов';

    public function accessRules()
    {
        return [
            [
                'allow',
                'roles' => ['userManagement'],
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

    public function actionEditAccess($id)
    {
        $patientGroup = $this->loadModel($id);
        $doctorArray = Doctor::model()->findAll();
        $groupAccessLevelArray = PatientGroupAccess::model()->getAllIndexByDoctor($id);
        $entityForSave = [];
        if (isset($_POST['PatientGroupAccess'])) {
            $valid = true;
            foreach ($doctorArray as $doctor) {
                if (isset($_POST['PatientGroupAccess'][$doctor->uid]) && $doctor->isEditable()) {
                    if (isset($groupAccessLevelArray[$doctor->uid])) {
                        $patientGroupAccess = $groupAccessLevelArray[$doctor->uid];
                        $patientGroupAccess->access_level = $_POST['PatientGroupAccess'][$doctor->uid]['access_level'];
                        $valid = $patientGroupAccess->validate() && $valid;
                        $entityForSave[$doctor->uid] = $patientGroupAccess;
                    } else {
                        $patientGroupAccess = new PatientGroupAccess();
                        $patientGroupAccess->patient_group_id = $id;
                        $patientGroupAccess->doctor_id = $doctor->uid;
                        $patientGroupAccess->access_level = $_POST['PatientGroupAccess'][$doctor->uid]['access_level'];
                        $valid = $patientGroupAccess->validate() && $valid;
                        $entityForSave[$doctor->uid] = $patientGroupAccess;
                    }
                }
            }

            if ($valid) {
                foreach ($entityForSave as $entity) {
                    $entity->save();
                }
                $this->redirect(['index']);
            }
        }

        $this->render(
            'editAccess',
            [
                'patientGroup'          => $patientGroup,
                'doctorArray'           => $doctorArray,
                'groupAccessLevelArray' => $groupAccessLevelArray + $entityForSave,
                'newGroupAccessLevel'   => new PatientGroupAccess(),
                'accessLevelList'       => PatientGroupAccess::getAccessLevelList()
            ]
        );
    }

    public function getModelClassName()
    {
        return 'PatientGroup';
    }

    public function actionView($id)
    {
        throw new CException('Действие недоступно.');
    }

    public function actionCreate()
    {
        throw new CException('Действие недоступно.');
    }

    public function actionUpdate($id)
    {
        throw new CException('Действие недоступно.');
    }

    protected function edit(CActiveRecord $model)
    {
        throw new CException('Действие недоступно.');
    }

    public function actionDelete($id)
    {
        throw new CException('Действие недоступно.');
    }


}

