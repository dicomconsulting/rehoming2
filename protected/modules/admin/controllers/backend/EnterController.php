<?php

class EnterController extends BaseAdminController
{
    protected $pageTitle = 'Входная страница';

    public function accessRules()
    {
        return [
            [
                'allow',
                'roles' => ['adminEnter'],
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

    public function actionIndex()
    {
        $this->render("index");
    }

    public function getModelClassName()
    {
        return 'User';
    }

}
