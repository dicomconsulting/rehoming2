<?php
Yii::import('admin.models.Doctor');

class RolesController extends BaseAdminController
{
    protected $pageTitle = 'Управление ролями пользователей';

    public function accessRules()
    {
        return [
            [
                'allow',
                'roles' => ['userManagement'],
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        Yii::app()->getModule("rights");

        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']);
        }

        $dataProvider = new RAuthItemDataProvider('roles', array(
            'type'=>CAuthItem::TYPE_ROLE,
        ));

        $this->render('index', ['dataProvider' => $dataProvider]);
    }

    public function actionEdit($id)
    {
        /** @var CDbAuthManager $am */
        $am = Yii::app()->authManager;
        $authItem = $am->getAuthItem($id);
        $operations = $am->getOperations();
        $errors = [];

        $errors = $this->processItem($errors, $authItem);

        $this->render(
            'edit',
            [
                'authItem' => $authItem,
                'possibleChilds' => $operations,
                'errors' => $errors
            ]
        );
    }

    public function getSelectedOperations(CAuthItem $item)
    {
        if (isset($_POST['operations'])) {
            return $_POST['operations'];
        } else {
            $result = [];

            $children = $item->getChildren();
            foreach ($children as $child) {
                /** @var CAuthItem $child */
                $result[] = $child->name;
            }

            return $result;
        }
    }

    public function actionCreate()
    {
        /** @var CDbAuthManager $am */
        $am = Yii::app()->authManager;
        $authItem = new CAuthItem($am, "", CAuthItem::TYPE_ROLE);
        $operations = $am->getOperations();
        $errors = [];

        $errors = $this->processItem($errors, $authItem);

        $this->render(
            'edit',
            [
                'authItem' => $authItem,
                'possibleChilds' => $operations,
                'errors' => $errors
            ]
        );
    }


    public function getModelClassName()
    {
        return false;
    }

    public function actionView($id)
    {
        throw new CException('Действие недостпно.');
    }

    public function actionUpdate($id)
    {
        throw new CException('Действие недостпно.');
    }

    protected function edit(CActiveRecord $model)
    {
        throw new CException('Действие недостпно.');
    }

    public function actionDelete($id)
    {
        /** @var CDbAuthManager $am */
        $am = Yii::app()->authManager;
        if ($item = $am->getAuthItem($id)) {
            $am->removeAuthItem($item->name);
        };

        $this->redirect(['index']);
    }

    /**
     * @param array $errors
     * @param CAuthItem $authItem
     * @return array
     */
    protected function processItem($errors, $authItem)
    {
        if (!empty($_POST)) {

            /** @var CDbAuthManager $am */
            $am = Yii::app()->authManager;

            if (!$authItem->name && (!isset($_POST['name']) || !trim($_POST['name']))) {
                $errors[] = "Заполнить название роли";
            } elseif (!$authItem->name) {
                if ($am->getAuthItem($_POST['name'])) {
                    $errors[] = "Это имя роли уже занято";
                } else {
                    $authItem->name = $_POST['name'];
                }
            }

            if (!isset($_POST['description']) || !trim($_POST['description'])) {
                $errors[] = "Заполнить описание роли";
            } else {
                $authItem->setDescription($_POST['description']);
            }

            if (!isset($_POST['operations']) || empty($_POST['operations'])) {
                $errors[] = "Выбрать операции";
            }

            if (empty($errors)) {

                if (!$am->getAuthItem($authItem->name)) {
                    $authItem = $am->createAuthItem($authItem->name, $authItem->type, $authItem->description);
                }

                foreach ($authItem->getChildren() as $child) {
                    /** @var CAuthItem $child */
                    $authItem->removeChild($child->name);
                }

                foreach ($_POST['operations'] as $operName) {
                    $authItem->addChild($operName);
                }

                $this->redirect(['index']);
                return $errors;
            }
            return $errors;
        }
        return $errors;
    }
}
