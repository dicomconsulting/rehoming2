<?php
Yii::import('admin.AdminModule');
Yii::import('admin.components.SettingManager');

class SettingController extends CController
{
    public $layout='//layouts/admin';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = [];

    protected $pageTitlePrefix = "Панель администратора";

    protected $pageTitle = 'Настройки';

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return [
            [
                'allow',
                'roles' => ['settingsManagement']
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

    public function actionIndex()
    {
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']);
        }

        $model = new Setting('search');
        $model->unsetAttributes();
        $this->render('index', ['model'=>$model]);
    }

    public function actionEdit()
    {
        $items = Setting::model()->findAll();
        if(isset($_POST['Setting'])) {
            $valid = true;
            $changedItems = [];
            foreach($items as $item)
            {
                if(isset($_POST['Setting'][$item->uid])) {
                    $item->attributes = $_POST['Setting'][$item->uid];
                    $changedItems[] = $item;
                }
                $valid = $item->validate() && $valid;
            }
            if($valid) {
                foreach ($changedItems as $item) {
                    $item->save();
                }
                $settingManager = new SettingManager(Yii::getPathOfAlias('webroot'));
                $settingManager->purgeCache();
                $this->redirect(['index']);
            }
        }
        $this->render('edit',array('items' => $items));
    }

    public function  getPageTitle()
    {
        $pageTitle = Yii::t('AdminModule.messages', $this->pageTitlePrefix);
        if (!empty($this->pageTitle)) {
            $pageTitle .= ' - ' . Yii::t(ucfirst(Yii::app()->controller->module->id) . 'Module.messages', $this->pageTitle);
        }
        return $pageTitle;
    }
}
