<?php
Yii::import('admin.models.UserGroup');

class UserGroupController extends BaseAdminController
{
    protected $pageTitle = 'Группы пациентов';

    public function accessRules()
    {
        return [
            [
                'allow',
                'roles' => ['userManagement'],
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

    public function getModelClassName()
    {
        return 'UserGroup';
    }
}
