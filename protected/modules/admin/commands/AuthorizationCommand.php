<?php
Yii::import('admin.models.User');
Yii::import('admin.models.BiotronikUser');

class AuthorizationCommand extends CConsoleCommand
{
    public function actionSetDefaultRules()
    {

        $this->createOperationIfNotExists('userManagement', "Управление пользователями");
        $this->createOperationIfNotExists('patientDeletion', "Удаление пациента");
        $this->createOperationIfNotExists('adminEnter', "Вход в панель управления");
        $this->createOperationIfNotExists('dictManagement', "Управление справочниками");
        $this->createOperationIfNotExists('settingsManagement', "Управление настройками проекта");
        $this->createOperationIfNotExists('infopagesManagement', "Управление инфо-страницами");
        $this->createOperationIfNotExists('taskManagement', "Управление задачами и ручной запуск");
        $this->createOperationIfNotExists('deviceManagement', "Управление параметрами устройств");

        $this->createOperationIfNotExists('viewPatientCard', "Просмотр карточек пациентов");
        $this->createOperationIfNotExists('viewPersonalData', "Просмотр персональных данных пациентов");

        $this->createOperationIfNotExists('viewProtocols', "Просмотр протоколов");
        $this->createOperationIfNotExists(
            'editProtocols',
            "Создание, редактирование и удаление неподписанных протоколов"
        );
        $this->createOperationIfNotExists('signProtocols', "Подписывание протоколов");
        $this->createOperationIfNotExists('unsignProtocols', "Снятие подписи с протоколов");

        $this->createOperationIfNotExists('acknowledgeMessage', "Менять статус \"Уведомлен\" у статусных сообщений");

        $this->createOperationIfNotExists('editClinicalData', "Создание, редактирование и удаление клинических данных");
        $this->createOperationIfNotExists('editHolter', "Создание, редактирование и удаление данных Холтера");
        $this->createOperationIfNotExists('editEkg', "Создание, редактирование и удаление данных электрокардиограммы");

        $this->createOperationIfNotExists('viewStatistics', "Просмотр статистики");

        /** @var CDbAuthManager $authManager */
        $authManager = Yii::app()->authManager;

        if (!$authManager->getAuthItem('baseAdmin')) {
            $role = $authManager->createRole('baseAdmin');
            $role->addChild('viewPatientCard');
        }

        if (!$authManager->getAuthItem('systemAdmin')) {
            $role = $authManager->createRole('systemAdmin', "Системный администратор, может всё");

            $this->addOperationsToRole(
                $role,
                [
                    'userManagement',
                    'patientDeletion',
                    'adminEnter',
                    'dictManagement',
                    'settingsManagement',
                    'infopagesManagement',
                    'taskManagement',
                    'deviceManagement',
                    'viewPatientCard',
                    'viewPersonalData',
                    'viewProtocols',
                    'editProtocols',
                    'signProtocols',
                    'unsignProtocols',
                    'acknowledgeMessage',
                    'editClinicalData',
                    'editHolter',
                    'editEkg',
                    'viewStatistics',
                ]
            );
        }

        if (!$authManager->getAuthItem('biotronikAdmin')) {
            $role = $authManager->createRole('biotronikAdmin', "Администратор с Биотроника");
            $this->addOperationsToRole(
                $role,
                [
                    'infopagesManagement',
                    'patientDeletion',
                    'adminEnter',
                    'viewPatientCard',
                    'viewPersonalData',
                    'viewProtocols',
                    'editProtocols',
                    'signProtocols',
                    'unsignProtocols',
                    'acknowledgeMessage',
                    'editClinicalData',
                    'editHolter',
                    'editEkg',
                    'viewStatistics',
                ]
            );
        }

        if (!$authManager->getAuthItem('biotronikUser')) {
            $role = $authManager->createRole('biotronikUser', "Пользователь с Биотроника");
            $this->addOperationsToRole(
                $role,
                [
                    'viewPatientCard',
                    'viewPersonalData',
                    'viewProtocols',
                    'editProtocols',
                    'acknowledgeMessage',
                    'editClinicalData',
                    'editHolter',
                    'editEkg',
                    'viewStatistics',
                ]
            );
        }

        if (!$authManager->getAuthItem('protocolSigner')) {
            $role = $authManager->createRole('protocolSigner', "Подписывание протоколов");
            $this->addOperationsToRole(
                $role,
                [
                    'signProtocols',
                ]
            );
        }

        if (!$authManager->getAuthItem('protocolUnsigner')) {
            $role = $authManager->createRole('protocolUnsigner', "Снятие подписи с протоколов");
            $this->addOperationsToRole(
                $role,
                [
                    'unsignProtocols',
                ]
            );
        }

        $authManager->save();
    }

    public function actionAssignRolesToUsers()
    {
        /** @var CAuthManager $authManager */
        $authManager = Yii::app()->authManager;

        $biotronikUsers = User::model()->findAllByAttributes(['authentication_type' => BiotronikUser::USER_IDENTITY]);
        foreach ($biotronikUsers as $biotronikUser) {
            /** @var BiotronikUser $biotronikUser */

            if ($biotronikUser->doctor->external_role == Doctor::ROLE_ADMIN) {
                $role = Doctor::LOCAL_ROLE_ADMIN;
            } else {
                $role = Doctor::LOCAL_ROLE_PHYSICIAN;
            }

            if (!$authManager->getAuthAssignment($role, $biotronikUser->uid)) {
                $authManager->assign($role, $biotronikUser->uid);
            }
        }

        $rootUser = User::model()->findByAttributes(['username' => User::ROOT_NAME]);
        if (!empty($rootUser)) {
            if (!$authManager->getAuthAssignment('systemAdmin', $rootUser->uid)) {
                $authManager->assign('systemAdmin', $rootUser->uid);
            }
        }

        $authManager->save();
    }

    /**
     * @param CAuthItem $role
     * @param [] $operations
     */
    protected function addOperationsToRole(CAuthItem $role, $operations)
    {
        foreach ($operations as $operation) {
            if (!$role->hasChild($operation)) {
                $role->addChild($operation);
            }
        }
    }

    protected function createOperationIfNotExists($name, $descr)
    {
        /** @var CDbAuthManager $authManager */
        $authManager = Yii::app()->authManager;
        if (!$item = $authManager->getAuthItem($name)) {
            $authManager->createOperation($name, $descr);
        } else {
            $item->setDescription($descr);
        }
    }

}
