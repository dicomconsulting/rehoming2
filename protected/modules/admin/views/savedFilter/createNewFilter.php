<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.01.16
 * Time: 14:17
 */

echo Yii::t('AdminModule.messages', 'Название');
?>
<br>
<input type="text" value="" id="newFilterName">
<br>
<?=Yii::t('AdminModule.messages', 'Сохраняя фильтр с существующим названием, вы переписываете существующий фильтр, а не создаете новый!');?>
<br>
<input type="button" value="<?=Yii::t('AdminModule.messages', 'Создать');?>" id="saveFilterButton">
<input type="button" value="<?=Yii::t('AdminModule.messages', 'Отмена');?>" id="cancelSaveFilterButton">

