<?php
/* @var $this MedicalCenterController */
/* @var $model MedicalCenter */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $('#doctor-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");
?>
<?php $this->widget('GridView', array(
    'id' => 'doctor-grid',
    'dataProvider' => $model->search(),
    'nullDisplay' => '---',
    'columns' => array(
        [
            "header" => Yii::t("AdminModule.messages", "№"),
            "value" => '$data->uid',
        ],
        [
            "header" => Yii::t("AdminModule.messages", "Обращение"),
            "value" => '$data->title',
        ],
        [
            "header" => Yii::t("AdminModule.messages", "Имя"),
            "value" => '$data->name',
        ],
        [
            "header" => Yii::t("AdminModule.messages", "Фамилия"),
            "value" => '$data->surname',
        ],
        [
            "header" => Yii::t("AdminModule.messages", "Пользователь системы"),
            'type' => 'html',
            "value" => function ($data, $row) {
                $user = $data->getLinkedUser();
                $rowValue = null;
                if ($user) {
                    $rowValue = CHtml::link($user->username, '/admin/backend/user/view/id/' . $user->uid);
                }
                return $rowValue;
            }
        ],
        [
            "header" => Yii::t("AdminModule.messages", "Группы пациентов"),
            'type' => 'html',
            "value" => function ($data, $row) {
                    /** @var User $user */
                    $rowValue = null;
                    $patientGroup = $data->patientGroup(["condition" => "access_level > 0"]);
                    if ($patientGroup) {
                        $first = true;
                        foreach ($patientGroup as $group) {
                            if (!$first) {
                                $rowValue .=  ", ";
                            } else {
                                $first = false;
                            }
                            /** @var PatientGroup $group */
                            $rowValue .= CHtml::link($group->name, Yii::app()->controller->createUrl("/patient/list/index", array("Patient[group_id]" => $group->uid)));
                        }
                    }
                    return $rowValue;
            }
        ],
        array(
            'class'=>'CButtonColumn',
            'template' => '{view} {update} {patientAccess} {delete}',
            'header' => CHtml::dropDownList(
                'pageSize',
                Yii::app()->user->getState('pageSize'),
                [10 => 10, 20 => 20, 50 => 50],
                [
                    'onchange' => "$.fn.yiiGridView.update('doctor-grid',{ data:{pageSize: $(this).val() }})",
                    'style' => 'width: 90px;'
                ]
            ),
            'buttons' => [
                'update' => [
                    'visible' => '$data->isEditable();'
                ],
                'delete' => [
                   'visible' => '$data->isEditable();'
                ],
                'patientAccess' => [
                    'label' => Yii::t("AdminModule.messages", "Права для групп пациентов"),
                    'imageUrl' => '/images/list.png',
                    'url' => 'Yii::app()->controller->createUrl("/admin/backend/doctor/patientAccess", ["id" => $data->uid])'
                ]
            ]
        ),
    ),
)); ?>

<?=CHtml::link(
    CHtml::tag('i', ['class' => 'icon-plus-sign'], ' ') . Yii::t("AdminModule.messages", "Создать"),
    $this->createUrl('/admin/backend/doctor/create'),
    ['class' => 'btn btn-primary', 'style' => 'float:right;']
);?>
