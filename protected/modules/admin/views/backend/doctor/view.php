<?php
/* @var $this MedicalCenterController */
/* @var $model MedicalCenter */
?>

    <h2><?=Yii::t("AdminModule.messages", "Врач")?> №<?php echo $model->uid;?></h2>
    <?=CHtml::link(
        CHtml::tag('i', ['class' => 'icon-list'], ' ') . Yii::t("AdminModule.messages", "Вернуться к списку"),
        $this->createUrl('/admin/backend/doctor/index'),
        ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 10px;']
    )?>
    <?=$model->isEditable() ? CHtml::link(
        CHtml::tag('i', ['class' => 'icon-edit'], ' ') . Yii::t("AdminModule.messages", "Редактировать"),
        $this->createUrl('/admin/backend/doctor/update/', ['id' => $model->uid]),
        ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 10px;']
    ) : '';?>
    <?=$model->isEditable() ? CHtml::link(
        CHtml::tag('i', ['class' => 'icon-trash'], ' ') . Yii::t("AdminModule.messages", "Удалить"),
        $this->createUrl('/admin/backend/doctor/delete/', ['id' => $model->uid]),
        ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 10px;']
    ) : '';?>
    <?php $this->widget('zii.widgets.CDetailView', array(
        'data' => $model,
        'attributes'=>array(
            [
                "name" => Yii::t("AdminModule.messages", "Обращение"),
                "value" => $model->title,
            ],
            [
                "name" => Yii::t("AdminModule.messages", "Имя"),
                "value" => $model->name,
            ],
            [
                "name" => Yii::t("AdminModule.messages", "Фамилия"),
                "value" => $model->surname,
            ],
            [
                "name" => Yii::t("AdminModule.messages", "Пользователь системы"),
                'type' => 'html',
                "value" => function($data){
                    $user = $data->getLinkedUser();
                    $rowValue = null;
                    if ($user) {
                        $rowValue = CHtml::link($user->username, '/admin/backend/user/view/id/' . $user->uid);
                    }
                    return $rowValue;
                }
            ]
        ),
    )); ?>


