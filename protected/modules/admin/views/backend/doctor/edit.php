<?php
/* @var $this DoctorController */
/* @var $model Doctor */
/* @var $form CActiveForm */
?>
<h2 class="offset1">
    <?=Yii::t("AdminModule.messages", "Врач")?>
    <?=$model->getIsNewRecord() ? '' : '№' . $model->uid;?>
</h2>
<div class="form">

    <?php $form = $this->beginWidget('I18nActiveForm', array(
        'id' => 'doctor-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => [
            'class' => 'form-horizontal'
        ]
    )); ?>

    <p class="note controls">
        <?=Yii::t("AdminModule.messages", "Поля отмеченные");?>
        <span class="required">"*"</span>
        <?=Yii::t("AdminModule.messages", "обязательны для заполнения");?>.
    </p>

    <div class="controls">
        <?=$form->errorSummary($model); ?>
    </div>

    <div class="control-group">
        <?=$form->labelEx($model, 'title', ['class' => 'control-label']); ?>
        <div class="controls">
            <?=$form->textField($model, 'title', ['readonly' => !$model->isEditable()]); ?>
            <?=$form->error($model, 'title');?>
        </div>
    </div>

    <div class="control-group">
        <?=$form->labelEx($model, 'name', ['class' => 'control-label']); ?>
        <div class="controls">
            <?=$form->textField($model, 'name', ['readonly' => !$model->isEditable()]); ?>
            <?=$form->error($model, 'name');?>
        </div>
    </div>

    <div class="control-group">
        <?=$form->labelEx($model, 'surname', ['class' => 'control-label']); ?>
        <div class="controls">
            <?=$form->textField($model, 'surname', ['readonly' => !$model->isEditable()]); ?>
            <?=$form->error($model, 'surname');?>
        </div>
    </div>

    <div class="control-group">
        <?=$form->labelEx($model, 'linkedUser', ['class' => 'control-label']); ?>
        <div class="controls">
            <?=$form->dropDownList(
                $model,
                'linkedUser',
                CHtml::listData($this->getUsersToLink($model), "uid", "username")
                ,['readonly' => !$model->isEditable()]
            ); ?>
            <?=$form->error($model, 'linkedUser');?>
        </div>
    </div>

    <div class="row buttons controls">
        <?=$model->isEditable() ? CHtml::submitButton(
            Yii::t('AdminModule.messages', $model->getIsNewRecord() ? 'Создать' : 'Обновить'),
            ['class' => 'btn btn-primary']
        ) : '';?>
        <?=CHtml::link(
            Yii::t('AdminModule.messages', 'Отмена'),
            $this->createUrl('/admin/backend/doctor/index'),
            ['class' => 'btn']
        );?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->