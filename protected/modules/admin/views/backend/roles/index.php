<?php
/* @var $this MedicalCenterController */
/* @var $dataProvider CArrayDataProvider */
?>

<?php $this->widget('GridView', array(
    'id' => 'roles-grid',
    'dataProvider' => $dataProvider,
    'columns' => array(
        [
            "header" => Yii::t("AdminModule.messages", "Название"),
            "value" => '$data->name',
        ],
        [
            "header" => Yii::t("AdminModule.messages", "Комментарий"),
            "value" => '$data->description',
        ],
        [
            'class'=>'CButtonColumn',
            'template' => '{update} {delete}',
            'updateButtonLabel' => Yii::t("AdminModule.messages", "Редактировать роль"),
            'updateButtonUrl' => function ($data) {
                    return Yii::app()->controller->createUrl("edit", array("id" => $data->name));
            },
            'deleteButtonLabel' => Yii::t("AdminModule.messages", "Удалить роль"),
            'deleteButtonUrl' => function ($data) {
                    return Yii::app()->controller->createUrl("delete", array("id" => $data->name));
            },

            'header' => CHtml::dropDownList(
                'pageSize',
                Yii::app()->user->getState('pageSize'),
                [10 => 10, 20 => 20, 50 => 50],
                [
                    'onchange' => "$.fn.yiiGridView.update('roles-grid',{ data:{pageSize: $(this).val() }})",
                    'style' => 'width: 70px;'
                ]
            ),
        ],
    )
));

echo CHtml::link(
    CHtml::tag('i', ['class' => 'icon-plus-sign'], ' ') . Yii::t("AdminModule.messages", "Создать роль"),
    $this->createUrl($this->id . '/create'),
    ['class' => 'btn btn-primary', 'style' => 'float:right;']
);