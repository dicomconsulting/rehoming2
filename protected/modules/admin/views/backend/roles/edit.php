<?php
/**
 * @var CAuthItem $authItem
 * @var RolesController $this
 * @var CAuthItem[] $possibleChilds
 * @var array $errors
 */
?>
<?php $this->renderPartial(
    '_form',
    array('authItem'=>$authItem, 'possibleChilds' => $possibleChilds, 'errors' => $errors)
);
?>

<!-- form -->