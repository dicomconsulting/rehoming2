<?php
/**
 * @var CAuthItem $authItem
 * @var RolesController $this
 * @var CAuthItem[] $possibleChilds
 * @var array $errors
 */
?>

    <h1>Создать новую роль</h1>

<?php $this->renderPartial(
    '_form',
    array('authItem'=>$authItem, 'possibleChilds' => $possibleChilds, 'errors' => $errors)
);
?>