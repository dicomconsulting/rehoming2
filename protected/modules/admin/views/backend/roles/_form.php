<?php
/**
 * @var CAuthItem $authItem
 * @var RolesController $this
 * @var CAuthItem[] $possibleChilds
 * @var array $errors
 */

?>
<?php if($authItem->name):?>
<h3>
    Роль "<?=$authItem->name;?>"
</h3>
<?php endif;?>

<div class="form">
    <?=CHtml::beginForm();?>

    <p class="note controls">
        <?=Yii::t("AdminModule.messages", "Поля отмеченные");?>
        <span class="required">"*"</span>
        <?=Yii::t("AdminModule.messages", "обязательны для заполнения");?>.
        <?php if($errors):?>
    <p class="alert"><?=Yii::t("AdminModule.messages", "Необхоидмо исправить следующие ошибки");?>:<br />
        <?php foreach($errors as $error):?>
            <?=$error;?><br/>
        <?php endforeach;?>
    </p>
<?php endif;?>
    </p>
    <div>
        <?php if(!$authItem->name): ?>
        <div class="row">
            <div class="control-group span8">
                <div >
                    <h4><?=Yii::t("AdminModule.messages", "Название роли");?> <span class="required">*</span></h4>
                    <div class="controls">
                        <?=CHtml::textField("name", $authItem->name); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php endif;?>
        <div class="row">
            <div class="control-group span8">
                <div >
                    <h4><?=Yii::t("AdminModule.messages", "Описание роли");?> <span class="required">*</span></h4>
                    <div class="controls">
                        <?=CHtml::textField("description", $authItem->description); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group">
                <div class="span6">
                    <h4><?=Yii::t("AdminModule.messages", "Разрешенные операции");?>  <span class="required">*</span></h4>
                </div>
                <div class="controls">
                    <?=CHtml::checkBoxList(
                        "operations",
                        $this->getSelectedOperations($authItem),
                        CHtml::listData(
                            $possibleChilds,
                            "name",
                            function (CAuthItem $model) {
                                return $model->description . " ({$model->name})";
                            }
                        ),
                        array(
                            "class" => "inline",
                            "template" => "<div class='span7'>{label}</div><div class='span1'>{input}</div>",
                            "labelOptions" => array("class" => "inline")
                        )
                    ); ?>
                </div>
            </div>
        </div>
        <div class="margin30">
            <?=CHtml::submitButton(
                Yii::t('AdminModule.messages', 'Сохранить'),
                ['class' => 'btn btn-primary']
            );
            ?>
            <?=CHtml::link(
                Yii::t('AdminModule.messages', 'Отмена'),
                $this->createUrl($this->id . '/index'),
                ['class' => 'btn']
            );
            ?>
        </div>
    </div>
    <?=CHtml::endForm();?>
</div>