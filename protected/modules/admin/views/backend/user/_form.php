<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'id' => 'user-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
            'htmlOptions' => ['autocomplete' => 'Off']
        )
    ); ?>

    <p class="note">Поля отмеченные знаком <span class="required">*</span> обязательны для заполнения.</p>

    <?php echo $form->errorSummary($model); ?>

    <?php if($model->isEditable()):?>

    <div class="row">
        <div class="span2">
        <?php echo $form->labelEx($model, 'username'); ?>
        </div>
        <div class="span4">
        <?php echo $form->textField($model, 'username', array('size' => 60, 'maxlength' => 128)); ?>
        <?php echo $form->error($model, 'username'); ?>
        </div>
    </div>

    <div class="row">
        <div class="span2">
        <?php echo $form->labelEx($model, 'new_password'); ?>
        </div>
        <div class="span4">
        <?php echo $form->passwordField($model, 'new_password', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'new_password'); ?>
        </div>
    </div>

        <div class="row">
        <div class="span2">
        <?php echo $form->labelEx($model, 'password_repeat'); ?>
        </div>
        <div class="span4">
        <?php echo $form->passwordField($model, 'password_repeat', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'password_repeat'); ?>
        </div>
    </div>

    <?php endif;?>

    <div class="row">
        <div class="span2">
        <?php echo $form->labelEx($model, 'email'); ?>
        </div>
        <div class="span4">
        <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 128)); ?>
        <?php echo $form->error($model, 'email'); ?>
        </div>
    </div>

    <div class="row">
        <div class="control-group">
            <div class="span6">
                <h4><?=Yii::t("AdminModule.messages", "Разрешенные роли");?>  <span class="required">*</span></h4>
            </div>
            <div class="controls">
                <?=CHtml::checkBoxList(
                    "roles",
                    $this->getSelectedRoles($model),
                    CHtml::listData(
                        $this->getAviableRoles(),
                        "name",
                        function (CAuthItem $model) {
                            return $model->description . " ({$model->name})";
                        }
                    ),
                    array(
                        "class" => "inline",
                        "template" => "<div class='span7'>{label}</div><div class='span1'>{input}</div>",
                        "labelOptions" => array("class" => "inline")
                    )
                ); ?>
            </div>
        </div>
    </div>

    <div class="row buttons margin30">
        <div class="span6">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
