<?php
/* @var $this UserController */
/* @var $model User */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $('#user-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");
?>
<?php $this->widget('GridView', array(
        'id' => 'user-grid',
        'dataProvider' => $model->search(),
        'nullDisplay' => '---',
        'columns' => array(
            [
                "header" => Yii::t("AdminModule.messages", "№"),
                "value" => '$data->uid',
            ],
            [
                "header" => Yii::t("AdminModule.messages", "Логин"),
                "value" => '$data->username',
            ],
            [
                "header" => Yii::t("AdminModule.messages", "Имейл"),
                "value" => '$data->email',
            ],
            [
                "header" => Yii::t("AdminModule.messages", "Исследователь"),
                "type" => "raw",
                "value" => function($data)
                    {
                        /** @var $data User */
                        if ($data->doctor_id) {
                            return $data->getDisplayName();
                        } else {
                            return "&nbsp;";
                        }
                    },
            ],
            array(
                'class'=>'CButtonColumn',
                'header' => CHtml::dropDownList(
                        'pageSize',
                        Yii::app()->user->getState('pageSize'),
                        ["20" => 20, 10 => 10, 50 => 50],
                        [
                            'onchange' => "$.fn.yiiGridView.update('user-grid',{ data:{pageSize: $(this).val() }})",
                            'style' => 'width: 70px;'
                        ]
                    ),
                'buttons' => [
                    'update',
                    'delete' => [
                        'visible' => '$data->isEditable();'
                    ],
                ]
            ),
        ),
    )); ?>

<?=CHtml::link(
    CHtml::tag('i', ['class' => 'icon-plus-sign'], ' ') . Yii::t("AdminModule.messages", "Создать"),
    $this->createUrl($this->id . '/create'),
    ['class' => 'btn btn-primary', 'style' => 'float:right;']
);?>
