<?php
    $this->pageTitle=Yii::app()->name . ' - ' . Yii::t('AdminModule.messages', 'Вход');
?>

<h2 class="offset1">
    <?=Yii::t('AdminModule.messages', 'Вход');?>
</h2>
<?php if(Yii::app()->user->hasFlash(RhWebUser::AUTOMATIC_LOGOUT_KEY)) { ?>
    <div class="badge badge-warning offset1">
        <?=Yii::app()->user->getFlash(RhWebUser::AUTOMATIC_LOGOUT_KEY);?>
    </div>
<?php } ?>
<div class="form">
<?php $form=$this->beginWidget('I18nActiveForm', array(
    'id'=>'login-form',
    'htmlOptions' => [
        'class' => 'form-horizontal'
    ]
)); ?>

    <p class="note controls">
        <?=Yii::t('AdminModule.messages', 'Поля с символом')?>
        <span class="required">*</span>
        <?=Yii::t('AdminModule.messages', 'обязательны для заполнения')?>
    </p>

    <div class="control-group">
        <?php echo $form->labelEx($model,'userGroup', ['class' => 'control-label']); ?>
        <div class="controls">
            <?php echo $form->textField($model,'userGroup'); ?>
            <?php echo $form->error($model,'userGroup'); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model,'username', ['class' => 'control-label']); ?>
        <div class="controls">
            <?php echo $form->textField($model,'username'); ?>
            <?php echo $form->error($model,'username'); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'password', ['class' => 'control-label']); ?>
        <div class="controls">
            <?php echo $form->passwordField($model,'password'); ?>
            <?php echo $form->error($model,'password'); ?>
        </div>
    </div>

    <div class="row buttons controls">
        <?php echo CHtml::submitButton(Yii::t('AdminModule.messages', 'Вход'), ['class' => 'btn btn-primary']); ?>
    </div>

<?php $this->endWidget(); ?>
</div><!-- form -->
