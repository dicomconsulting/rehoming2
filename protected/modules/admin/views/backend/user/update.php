<?php
/* @var $this UserController */
/* @var $model User */
?>

<h3>Редактирование пользователя <?php echo $model->username; ?></h3>

<?php $this->renderPartial('_form', array('model'=>$model));
