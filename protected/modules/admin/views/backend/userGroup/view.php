<?php
/* @var $this MedicalCenterController */
/* @var $model UserGroup */
?>

<h2><?= Yii::t("AdminModule.messages", "Группа пользователей") ?> &laquo;<?php echo $model->name; ?>&raquo;</h2>
<?=
CHtml::link(
    CHtml::tag('i', ['class' => 'icon-list'], ' ') . Yii::t("AdminModule.messages", "Вернуться к списку"),
    $this->createUrl('/admin/backend/userGroup/index'),
    ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 10px;']
)?>

<?=
CHtml::link(
    CHtml::tag('i', ['class' => 'icon-edit'], ' ') . Yii::t("AdminModule.messages", "Редактировать"),
    $this->createUrl('/admin/backend/userGroup/update/', ['id' => $model->uid]),
    ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 10px;']
);?>

<?=
CHtml::link(
    CHtml::tag('i', ['class' => 'icon-trash'], ' ') . Yii::t("AdminModule.messages", "Удалить"),
    $this->createUrl('/admin/backend/userGroup/delete/', ['id' => $model->uid]),
    ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 10px;']
);?>
<?php $this->widget(
    'zii.widgets.CDetailView',
    array(
        'data'       => $model,
        'attributes' => array(
            'name',
            'admin_name',
            'admin_pass',
            'mail_server',
            'mail_user',
            'mail_pass'
        ),
    )
); ?>


