<?php
/* @var $this DoctorController */
/* @var $model Doctor */
/* @var $form CActiveForm */
?>
<h2 class="offset1">
    <?=Yii::t("AdminModule.messages", "Врач")?>
    <?=$model->getIsNewRecord() ? '' : '№' . $model->uid;?>
</h2>
<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'doctor-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => [
            'class' => 'form-horizontal'
        ]
    )); ?>

    <p class="note controls">
        <?=Yii::t("AdminModule.messages", "Поля отмеченные");?>
        <span class="required">"*"</span>
        <?=Yii::t("AdminModule.messages", "обязательны для заполнения");?>.
    </p>

    <div class="controls">
        <?=$form->errorSummary($model); ?>
    </div>

    <div class="control-group">
        <?=$form->labelEx($model, 'name', ['class' => 'control-label']); ?>
        <div class="controls">
            <?=$form->textField($model, 'name'); ?>
            <?=$form->error($model, 'name');?>
        </div>
    </div>

    <div class="control-group">
        <?=$form->labelEx($model, 'admin_name', ['class' => 'control-label']); ?>
        <div class="controls">
            <?=$form->textField($model, 'admin_name'); ?>
            <?=$form->error($model, 'admin_name');?>
        </div>
    </div>

    <div class="control-group">
        <?=$form->labelEx($model, 'admin_pass', ['class' => 'control-label']); ?>
        <div class="controls">
            <?=$form->textField($model, 'admin_pass'); ?>
            <?=$form->error($model, 'admin_pass');?>
        </div>
    </div>

    <div class="control-group">
        <?=$form->labelEx($model, 'mail_server', ['class' => 'control-label']); ?>
        <div class="controls">
            <?=$form->textField($model, 'mail_server'); ?>
            <?=$form->error($model, 'mail_server');?>
        </div>
    </div>

    <div class="control-group">
        <?=$form->labelEx($model, 'mail_user', ['class' => 'control-label']); ?>
        <div class="controls">
            <?=$form->textField($model, 'mail_user'); ?>
            <?=$form->error($model, 'mail_user');?>
        </div>
    </div>

    <div class="control-group">
        <?=$form->labelEx($model, 'mail_pass', ['class' => 'control-label']); ?>
        <div class="controls">
            <?=$form->textField($model, 'mail_pass'); ?>
            <?=$form->error($model, 'mail_pass');?>
        </div>
    </div>


    <div class="row buttons controls">
        <?=
        CHtml::submitButton(
            Yii::t('AdminModule.messages', $model->getIsNewRecord() ? 'Создать' : 'Обновить'),
            ['class' => 'btn btn-primary']
        );?>

        <?=
        CHtml::link(
            Yii::t('AdminModule.messages', 'Отмена'),
            $this->createUrl('/admin/backend/userGroup/index'),
            ['class' => 'btn']
        );?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->