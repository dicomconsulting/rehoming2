<?php
/* @var $this MedicalCenterController */
/* @var $model MedicalCenter */

Yii::app()->clientScript->registerScript(
    'search',
    "$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $('#userGroup-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});"
);
?>
<?php $this->widget(
    'GridView',
    array(
        'id'           => 'userGroup-grid',
        'dataProvider' => $model->search(),
        'nullDisplay'  => '---',
        'columns'      => array(
            [
                "header" => Yii::t("AdminModule.messages", "№"),
                "value"  => '$data->uid',
            ],
            [
                'name'   => 'name',
                'header' => Yii::t("AdminModule.messages", "Наименование группы пользователей")
            ],
            [
                'name'   => 'admin_name',
            ],
            [
                'name'   => 'mail_user',
            ],
            array(
                'class'    => 'CButtonColumn',
                'template' => '{view} {update} {delete}',
                'header'   => CHtml::dropDownList(
                        'pageSize',
                        Yii::app()->user->getState('pageSize'),
                        [10 => 10, 20 => 20, 50 => 50],
                        [
                            'onchange' => "$.fn.yiiGridView.update('doctor-grid',{ data:{pageSize: $(this).val() }})",
                            'style'    => 'width: 90px;'
                        ]
                    ),
            ),
        ),
    )
); ?>

<?=
CHtml::link(
    CHtml::tag('i', ['class' => 'icon-plus-sign'], ' ') . Yii::t("AdminModule.messages", "Создать"),
    $this->createUrl('/admin/backend/userGroup/create'),
    ['class' => 'btn btn-primary', 'style' => 'float:right;']
);?>
