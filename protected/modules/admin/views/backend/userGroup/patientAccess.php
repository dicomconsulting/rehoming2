<?php
/* @var $this MedicalCenterController */
/* @var $model MedicalCenter */
/* @var $form CActiveForm */
?>
<h2 class="offset1">
    <?=$doctor->title;?>
    <?=$doctor->surname;?>
    <?=$doctor->name;?>
</h2>
<div class="form">
    <?php $form = $this->beginWidget('I18nActiveForm', array(
        'id' => 'patient-group-access-form',
        'enableAjaxValidation' => false
    )); ?>

    <table class="table table-condensed table-bordered rh-main-table">
        <col width = "5%">
        <col width = "50%">
        <col width = "15%">
        <col width = "15%">
        <col width = "15%">
        <tr>
            <th class="text-info">№</th>
            <th class="text-info"><?=Yii::t("AdminModule.messages", "Группа");?></th>
            <th class="text-info" style = "text-align: center;"><?=Yii::t("AdminModule.messages", "Полный доступ");?></th>
            <th class="text-info" style = "text-align: center;"><?=Yii::t("AdminModule.messages", "Только чтение");?></th>
            <th class="text-info" style = "text-align: center;"><?=Yii::t("AdminModule.messages", "Нет доступа");?></th>
        </tr>
        <?php foreach($patientGroupArray as $patientGroup) { ?>
            <?php $htmlOptions = ['template' => '<td style = "text-align: center;">{input}</td>', 'separator' => ' ']; ?>
            <tr>
                <td><?=$patientGroup->uid;?></td>
                <td>
                    <?=$patientGroup->name;?>
                </td>
                <?php
                    if (array_key_exists($patientGroup->uid, $groupAccessLevelArray)) {
                        $groupAccessLevel = $groupAccessLevelArray[$patientGroup->uid];
                    } else {
                        $groupAccessLevel = $newGroupAccessLevel;
                    }
                    if (!$doctor->isEditable()) {
                        $htmlOptions['disabled'] = 'disabled';
                    }
                ?>
                <?=$form->radioButtonList(
                    $groupAccessLevel,
                    "[{$patientGroup->uid}]access_level",
                    $accessLevelList,
                    $htmlOptions
                );?>
            </tr>
        <?php } ?>
    </table>
    <div>
        <?=$doctor->isEditable() ? CHtml::submitButton(
            Yii::t('AdminModule.messages', 'Сохранить'),
            ['class' => 'btn btn-primary']) : '';
        ?>
        <?=CHtml::link(
            Yii::t('AdminModule.messages', 'Отмена'),
            $this->createUrl('/admin/backend/doctor/index'),
            ['class' => 'btn']);
        ?>
    </div>
    <?php $this->endWidget(); ?>
</div><!-- form -->