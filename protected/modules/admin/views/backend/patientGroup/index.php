<?php
/* @var $this MedicalCenterController */
/* @var $model MedicalCenter */
?>

<?php $this->widget('GridView', array(
    'id' => 'patient-group-grid',
    'dataProvider' => $model->search(),
    'nullDisplay' => '---',
    'columns' => array(
        [
            "header" => Yii::t("AdminModule.messages", "№"),
            "value" => '$data->uid',
        ],
        [
            "header" => Yii::t("AdminModule.messages", "Название"),
            "value" => '$data->name',
        ],
        [
            "header" => Yii::t("AdminModule.messages", "Комментарий"),
            "value" => '$data->comment',
        ],
        [
            "header" => Yii::t("AdminModule.messages", "Количество пациентов"),
            'type' => 'raw',
            'htmlOptions' => ['style' => 'text-align: center;'],
            "value" => 'intval($data->patientCount)',
        ],
        [
            'class'=>'CButtonColumn',
            'template' => '{update} {patientList}',
            'updateButtonLabel' => Yii::t("AdminModule.messages", "Изменить права доступа"),
            'updateButtonUrl' => 'Yii::app()->controller->createUrl("editAccess", array("id" => $data->uid))',
            'header' => CHtml::dropDownList(
                'pageSize',
                Yii::app()->user->getState('pageSize'),
                [10 => 10, 20 => 20, 50 => 50],
                [
                    'onchange' => "$.fn.yiiGridView.update('patient-group-grid',{ data:{pageSize: $(this).val() }})",
                    'style' => 'width: 70px;'
                ]
            ),
            'buttons' => [
                'patientList' => [
                    'label' => Yii::t("AdminModule.messages", "Просмотр списка пациентов"),
                    'imageUrl' => '/images/list.png',
                    'url' => 'Yii::app()->controller->createUrl("/patient/list/index", array("Patient[group_id]" => $data->uid))'
                ]
            ]
        ],
    )
));
