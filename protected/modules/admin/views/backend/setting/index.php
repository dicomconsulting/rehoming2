<?php
/* @var $this MedicalCenterController */
/* @var $model MedicalCenter */

?>

<?php $this->widget('GridView', array(
    'id' => 'setting-grid',
    'dataProvider' => $model->search(),
    'columns' => array(
        [
            "header" => Yii::t("AdminModule.messages", "№"),
            "value" => '$data->uid',
        ],
        [
            "header" => Yii::t("AdminModule.messages", "Название"),
            "value" => '$data->name',
        ],
        [
            "header" => Yii::t("AdminModule.messages", "Описание"),
            "value" => '$data->description',
        ],
        [
            "header" => Yii::t("AdminModule.messages", "Значение"),
            "value" => '$data->value',
        ],
        array(
            'class'=>'CButtonColumn',
            'template' => '<i class="icon-ok"></i>',
            'header' => CHtml::dropDownList(
                'pageSize',
                Yii::app()->user->getState('pageSize'),
                [10 => 10, 20 => 20, 50 => 50],
                [
                    'onchange' => "$.fn.yiiGridView.update('setting-grid',{ data:{pageSize: $(this).val() }})",
                    'style' => 'width: 70px;'
                ]
            ),
        ),
    )
));
