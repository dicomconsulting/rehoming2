<?php
/* @var $this MedicalCenterController */
/* @var $model MedicalCenter */
/* @var $form CActiveForm */
?>
<h2 class="offset1">
    <?=Yii::t("AdminModule.messages", "Основные параметры")?>
</h2>
<?php $firstItem = reset($items); ?>
<?php if ($firstItem) { ?>
<div class="form">

    <?php $form = $this->beginWidget('I18nActiveForm', array(
        'id' => 'city-form',
        'enableAjaxValidation' => false
    )); ?>

    <p class="note controls">
        <?=Yii::t("AdminModule.messages", "Поля отмеченные");?>
        <span class="required">"*"</span>
        <?=Yii::t("AdminModule.messages", "обязательны для заполнения");?>.
    </p>
    <table class="table table-bordered">
        <col width = "5%">
        <col width = "35%">
        <col width = "60%" valign="middle">
        <tr>
            <th class="text-info">№</th>
            <th class="text-info"><?=$form->labelEx($firstItem, "name");?></th>
            <th class="text-info"><?=$form->labelEx($firstItem, "value");?></th>
        </tr>
        <?php foreach($items as $item) { ?>
            <tr>
                <td><?=$item->uid;?></td>
                <td>
                    <?=$item->name;?> <abbr title="<?=$item->description;?>"><i class="icon-info-sign"></i></abbr>
                </td>
                <td>
                    <?php if($item->serialized) {
                        $htmlOptions = [];
                        $attribute = "[{$item->uid}]value";
                        CHtml::resolveNameID($item, $attribute, $htmlOptions);
                        Yii::app()->clientScript->registerScript(
                            'tabs-' . $htmlOptions['id'],
                            "$('#{$htmlOptions['id']}').tagsInput({
                                'defaultText': '". Yii::t("AdminModule.messages", "Добавить значение") . "',
                                'height':'40px',
                                'width':'700px'
                            });"
                        );
                    }?>
                    <?=$form->textField($item, "[{$item->uid}]value", ['class' => 'input-xxlarge']);?>
                    <?=$form->error($item, "[{$item->uid}]value");?>
                </td>
            </tr>
        <?php } ?>
    </table>
    <div>
        <?=CHtml::submitButton(
            Yii::t('AdminModule.messages', 'Обновить'),
            ['class' => 'btn btn-primary']);
        ?>
        <?=CHtml::link(
            Yii::t('AdminModule.messages', 'Отмена'),
            $this->createUrl('/admin/backend/setting/index'),
            ['class' => 'btn']);
        ?>
    </div>
    <?php $this->endWidget(); ?>
    <?php } else { ?>
        <h3 class="alert-error"><?=Yii::t('AdminModule.messages', 'Выполните первоначальную установку настроек');?></h3>
    <?php } ?>

</div><!-- form -->