<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.03.16
 * Time: 14:27
 *
 * @var $this StatisticsController
 * @var $currentTemplate StatisticsTemplateFile
 */

?>
<br><br>
<h4><?= Yii::t('AdminModule.statistics', 'XLS шаблон статистики'); ?></h4>
<?= Yii::t('AdminModule.statistics', 'Шаблон по умолчанию: '); ?>
<a href='/admin/backend/statistics/downloadDefaultTemplate'><?= Yii::t('AdminModule.statistics', 'statisticsTemplate.xlsx'); ?></a><br>
<?php
if ($currentTemplate) {
    echo Yii::t('AdminModule.statistics', 'Текущий шаблон: ');
    echo '<a href="/admin/backend/statistics/downloadTemplate">';
    echo $currentTemplate->name;
    echo '</a><br><br>';
}
?>
<?= Yii::t('AdminModule.statistics', 'Выбрать новый шаблон:'); ?><br>
<?php if (Yii::app()->user->hasFlash('successSave')) {
    echo \TbHtml::alert(
        \TbHtml::ALERT_COLOR_SUCCESS,
        Yii::app()->user->getFlash('successSave')
    );
} ?>
<?php if (Yii::app()->user->hasFlash('errorSave')) {
    echo \TbHtml::alert(
        \TbHtml::ALERT_COLOR_DANGER,
        Yii::app()->user->getFlash('errorSave')
    );
} ?>

<form enctype='multipart/form-data' method='post'>
    <input type='file' name='xlsTemplate' accept=".xlsx">
    <input type='submit' value='<?= Yii::t('AdminModule.statistics', 'Загрузить'); ?>'>
</form>


