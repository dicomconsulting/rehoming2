<?php
/* @var $this MedicalCenterController */
/* @var $model MedicalCenter */
?>

<?php $this->widget('GridView', array(
    'id'           => 'patient-grid',
    'dataProvider' => $this->customizeDataProvider($model->search()),
    'nullDisplay'  => '---',
    'columns'      => array(
        [
            "header" => Yii::t("AdminModule.messages", "№"),
            "value"  => '$data->uid',
        ],
        [
            "header"      => Yii::t("AdminModule.messages", "Имя"),
            "value"       => function ($data) {
                return CHtml::link(
                    (Yii::app()->getModule('patient')->depersonalization ? $data->human_readable_id : $data->surname . ' ' . $data->name . ' ' . $data->middle_name),
                    $this->createUrl("/patient/status/summary", ['id' => $data->uid]),
                    ['target' => "_blank"]
                );
            },
            'type'        => 'raw',
            'htmlOptions' => ['width' => "30%"]
        ],
        [
            "header" => Yii::t("AdminModule.messages", "Комментарий"),
            "value"  => '$data->comment',
        ],
        [
            "header"      => Yii::t("AdminModule.messages", "Участник групп"),
            'type'        => 'raw',
            'htmlOptions' => ['style' => 'text-align: center;'],
            "value"       => function ($data) {
                /** @var Patient $data */
                return CHtml::link(
                    $data->group->name,
                    $this->createUrl("backend/patientGroup/editAccess",
                        ['id' => $data->group_id]
                    )
                );
            },
        ],
        [
            'class'              => 'CButtonColumn',
            'template'           => '{delete}',
            'buttons'            => [
                'delete' => [
                    'visible' => 'Yii::app()->getUser()->checkAccess("patientDeletion");'
                ],
            ],
            'deleteConfirmation' => Yii::t("AdminModule.messages", "Удалить данные без возможности восстановления") . '?',
            'deleteButtonLabel'  => Yii::t("AdminModule.messages", "Удалить данные без возможности восстановления"),
            'deleteButtonUrl'    => 'Yii::app()->controller->createUrl("delete", array("id" => $data->uid))',
            'header'             => CHtml::dropDownList(
                'pageSize',
                Yii::app()->user->getState('pageSize'),
                [10 => 10, 20 => 20, 50 => 50],
                [
                    'onchange' => "$.fn.yiiGridView.update('patient-grid',{ data:{pageSize: $(this).val() }})",
                    'style'    => 'width: 70px;'
                ]
            ),
        ],
    )
));
