<?php

return array(
    'sourcePath'  => YiiBase::getPathOfAlias("protocols"),
    'messagePath' => YiiBase::getPathOfAlias("protocols") . DIRECTORY_SEPARATOR . "messages",
    'languages'   => array('en','ru'),
    'fileTypes'   => array('php','phtml'),
    'exclude'     => array(),
    //'translator'  => '',
);