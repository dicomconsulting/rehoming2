<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 18.11.13
 * Time: 15:14
 *
 * @var Protocol[] $protocols
 * @var Protocol $protocol
 */
?>
<?php if (!$protocol->isOpened()): ?>
    <div class="row margin20">
        <div class="span6">
            <p><?= Yii::t('ProtocolsModule.protocols', "Закрытый протокол не может быть удален") ?></p>
        </div>
    </div>
<?php endif; ?>

<?php if ($protocols): ?>
    <div class="row margin20">
        <div class="span6">
            <p><?=
                Yii::t(
                    "protocols",
                    "Вы не можете сейчас удалить этот протокол, поскольку к этому исследованию уже заведены следующие протоколы"
                )?>:</p>
            <?php
            $i = 1;
            foreach ($protocols as $protocol):?>
                <?php
                if ($protocol->protocol_dictionary_uid == ProtocolDictionary::PROTOCOL_ENROLMENT
                    || $protocol->protocol_dictionary_uid == ProtocolDictionary::PROTOCOL_INITHM
                ) {
                    continue;
                }?>
                <?= $i++ ?>. <?= $protocol->protocolDictionary->name ?>, <?= $protocol->date ?><br/>
            <?php endforeach; ?>
            <?=
            Yii::t(
                "protocols",
                "Сначала необходимо удалить вышеперечисленные протоколы."
            )?></div>
    </div>
<?php endif;