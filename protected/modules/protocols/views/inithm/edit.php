<?php
/* @var $form CActiveForm */
/* @var $this EnrolmentController */
/* @var $protocol Protocol */
/* @var $research Research */
/* @var $initHm ProtocolInithm */

?>

<script type="text/javascript">
    //данные для зависимых полей
    var fieldsDependency = <?=CJSON::encode($initHm->getFieldsDependency()) ?>;
    var fieldsPrefix = "ProtocolInithm_";
</script>


<div class="form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'id'                     => 'protocol-protocol-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // See class documentation of CActiveForm for details on this,
            // you need to use the performAjaxValidation()-method described there.
            'enableAjaxValidation'   => true,
            'enableClientValidation' => true,
            'action'                 => Yii::app()->getRequest()->getOriginalUrl(),
            'htmlOptions'            => ['autocomplete' => 'off']
        )
    );
    ?>

    <div class="noprint">

    <?php echo $form->errorSummary(array($research, $protocol, $initHm)); ?>


    <h3><?php echo Yii::t('ProtocolsModule.protocols', "Инициализация Home Monitoring") ?></h3>

    <!--  ФОРМА ПРОТОКОЛА -->

    <div class="row" style="margin-top:20px;">
        <div class="span3">
                <?php echo $form->labelEx($protocol, 'date'); ?>
                <?php echo $form->dateField($protocol, 'date'); ?>
                <?php echo $form->error($protocol, 'date'); ?>
        </div>
        <div class="span3">
                <?php echo $form->labelEx($protocol, 'user_uid'); ?>
                <?php echo $form->hiddenField($protocol, 'user_uid'); ?>
                <p><?=$protocol->user->getDisplayName()?></p>
                <?php echo $form->error($protocol, 'user_uid'); ?>
        </div>
    </div>

    <!--  ФОРМА /ПРОТОКОЛА -->

    </div>

    <?php $this->widget("ProtocolIntro", array("protocol" => $protocol));?>

    <!--  ФОРМА ДАННЫХ ПРОТОКОЛА -->


	<div class="row margin20">
        <div class="span8">
        <?php echo $form->checkBox($initHm, 'patient_regged', array("class" => "inline")); ?>
		<?php echo $form->labelEx($initHm, 'patient_regged', array("class" => "inline")); ?>
		<?php echo $form->error($initHm, 'patient_regged'); ?>
	    </div>
	</div>

	<div class="row margin10">
        <div class="span8">
		<?php echo $form->labelEx($initHm, 'hm_options_changed'); ?>
		<?php echo $form->textField($initHm, 'hm_options_changed'); ?>
		<?php echo $form->error($initHm, 'hm_options_changed'); ?>
    	</div>
	</div>

    <div class="no_page_break">

	<div class="row margin10">
	    <div class="span8">
        <?php echo $form->checkBox($initHm, 'to_red', array("class" => "inline")); ?>
		<?php echo $form->labelEx($initHm, 'to_red', array("class" => "inline bold")); ?>
		<?php echo $form->error($initHm, 'to_red'); ?>
	    </div>
	</div>

	<div class="row margin10">
        <!--<div class="span3">
            <?php /*echo $form->labelEx($initHm, 'to_red_date'); */?>
            <?php /*echo $form->dateField($initHm, 'to_red_date'); */?>
            <?php /*echo $form->error($initHm, 'to_red_date'); */?>
        </div>-->

        <div class="span3">
            <?php echo $form->labelEx($initHm, 'to_red_descr'); ?>
            <?php echo $form->textArea($initHm, 'to_red_descr', array("style" => "width:350px", "rows" => 6)); ?>
            <?php echo $form->error($initHm, 'to_red_descr'); ?>
        </div>
	</div>

    </div>
    <div class="no_page_break">

	<div class="row">
	    <div class="span8">
        <?php echo $form->checkBox($initHm, 'from_red', array("class" => "inline")); ?>
		<?php echo $form->labelEx($initHm, 'from_red', array("class" => "inline bold")); ?>
		<?php echo $form->error($initHm, 'from_red'); ?>
	    </div>
	</div>

	<div class="row">
        <div class="span2">
            <?php echo $form->labelEx($initHm, 'from_red_status'); ?>
            <?php echo $form->dropDownList($initHm, 'from_red_status', ProtocolInithm::model()->getFromRedOptions(), array("class" => "input-medium")); ?>
            <?php echo $form->error($initHm, 'from_red_status'); ?>
        </div>

        <!--<div class="span2">
            <?php /*echo $form->labelEx($initHm, 'from_red_date'); */?>
            <?php /*echo $form->dateField($initHm, 'from_red_date', array("class" => "input-medium")); */?>
            <?php /*echo $form->error($initHm, 'from_red_date'); */?>
        </div>-->

        <div class="span2">
            <?php echo $form->labelEx($initHm, 'from_red_descr'); ?>
            <?php echo $form->textArea($initHm, 'from_red_descr', array("style" => "width:250px", "rows" => 6)); ?>
            <?php echo $form->error($initHm, 'from_red_descr'); ?>
        </div>
	</div>

    </div>

    <h5><?=Yii::t('ProtocolsModule.protocols', "Пожалуйста, приложите распечатку Home Monitoring \"Options\"!")?></h5>
	<div class="row">
	    <div class="span8">
		<?php echo $form->labelEx($initHm, 'comment', array("style" => "font-weight:bold;")); ?>
		<?php echo $form->textArea($initHm, 'comment', array("style" => "width:350px", "rows" => 6)); ?>
		<?php echo $form->error($initHm, 'comment'); ?>
	    </div>
	</div>

    <?php $this->widget("ProtocolSignature", array("protocol" => $protocol));?>

    <!--  /ФОРМА ДАННЫХ ПРОТОКОЛА -->

    <?php $this->widget("ProtocolFooterButtons", [
            "protocol" => $protocol,
            "research" => $research,
            "patient" => $research->patient,
        ]);?>

    <?php $this->endWidget();?>

</div><!-- form -->