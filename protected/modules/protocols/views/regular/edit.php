<?php
/* @var $form CActiveForm */
/* @var $this EnrolmentController */
/* @var $protocol Protocol */
/* @var $research Research */
/* @var $regular ProtocolRegular */
/* @var $enrolment ProtocolEnrolment */
/* @var $findingRedList array */


Yii::app()->clientScript->registerScriptFile(
    Yii::app()->assetManager->publish(
        Yii::getPathOfAlias('protocols.assets') . '/js/regular.js'
    ),
    CClientScript::POS_END
);
Yii::app()->clientScript->registerScriptFile(
    Yii::app()->assetManager->publish(
        'js/mustache.js'
    ),
    CClientScript::POS_END
);
Yii::app()->mustache->templatePathAlias = "protocols.views.regular";
?>
<script type="text/javascript">
    var redFindingTemplate = <?=CJSON::encode(Yii::app()->mustache->getTemplate("redFinding")); ?>;

    //данные для зависимых полей
    var fieldsDependency = <?=CJSON::encode($regular->getFieldsDependency()) ?>;
    var fieldsPrefix = "ProtocolRegular_";
</script>

<div class="form">

<?php $form = $this->beginWidget(
    'CActiveForm',
    array(
        'id'                     => 'protocol-protocol-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // See class documentation of CActiveForm for details on this,
        // you need to use the performAjaxValidation()-method described there.
        'enableAjaxValidation'   => true,
        'enableClientValidation' => false,
        'action'                 => Yii::app()->getRequest()->getOriginalUrl(),
        'htmlOptions'            => ['autocomplete' => 'off']
    )
);
?>

<div class="noprint">

<?php echo $form->errorSummary(array($research, $protocol, $regular)); ?>

<h3><?php echo Yii::t('ProtocolsModule.protocols', "Регулярное амбулаторное обследование") ?></h3>

<!--  ФОРМА ПРОТОКОЛА -->

<div class="row" style="margin-top:20px;">
    <div class="span3">
        <?php echo $form->labelEx($protocol, 'date'); ?>
        <?php echo $form->dateField($protocol, 'date'); ?>
        <?php echo $form->error($protocol, 'date'); ?>
    </div>
    <div class="span3">
        <?php echo $form->labelEx($protocol, 'user_uid'); ?>
        <?php echo $form->hiddenField($protocol, 'user_uid'); ?>
        <p><?=User::model()->findByPk($protocol->user_uid)->getDisplayName()?></p>
        <?php echo $form->error($protocol, 'user_uid'); ?>
    </div>
</div>

<!--  ФОРМА /ПРОТОКОЛА -->
</div>
<?php $this->widget("ProtocolIntro", array("protocol" => $protocol));?>

<div class="row margin20">
    <div class="span2">
        <strong><?=Yii::t('ProtocolsModule.protocols', "Имплантат")?>:</strong>
    </div>

    <div class="span1">
        <?php echo $form->checkBox($enrolment,'enrolment_pacer', array("class" => "inline", "disabled" => "disabled")); ?>
        <?php echo $form->labelEx($enrolment,'enrolment_pacer', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment,'enrolment_pacer'); ?>
    </div>

    <div class="span1">
        <?php echo $form->checkBox($enrolment,'enrolment_crt_p', array("class" => "inline", "disabled" => "disabled")); ?>
        <?php echo $form->labelEx($enrolment,'enrolment_crt_p', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment,'enrolment_crt_p'); ?>
    </div>

    <div class="span1">
        <?php echo $form->checkBox($enrolment,'enrolment_crt_d', array("class" => "inline", "disabled" => "disabled")); ?>
        <?php echo $form->labelEx($enrolment,'enrolment_crt_d', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment,'enrolment_crt_d'); ?>
    </div>

    <div class="span1">
        <?php echo $form->checkBox($enrolment,'enrolment_icd', array("class" => "inline", "disabled" => "disabled")); ?>
        <?php echo $form->labelEx($enrolment,'enrolment_icd', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment,'enrolment_icd'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span4">
        <strong><?=Yii::t('ProtocolsModule.protocols', "Дата включения пациента в исследование")?>:</strong>
    </div>

    <div class="span1">
        <?php echo $form->textField($research,'date_begin', array("class" => "inline input-small", "disabled" => "disabled")); ?>
    </div>
</div>

<!--  ФОРМА ДАННЫХ ПРОТОКОЛА -->
<div class="row margin20">
    <div class="span3">
        <?php echo $form->labelEx($regular, 'survey_period'); ?>
    </div>
    <div class="span3">
        <?php echo $form->dropDownList($regular, 'survey_period', ProtocolRegular::model()->getSurveyPeriods()); ?>
        <?php echo $form->error($regular, 'survey_period'); ?>
    </div>
</div>

<div class="row margin20">
    <div class="span3">
        <?php echo $form->labelEx($regular, 'visit_deflection'); ?>
    </div>
    <div class="span3">
        <?php echo $form->numberField($regular, 'visit_deflection'); ?>
        <?php echo $form->error($regular, 'visit_deflection'); ?>
    </div>
</div>

<div class="row margin20">
    <div class="span3">
        <?php echo $form->labelEx($regular, 'visit_missed'); ?>
    </div>
    <div class="span3">
        <?php echo $form->dropDownList(
            $regular,
            'visit_missed',
            ["" => Yii::t('ProtocolsModule.protocols', "Нет")] + ProtocolRegular::model()->getSurveyPeriods()
        ); ?>
        <?php echo $form->error($regular, 'visit_missed'); ?>
    </div>
</div>

<h5><?php echo Yii::t('ProtocolsModule.protocols', "Для пациентов ЭКД и/или системой CRT регулярное амбулаторное обследование должно проводиться каждые 3 месяца!") ?></h5>

<h4 class="margin20"><?php echo Yii::t('ProtocolsModule.protocols', "Посещение клиники") ?></h4>

<div class="row" style="margin-top:20px;">
    <div class="span2">
        <?php echo $form->labelEx($regular, 'clinic_distance'); ?>
    </div>
    <div class="span3">
        <?php echo $form->textField($regular, 'clinic_distance', ['class' => 'decimal_field']); ?>
        <?php echo $form->error($regular, 'clinic_distance'); ?>
    </div>
</div>

<div class="row" style="margin-top:10px;">
    <div class="span2">
        <?php echo $form->labelEx($regular, 'clinic_time'); ?>
    </div>
    <div class="span3">
        <?php echo $form->textField($regular, 'clinic_time', ['class' => 'decimal_field']); ?>
        <?php echo $form->error($regular, 'clinic_time'); ?>
    </div>
</div>

<h5><?=Yii::t('ProtocolsModule.protocols', "Пожалуйста, документируйте данное амбулаторное обследование распечатками с программатора!") ?></h5>
<h5><?=Yii::t('ProtocolsModule.protocols', 'Пожалуйста, документируйте все сообщения "red" (красный), "yellow" (желтый) Home Monitoring, а также все предпринятые вами действия с момента предыдущего амбулаторного обследования пациента!') ?></h5>
<h5><?=Yii::t('ProtocolsModule.protocols', 'Заполните формуляр нежелатательного явления, если необходимо!') ?></h5>
<h5><?=Yii::t('ProtocolsModule.protocols', 'Пожалуйста, документируйте изменения Home Monitoring "Опции", если необходимо!') ?></h5>


<h4 class="margin40"><?php echo Yii::t('ProtocolsModule.protocols', "Данные тренд-мониторинга имплантата") ?></h4>
<h5 class="margin40"><?php echo Yii::t('ProtocolsModule.protocols', 'Приложите анонимную копию распечатки данных мониторинга!') ?></h5>


<div class="row" style="margin-top:10px;">
    <div class="span2">
        <?php echo $form->labelEx($regular, 'stimul_mode'); ?>
    </div>
    <div class="span3">
        <?php echo $form->textField($regular, 'stimul_mode'); ?>
        <?php echo $form->error($regular, 'stimul_mode'); ?>
    </div>
</div>

<h4><?php echo Yii::t('ProtocolsModule.protocols', "Установки тахи (для пациентов с ИКД)") ?></h4>

<div class="row" style="margin-top:10px;">
    <div class="span2">
        <?php echo $form->labelEx($regular, 'idc_vt1'); ?>
    </div>
    <div class="span2">
        <?php echo $form->checkBox($regular, 'idc_vt1'); ?>
        <?php echo $form->error($regular, 'idc_vt1'); ?>
    </div>
    <div class="span1">
        <?php echo $form->textField($regular, 'idc_vt1_time', array("class" => "input-mini inline decimal_field")); ?>
        <?php echo $form->error($regular, 'idc_vt1_time'); ?>
    </div>
    <div class="span1">
        <?=Yii::t('ProtocolsModule.protocols', "мс")?>
    </div>
</div>

<div class="row" style="margin-top:10px;">
    <div class="span2">
        <?php echo $form->labelEx($regular, 'idc_vt2'); ?>
    </div>
    <div class="span2">
        <?php echo $form->checkBox($regular, 'idc_vt2'); ?>
        <?php echo $form->error($regular, 'idc_vt2'); ?>
    </div>
    <div class="span1">
        <?php echo $form->textField($regular, 'idc_vt2_time', array("class" => "input-mini inline decimal_field")); ?>
        <?php echo $form->error($regular, 'idc_vt2_time'); ?>
    </div>
    <div class="span1">
        <?=Yii::t('ProtocolsModule.protocols', "мс")?>
    </div>
</div>

<div class="row" style="margin-top:10px;">
    <div class="span2">
        <?php echo $form->labelEx($regular, 'idc_vf'); ?>
    </div>
    <div class="span2">
        <?php echo $form->checkBox($regular, 'idc_vf'); ?>
        <?php echo $form->error($regular, 'idc_vf'); ?>
    </div>
    <div class="span1">
        <?php echo $form->textField($regular, 'idc_vf_time', array("class" => "input-mini inline decimal_field")); ?>
        <?php echo $form->error($regular, 'idc_vf_time'); ?>
    </div>
    <div class="span1">
        <?=Yii::t('ProtocolsModule.protocols', "мс")?>
    </div>
</div>
<div class="no_page_break">

<h4><?php echo Yii::t('ProtocolsModule.protocols', "Предсердный электрод") ?></h4>

<div class="row" style="margin-top:10px;">
    <div class="span2">
        <?php echo $form->labelEx($regular, 'ae_pacing_threshold'); ?>
    </div>
    <div class="span2">
        <?php echo $form->textField($regular, 'ae_pacing_threshold', array("class" => "input-mini inline decimal_field")); ?>
        <?php echo $form->error($regular, 'ae_pacing_threshold'); ?>
    </div>
    <div class="span1">
        <?php echo $form->textField($regular, 'ae_pacing_threshold_dur', array("class" => "input-mini inline decimal_field")); ?>
        <?php echo $form->error($regular, 'ae_pacing_threshold_dur'); ?>
    </div>
    <div class="span1">
        <?=Yii::t('ProtocolsModule.protocols', "мс")?>
    </div>
</div>

<div class="row" style="margin-top:10px;">
    <div class="span2">
        <?php echo $form->labelEx($regular, 'ae_pulse_ampl'); ?>
    </div>
    <div class="span2">
        <?php echo $form->textField($regular, 'ae_pulse_ampl', array("class" => "input-mini inline decimal_field")); ?>
        <?php echo $form->error($regular, 'ae_pulse_ampl'); ?>
    </div>
</div>

<div class="row" style="margin-top:10px;">
    <div class="span2">
        <?php echo $form->labelEx($regular, 'ae_pacing_imp'); ?>
    </div>
    <div class="span2">
        <?php echo $form->textField($regular, 'ae_pacing_imp', array("class" => "input-mini inline")); ?>
        <?php echo $form->error($regular, 'ae_pacing_imp'); ?>
    </div>
</div>

</div>

<div class="no_page_break">

<h4><?php echo Yii::t('ProtocolsModule.protocols', "Желудочковый электрод") ?></h4>

<div class="row" style="margin-top:10px;">
    <div class="span2">
        <?php echo $form->labelEx($regular, 've_stimul_threshold'); ?>
    </div>
    <div class="span2">
        <?php echo $form->textField($regular, 've_stimul_threshold', array("class" => "input-mini inline decimal_field")); ?>
        <?php echo $form->error($regular, 've_stimul_threshold'); ?>
    </div>
    <div class="span1">
        <?php echo $form->textField($regular, 've_stimul_dur', array("class" => "input-mini inline decimal_field")); ?>
        <?php echo $form->error($regular, 've_stimul_dur'); ?>
    </div>
    <div class="span1">
        <?=Yii::t('ProtocolsModule.protocols', "мс")?>
    </div>
</div>

<div class="row" style="margin-top:10px;">
    <div class="span2">
        <?php echo $form->labelEx($regular, 've_wave_apl'); ?>
    </div>
    <div class="span2">
        <?php echo $form->textField($regular, 've_wave_apl', array("class" => "input-mini inline decimal_field")); ?>
        <?php echo $form->error($regular, 've_wave_apl'); ?>
    </div>
</div>


<div class="row" style="margin-top:10px;">
    <div class="span2">
        <?php echo $form->labelEx($regular, 've_imp'); ?>
    </div>
    <div class="span2">
        <?php echo $form->numberField($regular, 've_imp', array("class" => "input-mini inline")); ?>
        <?php echo $form->error($regular, 've_imp'); ?>
    </div>
</div>

<div class="row" style="margin-top:10px;">
    <div class="span2">
        <?php echo $form->labelEx($regular, 've_imp_shock'); ?>
    </div>
    <div class="span1">
        <?php echo $form->numberField($regular, 've_imp_shock', array("class" => "input-mini inline")); ?>
        <?php echo $form->error($regular, 've_imp_shock'); ?>
    </div>
    <div class="span2">
        <?=Yii::t('ProtocolsModule.protocols', "(для пациентов с ИКД)")?>
    </div>
</div>
</div>


<h4><?php echo Yii::t('ProtocolsModule.protocols', "Желудочковые аритмии") ?> <?=Yii::t('ProtocolsModule.protocols', "(для пациентов с ИКД)")?></h4>

<div class="row" style="margin-top:10px;">
    <div class="span2">
        <?=Yii::t('ProtocolsModule.protocols', "Количество")?>:
    </div>
    <div class="span2">
        <?php echo $form->numberField($regular, 'arr_vt1_cnt', array("class" => "input-mini inline")); ?>
        <?php echo $form->labelEx($regular, 'arr_vt1_cnt', array("class" => "inline")); ?>
        <?php echo $form->error($regular, 'arr_vt1_cnt'); ?>
    </div>
    <div class="span2">
        <?php echo $form->numberField($regular, 'arr_vt2_cnt', array("class" => "input-mini inline")); ?>
        <?php echo $form->labelEx($regular, 'arr_vt2_cnt', array("class" => "inline")); ?>
        <?php echo $form->error($regular, 'arr_vt2_cnt'); ?>
    </div>
    <div class="span2">
        <?php echo $form->numberField($regular, 'arr_vf_cnt', array("class" => "input-mini inline")); ?>
        <?php echo $form->labelEx($regular, 'arr_vf_cnt', array("class" => "inline")); ?>
        <?php echo $form->error($regular, 'arr_vf_cnt'); ?>
    </div>
</div>

<div class="row" style="margin-top:10px;">
    <div class="span2">
        <?=Yii::t('ProtocolsModule.protocols', "ATC начата/успешна")?>:
    </div>
    <div class="span4">
        <?php echo $form->numberField($regular, 'atc_began_vt', array("class" => "input-mini inline")); ?>
        <?php echo $form->error($regular, 'atc_began_vt'); ?>
        /
        <?php echo $form->numberField($regular, 'atc_compl_vt', array("class" => "input-mini inline")); ?>
        <?php echo $form->error($regular, 'atc_compl_vt'); ?>
        <?=Yii::t('ProtocolsModule.protocols', "ЖТ1/ЖТ2")?>
    </div>
    <div class="span3">
        <?php echo $form->numberField($regular, 'atc_began_vf', array("class" => "input-mini inline")); ?>
        <?php echo $form->error($regular, 'atc_began_vf'); ?>
        /
        <?php echo $form->numberField($regular, 'atc_compl_vf', array("class" => "input-mini inline")); ?>
        <?php echo $form->error($regular, 'atc_compl_vf'); ?>
        <?=Yii::t('ProtocolsModule.protocols', "ФЖ")?>
    </div>
</div>

<div class="row" style="margin-top:10px;">
    <div class="span3">
        <?=Yii::t('ProtocolsModule.protocols', "Шоков начато/отменено/успешно")?>:
    </div>
    <div class="span5">
        <?php echo $form->numberField($regular, 'shock_began', array("class" => "input-mini inline")); ?>
        <?php echo $form->error($regular, 'shock_began'); ?>
        /
        <?php echo $form->numberField($regular, 'shock_cancel', array("class" => "input-mini inline")); ?>
        <?php echo $form->error($regular, 'shock_cancel'); ?>
        /
        <?php echo $form->numberField($regular, 'shock_compl', array("class" => "input-mini inline")); ?>
        <?php echo $form->error($regular, 'shock_compl'); ?>
    </div>
</div>


<div class="row" style="margin-top:10px;">
    <div class="span3">
        <?php echo $form->labelEx($regular, 'svt_total_cnt', array("class" => "inline bold")); ?>
        <?php echo $form->numberField($regular, 'svt_total_cnt', array("class" => "input-mini inline")); ?>
        <?php echo $form->error($regular, 'svt_total_cnt'); ?>
    </div>
    <div class="span4">
        <?php echo $form->labelEx($regular, 'svt_12h_cnt', array("class" => "inline")); ?>
        <?php echo $form->numberField($regular, 'svt_12h_cnt', array("class" => "input-mini inline")); ?>
        <?php echo $form->error($regular, 'svt_12h_cnt'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span6">
        <?php echo $form->labelEx($regular, 'af_episode_time'); ?>
        <?php echo $form->error($regular, 'af_episode_time'); ?>
    </div>
</div>
<div class="row">
    <div class="span2">
        <?=Yii::t('ProtocolsModule.protocols', "дней");?>
        <?=CHtml::textField("af_episode_time[d]", ($regular->af_episode_time ? $regular->af_episode_time->d : ""), array("class" => "input-mini"))?>
    </div>
    <div class="span2">
        <?=Yii::t('ProtocolsModule.protocols', "часов");?>
        <?=CHtml::textField("af_episode_time[h]", ($regular->af_episode_time ? $regular->af_episode_time->h : ""), array("class" => "input-mini"))?>
    </div>
    <div class="span2">
        <?=Yii::t('ProtocolsModule.protocols', "минут");?>
        <?=CHtml::textField("af_episode_time[i]", ($regular->af_episode_time ? $regular->af_episode_time->i : ""), array("class" => "input-mini"))?>
    </div>
    <div class="span2">
        <?=Yii::t('ProtocolsModule.protocols', "секунд");?>
        <?=CHtml::textField("af_episode_time[i]", ($regular->af_episode_time ? $regular->af_episode_time->s : ""), array("class" => "input-mini"))?>
    </div>
</div>

<div class="row" style="margin-top:10px;">
    <div class="span3">
        <?php echo $form->labelEx($regular, 'af_episode_hr_max'); ?>
    </div>
    <div class="span4">
        <?php echo $form->numberField($regular, 'af_episode_hr_max', array("class" => "input-small")); ?>
        <?php echo $form->error($regular, 'af_episode_hr_max'); ?>
    </div>
</div>

<div class="row" style="margin-top:10px;">
    <div class="span3">
        <?php echo $form->labelEx($regular, 'af_episode_hr_avg'); ?>
    </div>
    <div class="span4">
        <?php echo $form->numberField($regular, 'af_episode_hr_avg', array("class" => "input-small")); ?>
        <?php echo $form->error($regular, 'af_episode_hr_avg'); ?>
    </div>
</div>

<div class="row" style="margin-top:20px;">
    <div class="span3">
        <?php echo $form->labelEx($regular, 'vent_contr_prop', array("class" => "bold")); ?>
    </div>
    <div class="span4">
        <?php echo $form->numberField($regular, 'vent_contr_prop'); ?>
        <?php echo $form->error($regular, 'vent_contr_prop'); ?>
    </div>
</div>

<h4><?php echo Yii::t('ProtocolsModule.protocols', "Физиологические параметры") ?></h4>

<div class="row" style="margin-top:20px;">
    <div class="span3">
        <?=Yii::t('ProtocolsModule.protocols', "Средний желудочковый ритм")?> (<?=Yii::t('ProtocolsModule.protocols', "ударов в минуту")?>):
    </div>
    <div class="span2">
        <?php echo $form->labelEx($regular, 'vent_rate_24'); ?>
        <?php echo $form->numberField($regular, 'vent_rate_24', array("class" => "input-mini")); ?>
        <?php echo $form->error($regular, 'vent_rate_24'); ?>
    </div>
    <div class="span3">
        <?php echo $form->labelEx($regular, 'vent_rate_fu'); ?>
        <?php echo $form->numberField($regular, 'vent_rate_fu', array("class" => "input-mini")); ?>
        <?php echo $form->error($regular, 'vent_rate_fu'); ?>
    </div>
</div>

<div class="row" style="margin-top:20px;">
    <div class="span3">
        <?=Yii::t('ProtocolsModule.protocols', "Средний желудочковый ритм в покое")?> (<?=Yii::t('ProtocolsModule.protocols', "ударов в минуту")?>):
    </div>
    <div class="span2">
        <?php echo $form->labelEx($regular, 'vent_rate_24_rest'); ?>
        <?php echo $form->numberField($regular, 'vent_rate_24_rest', array("class" => "input-mini")); ?>
        <?php echo $form->error($regular, 'vent_rate_24_rest'); ?>
    </div>
    <div class="span3">
        <?php echo $form->labelEx($regular, 'vent_rate_fu_rest'); ?>
        <?php echo $form->numberField($regular, 'vent_rate_fu_rest', array("class" => "input-mini")); ?>
        <?php echo $form->error($regular, 'vent_rate_fu_rest'); ?>
    </div>
</div>

<div class="row" style="margin-top:20px;">
    <div class="span3">
        <?=Yii::t('ProtocolsModule.protocols', "Активность пациента")?> (%):
    </div>
    <div class="span2">
        <?php echo $form->labelEx($regular, 'patient_act'); ?>
        <?php echo $form->numberField($regular, 'patient_act', array("class" => "input-mini")); ?>
        <?php echo $form->error($regular, 'patient_act'); ?>
    </div>
    <div class="span3">
        <?php echo $form->labelEx($regular, 'patient_act_fu'); ?>
        <?php echo $form->numberField($regular, 'patient_act_fu', array("class" => "input-mini")); ?>
        <?php echo $form->error($regular, 'patient_act_fu'); ?>
    </div>
</div>


<div class="row" style="margin-top:20px;">
    <div class="span3">
        <?=Yii::t('ProtocolsModule.protocols', "Количество желудочковых экстрасистол/час")?>:
    </div>
    <div class="span2">
        <?php echo $form->labelEx($regular, 'vent_contr_24'); ?>
        <?php echo $form->numberField($regular, 'vent_contr_24', array("class" => "input-mini")); ?>
        <?php echo $form->error($regular, 'vent_contr_24'); ?>
    </div>
    <div class="span3">
        <?php echo $form->labelEx($regular, 'vent_contr_fu'); ?>
        <?php echo $form->numberField($regular, 'vent_contr_fu', array("class" => "input-mini")); ?>
        <?php echo $form->error($regular, 'vent_contr_fu'); ?>
    </div>
</div>

<div class="no_page_break">

<div class="row" style="margin-top:40px;">
    <div class="span4">
        <?php echo $form->labelEx($regular, 'ekg_needed', array("class" => "bold")); ?>
    </div>
    <div class="span1">
        <?php echo $form->checkBox($regular, 'ekg_needed'); ?>
        <?php echo $form->error($regular, 'ekg_needed'); ?>
    </div>
</div>

<div class="row" style="margin-top:10px;">
    <div class="span4">
        <?php echo $form->labelEx($regular, 'ekg_supplied', array("class" => "bold")); ?>
    </div>
    <div class="span1">
        <?php echo $form->checkBox($regular, 'ekg_supplied'); ?>
        <?php echo $form->error($regular, 'ekg_supplied'); ?>
    </div>
</div>

<div class="row" style="margin-top:10px;">
    <div class="span4">
        <?php echo $form->labelEx($regular, 'program_change_needed', array("class" => "bold")); ?>
    </div>
    <div class="span1">
        <?php echo $form->checkBox($regular, 'program_change_needed'); ?>
        <?php echo $form->error($regular, 'program_change_needed'); ?>
    </div>
</div>

<div class="row" style="margin-top:10px;">
    <div class="span4">
        <?php echo $form->labelEx($regular, 'drugs_change_needed', array("class" => "bold")); ?>
    </div>
    <div class="span1">
        <?php echo $form->checkBox($regular, 'drugs_change_needed'); ?>
        <?php echo $form->error($regular, 'drugs_change_needed'); ?>
    </div>
</div>

<div class="row" style="margin-top:10px;">
    <div class="span4">
        <?php echo $form->labelEx($regular, 'program_changes', array("class" => "bold")); ?>
    </div>
</div>
<div class="row">
    <div class="span1">
        <?php echo $form->textArea($regular, 'program_changes', array("rows" => 10, "class" => "input-xxlarge")); ?>
        <?php echo $form->error($regular, 'program_changes'); ?>
    </div>
</div>
</div>

<div class="no_page_break">

<h3 class="margin30"><?php echo Yii::t('ProtocolsModule.protocols', "События, выявленные с помощью системы Home&nbsp;Monitoring") ?></h3>

<h5><?php echo Yii::t('ProtocolsModule.protocols', "Повреждение или смещение электрода") ?></h5>

<div class="row" style="margin-top:10px;">
    <div class="span4">
        <?php echo $form->labelEx($regular, 'elect_displ_detect_date'); ?>
    </div>
    <div class="span1">
        <?php echo $form->dateField($regular, 'elect_displ_detect_date'); ?>
        <?php echo $form->error($regular, 'elect_displ_detect_date'); ?>
    </div>
</div>

<div class="row">
    <div class="span4">
        <?php echo $form->labelEx($regular, 'elect_displ_hosp_date'); ?>
    </div>
    <div class="span1">
        <?php echo $form->dateField($regular, 'elect_displ_hosp_date'); ?>
        <?php echo $form->error($regular, 'elect_displ_hosp_date'); ?>
    </div>
</div>

<div class="row">
    <div class="span4">
        <?php echo $form->labelEx($regular, 'elect_displ_hosp_emerg'); ?>
    </div>
    <div class="span1">
        <?php echo $form->checkBox($regular, 'elect_displ_hosp_emerg'); ?>
        <?php echo $form->error($regular, 'elect_displ_hosp_emerg'); ?>
    </div>
</div>

<h5><?php echo Yii::t('ProtocolsModule.protocols', "Нарастание признаков сердечной недостаточности, требующих госпитализацию пациента") ?></h5>

<div class="row" style="margin-top:10px;">
    <div class="span4">
        <?php echo $form->labelEx($regular, 'hf_detect_date'); ?>
    </div>
    <div class="span1">
        <?php echo $form->dateField($regular, 'hf_detect_date'); ?>
        <?php echo $form->error($regular, 'hf_detect_date'); ?>
    </div>
</div>

<div class="row">
    <div class="span4">
        <?php echo $form->labelEx($regular, 'hf_hosp_date'); ?>
    </div>
    <div class="span1">
        <?php echo $form->dateField($regular, 'hf_hosp_date'); ?>
        <?php echo $form->error($regular, 'hf_hosp_date'); ?>
    </div>
</div>

<div class="row">
    <div class="span4">
        <?php echo $form->labelEx($regular, 'hf_hosp_emerg'); ?>
    </div>
    <div class="span1">
        <?php echo $form->checkBox($regular, 'hf_hosp_emerg'); ?>
        <?php echo $form->error($regular, 'hf_hosp_emerg'); ?>
    </div>
</div>

<h5><?php echo Yii::t('ProtocolsModule.protocols', "Неадекватные срабатывания ИКД, требующие перепрограммирования аппарата") ?></h5>

<div class="row" style="margin-top:10px;">
    <div class="span4">
        <?php echo $form->labelEx($regular, 'idc_fail_detect_date'); ?>
    </div>
    <div class="span1">
        <?php echo $form->dateField($regular, 'idc_fail_detect_date'); ?>
        <?php echo $form->error($regular, 'idc_fail_detect_date'); ?>
    </div>
</div>

<div class="row">
    <div class="span4">
        <?php echo $form->labelEx($regular, 'idc_fail_hosp_date'); ?>
    </div>
    <div class="span1">
        <?php echo $form->dateField($regular, 'idc_fail_hosp_date'); ?>
        <?php echo $form->error($regular, 'idc_fail_hosp_date'); ?>
    </div>
</div>

<div class="row">
    <div class="span4">
        <?php echo $form->labelEx($regular, 'idc_fail_hosp_emerg'); ?>
    </div>
    <div class="span1">
        <?php echo $form->checkBox($regular, 'idc_fail_hosp_emerg'); ?>
        <?php echo $form->error($regular, 'idc_fail_hosp_emerg'); ?>
    </div>
</div>
</div>


<?php if (!empty($findingRedList)): ?>
    <h3 class="margin30"><?php echo Yii::t(
            'ProtocolsModule.protocols',
            "Описание сообщений red (красный) Home Monitoring"
        ) ?></h3>

    <script><!--
        var redFindings = <?php if (!empty($findingRedList)) echo Message::encodeFindings($findingRedList); else echo "{}";?>;
        //--></script>


    <?php echo CHtml::dropDownList("redFindingSelector", 0, [Yii::t('ProtocolsModule.protocols', "Выберите событие...")] + Protocol::filterAndFormatFindingList($regular, $findingRedList)); ?>
    <a href="#" id="add_red_finding"><?=Yii::t('ProtocolsModule.protocols', "Добавить") ?></a>


    <div id="redFindingsContainer">
        <?php if ($regular->finding):
            $i = 0;
            foreach ($regular->finding as $val): ?>

                <?php
                /* @var Message $finding */
                $currFinding = null;
                foreach ($findingRedList as $finding) {
                    if ($val->finding_uid == $finding->getUid()) {
                        $currFinding = $finding;
                    }
                };

                if (!$currFinding) {
                    continue;
                }

                $i++;

                Yii::app()->mustache->render("redFinding", array(
                        "finding" => $currFinding->formatMessage(true),
                        "findingId" => $val->finding_uid,
                        "create_time" => $currFinding->data->date_create,
                        "action" => $val->action,
                        "action_date" => $val->action_date,
                        "clinic_visit" => $val->clinic_visit,
                        "clinic_distance" => $val->clinic_distance,
                        "clinic_time" => $val->clinic_time,
                        "counter" => $i,
                    ));
                ?>

            <?php endforeach; ?>
        <?php endif; ?>
    </div>

<?php endif; ?>

<div class="no_page_break">

<h3 class="margin30"><?php echo Yii::t('ProtocolsModule.protocols', "Изменения опций Home Monitoring") ?></h3>

<div class="row margin30">
    <div class="span8">
        <?php echo $form->checkBox($regular, 'option_changed', array("class" => "inline")); ?>
        <?php echo $form->labelEx($regular, 'option_changed', array("class" => "inline bold")); ?>
        <?php echo $form->error($regular, 'option_changed'); ?>
    </div>
</div>

<div class="row">
    <div class="span5">
        <?php echo $form->labelEx($regular, 'option_changed_new_value'); ?>
        <?php echo $form->textArea($regular, 'option_changed_new_value', array("style" => "width:350px", "rows" => 6)); ?>
        <?php echo $form->error($regular, 'option_changed_new_value'); ?>
    </div>

    <div class="span4">
        <?php echo $form->labelEx($regular, 'option_changed_reason'); ?>
        <?php echo $form->textArea($regular, 'option_changed_reason', array("style" => "width:350px", "rows" => 6)); ?>
        <?php echo $form->error($regular, 'option_changed_reason'); ?>
    </div>
</div>

<div class="row margin20">
    <div class="span8">
        <?php echo $form->checkBox($regular, 'red_finding', array("class" => "inline")); ?>
        <?php echo $form->labelEx($regular, 'red_finding', array("class" => "inline bold")); ?>
        <?php echo $form->error($regular, 'red_finding'); ?>
    </div>
</div>

<div class="row">
    <div class="span5">
        <?php echo $form->labelEx($regular, 'red_finding_date'); ?>
        <?php echo $form->dateField($regular, 'red_finding_date'); ?>
        <?php echo $form->error($regular, 'red_finding_date'); ?>
    </div>

    <div class="span4">
        <?php echo $form->labelEx($regular, 'red_finding_message'); ?>
        <?php echo $form->textArea($regular, 'red_finding_message', array("style" => "width:350px", "rows" => 6)); ?>
        <?php echo $form->error($regular, 'red_finding_message'); ?>
    </div>
</div>

<div class="row margin20">
    <div class="span8">
        <?php echo $form->checkBox($regular, 'red_finding_changed', array("class" => "inline")); ?>
        <?php echo $form->labelEx($regular, 'red_finding_changed', array("class" => "inline bold")); ?>
        <?php echo $form->error($regular, 'red_finding_changed'); ?>
    </div>
</div>

<div class="row">
    <div class="span5">
        <?php echo $form->labelEx($regular, 'red_finding_changed_date'); ?>
        <?php echo $form->dateField($regular, 'red_finding_changed_date'); ?>
        <?php echo $form->error($regular, 'red_finding_changed_date'); ?>
    </div>

    <div class="span4">
        <?php echo $form->labelEx($regular, 'red_finding_changed_to'); ?>
        <?php echo $form->dropDownList($regular, 'red_finding_changed_to', Protocol::getFromRedOptions()); ?>
        <?php echo $form->error($regular, 'red_finding_changed_to'); ?>
    </div>
</div>

<div class="row">
    <div class="span5">
        <?php echo $form->labelEx($regular, 'red_finding_changed_message'); ?>
        <?php echo $form->textArea($regular, 'red_finding_changed_message', array("style" => "width:350px", "rows" => 6)); ?>
        <?php echo $form->error($regular, 'red_finding_changed_message'); ?>
    </div>

    <div class="span4">
        <?php echo $form->labelEx($regular, 'red_finding_changed_comment'); ?>
        <?php echo $form->textArea($regular, 'red_finding_changed_comment', array("style" => "width:350px", "rows" => 6)); ?>
        <?php echo $form->error($regular, 'red_finding_changed_comment'); ?>
    </div>
</div>
</div>

<div class="row margin20">
    <div class="span8">
        <?php echo $form->labelEx($regular, 'comment', array("class" => "bold")); ?>
        <?php echo $form->textArea($regular, 'comment', array("rows" => 14, "class" => "input-xxlarge")); ?>
        <?php echo $form->error($regular, 'comment'); ?>
    </div>
</div>

<!--  /ФОРМА ДАННЫХ ПРОТОКОЛА -->
<?php $this->widget("ProtocolSignature", array("protocol" => $protocol));?>

<?php $this->widget("ProtocolFooterButtons", [
        "protocol" => $protocol,
        "research" => $research,
        "patient" => $research->patient,
    ]);?>

<?php $this->endWidget(); ?>

</div><!-- form -->