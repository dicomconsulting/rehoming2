<?php
/* @var $form CActiveForm */
/* @var $this RegularController */
/* @var $protocol Protocol */
/* @var $research Research */

?>
<?php if(Yii::app()->user->checkAccess("editProtocols")):?>
<div style="text-align: center;" class="margin30">
    <?php if($canCreateNew):?>
        <?=CHtml::link(Yii::t("ProtocolsModule.protocols", "Завести протокол") . " \"" . $this->getProtocolName() . "\"", $this->createUrl($this->id . "/edit", ["id" => $patientId]));?>
    <?php else:?>
        <?=Yii::t("ProtocolsModule.protocols", "Для создания протокола \"{protocol_name}\" необходимо заполнить и подписать протоколы \"Включения пациента в исследование\" и \"Инициализация Home Monitoring\"", ['{protocol_name}' => $this->getProtocolName()]);?>
    <?php endif;?>
</div>
<?php endif;?>
<script type="text/javascript">
    window.protocolName = '<?=$this->getProtocolName();?>';
</script>

<?php if($protocols):?>
<div class="rh_table_grid">
    <table class="items">
        <tr>
            <th style="width: 100px;"><?= Yii::t('ProtocolsModule.protocols', 'Дата')?></th>
            <th><?=Yii::t('ProtocolsModule.protocols', "Мед. центр") ?></th>
            <th title="<?= Yii::t('ProtocolsModule.protocols', 'Инициалы')?>"><?= Yii::t('ProtocolsModule.protocols', 'Иниц.')?></th>
            <th style="width: 150px;"><?= Yii::t('ProtocolsModule.protocols', 'Номер скрининга')?></th>
            <th style="width: 150px;"><?= Yii::t('ProtocolsModule.protocols', 'Исследователь')?></th>
            <th><?=Yii::t('ProtocolsModule.protocols', "Статус") ?></th>
            <th style="width: 200px;"><?= Yii::t('ProtocolsModule.protocols', 'Действия')?></th>
        </tr>
        <?php foreach ($protocols as $row):?>
            <tr style="text-align: center;<?php if (!$row->research->isOpened()): ?>background-color: #d3d3d3;<?php elseif($row->isOpened()):?>background-color: #d3a196;<?php else:?>background-color: #b2d6ff;<?php endif;?>">
                <td class="date_create_cell"><?=$row->date->format(RhDateTime::RH_DATE); ?></td>
                <td><?=$row->research->medicalCenter->code; ?></td>
                <td><?=$row->research->patient->getCode(); ?></td>
                <td><?=$row->research->getScreeningNumber(); ?></td>
                <td><?=$row->user->getDisplayName(); ?></td>
                <td><?php if($row->status == Protocol::STATUS_OPENED):?><span class="ui-icon ui-icon-unlocked"></span><?php else:?><span class="ui-icon ui-icon-locked"><?php endif;?></td>
                <td><?php if ($row->isOpened() && $row->research->isOpened() && Yii::app()->user->checkAccess("editProtocols")): ?>
                        <?=CHtml::link(Yii::t('ProtocolsModule.protocols', "Редактировать"), $this->createUrl($this->id . "/edit", array("id" => $row->research->patient_uid, "protocolId" => $row->uid)))?>
                        | <?=CHtml::link(Yii::t('ProtocolsModule.protocols', "Удалить"), $this->createUrl($this->id . "/delete", array("id" => $row->research->patient_uid, "protocolId" => $row->uid)), array('class' => 'deleteProtocolLink'))?>
                        |
                    <?php elseif(!$row->isOpened() || !Yii::app()->user->checkAccess("editProtocols")):?>
                        <?=CHtml::link(Yii::t('ProtocolsModule.protocols', "Посмотреть"), $this->createUrl($this->id . "/edit", array("id" => $row->research->patient_uid, "protocolId" => $row->uid)))?>
                        |
                    <?php endif;?>
                    <?=CHtml::link(Yii::t('ProtocolsModule.protocols', "Печать"), $this->createUrl($this->id . "/print", array("id" => $row->research->patient_uid, "protocolId" => $row->uid)))?>
                    | <?=CHtml::link(Yii::t('ProtocolsModule.protocols', "История"), $this->createUrl("/" . $this->module->id . "/history", array("id" => $row->research->patient_uid, "protocolId" => $row->uid)))?>
                </td>
    </tr>
<?php endforeach;?>
</table>
<?php endif;?>
</div>