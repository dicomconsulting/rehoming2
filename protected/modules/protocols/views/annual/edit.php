<?php
/* @var $form CActiveForm */
/* @var $this AnnualController */
/* @var $protocol Protocol */
/* @var $research Research */
/* @var $annual ProtocolAnnual */

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->assetManager->publish(
        Yii::getPathOfAlias('protocols.assets').'/js/annual.js'
    ),
    CClientScript::POS_END
);

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->assetManager->publish(
        'js/mustache.js'
    ),
    CClientScript::POS_END
);

Yii::app()->mustache->templatePathAlias = "protocols.views.annual";
?>

<script type="text/javascript">
    //данные для зависимых полей
    var fieldsDependency = <?=CJSON::encode($annual->getFieldsDependency()) ?>;
    var fieldsPrefix = "ProtocolAnnual_";
    var deseaseRelTemplate = <?=CJSON::encode(Yii::app()->mustache->getTemplate("deseaseRel")); ?>;
</script>

<div class="form">

<?php $form = $this->beginWidget(
    'CActiveForm',
    array(
        'id'                     => 'protocol-protocol-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // See class documentation of CActiveForm for details on this,
        // you need to use the performAjaxValidation()-method described there.
        'enableAjaxValidation'   => true,
        'enableClientValidation' => true,
        'action'                 => Yii::app()->getRequest()->getOriginalUrl(),
        'htmlOptions'            => ['autocomplete' => 'off']
    )
);
?>

<?php
$this->widget(
    'directory.widgets.DiseaseSelector',
    ['patient' => $this->patient]
);
?>

<div class="noprint">

<?php echo $form->errorSummary(array($research, $protocol, $annual)); ?>

<h3><?php echo Yii::t('ProtocolsModule.protocols', "Обследование через 12 месяцев ") ?></h3>

<!--  ФОРМА ПРОТОКОЛА -->

<div class="row" style="margin-top:20px;">
    <div class="span3">
        <?php echo $form->labelEx($protocol, 'date'); ?>
        <?php echo $form->dateField($protocol, 'date'); ?>
        <?php echo $form->error($protocol, 'date'); ?>
    </div>
    <div class="span3">
        <?php echo $form->labelEx($protocol, 'user_uid'); ?>
        <p><?=User::model()->findByPk($protocol->user_uid)->getDisplayName()?></p>
        <?php echo $form->error($protocol, 'user_uid'); ?>
    </div>
</div>

<!--  ФОРМА /ПРОТОКОЛА -->
</div>
<?php $this->widget("ProtocolIntro", array("protocol" => $protocol));?>

<h5><?=Yii::t(
    "protocols",
    "Пожалуйста, документируйте обследование, выполненное через 12 месяцев после включения " .
    "пациента в исследование (заключительное)!"
)?></h5>
<h5><?=Yii::t(
    "protocols",
    "Для обследований, выполненных в промежутке между включением пациента в исследование и заключительным, заполните " .
    "формуляр нежелательного явления (Adverse Event Form), если необходимо!"
)?></h5>
<h5><?=Yii::t('ProtocolsModule.protocols', "Если пациент потерян для исследования - заполните форму окончания исследования!")?></h5>


<!--  ФОРМА ДАННЫХ ПРОТОКОЛА -->

<h3><?=Yii::t('ProtocolsModule.protocols', "Общий результат")?></h3>

<div class="row margin20">
    <div class="span7">
    <?php echo $form->labelEx($annual, 'findings_cnt'); ?>
    <?php echo $form->dropDownList($annual,'findings_cnt', ProtocolAnnual::model()->getPossibleFindingsCnt()); ?>
    <?php echo $form->error($annual, 'findings_cnt'); ?>
    </div>
</div>

<h5><?=Yii::t('ProtocolsModule.protocols', "Пожалуйста, для каждого нежелательного явления заполните формуляр (Adverse Event Form)!")?></h5>

<h3><?=Yii::t('ProtocolsModule.protocols', "Предпринятые действия")?></h3>

<div class="row">
    <div class="span3">
        <?php echo $form->checkBox($annual, 'medical_therapy_changed', array("class" => "inline") ); ?>
        <?php echo $form->labelEx($annual, 'medical_therapy_changed', array("class" => "inline")); ?>
        <?php echo $form->error($annual, 'medical_therapy_changed'); ?>
    </div>

    <div class="span5">
        <?php echo $form->labelEx($annual, 'medical_therapy_comment'); ?>
        <?php echo $form->textArea($annual, 'medical_therapy_comment', array("rows" => 5, "class" => "input-large")); ?>
        <?php echo $form->error($annual, 'medical_therapy_comment'); ?>
    </div>
</div>

<div class="row">
    <div class="span3">
        <?php echo $form->checkBox($annual, 'other_therapy_changed', array("class" => "inline")); ?>
        <?php echo $form->labelEx($annual, 'other_therapy_changed', array("class" => "inline")); ?>
        <?php echo $form->error($annual, 'other_therapy_changed'); ?>
    </div>

    <div class="span5">
        <?php echo $form->labelEx($annual, 'other_therapy_comment'); ?>
        <?php echo $form->textArea($annual, 'other_therapy_comment', array("rows" => 5, "class" => "input-large")); ?>
        <?php echo $form->error($annual, 'other_therapy_comment'); ?>
    </div>
</div>

<h3><?=Yii::t('ProtocolsModule.protocols', "Наблюдение")?></h3>

<h4><?=Yii::t('ProtocolsModule.protocols', "Статус прибора")?></h4>

<div class="row">
    <div class="span2">
        <?=Yii::t('ProtocolsModule.protocols', "Ожидаемый ERI")?>
    </div>
    <div class="span2">
        <?php echo $form->numberField($annual, 'estimated_eri_yesars', array("class" => "input-small")); ?>
        <?php echo $form->labelEx($annual, 'estimated_eri_yesars', array("class" => "inline")); ?>
        <?php echo $form->error($annual, 'estimated_eri_yesars'); ?>
    </div>

    <div class="span3">
        <?php echo $form->numberField($annual, 'estimated_eri_months', array("class" => "input-small")); ?>
        <?php echo $form->labelEx($annual, 'estimated_eri_months', array("class" => "inline")); ?>
        <?php echo $form->error($annual, 'estimated_eri_months'); ?>
    </div>
</div>

<div class="no_page_break">

<h4><?=Yii::t('ProtocolsModule.protocols', "Диагностика (заполните, если применимо)")?></h4>

<div class="row">
    <div class="span3">
        <?php echo $form->labelEx($annual, 'atrial_arrhythmic_burden', array("class" => "inline")); ?>
    </div>
    <div class="span3">
        <?php echo $form->numberField($annual, 'atrial_arrhythmic_burden'); ?>
        <?php echo $form->error($annual, 'atrial_arrhythmic_burden'); ?>
    </div>
</div>

<div class="row">
<div class="span3">
    <?php echo $form->labelEx($annual, 'pacing_in_a', array("class" => "inline")); ?>
</div>
<div class="span3">
    <?php echo $form->numberField($annual, 'pacing_in_a'); ?>
    <?php echo $form->error($annual, 'pacing_in_a'); ?>
</div>
</div>

<div class="row">
<div class="span3">
    <?php echo $form->labelEx($annual, 'pacing_in_b', array("class" => "inline")); ?>
</div>
<div class="span3">
    <?php echo $form->numberField($annual, 'pacing_in_b'); ?>
    <?php echo $form->error($annual, 'pacing_in_b'); ?>
</div>
</div>
<h5><?=Yii::t('ProtocolsModule.protocols', 'Пожалуйста, распечатайте "История"')?></h5>
</div>

<div class="no_page_break">

<h4><?=Yii::t('ProtocolsModule.protocols', "Предсердно-желудочковые события - только для 2-хкамерной стимуляции")?></h4>

<div class="row">
<div class="span3">
    <?php echo $form->labelEx($annual, 'as_vs'); ?>
</div>
<div class="span3">
    <?php echo $form->numberField($annual, 'as_vs'); ?>
    <?php echo $form->error($annual, 'as_vs'); ?>
</div>
</div>

<div class="row">
<div class="span3">
    <?php echo $form->labelEx($annual, 'as_vp'); ?>
</div>
<div class="span3">
    <?php echo $form->numberField($annual, 'as_vp'); ?>
    <?php echo $form->error($annual, 'as_vp'); ?>
</div>
</div>

<div class="row">
<div class="span3">
    <?php echo $form->labelEx($annual, 'ap_vp'); ?>
</div>
<div class="span3">
    <?php echo $form->numberField($annual, 'ap_vp'); ?>
    <?php echo $form->error($annual, 'ap_vp'); ?>
</div>
</div>

<div class="row">
<div class="span3">
    <?php echo $form->labelEx($annual, 'ap_vs'); ?>
</div>
<div class="span3">
    <?php echo $form->numberField($annual, 'ap_vs'); ?>
    <?php echo $form->error($annual, 'ap_vs'); ?>
</div>
</div>

</div>

<h3><?=Yii::t('ProtocolsModule.protocols', "Параметры (установки до 12-месячного обследования)")?></h3>
<h4><?=Yii::t('ProtocolsModule.protocols','Подавление нежелательной желудочковой стимуляции')?></h4>

<div class="row">
<div class="span3"><?=Yii::t('ProtocolsModule.protocols', "Режим стимуляции")?></div>
<div class="span6">
<div class="row">
    <?php echo $form->checkBox($annual, 'stimul_ddd_adi', array("class" => "inline")); ?>
    <?php echo $form->labelEx($annual, 'stimul_ddd_adi', array("class" => "inline")); ?>
    <?php echo $form->error($annual, 'stimul_ddd_adi'); ?>
</div>

<div class="row">
    <?php echo $form->checkBox($annual, 'stimul_irs_on', array("class" => "inline")); ?>
    <?php echo $form->labelEx($annual, 'stimul_irs_on', array("class" => "inline")); ?>
    <?php echo $form->error($annual, 'stimul_irs_on'); ?>
</div>

<div class="row">
    <?php echo $form->checkBox($annual, 'stimul_irs_off', array("class" => "inline")); ?>
    <?php echo $form->labelEx($annual, 'stimul_irs_off', array("class" => "inline")); ?>
    <?php echo $form->error($annual, 'stimul_irs_off'); ?>
</div>
</div>
</div>

<div class="no_page_break">

    <h4><?=Yii::t('ProtocolsModule.protocols', "Контроль захвата и чувствительность (заполните, если применимо)")?></h4>

<div class="row">
    <div class="span3">
    <?php echo $form->labelEx($annual, 'capt_ctrl_a', array("class" => "inline")); ?>
    </div>
    <div class="span3">
    <?php
        CaptureControl::enableOnlyActive();
        echo $form->dropDownList($annual,'capt_ctrl_a', ProtocolAnnual::model()->getPossibleCaptureControl());
    ?>
    <?php echo $form->error($annual, 'capt_ctrl_a'); ?>
    </div>
</div>

<div class="row">
<div class="span3">
    <?php echo $form->labelEx($annual, 'capt_ctrl_v', array("class" => "inline")); ?>
</div>
<div class="span3">
    <?php
        CaptureControl::enableOnlyActive();
        echo $form->dropDownList($annual,'capt_ctrl_v', ProtocolAnnual::model()->getPossibleCaptureControl());
    ?>
    <?php echo $form->error($annual, 'capt_ctrl_v'); ?>
</div>
</div>

<div class="row">
<div class="span3">
    <?php echo $form->labelEx($annual, 'sens_a', array("class" => "inline")); ?>
</div>
<div class="span3">
    <?php echo $form->dropDownList($annual,'sens_a', ProtocolAnnual::model()->getPossibleSensivityValues()); ?>
    <?php echo $form->error($annual, 'sens_a'); ?>
</div>
</div>

<div class="row">
<div class="span3">
    <?php echo $form->labelEx($annual, 'sens_v', array("class" => "inline")); ?>
</div>
<div class="span3">
    <?php echo $form->dropDownList($annual,'sens_v', ProtocolAnnual::model()->getPossibleSensivityValues()); ?>
    <?php echo $form->error($annual, 'sens_v'); ?>
</div>
</div>

<h5><?=Yii::t('ProtocolsModule.protocols', "Пожалуйста, приложите распечатки начальных и финальных установок!")?></h5>

</div>

<h3><?=Yii::t('ProtocolsModule.protocols', "Измерения")?></h3>

<h5><?=Yii::t('ProtocolsModule.protocols', "Пожалуйста, заполните результаты измерений, если применимо!")?></h5>

<div class="row">
    <div class="span3">&nbsp;</div>
    <div class="span3"><strong><?=Yii::t('ProtocolsModule.protocols', "Предсердие")?></strong></div>
    <div class="span3"><strong><?=Yii::t('ProtocolsModule.protocols', "Желудочек")?></strong></div>
</div>

<div class="row margin10">
    <div class="span3"><?php echo $form->labelEx($annual, 'atr_conf'); ?></div>
    <div class="span3">
        <?php echo $form->dropDownList($annual,'atr_conf', ProtocolAnnual::model()->getImplConfOptions()); ?>
        <?php echo $form->error($annual, 'atr_conf'); ?>
    </div>
    <div class="span3">
        <?php echo $form->dropDownList($annual,'ven_conf', ProtocolAnnual::model()->getImplConfOptions()); ?>
        <?php echo $form->error($annual, 'ven_conf'); ?>
    </div>
</div>
<div class="no_page_break">

    <h5><?=Yii::t('ProtocolsModule.protocols', "Опрос импланта")?></h5>
<div class="row margin10">
    <div class="span3"><?php echo $form->labelEx($annual, 'req_atr_ampl'); ?></div>
    <div class="span3">
        <?php echo $form->textField($annual, 'req_atr_ampl', ['class' => 'decimal_field']); ?>
        <?php echo $form->error($annual, 'req_atr_ampl'); ?>
    </div>
    <div class="span3">
        <?php echo $form->textField($annual, 'req_ven_ampl', ['class' => 'decimal_field']); ?>
        <?php echo $form->error($annual, 'req_ven_ampl'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3"><?php echo $form->labelEx($annual, 'req_atr_stimul_thresh'); ?></div>
    <div class="span3">
        <?php echo $form->textField($annual, 'req_atr_stimul_thresh'); ?>
        <?php echo $form->error($annual, 'req_atr_stimul_thresh'); ?>
    </div>
    <div class="span3">
        <?php echo $form->textField($annual, 'req_ven_stimul_thresh'); ?>
        <?php echo $form->error($annual, 'req_ven_stimul_thresh'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3"><?php echo $form->labelEx($annual, 'req_atr_el_imp'); ?></div>
    <div class="span3">
        <?php echo $form->numberField($annual, 'req_atr_el_imp'); ?>
        <?php echo $form->error($annual, 'req_atr_el_imp'); ?>
    </div>
    <div class="span3">
        <?php echo $form->numberField($annual, 'req_ven_el_imp'); ?>
        <?php echo $form->error($annual, 'req_ven_el_imp'); ?>
    </div>
</div>


<div class="row margin10">
    <div class="span3"><?php echo $form->labelEx($annual, 'req_ven_shock_imp'); ?></div>
    <div class="span3">
        &nbsp;
    </div>
    <div class="span3">
        <?php echo $form->numberField($annual, 'req_ven_shock_imp'); ?>
        <?php echo $form->error($annual, 'req_ven_shock_imp'); ?>
    </div>
</div>
</div>

<div class="no_page_break">

    <h5><?=Yii::t('ProtocolsModule.protocols', "Автоматические тесты")?></h5>

<div class="row margin10">
    <div class="span3"><?php echo $form->labelEx($annual, 'atest_atr_ampl'); ?></div>
    <div class="span3">
        <?php echo $form->textField($annual, 'atest_atr_ampl', ['class' => 'decimal_field']); ?>
        <?php echo $form->error($annual, 'atest_atr_ampl'); ?>
    </div>
    <div class="span3">
        <?php echo $form->textField($annual, 'atest_ven_ampl', ['class' => 'decimal_field']); ?>
        <?php echo $form->error($annual, 'atest_ven_ampl'); ?>
    </div>
</div>


<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($annual, 'atest_atr_stimul_thresh'); ?>
    </div>
    <div class="span3">
        <?php echo $form->textField($annual, 'atest_atr_stimul_thresh'); ?>
        <?php echo $form->error($annual, 'atest_atr_stimul_thresh'); ?>
    </div>
    <div class="span3">
        <?php echo $form->textField($annual, 'atest_ven_stimul_thresh'); ?>
        <?php echo $form->error($annual, 'atest_ven_stimul_thresh'); ?>
    </div>
</div>


<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($annual, 'atest_atr_el_imp'); ?>
    </div>
    <div class="span3">
        <?php echo $form->numberField($annual, 'atest_atr_el_imp'); ?>
        <?php echo $form->error($annual, 'atest_atr_el_imp'); ?>
    </div>
    <div class="span3">
        <?php echo $form->numberField($annual, 'atest_ven_el_imp'); ?>
        <?php echo $form->error($annual, 'atest_ven_el_imp'); ?>
    </div>
</div>

</div>

<div class="no_page_break"><h5><?=Yii::t('ProtocolsModule.protocols', "Ручные тесты")?></h5>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($annual, 'test_atr_ampl'); ?>
    </div>
    <div class="span3">
        <?php echo $form->textField($annual, 'test_atr_ampl', ['class' => 'decimal_field']); ?>
        <?php echo $form->error($annual, 'test_atr_ampl'); ?>
    </div>
    <div class="span3">
        <?php echo $form->textField($annual, 'test_ven_ampl', ['class' => 'decimal_field']); ?>
        <?php echo $form->error($annual, 'test_ven_ampl'); ?>
    </div>
</div>


<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($annual, 'test_atr_stimul_thresh'); ?>
    </div>
    <div class="span3">
        <?php echo $form->textField($annual, 'test_atr_stimul_thresh'); ?>
        <?php echo $form->error($annual, 'test_atr_stimul_thresh'); ?>
    </div>
    <div class="span3">
        <?php echo $form->textField($annual, 'test_ven_stimul_thresh'); ?>
        <?php echo $form->error($annual, 'test_ven_stimul_thresh'); ?>
    </div>
</div>
</div>
<div class="no_page_break">
<div class="row margin10">
    <div class="span3">
        <h5><?=Yii::t('ProtocolsModule.protocols', "Электрод КС")?></h5>
    </div>
    <div class="span3">
        <?php echo $form->checkBox($annual, 'pacer'); ?>
        <?php echo $form->error($annual, 'pacer'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($annual, 'pacer_conf'); ?>
    </div>
    <div class="span3">
        <?php echo $form->dropDownList($annual,'pacer_conf', ProtocolAnnual::model()->getImplConfOptions()); ?>
        <?php echo $form->error($annual, 'pacer_conf'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($annual, 'pacer_ampl'); ?>
    </div>
    <div class="span3">
        <?php echo $form->textField($annual, 'pacer_ampl', ['class' => 'decimal_field']); ?>
        <?php echo $form->error($annual, 'pacer_ampl'); ?>
    </div>
</div>


<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($annual, 'pacer_stimul_thresh'); ?>
    </div>
    <div class="span3">
        <?php echo $form->textField($annual, 'pacer_stimul_thresh'); ?>
        <?php echo $form->error($annual, 'pacer_stimul_thresh'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($annual, 'pacer_imp', array()); ?>
    </div>
    <div class="span3">
        <?php echo $form->textField($annual, 'pacer_imp'); ?>
        <?php echo $form->error($annual, 'pacer_imp'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($annual, 'placement', array("class" => "bold")); ?>
    </div>
    <div class="span3">
        <?php echo $form->textArea($annual, 'placement', array("rows" => 5, "class" => "input-large")); ?>
        <?php echo $form->error($annual, 'placement'); ?>
    </div>
</div>
<h5><?=Yii::t('ProtocolsModule.protocols', "Пожалуйста, приложите распечатки начальных и финальных установок!")?></h5>
</div>

<div class="no_page_break">
<h3><?=Yii::t('ProtocolsModule.protocols', "Оценка функций")?></h3>

<div class="row">
    <div class="span3">
    </div>
    <div class="span5 rating margin30">
        <div class="span1">5</div>
        <div class="span1">4</div>
        <div class="span1">3</div>
        <div class="span1">2</div>
        <div class="span1">1</div>
    </div>
</div>

<div class="row">
    <div class="span3">
        <h5><?=Yii::t('ProtocolsModule.protocols', "Экономия времени (эффективность)")?></h5>
    </div>
    <div class="span5 rating margin10">
        <div class="span1"><?= Yii::t('ProtocolsModule.protocols', 'отлично')?></div>
        <div class="span1" style="margin-left: 10px;"><?= Yii::t('ProtocolsModule.protocols', 'удовлетворительно')?></div>
        <div class="span1"></div>
        <div class="span1" style="margin-left: 40px;"><?= Yii::t('ProtocolsModule.protocols', 'плохо')?></div>
    </div>
</div>


<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($annual, 'rtg_time_hmsc'); ?>
    </div>
    <div class="span5 rating">
        <?php echo $form->radioButtonList($annual, 'rtg_time_hmsc', ProtocolAnnual::model()->getRatingOptions(), array("class" => "inline","separator" => "", "template" => "<div class='span1'>{input}</div>")); ?>
        <?php echo $form->error($annual, 'rtg_time_hmsc'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($annual, 'rtg_time_semaphore'); ?>
    </div>
    <div class="span5 rating">
        <?php echo $form->radioButtonList($annual, 'rtg_time_semaphore', ProtocolAnnual::model()->getRatingOptions(), array("class" => "inline","separator" => "", "template" => "<div class='span1'>{input}</div>")); ?>
        <?php echo $form->error($annual, 'rtg_time_semaphore'); ?>
    </div>
</div>


<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($annual, 'rtg_time_iegm'); ?>
    </div>
    <div class="span5 rating">
        <?php echo $form->radioButtonList($annual, 'rtg_time_iegm', ProtocolAnnual::model()->getRatingOptions(), array("class" => "inline","separator" => "", "template" => "<div class='span1'>{input}</div>")); ?>
        <?php echo $form->error($annual, 'rtg_time_iegm'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3">
        <h5><?=Yii::t('ProtocolsModule.protocols', "Клиника (эффективность)")?></h5>
    </div>
    <div class="span5 rating margin10">
        <div class="span1"><?= Yii::t('ProtocolsModule.protocols', 'отлично')?></div>
        <div class="span1" style="margin-left: 10px;"><?= Yii::t('ProtocolsModule.protocols', 'удовлетворительно')?></div>
        <div class="span1"></div>
        <div class="span1" style="margin-left: 40px;"><?= Yii::t('ProtocolsModule.protocols', 'плохо')?></div>
    </div>
</div>


<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($annual, 'rtg_cl_hmsc'); ?>
    </div>
    <div class="span5 rating">
        <?php echo $form->radioButtonList($annual, 'rtg_cl_hmsc', ProtocolAnnual::model()->getRatingOptions(), array("class" => "inline","separator" => "", "template" => "<div class='span1'>{input}</div>")); ?>
        <?php echo $form->error($annual, 'rtg_cl_hmsc'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($annual, 'rtg_cl_semaphore'); ?>
    </div>
    <div class="span5 rating">
        <?php echo $form->radioButtonList($annual, 'rtg_cl_semaphore', ProtocolAnnual::model()->getRatingOptions(), array("class" => "inline","separator" => "", "template" => "<div class='span1'>{input}</div>")); ?>
        <?php echo $form->error($annual, 'rtg_cl_semaphore'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($annual, 'rtg_cl_iegm'); ?>
    </div>
    <div class="span5 rating">
        <?php echo $form->radioButtonList($annual, 'rtg_cl_iegm', ProtocolAnnual::model()->getRatingOptions(), array("class" => "inline","separator" => "", "template" => "<div class='span1'>{input}</div>")); ?>
        <?php echo $form->error($annual, 'rtg_cl_iegm'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($annual, 'rtg_cl_data_suff'); ?>
    </div>
    <div class="span5 rating">
        <?php echo $form->radioButtonList($annual, 'rtg_cl_data_suff', ProtocolAnnual::model()->getRatingOptions(), array("class" => "inline","separator" => "", "template" => "<div class='span1'>{input}</div>")); ?>
        <?php echo $form->error($annual, 'rtg_cl_data_suff'); ?>
    </div>
</div>

<h5><?=Yii::t('ProtocolsModule.protocols', "Пожалуйста, прокомментируйте все ваши выводы, которые имеют неудовлетворительный характер в разделе “Комментарий”!")?></h5>
<h5><?=Yii::t('ProtocolsModule.protocols', "Если Вы считаете, что данных Home Monitoring недостаточно для наблюдения пациента онлайн, укажите недостающие параметры в разделе “Комментарий”!")?></h5>
<h5><?=Yii::t('ProtocolsModule.protocols', "Распечатайте “История” и “Опции” Home Monitoring при окончании исследования пациента!")?></h5>
</div>

<div class="no_page_break">
<h3><?=Yii::t('ProtocolsModule.protocols', "Оценка клинической нагрузки")?></h3>

<h5><?=Yii::t('ProtocolsModule.protocols', "Клиническая нагрузка")?></h5>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($annual, 'cl_hosp_days'); ?>
    </div>
    <div class="span3">
        <?php echo $form->numberField($annual, 'cl_hosp_days'); ?>
        <?php echo $form->error($annual, 'cl_hosp_days'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($annual, 'cl_disability_days'); ?>
    </div>
    <div class="span3">
        <?php echo $form->numberField($annual, 'cl_disability_days'); ?>
        <?php echo $form->error($annual, 'cl_disability_days'); ?>
    </div>
</div>


<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($annual, 'cl_amb_calls'); ?>
    </div>
    <div class="span3">
        <?php echo $form->numberField($annual, 'cl_amb_calls'); ?>
        <?php echo $form->error($annual, 'cl_amb_calls'); ?>
    </div>
</div>

<h5><?=Yii::t('ProtocolsModule.protocols', "Клиническое состояние пациента")?></h5>

<div class="row margin10">
    <div class="span3">
        <?=Yii::t('ProtocolsModule.protocols', "Осложнения основного заболевания, которые возникли за год (перечислить и описать)")?>
    </div>
</div>

<div class="row margin10">
<div class="span8">
    <div id="comorbidities_container">
        <?php foreach ($annual->deseaseRel as $icd10):?>
            <?=Yii::app()->mustache->render("deseaseRel", array(
                    "id" => $icd10->icd10_uid,
                    "name" => $icd10->icd10->name,
                    "descr" => $icd10->descr
                ));            ?>
        <?php endforeach;?></div>
    <?=CHtml::link(
        Yii::t('ProtocolsModule.protocols', "Выбрать заболевание из справочника"),
        '#',
        array( 'onclick'=>'DiseaseSelector.getInstance().show(processComorbidities);return false;', 'class' => 'noprint')
    );?>
</div></div>

</div>

<div class="row margin20">
<div class="span8">
    <?php echo $form->labelEx($annual, 'comment', array("class" => "bold")); ?>
    <?php echo $form->textArea($annual, 'comment', array("rows" => 14, "class" => "input-xxlarge")); ?>
    <?php echo $form->error($annual, 'comment'); ?>
</div>
</div>

<h5><?=Yii::t('ProtocolsModule.protocols', "Передайте монитору исследования следующие документы, сохранив копии в клинике:")?></h5>

<ul>
<li><?=Yii::t('ProtocolsModule.protocols', "Индивидуальную регистрационную карту пациента, включая формуляры «Включение
     пациента в исследование», «Инициализация Home Monitoring», «Оценка функции Home
     Monitoring», «Регистрация нежелательного явления», «Регулярное амбулаторное
     обследование», «Обследование через 12 месяцев», «Окончание исследования»;")?></li>
<li><?=Yii::t('ProtocolsModule.protocols', "Распечатки с программатора;")?></li>
<li><?=Yii::t('ProtocolsModule.protocols', "Кардио-отчеты, полученные с сайта сервисного центра “Home Monitoring”.")?></li>
</ul>

<!--  /ФОРМА ДАННЫХ ПРОТОКОЛА -->

<?php $this->widget("ProtocolSignature", array("protocol" => $protocol));?>
<?php $this->widget("ProtocolFooterButtons", [
        "protocol" => $protocol,
        "research" => $research,
        "patient" => $research->patient,
    ]);?>


<?php $this->endWidget(); ?>

</div><!-- form -->