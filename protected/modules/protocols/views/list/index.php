<?php
/* @var $this ListController */
/* @var $patient Patient */
/* @var $researchOpened bool */

?>

<h2><?=Yii::t('ProtocolsModule.protocols', "Протоколы обследования пациентов") ?></h2>

<p>
    <?php if ($researchOpened):?>
        <?=Yii::t('ProtocolsModule.protocols', "Для редактирования и создания новых протоколов перейдите в соответствующую вкладку выше") ?>
    <?php else:?>
        <?=Yii::t('ProtocolsModule.protocols', "Для начала работы с протоколами необходимо {open_tag}включить пациента" .
            " в исследование{close_tag}", array(
                "{open_tag}" => '<a href="' . $this->createUrl("/protocols/enrolment", ['id' => $patient->uid]) . '">',
                '{close_tag}' => "</a>")
        ) ?>
    <?php endif;?>
</p>
