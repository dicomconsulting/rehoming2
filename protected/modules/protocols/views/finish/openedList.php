<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 18.11.13
 * Time: 15:14
 *
 * @var Protocol[] $protocols
 */
?>

<?php if($protocols):?>
<div class="row margin20">
    <div class="span6">
        <p><?=Yii::t(
    "protocols",
    "Вы не можете сейчас завершить это исследование, поскольку у этого исследования есть следующие открытые протоколы"
)?>:</p>
<?php
$i = 1;
foreach($protocols as $protocol):?>
    <?=$i++?>. <?=$protocol->protocolDictionary->name?>, <?=$protocol->date?><br />
<?php endforeach;?>
<?=Yii::t(
    "protocols",
    "Сначала необходимо удалить или закрыть вышеперечисленные протоколы."
)?></div>
</div>
<?php endif;