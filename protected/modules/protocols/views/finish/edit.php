<?php
/* @var $form CActiveForm */
/* @var $this AnnualController */
/* @var $protocol Protocol */
/* @var $research Research */
/* @var $finish ProtocolFinish */


?>

<div class="form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'id'                     => 'protocol-protocol-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // See class documentation of CActiveForm for details on this,
            // you need to use the performAjaxValidation()-method described there.
            'enableAjaxValidation'   => true,
            'enableClientValidation' => true,
            'action'                 => Yii::app()->getRequest()->getOriginalUrl(),
            'htmlOptions'            => ['autocomplete' => 'off']
        )
    );
    ?>

    <div class="noprint">

<?php echo $form->errorSummary(array($research, $protocol, $finish)); ?>

<h2><?php echo Yii::t('ProtocolsModule.protocols', "Окончание исследования") ?></h2>

<!--  ФОРМА ПРОТОКОЛА -->

<div class="row" style="margin-top:20px;">
    <div class="span3">
        <?php echo $form->labelEx($protocol, 'date'); ?>
        <?php echo $form->dateField($protocol, 'date'); ?>
        <?php echo $form->error($protocol, 'date'); ?>
    </div>
    <div class="span3">
        <?php echo $form->labelEx($protocol, 'user_uid'); ?>
        <?php echo $form->hiddenField($protocol, 'user_uid'); ?>
        <p><?=User::model()->findByPk($protocol->user_uid)->getDisplayName()?></p>
        <?php echo $form->error($protocol, 'user_uid'); ?>
    </div>
</div>

<!--  ФОРМА /ПРОТОКОЛА -->

</div>

<?php $this->widget("ProtocolIntro", array("protocol" => $protocol));?>

<h3>1. <?php echo Yii::t('ProtocolsModule.protocols', "Окончание исследования") ?></h3>

<!--  ФОРМА ДАННЫХ ПРОТОКОЛА -->

    <div class="row" style="margin-top:20px;">
        <div class="span3">
            <?php echo $form->labelEx($research, 'date_end'); ?>
        </div>
        <div class="span3">
            <?php echo $form->dateField($research, 'date_end'); ?>
            <?php echo $form->error($research, 'date_end'); ?>
        </div>
    </div>

    <div class="row" style="margin-top:20px;">
        <div class="span3">
            <?php echo $form->labelEx($finish, 'regular'); ?>
        </div>
        <div class="span3">
            <?php echo $form->checkBox($finish, 'regular'); ?>
            <?php echo $form->error($finish, 'regular'); ?>
        </div>
    </div>

    <div class="row" style="margin-top:20px;">
        <div class="span3">
            <?php echo $form->labelEx($finish, 'cause'); ?>
        </div>
        <div class="span3">
            <?php echo $form->dropDownList($finish, 'cause', ProtocolFinish::model()->getFinishCause()); ?>
            <?php echo $form->error($finish, 'cause'); ?>
        </div>
    </div>

    <div class="row" style="margin-top:20px;">
        <div class="span3">
            <?php echo $form->labelEx($finish, 'comment'); ?>
        </div>
        <div class="span3">
            <?php echo $form->textArea($finish, 'comment', array("class" => "input-xlarge", "rows" => 8)); ?>
            <?php echo $form->error($finish, 'comment'); ?>
        </div>
    </div>

    <div class="row" style="margin-top:20px;">
        <div class="span3">
            <?php echo $form->labelEx($finish, 'last_survey_date'); ?>
        </div>
        <div class="span3">
            <?php echo $form->dateField($finish, 'last_survey_date'); ?>
            <?php echo $form->error($finish, 'last_survey_date'); ?>
        </div>
    </div>

    <h5><?=Yii::t('ProtocolsModule.protocols', "Пожалуйста, передайте в течение двух недель монитору исследования следующие документы - сохранив копии в клинике!") ?></h5>
    <ul>
        <li>Индивидуальную регистрационную карту пациента</li>
        <li>Распечатки с программатора</li>
        <li>Распечатки с сайта "Home Monitoring Service Center"</li>
    </ul>
<div class="no_page_break">
    <h3>2. <?php echo Yii::t('ProtocolsModule.protocols', "Комментарий/примечание") ?></h3>

    <div class="row" style="margin-top:20px;">
        <div class="span8">
            <?php echo $form->textArea($finish, 'general_comment', array("class" => "input-xxlarge", "rows" => 10)); ?>
            <?php echo $form->error($finish, 'general_comment'); ?>
        </div>
    </div>

</div>
    <?php $this->widget("ProtocolSignature", array("protocol" => $protocol));?>

    <!--  /ФОРМА ДАННЫХ ПРОТОКОЛА -->

    <div class="row buttons margin30 noprint">
        <div class="span6">
            <?php if ($protocol->isOpened() && Yii::app()->user->checkAccess("editProtocols")):?>
                <?php echo CHtml::submitButton(Yii::t('ProtocolsModule.protocols', "Сохранить")); ?>
            <?php endif;?>

            <?php echo CHtml::button(
                Yii::t('ProtocolsModule.protocols', "Отменить"),
                array("data-return_location" => $this->createUrl($this->id . "/index", array("id" => $this->patient->uid)),
                    'class' => 'form_cancel_button'
                )
            ); ?>

            <?php if ($protocol->isOpened() && Yii::app()->user->checkAccess("signProtocols")):?>
                <?php echo CHtml::submitButton(Yii::t('ProtocolsModule.protocols', "Подписать"), array("name" => "sign")); ?>
            <?php endif;?>
        </div>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->