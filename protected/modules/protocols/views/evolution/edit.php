<?php
/* @var $form CActiveForm */
/* @var $this EnrolmentController */
/* @var $protocol Protocol */
/* @var $research Research */
/* @var $evolution ProtocolEvolution */
/* @var $findingRedList array */
/* @var $findingYellowList array */


Yii::app()->clientScript->registerScriptFile(
    Yii::app()->assetManager->publish(
        Yii::getPathOfAlias('protocols.assets').'/js/evolution.js'
    ),
    CClientScript::POS_END
);

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->assetManager->publish(
        'js/mustache.js'
    ),
    CClientScript::POS_END
);
Yii::app()->mustache->templatePathAlias = "protocols.views.evolution";
?>
<script type="text/javascript">
    var redFindingTemplate = <?=CJSON::encode(Yii::app()->mustache->getTemplate("redFinding")); ?>;
    var yellowFindingTemplate = <?=CJSON::encode(Yii::app()->mustache->getTemplate("yellowFinding")); ?>;

    //данные для зависимых полей
    var fieldsDependency = <?=CJSON::encode($evolution->getFieldsDependency()) ?>;
    var fieldsPrefix = "ProtocolEvolution_";
</script>

<div class="form">

<?php $form = $this->beginWidget(
    'CActiveForm',
    array(
        'id'                     => 'protocol-protocol-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // See class documentation of CActiveForm for details on this,
        // you need to use the performAjaxValidation()-method described there.
        'enableAjaxValidation'   => true,
        'enableClientValidation' => true,
        'action'                 => Yii::app()->getRequest()->getOriginalUrl(),
        'htmlOptions'            => ['autocomplete' => 'off']
    )
);
?>

<div class="noprint">

<?php echo $form->errorSummary(array($research, $protocol, $evolution)); ?>

<h3><?=$this->getProtocolName()?></h3>

<!--  ФОРМА ПРОТОКОЛА -->

    <div class="row" style="margin-top:20px;">
        <div class="span3">
                <?php echo $form->labelEx($protocol, 'date'); ?>
                <?php echo $form->dateField($protocol, 'date'); ?>
                <?php echo $form->error($protocol, 'date'); ?>
        </div>
        <div class="span3">
                <?php echo $form->labelEx($protocol, 'user_uid'); ?>
                <?php echo $form->hiddenField($protocol, 'user_uid'); ?>
                <p><?=User::model()->findByPk($protocol->user_uid)->getDisplayName()?></p>
                <?php echo $form->error($protocol, 'user_uid'); ?>
        </div>
    </div>

    <!--  ФОРМА /ПРОТОКОЛА -->

</div>
<?php $this->widget("ProtocolIntro", array("protocol" => $protocol));?>

<p class="bold"><?=Yii::t('ProtocolsModule.protocols', "Пожалуйста, документируйте все сообщения “red” (красный) Home Monitoring, а также все предпринятые вами действия с момента включения пациента в исследование и до момента выхода его из исследования (12 месяцев наблюдения)!") ?></p>
<p class="bold"><?=Yii::t('ProtocolsModule.protocols', "Заполните формуляр нежелательного явления (Adverse Event Form), если необходимо!") ?></p>
<p class="bold"><?=Yii::t('ProtocolsModule.protocols', "Пожалуйста, документируйте изменения Home Monitoring “Options”, если необходимо!") ?></p>


    <!--  ФОРМА ДАННЫХ ПРОТОКОЛА -->

	<div class="row margin30">
	    <div class="span8">
		<?php echo $form->checkBox($evolution, 'option_changed', array("class" => "inline")); ?>
		<?php echo $form->labelEx($evolution, 'option_changed', array("class" => "inline bold")); ?>
		<?php echo $form->error($evolution, 'option_changed'); ?>
	    </div>
	</div>

	<div class="row">
        <div class="span5">
            <?php echo $form->labelEx($evolution, 'option_changed_new_value'); ?>
            <?php echo $form->textArea($evolution, 'option_changed_new_value', array("style" => "width:350px", "rows" => 6)); ?>
            <?php echo $form->error($evolution, 'option_changed_new_value'); ?>
        </div>

        <div class="span5">
            <?php echo $form->labelEx($evolution, 'option_changed_reason'); ?>
            <?php echo $form->textArea($evolution, 'option_changed_reason', array("style" => "width:350px", "rows" => 6)); ?>
            <?php echo $form->error($evolution, 'option_changed_reason'); ?>
        </div>
	</div>

	<div class="row margin20">
	    <div class="span8">
		<?php echo $form->checkBox($evolution, 'red_finding', array("class" => "inline")); ?>
		<?php echo $form->labelEx($evolution, 'red_finding', array("class" => "inline bold")); ?>
		<?php echo $form->error($evolution, 'red_finding'); ?>
    	</div>
	</div>

	<div class="row">
        <div class="span5">
            <?php echo $form->labelEx($evolution, 'red_finding_date'); ?>
            <?php echo $form->dateField($evolution, 'red_finding_date'); ?>
            <?php echo $form->error($evolution, 'red_finding_date'); ?>
        </div>

        <div class="span5">
            <?php echo $form->labelEx($evolution, 'red_finding_message'); ?>
            <?php echo $form->textArea($evolution, 'red_finding_message', array("style" => "width:350px", "rows" => 6)); ?>
            <?php echo $form->error($evolution, 'red_finding_message'); ?>
        </div>
	</div>

    <div class="no_page_break">

	<div class="row margin20">
	    <div class="span8">
		<?php echo $form->checkBox($evolution, 'red_finding_changed', array("class" => "inline")); ?>
		<?php echo $form->labelEx($evolution, 'red_finding_changed', array("class" => "inline bold")); ?>
		<?php echo $form->error($evolution, 'red_finding_changed'); ?>
	    </div>
	</div>

	<div class="row">
        <div class="span5">
		<?php echo $form->labelEx($evolution, 'red_finding_changed_date'); ?>
		<?php echo $form->dateField($evolution, 'red_finding_changed_date'); ?>
		<?php echo $form->error($evolution, 'red_finding_changed_date'); ?>
    	</div>

        <div class="span5">
		<?php echo $form->labelEx($evolution, 'red_finding_changed_to'); ?>
		<?php echo $form->dropDownList($evolution, 'red_finding_changed_to', Protocol::getFromRedOptions()); ?>
		<?php echo $form->error($evolution, 'red_finding_changed_to'); ?>
    	</div>
	</div>

	<div class="row">
        <div class="span5">
		<?php echo $form->labelEx($evolution, 'red_finding_changed_message'); ?>
		<?php echo $form->textArea($evolution, 'red_finding_changed_message', array("style" => "width:350px", "rows" => 6)); ?>
		<?php echo $form->error($evolution, 'red_finding_changed_message'); ?>
    	</div>

        <div class="span5">
		<?php echo $form->labelEx($evolution, 'red_finding_changed_comment'); ?>
		<?php echo $form->textArea($evolution, 'red_finding_changed_comment', array("style" => "width:350px", "rows" => 6)); ?>
		<?php echo $form->error($evolution, 'red_finding_changed_comment'); ?>
    	</div>
	</div>
    <h5><?=Yii::t('ProtocolsModule.protocols', "Пожалуйста, распечатайте Home Monitoring \"Опции\" при окончании исследования (обследование через 12 месяцев после включения)!") ?></h5>

    </div>
    <div class="no_page_break">

    <?php if (!empty($findingRedList)): ?>
    <h3><?=Yii::t('ProtocolsModule.protocols', "Описание сообщений red (красный) Home Monitoring") ?></h3>

    <script><!--
    var redFindings = <?php if (!empty($findingRedList)) echo Message::encodeFindings($findingRedList); else echo "{}";?>;
    //--></script>


    <?php echo CHtml::dropDownList("redFindingSelector", 0, [Yii::t('ProtocolsModule.protocols', "Выберите событие...")] + Protocol::filterAndFormatFindingList($evolution, $findingRedList)); ?>
    <a href="#" id="add_red_finding"><?=Yii::t('ProtocolsModule.protocols', "Добавить") ?></a>


        <div id="redFindingsContainer">
        <?php if($evolution->finding):?>
        <?php
            $i=0;
            foreach ($evolution->finding as $val):?>

            <?php
            /* @var Message $finding */
            $currFinding = null;
            foreach ($findingRedList as $finding) {
                if ($val->finding_uid == $finding->data->uid) {
                    $currFinding = $finding;
                }
            };

            if (!$currFinding) {
                continue;
            } else {
                $i++;
            }

            Yii::app()->mustache->render("redFinding", array(
                    "finding" => $currFinding->formatMessage(true),
                    "findingId" => $val->finding_uid,
                    "create_time" => $currFinding->data->date_create,
                    "action" => $val->action,
                    "action_date" => $val->action_date,
                    "clinic_visit" => $val->clinic_visit,
                    "clinic_distance" => $val->clinic_distance,
                    "clinic_time" => $val->clinic_time,
                    "counter" => $i,
                ));

            ?>

        <?php endforeach;?>
        <?php endif;?>
        </div>

    <?php endif;?>

    </div>
    <div class="no_page_break">

    <?php if (!empty($findingYellowList)): ?>

        <?php ob_start();?>

        <script><!--
            var yellowFindings = <?php if (!empty($findingYellowList)) echo Message::encodeFindings($findingYellowList); else echo "{}";?>;
        //--></script>


        <div class="noprint">
            <?php echo CHtml::dropDownList("yellowFindingSelector", 0, array(Yii::t('ProtocolsModule.protocols', "Выберите событие...")) + Protocol::filterAndFormatFindingList($evolution, $findingYellowList)); ?>
            <a href="#" id="add_yellow_finding"><?=Yii::t('ProtocolsModule.protocols', "Добавить") ?></a>
        </div>

        <div id="yellowFindingsContainer">
            <?php if($evolution->finding): ?>
                <?php
                $i=0;
                foreach ($evolution->finding as $val):?>

                    <?php
                    /* @var Message $finding */
                    $currFinding = null;
                    foreach ($findingYellowList as $finding) {
                        if ($val->finding_uid == $finding->data->uid) {
                            $currFinding = $finding;
                        }
                    };
                    if (!$currFinding) {
                        continue;
                    } else {
                        $i++;
                    }

                    Yii::app()->mustache->render("yellowFinding", array(
                            "finding" => $currFinding->formatMessage(true),
                            "findingId" => $val->finding_uid,
                            "create_time" => $currFinding->data->date_create,
                            "counter" => $i,
                        ));
                    ?>


                <?php endforeach;?>
            <?php endif;?>
        </div>
        <?php $findings = ob_get_clean();?>
            <h3<?php if(!isset($currFinding)):?> class="noprint"<?php endif;?>><?php echo Yii::t('ProtocolsModule.protocols', "Описание сообщений yellow (желтый) Home Monitoring") ?></h3>
            <div <?php if(!isset($currFinding)):?> class="noprint"<?php endif;?>>
            <h5><?php echo Yii::t('ProtocolsModule.protocols', "Пожалуйста, документируйте сообщение “yellow” Home Monitoring следующего характера:") ?></h5>
                <ul class="bold">
                    <li>События, связанные со срабатыванием ИКД</li>
                    <li>События «Heart failure monitor» (%CRT, количество экстрасистол…)</li>
                    <li>События, связанные с системой стимуляции (например «безопасный запас порога стимуляции ПЖ ниже допустимого значения)</li>
                </ul>
            </div>
        <?=$findings?>
    <?php endif;?>

    </div>

    <!--  /ФОРМА ДАННЫХ ПРОТОКОЛА -->

    <?php $this->widget("ProtocolSignature", array("protocol" => $protocol));?>
    <?php $this->widget("ProtocolFooterButtons", [
        "protocol" => $protocol,
        "research" => $research,
        "patient" => $research->patient,
    ]);?>


    <?php $this->endWidget();?>

</div><!-- form -->