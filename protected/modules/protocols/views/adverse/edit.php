<?php
/* @var $form CActiveForm */
/* @var $this AnnualController */
/* @var $protocol Protocol */
/* @var $research Research */
/* @var $adverse ProtocolAdverse */

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->assetManager->publish(
        Yii::getPathOfAlias('protocols.assets').'/js/adverse.js'
    ),
    CClientScript::POS_END
);

?>

<script type="text/javascript">
    //данные для зависимых полей
    var fieldsDependency = <?=CJSON::encode($adverse->getFieldsDependency()) ?>;
    var fieldsPrefix = "ProtocolAdverse_";
</script>


<div class="form">

<?php $form = $this->beginWidget(
    'CActiveForm',
    array(
        'id'                     => 'protocol-protocol-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // See class documentation of CActiveForm for details on this,
        // you need to use the performAjaxValidation()-method described there.
        'enableAjaxValidation'   => true,
        'enableClientValidation' => true,
        'action'                 => Yii::app()->getRequest()->getOriginalUrl(),
        'htmlOptions'            => ['autocomplete' => 'off']
    )
);
?>

<div class="noprint">

<?php echo $form->errorSummary(array($research, $protocol, $adverse)); ?>

<h2><?php echo Yii::t('ProtocolsModule.protocols', "Нежелательное явление") ?></h2>

<!--  ФОРМА ПРОТОКОЛА -->

<div class="row" style="margin-top:20px;">
    <div class="span3">
        <?php echo $form->labelEx($protocol, 'date'); ?>
        <?php echo $form->dateField($protocol, 'date'); ?>
        <?php echo $form->error($protocol, 'date'); ?>
    </div>
    <div class="span3">
        <?php echo $form->labelEx($protocol, 'user_uid'); ?>
        <?php echo $form->hiddenField($protocol, 'user_uid'); ?>
        <p><?=User::model()->findByPk($protocol->user_uid)->getDisplayName()?></p>
        <?php echo $form->error($protocol, 'user_uid'); ?>
    </div>
</div>
</div>
<?php $this->widget("ProtocolIntro", array("protocol" => $protocol));?>

<!--  ФОРМА /ПРОТОКОЛА -->
<h5><?php echo Yii::t('ProtocolsModule.protocols', "В случае серьезного нежелательного явления отправьте отчет и сопроводительные документы в течение 2 дней по факсу на номер +7 (495) 956–13–09, доб. 306 или E-mail по адресу {open_tag}SAE-Rehoming@ameruss.com{close_tag}.",
        array("{open_tag}" => "<a href='mailto:SAE-Rehoming@ameruss.com'>", "{close_tag}" => "</a>"))?></h5>
<h3>1. <?php echo Yii::t('ProtocolsModule.protocols', "Нежелательное явление (НЯ)") ?></h3>
<!--  ФОРМА ДАННЫХ ПРОТОКОЛА -->

<div class="row margin20">
    <div class="span3">
        <?php echo $form->labelEx($adverse, 'protocol_type'); ?>
    </div>
    <div class="span5">
        <?php echo $form->dropDownList($adverse,'protocol_type', ProtocolAdverse::model()->getReportTypes()); ?>
        <?php echo $form->error($adverse, 'protocol_type'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($adverse, 'adv_begin_date'); ?>
    </div>
    <div class="span5">
        <?php echo $form->dateField($adverse,'adv_begin_date'); ?>
        <?php echo $form->error($adverse, 'adv_begin_date'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($adverse, 'adv_detect_by_hm'); ?>
    </div>
    <div class="span5">
        <?php echo $form->checkBox($adverse,'adv_detect_by_hm'); ?>
        <?php echo $form->error($adverse, 'adv_detect_by_hm'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($adverse, 'adv_detect_by_hm_date'); ?>
    </div>
    <div class="span5">
        <?php echo $form->dateField($adverse,'adv_detect_by_hm_date'); ?>
        <?php echo $form->error($adverse, 'adv_detect_by_hm_date'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($adverse, 'adv_result'); ?>
    </div>
    <div class="span5">
        <?php
            AdverseResult::enableOnlyActive();
            echo $form->dropDownList($adverse,'adv_result', ProtocolAdverse::model()->getAdverseResults());
        ?>
        <?php echo $form->error($adverse, 'adv_result'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($adverse, 'adv_resolve_date'); ?>
    </div>
    <div class="span5">
        <?php echo $form->dateField($adverse,'adv_resolve_date'); ?>
        <?php echo $form->error($adverse, 'adv_resolve_date'); ?>
    </div>
</div>

<h3>2. <?php echo Yii::t('ProtocolsModule.protocols', "Описание нежелательного явления") ?></h3>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($adverse, 'adv_descr'); ?>
    </div>
    <div class="span5">
        <?php echo $form->textArea($adverse,'adv_descr', array("class" => "input-xxlarge", "rows" => 6)); ?>
        <?php echo $form->error($adverse, 'adv_descr'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($adverse, 'adv_symptoms'); ?>
    </div>
    <div class="span5">
        <?php echo $form->textArea($adverse,'adv_symptoms', array("class" => "input-xxlarge", "rows" => 6)); ?>
        <?php echo $form->error($adverse, 'adv_symptoms'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($adverse, 'adv_actions'); ?>
    </div>
    <div class="span5">
        <?php echo $form->textArea($adverse,'adv_actions', array("class" => "input-xxlarge", "rows" => 6)); ?>
        <?php echo $form->error($adverse, 'adv_actions'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($adverse, 'adv_actions_result'); ?>
    </div>
    <div class="span5">
        <?php echo $form->textArea($adverse,'adv_actions_result', array("class" => "input-xxlarge", "rows" => 6)); ?>
        <?php echo $form->error($adverse, 'adv_actions_result'); ?>
    </div>
</div>

<div class="no_page_break">

<h3>3. <?php echo Yii::t('ProtocolsModule.protocols', "Связь нежелательного явления с имплантатом") ?></h3>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($adverse, 'adv_iml_relation'); ?>
    </div>
    <div class="span5">
        <?php echo $form->dropDownList($adverse,'adv_iml_relation', ProtocolAdverse::model()->getAdverseImplRelation()); ?>
        <?php echo $form->error($adverse, 'adv_iml_relation'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($adverse, 'adv_iml_relation_device'); ?>
    </div>
    <div class="span5">
        <?php echo $form->dropDownList($adverse,'adv_iml_relation_device', ProtocolAdverse::model()->getAdverseDevice()); ?>
        <?php echo $form->error($adverse, 'adv_iml_relation_device'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($adverse, 'adv_iml_relation_electr'); ?>
    </div>
    <div class="span5">
        <?php echo $form->dropDownList($adverse,'adv_iml_relation_electr', ProtocolAdverse::model()->getAdverseElectrode()); ?>
        <?php echo $form->error($adverse, 'adv_iml_relation_electr'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($adverse,'impl_model'); ?>
        <?php echo $form->textField($adverse,'impl_model'); ?>
        <?php echo $form->error($adverse,'impl_model'); ?>
    </div>
    <div class="span5">
        <?php echo $form->labelEx($adverse,'impl_sn'); ?>
        <?php echo $form->textField($adverse,'impl_sn'); ?>
        <?php echo $form->error($adverse,'impl_sn'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($adverse, 'impl_expl'); ?>
    </div>
    <div class="span5">
        <?php echo $form->checkBox($adverse,'impl_expl'); ?>
        <?php echo $form->error($adverse, 'impl_expl'); ?>
        <h5><?php echo Yii::t('ProtocolsModule.protocols', "Пожалуйста, отправьте прибор в BIOTRONIK!") ?></h5>
    </div>
</div>
</div>
<h5><?php echo Yii::t('ProtocolsModule.protocols', "Пожалуйста, отметьте любую связь нежелательного явления с имплантатом (в том числе, потенциальную) в разделе 6!") ?></h5>
<h3>4. <?php echo Yii::t('ProtocolsModule.protocols', "Серьезность нежелательного явления") ?></h3>

<div class="row margin10">
    <div class="span5">
        <?php echo $form->labelEx($adverse, 'adv_lethal'); ?>
    </div>
    <div class="span4">
        <?php echo $form->checkBox($adverse,'adv_lethal'); ?>
        <?php echo $form->error($adverse, 'adv_lethal'); ?>
    </div>
</div>
<div class="row margin10">
    <div class="span5">
        4.2 <?php echo Yii::t('ProtocolsModule.protocols', "Нежелательное явление привело к серьезному ухудшению состояния здоровья пациента, результатом чего стало (1-4):") ?>
    </div>
</div>
<div class="row margin10">
    <div class="span5">
        <?php echo $form->labelEx($adverse, 'adv_injury'); ?>
    </div>
    <div class="span4">
        <?php echo $form->checkBox($adverse,'adv_injury'); ?>
        <?php echo $form->error($adverse, 'adv_injury'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span5">
        <?php echo $form->labelEx($adverse, 'adv_perm_damage'); ?>
    </div>
    <div class="span4">
        <?php echo $form->checkBox($adverse,'adv_perm_damage'); ?>
        <?php echo $form->error($adverse, 'adv_perm_damage'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span5">
        <?php echo $form->labelEx($adverse, 'adv_hospitalization'); ?>
    </div>
    <div class="span4">
        <?php echo $form->checkBox($adverse,'adv_hospitalization'); ?>
        <?php echo $form->error($adverse, 'adv_hospitalization'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span5">
        <?php echo $form->labelEx($adverse, 'adv_surg'); ?>
    </div>
    <div class="span4">
        <?php echo $form->checkBox($adverse,'adv_surg'); ?>
        <?php echo $form->error($adverse, 'adv_surg'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span5">
        <?php echo $form->labelEx($adverse, 'adv_distress'); ?>
    </div>
    <div class="span4">
        <?php echo $form->checkBox($adverse,'adv_distress'); ?>
        <?php echo $form->error($adverse, 'adv_distress'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span5">
        <?php echo $form->labelEx($adverse, 'adv_prevented'); ?>
    </div>
    <div class="span4">
        <?php echo $form->checkBox($adverse,'adv_prevented'); ?>
        <?php echo $form->error($adverse, 'adv_prevented'); ?>
    </div>
</div>

<h5><?php echo Yii::t('ProtocolsModule.protocols', "Если выполнено одно из условий этого раздела, то вы наблюдали серьезное нежелательное явление!") ?></h5>


<div class="no_page_break">
<h3>5. <?php echo Yii::t('ProtocolsModule.protocols', "Смерть пациента (если применимо)") ?></h3>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($adverse, 'adv_death_date'); ?>
    </div>
    <div class="span5">
        <?php echo $form->dateField($adverse,'adv_death_date'); ?>
        <?php echo $form->error($adverse, 'adv_death_date'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($adverse, 'adv_death_cause'); ?>
    </div>
    <div class="span5">
        <?php echo $form->textArea($adverse,'adv_death_cause', array("rows" => 4, "class" => "input-xxlarge")); ?>
        <?php echo $form->error($adverse, 'adv_death_cause'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($adverse, 'adv_death_cardio'); ?>
    </div>
    <div class="span5">
        <?php echo $form->dropDownList($adverse,'adv_death_cardio', ProtocolAdverse::model()->getDeathCause()); ?>
        <?php echo $form->error($adverse, 'adv_death_cardio'); ?>
    </div>
</div>

<div class="row margin10">
    <div class="span3">
        <?php echo $form->labelEx($adverse, 'adv_death_arr'); ?>
    </div>
    <div class="span5">
        <?php echo $form->dropDownList($adverse,'adv_death_arr', ProtocolAdverse::model()->getDeathCause()); ?>
        <?php echo $form->error($adverse, 'adv_death_arr'); ?>
    </div>
</div>

<h5><?php echo Yii::t('ProtocolsModule.protocols', "Пожалуйста перешлите монитору исследования копию свидетельства о смерти, протокола вскрытия как только они будут готовы!") ?></h5>
</div>

<div class="no_page_break">
<h3>6. <?php echo Yii::t('ProtocolsModule.protocols', "Комментарий/примечания") ?></h3>

<h5><?php echo Yii::t('ProtocolsModule.protocols', "Пожалуйста, отметьте любую связь НЯ с имплантатом (в том числе потенциальную)") ?></h5>

<div class="row margin10">
    <div class="span8">
        <?php echo $form->textArea($adverse,'adv_comment', array("class" => "input-xxlarge", "rows" => 10)); ?>
        <?php echo $form->error($adverse, 'adv_comment'); ?>
    </div>
</div>

<h5><?php echo Yii::t('ProtocolsModule.protocols', "В случае серьезного нежелательного явления отправьте отчет и сопроводительные документы в течение 2 дней!") ?></h5>
<h5><?php echo Yii::t('ProtocolsModule.protocols', "В случае нежелательного явления отправьте отчет и сопроводительные документы в течение 14 дней - сохраняйте копии всех документов в клинике!") ?></h5>
</div>
<!--  /ФОРМА ДАННЫХ ПРОТОКОЛА -->

<?php $this->widget("ProtocolSignature", array("protocol" => $protocol));?>
<?php $this->widget("ProtocolFooterButtons", [
        "protocol" => $protocol,
        "research" => $research,
        "patient" => $research->patient,
    ]);?>

<?php $this->endWidget(); ?>

</div><!-- form -->