<?php
/* @var $form CActiveForm */
/* @var $this EnrolmentController */
/* @var $protocols Protocol[] */
/* @var $research Research */
/* @var $enrolment ProtocolEnrolment */
/* @var $patientId integer */
/* @var $canCreateNew bool */

?>
<?php if(Yii::app()->user->checkAccess("editProtocols")):?>
<?php if($canCreateNew):?><div style="text-align: center;" class="margin30"><?=CHtml::link(Yii::t("ProtocolsModule.protocols", "Завести протокол") . " \"" . $this->getProtocolName() . "\"", $this->createUrl($this->id . "/edit", ["id" => $patientId]));?></div><?php endif;?>
<?php endif;?>

<script type="text/javascript">
    window.protocolName = '<?=$this->getProtocolName();?>';
</script>
<?php if($protocols):?>
<div class="rh_table_grid">
<table class="items margin30">
    <tr>
        <th><?=Yii::t('ProtocolsModule.protocols', "Мед. центр") ?></th>
        <th title="<?= Yii::t('ProtocolsModule.protocols', 'Инициалы')?>"><?= Yii::t('ProtocolsModule.protocols', 'Иниц.')?></th>
        <th><?=Yii::t('ProtocolsModule.protocols', "Номер скрининга") ?></th>
        <th><?=Yii::t('ProtocolsModule.protocols', "Дата включения") ?></th>
        <th><?=Yii::t('ProtocolsModule.protocols', "Дата исключения") ?></th>
        <th><?=Yii::t('ProtocolsModule.protocols', "Исс-ль") ?></th>
        <th><?=Yii::t('ProtocolsModule.protocols', "Статус") ?></th>
        <th><?=Yii::t('ProtocolsModule.protocols', "Действия") ?></th>
    </tr>
    <?php foreach ($protocols as $row):?>
        <tr style="text-align: center;<?php if ($row->research->getStatus() == Research::STATUS_CLOSED): ?>background-color: #d3d3d3;<?php elseif($row->isOpened()):?>background-color: #d3a196;<?php else:?>background-color: #b2d6ff;<?php endif;?>">
            <td><?=$row->research->medicalCenter->code; ?></td>
            <td><?=$row->research->patient_code; ?></td>
            <td><?=$row->research->getScreeningNumber(); ?></td>
            <td class="date_create_cell"><?=$row->research->date_begin->format(RhDateTime::RH_DATE); ?></td>
            <td><?php if($row->research->date_end):?><?=$row->research->date_end->format(RhDateTime::RH_DATE);; ?><?php endif;?></td>
            <td><?=$row->user->getDisplayName(); ?></td>
            <td><?php if($row->status == Protocol::STATUS_OPENED):?><span class="ui-icon ui-icon-unlocked"></span><?php else:?><span class="ui-icon ui-icon-locked"><?php endif;?></td>
            <td><?php
                if ($row->research->getStatus() == Research::STATUS_BEGUN  && Yii::app()->user->checkAccess("editProtocols")):?>
                    <?=CHtml::link(Yii::t('ProtocolsModule.protocols', "Редактировать"), $this->createUrl($this->id . "/edit", ['id' => $row->research->patient_uid, 'protocolId' => $row->uid]))?>
                    | <?=CHtml::link(Yii::t('ProtocolsModule.protocols', "Удалить"), $this->createUrl($this->id . "/delete", array("id" => $row->research->patient_uid, "protocolId" => $row->uid)), array('class' => 'deleteProtocolLink'))?>
                    |
                <?php elseif(!$row->isOpened() || !Yii::app()->user->checkAccess("editProtocols")):?>
                    <?=CHtml::link(Yii::t('ProtocolsModule.protocols', "Посмотреть"), $this->createUrl($this->id . "/edit", ['id' => $row->research->patient_uid, 'protocolId' => $row->uid]))?>
                    |
                <?php endif;?>
                <?=CHtml::link(Yii::t('ProtocolsModule.protocols', "Печать"), $this->createUrl($this->id . "/print", array("id" => $row->research->patient_uid, "protocolId" => $row->uid)))?>
                | <?=CHtml::link(Yii::t('ProtocolsModule.protocols', "История"), $this->createUrl("/" . $this->module->id . "/history", array("id" => $row->research->patient_uid, "protocolId" => $row->uid)))?>
                | <?=CHtml::link(Yii::t('ProtocolsModule.protocols', "Общая история"), $this->createUrl("/" . $this->module->id . "/history/all", array("id" => $row->research->patient_uid, "researchId" => $row->research_uid)), ["title" => "История по всем документам, заведенных по иследованию"])?>
                |
                <?php
                if(!$row->isOpened() && $row->research->isOpened()):?>
                    <?=CHtml::link(Yii::t('ProtocolsModule.protocols', "Завершить"), $this->createUrl("/protocols/finish/edit", array("id" => $row->research->patient_uid)))?>
                <?php endif;?>
            </td>
        </tr>
    <?php endforeach;?>
</table>
</div>
<?php endif;?>