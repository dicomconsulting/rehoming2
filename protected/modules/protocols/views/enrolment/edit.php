<?php
/* @var $form CActiveForm */
/* @var $this EnrolmentController */
/* @var $protocol Protocol */
/* @var $research Research */
/* @var $enrolment ProtocolEnrolment */
/* @var $pharmGroups array */
/* @var $pharmMeasureUnits array */


Yii::app()->clientScript->registerScriptFile(
    Yii::app()->assetManager->publish(
        'js/mustache.js'
    ),
    CClientScript::POS_END
);

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->assetManager->publish(
        Yii::getPathOfAlias('protocols.assets').'/js/enrolment.js'
    ),
    CClientScript::POS_END
);

Yii::app()->mustache->templatePathAlias = "protocols.views.enrolment";
?>

<script type="text/javascript">
    //данные для зависимых полей
    var fieldsDependency = <?=CJSON::encode($enrolment->getFieldsDependency()) ?>;
    var fieldsPrefix = "ProtocolEnrolment_";
</script>

<?php
$this->widget(
    'directory.widgets.DiseaseSelector',
    ['patient' => $this->patient]
);

$this->widget(
    'directory.widgets.PharmSelector',
    ['patient' => $this->patient]
);
?>

<div class="form">

    <?php
    $form=$this->beginWidget('CActiveForm', array(
        'id'=>'protocol-protocol-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // See class documentation of CActiveForm for details on this,
        // you need to use the performAjaxValidation()-method described there.
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array("validateOnSubmit" => true, "afterValidate" => "js:afterValidate", 'validationUrl'),
        'action' => Yii::app()->getRequest()->getOriginalUrl(),
        'htmlOptions' => ['autocomplete' => 'off']
        )
    );
    ?>

    <div class="noprint">

    <?php echo $form->errorSummary(array($research, $protocol, $enrolment)); ?>
    <h2 class="margin30"><?php echo Yii::t('ProtocolsModule.protocols', "Протокол включения пациента в исследование"); ?></h2>

    <!--  ФОРМА ИССЛЕДОВАНИЯ  -->
    <div class="row margin30">
        <div class="span3">
                <?php echo $form->labelEx($research, 'date_begin'); ?>
                <?php echo $form->dateField($research, 'date_begin'); ?>
                <?php echo $form->error($research, 'date_begin'); ?>
        </div>
        <div class="span5">
                <?php echo $form->labelEx($research, 'medical_center_uid'); ?>
                <div class="noprint"><?php
                    MedicalCenter::enableOnlyActive();
                    echo $form->dropDownList(
                        $research,
                        'medical_center_uid',
                        CHtml::listData(MedicalCenter::model()->withI18n(Yii::app()->language)->findAll(array("order" => "code")), "uid" , function($model){return $model->code;})
                    ); ?>
                </div>
                <div class="only_for_print"><?php if($research->medicalCenter):?><?=$research->medicalCenter->getDisplayName()?><?php endif;?></div>
                <?php echo $form->error($research, 'medical_center_uid'); ?>
        </div>
    </div>

    <!--  /ФОРМА ИССЛЕДОВАНИЯ  -->


    <!--  ФОРМА ПРОТОКОЛА -->

    <div class="row margin10">
        <div class="span2">
            <?php echo $form->labelEx($research, 'screening_number'); ?>
            <div class="noprint"><?php echo $form->numberField($research, 'screening_number', array("class" => "input-small")); ?></div>
            <div class="only_for_print"><?=$research->getScreeningNumber()?></div>
            <?php echo $form->error($research, 'screening_number'); ?>
        </div>

        <div class="span2">
            <?php echo $form->labelEx($research, 'patient_code'); ?>
            <div class="noprint"><?php echo $form->textField($research, 'patient_code', array("class" => "input-small")); ?></div>
            <?php echo $form->error($research, 'patient_code'); ?>
        </div>

        <div class="span3">
                <?php echo $form->labelEx($protocol, 'date'); ?>
                <?php echo $form->dateField($protocol, 'date'); ?>
                <?php echo $form->error($protocol, 'date'); ?>
        </div>
        <div class="span3">
                <?php echo $form->labelEx($protocol, 'user_uid'); ?>
                <?php echo $form->hiddenField($protocol, 'user_uid'); ?>
                <p><?=User::model()->findByPk($protocol->user_uid)->getDisplayName()?></p>
                <?php echo $form->error($protocol, 'user_uid'); ?>
        </div>
    </div>

    <!--  ФОРМА /ПРОТОКОЛА -->

    </div>

    <?php if($research->scenario != 'insert') $this->widget("ProtocolIntroEnrolment", array("protocol" => $protocol));?>

    <!--  ФОРМА ДАННЫХ ПРОТОКОЛА -->

    <h3><?php echo Yii::t('ProtocolsModule.protocols', "Общие сведения") ?></h3>

    <div class="row margin20">
        <div class="span2">
            <?=Yii::t('ProtocolsModule.protocols', "Пол")?>:<br />
            <?php if ($this->patient->sex == Patient::FEMALE):?>
                <?=Yii::t('ProtocolsModule.protocols', "женский")?>
            <?php elseif ($this->patient->sex == Patient::MALE):?>
                <?=Yii::t('ProtocolsModule.protocols', "мужской")?>
            <?php else:?>
                <?=Yii::t('ProtocolsModule.protocols', "Неизвестно")?>
            <?php endif;?>
        </div>
        <div class="span2">
                <?php echo $form->labelEx($enrolment, 'patient_height'); ?>
                <?php echo $form->numberField($enrolment, 'patient_height', array("class" => "input-small")); ?>
                <?php echo $form->error($enrolment, 'patient_height'); ?>
        </div>
        <div class="span2">
                <?php echo $form->labelEx($enrolment, 'patient_weight'); ?>
                <?php echo $form->textField($enrolment, 'patient_weight', array("class" => "input-small decimal_field")); ?>
                <?php echo $form->error($enrolment, 'patient_weight'); ?>
        </div>
        <div class="span2">
                <?php echo $form->labelEx($enrolment, 'patient_nyha'); ?>
                <?php $nyhaOptions = ProtocolEnrolment::model()->getNyhaOptions();
                echo $form->dropDownList(
                    $enrolment,
                    'patient_nyha',
                    CHtml::listData($nyhaOptions, "uid", "name"),
                    array("class" => "input-small", 'empty' => '')
                ); ?>
                <?php echo $form->error($enrolment, 'patient_nyha'); ?>
        </div>
    </div>

    <h3><?php echo Yii::t('ProtocolsModule.protocols', "Критерии включения") ?></h3>

    <div class="row">
        <div class="span3">
            <strong><?=Yii::t('ProtocolsModule.protocols', "Имплантирован с функцией мобильного телемониторинга")?><sup>*</sup>:</strong>
        </div>

        <div class="span1">
            <?php echo $form->checkBox(
                $enrolment,
                'enrolment_pacer',
                array("class"    => "inline",
                      "onChange" => "js:checkDevices('enrolment_pacer', 'enrolment_icd');checkDevices('enrolment_pacer', 'enrolment_crt_d');checkDevices('enrolment_pacer', 'enrolment_crt_p');"
                )
            ); ?>
            <?php echo $form->labelEx($enrolment, 'enrolment_pacer', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'enrolment_pacer'); ?>
        </div>
        <div class="span1">
            <?php echo $form->checkBox(
                $enrolment,
                'enrolment_crt_p',
                array("class" => "inline", "onChange" => "js:checkDevices('enrolment_crt_p', 'enrolment_crt_d');checkDevices('enrolment_crt_p', 'enrolment_icd');checkDevices('enrolment_crt_p', 'enrolment_pacer')")
            ); ?>
            <?php echo $form->labelEx($enrolment, 'enrolment_crt_p', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'enrolment_crt_p'); ?>
        </div>

        <div class="span1">
            <?php echo $form->checkBox(
                $enrolment,
                'enrolment_crt_d',
                array("class" => "inline", "onChange" => "js:checkDevices('enrolment_crt_d', 'enrolment_icd');checkDevices('enrolment_crt_d', 'enrolment_crt_p');checkDevices('enrolment_crt_d', 'enrolment_pacer');")
            ); ?>
            <?php echo $form->labelEx($enrolment, 'enrolment_crt_d', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'enrolment_crt_d'); ?>
        </div>

    <div class="span1">
        <?php echo $form->checkBox(
            $enrolment,
            'enrolment_icd',
            array(
                "class" => "inline",
                "onChange" => "js:checkDevices('enrolment_pacer', 'enrolment_icd');checkDevices('enrolment_icd', 'enrolment_crt_d');checkDevices('enrolment_icd', 'enrolment_crt_p');"
            )
        ); ?>
        <?php echo $form->labelEx($enrolment, 'enrolment_icd', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment, 'enrolment_icd'); ?>
    </div>
    </div>

    <div class="row margin20">
    <div class="span8">
        <?php echo $form->checkBox($enrolment, 'enrolment_age_over_18', array("class" => "inline")); ?>
        <?php echo $form->labelEx($enrolment, 'enrolment_age_over_18', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment, 'enrolment_age_over_18'); ?>
    </div>
    </div>

    <div class="row margin10">
    <div class="span8">
        <?php echo $form->checkBox($enrolment, 'enrolment_able', array("class" => "inline")); ?>
        <?php echo $form->labelEx($enrolment, 'enrolment_able', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment, 'enrolment_able'); ?>
    </div>
    </div>

    <div class="row margin10">
    <div class="span8">
        <?php echo $form->checkBox($enrolment, 'enrolment_agree', array("class" => "inline")); ?>
        <?php echo $form->labelEx($enrolment, 'enrolment_agree', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment, 'enrolment_agree'); ?>
    </div>
    </div>

    <div class="row margin10">
    <div class="span8">
        <?php echo $form->checkBox($enrolment, 'enrolment_gsm', array("class" => "inline")); ?>
        <?php echo $form->labelEx($enrolment, 'enrolment_gsm', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment, 'enrolment_gsm'); ?>
    </div>
    </div>

    <div class="row margin10">
    <div class="span8">
        <?php echo $form->checkBox($enrolment, 'enrolment_patient_accessible', array("class" => "inline")); ?>
        <?php echo $form->labelEx($enrolment, 'enrolment_patient_accessible', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment, 'enrolment_patient_accessible'); ?>
    </div>
    </div>

    <h3><?php echo Yii::t('ProtocolsModule.protocols', "Критерии исключения") ?></h3>

    <div class="row margin10">
        <div class="span8">
            <?php echo $form->checkBox($enrolment, 'exclude_surgery', array("class" => "inline")); ?>
            <?php echo $form->labelEx($enrolment, 'exclude_surgery', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'exclude_surgery'); ?>
        </div>
    </div>

    <div class="row margin10">
        <div class="span8">
            <?php echo $form->checkBox($enrolment, 'exclude_heartattack', array("class" => "inline")); ?>
            <?php echo $form->labelEx($enrolment, 'exclude_heartattack', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'exclude_heartattack'); ?>
        </div>
    </div>

    <div class="row margin10">
        <div class="span8">
            <?php echo $form->checkBox($enrolment, 'exclude_shocks', array("class" => "inline")); ?>
            <?php echo $form->labelEx($enrolment, 'exclude_shocks', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'exclude_shocks'); ?>
        </div>
    </div>

    <div class="row margin10">
        <div class="span8">
            <?php echo $form->checkBox($enrolment, 'excl_crit_hardware', array("class" => "inline")); ?>
            <?php echo $form->labelEx($enrolment, 'excl_crit_hardware', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'excl_crit_hardware'); ?>
        </div>
    </div>

    <div class="row margin10">
        <div class="span8">
            <?php echo $form->checkBox($enrolment, 'excl_crit_infection', array("class" => "inline")); ?>
            <?php echo $form->labelEx($enrolment, 'excl_crit_infection', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'excl_crit_infection'); ?>
        </div>
    </div>


    <div class="row margin10">
        <div class="span8">
            <?php echo $form->checkBox($enrolment, 'excl_crit_unable', array("class" => "inline")); ?>
            <?php echo $form->labelEx($enrolment, 'excl_crit_unable', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'excl_crit_unable'); ?>
        </div>
    </div>

    <div class="row margin10">
        <div class="span8">
            <?php echo $form->checkBox($enrolment, 'excl_crit_busy', array("class" => "inline")); ?>
            <?php echo $form->labelEx($enrolment, 'excl_crit_busy', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'excl_crit_busy'); ?>
        </div>
    </div>

    <div class="row margin10">
        <div class="span8">
            <?php echo $form->checkBox($enrolment, 'excl_crit_nomobile', array("class" => "inline")); ?>
            <?php echo $form->labelEx($enrolment, 'excl_crit_nomobile', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'excl_crit_nomobile'); ?>
        </div>
    </div>

    <div class="row margin10">
        <div class="span8">
            <?php echo $form->checkBox($enrolment, 'excl_crit_pregnancy', array("class" => "inline")); ?>
            <?php echo $form->labelEx($enrolment, 'excl_crit_pregnancy', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'excl_crit_pregnancy'); ?>
        </div>
    </div>


    <div class="row margin20">
    <div class="span3">
        <?php echo $form->labelEx($enrolment, 'visit_date', array("style" => "font-weight:bold;")); ?>
        </div>
        <div class="span7">
        <?php echo $form->dateField($enrolment, 'visit_date'); ?>
        <?php echo $form->error($enrolment, 'visit_date'); ?>
    </div>
    </div>

    <script>
        var diagnosisTemplate = <?=CJSON::encode(Yii::app()->mustache->getTemplate("diagnosis")); ?>;
    </script>
    <div class="row">
        <div class="span2">
            <?=Yii::t("ProtocolsModule.protocols", "Клинический диагноз")?>&nbsp;*
        </div>
        <div class="span7">
            <div id="diagnosis_container">
                <?php if($enrolment->diagnosis) {
                    foreach($enrolment->diagnosis as $icd10) {
                        /** @var Icd10 $icd10 */
                        Yii::app()->mustache->render("diagnosis", array(
                        "id" => $icd10->uid,
                        "name" => $icd10->name,
                        ));
                    };
                };?>
            </div>
            <?=CHtml::link(Yii::t('ProtocolsModule.protocols', "Выбрать диагноз из справочника"), '#', array( 'onclick'=>'DiseaseSelector.getInstance().show(processDisease);return false;', 'class' => 'noprint'));?>
            <?php echo $form->error($enrolment, 'diagnosis'); ?>
        </div>
    </div>

    <div class="row margin10">
        <div class="span2">
            <?php echo $form->labelEx($enrolment, 'diagnosis_comment'); ?>
        </div>
        <div class="span7">
            <?php echo $form->textArea($enrolment, 'diagnosis_comment', array("rows" => 6, "class" => "input-xxlarge")); ?>
            <?php echo $form->error($enrolment, 'diagnosis_comment'); ?>
        </div>
    </div>

    <h3><?php echo Yii::t('ProtocolsModule.protocols', "Нарушения ритма и проводимости") ?></h3>
    <p><?php echo Yii::t('ProtocolsModule.protocols', "Синусовая брадикардия") ?></p>

    <div class="row">
        <div class="span2">
            <?php echo $form->labelEx($enrolment, 'ca_block'); ?>
        </div>
        <div class="span1">
            <?php echo $form->checkBox($enrolment, 'ca_block', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'ca_block'); ?>
        </div>
        <div class="span2">
            <?php echo $form->labelEx($enrolment, 'ca_block_grade'); ?>
        </div>
        <div class="span2">
            <?=$form->dropDownList(
                $enrolment,
                'ca_block_grade',
                CHtml::listData(ProtocolEnrolment::getBlockadeDegreeOptions(), "uid", "name"),
                array("class" => "input-small", 'empty' => '')
            );?>
            <?php echo $form->error($enrolment, 'ca_block_grade'); ?>
        </div>
    </div>

    <div class="row margin10">
        <div class="span3">
            <?php echo $form->labelEx($enrolment, 'ca_pause_duration'); ?>
        </div>
        <div class="span1">
            <?php echo $form->checkBox($enrolment, 'ca_pause', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'ca_pause'); ?>
        </div>
        <div class="span2">
            <?php echo $form->textField($enrolment, 'ca_pause_duration', array("class" => "input-small decimal_field")); ?> (<?=Yii::t("ProtocolsModule.protocols", "мс")?>)
            <?php echo $form->error($enrolment, 'ca_pause_duration'); ?>
        </div>
    </div>

    <div class="row">
        <div class="span5">
            <?php echo $form->labelEx($enrolment, 'atrial_arr'); ?>
        </div>
        <div class="span2">
            <?php echo $form->numberField($enrolment, 'atrial_arr', array("class" => "input-small")); ?>
            <?php echo $form->error($enrolment, 'atrial_arr'); ?>
        </div>
    </div>


    <div class="row">
        <div class="span5">
            <?php echo $form->labelEx($enrolment, 'atrial_tachy'); ?>
        </div>
        <div class="span2">
            <?php echo $form->numberField($enrolment, 'atrial_tachy', array("class" => "input-small")); ?>
            <?php echo $form->error($enrolment, 'atrial_tachy'); ?>
        </div>
    </div>

    <div class="row">
        <div class="span2">
            <?php echo $form->labelEx($enrolment, 'atrial_fibrillation', array("class" => "bold")); ?>
        </div>
        <div class="span1">
            <?php echo $form->checkBox($enrolment, 'atrial_fibrillation', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'atrial_fibrillation'); ?>
        </div>
        <div class="span3">
            <?php echo $form->labelEx($enrolment, 'atrial_flutter', array("class" => "bold")); ?>
        </div>
        <div class="span1">
            <?php echo $form->checkBox($enrolment, 'atrial_flutter', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'atrial_flutter'); ?>
        </div>
    </div>

    <div class="row">
        <div class="span2">
            <?php echo $form->labelEx($enrolment, 'atrial_permanent'); ?>
        </div>
        <div class="span3">
            <?php echo $form->checkBox($enrolment, 'atrial_permanent', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'atrial_permanent'); ?>
        </div>
    </div>

    <div class="row margin10">
        <div class="span4">
            <?php echo $form->labelEx($enrolment, 'tachy_history_duration'); ?>
        </div>
        <div class="span2">
            <?php echo $form->numberField($enrolment, 'tachy_history_duration'); ?>
            <?php echo $form->error($enrolment, 'tachy_history_duration'); ?>
        </div>
    </div>

    <div class="row margin10">
        <div class="span4">
            <?php echo $form->labelEx($enrolment, 'tachy_latest_date'); ?>
        </div>
        <div class="span2">
            <?php echo $form->dateField($enrolment, 'tachy_latest_date'); ?>
            <?php echo $form->error($enrolment, 'tachy_latest_date'); ?>
        </div>
    </div>

    <div class="row margin10">
        <div class="span3">
            <?php echo Yii::t('ProtocolsModule.protocols', "Продолжительность эпизодов (минут)") ?>
        </div>
        <div class="span1">
            <?php echo $form->numberField($enrolment, 'tachy_shortest_episode', array("class" => "input-mini", "onChange" => "js:checkEpisodes('tachy_shortest_episode', 'tachy_longest_episode')")); ?>
            <?php echo $form->error($enrolment, 'tachy_shortest_episode'); ?>
        </div>
        <div class="span1">
            <?php echo Yii::t('ProtocolsModule.protocols', "минимум") ?>
        </div>
        <div class="span1">
            <?php echo $form->numberField($enrolment, 'tachy_longest_episode', array("class" => "input-mini", "onChange" => "js:checkEpisodes('tachy_shortest_episode', 'tachy_longest_episode')")); ?>
            <?php echo $form->error($enrolment, 'tachy_longest_episode'); ?>
        </div>
        <div class="span1">
            <?php echo Yii::t('ProtocolsModule.protocols', "максимум") ?>
        </div>
    </div>


    <div class="row margin10">
        <div class="span3">
            <?php echo $form->labelEx($enrolment, 'tachy_frequency'); ?>
        </div>
        <div class="span1">
            <?php echo $form->numberField($enrolment, 'tachy_frequency', array("class" => "input-mini")); ?>
            <?php echo $form->error($enrolment, 'tachy_frequency'); ?>
        </div>
    </div>

    <div class="row margin10">
        <div class="span3">
            <?php echo $form->labelEx($enrolment, 'arr_cupped'); ?>
        </div>
        <div class="span3">
            <?php echo $form->dropDownList($enrolment, 'arr_cupped', ProtocolEnrolment::model()->getArrCuppingOptions()); ?>
            <?php echo $form->error($enrolment, 'arr_cupped'); ?>
        </div>
    </div>

    <div class="row margin20">
        <div class="span2">
            <?php echo $form->labelEx($enrolment, 'ab_block', array("style" => "font-weight:bold;")); ?>
        </div>
        <div class="span1">
            <?php echo $form->checkBox($enrolment, 'ab_block', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'ab_block'); ?>
        </div>
        <div class="span2">
            <?php echo $form->labelEx($enrolment, 'ab_block_grade'); ?>
        </div>
        <div class="span2">
            <?=$form->dropDownList(
                $enrolment,
                'ab_block_grade',
                CHtml::listData(ProtocolEnrolment::getBlockadeDegreeOptions(), "uid", "name"),
                array("class" => "input-small", 'empty' => '')
            );?>
            <?php echo $form->error($enrolment, 'ab_block_grade'); ?>
        </div>
    </div>

    <div class="row margin10">
        <div class="span3">
            <?php echo $form->labelEx($enrolment, 'ab_pause_duration'); ?>
        </div>
        <div class="span1">
            <?php echo $form->checkBox($enrolment, 'ab_pause', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'ab_pause'); ?>
        </div>
        <div class="span2">
            <?php echo $form->textField($enrolment, 'ab_pause_duration', array("class" => "input-small decimal_field")); ?>  (<?=Yii::t("ProtocolsModule.protocols", "мс")?>)
            <?php echo $form->error($enrolment, 'ab_pause_duration'); ?>
        </div>
    </div>

    <div class="row">
        <div class="span3">
            <?php echo $form->labelEx($enrolment, 'left_branch_block'); ?>
        </div>
        <div class="span3">
            <?php echo $form->checkBox($enrolment, 'left_branch_block', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'left_branch_block'); ?>
        </div>
    </div>

    <div class="row margin10">
        <div class="span3">
            <?php echo $form->labelEx($enrolment, 'right_branch_block'); ?>
        </div>
        <div class="span3">
            <?php echo $form->checkBox($enrolment, 'right_branch_block', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'right_branch_block'); ?>
        </div>
    </div>

    <div class="row margin10">
        <div class="span3">
            <?php echo $form->labelEx($enrolment, 'both_branch_block'); ?>
        </div>
        <div class="span3">
            <?php echo $form->checkBox($enrolment, 'both_branch_block', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'both_branch_block'); ?>
        </div>
    </div>

    <div class="row margin10">
        <div class="span3">
            <?php echo $form->labelEx($enrolment, 'qrs_duration'); ?>
        </div>
        <div class="span3">
            <?php echo $form->numberField($enrolment, 'qrs_duration', array("class" => "inline input-small")); ?>
            <?php echo $form->error($enrolment, 'qrs_duration'); ?>
        </div>
    </div>

    <div class="row margin10">
        <div class="span3">
            <?php echo $form->labelEx($enrolment, 'vent_arr', ['class' => 'bold']); ?>
        </div>
        <div class="span3">
            <?php echo $form->checkBox($enrolment, 'vent_arr', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'vent_arr'); ?>
        </div>
    </div>

    <div class="row margin10">
        <div class="span3">
            <?php echo $form->labelEx($enrolment, 'vent_fibrillation', ['class' => 'bold']); ?>
        </div>
        <div class="span3">
            <?php echo $form->checkBox($enrolment, 'vent_fibrillation', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'vent_fibrillation'); ?>
        </div>
    </div>

    <div class="row margin10">
        <div class="span3">
            <?php echo $form->labelEx($enrolment, 'vent_tachy', ['class' => 'bold']); ?>
        </div>
        <div class="span3">
            <?php echo $form->checkBox($enrolment, 'vent_tachy', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'vent_tachy'); ?>
        </div>
    </div>

    <div class="row margin10">
        <div class="span3">
            <?php echo $form->labelEx($enrolment, 'vent_tachy_type', ['class' => 'bold']); ?>
        </div>
        <div class="span3">
            <?php echo $form->dropDownList($enrolment, 'vent_tachy_type', ProtocolEnrolment::model()->getVentTachyOptions(), ['empty' => '']); ?>
            <?php echo $form->error($enrolment, 'vent_tachy_type'); ?>
        </div>
    </div>

    <div class="row">
        <div class="span3">
            <?php echo $form->labelEx($enrolment, 'consciousness'); ?>
        </div>
        <div class="span3">
            <?php
                AttackConsciousness::enableOnlyActive();
                echo $form->dropDownList(
                    $enrolment,
                    'consciousness',
                    ProtocolEnrolment::model()->getConsciousnessOptions(),
                    ['empty' => '']
                );
            ?>
            <?php echo $form->error($enrolment, 'consciousness'); ?>
        </div>
    </div>

    <div class="row margin10">
        <div class="span3">
            <?php echo $form->labelEx($enrolment, 'syncope_unknown_origin'); ?>
        </div>
        <div class="span3">
            <?php echo $form->checkBox($enrolment, 'syncope_unknown_origin', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'syncope_unknown_origin'); ?>
        </div>
    </div>

    <div class="no_page_break">
    <h3><?php echo Yii::t('ProtocolsModule.protocols', "Этиология") ?></h3>

    <div class="row">
        <script>
            var etiologyTemplate = <?=CJSON::encode(Yii::app()->mustache->getTemplate("etiology")); ?>;
        </script>
        <?php echo $form->checkBoxList(
            $enrolment,
            'etiologyIds',
            $enrolment->getDefaultDiseases(),
            array("class" => "inline", "template" => "<div class='span7'>{label}</div><div class='span1'>{input}</div>", "labelOptions" => array("class" => "inline")));
            ?>
        <div class="clearfix"></div>
        <div style="margin-left: 30px;">
        <div id="etiology_container"></div>
        <?=CHtml::link(Yii::t('ProtocolsModule.protocols', "Выбрать диагноз из справочника"), '#', array( 'onclick'=>'DiseaseSelector.getInstance().show(processEtiology);return false;', 'class' => 'noprint'));?>
        </div>
        <?php echo $form->error($enrolment, 'etiology'); ?>
    </div>
    </div>
    <h3<?php if(!$enrolment->pharm):?> class="noprint"<?php endif;?>><?=Yii::t('ProtocolsModule.protocols', "Препараты, принимаемые к моменту включения в исследование") ?></h3>
    <script>
        var pharmProductTemplate = <?=CJSON::encode(Yii::app()->mustache->getTemplate("pharmProduct")); ?>;
        var pharmUnits = <?=CJSON::encode($pharmMeasureUnits); ?>;
    </script>
    <?php
    $pharmToRender = $enrolment->pharm;

    foreach ($pharmGroups as $key => $group): ?>
        <?php

        $pharmRendered = "";

        foreach ($pharmToRender as $jey => $pharmRelObject) {
            $pharmMeasureUnitsForTempl = $this->preparePharmUnits($pharmMeasureUnits, $pharmRelObject->measure);

            foreach ($group['products'] as  $key => $pharm) {
                if ($pharm->id == $pharmRelObject->pharm_product_uid) {
                    ob_start();
                    Yii::app()->mustache->render("pharmProduct", array(
                            "pharmName" => $pharm->name,
                            "pharmId" => $pharmRelObject->pharm_product_uid,
                            "dose" => $pharmRelObject->dose,
                            "measure" => $pharmMeasureUnitsForTempl
                        ));
                    $pharmRendered .= ob_get_clean();
                    unset($pharmToRender[$jey]);
                }
            }
        }
        ?>


    <div class="pharm_cover margin10">
        <?php if($pharmRendered):?>
            <span class="bolder"><?=$group['name'];?></span>
        <?php else:?>
        <div class="noprint" style="font-weight: 500;">
            <?=$group['name'];?>
        </div>
        <?php endif;?>
        <div class="noprint">
            <br />
            <?php
            echo CHtml::dropDownList("pharmSelector_" . $key, 0, array("0" => Yii::t('ProtocolsModule.protocols', "выберите препарат")) + CHtml::listData($group['products'], 'id' , 'name'), array("class" => "pharm_selector"));
            ?>
            <a href="#" class="add_pharm"><?=Yii::t('ProtocolsModule.protocols', "Добавить") ?></a>
        </div>
        <div class="pharm_container"><?=$pharmRendered?></div>
    </div>
    <div class="noprint">
        <hr />
    </div>
    <?php endforeach;?>

    <div class="row margin10">
        <div class="span10">
            <div id="otherPharmContainer">
                <?php if(!empty($pharmToRender)):?>
                <span class="only_for_print bolder"><?=Yii::t('ProtocolsModule.protocols', "Другие препараты")?>:</span>
                <?php

                foreach ($pharmToRender as $pharmRelObject) {
                    $pharmMeasureUnitsForTempl = $this->preparePharmUnits($pharmMeasureUnits, $pharmRelObject->measure);

                    echo Yii::app()->mustache->render("pharmProduct", array(
                            "pharmName" => $pharmRelObject->pharmProduct->name,
                            "pharmId" => $pharmRelObject->pharm_product_uid,
                            "dose" => $pharmRelObject->dose,
                            "measure" => $pharmMeasureUnitsForTempl
                        ));
                }
                ?>
                <?php endif;?>
            </div>
            <?=CHtml::link(
                Yii::t('ProtocolsModule.protocols', "Выбрать другой препарат из справочника"),
                '#',
                array( 'onclick'=>'PharmSelector.getInstance().show(processPharmSelected);return false;', "class" => "noprint")
            );?>
        </div>
    </div>


    <div class="no_page_break">
    <h5><?php echo Yii::t('ProtocolsModule.protocols', "Другие препараты") ?></h5>

    <div class="row margin10">
        <div class="span8">
            <?php echo $form->textArea($enrolment, 'pharm_custom', array("class" => "input-xxlarge", 'rows' => 6)); ?>
            <?php echo $form->error($enrolment, 'pharm_custom'); ?>
        </div>
    </div>
    </div>


    <div class="no_page_break">
    <h3><?php echo Yii::t('ProtocolsModule.protocols', "Операции на сердце") ?></h3>
    <div class="noprint">
        <script>
            var surgeryTemplate = <?=CJSON::encode(Yii::app()->mustache->getTemplate("surgery")); ?>;
        </script>
    <?php
    $surgeryList=Surgery::model()->withI18n("ru")->findAll();
    echo CHtml::dropDownList("surgeryTypeSelector", 0, array(0 => Yii::t('ProtocolsModule.protocols', "выберите операцию")) + CHtml::listData($surgeryList, 'uid' , 'name')); ?> <a href="#" id="add_surgery"><?=Yii::t('ProtocolsModule.protocols', "Добавить") ?></a>
    </div>

        <div id="surgeryContainer">
        <?php if($enrolment->surgeryRel): ?>
        <?php foreach ($enrolment->surgeryRel as $surgRel):?>
            <?php
                $rand = uniqid();
                Yii::app()->mustache->render("surgery", array(
                        "surgeryName" => $surgRel->surgery->name,
                        "surgeryId" => $surgRel->surgery_uid,
                        "rnd" => $rand,
                        "date" => $surgRel->date,
                        "descr" => $surgRel->descr
                    ));
            ?>
        <?php endforeach;?>
        <?php endif;?>
        </div>


    <div class="row margin10">
        <div class="span3">
            <?php echo Yii::t('ProtocolsModule.protocols', "Аритмии") ?>:
        </div>
    </div>
    <div class="row margin10">
        <div class="span1">
            <?php echo $form->labelEx($enrolment, 'surg_arr_rfa'); ?>
        </div>
        <div class="span1">
            <?php echo $form->checkBox($enrolment, 'surg_arr_rfa', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'surg_arr_rfa'); ?>
        </div>
        <div class="span1">
            <?php echo $form->labelEx($enrolment, 'surg_arr'); ?>
        </div>
        <div class="span1">
            <?php echo $form->checkBox($enrolment, 'surg_arr', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'surg_arr'); ?>
        </div>
    </div>
    </div>

    <div class="no_page_break">

    <h3><?php echo Yii::t('ProtocolsModule.protocols', "Оценка клинической нагрузки") ?></h3>
    <h4><?php echo Yii::t('ProtocolsModule.protocols', "Клиническая нагрузка") ?></h4>

    <div class="row margin10">
        <div class="span5">
            <?php echo $form->labelEx($enrolment, 'hispital_days'); ?>
        </div>
        <div class="span3">
            <?php echo $form->numberField($enrolment, 'hispital_days', array("class" => "input-small")); ?>
            <?php echo $form->error($enrolment, 'hispital_days'); ?>
        </div>
    </div>


    <div class="row">
        <div class="span5">
            <?php echo $form->labelEx($enrolment, 'sick_days'); ?>
        </div>
        <div class="span3">
            <?php echo $form->numberField($enrolment, 'sick_days', array("class" => "input-small")); ?>
            <?php echo $form->error($enrolment, 'sick_days'); ?>
        </div>
    </div>

    <div class="row">
        <div class="span5">
            <?php echo $form->labelEx($enrolment, 'ambulance_calls'); ?>
        </div>
        <div class="span3">
            <?php echo $form->numberField($enrolment, 'ambulance_calls', array("class" => "input-small")); ?>
            <?php echo $form->error($enrolment, 'ambulance_calls'); ?>
        </div>
    </div>

    <div class="no_page_break">
    <h4><?php echo Yii::t('ProtocolsModule.protocols', "Клиническое состояние пациента") ?></h4>

    <div class="row">
    <div class="span5">
        <?php echo $form->labelEx($enrolment, 'disease_complications'); ?>
    </div>
    </div>
    <div class="row">
    <div class="span5">
        <?php echo $form->textArea($enrolment, 'disease_complications', array("class" => "input-xxlarge", "rows" => 6)); ?>
        <?php echo $form->error($enrolment, 'disease_complications'); ?>
    </div>
    </div>
    </div>

    </div>

    <h3><?php echo Yii::t('ProtocolsModule.protocols', "Показания к имплантации") ?></h3>


    <div class="row">
    <div class="span5">
        <h4><?php echo Yii::t('ProtocolsModule.protocols', "ЭКС") ?></h4>
        <?php echo $form->checkBox($enrolment, 'impl_sssu', array("class" => "inline")); ?>
        <?php echo $form->labelEx($enrolment, 'impl_sssu', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment, 'impl_sssu'); ?>
        <br />
        <?php echo $form->checkBox($enrolment, 'impl_av_block_perm', array("class" => "inline")); ?>
        <?php echo $form->labelEx($enrolment, 'impl_av_block_perm', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment, 'impl_av_block_perm'); ?>
        <br />
        <?php echo $form->checkBox($enrolment, 'impl_av_block_trans', array("class" => "inline")); ?>
        <?php echo $form->labelEx($enrolment, 'impl_av_block_trans', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment, 'impl_av_block_trans'); ?>
        <br />
        <?php echo $form->checkBox($enrolment, 'impl_brady', array("class" => "inline")); ?>
        <?php echo $form->labelEx($enrolment, 'impl_brady', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment, 'impl_brady'); ?>
        <br />
        <?php echo $form->checkBox($enrolment, 'impl_binodal_av_perm', array("class" => "inline")); ?>
        <?php echo $form->labelEx($enrolment, 'impl_binodal_av_perm', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment, 'impl_binodal_av_perm'); ?>
        <br />
        <?php echo $form->checkBox($enrolment, 'impl_binodal_av_trans', array("class" => "inline")); ?>
        <?php echo $form->labelEx($enrolment, 'impl_binodal_av_trans', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment, 'impl_binodal_av_trans'); ?>
        <br />
        <?php echo $form->checkBox($enrolment, 'impl_vaso_syncope', array("class" => "inline")); ?>
        <?php echo $form->labelEx($enrolment, 'impl_vaso_syncope', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment, 'impl_vaso_syncope'); ?>

    </div>
    <div class="span5">
        <h4><?php echo Yii::t('ProtocolsModule.protocols', "ИКД, CRT") ?></h4>
        <?php echo $form->checkBox($enrolment, 'impl_prime_prevent', array("class" => "inline")); ?>
        <?php echo $form->labelEx($enrolment, 'impl_prime_prevent', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment, 'impl_prime_prevent'); ?>
        <br />
        <?php echo $form->checkBox($enrolment, 'impl_second_prevent', array("class" => "inline")); ?>
        <?php echo $form->labelEx($enrolment, 'impl_second_prevent', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment, 'impl_second_prevent'); ?>
        <br />
        <?php echo $form->checkBox($enrolment, 'impl_heart_failure', array("class" => "inline")); ?>
        <?php echo $form->labelEx($enrolment, 'impl_heart_failure', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment, 'impl_heart_failure'); ?>
        <br />
        <br />

    </div>
    </div>


    <div class="row margin10">
    <div class="span2">
        <?php echo $form->labelEx($enrolment, 'impl_other_ind'); ?>
    </div>
    <div class="span7">
        <?php echo $form->textArea($enrolment, 'impl_other_ind', array("rows" => 6, "class" => "input-xxlarge")); ?>
        <?php echo $form->error($enrolment, 'impl_other_ind'); ?>
    </div>
    </div>

    <div class="no_page_break">

    <h4><?php echo Yii::t('ProtocolsModule.protocols', "ИМПЛАНТАЦИЯ стимулирующей системы") ?></h4>

    <div class="row">
    <div class="span1">
    </div>
        <div class="span1">
        <?php echo $form->labelEx($enrolment, 'impl_pacer', array("class" => "bold inline")); ?>
        <?php echo $form->checkBox($enrolment, 'impl_pacer', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment, 'impl_pacer'); ?>
    </div>
    <div class="span1">
        <?php echo $form->labelEx($enrolment, 'impl_crt_p', array("class" => "bold inline")); ?>
        <?php echo $form->checkBox($enrolment, 'impl_crt_p', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment, 'impl_crt_p'); ?>
    </div>
    <div class="span1">
        <?php echo $form->labelEx($enrolment, 'impl_crt_d', array("class" => "bold inline")); ?>
        <?php echo $form->checkBox($enrolment, 'impl_crt_d', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment, 'impl_crt_d'); ?>
    </div>
    <div class="span1">
        <?php echo $form->labelEx($enrolment, 'impl_idc', array("class" => "bold inline")); ?>
        <?php echo $form->checkBox($enrolment, 'impl_idc', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment, 'impl_idc'); ?>
    </div>
    </div>

    <div class="row margin10">
    <div class="span4 print_no_inline">
        <?php echo $form->labelEx($enrolment, 'impl_model', array("class" => "inline")); ?>
        <?php echo $form->textField($enrolment, 'impl_model', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment, 'impl_model'); ?>
    </div>
    <div class="span4 print_no_inline">
        <?php echo $form->labelEx($enrolment, 'impl_model_sn', array("class" => "inline")); ?>
        <?php echo $form->textField($enrolment, 'impl_model_sn', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment, 'impl_model_sn'); ?>
    </div>
    </div>

    <div class="row">
    <div class="span4 print_no_inline">
        <?php echo $form->labelEx($enrolment, 'impl_model_idc', array("class" => "inline")); ?>
        <?php echo $form->textField($enrolment, 'impl_model_idc', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment, 'impl_model_idc'); ?>
    </div>
    <div class="span4 print_no_inline">
        <?php echo $form->labelEx($enrolment, 'impl_model_sn_idc', array("class" => "inline")); ?>
        <?php echo $form->textField($enrolment, 'impl_model_sn_idc', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment, 'impl_model_sn_idc'); ?>
    </div>
    </div>

    <div class="row">
    <div class="span4 print_no_inline">
        <?php echo $form->labelEx($enrolment, 'impl_pacer_model', array("class" => "inline")); ?>
        <?php echo $form->textField($enrolment, 'impl_pacer_model', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment, 'impl_pacer_model'); ?>
    </div>
    <div class="span4 print_no_inline">
        <?php echo $form->labelEx($enrolment, 'impl_pacer_sn', array("class" => "inline")); ?>
        <?php echo $form->textField($enrolment, 'impl_pacer_sn', array("class" => "inline")); ?>
        <?php echo $form->error($enrolment, 'impl_pacer_sn'); ?>
    </div>
    </div>

    </div>

    <div class="no_page_break">

    <div class="row margin10">
        <div class="span3">
            <strong><?php echo Yii::t('ProtocolsModule.protocols', "Предсердный электрод") ?></strong>
        </div>
    </div>

    <div class="row margin10">
        <div class="span4 print_no_inline">
            <?php echo $form->labelEx($enrolment, 'ae_model', array("class" => "inline")); ?>
            <?php echo $form->textField($enrolment, 'ae_model', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'ae_model'); ?>
        </div>
        <div class="span4 print_no_inline">
            <?php echo $form->labelEx($enrolment, 'ae_sn', array("class" => "inline")); ?>
            <?php echo $form->textField($enrolment, 'ae_sn', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'ae_sn'); ?>
        </div>
    </div>

    <div class="row margin10">
        <div class="span4">
            <?php echo $form->labelEx($enrolment, 'ae_conf', array("class" => "inline")); ?>
        </div>
        <div class="span5">
            <?php echo $form->dropDownList($enrolment, 'ae_conf', ProtocolEnrolment::model()->getImplConfOptions()); ?>
            <?php echo $form->error($enrolment, 'ae_conf'); ?>
        </div>
    </div>

    <div class="row ">
        <div class="span4">
            <?php echo $form->labelEx($enrolment, 'ae_pacing_threshold'); ?>
        </div>
        <div class="span5">
            <?php echo $form->textField($enrolment, 'ae_pacing_threshold', array("class" => "input-mini decimal_field")); ?>
            <?php echo $form->error($enrolment, 'ae_pacing_threshold'); ?>
        </div>
    </div>

    <div class="row ">
        <div class="span4">
            <?php echo $form->labelEx($enrolment, 'ae_pacing_threshold_dur'); ?>
        </div>
        <div class="span5">
            <?php echo $form->textField($enrolment, 'ae_pacing_threshold_dur', array("class" => "input-mini decimal_field")); ?>
            <?php echo $form->error($enrolment, 'ae_pacing_threshold_dur'); ?>
        </div>
    </div>

    <div class="row ">
        <div class="span4">
            <?php echo $form->labelEx($enrolment, 'ae_pulse_ampl'); ?>
        </div>
        <div class="span5">
            <?php echo $form->textField($enrolment, 'ae_pulse_ampl', array("class" => "input-mini decimal_field")); ?>
            <?php echo $form->error($enrolment, 'ae_pulse_ampl'); ?>
        </div>
    </div>

    <div class="row ">
        <div class="span4">
            <?php echo $form->labelEx($enrolment, 'ae_pacing_imp'); ?>
        </div>
        <div class="span5">
            <?php echo $form->numberField($enrolment, 'ae_pacing_imp', array("class" => "input-mini")); ?>
            <?php echo $form->error($enrolment, 'ae_pacing_imp'); ?>
        </div>
    </div>

    </div>
    <div class="no_page_break">

    <div class="row margin20">
        <div class="span4">
        <?php echo $form->labelEx($enrolment, 've_model', array("class" => "bold")); ?>
        </div>
        <div class="span5">
        <?php echo $form->textField($enrolment, 've_model'); ?>
        <?php echo $form->error($enrolment, 've_model'); ?>
        </div>
    </div>

    <div class="row">
        <div class="span4">
        <?php echo $form->labelEx($enrolment, 've_sn'); ?>
        </div>
        <div class="span5">
        <?php echo $form->textField($enrolment, 've_sn'); ?>
        <?php echo $form->error($enrolment, 've_sn'); ?>
        </div>
    </div>

    <div class="row">
        <div class="span4">
            <?php echo $form->labelEx($enrolment, 've_conf'); ?>
        </div>
        <div class="span5">
            <?php echo $form->dropDownList($enrolment, 've_conf', ProtocolEnrolment::model()->getImplConfOptions()); ?>
            <?php echo $form->error($enrolment, 've_conf'); ?>
        </div>
    </div>

    <div class="row">
        <div class="span4">
            <?php echo $form->labelEx($enrolment, 've_stimul_threshold'); ?>
        </div>
        <div class="span5">
            <?php echo $form->textField($enrolment, 've_stimul_threshold', array("class" => "input-mini decimal_field")); ?>
            <?php echo $form->error($enrolment, 've_stimul_threshold'); ?>
        </div>
    </div>

    <div class="row">
        <div class="span4">
            <?php echo $form->labelEx($enrolment, 've_stimul_dur'); ?>
        </div>
        <div class="span5">
            <?php echo $form->textField($enrolment, 've_stimul_dur', array("class" => "input-mini decimal_field")); ?>
            <?php echo $form->error($enrolment, 've_stimul_dur'); ?>
        </div>
    </div>

    <div class="row">
        <div class="span4">
            <?php echo $form->labelEx($enrolment, 've_wave_apl'); ?>
        </div>
        <div class="span5">
            <?php echo $form->textField($enrolment, 've_wave_apl', array("class" => "input-mini decimal_field")); ?>
            <?php echo $form->error($enrolment, 've_wave_apl'); ?>
        </div>
    </div>

    <div class="row">
        <div class="span4">
            <?php echo $form->labelEx($enrolment, 've_imp'); ?>
        </div>
        <div class="span5">
            <?php echo $form->numberField($enrolment, 've_imp', array("class" => "input-mini")); ?>
            <?php echo $form->error($enrolment, 've_imp'); ?>
        </div>
    </div>

    </div>
    <div class="no_page_break">

    <div class="row margin20">
        <div class="span3">
            <?php echo $form->labelEx($enrolment, 'sve', array("class" => "bold")); ?>
        </div>
        <div class="span5">
            <?php echo $form->checkBox($enrolment, 'sve'); ?>
            <?php echo $form->error($enrolment, 'sve'); ?>
        </div>
    </div>

    <div class="row">
        <div class="span3">
            <?php echo $form->labelEx($enrolment, 'sve_imp'); ?>
        </div>
        <div class="span5">
            <?php echo $form->numberField($enrolment, 'sve_imp', array("class" => "input-mini")); ?>
            <?php echo $form->error($enrolment, 'sve_imp'); ?>
        </div>
    </div>

    </div>
    <div class="no_page_break">

    <div class="row margin20">
        <div class="span4">
            <?php echo $form->labelEx($enrolment, 'pacer_model', array("class" => "bold")); ?>
        </div>
        <div class="span5">
            <?php echo $form->textField($enrolment, 'pacer_model'); ?>
            <?php echo $form->error($enrolment, 'pacer_model'); ?>
        </div>
    </div>

    <div class="row">
        <div class="span4">
            <?php echo $form->labelEx($enrolment, 'pacer_sn'); ?>
        </div>
        <div class="span5">
            <?php echo $form->textField($enrolment, 'pacer_sn'); ?>
            <?php echo $form->error($enrolment, 'pacer_sn'); ?>
        </div>
    </div>

    <div class="row">
        <div class="span4">
            <?php echo $form->labelEx($enrolment, 'sve_success'); ?>
        </div>
        <div class="span5">
            <?php echo $form->checkBox($enrolment, 'sve_success'); ?>
            <?php echo $form->error($enrolment, 'sve_success'); ?>
        </div>
    </div>

    <div class="row margin10">
        <div class="span4">
            <?php echo $form->labelEx($enrolment, 'pacer_conf'); ?>
        </div>
        <div class="span5">
            <?php echo $form->dropDownList($enrolment, 'pacer_conf', ProtocolEnrolment::model()->getImplConfOptions()); ?>
            <?php echo $form->error($enrolment, 'pacer_conf'); ?>
        </div>
    </div>

    <div class="row">
        <div class="span4">
            <?php echo $form->labelEx($enrolment, 'pacer_stimul_threshold'); ?>
        </div>
        <div class="span5">
            <?php echo $form->textField($enrolment, 'pacer_stimul_threshold', array("class" => "input-mini decimal_field")); ?>
            <?php echo $form->error($enrolment, 'pacer_stimul_threshold'); ?>
        </div>
    </div>

    <div class="row">
        <div class="span4">
            <?php echo $form->labelEx($enrolment, 'pacer_stimul_dur'); ?>
        </div>
        <div class="span5">
            <?php echo $form->textField($enrolment, 'pacer_stimul_dur', array("class" => "input-mini decimal_field")); ?>
            <?php echo $form->error($enrolment, 'pacer_stimul_dur'); ?>
        </div>
    </div>

    <div class="row">
        <div class="span4">
            <?php echo $form->labelEx($enrolment, 'pacer_ampl'); ?>
        </div>
        <div class="span5">
            <?php echo $form->textField($enrolment, 'pacer_ampl', array("class" => "input-mini decimal_field")); ?>
            <?php echo $form->error($enrolment, 'pacer_ampl'); ?>
        </div>
    </div>

    <div class="row">
        <div class="span4">
            <?php echo $form->labelEx($enrolment, 'pacer_imp'); ?>
        </div>
        <div class="span5">
            <?php echo $form->numberField($enrolment, 'pacer_imp', array("class" => "input-mini")); ?>
            <?php echo $form->error($enrolment, 'pacer_imp'); ?>
        </div>
    </div>


    <div class="row">
        <div class="span2">
            <?php echo $form->labelEx($enrolment, 'pacer_placement'); ?>
        </div>
        <div class="span5">
            <?php echo $form->dropDownList($enrolment, 'pacer_placement', array(0 => "") + CHtml::listData(ProtocolEnrolment::model()->getPlacementOptions(), 'uid' , 'name')); ?>
            <?php echo $form->error($enrolment, 'pacer_placement'); ?>
        </div>
    </div>

    </div>
    <div class="no_page_break">

        <h3><?php echo Yii::t('ProtocolsModule.protocols', "Пациенту выдан трансмиттер") ?></h3>

    <div class="row">
        <div class="span2">
            <?php echo $form->labelEx($enrolment, 'trans_uid'); ?>
        </div>
        <div class="span5">
            <?php echo $form->dropDownList($enrolment, 'trans_uid', CHtml::listData(Transmitter::getList(), 'uid' , 'name')); ?>
            <?php echo $form->error($enrolment, 'trans_uid'); ?>
        </div>
    </div>

    <div class="row">
        <div class="span2">
            <?php echo $form->labelEx($enrolment, 'trans_sn'); ?>
        </div>
        <div class="span5">
            <?php echo $form->textField($enrolment, 'trans_sn'); ?>
            <?php echo $form->error($enrolment, 'trans_sn'); ?>
        </div>
    </div>

    <div class="row">
        <div class="span6">
            <?php echo $form->labelEx($enrolment, 'trans_reg_date'); ?>
            <?php echo $form->dateField($enrolment, 'trans_reg_date'); ?>
            <?php echo $form->error($enrolment, 'trans_reg_date'); ?>
        </div>
    </div>


    <div class="row">
        <div class="span1">
            <?php echo $form->checkBox($enrolment, 'trans_guide_granted', array("class" => "inline")); ?>
        </div>
        <div class="span6">
            <?php echo $form->labelEx($enrolment, 'trans_guide_granted', array("class" => "inline")); ?>
            <?php echo $form->error($enrolment, 'trans_guide_granted'); ?>
        </div>
    </div>

    </div>

    <div class="row margin30">
        <div class="span5">
            <?php echo $form->labelEx($enrolment, 'form_hm_signed', ['class' => 'bold']); ?>
            <?php echo $form->error($enrolment, 'form_hm_signed'); ?>
        </div>
        <div class="span2">
            <?php echo $form->checkBox($enrolment, 'form_hm_signed', array("class" => "inline")); ?>
        </div>
    </div>

    <div class="row">
        <div class="span5">
            <?php echo $form->labelEx($enrolment, 'form_rh_signed', ['class' => 'bold']); ?>
            <?php echo $form->error($enrolment, 'form_rh_signed'); ?>
        </div>
        <div class="span2">
            <?php echo $form->checkBox($enrolment, 'form_rh_signed', array("class" => "inline")); ?>
        </div>
    </div>

    <?php $this->widget("ProtocolSignature", array("protocol" => $protocol))?>

    <!--  /ФОРМА ДАННЫХ ПРОТОКОЛА -->

    <?php $this->widget("ProtocolFooterButtons", [
            "protocol" => $protocol,
            "research" => $research,
            "patient" => $research->patient,
        ]);?>



    <?php $this->endWidget();?>

</div><!-- form -->