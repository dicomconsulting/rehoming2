<?php
/* @var $this HistoryController */
/* @var $research Research */
/* @var $history ProtocolHistory[]*/

?>

<h3><?=Yii::t('ProtocolsModule.protocols', "История изменений всех протоколов по исследованию") ?></h3>
<?php if($history):?>
<div class="rh_table_grid">
    <table class="items">
        <tr>
            <th style="width: 100px;"><?= Yii::t('ProtocolsModule.protocols', 'Дата')?></th>
            <th style="width: 100px;"><?= Yii::t('ProtocolsModule.protocols', 'Протокол')?></th>
            <th style="width: 150px;"><?= Yii::t('ProtocolsModule.protocols', 'Исследователь')?></th>
            <th style="width: 200px;"><?= Yii::t('ProtocolsModule.protocols', 'Действие')?></th>
            <th style="width: 40px;"><?= Yii::t('ProtocolsModule.protocols', 'Удален')?></th>
        </tr>
        <?php foreach ($history as $row):?>
            <tr style="text-align: center;<?php if($row->archive): ?> background-color: #d3d3d3;<?php endif; ?>">
                <td class="date_create_cell"><?=$row->event_time->format(RhDateTime::RH_DATETIME); ?></td>
                <td><?=$row->protocolDictionary->name; ?></td>
                <td><?=$row->user->getDisplayName(); ?></td>
                <td><?=$row->getEventLabel($row->operation_id); ?></td>
                <td><?php if($row->archive): ?><?=Yii::t("ProtocolsModule.protocols", "Да");?><?php else:?><?=Yii::t("ProtocolsModule.protocols", "Нет");?><?php endif; ?></td>
            </tr>
        <?php endforeach;?>
    </table>
</div>
<?php else:?>
    <p><?= Yii::t('ProtocolsModule.protocols', 'По данному исследованию отсутствует история операций')?></p>
<?php endif;?>
