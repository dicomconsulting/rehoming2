<?php
/* @var $this HistoryController */
/* @var $protocol Protocol */
/* @var $history ProtocolHistory[]*/

?>

<h3><?=Yii::t('ProtocolsModule.protocols', "История изменений протокола") ?> "<?=$protocol->protocolDictionary->name?>"</h3>
<?php if($history):?>
<div class="rh_table_grid">
    <table class="items">
        <tr>
            <th style="width: 100px;"><?= Yii::t('ProtocolsModule.protocols', 'Дата')?></th>
            <th style="width: 150px;"><?= Yii::t('ProtocolsModule.protocols', 'Исследователь')?></th>
            <th style="width: 200px;"><?= Yii::t('ProtocolsModule.protocols', 'Действие')?></th>
        </tr>
        <?php foreach ($history as $row):?>
            <tr style="text-align: center;">
                <td class="date_create_cell"><?=$row->event_time->format(RhDateTime::RH_DATETIME); ?></td>
                <td><?=$row->user->getDisplayName(); ?></td>
                <td><?=$row->getEventLabel($row->operation_id); ?></td>
            </tr>
        <?php endforeach;?>
    </table>
</div>
<?php else:?>
    <p><?= Yii::t('ProtocolsModule.protocols', 'По данному протоколу отсутствует история операций')?></p>
<?php endif;?>
