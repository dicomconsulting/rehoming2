<?php
/* @var $this EnrolmentController */
/* @var $model ProtocolEnrolment */
/* @var $formEnrolment CActiveForm */
?>

<div class="form">

<?php $formEnrolment=$this->beginWidget('CActiveForm', array(
	'id'=>'protocol-enrolment-enrolment-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
));
?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $formEnrolment->errorSummary($model); ?>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'protocol_uid'); ?>
		<?php echo $formEnrolment->textField($model,'protocol_uid'); ?>
		<?php echo $formEnrolment->error($model,'protocol_uid'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'patient_height'); ?>
		<?php echo $formEnrolment->textField($model,'patient_height'); ?>
		<?php echo $formEnrolment->error($model,'patient_height'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'enrolment_pacer'); ?>
		<?php echo $formEnrolment->textField($model,'enrolment_pacer'); ?>
		<?php echo $formEnrolment->error($model,'enrolment_pacer'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'enrolment_crt'); ?>
		<?php echo $formEnrolment->textField($model,'enrolment_crt'); ?>
		<?php echo $formEnrolment->error($model,'enrolment_crt'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'enrolment_icd'); ?>
		<?php echo $formEnrolment->textField($model,'enrolment_icd'); ?>
		<?php echo $formEnrolment->error($model,'enrolment_icd'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'enrolment_tele'); ?>
		<?php echo $formEnrolment->textField($model,'enrolment_tele'); ?>
		<?php echo $formEnrolment->error($model,'enrolment_tele'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'enrolment_able'); ?>
		<?php echo $formEnrolment->textField($model,'enrolment_able'); ?>
		<?php echo $formEnrolment->error($model,'enrolment_able'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'enrolment_agree'); ?>
		<?php echo $formEnrolment->textField($model,'enrolment_agree'); ?>
		<?php echo $formEnrolment->error($model,'enrolment_agree'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'enrolment_gsm'); ?>
		<?php echo $formEnrolment->textField($model,'enrolment_gsm'); ?>
		<?php echo $formEnrolment->error($model,'enrolment_gsm'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'ca_block'); ?>
		<?php echo $formEnrolment->textField($model,'ca_block'); ?>
		<?php echo $formEnrolment->error($model,'ca_block'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'ca_block_grade'); ?>
		<?php echo $formEnrolment->textField($model,'ca_block_grade'); ?>
		<?php echo $formEnrolment->error($model,'ca_block_grade'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'atrial_arr'); ?>
		<?php echo $formEnrolment->textField($model,'atrial_arr'); ?>
		<?php echo $formEnrolment->error($model,'atrial_arr'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'atrial_tachy'); ?>
		<?php echo $formEnrolment->textField($model,'atrial_tachy'); ?>
		<?php echo $formEnrolment->error($model,'atrial_tachy'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'atrial_fibrillation'); ?>
		<?php echo $formEnrolment->textField($model,'atrial_fibrillation'); ?>
		<?php echo $formEnrolment->error($model,'atrial_fibrillation'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'atrial_flutter'); ?>
		<?php echo $formEnrolment->textField($model,'atrial_flutter'); ?>
		<?php echo $formEnrolment->error($model,'atrial_flutter'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'tachy_history_duration'); ?>
		<?php echo $formEnrolment->textField($model,'tachy_history_duration'); ?>
		<?php echo $formEnrolment->error($model,'tachy_history_duration'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'tachy_longest_episode'); ?>
		<?php echo $formEnrolment->textField($model,'tachy_longest_episode'); ?>
		<?php echo $formEnrolment->error($model,'tachy_longest_episode'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'tachy_shortest_episode'); ?>
		<?php echo $formEnrolment->textField($model,'tachy_shortest_episode'); ?>
		<?php echo $formEnrolment->error($model,'tachy_shortest_episode'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'tachy_frequency'); ?>
		<?php echo $formEnrolment->textField($model,'tachy_frequency'); ?>
		<?php echo $formEnrolment->error($model,'tachy_frequency'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'ab_block'); ?>
		<?php echo $formEnrolment->textField($model,'ab_block'); ?>
		<?php echo $formEnrolment->error($model,'ab_block'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'ab_block_grade'); ?>
		<?php echo $formEnrolment->textField($model,'ab_block_grade'); ?>
		<?php echo $formEnrolment->error($model,'ab_block_grade'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'left_branch_block'); ?>
		<?php echo $formEnrolment->textField($model,'left_branch_block'); ?>
		<?php echo $formEnrolment->error($model,'left_branch_block'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'right_branch_block'); ?>
		<?php echo $formEnrolment->textField($model,'right_branch_block'); ?>
		<?php echo $formEnrolment->error($model,'right_branch_block'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'both_branch_block'); ?>
		<?php echo $formEnrolment->textField($model,'both_branch_block'); ?>
		<?php echo $formEnrolment->error($model,'both_branch_block'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'qrs_duration'); ?>
		<?php echo $formEnrolment->textField($model,'qrs_duration'); ?>
		<?php echo $formEnrolment->error($model,'qrs_duration'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'vent_arr'); ?>
		<?php echo $formEnrolment->textField($model,'vent_arr'); ?>
		<?php echo $formEnrolment->error($model,'vent_arr'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'vent_fibrillation'); ?>
		<?php echo $formEnrolment->textField($model,'vent_fibrillation'); ?>
		<?php echo $formEnrolment->error($model,'vent_fibrillation'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'vent_tachy'); ?>
		<?php echo $formEnrolment->textField($model,'vent_tachy'); ?>
		<?php echo $formEnrolment->error($model,'vent_tachy'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'vent_tachy_type'); ?>
		<?php echo $formEnrolment->textField($model,'vent_tachy_type'); ?>
		<?php echo $formEnrolment->error($model,'vent_tachy_type'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'syncope_unknown_origin'); ?>
		<?php echo $formEnrolment->textField($model,'syncope_unknown_origin'); ?>
		<?php echo $formEnrolment->error($model,'syncope_unknown_origin'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'surg_arr_rfa'); ?>
		<?php echo $formEnrolment->textField($model,'surg_arr_rfa'); ?>
		<?php echo $formEnrolment->error($model,'surg_arr_rfa'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'surg_arr'); ?>
		<?php echo $formEnrolment->textField($model,'surg_arr'); ?>
		<?php echo $formEnrolment->error($model,'surg_arr'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'hispital_days'); ?>
		<?php echo $formEnrolment->textField($model,'hispital_days'); ?>
		<?php echo $formEnrolment->error($model,'hispital_days'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'sick_days'); ?>
		<?php echo $formEnrolment->textField($model,'sick_days'); ?>
		<?php echo $formEnrolment->error($model,'sick_days'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'ambulance_calls'); ?>
		<?php echo $formEnrolment->textField($model,'ambulance_calls'); ?>
		<?php echo $formEnrolment->error($model,'ambulance_calls'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'impl_sssu'); ?>
		<?php echo $formEnrolment->textField($model,'impl_sssu'); ?>
		<?php echo $formEnrolment->error($model,'impl_sssu'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'impl_av_block_perm'); ?>
		<?php echo $formEnrolment->textField($model,'impl_av_block_perm'); ?>
		<?php echo $formEnrolment->error($model,'impl_av_block_perm'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'impl_av_block_trans'); ?>
		<?php echo $formEnrolment->textField($model,'impl_av_block_trans'); ?>
		<?php echo $formEnrolment->error($model,'impl_av_block_trans'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'impl_brady'); ?>
		<?php echo $formEnrolment->textField($model,'impl_brady'); ?>
		<?php echo $formEnrolment->error($model,'impl_brady'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'impl_binodal_av_perm'); ?>
		<?php echo $formEnrolment->textField($model,'impl_binodal_av_perm'); ?>
		<?php echo $formEnrolment->error($model,'impl_binodal_av_perm'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'impl_binodal_av_trans'); ?>
		<?php echo $formEnrolment->textField($model,'impl_binodal_av_trans'); ?>
		<?php echo $formEnrolment->error($model,'impl_binodal_av_trans'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'impl_vaso_syncope'); ?>
		<?php echo $formEnrolment->textField($model,'impl_vaso_syncope'); ?>
		<?php echo $formEnrolment->error($model,'impl_vaso_syncope'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'impl_prime_prevent'); ?>
		<?php echo $formEnrolment->textField($model,'impl_prime_prevent'); ?>
		<?php echo $formEnrolment->error($model,'impl_prime_prevent'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'impl_second_prevent'); ?>
		<?php echo $formEnrolment->textField($model,'impl_second_prevent'); ?>
		<?php echo $formEnrolment->error($model,'impl_second_prevent'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'impl_pacer'); ?>
		<?php echo $formEnrolment->textField($model,'impl_pacer'); ?>
		<?php echo $formEnrolment->error($model,'impl_pacer'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'impl_crt'); ?>
		<?php echo $formEnrolment->textField($model,'impl_crt'); ?>
		<?php echo $formEnrolment->error($model,'impl_crt'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'impl_idc'); ?>
		<?php echo $formEnrolment->textField($model,'impl_idc'); ?>
		<?php echo $formEnrolment->error($model,'impl_idc'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'ae'); ?>
		<?php echo $formEnrolment->textField($model,'ae'); ?>
		<?php echo $formEnrolment->error($model,'ae'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'ae_conf'); ?>
		<?php echo $formEnrolment->textField($model,'ae_conf'); ?>
		<?php echo $formEnrolment->error($model,'ae_conf'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'ae_pacing_imp'); ?>
		<?php echo $formEnrolment->textField($model,'ae_pacing_imp'); ?>
		<?php echo $formEnrolment->error($model,'ae_pacing_imp'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'ve_conf'); ?>
		<?php echo $formEnrolment->textField($model,'ve_conf'); ?>
		<?php echo $formEnrolment->error($model,'ve_conf'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'ve_imp'); ?>
		<?php echo $formEnrolment->textField($model,'ve_imp'); ?>
		<?php echo $formEnrolment->error($model,'ve_imp'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'sve'); ?>
		<?php echo $formEnrolment->textField($model,'sve'); ?>
		<?php echo $formEnrolment->error($model,'sve'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'sve_imp'); ?>
		<?php echo $formEnrolment->textField($model,'sve_imp'); ?>
		<?php echo $formEnrolment->error($model,'sve_imp'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'sve_success'); ?>
		<?php echo $formEnrolment->textField($model,'sve_success'); ?>
		<?php echo $formEnrolment->error($model,'sve_success'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'pacer_conf'); ?>
		<?php echo $formEnrolment->textField($model,'pacer_conf'); ?>
		<?php echo $formEnrolment->error($model,'pacer_conf'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'pacer_imp'); ?>
		<?php echo $formEnrolment->textField($model,'pacer_imp'); ?>
		<?php echo $formEnrolment->error($model,'pacer_imp'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'trans_uid'); ?>
		<?php echo $formEnrolment->textField($model,'trans_uid'); ?>
		<?php echo $formEnrolment->error($model,'trans_uid'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'trans_guide_granted'); ?>
		<?php echo $formEnrolment->textField($model,'trans_guide_granted'); ?>
		<?php echo $formEnrolment->error($model,'trans_guide_granted'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'patient_weight'); ?>
		<?php echo $formEnrolment->textField($model,'patient_weight'); ?>
		<?php echo $formEnrolment->error($model,'patient_weight'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'ca_pause_duration'); ?>
		<?php echo $formEnrolment->textField($model,'ca_pause_duration'); ?>
		<?php echo $formEnrolment->error($model,'ca_pause_duration'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'arr_cupped'); ?>
		<?php echo $formEnrolment->textField($model,'arr_cupped'); ?>
		<?php echo $formEnrolment->error($model,'arr_cupped'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'ab_pause_duration'); ?>
		<?php echo $formEnrolment->textField($model,'ab_pause_duration'); ?>
		<?php echo $formEnrolment->error($model,'ab_pause_duration'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'consciousness'); ?>
		<?php echo $formEnrolment->textField($model,'consciousness'); ?>
		<?php echo $formEnrolment->error($model,'consciousness'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'ae_pacing_threshold'); ?>
		<?php echo $formEnrolment->textField($model,'ae_pacing_threshold'); ?>
		<?php echo $formEnrolment->error($model,'ae_pacing_threshold'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'ae_pulse_ampl'); ?>
		<?php echo $formEnrolment->textField($model,'ae_pulse_ampl'); ?>
		<?php echo $formEnrolment->error($model,'ae_pulse_ampl'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'ve_stimul_threshold'); ?>
		<?php echo $formEnrolment->textField($model,'ve_stimul_threshold'); ?>
		<?php echo $formEnrolment->error($model,'ve_stimul_threshold'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'ve_stimul_dur'); ?>
		<?php echo $formEnrolment->textField($model,'ve_stimul_dur'); ?>
		<?php echo $formEnrolment->error($model,'ve_stimul_dur'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'ve_wave_apl'); ?>
		<?php echo $formEnrolment->textField($model,'ve_wave_apl'); ?>
		<?php echo $formEnrolment->error($model,'ve_wave_apl'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'pacer_stimul_threshold'); ?>
		<?php echo $formEnrolment->textField($model,'pacer_stimul_threshold'); ?>
		<?php echo $formEnrolment->error($model,'pacer_stimul_threshold'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'pacer_stimul_dur'); ?>
		<?php echo $formEnrolment->textField($model,'pacer_stimul_dur'); ?>
		<?php echo $formEnrolment->error($model,'pacer_stimul_dur'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'pacer_ampl'); ?>
		<?php echo $formEnrolment->textField($model,'pacer_ampl'); ?>
		<?php echo $formEnrolment->error($model,'pacer_ampl'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'patient_nyha'); ?>
		<?php echo $formEnrolment->textField($model,'patient_nyha'); ?>
		<?php echo $formEnrolment->error($model,'patient_nyha'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'impl_model'); ?>
		<?php echo $formEnrolment->textField($model,'impl_model'); ?>
		<?php echo $formEnrolment->error($model,'impl_model'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'impl_model_sn'); ?>
		<?php echo $formEnrolment->textField($model,'impl_model_sn'); ?>
		<?php echo $formEnrolment->error($model,'impl_model_sn'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'ve_model'); ?>
		<?php echo $formEnrolment->textField($model,'ve_model'); ?>
		<?php echo $formEnrolment->error($model,'ve_model'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'ve_sn'); ?>
		<?php echo $formEnrolment->textField($model,'ve_sn'); ?>
		<?php echo $formEnrolment->error($model,'ve_sn'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'pacer_sn'); ?>
		<?php echo $formEnrolment->textField($model,'pacer_sn'); ?>
		<?php echo $formEnrolment->error($model,'pacer_sn'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'trans_sn'); ?>
		<?php echo $formEnrolment->textField($model,'trans_sn'); ?>
		<?php echo $formEnrolment->error($model,'trans_sn'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'visit_date'); ?>
		<?php echo $formEnrolment->textField($model,'visit_date'); ?>
		<?php echo $formEnrolment->error($model,'visit_date'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'diagnosis'); ?>
		<?php echo $formEnrolment->textField($model,'diagnosis'); ?>
		<?php echo $formEnrolment->error($model,'diagnosis'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'diagnosis_comment'); ?>
		<?php echo $formEnrolment->textField($model,'diagnosis_comment'); ?>
		<?php echo $formEnrolment->error($model,'diagnosis_comment'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'tachy_latest_date'); ?>
		<?php echo $formEnrolment->textField($model,'tachy_latest_date'); ?>
		<?php echo $formEnrolment->error($model,'tachy_latest_date'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'disease_complications'); ?>
		<?php echo $formEnrolment->textField($model,'disease_complications'); ?>
		<?php echo $formEnrolment->error($model,'disease_complications'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'impl_other_ind'); ?>
		<?php echo $formEnrolment->textField($model,'impl_other_ind'); ?>
		<?php echo $formEnrolment->error($model,'impl_other_ind'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'ae_model'); ?>
		<?php echo $formEnrolment->textField($model,'ae_model'); ?>
		<?php echo $formEnrolment->error($model,'ae_model'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'ae_sn'); ?>
		<?php echo $formEnrolment->textField($model,'ae_sn'); ?>
		<?php echo $formEnrolment->error($model,'ae_sn'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'pacer_model'); ?>
		<?php echo $formEnrolment->textField($model,'pacer_model'); ?>
		<?php echo $formEnrolment->error($model,'pacer_model'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'pacer_placement'); ?>
		<?php echo $formEnrolment->textField($model,'pacer_placement'); ?>
		<?php echo $formEnrolment->error($model,'pacer_placement'); ?>
	</div>

	<div class="row">
		<?php echo $formEnrolment->labelEx($model,'trans_reg_date'); ?>
		<?php echo $formEnrolment->textField($model,'trans_reg_date'); ?>
		<?php echo $formEnrolment->error($model,'trans_reg_date'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->