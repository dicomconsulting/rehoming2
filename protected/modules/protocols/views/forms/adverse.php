<?php
/* @var $this ProtocolAdverseController */
/* @var $model ProtocolAdverse */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'protocol-adverse-adverse-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'protocol_uid'); ?>
		<?php echo $form->textField($model,'protocol_uid'); ?>
		<?php echo $form->error($model,'protocol_uid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'protocol_type'); ?>
		<?php echo $form->textField($model,'protocol_type'); ?>
		<?php echo $form->error($model,'protocol_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adv_detect_by_hm'); ?>
		<?php echo $form->textField($model,'adv_detect_by_hm'); ?>
		<?php echo $form->error($model,'adv_detect_by_hm'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adv_result'); ?>
		<?php echo $form->textField($model,'adv_result'); ?>
		<?php echo $form->error($model,'adv_result'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adv_iml_relation'); ?>
		<?php echo $form->textField($model,'adv_iml_relation'); ?>
		<?php echo $form->error($model,'adv_iml_relation'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adv_iml_relation_device'); ?>
		<?php echo $form->textField($model,'adv_iml_relation_device'); ?>
		<?php echo $form->error($model,'adv_iml_relation_device'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adv_iml_relation_electr'); ?>
		<?php echo $form->textField($model,'adv_iml_relation_electr'); ?>
		<?php echo $form->error($model,'adv_iml_relation_electr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'impl_expl'); ?>
		<?php echo $form->textField($model,'impl_expl'); ?>
		<?php echo $form->error($model,'impl_expl'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adv_lethal'); ?>
		<?php echo $form->textField($model,'adv_lethal'); ?>
		<?php echo $form->error($model,'adv_lethal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adv_injury'); ?>
		<?php echo $form->textField($model,'adv_injury'); ?>
		<?php echo $form->error($model,'adv_injury'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adv_perm_damage'); ?>
		<?php echo $form->textField($model,'adv_perm_damage'); ?>
		<?php echo $form->error($model,'adv_perm_damage'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adv_hospitalization'); ?>
		<?php echo $form->textField($model,'adv_hospitalization'); ?>
		<?php echo $form->error($model,'adv_hospitalization'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adv_surg'); ?>
		<?php echo $form->textField($model,'adv_surg'); ?>
		<?php echo $form->error($model,'adv_surg'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adv_distress'); ?>
		<?php echo $form->textField($model,'adv_distress'); ?>
		<?php echo $form->error($model,'adv_distress'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adv_prevented'); ?>
		<?php echo $form->textField($model,'adv_prevented'); ?>
		<?php echo $form->error($model,'adv_prevented'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adv_death_cardio'); ?>
		<?php echo $form->textField($model,'adv_death_cardio'); ?>
		<?php echo $form->error($model,'adv_death_cardio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adv_death_arr'); ?>
		<?php echo $form->textField($model,'adv_death_arr'); ?>
		<?php echo $form->error($model,'adv_death_arr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'impl_model'); ?>
		<?php echo $form->textField($model,'impl_model'); ?>
		<?php echo $form->error($model,'impl_model'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'impl_sn'); ?>
		<?php echo $form->textField($model,'impl_sn'); ?>
		<?php echo $form->error($model,'impl_sn'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adv_begin_date'); ?>
		<?php echo $form->textField($model,'adv_begin_date'); ?>
		<?php echo $form->error($model,'adv_begin_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adv_detect_by_hm_date'); ?>
		<?php echo $form->textField($model,'adv_detect_by_hm_date'); ?>
		<?php echo $form->error($model,'adv_detect_by_hm_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adv_resolve_date'); ?>
		<?php echo $form->textField($model,'adv_resolve_date'); ?>
		<?php echo $form->error($model,'adv_resolve_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adv_descr'); ?>
		<?php echo $form->textField($model,'adv_descr'); ?>
		<?php echo $form->error($model,'adv_descr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adv_symptoms'); ?>
		<?php echo $form->textField($model,'adv_symptoms'); ?>
		<?php echo $form->error($model,'adv_symptoms'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adv_actions'); ?>
		<?php echo $form->textField($model,'adv_actions'); ?>
		<?php echo $form->error($model,'adv_actions'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adv_actions_result'); ?>
		<?php echo $form->textField($model,'adv_actions_result'); ?>
		<?php echo $form->error($model,'adv_actions_result'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adv_death_date'); ?>
		<?php echo $form->textField($model,'adv_death_date'); ?>
		<?php echo $form->error($model,'adv_death_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adv_death_cause'); ?>
		<?php echo $form->textField($model,'adv_death_cause'); ?>
		<?php echo $form->error($model,'adv_death_cause'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adv_comment'); ?>
		<?php echo $form->textField($model,'adv_comment'); ?>
		<?php echo $form->error($model,'adv_comment'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->