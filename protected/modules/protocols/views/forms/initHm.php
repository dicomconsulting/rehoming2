<?php
/* @var $this ProtocolInithmController */
/* @var $model ProtocolInithm */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'protocol-inithm-initHm-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'protocol_uid'); ?>
		<?php echo $form->textField($model,'protocol_uid'); ?>
		<?php echo $form->error($model,'protocol_uid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'patient_regged'); ?>
		<?php echo $form->textField($model,'patient_regged'); ?>
		<?php echo $form->error($model,'patient_regged'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hm_options_changed'); ?>
		<?php echo $form->textField($model,'hm_options_changed'); ?>
		<?php echo $form->error($model,'hm_options_changed'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'to_red'); ?>
		<?php echo $form->textField($model,'to_red'); ?>
		<?php echo $form->error($model,'to_red'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'from_red'); ?>
		<?php echo $form->textField($model,'from_red'); ?>
		<?php echo $form->error($model,'from_red'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'from_red_status'); ?>
		<?php echo $form->textField($model,'from_red_status'); ?>
		<?php echo $form->error($model,'from_red_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'to_red_date'); ?>
		<?php echo $form->textField($model,'to_red_date'); ?>
		<?php echo $form->error($model,'to_red_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'to_red_descr'); ?>
		<?php echo $form->textField($model,'to_red_descr'); ?>
		<?php echo $form->error($model,'to_red_descr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'from_red_date'); ?>
		<?php echo $form->textField($model,'from_red_date'); ?>
		<?php echo $form->error($model,'from_red_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'from_red_descr'); ?>
		<?php echo $form->textField($model,'from_red_descr'); ?>
		<?php echo $form->error($model,'from_red_descr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'comment'); ?>
		<?php echo $form->textField($model,'comment'); ?>
		<?php echo $form->error($model,'comment'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->