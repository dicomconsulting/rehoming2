<?php
/* @var $this ProtocolAnnualController */
/* @var $model ProtocolAnnual */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'protocol-annual-annual-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'protocol_uid'); ?>
		<?php echo $form->textField($model,'protocol_uid'); ?>
		<?php echo $form->error($model,'protocol_uid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'findings_cnt'); ?>
		<?php echo $form->textField($model,'findings_cnt'); ?>
		<?php echo $form->error($model,'findings_cnt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'medical_therapy_changed'); ?>
		<?php echo $form->textField($model,'medical_therapy_changed'); ?>
		<?php echo $form->error($model,'medical_therapy_changed'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'other_therapy_changed'); ?>
		<?php echo $form->textField($model,'other_therapy_changed'); ?>
		<?php echo $form->error($model,'other_therapy_changed'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estimated_eri_yesars'); ?>
		<?php echo $form->textField($model,'estimated_eri_yesars'); ?>
		<?php echo $form->error($model,'estimated_eri_yesars'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estimated_eri_months'); ?>
		<?php echo $form->textField($model,'estimated_eri_months'); ?>
		<?php echo $form->error($model,'estimated_eri_months'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'atrial_arrhythmic_burden'); ?>
		<?php echo $form->textField($model,'atrial_arrhythmic_burden'); ?>
		<?php echo $form->error($model,'atrial_arrhythmic_burden'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pacing_in_a'); ?>
		<?php echo $form->textField($model,'pacing_in_a'); ?>
		<?php echo $form->error($model,'pacing_in_a'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pacing_in_b'); ?>
		<?php echo $form->textField($model,'pacing_in_b'); ?>
		<?php echo $form->error($model,'pacing_in_b'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'as_vs'); ?>
		<?php echo $form->textField($model,'as_vs'); ?>
		<?php echo $form->error($model,'as_vs'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'as_vp'); ?>
		<?php echo $form->textField($model,'as_vp'); ?>
		<?php echo $form->error($model,'as_vp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ap_vp'); ?>
		<?php echo $form->textField($model,'ap_vp'); ?>
		<?php echo $form->error($model,'ap_vp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ap_vs'); ?>
		<?php echo $form->textField($model,'ap_vs'); ?>
		<?php echo $form->error($model,'ap_vs'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stimul_ddd_adi'); ?>
		<?php echo $form->textField($model,'stimul_ddd_adi'); ?>
		<?php echo $form->error($model,'stimul_ddd_adi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stimul_irs_on'); ?>
		<?php echo $form->textField($model,'stimul_irs_on'); ?>
		<?php echo $form->error($model,'stimul_irs_on'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stimul_irs_off'); ?>
		<?php echo $form->textField($model,'stimul_irs_off'); ?>
		<?php echo $form->error($model,'stimul_irs_off'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'capt_ctrl_a'); ?>
		<?php echo $form->textField($model,'capt_ctrl_a'); ?>
		<?php echo $form->error($model,'capt_ctrl_a'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'capt_ctrl_v'); ?>
		<?php echo $form->textField($model,'capt_ctrl_v'); ?>
		<?php echo $form->error($model,'capt_ctrl_v'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sens_a'); ?>
		<?php echo $form->textField($model,'sens_a'); ?>
		<?php echo $form->error($model,'sens_a'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sens_v'); ?>
		<?php echo $form->textField($model,'sens_v'); ?>
		<?php echo $form->error($model,'sens_v'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'atr_conf'); ?>
		<?php echo $form->textField($model,'atr_conf'); ?>
		<?php echo $form->error($model,'atr_conf'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ven_conf'); ?>
		<?php echo $form->textField($model,'ven_conf'); ?>
		<?php echo $form->error($model,'ven_conf'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'req_atr_stimul_thresh'); ?>
		<?php echo $form->textField($model,'req_atr_stimul_thresh'); ?>
		<?php echo $form->error($model,'req_atr_stimul_thresh'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'req_ven_stimul_thresh'); ?>
		<?php echo $form->textField($model,'req_ven_stimul_thresh'); ?>
		<?php echo $form->error($model,'req_ven_stimul_thresh'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'req_atr_el_imp'); ?>
		<?php echo $form->textField($model,'req_atr_el_imp'); ?>
		<?php echo $form->error($model,'req_atr_el_imp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'req_ven_el_imp'); ?>
		<?php echo $form->textField($model,'req_ven_el_imp'); ?>
		<?php echo $form->error($model,'req_ven_el_imp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'req_ven_shock_imp'); ?>
		<?php echo $form->textField($model,'req_ven_shock_imp'); ?>
		<?php echo $form->error($model,'req_ven_shock_imp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'atest_atr_stimul_thresh'); ?>
		<?php echo $form->textField($model,'atest_atr_stimul_thresh'); ?>
		<?php echo $form->error($model,'atest_atr_stimul_thresh'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'atest_ven_stimul_thresh'); ?>
		<?php echo $form->textField($model,'atest_ven_stimul_thresh'); ?>
		<?php echo $form->error($model,'atest_ven_stimul_thresh'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'atest_atr_el_imp'); ?>
		<?php echo $form->textField($model,'atest_atr_el_imp'); ?>
		<?php echo $form->error($model,'atest_atr_el_imp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'atest_ven_el_imp'); ?>
		<?php echo $form->textField($model,'atest_ven_el_imp'); ?>
		<?php echo $form->error($model,'atest_ven_el_imp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'test_atr_stimul_thresh'); ?>
		<?php echo $form->textField($model,'test_atr_stimul_thresh'); ?>
		<?php echo $form->error($model,'test_atr_stimul_thresh'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'test_ven_stimul_thresh'); ?>
		<?php echo $form->textField($model,'test_ven_stimul_thresh'); ?>
		<?php echo $form->error($model,'test_ven_stimul_thresh'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pacer'); ?>
		<?php echo $form->textField($model,'pacer'); ?>
		<?php echo $form->error($model,'pacer'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pacer_conf'); ?>
		<?php echo $form->textField($model,'pacer_conf'); ?>
		<?php echo $form->error($model,'pacer_conf'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pacer_stimul_thresh'); ?>
		<?php echo $form->textField($model,'pacer_stimul_thresh'); ?>
		<?php echo $form->error($model,'pacer_stimul_thresh'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pacer_imp'); ?>
		<?php echo $form->textField($model,'pacer_imp'); ?>
		<?php echo $form->error($model,'pacer_imp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rtg_time_hmsc'); ?>
		<?php echo $form->textField($model,'rtg_time_hmsc'); ?>
		<?php echo $form->error($model,'rtg_time_hmsc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rtg_time_semaphore'); ?>
		<?php echo $form->textField($model,'rtg_time_semaphore'); ?>
		<?php echo $form->error($model,'rtg_time_semaphore'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rtg_time_iegm'); ?>
		<?php echo $form->textField($model,'rtg_time_iegm'); ?>
		<?php echo $form->error($model,'rtg_time_iegm'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rtg_cl_hmsc'); ?>
		<?php echo $form->textField($model,'rtg_cl_hmsc'); ?>
		<?php echo $form->error($model,'rtg_cl_hmsc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rtg_cl_semaphore'); ?>
		<?php echo $form->textField($model,'rtg_cl_semaphore'); ?>
		<?php echo $form->error($model,'rtg_cl_semaphore'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rtg_cl_iegm'); ?>
		<?php echo $form->textField($model,'rtg_cl_iegm'); ?>
		<?php echo $form->error($model,'rtg_cl_iegm'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rtg_cl_data_suff'); ?>
		<?php echo $form->textField($model,'rtg_cl_data_suff'); ?>
		<?php echo $form->error($model,'rtg_cl_data_suff'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cl_hosp_days'); ?>
		<?php echo $form->textField($model,'cl_hosp_days'); ?>
		<?php echo $form->error($model,'cl_hosp_days'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cl_disability_days'); ?>
		<?php echo $form->textField($model,'cl_disability_days'); ?>
		<?php echo $form->error($model,'cl_disability_days'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cl_amb_calls'); ?>
		<?php echo $form->textField($model,'cl_amb_calls'); ?>
		<?php echo $form->error($model,'cl_amb_calls'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'req_atr_ampl'); ?>
		<?php echo $form->textField($model,'req_atr_ampl'); ?>
		<?php echo $form->error($model,'req_atr_ampl'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'req_ven_ampl'); ?>
		<?php echo $form->textField($model,'req_ven_ampl'); ?>
		<?php echo $form->error($model,'req_ven_ampl'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'atest_atr_ampl'); ?>
		<?php echo $form->textField($model,'atest_atr_ampl'); ?>
		<?php echo $form->error($model,'atest_atr_ampl'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'atest_ven_ampl'); ?>
		<?php echo $form->textField($model,'atest_ven_ampl'); ?>
		<?php echo $form->error($model,'atest_ven_ampl'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'test_atr_ampl'); ?>
		<?php echo $form->textField($model,'test_atr_ampl'); ?>
		<?php echo $form->error($model,'test_atr_ampl'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'test_ven_ampl'); ?>
		<?php echo $form->textField($model,'test_ven_ampl'); ?>
		<?php echo $form->error($model,'test_ven_ampl'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pacer_ampl'); ?>
		<?php echo $form->textField($model,'pacer_ampl'); ?>
		<?php echo $form->error($model,'pacer_ampl'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'medical_therapy_comment'); ?>
		<?php echo $form->textField($model,'medical_therapy_comment'); ?>
		<?php echo $form->error($model,'medical_therapy_comment'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'other_therapy_comment'); ?>
		<?php echo $form->textField($model,'other_therapy_comment'); ?>
		<?php echo $form->error($model,'other_therapy_comment'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'placement'); ?>
		<?php echo $form->textField($model,'placement'); ?>
		<?php echo $form->error($model,'placement'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cl_disease_complications'); ?>
		<?php echo $form->textField($model,'cl_disease_complications'); ?>
		<?php echo $form->error($model,'cl_disease_complications'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'comment'); ?>
		<?php echo $form->textField($model,'comment'); ?>
		<?php echo $form->error($model,'comment'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->