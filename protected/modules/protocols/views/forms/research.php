<?php
/* @var $this EnrolmentController */
/* @var $model Research */
/* @var $formResearch CActiveForm */
?>

<div class="form">

<?php $formResearch=$this->beginWidget('CActiveForm', array(
	'id'=>'research-research-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
));

?>

	<?php echo $formResearch->errorSummary($model); ?>

	<div class="row">
		<?php echo $formResearch->labelEx($model,'date_begin'); ?>
		<?php echo $formResearch->dateField($model,'date_begin'); ?>
		<?php echo $formResearch->error($model,'date_begin'); ?>
	</div>

	<div class="row">
		<?php echo $formResearch->labelEx($model,'patient_uid'); ?>
		<?php echo $formResearch->textField($model,'patient_uid'); ?>
		<?php echo $formResearch->error($model,'patient_uid'); ?>
	</div>

	<div class="row">
		<?php echo $formResearch->labelEx($model,'medical_center_uid'); ?>
		<?php echo $formResearch->textField($model,'medical_center_uid'); ?>
		<?php echo $formResearch->error($model,'medical_center_uid'); ?>
	</div>

	<div class="row">
		<?php echo $formResearch->labelEx($model,'screening_number'); ?>
		<?php echo $formResearch->textField($model,'screening_number'); ?>
		<?php echo $formResearch->error($model,'screening_number'); ?>
	</div>

<?php $this->endWidget();?>

</div><!-- form -->