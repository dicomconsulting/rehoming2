<?php
/* @var $this ProtocolRegularController */
/* @var $model ProtocolRegular */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'protocol-regular-regular-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'protocol_uid'); ?>
		<?php echo $form->textField($model,'protocol_uid'); ?>
		<?php echo $form->error($model,'protocol_uid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'survey_period'); ?>
		<?php echo $form->textField($model,'survey_period'); ?>
		<?php echo $form->error($model,'survey_period'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'clinic_distance'); ?>
		<?php echo $form->textField($model,'clinic_distance'); ?>
		<?php echo $form->error($model,'clinic_distance'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'clinic_time'); ?>
		<?php echo $form->textField($model,'clinic_time'); ?>
		<?php echo $form->error($model,'clinic_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idc_vt1'); ?>
		<?php echo $form->textField($model,'idc_vt1'); ?>
		<?php echo $form->error($model,'idc_vt1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idc_vt2'); ?>
		<?php echo $form->textField($model,'idc_vt2'); ?>
		<?php echo $form->error($model,'idc_vt2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idc_vf'); ?>
		<?php echo $form->textField($model,'idc_vf'); ?>
		<?php echo $form->error($model,'idc_vf'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ae_pacing_imp'); ?>
		<?php echo $form->textField($model,'ae_pacing_imp'); ?>
		<?php echo $form->error($model,'ae_pacing_imp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ve_imp'); ?>
		<?php echo $form->textField($model,'ve_imp'); ?>
		<?php echo $form->error($model,'ve_imp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ve_imp_shock'); ?>
		<?php echo $form->textField($model,'ve_imp_shock'); ?>
		<?php echo $form->error($model,'ve_imp_shock'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'arr_vt1_cnt'); ?>
		<?php echo $form->textField($model,'arr_vt1_cnt'); ?>
		<?php echo $form->error($model,'arr_vt1_cnt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'arr_vt2_cnt'); ?>
		<?php echo $form->textField($model,'arr_vt2_cnt'); ?>
		<?php echo $form->error($model,'arr_vt2_cnt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'arr_vf_cnt'); ?>
		<?php echo $form->textField($model,'arr_vf_cnt'); ?>
		<?php echo $form->error($model,'arr_vf_cnt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'atc_began_vt'); ?>
		<?php echo $form->textField($model,'atc_began_vt'); ?>
		<?php echo $form->error($model,'atc_began_vt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'atc_compl_vt'); ?>
		<?php echo $form->textField($model,'atc_compl_vt'); ?>
		<?php echo $form->error($model,'atc_compl_vt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'atc_began_vf'); ?>
		<?php echo $form->textField($model,'atc_began_vf'); ?>
		<?php echo $form->error($model,'atc_began_vf'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'atc_compl_vf'); ?>
		<?php echo $form->textField($model,'atc_compl_vf'); ?>
		<?php echo $form->error($model,'atc_compl_vf'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'shock_began'); ?>
		<?php echo $form->textField($model,'shock_began'); ?>
		<?php echo $form->error($model,'shock_began'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'shock_cancel'); ?>
		<?php echo $form->textField($model,'shock_cancel'); ?>
		<?php echo $form->error($model,'shock_cancel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'shock_compl'); ?>
		<?php echo $form->textField($model,'shock_compl'); ?>
		<?php echo $form->error($model,'shock_compl'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'svt_total_cnt'); ?>
		<?php echo $form->textField($model,'svt_total_cnt'); ?>
		<?php echo $form->error($model,'svt_total_cnt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'svt_12h_cnt'); ?>
		<?php echo $form->textField($model,'svt_12h_cnt'); ?>
		<?php echo $form->error($model,'svt_12h_cnt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'af_episode_hr_max'); ?>
		<?php echo $form->textField($model,'af_episode_hr_max'); ?>
		<?php echo $form->error($model,'af_episode_hr_max'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'af_episode_hr_avg'); ?>
		<?php echo $form->textField($model,'af_episode_hr_avg'); ?>
		<?php echo $form->error($model,'af_episode_hr_avg'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vent_contr_prop'); ?>
		<?php echo $form->textField($model,'vent_contr_prop'); ?>
		<?php echo $form->error($model,'vent_contr_prop'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vent_rate_24'); ?>
		<?php echo $form->textField($model,'vent_rate_24'); ?>
		<?php echo $form->error($model,'vent_rate_24'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vent_rate_fu'); ?>
		<?php echo $form->textField($model,'vent_rate_fu'); ?>
		<?php echo $form->error($model,'vent_rate_fu'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vent_rate_24_rest'); ?>
		<?php echo $form->textField($model,'vent_rate_24_rest'); ?>
		<?php echo $form->error($model,'vent_rate_24_rest'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vent_rate_fu_rest'); ?>
		<?php echo $form->textField($model,'vent_rate_fu_rest'); ?>
		<?php echo $form->error($model,'vent_rate_fu_rest'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'patient_act'); ?>
		<?php echo $form->textField($model,'patient_act'); ?>
		<?php echo $form->error($model,'patient_act'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'patient_act_fu'); ?>
		<?php echo $form->textField($model,'patient_act_fu'); ?>
		<?php echo $form->error($model,'patient_act_fu'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vent_contr_24'); ?>
		<?php echo $form->textField($model,'vent_contr_24'); ?>
		<?php echo $form->error($model,'vent_contr_24'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vent_contr_fu'); ?>
		<?php echo $form->textField($model,'vent_contr_fu'); ?>
		<?php echo $form->error($model,'vent_contr_fu'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ekg_needed'); ?>
		<?php echo $form->textField($model,'ekg_needed'); ?>
		<?php echo $form->error($model,'ekg_needed'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ekg_supplied'); ?>
		<?php echo $form->textField($model,'ekg_supplied'); ?>
		<?php echo $form->error($model,'ekg_supplied'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'program_change_needed'); ?>
		<?php echo $form->textField($model,'program_change_needed'); ?>
		<?php echo $form->error($model,'program_change_needed'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'drugs_change_needed'); ?>
		<?php echo $form->textField($model,'drugs_change_needed'); ?>
		<?php echo $form->error($model,'drugs_change_needed'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'elect_displ_hosp_emerg'); ?>
		<?php echo $form->textField($model,'elect_displ_hosp_emerg'); ?>
		<?php echo $form->error($model,'elect_displ_hosp_emerg'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hf_hosp_emerg'); ?>
		<?php echo $form->textField($model,'hf_hosp_emerg'); ?>
		<?php echo $form->error($model,'hf_hosp_emerg'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idc_fail_hosp_emerg'); ?>
		<?php echo $form->textField($model,'idc_fail_hosp_emerg'); ?>
		<?php echo $form->error($model,'idc_fail_hosp_emerg'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'option_changed'); ?>
		<?php echo $form->textField($model,'option_changed'); ?>
		<?php echo $form->error($model,'option_changed'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'red_finding'); ?>
		<?php echo $form->textField($model,'red_finding'); ?>
		<?php echo $form->error($model,'red_finding'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'red_finding_changed'); ?>
		<?php echo $form->textField($model,'red_finding_changed'); ?>
		<?php echo $form->error($model,'red_finding_changed'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'red_finding_changed_to'); ?>
		<?php echo $form->textField($model,'red_finding_changed_to'); ?>
		<?php echo $form->error($model,'red_finding_changed_to'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stimul_mode'); ?>
		<?php echo $form->textField($model,'stimul_mode'); ?>
		<?php echo $form->error($model,'stimul_mode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idc_vt1_time'); ?>
		<?php echo $form->textField($model,'idc_vt1_time'); ?>
		<?php echo $form->error($model,'idc_vt1_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idc_vt2_time'); ?>
		<?php echo $form->textField($model,'idc_vt2_time'); ?>
		<?php echo $form->error($model,'idc_vt2_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idc_vf_time'); ?>
		<?php echo $form->textField($model,'idc_vf_time'); ?>
		<?php echo $form->error($model,'idc_vf_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ae_pacing_threshold'); ?>
		<?php echo $form->textField($model,'ae_pacing_threshold'); ?>
		<?php echo $form->error($model,'ae_pacing_threshold'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ae_pacing_threshold_dur'); ?>
		<?php echo $form->textField($model,'ae_pacing_threshold_dur'); ?>
		<?php echo $form->error($model,'ae_pacing_threshold_dur'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ae_pulse_ampl'); ?>
		<?php echo $form->textField($model,'ae_pulse_ampl'); ?>
		<?php echo $form->error($model,'ae_pulse_ampl'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ve_stimul_threshold'); ?>
		<?php echo $form->textField($model,'ve_stimul_threshold'); ?>
		<?php echo $form->error($model,'ve_stimul_threshold'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ve_stimul_dur'); ?>
		<?php echo $form->textField($model,'ve_stimul_dur'); ?>
		<?php echo $form->error($model,'ve_stimul_dur'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ve_wave_apl'); ?>
		<?php echo $form->textField($model,'ve_wave_apl'); ?>
		<?php echo $form->error($model,'ve_wave_apl'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'af_episode_time'); ?>
		<?php echo $form->textField($model,'af_episode_time'); ?>
		<?php echo $form->error($model,'af_episode_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'program_changes'); ?>
		<?php echo $form->textField($model,'program_changes'); ?>
		<?php echo $form->error($model,'program_changes'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'elect_displ_detect_date'); ?>
		<?php echo $form->textField($model,'elect_displ_detect_date'); ?>
		<?php echo $form->error($model,'elect_displ_detect_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'elect_displ_hosp_date'); ?>
		<?php echo $form->textField($model,'elect_displ_hosp_date'); ?>
		<?php echo $form->error($model,'elect_displ_hosp_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hf_detect_date'); ?>
		<?php echo $form->textField($model,'hf_detect_date'); ?>
		<?php echo $form->error($model,'hf_detect_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hf_hosp_date'); ?>
		<?php echo $form->textField($model,'hf_hosp_date'); ?>
		<?php echo $form->error($model,'hf_hosp_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idc_fail_detect_date'); ?>
		<?php echo $form->textField($model,'idc_fail_detect_date'); ?>
		<?php echo $form->error($model,'idc_fail_detect_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idc_fail_hosp_date'); ?>
		<?php echo $form->textField($model,'idc_fail_hosp_date'); ?>
		<?php echo $form->error($model,'idc_fail_hosp_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'option_changed_new_value'); ?>
		<?php echo $form->textField($model,'option_changed_new_value'); ?>
		<?php echo $form->error($model,'option_changed_new_value'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'option_changed_reason'); ?>
		<?php echo $form->textField($model,'option_changed_reason'); ?>
		<?php echo $form->error($model,'option_changed_reason'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'red_finding_date'); ?>
		<?php echo $form->textField($model,'red_finding_date'); ?>
		<?php echo $form->error($model,'red_finding_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'red_finding_message'); ?>
		<?php echo $form->textField($model,'red_finding_message'); ?>
		<?php echo $form->error($model,'red_finding_message'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'red_finding_changed_date'); ?>
		<?php echo $form->textField($model,'red_finding_changed_date'); ?>
		<?php echo $form->error($model,'red_finding_changed_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'red_finding_changed_message'); ?>
		<?php echo $form->textField($model,'red_finding_changed_message'); ?>
		<?php echo $form->error($model,'red_finding_changed_message'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'red_finding_changed_comment'); ?>
		<?php echo $form->textField($model,'red_finding_changed_comment'); ?>
		<?php echo $form->error($model,'red_finding_changed_comment'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->