<?php
/* @var $this ProtocolEvolutionController */
/* @var $model ProtocolEvolution */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'protocol-evolution-evolution-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'protocol_uid'); ?>
		<?php echo $form->textField($model,'protocol_uid'); ?>
		<?php echo $form->error($model,'protocol_uid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'option_changed'); ?>
		<?php echo $form->textField($model,'option_changed'); ?>
		<?php echo $form->error($model,'option_changed'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'red_finding'); ?>
		<?php echo $form->textField($model,'red_finding'); ?>
		<?php echo $form->error($model,'red_finding'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'red_finding_changed'); ?>
		<?php echo $form->textField($model,'red_finding_changed'); ?>
		<?php echo $form->error($model,'red_finding_changed'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'option_changed_new_value'); ?>
		<?php echo $form->textField($model,'option_changed_new_value'); ?>
		<?php echo $form->error($model,'option_changed_new_value'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'option_changed_reason'); ?>
		<?php echo $form->textField($model,'option_changed_reason'); ?>
		<?php echo $form->error($model,'option_changed_reason'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'red_finding_date'); ?>
		<?php echo $form->textField($model,'red_finding_date'); ?>
		<?php echo $form->error($model,'red_finding_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'red_finding_message'); ?>
		<?php echo $form->textField($model,'red_finding_message'); ?>
		<?php echo $form->error($model,'red_finding_message'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'red_finding_changed_date'); ?>
		<?php echo $form->textField($model,'red_finding_changed_date'); ?>
		<?php echo $form->error($model,'red_finding_changed_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'red_finding_changed_message'); ?>
		<?php echo $form->textField($model,'red_finding_changed_message'); ?>
		<?php echo $form->error($model,'red_finding_changed_message'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'red_finding_changed_comment'); ?>
		<?php echo $form->textField($model,'red_finding_changed_comment'); ?>
		<?php echo $form->error($model,'red_finding_changed_comment'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->