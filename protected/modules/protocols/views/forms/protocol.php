<?php
/* @var $this EnrolmentController */
/* @var $model Protocol */
/* @var $formProtocol CActiveForm */
?>

<div class="form">

<?php $formProtocol=$this->beginWidget('CActiveForm', array(
	'id'=>'protocol-protocol-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
));
?>

	<?php echo $formProtocol->errorSummary($model); ?>

	<div class="row">
		<?php echo $formProtocol->labelEx($model,'date'); ?>
		<?php echo $formProtocol->dateField($model,'date'); ?>
		<?php echo $formProtocol->error($model,'date'); ?>
	</div>

	<div class="row">
		<?php echo $formProtocol->labelEx($model,'protocol_dictionary_uid'); ?>
		<?php echo $formProtocol->textField($model,'protocol_dictionary_uid'); ?>
		<?php echo $formProtocol->error($model,'protocol_dictionary_uid'); ?>
	</div>

	<div class="row">
		<?php echo $formProtocol->labelEx($model,'research_uid'); ?>
		<?php echo $formProtocol->textField($model,'research_uid'); ?>
		<?php echo $formProtocol->error($model,'research_uid'); ?>
	</div>

	<div class="row">
		<?php echo $formProtocol->labelEx($model,'user_id'); ?>
		<?php echo $formProtocol->textField($model,'user_id'); ?>
		<?php echo $formProtocol->error($model,'user_id'); ?>
	</div>



<?php $this->endWidget();?>

</div><!-- form -->