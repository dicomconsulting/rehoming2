<?php

/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 29.10.13
 * Time: 14:05
 *
 * Класс создан для заполнения протоколов данными полученными от устройств
 */
class ProtocolFiller
{

    /** Состояние на дату включения */
    const STATE_BEGIN = 1;
    /** Последнее доступное состояние */
    const STATE_LAST = 2;
    /** @var  Patient */
    protected $patient;
    /** @var  ProtocolFillableInterface */
    protected $protocol;

    public function __construct(ProtocolFillableInterface $protocol, Patient $patient)
    {
        $this->protocol = $protocol;
        $this->patient  = $patient;
    }

    public function fill($state)
    {
        if (!$this->assignDeviceState($state)) {
            return;
        }

        $paramsToFieldsMap = $this->protocol->getFillingMap();

        foreach ($paramsToFieldsMap as $field => $parameter) {
            if (!is_array($parameter)) {
                $parameter = array("param" => $parameter);
            }

            $paramName = $parameter['param'];

            if (array_key_exists('filter', $parameter)) {
                $filterName  = 'filter' . ucfirst($parameter['filter']);
                $filterValue = $this->$filterName($parameter);
                if ($filterValue !== true) {
                    $this->protocol->$field = $filterValue;
                    continue;
                }
            }

            if (isset($parameter['source'])) {
                $value = $this->getFromOtherSource($parameter);
            } else {

                if (isset($this->patient->device->model->parameters[$paramName])) {

                    if (!isset($parameter["statValue"])) { //необходимо получить агрегированное значение
                        $value = $this->getParameterValue($paramName);
                    } else {
                        $value = $this->getParameterValue($paramName, $parameter["statValue"]);
                    }

                } else {
                    continue;
                }
            }

            if (array_key_exists('transform', $parameter)) {
                //необходимо провести конвертацию данных из формата HM в формат, поддерживаемый протоколами
                $transformMethod = "transformDefault" . ucfirst($parameter["transform"]);

                $value = $this->$transformMethod($value);
            }
            if ($field == 'ae_pacing_imp' && $value == 0) {
                $value = null;
            }
            $this->protocol->$field = $value;
        }
    }

    /*
     * Проверка что у пациента в протоколе включения стоит «Постоянная форма»=Да. В таком случае выводить 0.
     *
     */
    protected function filterIsPermanentFormPatient($parameter)
    {
        $protocolEnrolment = PatientStats::getFirstProtocolEnrolment($this->patient);
        if ($protocolEnrolment) {
            if ($protocolEnrolment->atrial_permanent) {
                return true;
            }
        }
        return '0';
    }

    protected function assignDeviceState($state)
    {
        try {
            if ($state == self::STATE_BEGIN) {
                $this->patient->device->assignFirstDeviceState();
            } elseif ($state == self::STATE_LAST) {
                $this->patient->device->assignStateValuesToDeviceModelParameters();
            }

            return true;
        } catch (CException $e) {
            return false;
        }
    }

    /**
     * Возвращает данные из других источников, связанных с пациентом
     *
     * @param $param
     * @return mixed|null
     */
    public function getFromOtherSource($param)
    {
        $paramName = $param['param'];

        switch ($param['source']) {
            case 'device':
                if (array_key_exists('deviceTypeFilter', $param)) {
                    $deviceTypeAlias = strtolower($this->patient->device->model->type->alias);
                    $filterTypeAlias = strtolower($param['deviceTypeFilter']);
                    if (substr($deviceTypeAlias, 0, strlen($filterTypeAlias)) !== $filterTypeAlias) {
                        return null;
                    }
                }
                return $this->patient->device->$paramName;
            case 'longEpisode':
                return $this->getLongEpisodeCount($paramName);
            case 'device_type':
                return $this->isDeviceTypeEqual($paramName);
            case 'transmitter':
                return $this->getTransmitterUid($paramName);
            case 'historyMessage':
                return $this->getHistoryMessage($paramName);
            case 'protocolRegularParameter':
                return $this->sourceProtocolRegularParameter($paramName);
                break;

        }

        return null;
    }

    protected function isDeviceTypeEqual($paramName)
    {
        $type = $this->patient->device->model->type->alias;
        if ($paramName == $type) {
            return 1;
        } else {
            return 0;
        }
    }

    protected function getTransmitterUid()
    {
        $type = $this->patient->device->transmitter_type;
        foreach (Transmitter::getList() as $row) {
            if ($row['name'] == $type) {
                return $row['uid'];
            }
        };

        return null;
    }

    /**
     * Получить статистику по сообщениям устройств
     * @param $paramName
     * @return int
     */
    protected function getHistoryMessage($paramName)
    {
        $deviceUid = $this->patient->device->uid;

        $sql = 'SELECT count(*) as value FROM {{history_message}} AS history_message WHERE history_message.text LIKE :episode_text and history_message.device_id = :device_id';

        $command = Yii::app()->db->createCommand($sql);
        $command->bindValue(':episode_text', $paramName . '%');
        $command->bindValue(':device_id', $deviceUid);

        $sqlResult = $command->queryScalar();;

        return $sqlResult;
    }

    protected function getParameterValue($parameterName, $mod = null)
    {
        switch ($mod) {
            case 'maxAllTime':
                $value = $this->patient->device->model->parameters[$parameterName]->getMaxByAllStates();
                break;
            case 'maxFu':
                $value = $this->patient->device->model->parameters[$parameterName]->getMaxValue();
                break;
            case 'countFu': //количество с последнего FU, параметр - счетчик
                $value = $this->patient->device->model->parameters[$parameterName]->getMaxValue() -
                    $this->patient->device->model->parameters[$parameterName]->getMinValue();
                break;
            case 'meanFu':
                $value = round($this->patient->device->model->parameters[$parameterName]->getMeanValue());
                break;
            case 'meanByCountValue':
// Пока закоментировали так как выходила на Казахстанских пользователял
//                $value = $this->patient->device->model->parameters[$parameterName]->getMeanByCountValue(1);
		  $value = NULL;
                break;
            default:
                $value = $this->patient->device->model->parameters[$parameterName]->getCurrentValue()->value;
        }

        if ($value instanceof I18NValue) {
            $value = (string)$value;
        }

        return $value;
    }

    /**
     * Трансформирует исходные, полученные от устройств, в форму протоколов.
     * Полярность устройств.
     *
     * @param $value
     * @return int|null
     */
    protected function transformDefaultPolarity($value)
    {
        if ($value == "unipolar") {
            return 2;
        } elseif ($value == "bipolar") {
            return 1;
        } else {
            return null;
        }
    }

    /**
     * Трансформирует исходные, полученные от устройств, в форму протоколов.
     * Численные значения.
     *
     * @param $value
     * @return string|null
     */
    protected function transformDefaultNumeric($value)
    {
        if ($value) {
            return str_replace(".", ",", (double)$value);
        } else {
            return null;
        }
    }

    /**
     * Трансформирует исходные, полученные от устройств, в форму протоколов.
     * Челочисленные значения.
     *
     * @param $value
     * @return int|null
     */
    protected function transformDefaultInteger($value)
    {
        if ($value) {
            return (int)$value;
        } else {
            return null;
        }
    }

    /**
     * Трансформирует исходные, полученные от устройств, в форму протоколов.
     * Челочисленные значения.
     *
     * @param $value
     * @return int|null
     */
    protected function transformDefaultRoundInteger($value)
    {
        if ($value) {
            return round($value);
        } else {
            return null;
        }
    }

    /**
     * Трансформирует исходные, полученные от устройств, в форму протоколов.
     * Челочисленные значения с конца строки
     *
     * @param $value
     * @return int|null
     */
    protected function transformDefaultIntFilter($value)
    {
        $returnValue = '';
        $len         = strlen($value);
        for ($i = 0; $i < $len; $i++) {
            if ((ord($value[$i]) > 47) && (ord($value[$i]) < 58)) {
                $returnValue .= $value[$i];
            }
        }
        return $returnValue;
    }

    protected function transformDefaultRhDate($value)
    {
        if ($value instanceof RhDateTime) {
            return $value->format("Y-m-d");
        } else {
            return null;
        }
    }


    /**
     * TODO: вынести потом в отдельнные filler классы для каждого отдельного протокола
     *
     * @param $paramName
     * @return float|string
     */
    protected function sourceProtocolRegularParameter($paramName)
    {
        if ($paramName === 'vent_contr_prop') {
            if (strpos($this->patient->device->device_model, 'HF-T')) {
                // Трехкамерные устройства
                $value1 = $this->getParameterValue('biv_stimulation', 'meanByCountValue');
                $value2 = $this->getParameterValue('rvp_without_lvp', 'meanByCountValue');
                return round($value1 + $value2);

            } else {
                $value1 = $this->getParameterValue('ven_pacing_vp', 'meanByCountValue');
                return $value1;
            }
        }

    }

    /**
     * Возвращает количество эпизодов данного типа, длиннее 12 часов
     *
     * @param $episodeType
     * @return int
     */
    protected function getLongEpisodeCount($episodeType)
    {
        $duration = 12 * 3600;

        $state = $this->patient->device->getLastState();

        $sql     = "SELECT val_dur.value
            FROM {{device_value}} AS val
            JOIN {{parameter}} AS param ON param.`uid` = val.`parameter_id`
            JOIN {{device_state}} AS state ON state.`uid` = val.`device_state_id`
            JOIN {{device_value}} AS val_dur ON `val_dur`.`device_state_id` = val.`device_state_id`
            JOIN {{parameter}} AS param_dur ON `param_dur`.`uid` = `val_dur`.`parameter_id`
            WHERE
            param.`system_name` = 'episode_type'
            AND val.`value` = :episode_type
            AND state.`device_id` = :device_uid
            AND state.`create_time` >= :last_follow_up
            AND `param_dur`.`system_name` = 'episode_duration'
            AND `val_dur`.`value` >= :episode_duration";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindValue(":episode_type", $episodeType);
        $command->bindValue(":device_uid", $this->patient->device->uid);
        $command->bindValue(":last_follow_up", $state->last_follow_up);
        $command->bindValue(":episode_duration", $duration, PDO::PARAM_INT);

        return count($command->queryAll());
    }
}
