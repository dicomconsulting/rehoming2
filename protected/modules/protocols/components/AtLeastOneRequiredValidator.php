<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 30.10.13
 * Time: 17:34
 *
 * Валидатор проверяющий что заполнено по крайней мере одно поле из перечисленных. В CActiveRecord::rules() может
 * быть определено несколько правил этого типа для проверки разных групп полей
 */

class AtLeastOneRequiredValidator extends CValidator
{

    /**
     * Храним тут факт того что для конкретного набора атрибутов было показано сообщение об ошибке
     *
     * @var array
     */
    static public $fired = [];

    /**
     * @param CModel $object
     * @param string $attribute
     * @throws CException
     */
    protected function validateAttribute($object, $attribute)
    {
        $fieldsHash = $this->getFieldsHash();

        if (isset(self::$fired[$fieldsHash])) {
            return;
        }

        $found = false;
        $fieldNames = [];
        foreach ($this->attributes as $field) {
            if ($object->$field) {
                $found = true;
            }
            $fieldNames[] = $object->getAttributeLabel($field);
        }


        if (!$found) {
            $this->addError(
                $object,
                $attribute,
                Yii::t("ProtocolsModule.protocols", "Должно быть заполнено по крайней мере одно поле из:")
                . "\"" .  implode("\", \"", $fieldNames) . "\""
            );

            self::$fired[$fieldsHash] = true;
        }
    }

    /**
     * Возвращает хэш имен набора полей, которые должны быть заполнены
     *
     * @return string
     */
    protected function getFieldsHash()
    {
        return md5(implode($this->attributes));
    }
}
