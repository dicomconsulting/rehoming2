<?php

/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 13.01.14
 * Time: 10:46
 *
 *  Поведение, отвечающее за логирований действий по изменению протоколов
 */
class ProtocolDataLoggingBehavior extends CActiveRecordBehavior
{

    /** @var array Какие связи будут проигнорированы при сохранении предыдущего состояния */
    protected $excludedRelationTypes = [CActiveRecord::BELONGS_TO, CActiveRecord::HAS_ONE];

    /** @var  ProtocolHistory */
    protected $loggingModel;

    /** @var  CActiveRecord */
    protected $previousState;

    protected $previousRels;

    const PROTOCOL_PK_NAME = "protocol_uid";

    public function __construct()
    {
        $this->loggingModel = new ProtocolHistory();
    }

    public function beforeSave($event)
    {
        $this->saveState();
    }

    public function afterSave($event)
    {
        /** @var CActiveRecord $owner */
        $owner = $event->sender;
        $userId = Yii::app()->user->id;

        if (!$owner->getIsNewRecord()) {
            if ($this->modelChanged($this->previousState, $owner)) {
                ProtocolHistory::addEvent(
                    ProtocolHistory::EVENT_UPDATED,
                    $owner->protocol,
                    new RhDateTime(),
                    $userId
                );
            }
        }
    }

    /**
     * Сохраняет предыдущее состояние модели, в том числе идентификаторы связанных моделей
     */
    protected function saveState()
    {
        /** @var CActiveRecord $owner */
        $owner = $this->getOwner();
        if (!$owner->getIsNewRecord()) {
            $this->previousState = $owner::model()->findByPk($owner->getPrimaryKey());
            $this->previousRels = $this->getRels($this->previousState);
        }
    }


    /**
     * Возвращает изменились ли данные в модели. Также проверяет связанные
     *
     * @param CActiveRecord $source
     * @param CActiveRecord $compareTo
     * @return bool
     */
    protected function modelChanged($source, $compareTo)
    {
        $changed = false;

        $sourceAttrs = $source->getAttributes();
        $compareAttrs = $compareTo->getAttributes();

        if (array_udiff_assoc($sourceAttrs, $compareAttrs, [$this, 'compareVals'])
            || array_udiff_assoc($compareAttrs, $sourceAttrs, [$this, 'compareVals'])
        ) {
            $changed = true;
        }

        $newRels = $this->getRels($compareTo);

        if (array_keys($newRels) != array_keys($this->previousRels)) {
            $changed = true;
        }

        if (!$changed) {
            foreach ($this->previousRels as $relName => $rel) {
                if ($this->previousRels[$relName] != $newRels[$relName]) {
                    $changed = true;
                }
            }
        }

        return $changed;
    }

    public function compareVals($a, $b)
    {
        if ($a instanceof DateInterval) {
            $a = DateIntervalHelper::toSeconds($a);
        }
        if ($b instanceof DateInterval) {
            $b = DateIntervalHelper::toSeconds($b);
        }

        if ($a == $b) {
            return 0;
        } elseif ($a > $b) {
            return 1;
        } else {
            return -1;
        }
    }

    /**
     * @param CActiveRecord $owner
     * @param bool $refresh
     * @return array
     */
    protected function getRels($owner, $refresh = false)
    {
        $rels = $owner->relations();

        $relsWithIds = [];

        foreach ($rels as $relName => $rel) {
            if (in_array($rel[0], $this->excludedRelationTypes)) {
                continue;
            }

            $relsWithIds[$relName] = [];

            /** @var CActiveRecord[] $related */
            $related = $owner->$relName;
            foreach ($related as $record) {
                $relsWithIds[$relName][] = $this->getRelPkStr($record);
            }
        }

        return $relsWithIds;
    }

    /**
     * Преобразует первичный ключ модели к сравнимому в виде строки значению, удаляя идентификтор протокола
     *
     * @param CActiveRecord $record
     * @return mixed|string
     */
    protected function getRelPkStr(CActiveRecord $record)
    {
        $pk = $record->getPrimaryKey();

        if (!is_array($pk)) {
            return $pk;
        }

        if (array_key_exists(self::PROTOCOL_PK_NAME, $pk)) {
            unset($pk[self::PROTOCOL_PK_NAME]);
        }

        return implode("%", $pk);
    }
}
