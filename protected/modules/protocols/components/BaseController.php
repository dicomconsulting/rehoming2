<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 24.10.13
 * Time: 11:29
 */

class BaseController extends Controller
{

    const PROTOCOL_DICT_UID = 0;
    /**
     * Ссылка на пациента
     *
     * @var Patient
     */
    public $patient;

    /*
     * Подключаем лейаут с внутренним меню
    */
    public $layout = '//layouts/innerMenu';

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function behaviors()
    {
        return array(
            'pdfable' => array(
                'class' => 'ext.pdfable.Pdfable',
                // Global default options for wkhtmltopdf
                'pdfOptions' => array(
                    // Use binary path from module config
                    'bin' => Yii::app()->getModule("pdfable")->bin,
                    'use-xserver',
                    'dpi' => 600,
                    'margin-top' => 19,
                    'margin-right' => 5,
                    'margin-bottom' => 15,
                    'margin-left' => 10,
                    'header-spacing' => 3,
                    'footer-spacing' => 3,
                ),
                // Default page options
                'pdfPageOptions' => array(
                    'print-media-type',
                    'enable-javascript',
                    'javascript-delay' => 500,
                ),
            )
        );
    }

    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => ['printHeader','printFooter'],
                'users' => ['*'],
            ],
            [
                'allow',
                'actions' => ['delete'],
                'roles' => ['editProtocols']
            ],
            [
                'allow',
                'actions' => ['unsign'],
                'roles' => ['unsignProtocols']
            ],
            [
                'deny',
                'actions' => ['delete', 'unsign'],
                'users' => ['*'],
            ],
            [
                'allow',
                'roles' => ['viewProtocols']
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

    public function init()
    {
        $patientId = Yii::app()->getRequest()->getQuery('id');

        if (!$patientId) {
            throw new CHttpException(400, Yii::t(
                "protocols",
                "Не передан обязательный параметр id (идентификатор пациента)"
            ));
        }

        $state = UserGroupSeparationBehavior::disableDataSeparation();
        $this->patient = $this->loadPatient($patientId);
        UserGroupSeparationBehavior::setState($state);

        if (static::PROTOCOL_DICT_UID) {
            $this->setPageTitle($this->getProtocolName());
        } else {
            $this->setPageTitle(Yii::t('ProtocolsModule.protocols', "Исследование Rehoming"));
        }

        //настройки вывода в pdf
        $pdfOptions = $this->getPdfPageOptions();
        $pdfOptions['header-html'] = Yii::app()->request->getBaseUrl(true) . $this->createUrl(
            $this->id . "/printHeader",
            array("id" => $this->patient->uid, "protocolId" => Yii::app()->request->getParam("protocolId", 0))
        );
        $pdfOptions['footer-html'] = Yii::app()->request->getBaseUrl(true) . $this->createUrl(
            $this->id . "/printFooter",
            array("id" => $this->patient->uid, "protocolId" => Yii::app()->request->getParam("protocolId", 0))
        );
        $this->setPdfPageOptions($pdfOptions);

        Yii::app()->clientScript->registerCssFile('/css/grid/style.css');

        Yii::app()->clientScript->registerScriptFile(
            Yii::app()->assetManager->publish(
                Yii::getPathOfAlias('protocols.assets') . '/js/common.js'
            ),
            CClientScript::POS_END
        );

        Yii::app()->getModule('admin');
    }

    /**
     * @param $patientId
     * @return Patient
     */
    protected function loadPatient($patientId)
    {
        Yii::app()->getModule('patient');
        return Patient::model()->onlyAssigned()->findByPk($patientId);
    }

    public function actionPrintHeader(
        $id,
        $protocolId,
        $page = 0,
        $title = "",
        $frompage = 0,
        $topage = 0,
        $doctitle = 0
    ) {
        if ($page == 1) {
            $widgetName = "ProtocolsFirstPageHeader";
        } else {
            $widgetName = "ProtocolsHeader";
        }

        $state = UserGroupSeparationBehavior::disableDataSeparation();

        echo $this->widget($widgetName, array(
                "title" => $title,
                "patient" => $this->patient,
                "protocolId" => $protocolId
            ), true);

        UserGroupSeparationBehavior::setState($state);
        Yii::app()->end();
    }

    public function actionPrintFooter(
        $id,
        $protocolId,
        $page = 0,
        $title = "",
        $frompage = 0,
        $topage = 0,
        $doctitle = 0
    ) {

        $widgetName = "ProtocolsFooter";
        echo $this->widget(
            $widgetName,
            array(
                "title" => $title,
                "patient" => $this->patient,
                "protocolId" => $protocolId,
                "page" => $page,
                "pageTotal" => $topage),
            true
        );

        Yii::app()->end();
    }

    /**
     * Удаление протокола
     *
     * @param $id
     * @param $protocolId
     */
    public function actionDelete($id, $protocolId)
    {
        /** @var Protocol $protocol */
        $protocol = Protocol::model()->findByPk($protocolId);
        if ($protocol && $protocol->isOpened() && $protocol->research->patient_uid == $id) {
            $protocol->delete();
        }

        $this->redirect($this->createUrl($this->id . "/index", array("id" => $id)));
    }

    /**
     * Снятие статус "подписан" с протокола
     *
     * @param $id
     * @param $protocolId
     */
    public function actionUnsign($id, $protocolId)
    {
        /** @var Protocol $protocol */
        $protocol = Protocol::model()->findByPk($protocolId);
        $protocol->status = 0;
        $protocol->save();
        $this->redirect($this->createUrl($this->id . "/index", array("id" => $id)));
    }

    /**
     * @return mixed
     */
    protected function getProtocolName()
    {
        return ProtocolDictionary::model()->withI18n()->findByPk(static::PROTOCOL_DICT_UID)->name;
    }
}
