<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 30.10.13
 * Time: 17:34
 */

class ProtocolsDependencyValidator extends CValidator
{
    /**
     * @param CModel $object
     * @param string $attribute
     * @throws CException
     */
    protected function validateAttribute($object, $attribute)
    {
        if ($object instanceof FieldDependencyDescriptorInterface) {
            $dependency = $object->getFieldsDependency();

            foreach ($dependency as $parent => $subs) {
                if (in_array($attribute, $subs)) {
                    if ($object->$parent && empty($object->$attribute)) {
                        $this->addError(
                            $object,
                            $attribute,
                            Yii::t('ProtocolsModule.protocols', "Необходимо заполнить поле \"{attribute}\"")
                        );
                    }
                }
            }
        } else {
            throw new CException("Object must implement FieldDependencyDescriptorInterface to validate this value");
        }
    }
}
