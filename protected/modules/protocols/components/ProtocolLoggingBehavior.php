<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 13.01.14
 * Time: 10:46
 *
 * Поведение, отвечающее за логирований действий по созданию, удалению и подписыванию протоколов
 */

class ProtocolLoggingBehavior extends CActiveRecordBehavior
{

    /** @var  ProtocolHistory */
    protected $loggingModel;

    /** @var  Protocol */
    protected $previousState;

    public function __construct()
    {
        $this->loggingModel = new ProtocolHistory();
    }

    public function beforeSave($event)
    {
        $this->saveState();
    }

    public function afterSave($event)
    {
        /** @var Protocol $owner */
        $owner = $event->sender;
        $userId = Yii::app()->user->id;

        if ($owner->getIsNewRecord()) {

            ProtocolHistory::addEvent(
                ProtocolHistory::EVENT_CREATED,
                $owner,
                new RhDateTime(),
                $userId
            );

            if ($owner->status == Protocol::STATUS_CLOSED) {
                ProtocolHistory::addEvent(
                    ProtocolHistory::EVENT_SIGNED,
                    $owner,
                    new RhDateTime(),
                    $userId
                );
            }
        } else {

            if ($owner->status != $this->previousState->status) {
                if ($owner->status == Protocol::STATUS_CLOSED) {
                    $loggingEvent = ProtocolHistory::EVENT_SIGNED;
                } else {
                    $loggingEvent = ProtocolHistory::EVENT_UNSIGNED;
                }

                ProtocolHistory::addEvent(
                    $loggingEvent,
                    $owner,
                    new RhDateTime(),
                    $userId
                );
            }
        }
    }

    public function beforeDelete($event)
    {
        $this->saveState();
    }

    public function afterDelete($event)
    {
        /** @var Protocol $owner */
        $owner = $this->getOwner();
        $userId = Yii::app()->user->id;

        ProtocolHistory::addEvent(
            ProtocolHistory::EVENT_DELETED,
            $owner,
            new RhDateTime(),
            $userId
        );

        $this->loggingModel->archiveEvents($owner->uid);
    }

    protected function saveState()
    {
        /** @var Protocol $owner */
        $owner = $this->getOwner();
        if (!$owner->getIsNewRecord()) {
            $this->previousState = $owner->findByPk($owner->uid);
        }
    }
}
