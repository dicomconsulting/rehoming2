<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 30.10.13
 * Time: 17:34
 */

class CustomFloatValidator extends CValidator
{

    protected $pattern = "/^\d+(\,\d*)?$/i";

    public function clientValidateAttribute($object, $attribute)
    {

        $condition = "value && !value.match({$this->pattern})";

        return "if(" . $condition . ") { messages.push(" .
            CJSON::encode(Yii::t('ProtocolsModule.protocols', "Поле должно быть числом c разделителем \"запятая\", например, \"33,6\""))
            . ");}";
    }

    protected function validateAttribute($object, $attribute)
    {
        $value = $object->$attribute;

        if ($value && !preg_match($this->pattern, $value)) {
            $this->addError(
                $object,
                $attribute,
                Yii::t('ProtocolsModule.protocols', "Поле \"{attribute}\" должно быть числом, например, 33,6")
            );
        }
    }
}
