<?php

class EnrolmentController extends BaseController
{

    /**
     * Сcылка на словарь протоколов
     */
    const PROTOCOL_DICT_UID = 1;

    public function actions()
    {
        return array(
            'diseaseSelector.' => 'directory.widgets.DiseaseSelector',
            'pharmSelector.'   => 'directory.widgets.PharmSelector',
        );
    }

    public function actionIndex($id)
    {
        $protocols = Protocol::getProtocolsByTypeAndPatient(self::PROTOCOL_DICT_UID, $this->patient);

        $this->render(
            "index",
            array(
                "protocols"    => $protocols,
                "patientId"    => $id,
                "canCreateNew" => !Research::model()->isWorkflowBegun($this->patient)
            )
        );
    }

    public function actionEdit($id, $protocolId = 0)
    {
        $modelProtocol = Protocol::model()->findByPk($protocolId);

        if ($modelProtocol) {
            /** @var Protocol $modelProtocol */
            $modelResearch = $modelProtocol->research;
            $modelProtocolEnrolment = $modelProtocol->protocolEnrolment;
        } else {
            $modelResearch = new Research;
//            $modelResearch->assignScreeningNumber();
            $modelResearch->patient_uid = $this->patient->uid;
            $modelResearch->patient_code = $this->patient->getCode();

            $modelProtocol = new Protocol;
            $modelProtocol->protocol_dictionary_uid = self::PROTOCOL_DICT_UID;
            $modelProtocol->user_uid = Yii::app()->user->id;
            $modelProtocol->research = $modelResearch;

            $modelProtocolEnrolment = new ProtocolEnrolment;
            $modelProtocolEnrolment->protocol = $modelProtocol;

            // Заполнение значением по умолчанию
            $modelProtocolEnrolment->initDefaultValues();

            $filler = new ProtocolFiller($modelProtocolEnrolment, $this->patient);
            $filler->fill(ProtocolFiller::STATE_LAST);
        }

        $transaction = $modelResearch->dbConnection->beginTransaction();

        // Аякс-валидация
        if (Yii::app()->getRequest()->getIsAjaxRequest()) {
            echo CActiveForm::validate(array($modelProtocolEnrolment));
            Yii::app()->end();
        }

        if (isset($_POST['Research']) && isset($_POST['Protocol']) && isset($_POST['ProtocolEnrolment'])
            && Yii::app()->user->checkAccess("editProtocols")
        ) {

            $modelResearch->attributes = $_POST['Research'];
            $modelProtocol->attributes = $_POST['Protocol'];
            $modelProtocolEnrolment->attributes = $_POST['ProtocolEnrolment'];

            if (isset($_POST['ProtocolEnrolment']['surgeryRel'])
                && !empty($_POST['ProtocolEnrolment']['surgeryRel'])
            ) {

                $modelProtocolEnrolment->assignRelation(
                    "surgeryRel",
                    "surgery_uid",
                    $_POST['ProtocolEnrolment']['surgeryRel']
                );

            } else {
                $modelProtocolEnrolment->surgeryRel = array();
            }

            //добавляем в модель связь с лекарственными препаратами
            if (isset($_POST['ProtocolEnrolment']['pharm'])
                && !empty($_POST['ProtocolEnrolment']['pharm'])
            ) {

                $modelProtocolEnrolment->assignRelation(
                    "pharm",
                    "pharm_product_uid",
                    $_POST['ProtocolEnrolment']['pharm']
                );
            } else {
                $modelProtocolEnrolment->pharm = array();
            }

            if (isset($_POST['ProtocolEnrolment']['diagnosis'])
                && !empty($_POST['ProtocolEnrolment']['diagnosis'])
            ) {
                $modelProtocolEnrolment->diagnosis = Icd10::model()
                    ->findAllByPk($_POST['ProtocolEnrolment']['diagnosis']);
            } else {
                $modelProtocolEnrolment->diagnosis = array();
            }


            if ($modelResearch->validate()) {
                $modelResearch->save();

                $modelProtocol->research_uid = $modelResearch->uid;

                if ($modelProtocol->validate()) {

                    $modelProtocol->save();

                    $modelProtocolEnrolment->protocol_uid = $modelProtocol->uid;

                    if ($modelProtocolEnrolment->validate()) {

                        /**
                         * Работаем с дополнительным полем etiologyIds т.к. при редактировании в
                         * ProtocolEnrolment::$etiology будет
                         * массив объектов, которые checkBoxList "не поймет"
                         */
                        if (isset($_POST['ProtocolEnrolment']['etiologyIds'])
                            && !empty($_POST['ProtocolEnrolment']['etiologyIds'])
                        ) {
                            $modelProtocolEnrolment->etiology = Icd10::model()
                                ->findAllByPk($_POST['ProtocolEnrolment']['etiologyIds']);
                        } else {
                            $modelProtocolEnrolment->etiology = array();
                        }

                        $modelProtocolEnrolment->save();

                        if (empty($modelProtocolEnrolment->surgeryRel)) {
                            ProtocolEnrolmentSurgery::model()->deleteAllByAttributes(
                                array("protocol_uid" => $modelProtocolEnrolment->protocol_uid)
                            );
                        }

                        if (empty($modelProtocolEnrolment->pharm)) {
                            ProtocolEnrolmentPharm::model()->deleteAllByAttributes(
                                array("protocol_uid" => $modelProtocolEnrolment->protocol_uid)
                            );
                        }

                        if (isset($_POST['sign']) && $_POST['sign']
                            && Yii::app()->user->checkAccess("signProtocols")
                        ) {
                            //меняет статус протокола, после успешной валидации всех данных
                            $modelProtocol->status = 1;
                            $modelProtocol->save();
                        }

                        $transaction->commit();

                        $url = $this->createUrl($this->id . "/index", ['id' => $modelProtocol->research->patient_uid]);
                        $this->redirect($url);

                    } else {
                        $transaction->rollback();
                    }
                } else {
                    $transaction->rollback();
                }
            }
        }

//        $pharmGroups = PharmClinicGroup::model()->withI18n(Yii::app()->language)->findAllByPk(
//            ProtocolEnrolment::model()->getPharmGroups()
//        );
        $pharmGroups = ProtocolEnrolment::getPharmsTmp();
        $pharmMeasureUnits = PharmMeasure::makeFlatArray(MeasurementUnit::model()->getPharmUnits());

        if ($modelResearch->screening_number) {
            $modelResearch->screening_number = str_pad($modelResearch->screening_number, 3, 0, STR_PAD_LEFT);
        }
        $this->render(
            "edit",
            array(
                "research"          => $modelResearch,
                "protocol"          => $modelProtocol,
                "enrolment"         => $modelProtocolEnrolment,
                "pharmGroups"       => $pharmGroups,
                "pharmMeasureUnits" => $pharmMeasureUnits,
            )
        );
    }

    public function actionDelete($id, $protocolId)
    {
        /** @var Protocol $protocol */
        $protocol = Protocol::model()->findByPk($protocolId);
        $research = $protocol->research;
        $protocols = $research->getAllProtocols();

        if (count($protocols) > 1 || !$protocol->isOpened()) {
            $this->render("delete", array('protocols' => $protocols, "protocol" => $protocol));
        } else {
            $research->delete();
            $this->redirect($this->createUrl($this->id . "/index", ['id' => $id]));
        }
    }

    /**
     * Снятие статус "подписан" с протокола
     *
     * @TODO вынести логику определения возможности открытия протокола в модель, возможно, понадобится система правил
     * @param $id
     * @param $protocolId
     */
    public function actionUnsign($id, $protocolId)
    {
        /** @var Protocol $protocol */
        $protocol = Protocol::model()->findByPk($protocolId);
        $protocols = $protocol->research->getAllProtocols();

        # https://jira.kirkazan.ru/browse/REHOMING-304
        # 2. Сделать возможность редактировать протокол Включения в исследование при созданном протоколе Инициализация НМ
        $exclude = [ProtocolDictionary::PROTOCOL_ENROLMENT, ProtocolDictionary::PROTOCOL_INITHM];

        $protocolsToShow = [];

        foreach ($protocols as $key => $row) {
            /** @var Protocol $row */
            if (in_array($row->protocol_dictionary_uid, $exclude)) {
                continue;
            }
            $protocolsToShow[] = $row;
        }

        if ($protocolsToShow) {
            $this->render("unsign", array("protocols" => $protocolsToShow));
        } else {
            $protocol->status = 0;
            $protocol->save();
            $this->redirect($this->createUrl($this->id . "/index", array("id" => $id)));
        }
    }

    public function actionPrint($id, $protocolId)
    {
        $this->layout = '//layouts/print';

        /** @var Protocol $modelProtocol */
        $modelProtocol = Protocol::model()->findByPk($protocolId);
        $modelResearch = $modelProtocol->research;
        $modelProlocolEnrolment = $modelProtocol->protocolEnrolment;

        $pharmGroups = ProtocolEnrolment::getPharmsTmp();
        $pharmMeasureUnits = PharmMeasure::makeFlatArray(MeasurementUnit::model()->getPharmUnits());

        $method = "renderPdf";

        $this->$method(
            "edit",
            array(
                "research"          => $modelResearch,
                "protocol"          => $modelProtocol,
                "enrolment"         => $modelProlocolEnrolment,
                "pharmGroups"       => $pharmGroups,
                "pharmMeasureUnits" => $pharmMeasureUnits,
            )
        );
    }


    /**
     * Преобразует ед. измерения лекарственных препаратов для использования в шаблоне mustache
     *
     * @param $units
     * @param $selected
     * @return array
     */
    public function preparePharmUnits($units, $selected)
    {
        $res = array();

        foreach ($units as $key => $unit) {
            $tmpArr = array(
                "id"  => $key,
                "val" => $unit
            );

            if ($key == $selected) {
                $tmpArr['selected'] = true;
            }

            $res[] = $tmpArr;
        }

        return $res;
    }
}
