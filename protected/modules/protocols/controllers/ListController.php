<?php

class ListController extends BaseController
{

    public function actionIndex($id)
    {

        try {
            $research = Research::model()->getOpenedByPatient($id);
        } catch (ProtocolsException $exception) {
        }

        $this->render(
            'index',
            array(
                "patient" => $this->patient,
                "researchOpened" => isset($research)
            )
        );
    }
}