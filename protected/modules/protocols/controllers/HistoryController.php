<?php

class HistoryController extends BaseController
{
    public function init()
    {
        parent::init();
        $this->pageTitle = Yii::t("ProtocolsModule.protocols", "История протокола");
    }


    public function actionIndex($id, $protocolId)
    {
        /** @var Protocol $protocol */
        $protocol = Protocol::model()->findByPk($protocolId);

        if (!$protocol) {
            throw new CHttpException(404, Yii::t("ProtocolsModule.protocols", "Протокол не существует"));
        }

        $history = ProtocolHistory::getHistoryByProtocol($protocol->uid);

        $this->render("index", ['protocol' => $protocol, 'history' => $history]);
    }

    public function actionAll($id, $researchId)
    {
        /** @var Research $research */
        $research = Research::model()->findByPk($researchId);

        if (!$research) {
            throw new CHttpException(404, Yii::t("ProtocolsModule.protocols", "Исследование не существует"));
        }

        $history = ProtocolHistory::getHistoryByResearch($research->uid);

        $this->render("all", ['research' => $research, 'history' => $history]);
    }
}