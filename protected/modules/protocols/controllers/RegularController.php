<?php

class RegularController extends BaseController
{

    /**
     * Сcылка на словарь протоколов
     */
    const PROTOCOL_DICT_UID = 6;

    public function actionIndex($id)
    {
        $protocols = Protocol::getProtocolsByTypeAndPatient(self::PROTOCOL_DICT_UID, $this->patient);

        $modelResearch = Research::model()->getOpenedByPatient($id);
        $canCreateNew = $modelResearch && ($modelResearch->getStatus() == Research::STATUS_INITIATED || $modelResearch->getStatus() == Research::STATUS_ENROLLED); //@todo;

        $this->render(
            "index",
            array(
                "protocols" => $protocols,
                "patientId" => $id,
                "canCreateNew" => $canCreateNew
            )
        );
    }

    public function actionEdit($id, $protocolId = false)
    {

        if ($protocolId) {
            /** @var Protocol $modelProtocol */
            $modelProtocol = Protocol::model()->with(array("research", "protocolRegular"))->findByPk($protocolId);
            $modelResearch = $modelProtocol->research;

            $modelProlocolRegular = $modelProtocol->protocolRegular;
        } else {

            $modelResearch = Research::model()->getOpenedByPatient($id);

            if (!$modelResearch) {
                $this->redirect($this->createUrl("/protocols/list/index", ["id" => $id]));
            }

            $modelProtocol = new Protocol;
            $modelProtocol->protocol_dictionary_uid = self::PROTOCOL_DICT_UID;
            $modelProtocol->research_uid = $modelResearch->uid;
            $modelProtocol->user_uid = Yii::app()->user->id;

            $modelProlocolRegular = new ProtocolRegular;

            $filler = new ProtocolFiller($modelProlocolRegular, $this->patient);
            $filler->fill(ProtocolFiller::STATE_LAST);
        }

        // Аякс-валидация
        if (Yii::app()->getRequest()->getIsAjaxRequest()) {
            echo CActiveForm::validate(array($modelProlocolRegular));
            Yii::app()->end();
        }

        //список НЯ по пациенту
        $findingRedList = Message::getMessagesByPatient(
            $modelResearch->patient,
            null,
            'red',
            true
        );

        $transaction = $modelResearch->dbConnection->beginTransaction();

        if (isset($_POST['Protocol']) && isset($_POST['ProtocolRegular'])
            && Yii::app()->user->checkAccess("editProtocols")) {

            $this->prepareProtocolData($_POST['Protocol']);

            $modelProtocol->attributes = $_POST['Protocol'];
            $modelProlocolRegular->attributes = $_POST['ProtocolRegular'];

            //добавляем в модель связь с НЯ
            if (isset($_POST['ProtocolRegular']['finding'])
                && !empty($_POST['ProtocolRegular']['finding'])) {

                $modelProlocolRegular->assignRelation(
                    "finding",
                    "finding_uid",
                    $_POST['ProtocolRegular']['finding']
                );
            } else {
                $modelProlocolRegular->finding = array();
            }

            if ($modelProtocol->validate()) {

                $modelProtocol->save();

                $modelProlocolRegular->protocol_uid = $modelProtocol->uid;

                if ($modelProlocolRegular->validate()) {

                    $modelProlocolRegular->save();

                    if (empty($modelProlocolRegular->finding)) {
                        ProtocolFindingRelation::model()->deleteAllByAttributes(
                            array("protocol_uid" => $modelProlocolRegular->protocol_uid)
                        );
                    }

                    if (isset($_POST['sign']) && Yii::app()->user->checkAccess("signProtocols")) {
                        //меняет статус протокола
                        $modelProtocol->status = 1;
                        $modelProtocol->save();
                    }

                    $transaction->commit();

                    $this->redirect($this->createUrl($this->id . "/index", ['id' => $id]));

                } else {
                    $transaction->rollback();
                }
            }
        }

        $modelProlocolEnrolment = ProtocolEnrolment::model()->getByResearch($modelResearch);

        $this->render(
            "edit",
            array(
                "research" => $modelResearch,
                "protocol" => $modelProtocol,
                "regular" => $modelProlocolRegular,
                "enrolment" => $modelProlocolEnrolment,
                "findingRedList" => $findingRedList,
            )
        );
    }

    protected function prepareProtocolData(&$data)
    {
        if (isset($data['af_episode_time']) && is_array($data['af_episode_time'])) {
            $d = isset($data['af_episode_time']['d']) ? $data['af_episode_time']['d']: 0;
            $h = isset($data['af_episode_time']['h']) ? $data['af_episode_time']['h']: 0;
            $i = isset($data['af_episode_time']['i']) ? $data['af_episode_time']['i']: 0;
            $s = isset($data['af_episode_time']['s']) ? $data['af_episode_time']['s']: 0;

            $data['af_episode_time'] = new DateInterval(
                sprintf("P0000-00-%1$02dT%2$02d:%3$02d:%4$02d"),
                $d,
                $h,
                $i,
                $s
            );
        }
    }

    public function actionPrint($id, $protocolId)
    {
        $this->layout = '//layouts/print';

        /** @var Protocol $modelProtocol */
        $modelProtocol = Protocol::model()->with(array("research"))->findByPk($protocolId);
        $modelResearch = $modelProtocol->research;
        $modelProlocolRegular = $modelProtocol->protocolRegular;

        $modelProlocolEnrolment = ProtocolEnrolment::model()->getByResearch($modelResearch);

        //список НЯ по пациенту
        $findingRedList = Message::getMessagesByPatient(
            $modelResearch->patient,
            null,
            'red',
            true
        );

        $this->renderPdf(
            "edit",
            array(
                "research" => $modelResearch,
                "protocol" => $modelProtocol,
                "regular" => $modelProlocolRegular,
                "enrolment" => $modelProlocolEnrolment,
                "findingRedList" => $findingRedList,
            )
        );
    }
}