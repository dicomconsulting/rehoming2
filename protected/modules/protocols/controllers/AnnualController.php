<?php

class AnnualController extends BaseController
{

    public function actions()
    {
        return array(
            'diseaseSelector.'=>'directory.widgets.DiseaseSelector',
        );
    }

    /**
     * Сcылка на словарь протоколов
     */
    const PROTOCOL_DICT_UID = 4;

    public function actionIndex($id)
    {
        $protocols = Protocol::getProtocolsByTypeAndPatient(self::PROTOCOL_DICT_UID, $this->patient);

        $modelResearch = Research::model()->getOpenedByPatient($id);
        $canCreateNew = $modelResearch && ($modelResearch->getStatus() == Research::STATUS_INITIATED || $modelResearch->getStatus() == Research::STATUS_ENROLLED); //@todo

        $this->render(
            "index",
            array(
                "protocols" => $protocols,
                "patientId" => $id,
                "canCreateNew" => $canCreateNew
            )
        );
    }

    public function actionEdit($id, $protocolId = false)
    {

        if ($protocolId) {
            /** @var Protocol $modelProtocol */
            $modelProtocol = Protocol::model()->with(array("research", "protocolAnnual"))->findByPk($protocolId);
            $modelResearch = $modelProtocol->research;

            $modelProlocolAnnual = $modelProtocol->protocolAnnual;
        } else {

            $modelResearch = Research::model()->getOpenedByPatient($id);

            if (!$modelResearch) {
                $this->redirect($this->createUrl("/protocols/list/index", ["id" => $id]));
            }

            $modelProtocol = new Protocol;
            $modelProtocol->protocol_dictionary_uid = self::PROTOCOL_DICT_UID;
            $modelProtocol->research_uid = $modelResearch->uid;
            $modelProtocol->user_uid = Yii::app()->user->id;

            $modelProlocolAnnual = new ProtocolAnnual;

            $filler = new ProtocolFiller($modelProlocolAnnual, $this->patient);
            $filler->fill(ProtocolFiller::STATE_LAST);
        }

        // Аякс-валидация
        if (Yii::app()->getRequest()->getIsAjaxRequest()) {
            echo CActiveForm::validate(array($modelProlocolAnnual));
            Yii::app()->end();
        }

        $transaction = $modelResearch->dbConnection->beginTransaction();

        if (isset($_POST['Protocol']) && isset($_POST['ProtocolAnnual'])
            && Yii::app()->user->checkAccess("editProtocols")) {

            $modelProtocol->attributes = $_POST['Protocol'];
            $modelProlocolAnnual->attributes = $_POST['ProtocolAnnual'];

            if (isset($_POST['ProtocolAnnual']['deseaseRel'])
                && !empty($_POST['ProtocolAnnual']['deseaseRel'])) {

                $modelProlocolAnnual->assignRelation(
                    "deseaseRel",
                    "icd10_uid",
                    $_POST['ProtocolAnnual']['deseaseRel']
                );

            } else {
                $modelProlocolAnnual->deseaseRel = array();
            }

            if ($modelProtocol->validate()) {

                $modelProtocol->save();

                $modelProlocolAnnual->protocol_uid = $modelProtocol->uid;

                if ($modelProlocolAnnual->validate()) {

                    $modelProlocolAnnual->save();

                    if (empty($modelProlocolAnnual->deseaseRel)) {
                        ProtocolAnnualDesease::model()->deleteAllByAttributes(
                            array("protocol_uid" => $modelProlocolAnnual->protocol_uid)
                        );
                    }

                    if (isset($_POST['sign']) && Yii::app()->user->checkAccess("signProtocols")) {
                        //меняет статус протокола, после успешной валидации всех данных
                        $modelProtocol->status = 1;
                        $modelProtocol->save();
                    }

                    $transaction->commit();

                    $this->redirect($this->createUrl($this->id . "/index", ['id' => $id]));
                } else {
                    $transaction->rollback();
                }
            }
        }

        $this->render(
            "edit",
            array(
                "research" => $modelResearch,
                "protocol" => $modelProtocol,
                "annual" => $modelProlocolAnnual,
            )
        );
    }

    public function actionPrint($id, $protocolId)
    {
        $this->layout = '//layouts/print';

        /** @var Protocol $modelProtocol */
        $modelProtocol = Protocol::model()->with(array("research"))->findByPk($protocolId);
        $modelResearch = $modelProtocol->research;
        $modelProlocolAnnual = $modelProtocol->protocolAnnual;

        $this->renderPdf(
            "edit",
            array(
                "research" => $modelResearch,
                "protocol" => $modelProtocol,
                "annual" => $modelProlocolAnnual,
            )
        );
    }
}
