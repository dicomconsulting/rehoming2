<?php

class AdverseController extends BaseController
{

    /**
     * Сcылка на словарь протоколов
     */
    const PROTOCOL_DICT_UID = 5;

    public function actionIndex($id)
    {
        $protocols = Protocol::getProtocolsByTypeAndPatient(self::PROTOCOL_DICT_UID, $this->patient);

        $modelResearch = Research::model()->getOpenedByPatient($id);
        $canCreateNew = $modelResearch && ($modelResearch->getStatus() == Research::STATUS_INITIATED || $modelResearch->getStatus() == Research::STATUS_ENROLLED); //@todo;

        $this->render(
            "index",
            array(
                "protocols" => $protocols,
                "patientId" => $id,
                "canCreateNew" => $canCreateNew
            )
        );
    }

    public function actionEdit($id, $protocolId = false)
    {

        if ($protocolId) {
            /** @var Protocol $modelProtocol */
            $modelProtocol = Protocol::model()->with(array("research", "protocolAdverse"))->findByPk($protocolId);
            $modelResearch = $modelProtocol->research;
            $modelProlocolAdverse = $modelProtocol->protocolAdverse;
        } else {

            $modelResearch = Research::model()->getOpenedByPatient($id);

            if (!$modelResearch) {
                $this->redirect($this->createUrl("/protocols/list/index", ["id" => $id]));
            }

            $modelProtocol = new Protocol;
            $modelProtocol->protocol_dictionary_uid = self::PROTOCOL_DICT_UID;
            $modelProtocol->research_uid = $modelResearch->uid;
            $modelProtocol->user_uid = Yii::app()->user->id;

            $modelProlocolAdverse = new ProtocolAdverse;

            $filler = new ProtocolFiller($modelProlocolAdverse, $this->patient);
            $filler->fill(ProtocolFiller::STATE_LAST);
        }

        // Аякс-валидация
        if (Yii::app()->getRequest()->getIsAjaxRequest()) {
            echo CActiveForm::validate(array($modelProlocolAdverse));
            Yii::app()->end();
        }

        $transaction = $modelResearch->dbConnection->beginTransaction();

        if (isset($_POST['Protocol']) && isset($_POST['ProtocolAdverse'])
            && Yii::app()->user->checkAccess("editProtocols")) {

            $modelProtocol->attributes = $_POST['Protocol'];
            $modelProlocolAdverse->attributes = $_POST['ProtocolAdverse'];

            if ($modelProtocol->validate()) {

                $modelProtocol->save();

                $modelProlocolAdverse->protocol_uid = $modelProtocol->uid;

                if ($modelProlocolAdverse->validate()) {
                    $modelProlocolAdverse->save();

                    if (isset($_POST['sign']) && Yii::app()->user->checkAccess("signProtocols")) {
                        //меняет статус протокола, после успешной валидации всех данных
                        $modelProtocol->status = 1;
                        $modelProtocol->save();
                    }

                    $transaction->commit();

                    $this->redirect($this->createUrl($this->id . "/index", ['id' => $id]));

                } else {
                    $transaction->rollback();
                }
            }
        }

        $this->render(
            "edit",
            array(
                "research" => $modelResearch,
                "protocol" => $modelProtocol,
                "adverse" => $modelProlocolAdverse,
            )
        );
    }

    public function actionPrint($id, $protocolId)
    {
        $this->layout = '//layouts/print';

        /** @var Protocol $modelProtocol */
        $modelProtocol = Protocol::model()->with(array("research"))->findByPk($protocolId);
        $modelResearch = $modelProtocol->research;
        $modelProlocolAdverse = $modelProtocol->protocolAdverse;


        $this->renderPdf(
            "edit",
            array(
                "research" => $modelResearch,
                "protocol" => $modelProtocol,
                "adverse" => $modelProlocolAdverse,
            )
        );
    }
}