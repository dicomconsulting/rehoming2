<?php

class InithmController extends BaseController
{

    /**
     * Сcылка на словарь протоколов
     */
    const PROTOCOL_DICT_UID = 2;

    public function actionIndex($id)
    {
        $protocols = Protocol::getProtocolsByTypeAndPatient(self::PROTOCOL_DICT_UID, $this->patient);

        $modelResearch = Research::model()->getOpenedByPatient($id);
        $canCreateNew = $modelResearch && !$modelResearch->isInitiatingBegun();


        $this->render(
            "index",
            array(
                "protocols" => $protocols,
                "patientId" => $id,
                "canCreateNew" => $canCreateNew,
                'research' => $modelResearch
            )
        );
    }

    public function actionEdit($id, $protocolId = false)
    {

        if ($protocolId) {
            /** @var Protocol $modelProtocol */
            $modelProtocol = Protocol::model()->with(array("research", "protocolInithm"))->findByPk($protocolId);
            $modelResearch = $modelProtocol->research;
            $modelProlocolInithm = $modelProtocol->protocolInithm;
        } else {

            $modelResearch = Research::model()->getOpenedByPatient($id);

            if (!$modelResearch) {
                $this->redirect($this->createUrl("/protocols/list/index", ["id" => $id]));
            }

            $modelProtocol = new Protocol;
            $modelProtocol->protocol_dictionary_uid = self::PROTOCOL_DICT_UID;
            $modelProtocol->research_uid = $modelResearch->uid;
            $modelProtocol->user_uid = Yii::app()->user->id;

            $modelProlocolInithm = new ProtocolInithm;
        }

        // Аякс-валидация
        if (Yii::app()->getRequest()->getIsAjaxRequest()) {
            echo CActiveForm::validate(array($modelProlocolInithm));
            Yii::app()->end();
        }

        $transaction = $modelResearch->dbConnection->beginTransaction();

        if (isset($_POST['Protocol']) && isset($_POST['ProtocolInithm'])
            && Yii::app()->user->checkAccess("editProtocols")) {
            $modelProtocol->attributes = $_POST['Protocol'];
            $modelProlocolInithm->attributes = $_POST['ProtocolInithm'];

            if ($modelProtocol->validate()) {

                $modelProtocol->save();

                $modelProlocolInithm->protocol_uid = $modelProtocol->uid;

                if ($modelProlocolInithm->validate()) {

                    $modelProlocolInithm->save();

                    if (isset($_POST['sign']) && Yii::app()->user->checkAccess("signProtocols")) {
                        //меняет статус протокола
                        $modelProtocol->status = 1;
                        $modelProtocol->save();
                    }

                    $transaction->commit();

                    $this->redirect($this->createUrl($this->id . "/index", ['id' => $id]));

                } else {
                    $transaction->rollback();
                }
            }
        }

        $this->render(
            "edit",
            array(
                "research" => $modelResearch,
                "protocol" => $modelProtocol,
                "initHm" => $modelProlocolInithm,
            )
        );
    }


    public function actionDelete($id, $protocolId)
    {
        /** @var Protocol $protocol */
        $protocol = Protocol::model()->findByPk($protocolId);
        $research = $protocol->research;
        $protocols = $research->getAllProtocols();

        if ($research->getStatus() != Research::STATUS_ENROLLED || !$protocol->isOpened()) {
            $this->render("delete", array('protocols' => $protocols, "protocol" => $protocol));
        } else {
            $protocol->delete();
            $this->redirect($this->createUrl($this->id . "/index", ['id' => $id]));
        }
    }


    /**
     * Снятие статус "подписан" с протокола
     * @param $id
     * @param $protocolId
     */
    public function actionUnsign($id, $protocolId)
    {
        /** @var Protocol $protocol */
        $protocol = Protocol::model()->findByPk($protocolId);
        $protocols = $protocol->research->getAllProtocols();

        $exclude = [ProtocolDictionary::PROTOCOL_ENROLMENT, ProtocolDictionary::PROTOCOL_INITHM];

        $protocolsToShow = [];

        foreach ($protocols as $key => $row) {
            /** @var Protocol $row */
            if (in_array($row->protocol_dictionary_uid, $exclude)) {
                continue;
            }
            $protocolsToShow[] = $row;
        }

        if ($protocolsToShow) {
            $this->render("unsign", array("protocols" => $protocolsToShow));
        } else {
            $protocol->status = 0;
            $protocol->save();
            $this->redirect($this->createUrl($this->id . "/index", array("id" => $id)));
        }
    }

    public function actionPrint($id, $protocolId)
    {
        $this->layout = '//layouts/print';

        /** @var Protocol $modelProtocol */
        $modelProtocol = Protocol::model()->with(array("research", "protocolInithm"))->findByPk($protocolId);
        $modelResearch = $modelProtocol->research;
        $modelProlocolInithm = $modelProtocol->protocolInithm;

        $this->renderPdf(
            "edit",
            array(
                "research" => $modelResearch,
                "protocol" => $modelProtocol,
                "initHm" => $modelProlocolInithm,
            )
        );
    }
}