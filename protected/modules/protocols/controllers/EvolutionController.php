<?php

class EvolutionController extends BaseController
{

    /**
     * Сcылка на словарь протоколов
     */
    const PROTOCOL_DICT_UID = 3;

    public function actionIndex($id)
    {
        $protocols = Protocol::getProtocolsByTypeAndPatient(self::PROTOCOL_DICT_UID, $this->patient);

        $modelResearch = Research::model()->getOpenedByPatient($id);
        $canCreateNew = $modelResearch && ($modelResearch->getStatus() == Research::STATUS_INITIATED || $modelResearch->getStatus() == Research::STATUS_ENROLLED); //@todo

        $this->render(
            "index",
            array(
                "protocols" => $protocols,
                "patientId" => $id,
                "canCreateNew" => $canCreateNew
            )
        );
    }

    public function actionEdit($id, $protocolId = false)
    {

        if ($protocolId) {
            /** @var Protocol $modelProtocol */
            $modelProtocol = Protocol::model()->with(array("research", "protocolEvolution"))->findByPk($protocolId);
            $modelResearch = $modelProtocol->research;

            $modelProlocolEvolution = $modelProtocol->protocolEvolution;
        } else {
            //создаем
            $modelResearch = Research::model()->getOpenedByPatient($id);

            if (!$modelResearch) {
                $this->redirect($this->createUrl("/protocols/list/index", ["id" => $id]));
            }

            $modelProtocol = new Protocol;
            $modelProtocol->protocol_dictionary_uid = self::PROTOCOL_DICT_UID;
            $modelProtocol->research_uid = $modelResearch->uid;
            $modelProtocol->user_uid = Yii::app()->user->id;

            $modelProlocolEvolution = new ProtocolEvolution;
        }

        // Аякс-валидация
        if (Yii::app()->getRequest()->getIsAjaxRequest()) {
            echo CActiveForm::validate(array($modelProlocolEvolution));
            Yii::app()->end();
        }

        //список НЯ по пациенту
        $findingRedList = Message::getMessagesByPatient(
            $modelResearch->patient,
            null,
            'red',
            true
        );

        $findingYellowList = Message::getMessagesByPatient(
            $modelResearch->patient,
            null,
            'yellow',
            true
        );

        $transaction = $modelResearch->dbConnection->beginTransaction();

        if (isset($_POST['Protocol']) && isset($_POST['ProtocolEvolution'])
            && Yii::app()->user->checkAccess("editProtocols")) {

            $modelProtocol->attributes = $_POST['Protocol'];
            $modelProlocolEvolution->attributes = $_POST['ProtocolEvolution'];

            if (isset($_POST['ProtocolEvolution']['finding'])
                && !empty($_POST['ProtocolEvolution']['finding'])) {

                $modelProlocolEvolution->assignRelation(
                    "finding",
                    "finding_uid",
                    $_POST['ProtocolEvolution']['finding']
                );
            } else {
                $modelProlocolEvolution->finding = array();
            }

            if ($modelProtocol->validate()) {

                $modelProtocol->save();

                $modelProlocolEvolution->protocol_uid = $modelProtocol->uid;

                if ($modelProlocolEvolution->validate()) {

                    $modelProlocolEvolution->save();

                    //добавляем в модель связь с НЯ
                    if (empty($modelProlocolEvolution->finding)) {
                        ProtocolFindingRelation::model()->deleteAllByAttributes(
                            array("protocol_uid" => $modelProlocolEvolution->protocol_uid)
                        );
                    }

                    if (isset($_POST['sign']) && Yii::app()->user->checkAccess("signProtocols")) {
                        //меняет статус протокола, после успешной валидации всех данных
                        $modelProtocol->status = 1;
                        $modelProtocol->save();
                    }

                    $transaction->commit();

                    $this->redirect($this->createUrl($this->id . "/index", ['id' => $id]));

                } else {
                    $transaction->rollback();
                }
            }
        }

        $this->render(
            "edit",
            array(
                "research" => $modelResearch,
                "protocol" => $modelProtocol,
                "evolution" => $modelProlocolEvolution,
                "findingRedList" => $findingRedList,
                "findingYellowList" => $findingYellowList,
            )
        );
    }

    public function actionPrint($id, $protocolId)
    {
        $this->layout = '//layouts/print';

        /** @var Protocol $modelProtocol */
        $modelProtocol = Protocol::model()->with(array("research"))->findByPk($protocolId);
        $modelResearch = $modelProtocol->research;
        $modelProlocolEvolution = $modelProtocol->protocolEvolution;

        //список НЯ по пациенту
        $findingRedList = Message::getMessagesByPatient(
            $modelResearch->patient,
            null,
            'red',
            true
        );

        $findingYellowList = Message::getMessagesByPatient(
            $modelResearch->patient,
            null,
            'yellow',
            true
        );

        $this->renderPdf(
            "edit",
            array(
                "research" => $modelResearch,
                "protocol" => $modelProtocol,
                "evolution" => $modelProlocolEvolution,
                "findingRedList" => $findingRedList,
                "findingYellowList" => $findingYellowList,
            )
        );
    }
}
