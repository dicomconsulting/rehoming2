<?php

class FinishController extends BaseController
{
    /**
     * Сcылка на словарь протоколов
     */
    const PROTOCOL_DICT_UID = 7;

    public function actionIndex($id)
    {
        $protocols = Protocol::getProtocolsByTypeAndPatient(self::PROTOCOL_DICT_UID, $this->patient);

        $modelResearch = Research::model()->getOpenedByPatient($id);
        $canCreateNew = $modelResearch
            && ($modelResearch->getStatus() == Research::STATUS_INITIATED || $modelResearch->getStatus() == Research::STATUS_ENROLLED) && !$modelResearch->isTerminationBegun(); //@todo

        $this->render(
            "index",
            array(
                "protocols" => $protocols,
                "patientId" => $id,
                "canCreateNew" => $canCreateNew
            )
        );
    }


    public function actionEdit($id, $protocolId = false)
    {

        if ($protocolId) {
            /** @var Protocol $modelProtocol */
            $modelProtocol = Protocol::model()->with(array("research", "protocolFinish"))->findByPk($protocolId);
            $modelResearch = $modelProtocol->research;

            $modelProlocolFinish = $modelProtocol->protocolFinish;
        } else {

            $modelResearch = Research::model()->getOpenedByPatient($id);

            if (!$modelResearch) {
                $this->redirect($this->createUrl("/protocols/list/index", ["id" => $id]));
            }

            $modelProtocol = new Protocol;
            $modelProtocol->protocol_dictionary_uid = self::PROTOCOL_DICT_UID;
            $modelProtocol->research_uid = $modelResearch->uid;
            $modelProtocol->user_uid = Yii::app()->user->id;

            $modelProlocolFinish = new ProtocolFinish;
        }

        $modelResearch->setScenario("finish");

        // Аякс-валидация
        if (Yii::app()->getRequest()->getIsAjaxRequest()) {
            echo CActiveForm::validate(array($modelProlocolFinish));
            Yii::app()->end();
        }

        $transaction = $modelResearch->dbConnection->beginTransaction();

        if (isset($_POST['Research']) && isset($_POST['Protocol']) && isset($_POST['ProtocolFinish'])
            && Yii::app()->user->checkAccess("editProtocols")) {

            $modelResearch->attributes = $_POST['Research'];
            $modelProtocol->attributes = $_POST['Protocol'];
            $modelProlocolFinish->attributes = $_POST['ProtocolFinish'];

            if ($modelResearch->validate()) {
                $modelResearch->save();

                if ($modelProtocol->validate()) {

                    $modelProtocol->save();

                    $modelProlocolFinish->protocol_uid = $modelProtocol->uid;

                    if ($modelProlocolFinish->validate()) {

                        $modelProlocolFinish->save();

                        if (isset($_POST['sign']) && Yii::app()->user->checkAccess("signProtocols")) {
                            if ($modelResearch->canBeClosed()) {
                                //меняет статус протокола
                                $modelProtocol->status = 1;
                                $modelProtocol->save();
                            } else {
                                $this->redirect($this->createUrl($this->id . "/openedList", array("id" => $id)));
                            }
                        }

                        $transaction->commit();

                        $this->redirect($this->createUrl($this->id . "/index", ['id' => $id]));

                    } else {
                        $transaction->rollback();
                    }
                } else {
                    $transaction->rollback();
                }
            }
        }

        $this->render(
            "edit",
            array(
                "research" => $modelResearch,
                "protocol" => $modelProtocol,
                "finish" => $modelProlocolFinish,
            )
        );
    }

    public function actionOpenedList($id)
    {
        $research = Research::model()->getOpenedByPatient($id);
        $protocols = $research->getOpenedProtocols();
        $this->render("openedList", array("protocols" => $protocols));
    }

    public function actionPrint($id, $protocolId)
    {
        $this->layout = '//layouts/print';

        /** @var Protocol $modelProtocol */
        $modelProtocol = Protocol::model()->with(array("research"))->findByPk($protocolId);
        $modelResearch = $modelProtocol->research;
        $modelProlocolFinish = $modelProtocol->protocolFinish;


        $this->renderPdf(
            "edit",
            array(
                "research" => $modelResearch,
                "protocol" => $modelProtocol,
                "finish" => $modelProlocolFinish,
            )
        );
    }
}