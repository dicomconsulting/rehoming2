<?php

class m131127_134505_alter_annual_remove_complications extends I18nDbMigration
{
    public function safeUp()
    {
        $this->dropColumn(
            "{{protocol_annual}}",
            "cl_disease_complications"
        );
    }

    public function safeDown()
    {
        $this->addColumn(
            "{{protocol_annual}}",
            "cl_disease_complications",
            "text COMMENT 'Осложнения основного заболевания, которые возникли за год (перечислить и описать)'"
        );
    }
}
