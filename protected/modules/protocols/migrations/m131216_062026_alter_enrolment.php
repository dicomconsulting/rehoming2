<?php

class m131216_062026_alter_enrolment extends I18nDbMigration
{
    public function safeUp()
    {
        $this->renameColumn("{{protocol_enrolment}}", "impl_crt", "impl_crt_p");
        $this->addColumn(
            "{{protocol_enrolment}}",
            "impl_crt_d",
            "TINYINT(1) NULL  COMMENT 'CRT-D' AFTER `impl_crt_p`"
        );
    }

    public function safeDown()
    {
        $this->dropColumn("{{protocol_enrolment}}", "impl_crt_d");
        $this->renameColumn("{{protocol_enrolment}}", "impl_crt_p", "impl_crt");
    }
}
