<?php

class m131106_113156_alter_enrolment extends I18nDbMigration
{
    public function safeUp()
    {

        $this->addColumn(
            "{{protocol_enrolment}}",
            "impl_model_idc",
            "varchar(255) NULL COMMENT 'Модель ИКД' AFTER impl_model_sn"
        );

        $this->addColumn(
            "{{protocol_enrolment}}",
            "impl_model_sn_idc",
            "varchar(255) NULL COMMENT 'Серийный номер ИКД' AFTER impl_model_idc"
        );

    }

    public function safeDown()
    {
        $this->dropColumn("{{protocol_enrolment}}", "impl_model_sn_idc");
        $this->dropColumn("{{protocol_enrolment}}", "impl_model_idc");
    }
}
