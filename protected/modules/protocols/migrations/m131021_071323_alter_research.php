<?php

class m131021_071323_alter_research extends I18nDbMigration
{
    public function safeUp()
    {

        /**
         * фикс типа поля, по ошибке был int, а не date
         */

        $this->execute("UPDATE {{research}} SET date_end = NULL;");
        $this->execute(
            "ALTER TABLE {{research}}
  CHANGE `date_end` `date_end` DATE NULL  COMMENT 'Дата окончания исследования';"
        );

    }

    public function safeDown()
    {
        $this->execute(
            "ALTER TABLE {{research}}
        CHANGE `date_end` `date_end` INT(11) NULL  COMMENT 'Дата окончания исследования';"
        );
    }
}
