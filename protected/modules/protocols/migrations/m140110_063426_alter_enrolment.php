<?php

class m140110_063426_alter_enrolment extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn(
            "{{protocol_enrolment}}",
            "impl_heart_failure",
            "TINYINT(1) NULL COMMENT 'Сердечная недостаточность' AFTER `impl_second_prevent`"
        );
    }

    public function safeDown()
    {
        $this->dropColumn("{{protocol_enrolment}}", "impl_heart_failure");
    }
}
