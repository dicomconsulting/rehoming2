<?php

class m131021_061903_finish extends I18nDbMigration
{
    public function safeUp()
    {

        $this->createTable(
            "{{protocol_finish}}",
            array(
                "protocol_uid" => 'INT(11) NOT NULL',
                "regular" => 'TINYINT(1) DEFAULT 0 COMMENT "Регулярное окончание исследования"',
                "cause" => "TINYINT(1) DEFAULT 0 COMMENT 'Причина окончания исследования'",
                "comment" => "TEXT COMMENT 'Примечание'",
                "last_survey_date" => "DATE COMMENT 'Дата последнего обследования'",
                "general_comment" => "TEXT COMMENT 'Примечание'",
                "PRIMARY KEY (`protocol_uid`)"
            ),
            "ENGINE=INNODB COMMENT='Данные протокола \"Завершение обследования\"'"
        );

        $this->addForeignKey(
            "fk_protocol_uid_protocol_finish",
            "{{protocol_finish}}",
            "protocol_uid",
            "{{protocol}}",
            "uid",
            "CASCADE",
            "CASCADE"
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_protocol_uid_protocol_finish", "{{protocol_finish}}");
        $this->dropTable("{{protocol_adverse}}");
    }
}
