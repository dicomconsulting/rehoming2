<?php

class m131115_111242_alter_enrolment_pharm extends I18nDbMigration
{
    public function safeUp()
    {
        $this->dropForeignKey("fk_protocol_uid_protocol_enrolment_pharm", "{{protocol_enrolment_pharm}}");
        $this->addForeignKey(
            "fk_protocol_uid_protocol_enrolment_pharm",
            "{{protocol_enrolment_pharm}}",
            "protocol_uid",
            "{{protocol_enrolment}}",
            "protocol_uid",
            "CASCADE",
            "CASCADE"
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_protocol_uid_protocol_enrolment_pharm", "{{protocol_enrolment_pharm}}");
        $this->addForeignKey(
            "fk_protocol_uid_protocol_enrolment_pharm",
            "{{protocol_enrolment_pharm}}",
            "protocol_uid",
            "{{protocol}}",
            "uid",
            "CASCADE",
            "CASCADE"
        );
    }
}
