<?php

class m131010_095256_enrolment extends I18nDbMigration
{
    public function safeUp()
    {

        $this->execute(
            "CREATE TABLE {{protocol_enrolment}} (
  `protocol_uid` int(11) NOT NULL,
  `patient_height` int(11) DEFAULT NULL COMMENT 'Рост',
  `patient_weight` decimal(10,2) DEFAULT NULL COMMENT 'Вес',
  `patient_nyha` enum('I','II','III','IV','na','none') DEFAULT NULL COMMENT 'NYHA класс CH, na - оценка не производилась, none - нет',
  `enrolment_pacer` tinyint(1) DEFAULT NULL COMMENT 'ЭКС',
  `enrolment_crt` tinyint(1) DEFAULT NULL COMMENT 'CRT',
  `enrolment_icd` tinyint(1) DEFAULT NULL COMMENT 'ИКД',
  `enrolment_tele` tinyint(1) DEFAULT NULL COMMENT 'Имплантирован с функцией мобильного телемониторинга',
  `enrolment_able` tinyint(1) DEFAULT NULL COMMENT 'Пациент желает и может выполнять требования протокола клинического исследования',
  `enrolment_agree` tinyint(1) DEFAULT NULL COMMENT 'Пациент дал письменное информированное огласие на участие в исследовании',
  `enrolment_gsm` tinyint(1) DEFAULT NULL COMMENT 'Наличие сети мобильной связи GSM в месте проживания пациента',
  `visit_date` date DEFAULT NULL COMMENT 'Дата предварительного визита:',
  `diagnosis` text COMMENT 'Клинический диагноз',
  `diagnosis_comment` text COMMENT 'Примечание к диагнозу',
  `ca_block` tinyint(1) DEFAULT NULL COMMENT 'СА блокада',
  `ca_block_grade` int(11) DEFAULT NULL COMMENT 'Степень',
  `ca_pause_duration` decimal(10,2) DEFAULT NULL COMMENT 'Документированная максимальная пауза',
  `atrial_arr` int(11) DEFAULT NULL COMMENT 'Предсердная экстрасистолия (за сутки)',
  `atrial_tachy` int(11) DEFAULT NULL COMMENT 'Предсердная тахикардия (за сутки)',
  `atrial_fibrillation` tinyint(1) DEFAULT NULL COMMENT 'Фибрилляция предсердий',
  `atrial_flutter` tinyint(1) DEFAULT NULL COMMENT 'Трепетание предсердий',
  `tachy_history_duration` int(11) DEFAULT NULL COMMENT 'Длительность анамнеза тахикардии (месяцев)',
  `tachy_latest_date` date DEFAULT NULL COMMENT 'Дата последнего эпизода',
  `tachy_longest_episode` int(11) DEFAULT NULL COMMENT 'Продолжительность эпизодов (минут) максимум',
  `tachy_shortest_episode` int(11) DEFAULT NULL COMMENT 'Продолжительность эпизодов (минут) минимум',
  `tachy_frequency` int(11) DEFAULT NULL COMMENT 'Частота возникновения эпизодов за последние 3 месяца',
  `arr_docked` enum('self','medication','eit') DEFAULT NULL COMMENT 'Аритмия купируется',
  `ab_block` tinyint(1) DEFAULT NULL COMMENT 'АВ блокада',
  `ab_block_grade` int(11) DEFAULT NULL COMMENT 'Степень',
  `ab_pause_duration` decimal(10,2) DEFAULT NULL COMMENT 'Документированная максимальная пауза',
  `left_branch_block` tinyint(1) DEFAULT NULL COMMENT 'Блокада левой ножки пучка Гиса',
  `right_branch_block` tinyint(1) DEFAULT NULL COMMENT 'Блокада правой ножки пучка Гиса',
  `both_branch_block` tinyint(1) DEFAULT NULL COMMENT 'Двухпучковая блокада',
  `qrs_duration` int(11) DEFAULT NULL COMMENT 'Длительность комплекса QRS (мс)',
  `vent_arr` tinyint(1) DEFAULT NULL COMMENT 'Желудочковая экстрасистолия',
  `vent_fibrillation` tinyint(1) DEFAULT NULL COMMENT 'Фибрилляция желудочков',
  `vent_tachy` tinyint(1) DEFAULT NULL COMMENT 'Желудочковая тахикардия',
  `vent_tachy_type` tinyint(1) DEFAULT NULL COMMENT 'Тип желудочковой тахикардии',
  `consciousness` enum('presyncope','syncope','app_death') DEFAULT NULL COMMENT 'Сознание при приступе',
  `syncope_unknown_origin` tinyint(1) DEFAULT NULL COMMENT 'Синкопы неизвестного генеза',
  `surg_arr_rfa` tinyint(1) DEFAULT NULL COMMENT 'РЧА',
  `surg_arr` tinyint(1) DEFAULT NULL COMMENT 'Операция',
  `hispital_days` int(11) DEFAULT NULL COMMENT 'Количество койко-дней, проведенных в стационаре за год',
  `sick_days` int(11) DEFAULT NULL COMMENT 'Количество дней нетрудоспособности (на больничном) за год',
  `ambulance_calls` int(11) DEFAULT NULL COMMENT 'Количество обращений к службе скорой помощи за год',
  `disease_complications` text COMMENT 'Осложнения основного заболевания, которые возникли за год',
  `impl_sssu` tinyint(1) DEFAULT NULL COMMENT 'СССУ, включая тахи-бради синдром',
  `impl_av_block_perm` tinyint(1) DEFAULT NULL COMMENT 'Постоянная АВ-блокада высокой степени',
  `impl_av_block_trans` tinyint(1) DEFAULT NULL COMMENT 'Преходящая АВ-блокада высокой степени',
  `impl_brady` tinyint(1) DEFAULT NULL COMMENT 'Брадикардия с постоянной формой ФП',
  `impl_binodal_av_perm` tinyint(1) DEFAULT NULL COMMENT 'Бинодальное заболевание + постоянная АВ-блокада',
  `impl_binodal_av_trans` tinyint(1) DEFAULT NULL COMMENT 'Бинодальное заболевание + преходящая АВ-блокада',
  `impl_vaso_syncope` tinyint(1) DEFAULT NULL COMMENT 'Вазовагальные синкопы',
  `impl_other_ind` text COMMENT 'Другие',
  `impl_prime_prevent` tinyint(1) DEFAULT NULL COMMENT 'Первичная профилактика',
  `impl_second_prevent` tinyint(1) DEFAULT NULL COMMENT 'Вторичная профилактика',
  `impl_pacer` tinyint(1) DEFAULT NULL COMMENT 'ЭКС',
  `impl_crt` tinyint(1) DEFAULT NULL COMMENT 'CRT',
  `impl_idc` tinyint(1) DEFAULT NULL COMMENT 'ИКД',
  `impl_model` varchar(255) DEFAULT NULL COMMENT 'Модель',
  `impl_model_sn` varchar(255) DEFAULT NULL COMMENT 'Серийный номер',
  `ae` tinyint(1) DEFAULT NULL COMMENT 'Предсердный электрод',
  `ae_model` text COMMENT 'Модель',
  `ae_sn` text COMMENT 'Серийный номер',
  `ae_conf` tinyint(1) DEFAULT NULL COMMENT 'Конфигурация',
  `ae_pacing_threshold` decimal(10,2) DEFAULT NULL COMMENT 'Порог стимуляции(В) / длительность импульса (мс)',
  `ae_pulse_ampl` decimal(10,2) DEFAULT NULL COMMENT 'Амплитуда волны P (мВ)',
  `ae_pacing_imp` int(11) DEFAULT NULL COMMENT 'Импеданс (Ом)',
  `ve_model` varchar(255) DEFAULT NULL COMMENT 'Желудочковый электрод (модель)',
  `ve_sn` varchar(100) DEFAULT NULL COMMENT 'Серийный номер',
  `ve_conf` tinyint(1) DEFAULT NULL COMMENT 'Конфигурация',
  `ve_stimul_threshold` decimal(10,2) DEFAULT NULL COMMENT 'Порог стимуляции(В)',
  `ve_stimul_dur` decimal(10,2) DEFAULT NULL COMMENT 'Длительность стимула (мс)',
  `ve_wave_apl` decimal(10,2) DEFAULT NULL COMMENT 'Амплитуда волны R (мВ)',
  `ve_imp` int(11) DEFAULT NULL COMMENT 'Импеданс (Ом)',
  `sve` tinyint(1) DEFAULT NULL COMMENT 'Шоковый желудочковый электрод',
  `sve_imp` int(11) DEFAULT NULL COMMENT 'Шоковый импеданс (Ом)',
  `sve_success` tinyint(1) DEFAULT NULL COMMENT 'Имплантация успешна',
  `pacer_model` text COMMENT 'Электрод КС (модель)',
  `pacer_sn` varchar(100) DEFAULT NULL COMMENT 'Серийный номер',
  `pacer_conf` tinyint(1) DEFAULT NULL COMMENT 'Конфигурация',
  `pacer_stimul_threshold` decimal(10,2) DEFAULT NULL COMMENT 'Порог стимуляции(В)',
  `pacer_stimul_dur` decimal(10,2) DEFAULT NULL COMMENT 'Длительность стимула (мс)',
  `pacer_ampl` decimal(10,2) DEFAULT NULL COMMENT 'Амплитуда волны R (мВ)',
  `pacer_imp` int(11) DEFAULT NULL COMMENT 'Импеданс (Ом)',
  `pacer_placement` text COMMENT 'Положение',
  `trans_uid` int(11) DEFAULT NULL COMMENT 'Трансмиттер (модель)',
  `trans_sn` varchar(100) DEFAULT NULL COMMENT 'Серийный номер',
  `trans_reg_date` date DEFAULT NULL COMMENT 'Дата регистрации пациента и трансмиттера на сайте сервисного центра',
  `trans_guide_granted` tinyint(1) DEFAULT NULL COMMENT 'Пациенту выдана инструкция по использованию трансмиттера',
  PRIMARY KEY (`protocol_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        );

        $this->addForeignKey(
            'fk_protocol_uid_protocol_enrolment',
            '{{protocol_enrolment}}',
            'protocol_uid',
            '{{protocol}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );


    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_protocol_uid_protocol_enrolment", "{{protocol_enrolment}}");
        $this->dropTable("{{protocol_enrolment}}");
    }
}
