<?php

class m131101_125527_alter_enrolment extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn("{{protocol_enrolment}}", "ca_pause", "TINYINT(1) DEFAULT 0 COMMENT 'Документированная максимальная пауза' AFTER `ca_block_grade`");
        $this->addColumn("{{protocol_enrolment}}", "ab_pause", "TINYINT(1) DEFAULT 0 COMMENT 'Документированная максимальная пауза' AFTER `ab_block_grade`");
    }

    public function safeDown()
    {
        $this->dropColumn("{{protocol_enrolment}}", "ca_pause");
        $this->dropColumn("{{protocol_enrolment}}", "ab_pause");
    }
}
