<?php

class m131217_102927_alter_enrolment extends I18nDbMigration
{
    public function safeUp()
    {
        $this->execute("UPDATE {{protocol_enrolment}} SET consciousness = NULL;");

        $this->alterColumn(
            "{{protocol_enrolment}}",
            "consciousness",
            "INT(11) DEFAULT NULL COMMENT 'Сознание при приступе'"
        );
    }

    public function safeDown()
    {
        $this->execute("UPDATE {{protocol_enrolment}} SET consciousness = NULL;");

        $this->alterColumn(
            "{{protocol_enrolment}}",
            "consciousness",
            "enum('presyncope','syncope','app_death') DEFAULT NULL COMMENT 'Сознание при приступе'"
        );
    }
}
