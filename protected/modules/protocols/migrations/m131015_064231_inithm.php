<?php

class m131015_064231_inithm extends I18nDbMigration
{
    public function safeUp()
    {

        $this->createTable(
            '{{protocol_inithm}}',
            array(
                'protocol_uid' => "INT(11) NOT NULL COMMENT 'Ссылка на протокол'",
                'patient_regged' => "TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'Пациент зарегистрирован в HomeMonitoring до включения'",
                'hm_options_changed' => "TINYINT(1) DEFAULT 0 COMMENT 'Конфигурация событий Home monitoring (Options)'",
                'to_red' => "TINYINT(1) DEFAULT 0 COMMENT 'Статус событий Home Monitoring изменен на red (красный)'",
                'to_red_date' => "DATE COMMENT 'Дата изменения'",
                'to_red_descr' => "TEXT COMMENT 'Примечание'",
                'from_red' => "TINYINT(1) DEFAULT 0 COMMENT 'Статус событий Home Monitoring изменен с red (красный) на другой'",
                'from_red_date' => "DATE COMMENT 'Дата изменения'",
                'from_red_status' => "TINYINT(1) DEFAULT 0 COMMENT 'На какой цвет'",
                'from_red_descr' => "TEXT COMMENT 'Примечание'",
                'comment' => "TEXT COMMENT 'Комментарий/Примечание'",
                'PRIMARY KEY (`protocol_uid`)'
            ),
            "ENGINE=INNODB COMMENT='Таблица данных протокола \"Инициализация HomeMonitoring\"'"
        );

        $this->addForeignKey(
            "fk_protocol_uid_inithm",
            "{{protocol_inithm}}",
            "protocol_uid",
            "{{protocol}}",
            "uid",
            "CASCADE",
            "CASCADE"
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_protocol_uid_inithm", "{{protocol_inithm}}");
        $this->dropTable('{{protocol_inithm}}');
    }
}
