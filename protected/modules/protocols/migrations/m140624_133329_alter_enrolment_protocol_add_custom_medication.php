<?php

class m140624_133329_alter_enrolment_protocol_add_custom_medication extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn(
            '{{protocol_enrolment}}',
            'pharm_custom',
            'text COMMENT "Другие принимаемые препараты" AFTER syncope_unknown_origin'
        );
    }

    public function safeDown()
    {
        $this->dropColumn(
            '{{protocol_enrolment}}',
            'pharm_custom'
        );
    }
}
