<?php

class m140203_142059_alter_annual extends I18nDbMigration
{
    public function safeUp()
    {

        $this->alterColumn(
            "{{protocol_annual}}",
            "req_atr_stimul_thresh",
            "DECIMAL(10,2) NULL  COMMENT 'Порог стимуляции (В)'"
        );

        $this->alterColumn(
            "{{protocol_annual}}",
            "req_ven_stimul_thresh",
            "DECIMAL(10,2) NULL  COMMENT 'Порог стимуляции (В)'"
        );

        $this->alterColumn(
            "{{protocol_annual}}",
            "atest_atr_stimul_thresh",
            "DECIMAL(10,2) NULL  COMMENT 'Порог стимуляции (В)'"
        );

        $this->alterColumn(
            "{{protocol_annual}}",
            "atest_ven_stimul_thresh",
            "DECIMAL(10,2) NULL  COMMENT 'Порог стимуляции (В)'"
        );
        $this->alterColumn(
            "{{protocol_annual}}",
            "test_atr_stimul_thresh",
            "DECIMAL(10,2) NULL  COMMENT 'Порог стимуляции (В)'"
        );

        $this->alterColumn(
            "{{protocol_annual}}",
            "test_ven_stimul_thresh",
            "DECIMAL(10,2) NULL  COMMENT 'Порог стимуляции (В)'"
        );

        $this->alterColumn(
            "{{protocol_annual}}",
            "pacer_stimul_thresh",
            "DECIMAL(10,2) NULL  COMMENT 'Порог стимуляции (В)'"
        );
    }

    public function safeDown()
    {
        $this->alterColumn(
            "{{protocol_annual}}",
            "req_atr_stimul_thresh",
            " int(11) DEFAULT NULL  COMMENT 'Порог стимуляции (В)'"
        );

        $this->alterColumn(
            "{{protocol_annual}}",
            "req_ven_stimul_thresh",
            " int(11) DEFAULT NULL  COMMENT 'Порог стимуляции (В)'"
        );

        $this->alterColumn(
            "{{protocol_annual}}",
            "atest_atr_stimul_thresh",
            " int(11) DEFAULT NULL  COMMENT 'Порог стимуляции (В)'"
        );

        $this->alterColumn(
            "{{protocol_annual}}",
            "atest_ven_stimul_thresh",
            " int(11) DEFAULT NULL  COMMENT 'Порог стимуляции (В)'"
        );

        $this->alterColumn(
            "{{protocol_annual}}",
            "test_atr_stimul_thresh",
            " int(11) DEFAULT NULL  COMMENT 'Порог стимуляции (В)'"
        );

        $this->alterColumn(
            "{{protocol_annual}}",
            "test_ven_stimul_thresh",
            " int(11) DEFAULT NULL  COMMENT 'Порог стимуляции (В)'"
        );

        $this->alterColumn(
            "{{protocol_annual}}",
            "pacer_stimul_thresh",
            " int(11) DEFAULT NULL  COMMENT 'Порог стимуляции (В)'"
        );

    }
}
