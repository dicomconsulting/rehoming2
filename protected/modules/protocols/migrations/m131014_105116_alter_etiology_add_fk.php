<?php

class m131014_105116_alter_etiology_add_fk extends I18nDbMigration
{
    public function safeUp()
    {

        $this->addForeignKey(
            'fk_disease_uid_protocol_enrolment_etiology',
            '{{protocol_enrolment_etiology}}',
            'disease_uid',
            '{{icd10}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_disease_uid_protocol_enrolment_etiology',
            '{{protocol_enrolment_etiology}}'
        );
    }
}
