<?php

class m140130_132347_alter_enrolment extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn(
            "{{protocol_enrolment}}",
            "enrolment_age_over_18",
            "TINYINT(1) NULL  COMMENT 'Пациент старше 18 лет' AFTER `enrolment_icd`"
        );

        $this->addColumn(
            "{{protocol_enrolment}}",
            "enrolment_patient_accessible",
            "TINYINT(1) NULL  COMMENT 'Пациент не предполагает смену места жительства в течение
периода участия в исследовании' AFTER `enrolment_gsm`"
        );

        $this->addColumn(
            "{{protocol_enrolment}}",
            "exclude_surgery",
            "TINYINT(1) NULL  COMMENT 'Операция на сердце в течение последнего месяца' " .
            "AFTER `enrolment_patient_accessible`"
        );

        $this->addColumn(
            "{{protocol_enrolment}}",
            "exclude_heartattack",
            "TINYINT(1) NULL  COMMENT 'Инфаркт миокарда в течение последнего месяца' " .
            "AFTER `exclude_surgery`"
        );

        $this->addColumn(
            "{{protocol_enrolment}}",
            "exclude_shocks",
            "TINYINT(1) NULL  COMMENT 'Пациент с ИКД получил более двух шоковых разрядов за последние 6 месяцев' " .
            "AFTER `exclude_heartattack`"
        );

        $this->addColumn(
            "{{protocol_enrolment}}",
            "excl_crit_hardware",
            "TINYINT(1) NULL  COMMENT 'Дислокация, перелом, нарушение изоляции электродов...' " .
            "AFTER `exclude_shocks`"
        );

        $this->addColumn(
            "{{protocol_enrolment}}",
            "excl_crit_infection",
            "TINYINT(1) NULL  COMMENT 'Инфекция, связанная с системой стимуляции' " .
            "AFTER `excl_crit_hardware`"
        );

        $this->addColumn(
            "{{protocol_enrolment}}",
            "excl_crit_unable",
            "TINYINT(1) NULL COMMENT 'Пациент не в состоянии управляться с системой Home Monitoring' " .
            "AFTER `excl_crit_infection`"
        );

        $this->addColumn(
            "{{protocol_enrolment}}",
            "excl_crit_busy",
            "TINYINT(1) NULL COMMENT 'Пациент участвует в другом клиническом исследовании' " .
            "AFTER `excl_crit_unable`"
        );

        $this->addColumn(
            "{{protocol_enrolment}}",
            "excl_crit_nomobile",
            "TINYINT(1) NULL COMMENT 'Отсутствие сети мобильной телефонной связи GSM в районе проживания пациента' " .
            "AFTER `excl_crit_busy`"
        );

        $this->addColumn(
            "{{protocol_enrolment}}",
            "excl_crit_pregnancy",
            "TINYINT(1) NULL COMMENT 'Беременная или кормящая женщина' " .
            "AFTER `excl_crit_busy`"
        );

        $this->addColumn(
            "{{protocol_enrolment}}",
            "atrial_permanent",
            "TINYINT(1) NULL COMMENT 'Постоянная форма' " .
            "AFTER `atrial_flutter`"
        );

        $this->addColumn(
            "{{protocol_enrolment}}",
            "form_hm_signed",
            "TINYINT(1) NULL COMMENT 'Форма согласия на использование BIOTRONIK Home Monitoring' " .
            "AFTER `trans_guide_granted`"
        );

        $this->addColumn(
            "{{protocol_enrolment}}",
            "form_rh_signed",
            "TINYINT(1) NULL COMMENT 'Форма информированного согласия пациента на
             участие в исследовании ReHoming подписана пациентом' " .
            "AFTER `form_hm_signed`"
        );
    }

    public function safeDown()
    {
        $this->dropColumn("{{protocol_enrolment}}", "form_rh_signed");
        $this->dropColumn("{{protocol_enrolment}}", "form_hm_signed");
        $this->dropColumn("{{protocol_enrolment}}", "atrial_permanent");
        $this->dropColumn("{{protocol_enrolment}}", "excl_crit_pregnancy");
        $this->dropColumn("{{protocol_enrolment}}", "excl_crit_nomobile");
        $this->dropColumn("{{protocol_enrolment}}", "excl_crit_busy");
        $this->dropColumn("{{protocol_enrolment}}", "excl_crit_unable");
        $this->dropColumn("{{protocol_enrolment}}", "excl_crit_infection");
        $this->dropColumn("{{protocol_enrolment}}", "excl_crit_hardware");
        $this->dropColumn("{{protocol_enrolment}}", "exclude_shocks");
        $this->dropColumn("{{protocol_enrolment}}", "exclude_heartattack");
        $this->dropColumn("{{protocol_enrolment}}", "exclude_surgery");
        $this->dropColumn("{{protocol_enrolment}}", "enrolment_patient_accessible");
        $this->dropColumn("{{protocol_enrolment}}", "enrolment_age_over_18");
    }
}
