<?php

class m131228_121441_add_protocol_history_table extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{protocol_history}}',
            array(
                'uid' => 'pk',
                'research_id' => 'INTEGER NOT NULL',
                'protocol_id' => 'INTEGER NOT NULL',
                'protocol_dictionary_id' => 'INTEGER NOT NULL',
                'operation_id' => 'TINYINT(1) NOT NULL',
                'event_time' => 'DATETIME NOT NULL',
                'user_name' => 'INTEGER NOT NULL',
                'archive' => 'TINYINT(1) NOT NULL DEFAULT 0'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );

        $this->addForeignKey(
            'fk_protocol_history_research',
            '{{protocol_history}}',
            'research_id',
            '{{research}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_protocol_history_research', '{{protocol_history}}');
        $this->dropTable('{{protocol_history}}');
    }
}
