<?php

class m131011_071812_enrolment_surgery extends I18nDbMigration
{
    public function safeUp()
    {

        $this->createTable(
            '{{protocol_enrolment_surgery}}',
            array(
                'protocol_uid' => "int(11) NOT NULL COMMENT 'Ид протокола'",
                'surgery_uid' => "int(11) NOT NULL COMMENT 'ид типа операции'",
                'date' => "DATE NOT NULL  COMMENT 'дата операции'",
                'descr' => "TEXT NULL COMMENT 'Характер операции'",
                'PRIMARY KEY (`protocol_uid`,`surgery_uid`,`date`)',
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );

        $this->createIndex(
            "idx_protocol_surgery_unique",
            '{{protocol_enrolment_surgery}}',
            "protocol_uid,surgery_uid",
            true
        );

        $this->addForeignKey(
            'fk_protocol_uid_protocol_enrolment_surgery',
            '{{protocol_enrolment_surgery}}',
            'protocol_uid',
            '{{protocol}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_surgery_uid_protocol_enrolment_surgery',
            '{{protocol_enrolment_surgery}}',
            'surgery_uid',
            '{{surgery}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );


    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_protocol_uid_protocol_enrolment_surgery", "{{protocol_enrolment_surgery}}");
        $this->dropForeignKey("fk_surgery_uid_protocol_enrolment_surgery", "{{protocol_enrolment_surgery}}");
        $this->dropTable("{{protocol_enrolment_surgery}}");
    }
}
