<?php

class m131113_134338_alter_surgery_rel extends I18nDbMigration
{
    public function safeUp()
    {
        $this->dropForeignKey("fk_protocol_uid_protocol_enrolment_surgery", "{{protocol_enrolment_surgery}}");
        $this->addForeignKey("fk_protocol_uid_protocol_enrolment_surgery", "{{protocol_enrolment_surgery}}", "protocol_uid", "{{protocol_enrolment}}", "protocol_uid", "CASCADE", "CASCADE");
    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_protocol_uid_protocol_enrolment_surgery", "{{protocol_enrolment_surgery}}");
        $this->addForeignKey("fk_protocol_uid_protocol_enrolment_surgery", "{{protocol_enrolment_surgery}}", "protocol_uid", "{{protocol}}", "uid", "CASCADE", "CASCADE");
    }
}
