<?php

class m140110_072407_alter_regular extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn(
            "{{protocol_regular}}",
            "visit_deflection",
            "INT(11) DEFAULT NULL COMMENT 'Отклонение даты от расписания' AFTER `survey_period`"
        );

        $this->addColumn(
            "{{protocol_regular}}",
            "visit_missed",
            "INT(11) DEFAULT NULL COMMENT 'Пропущен визит' AFTER `visit_deflection`"
        );
    }

    public function safeDown()
    {
        $this->dropColumn("{{protocol_regular}}", "visit_deflection");
        $this->dropColumn("{{protocol_regular}}", "visit_missed");
    }
}
