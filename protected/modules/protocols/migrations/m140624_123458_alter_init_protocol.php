<?php

class m140624_123458_alter_init_protocol extends I18nDbMigration
{
    public function safeUp()
    {
        $this->execute("UPDATE {{protocol_inithm}} SET hm_options_changed = (CASE WHEN hm_options_changed = '0' THEN 'Default' WHEN hm_options_changed = '1' THEN 'минимум установок' END) WHERE hm_options_changed IN ('0', '1')");
    }

    public function safeDown()
    {
        return true;
    }
}
