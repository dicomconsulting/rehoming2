<?php

class m131009_065855_protocol extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{protocol}}',
            array(
                'uid' => 'pk',
                'date' => "DATE NOT NULL COMMENT 'Дата создания протокола'",
                'protocol_dictionary_uid' => "INT(11) NOT NULL COMMENT 'Ссылка на тип протокола'",
                'research_uid' => "INT(11) NOT NULL COMMENT 'Ссылка на исследование'",
                'user_id' => "INT(11) NOT NULL COMMENT 'Ссылка на врача, который завел протокол'",
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );

        $this->addForeignKey(
            'fk_user_id_protocol',
            '{{protocol}}',
            'user_id',
            '{{user}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_protocol_dictionary_uid_protocol',
            '{{protocol}}',
            'protocol_dictionary_uid',
            '{{protocol_dictionary}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {

        $this->dropForeignKey("fk_user_id_protocol", "{{protocol}}");
        $this->dropForeignKey("fk_protocol_dictionary_uid_protocol", "{{protocol}}");

        $this->dropTable('{{protocol}}');
    }
}
