<?php

class m131009_065503_protocol_dictionary extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTableWithI18n(
            '{{protocol_dictionary}}',
            array(
                'uid' => 'pk',
                'name' => 'VARCHAR(100) NOT NULL',
                'alias' => "VARCHAR(100) NOT NULL COMMENT 'Псевдоним названия протокола на английском, для ссылок на дочерние таблицы'",
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci',
            array("name")
        );

    }

    public function safeDown()
    {
        $this->dropTableWithI18n('{{protocol_dictionary}}');
    }
}
