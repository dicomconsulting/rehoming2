<?php

class m131127_120233_alter_enrolment_change_placement extends I18nDbMigration
{
    public function safeUp()
    {
        $this->execute("UPDATE {{protocol_enrolment}} SET pacer_placement = NULL");

        $this->alterColumn("{{protocol_enrolment}}", "pacer_placement", " INT(11) NULL  COMMENT 'Положение'");
    }

    public function safeDown()
    {

    }
}
