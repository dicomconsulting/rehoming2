<?php

class m131217_100336_alter_enrolment extends I18nDbMigration
{
    public function safeUp()
    {
        $this->renameColumn(
            "{{protocol_enrolment}}",
            "arr_docked",
            "arr_cupped"
        );

        $this->execute("UPDATE {{protocol_enrolment}} SET arr_cupped = NULL;");

        $this->alterColumn(
            "{{protocol_enrolment}}",
            "arr_cupped",
            "INT(11) DEFAULT NULL COMMENT 'Аритмия купируется'"
        );
    }

    public function safeDown()
    {
        $this->renameColumn(
            "{{protocol_enrolment}}",
            "arr_cupped",
            "arr_docked"
        );

        $this->execute("UPDATE {{protocol_enrolment}} SET arr_docked = NULL;");

        $this->alterColumn(
            "{{protocol_enrolment}}",
            "arr_docked",
            "enum('self','medication','eit') DEFAULT NULL COMMENT 'Аритмия купируется'"
        );
    }
}
