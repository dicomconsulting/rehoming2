<?php

class m131118_134808_alter_research extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn(
            "{{research}}",
            "patient_code",
            "CHAR(10) NULL  COMMENT 'Код пациента' AFTER `screening_number`"
        );
    }

    public function safeDown()
    {
        $this->dropColumn(
            "{{research}}",
            "patient_code"
        );
    }
}
