<?php

class m140131_094100_alter_regular extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn(
            "{{protocol_regular}}",
            "comment",
            "text DEFAULT NULL COMMENT 'Комментарий / Примечание '"
        );
    }

    public function safeDown()
    {
        $this->dropColumn(
            "{{protocol_regular}}",
            "comment"
        );
    }
}
