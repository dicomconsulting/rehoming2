<?php

class m131203_090653_alter_enrolment extends I18nDbMigration
{
    public function safeUp()
    {
        $this->renameColumn("{{protocol_enrolment}}", "enrolment_crt", "enrolment_crt_p");
        $this->addColumn(
            "{{protocol_enrolment}}",
            "enrolment_crt_d",
            "TINYINT(1) NULL  COMMENT 'CRT-D' AFTER `enrolment_crt_p`"
        );
    }

    public function safeDown()
    {
        $this->dropColumn("{{protocol_enrolment}}", "enrolment_crt_d");
        $this->renameColumn("{{protocol_enrolment}}", "enrolment_crt_p", "enrolment_crt");
    }
}
