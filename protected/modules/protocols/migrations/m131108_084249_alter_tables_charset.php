<?php

class m131108_084249_alter_tables_charset extends I18nDbMigration
{
    public function safeUp()
    {
        $this->execute("ALTER TABLE {{protocol}} CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;");
        $this->execute("ALTER TABLE {{protocol_annual}} CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;");
        $this->execute("ALTER TABLE {{protocol_adverse}} CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;");
        $this->execute("ALTER TABLE {{protocol_dictionary}} CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;");
        $this->execute("ALTER TABLE {{protocol_dictionary_i18n}} CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;");
        $this->execute("ALTER TABLE {{protocol_enrolment}} CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;");
        $this->execute("ALTER TABLE {{protocol_enrolment_etiology}} CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;");
        $this->execute("ALTER TABLE {{protocol_enrolment_pharm}} CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;");
        $this->execute("ALTER TABLE {{protocol_enrolment_surgery}} CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;");
        $this->execute("ALTER TABLE {{protocol_evolution}} CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;");
        $this->execute("ALTER TABLE {{protocol_finding_relation}} CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;");
        $this->execute("ALTER TABLE {{protocol_finish}} CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;");
        $this->execute("ALTER TABLE {{protocol_inithm}} CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;");
        $this->execute("ALTER TABLE {{protocol_regular}} CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;");
    }

    public function safeDown()
    {
        echo "m131108_084249_alter_tables_charset does not support migration down.\\n";

        return false;
    }
}
