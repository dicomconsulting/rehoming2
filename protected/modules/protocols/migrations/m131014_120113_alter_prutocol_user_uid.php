<?php

class m131014_120113_alter_prutocol_user_uid extends I18nDbMigration
{
    public function safeUp()
    {
        $this->dropForeignKey('fk_user_id_protocol', '{{protocol}}');
        $this->execute("ALTER TABLE {{protocol}}  CHANGE `user_id` `user_uid` INT(11) NOT NULL  COMMENT 'Ссылка на врача, который завел протокол';");
        $this->addForeignKey(
            'fk_user_id_protocol',
            '{{protocol}}',
            'user_uid',
            '{{user}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_user_id_protocol', '{{protocol}}');
        $this->execute("ALTER TABLE {{protocol}}  CHANGE `user_uid` `user_id` INT(11) NOT NULL  COMMENT 'Ссылка на врача, который завел протокол';");
        $this->addForeignKey(
            'fk_user_id_protocol',
            '{{protocol}}',
            'user_id',
            '{{user}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }
}
