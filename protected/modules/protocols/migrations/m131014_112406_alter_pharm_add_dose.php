<?php

class m131014_112406_alter_pharm_add_dose extends I18nDbMigration
{
    public function safeUp()
    {

        $this->addColumn(
            "{{protocol_enrolment_pharm}}",
            "dose",
            "decimal(10,2) NULL  COMMENT 'Доза'"
        );

        $this->addColumn(
            "{{protocol_enrolment_pharm}}",
            "measure",
            "varchar(100) NULL  COMMENT 'Ед.измерения'"
        );

    }

    public function safeDown()
    {
        $this->dropColumn("{{protocol_enrolment_pharm}}", "measure");
        $this->dropColumn("{{protocol_enrolment_pharm}}", "dose");
    }
}
