<?php

class m131015_100545_evolution extends I18nDbMigration
{
    public function safeUp()
    {

        $this->createTable(
            "{{protocol_evolution}}",
            array(
                "protocol_uid" => "INT(11) NOT NULL COMMENT 'Ссылка на описание протокола'",
                "option_changed" => "TINYINT(1) DEFAULT 0 COMMENT 'Опция Home Monitoring изменена со времени включения'",
                "option_changed_new_value" => "TEXT COMMENT 'Укажите новую опцию'",
                "option_changed_reason" => "TEXT COMMENT 'Причина изменения'",
                "red_finding" => "TINYINT(1) DEFAULT 0  COMMENT 'Статус сообщения Home Monitoring изменен на “red” (красный)'",
                "red_finding_date" => "DATE COMMENT 'Дата изменения'",
                "red_finding_message" => "TEXT COMMENT 'Сообщение'",
                "red_finding_changed" => "TINYINT(1) DEFAULT 0  COMMENT 'Статус сообщения Home Monitoring изменен с “red” (красный) на другой'",
                "red_finding_changed_date" => "DATE COMMENT 'Дата изменения'",
                "red_finding_changed_to" => "TINYINT(1) DEFAULT 0  COMMENT 'На какой цвет'",
                "red_finding_changed_message" => "TEXT COMMENT 'Сообщение'",
                "red_finding_changed_comment" => "TEXT COMMENT 'Примечание'",
                "PRIMARY KEY (`protocol_uid`)"
            ),
            "ENGINE=INNODB COMMENT='Протокол \"Оценка функций HM\"'"
        );

        $this->addForeignKey(
            "fk_protocol_uid_protocol_evolution",
            "{{protocol_evolution}}",
            "protocol_uid",
            "{{protocol}}",
            "uid",
            "CASCADE",
            "CASCADE"
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_protocol_uid_protocol_evolution", "{{protocol_evolution}}");
        $this->dropTable('{{protocol_evolution}}');
    }
}
