<?php

class m131120_074206_alter_enrolment_nyha extends I18nDbMigration
{
    public function safeUp()
    {
        $this->execute("UPDATE {{protocol_enrolment}} SET patient_nyha = NULL");

        $this->alterColumn("{{protocol_enrolment}}", "patient_nyha", " INT(11) NULL  COMMENT 'NYHA класс CH'");
    }

    public function safeDown()
    {

    }
}
