<?php

class m131010_095624_enrolment_pharm extends I18nDbMigration
{
    public function safeUp()
    {

        $this->createTable(
            '{{protocol_enrolment_pharm}}',
            array(
                'protocol_uid' => "int(11) NOT NULL COMMENT 'Ид протокола'",
                'pharm_product_uid' => "int(11) NOT NULL COMMENT 'ид лекарственного средства'",
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );

        $this->createIndex(
            "idx_protocol_pharm_unique",
            '{{protocol_enrolment_pharm}}',
            "protocol_uid,pharm_product_uid",
            true
        );

        $this->addForeignKey(
            'fk_protocol_uid_protocol_enrolment_pharm',
            '{{protocol_enrolment_pharm}}',
            'protocol_uid',
            '{{protocol}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );


    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_protocol_uid_protocol_enrolment_pharm", "{{protocol_enrolment_pharm}}");
        $this->dropTable("{{protocol_enrolment_pharm}}");
    }
}
