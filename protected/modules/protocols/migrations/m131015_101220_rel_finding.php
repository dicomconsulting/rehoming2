<?php

class m131015_101220_rel_finding extends I18nDbMigration
{
    public function safeUp()
    {

        $this->createTable(
            "{{protocol_finding_relation}}",
            array(
                "protocol_uid" => "INT(11) NOT NULL COMMENT 'Ссылка на описание протокола'",
                "finding_uid" => "INT(11) NOT NULL COMMENT 'Ссылка на описание нежелательное явление'",
                "action" => "TEXT COMMENT 'Предпринятые действия'",
                "action_date" => "DATE COMMENT 'Дата первого предпринятого действия'",
                "clinic_visit" => "TINYINT(1) DEFAULT 0 COMMENT 'Посещение клиники'",
                "clinic_distance" => "DECIMAL(10,2) COMMENT 'Расстояние (км)'",
                "clinic_time" => "DECIMAL(10,2) COMMENT 'Время в пути (часов)'",
                "PRIMARY KEY (`protocol_uid`, `finding_uid`)"
            ),
            "ENGINE=INNODB COMMENT='Связь протоколов и нежелательных явлений'"
        );

        $this->addForeignKey(
            "fk_protocol_uid_protocol_finding_relation",
            "{{protocol_finding_relation}}",
            "protocol_uid",
            "{{protocol}}",
            "uid",
            "CASCADE",
            "CASCADE"
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_protocol_uid_protocol_finding_relation", "{{protocol_finding_relation}}");
        $this->dropTable('{{protocol_finding_relation}}');
    }
}
