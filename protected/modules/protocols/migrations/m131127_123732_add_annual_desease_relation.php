<?php

class m131127_123732_add_annual_desease_relation extends I18nDbMigration
{
    public function safeUp()
    {

        $this->createTable(
            '{{protocol_annual_desease}}',
            array(
                'protocol_uid' => "int(11) NOT NULL COMMENT 'Ид протокола'",
                'icd10_uid' => "int(11) NOT NULL COMMENT 'ид заболевания'",
                'descr' => "TEXT NULL COMMENT 'подробности'",
                'PRIMARY KEY (`protocol_uid`,`icd10_uid`)',
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT="Осложнения заболевания"'
        );

        $this->createIndex(
            "idx_protocol_icd10_unique",
            '{{protocol_annual_desease}}',
            "protocol_uid,icd10_uid",
            true
        );

        $this->addForeignKey(
            'fk_protocol_uid_protocol_annual_desease',
            '{{protocol_annual_desease}}',
            'protocol_uid',
            '{{protocol}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_icd10_uid_protocol_annual_desease',
            '{{protocol_annual_desease}}',
            'icd10_uid',
            '{{icd10}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );


    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_protocol_uid_protocol_annual_desease", "{{protocol_annual_desease}}");
        $this->dropForeignKey("fk_icd10_uid_protocol_annual_desease", "{{protocol_annual_desease}}");
        $this->dropTable("{{protocol_annual_desease}}");
    }
}
