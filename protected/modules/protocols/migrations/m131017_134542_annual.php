<?php

class m131017_134542_annual extends I18nDbMigration
{
    public function safeUp()
    {

        $this->execute(
            "CREATE TABLE {{protocol_annual}}(
  `protocol_uid` INT(11) NOT NULL,
  `findings_cnt` INT(11) DEFAULT 0 COMMENT 'Число нежелательных явлений или нежелтальных явлений, связанных с прибором (с момента включения пациента в исследование)',
  `medical_therapy_changed` TINYINT(1) DEFAULT 0 COMMENT 'Изменения медикаментозной терапии с момента включения',
  `medical_therapy_comment` TEXT COMMENT 'Примечание (укажите какие)',
  `other_therapy_changed` TINYINT(1) DEFAULT 0 COMMENT 'Другие изменения терапии с момента включения',
  `other_therapy_comment` TEXT COMMENT 'Примечание',
  `estimated_eri_yesars` INT(11) COMMENT 'Ожидаемый ERI, лет',
  `estimated_eri_months` INT(11) COMMENT 'Ожидаемый ERI, месяцев',
  `atrial_arrhythmic_burden` INT(11) COMMENT 'Atrial arrhythmic burden (%)',
  `pacing_in_a` INT(11) COMMENT 'Pacing in A (%)',
  `pacing_in_b` INT(11) COMMENT 'Pacing in B (%)',
  `as_vs` INT(11) COMMENT 'AsVs (%)',
  `as_vp` INT(11) COMMENT 'AsVp (%)',
  `ap_vp` INT(11) COMMENT 'ApVp (%)',
  `ap_vs` INT(11) COMMENT 'ApVs (%)',
  `stimul_ddd_adi` TINYINT(1) DEFAULT 0 COMMENT 'подавление Vp / режим DDD(R) - ADI(R)',
  `stimul_irs_on` TINYINT(1) DEFAULT 0 COMMENT 'другой режим и AV hysteresis = IRSplus On',
  `stimul_irs_off` TINYINT(1) DEFAULT 0 COMMENT 'другой режим и AV hysteresis = IRSplus Off',
  `capt_ctrl_a` TINYINT(1) DEFAULT 0 COMMENT 'Capture control A, On - 1, Off - 2, ATM - 3',
  `capt_ctrl_v` TINYINT(1) DEFAULT 0 COMMENT 'Capture control V, On - 1, Off - 2, ATM - 3',
  `sens_a` TINYINT(1) DEFAULT 0 COMMENT 'Sensitivity A, 1 - Auto, 2 - другая установка',
  `sens_v` TINYINT(1) DEFAULT 0 COMMENT 'Sensitivity V, 1 - Auto, 2 - другая установка',
  `atr_conf` TINYINT(1) DEFAULT 0 COMMENT 'Конфигурация, предсердие',
  `ven_conf` TINYINT(1) DEFAULT 0 COMMENT 'Конфигурация, желудочек',
  `req_atr_ampl` DECIMAL(10,2) COMMENT 'Амплитуда волны P/R (мВ)',
  `req_ven_ampl` DECIMAL(10,2) COMMENT 'Амплитуда волны P/R (мВ)',
  `req_atr_stimul_thresh` INT(11) COMMENT 'Порог стимуляции (В)',
  `req_ven_stimul_thresh` INT(11) COMMENT 'Порог стимуляции (В)',
  `req_atr_el_imp` INT(11) COMMENT 'Импеданс электрода (Ом)',
  `req_ven_el_imp` INT(11) COMMENT 'Импеданс электрода (Ом)',
  `req_ven_shock_imp` INT(11) COMMENT 'Шоковый импеданс (Ом)',
  `atest_atr_ampl` DECIMAL(10,2) COMMENT 'Амплитуда волны P/R (мВ)',
  `atest_ven_ampl` DECIMAL(10,2) COMMENT 'Амплитуда волны P/R (мВ)',
  `atest_atr_stimul_thresh` INT(11) COMMENT 'Порог стимуляции (В)',
  `atest_ven_stimul_thresh` INT(11) COMMENT 'Порог стимуляции (В)',
  `atest_atr_el_imp` INT(11) COMMENT 'Импеданс электрода (Ом)',
  `atest_ven_el_imp` INT(11) COMMENT 'Импеданс электрода (Ом)',
  `test_atr_ampl` DECIMAL(10,2) COMMENT 'Амплитуда волны P/R (мВ)',
  `test_ven_ampl` DECIMAL(10,2) COMMENT 'Амплитуда волны P/R (мВ)',
  `test_atr_stimul_thresh` INT(11) COMMENT 'Порог стимуляции (В)',
  `test_ven_stimul_thresh` INT(11) COMMENT 'Порог стимуляции (В)',
  `pacer` TINYINT(1) DEFAULT 0 COMMENT 'Электрод КС',
  `pacer_conf` TINYINT(1) DEFAULT 0 COMMENT 'Конфигурация ЭКС',
  `pacer_ampl` DECIMAL(10,2) COMMENT 'Амплитуда волны R (мВ)',
  `pacer_stimul_thresh` INT(11) COMMENT 'Порог стимуляции (В)',
  `pacer_imp` INT(11) COMMENT 'Импеданс (Ом)',
  `placement` TEXT COMMENT 'Положение',
  `rtg_time_hmsc` TINYINT(1) COMMENT 'Home Monitoring Service Center',
  `rtg_time_semaphore` TINYINT(1) COMMENT 'Концепция \"Светофор\"',
  `rtg_time_iegm` TINYINT(1) COMMENT 'IEGM online (ВЭГМ онлайн)',
  `rtg_cl_hmsc` TINYINT(1) COMMENT 'Home Monitoring Service Center',
  `rtg_cl_semaphore` TINYINT(1) COMMENT 'Концепция \"Светофор\"',
  `rtg_cl_iegm` TINYINT(1) COMMENT 'IEGM online (ВЭГМ онлайн)',
  `rtg_cl_data_suff` TINYINT(1) COMMENT 'Достаточность данных HM',
  `cl_hosp_days` INT(11) COMMENT 'Количество койко-дней, проведенных в стационаре за год',
  `cl_disability_days` INT(11) COMMENT 'Количество дней нетрудоспособности (на больничном) за год',
  `cl_amb_calls` INT(11) COMMENT 'Количество обращений к службе скорой помощи за год',
  `cl_disease_complications` TEXT COMMENT 'Осложнения основного заболевания, которые возникли за год (перечислить и описать)',
  `comment` TEXT COMMENT 'Комментарий/Примечание',
  PRIMARY KEY (`protocol_uid`)
) ENGINE=INNODB
COMMENT='Обследование через 12 месяцев';
"
        );

        $this->addForeignKey(
            "fk_protocol_uid_protocol_annual",
            "{{protocol_annual}}",
            "protocol_uid",
            "{{protocol}}",
            "uid",
            "CASCADE",
            "CASCADE"
        );

    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_protocol_uid_protocol_annual", "{{protocol_annual}}");
        $this->dropTable("{{protocol_annual}}");
    }
}
