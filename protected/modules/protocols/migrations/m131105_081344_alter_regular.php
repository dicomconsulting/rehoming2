<?php

class m131105_081344_alter_regular extends I18nDbMigration
{
    public function safeUp()
    {
        $this->alterColumn("{{protocol_regular}}", "idc_vt1_time", "INT(11) NULL COMMENT \"время VT1\"");
        $this->alterColumn("{{protocol_regular}}", "idc_vt2_time", "INT(11) NULL COMMENT \"время VT2\"");
        $this->alterColumn("{{protocol_regular}}", "idc_vf_time", "INT(11) NULL  COMMENT \"время VF\"");
        $this->alterColumn("{{protocol_regular}}", "arr_vt1_cnt", "INT(11) DEFAULT 0  COMMENT 'Количество ЖТ1'");
        $this->alterColumn("{{protocol_regular}}", "arr_vt2_cnt", "INT(11) DEFAULT 0  COMMENT 'Количество ЖТ2'");
        $this->alterColumn("{{protocol_regular}}", "arr_vf_cnt", "INT(11) DEFAULT 0  COMMENT 'Количество ФЖ'");
    }

    public function safeDown()
    {
        $this->alterColumn("{{protocol_regular}}", "idc_vt1_time", "decimal(10,2) NULL  COMMENT 'время VT1'");
        $this->alterColumn("{{protocol_regular}}", "idc_vt2_time", "decimal(10,2) NULL  COMMENT 'время VT2'");
        $this->alterColumn("{{protocol_regular}}", "idc_vf_time", "decimal(10,2) NULL  COMMENT 'время VF'");
        $this->alterColumn("{{protocol_regular}}", "arr_vt1_cnt", "INT(11) NULL COMMENT 'Количество ЖТ1'");
        $this->alterColumn("{{protocol_regular}}", "arr_vt2_cnt", "INT(11) NULL COMMENT 'Количество ЖТ2'");
        $this->alterColumn("{{protocol_regular}}", "arr_vf_cnt", "INT(11) NULL COMMENT 'Количество ФЖ'");
    }
}
