<?php

class m131121_071457_alter_protocol_add_status extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn(
            "{{protocol}}",
            "status",
            "TINYINT(1) DEFAULT 0 COMMENT 'Статус протокола, 0 - в работе, 1 - закрыт'"
        );
    }

    public function safeDown()
    {
        $this->dropColumn("{{protocol}}", "status");
    }

}
