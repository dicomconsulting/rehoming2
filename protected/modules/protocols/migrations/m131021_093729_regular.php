<?php

class m131021_093729_regular extends I18nDbMigration
{
    public function safeUp()
    {
        $this->execute(
            "CREATE TABLE {{protocol_regular}} (
          `protocol_uid` int(11) NOT NULL,
          `survey_period` tinyint(1) DEFAULT NULL COMMENT 'Обследование через',
          `clinic_distance` decimal(10,2) DEFAULT 0 COMMENT 'Расстояние (км)',
          `clinic_time` decimal(10,2) DEFAULT 0 COMMENT 'Время в пути (часов)',
          `stimul_mode` varchar(255) DEFAULT null COMMENT 'Режим стимуляции',
          `idc_vt1` tinyint(1) DEFAULT 0 COMMENT 'VT1',
          `idc_vt1_time` decimal(10,2) DEFAULT 0 COMMENT 'время VT1',
          `idc_vt2` tinyint(1) DEFAULT 0 COMMENT 'VT2',
          `idc_vt2_time` decimal(10,2) DEFAULT 0 COMMENT 'время VT2',
          `idc_vf` tinyint(1) DEFAULT 0 COMMENT 'VF',
          `idc_vf_time` decimal(10,2) DEFAULT 0 COMMENT 'время VF',
          `ae_pacing_threshold` decimal(10,2) DEFAULT NULL COMMENT 'Предсердный электрод, Порог стимуляции (В)',
          `ae_pacing_threshold_dur` decimal(10,2) DEFAULT NULL COMMENT 'Предсердный электрод, Длительность импульса (мс)',
          `ae_pulse_ampl` decimal(10,2) DEFAULT NULL COMMENT 'Амплитуда волны P (мВ)',
          `ae_pacing_imp` int(11) DEFAULT NULL COMMENT 'Импеданс (Ом)',
          `ve_stimul_threshold` decimal(10,2) DEFAULT NULL COMMENT 'Порог стимуляции(В)',
          `ve_stimul_dur` decimal(10,2) DEFAULT NULL COMMENT 'Длительность стимула (мс)',
          `ve_wave_apl` decimal(10,2) DEFAULT NULL COMMENT 'Амплитуда волны R (мВ)',
          `ve_imp` int(11) DEFAULT NULL COMMENT 'Импеданс (Ом)',
          `ve_imp_shock` int(11) DEFAULT NULL COMMENT 'Импеданс шоковый (Ом)',

          `arr_vt1_cnt` int(11) DEFAULT NULL COMMENT 'Количество ЖТ1',
          `arr_vt2_cnt` int(11) DEFAULT NULL COMMENT 'Количество ЖТ2',
          `arr_vf_cnt` int(11) DEFAULT NULL COMMENT 'Количество ФЖ',
          `atc_began_vt` int(11) DEFAULT NULL COMMENT 'АТС начато ЖТ1/ЖТ2',
          `atc_compl_vt` int(11) DEFAULT NULL COMMENT 'АТС успешно ЖТ1/ЖТ2',
          `atc_began_vf` int(11) DEFAULT NULL COMMENT 'АТС начато ФЖ',
          `atc_compl_vf` int(11) DEFAULT NULL COMMENT 'АТС успешно ФЖ',
          `shock_began` int(11) DEFAULT NULL COMMENT 'Шоков начато',
          `shock_cancel` int(11) DEFAULT NULL COMMENT 'Шоков отменено',
          `shock_compl` int(11) DEFAULT NULL COMMENT 'Шоков успешно',
          `svt_total_cnt` int(11) DEFAULT NULL COMMENT 'Количество эпизодов НЖТ',
          `svt_12h_cnt` int(11) DEFAULT NULL COMMENT 'Количество эпизодов НЖТ длительностью более 12 часов',

          `af_episode_time` TIME DEFAULT NULL COMMENT 'Продолжительность самого длинного эпизода ФП',
          `af_episode_hr_max` int(11) DEFAULT NULL COMMENT 'Максимальная ЧСС при ФП (ударов в минуту)',
          `af_episode_hr_avg` int(11) DEFAULT NULL COMMENT 'Средняя ЧСС при ФП (ударов в минуту)',

          `vent_contr_prop` int(11) DEFAULT NULL COMMENT 'Доля стимулиролированных сокращений желудочков (Vp) (%)',

          `vent_rate_24` int(11) DEFAULT NULL COMMENT 'Средний желудочковый ритм за 24 часа (ударов в минуту)',
          `vent_rate_fu` int(11) DEFAULT NULL COMMENT 'Средний желудочковый ритм с последнего FU (ударов в минуту)',
          `vent_rate_24_rest` int(11) DEFAULT NULL COMMENT 'Средний желудочковый ритм в покое за 24 часа (ударов в минуту)',
          `vent_rate_fu_rest` int(11) DEFAULT NULL COMMENT 'Средний желудочковый ритм в покое с последнего FU (ударов в минуту)',
          `patient_act` int(11) DEFAULT NULL COMMENT 'Активность пациента за 24 часа (%)',
          `patient_act_fu` int(11) DEFAULT NULL COMMENT 'Активность пациента с последнего FU (%)',
          `vent_contr_24` int(11) DEFAULT NULL COMMENT 'Количество желудочковых экстрасистол за 24 часа',
          `vent_contr_fu` int(11) DEFAULT NULL COMMENT 'Количество желудочковых экстрасистол с последнего FU',

          `ekg_needed` tinyint(1) DEFAULT NULL COMMENT 'Необходимо обследование пациента методом эхокардиографии?',
          `ekg_supplied` tinyint(1) DEFAULT NULL COMMENT 'Протокол эхокардиографического обследования прилагается?',
          `program_change_needed` tinyint(1) DEFAULT NULL COMMENT 'Необходимо изменение в программе имплантата?',
          `drugs_change_needed` tinyint(1) DEFAULT NULL COMMENT 'Необходимо изменение в медикаментозной антиаритмической терапии?',
          `program_changes` TEXT DEFAULT NULL COMMENT 'Какие изменения были внесены в программу аппарата?',

          `elect_displ_detect_date` DATE DEFAULT NULL COMMENT 'Повреждение или смещение электрода, дата выявления',
          `elect_displ_hosp_date` DATE DEFAULT NULL COMMENT 'Повреждение или смещение электрода, дата госпитализации',
          `elect_displ_hosp_emerg` tinyint(1) DEFAULT NULL COMMENT 'Экстренность госпитализации',

          `hf_detect_date` DATE DEFAULT NULL COMMENT 'Нарастание признаков сердечной недостаточности, требующих госпитализацию пациента, дата выявления',
          `hf_hosp_date` DATE DEFAULT NULL COMMENT 'Дата госпитализации',
          `hf_hosp_emerg` tinyint(1) DEFAULT NULL COMMENT 'Экстренность госпитализации',

          `idc_fail_detect_date` DATE DEFAULT NULL COMMENT 'Неадекватные срабатывания ИКД, требующие перепрограммирования аппарата, дата выявления',
          `idc_fail_hosp_date` DATE DEFAULT NULL COMMENT 'Дата осмотра',
          `idc_fail_hosp_emerg` tinyint(1) DEFAULT NULL COMMENT 'Экстренность осмотра',

          `option_changed` tinyint(1) DEFAULT '0' COMMENT 'Опция Home Monitoring изменена со времени включения',
          `option_changed_new_value` text COMMENT 'Укажите новую опцию',
          `option_changed_reason` text COMMENT 'Причина изменения',
          `red_finding` tinyint(1) DEFAULT '0' COMMENT 'Статус сообщения Home Monitoring изменен на “red” (красный)',
          `red_finding_date` date DEFAULT NULL COMMENT 'Дата изменения',
          `red_finding_message` text COMMENT 'Сообщение',
          `red_finding_changed` tinyint(1) DEFAULT '0' COMMENT 'Статус сообщения Home Monitoring изменен с “red” (красный) на другой',
          `red_finding_changed_date` date DEFAULT NULL COMMENT 'Дата изменения',
          `red_finding_changed_to` tinyint(1) DEFAULT '0' COMMENT 'На какой цвет',
          `red_finding_changed_message` text COMMENT 'Сообщение',
          `red_finding_changed_comment` text COMMENT 'Примечание',
          PRIMARY KEY (`protocol_uid`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Данные протокола регулярного обследования';"
        );


        $this->addForeignKey(
            'fk_protocol_uid_protocol_regular',
            '{{protocol_regular}}',
            'protocol_uid',
            '{{protocol}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );


    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_protocol_uid_protocol_regular", "{{protocol_regular}}");
        $this->dropTable("{{protocol_regular}}");
    }
}
