<?php

class m140127_080701_add_enrolment_diagnosis_rel extends I18nDbMigration
{
    public function safeUp()
    {

        $this->createTable(
            '{{protocol_enrolment_diagnosis}}',
            array(
                'protocol_uid' => "int(11) NOT NULL COMMENT 'Ид протокола'",
                'icd10_uid' => "int(11) NOT NULL COMMENT 'ид заболевания'",
                'PRIMARY KEY (`protocol_uid`,`icd10_uid`)',
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT="Клинический диагноз,' .
            ' отражаемый в протоколе включения"'
        );

        $this->createIndex(
            "idx_protocol_icd10_unique",
            '{{protocol_enrolment_diagnosis}}',
            "protocol_uid,icd10_uid",
            true
        );

        $this->addForeignKey(
            'fk_protocol_uid_protocol_enrolment_diagnosis',
            '{{protocol_enrolment_diagnosis}}',
            'protocol_uid',
            '{{protocol}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_icd10_uid_protocol_enrolment_diagnosis',
            '{{protocol_enrolment_diagnosis}}',
            'icd10_uid',
            '{{icd10}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );

        $this->dropColumn("{{protocol_enrolment}}", "diagnosis");
    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_protocol_uid_protocol_enrolment_diagnosis", "{{protocol_enrolment_diagnosis}}");
        $this->dropForeignKey("fk_icd10_uid_protocol_enrolment_diagnosis", "{{protocol_enrolment_diagnosis}}");
        $this->dropTable("{{protocol_enrolment_diagnosis}}");

        $this->addColumn("{{protocol_enrolment}}", "diagnosis", "int(11) DEFAULT NULL COMMENT 'Клинический диагноз'");
    }
}
