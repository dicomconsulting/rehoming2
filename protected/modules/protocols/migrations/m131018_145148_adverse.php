<?php

class m131018_145148_adverse extends I18nDbMigration
{
    public function safeUp()
    {

        $this->execute(
            "CREATE TABLE {{protocol_adverse}}(
              `protocol_uid` INT(11) NOT NULL,
              `protocol_type` TINYINT(1) DEFAULT 0 COMMENT 'Тип отчета',
              `adv_begin_date` DATE COMMENT 'Дата начала нежелательного явления',
              `adv_detect_by_hm` TINYINT(1) DEFAULT 0 COMMENT 'НЯ обнаружено благодаря функции Home Monitoring',
              `adv_detect_by_hm_date` DATE COMMENT 'Дата обнаружения',
              `adv_result` TINYINT(1) COMMENT 'Исход',
              `adv_resolve_date` DATE COMMENT 'Дата разрешения НЯ',
              `adv_descr` TEXT COMMENT 'Описание нежелательного явления',
              `adv_symptoms` TEXT COMMENT 'Симптомы',
              `adv_actions` TEXT COMMENT 'Предпринятые действия',
              `adv_actions_result` TEXT COMMENT 'Результат',
              `adv_iml_relation` TINYINT(1) DEFAULT 0 COMMENT 'Связь нежелательного явления с имплантатом',
              `adv_iml_relation_device` TINYINT(1) DEFAULT 0 COMMENT 'Укажите имплантат, связанный с нежелательным явлением',
              `adv_iml_relation_electr` TINYINT(1) DEFAULT 0 COMMENT 'Электрод',
              `impl_model` VARCHAR(255) COMMENT 'Модель',
              `impl_sn` VARCHAR(255) COMMENT 'Серийный номер',
              `impl_expl` TINYINT(1) DEFAULT 0 COMMENT 'Прибор эксплантирован',
              `adv_lethal` TINYINT(1) DEFAULT 0 COMMENT 'Нежелательное явление привело к смерти пациента',
              `adv_injury` TINYINT(1) DEFAULT 0 COMMENT 'жизнеугрожающее заболевание или ранение',
              `adv_perm_damage` TINYINT(1) DEFAULT 0 COMMENT 'хроническое или необратимое поражение структуры или функции организма',
              `adv_hospitalization` TINYINT(1) DEFAULT 0 COMMENT 'госпитализация или продление госпитализации',
              `adv_surg` TINYINT(1) DEFAULT 0 COMMENT 'медицинское или хирургическое вмешательство для предупреждения поражения структуры или функции организма',
              `adv_distress` TINYINT(1) DEFAULT 0 COMMENT 'НЯ привело к дистрессу, смерти плода или врожденной аномалии, или дефекту рождения',
              `adv_prevented` TINYINT(1) DEFAULT 0 COMMENT 'Только для НЯ, связанных с прибором: явление, вероятно, привело бы к одному из указанных выше результатов, если бы не были предприняты правильные действия, вмешательства, или, если бы обстоятельства складывались менее благоприятно',
              `adv_death_date`  DATE COMMENT 'Дата смерти',
              `adv_death_cause` TEXT COMMENT 'Основная причина смерти',
              `adv_death_cardio` TINYINT(1) DEFAULT 0 COMMENT 'Сердечно-сосудистая причина',
              `adv_death_arr` TINYINT(1) DEFAULT 0 COMMENT 'Аритмическая причина',
              `adv_comment` TEXT COMMENT 'Примечание',
              PRIMARY KEY (`protocol_uid`)
            ) ENGINE=INNODB;
        "
        );

        $this->addForeignKey(
            "fk_protocol_uid_protocol_adverse",
            "{{protocol_adverse}}",
            "protocol_uid",
            "{{protocol}}",
            "uid",
            "CASCADE",
            "CASCADE"
        );

    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_protocol_uid_protocol_adverse", "{{protocol_adverse}}");
        $this->dropTable("{{protocol_adverse}}");
    }
}
