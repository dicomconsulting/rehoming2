<?php

class m131119_134534_alter_surgery extends I18nDbMigration
{
    public function safeUp()
    {
        $this->dropIndex("idx_protocol_surgery_unique", "{{protocol_enrolment_surgery}}");
    }

    public function safeDown()
    {
        $this->createIndex(
            "idx_protocol_surgery_unique",
            '{{protocol_enrolment_surgery}}',
            "protocol_uid,surgery_uid",
            true
        );
    }
}
