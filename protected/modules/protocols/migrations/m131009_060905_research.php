<?php

class m131009_060905_research extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{research}}',
            array(
                'uid' => 'pk',
                'date_begin' => "DATE NOT NULL COMMENT 'Дата включения в исследование пациента'",
                'date_end' => "INT(11) COMMENT 'Дата окончания исследования'",
                'patient_uid' => "INT(11) NOT NULL COMMENT 'Ссылка на пациента'",
                'medical_center_uid' => "INT(11) NOT NULL COMMENT 'Ссылка на мед. центр'",
                'screening_number' => "INT(11) NOT NULL COMMENT 'Номер отбора'",
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );

        $this->createIndex(
            'unique_screening_number',
            '{{research}}',
            'screening_number',
            true
        );

        $this->addForeignKey(
            'fk_patient_uid_research',
            '{{research}}',
            'patient_uid',
            '{{patient}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_medical_center_uid_research',
            '{{research}}',
            'medical_center_uid',
            '{{medical_center}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {

        $this->dropForeignKey("fk_patient_uid_research", "{{research}}");
        $this->dropForeignKey("fk_medical_center_uid_research", "{{research}}");


        $this->dropTable('{{research}}');
    }
}
