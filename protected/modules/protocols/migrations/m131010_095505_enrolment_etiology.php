<?php

class m131010_095505_enrolment_etiology extends I18nDbMigration
{
    public function safeUp()
    {

        $this->createTable(
            '{{protocol_enrolment_etiology}}',
            array(
                'protocol_uid' => "int(11) NOT NULL COMMENT 'Ид протокола'",
                'disease_uid' => "int(11) NOT NULL COMMENT 'Ид заболевания'",
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT="Этиология пациента на момент включения в исследование"'
        );

        $this->createIndex(
            "idx_protocol_etiology_unique",
            '{{protocol_enrolment_etiology}}',
            "protocol_uid,disease_uid",
            true
        );

        $this->addForeignKey(
            'fk_protocol_uid_protocol_enrolment_etiology',
            '{{protocol_enrolment_etiology}}',
            'protocol_uid',
            '{{protocol}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );


    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_protocol_uid_protocol_enrolment_etiology", "{{protocol_enrolment_etiology}}");
        $this->dropIndex('idx_protocol_etiology_unique', '{{protocol_enrolment_etiology}}');
        $this->dropTable('{{protocol_enrolment_etiology}}');
    }
}
