<?php

class m131023_120322_alter_enrol_add_pacing_dur extends I18nDbMigration
{
    public function safeUp()
    {

        $this->execute(
            "ALTER TABLE {{protocol_enrolment}}
                    CHANGE `ae_pacing_threshold` `ae_pacing_threshold` DECIMAL(10,2) NULL  COMMENT 'Порог стимуляции(В)',
                    ADD COLUMN `ae_pacing_threshold_dur` DECIMAL(10,2) NULL  COMMENT 'Длительность импульса (мс)' AFTER `ae_pacing_threshold`;"
        );
    }

    public function safeDown()
    {
        $this->dropColumn("{{protocol_enrolment}}", "ae_pacing_threshold_dur");
    }
}
