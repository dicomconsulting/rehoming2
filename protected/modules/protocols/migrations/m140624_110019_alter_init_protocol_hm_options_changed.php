<?php

class m140624_110019_alter_init_protocol_hm_options_changed extends I18nDbMigration
{
    public function safeUp()
    {
        $this->alterColumn('{{protocol_inithm}}', 'hm_options_changed', 'VARCHAR(255) DEFAULT "" COMMENT "Конфигурация событий Home monitoring (Options)"');
    }

    public function safeDown()
    {
        $this->execute("UPDATE {{protocol_inithm}} SET hm_options_changed = 0;");
        $this->alterColumn('{{protocol_inithm}}', 'hm_options_changed', 'TINYINT(1) DEFAULT 0 COMMENT "Конфигурация событий Home monitoring (Options)"');
    }
}
