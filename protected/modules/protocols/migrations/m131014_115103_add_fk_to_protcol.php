<?php

class m131014_115103_add_fk_to_protcol extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addForeignKey(
            'fk_research_uid_protocol',
            '{{protocol}}',
            'research_uid',
            '{{research}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_research_uid_protocol', '{{protocol}}');
    }
}
