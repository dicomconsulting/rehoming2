<?php

class m131030_073447_alter_regular extends I18nDbMigration
{
    public function safeUp()
    {

        $this->alterColumn("{{protocol_regular}}", "af_episode_time", "INT NULL COMMENT 'Продолжительность самого длинного эпизода ФП'");
    }

    public function safeDown()
    {
        $this->alterColumn("{{protocol_regular}}", "af_episode_time", "TIME NULL COMMENT 'Продолжительность самого длинного эпизода ФП'");
    }
}
