<?php

class m131101_075031_alter_enrolment extends I18nDbMigration
{
    public function safeUp()
    {

        $this->execute("UPDATE {{protocol_enrolment}} SET diagnosis = NULL");

        $this->alterColumn(
            "{{protocol_enrolment}}",
            "diagnosis",
            "INT(11) NULL  COMMENT 'Клинический диагноз'"
        );

        $this->dropColumn("{{protocol_enrolment}}", "enrolment_tele");
        $this->dropColumn("{{protocol_enrolment}}", "ae");
    }

    public function safeDown()
    {
        $this->alterColumn(
            "{{protocol_enrolment}}",
            "diagnosis",
            "TEXT NULL  COMMENT 'Клинический диагноз'"
        );

        $this->addColumn("{{protocol_enrolment}}", "enrolment_tele", "tinyint(1) NULL comment 'Имплантирован с функцией мобильного телемониторинга'");
        $this->addColumn("{{protocol_enrolment}}", "enrolment_tele", "tinyint(1) NULL comment 'Предсердный электрод'");
    }
}
