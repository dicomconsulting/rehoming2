<?php

class m140414_075313_alter_enrolment_protocol extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn(
            '{{protocol_enrolment}}',
            'impl_pacer_sn',
            'varchar(255) DEFAULT NULL COMMENT "Серийный номер ЭКС" AFTER ae_sn'
        );
        $this->addColumn(
            '{{protocol_enrolment}}',
            'impl_pacer_model',
            'varchar(255) DEFAULT NULL COMMENT "Модель ЭКС" AFTER impl_pacer_sn'
        );
    }

    public function safeDown()
    {
        $this->dropColumn(
            '{{protocol_enrolment}}',
            'impl_pacer_sn'
        );
        $this->dropColumn(
            '{{protocol_enrolment}}',
            'impl_pacer_model'
        );
    }
}
