<?php

class m131118_102124_alter_regular extends I18nDbMigration
{
    public function safeUp()
    {
        $this->execute(
            "ALTER TABLE {{protocol_regular}}
  CHANGE `clinic_distance` `clinic_distance` DECIMAL(10,2) NULL  COMMENT 'Расстояние (км)',
  CHANGE `clinic_time` `clinic_time` DECIMAL(10,2) NULL  COMMENT 'Время в пути (часов)',
  CHANGE `arr_vt1_cnt` `arr_vt1_cnt` INT(11) NULL  COMMENT 'Количество ЖТ1',
  CHANGE `arr_vt2_cnt` `arr_vt2_cnt` INT(11) NULL  COMMENT 'Количество ЖТ2',
  CHANGE `arr_vf_cnt` `arr_vf_cnt` INT(11) NULL  COMMENT 'Количество ФЖ';"
        );
    }

    public function safeDown()
    {
        $this->execute(
            "ALTER TABLE {{protocol_regular}}
  CHANGE `clinic_distance` `clinic_distance` DECIMAL(10,2) DEFAULT 0 NULL  COMMENT 'Расстояние (км)',
  CHANGE `clinic_time` `clinic_time` DECIMAL(10,2) DEFAULT 0  NULL  COMMENT 'Время в пути (часов)',
  CHANGE `arr_vt1_cnt` `arr_vt1_cnt` INT(11) DEFAULT 0 NULL COMMENT 'Количество ЖТ1',
  CHANGE `arr_vt2_cnt` `arr_vt2_cnt` INT(11) DEFAULT 0  NULL COMMENT 'Количество ЖТ2',
  CHANGE `arr_vf_cnt` `arr_vf_cnt` INT(11) DEFAULT 0 NULL COMMENT 'Количество ФЖ';"
        );
    }
}
