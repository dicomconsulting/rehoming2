$(function () {
    //влияние селекта "Связь нежелательного явления с имплантатом"
    var adverseRelation = $("#ProtocolAdverse_adv_iml_relation");
    var relatedFields = ["adv_iml_relation_device", "adv_iml_relation_electr", "impl_model", "impl_sn", "impl_expl"];
    var fieldPrefix = window.fieldsPrefix;

    handleRelations();

    adverseRelation.on("change", function() {
        handleRelations();
    });

    function handleRelations() {
        var optionVal = adverseRelation.find("option:selected").val();
        if (optionVal != 0) {
            showRelated();
        } else {
            hideRelated();
        }
    }

    function showRelated() {
        $.each(relatedFields, function(i,el) {
            $("#" + fieldPrefix + el).removeAttr("disabled");
        });
    }

    function hideRelated() {
        $.each(relatedFields, function(i,el) {
            var field = $("#" + fieldPrefix + el);
            field.attr("disabled", "disabled");
        });
    }

});

