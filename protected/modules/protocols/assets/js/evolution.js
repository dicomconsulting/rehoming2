/**
 * Created with JetBrains PhpStorm.
 * User: Дамир
 * Date: 14.10.13
 * Time: 12:24
 */

$(function () {

    $("#add_red_finding").on("click", function() {
        if (checkFindingSelected("red")){
            renderRedFindingRow();
        }

        return false;
    });

    $("#add_yellow_finding").on("click", function() {
        if (checkFindingSelected("yellow")){
            renderYellowFindingRow();
        }

        return false;
    });

    $(document).on("click", ".remove_red_finding", function () {
        removeRedFinding(this);

        return false;
    });

    $(document).on("click", ".remove_yellow_finding", function () {
        removeYellowFinding(this);

        return false;
    });

    function renderRedFindingRow () {

        var jqFindingSelector = $("#redFindingSelector");
        var jqFindingContainer = $("#redFindingsContainer");

        var findingId = jqFindingSelector.find("option:selected").val();

        var currFinding;

        for (var i in window.redFindings) {
            if (window.redFindings[i].uid == findingId) {
                currFinding = window.redFindings[i];
            }
        }

        var templData = {};
        templData.finding = currFinding.finding;
        templData.findingId = findingId;
        templData.create_time = currFinding.date_create;

        jqFindingContainer.append(Mustache.render(window.redFindingTemplate, templData));
        jqFindingSelector.find("option:selected").remove();

        attachDatepickerToFields(jqFindingSelector);
        renumberFindings("red");
    }

    function removeRedFinding(link) {

        var jqFindingSelector = $("#redFindingSelector");

        var linkParent = $(link).parent().parent().parent();
        var findingId = linkParent.find("input.findingId").val();

        var currFinding;

        for (var i in window.redFindings) {
            if (window.redFindings[i].uid == findingId) {
                currFinding = window.redFindings[i];
            }
        }

        jqFindingSelector.append("<option value='"+findingId+"'>"+ currFinding['date_create'] + " " + currFinding['finding'] +"</option>");

        linkParent.remove();

        renumberFindings("red");
    }

    function renderYellowFindingRow () {

        var jqFindingSelector = $("#yellowFindingSelector");
        var jqFindingContainer = $("#yellowFindingsContainer");

        var findingId = jqFindingSelector.find("option:selected").val();

        if (!findingId) {
            return;
        }

        var currFinding;

        for (var i in window.yellowFindings) {
            if (window.yellowFindings[i].uid == findingId) {
                currFinding = window.yellowFindings[i];
            }
        }

        var templData = {};
        templData.finding = currFinding.finding;
        templData.findingId = findingId;
        templData.create_time = currFinding.date_create;

        jqFindingContainer.append(Mustache.render(window.yellowFindingTemplate, templData));
        jqFindingSelector.find("option:selected").remove();

        renumberFindings("yellow");
    }

    function removeYellowFinding(link) {

        var jqFindingSelector = $("#yellowFindingSelector");

        var linkParent = $(link).parent().parent().parent();
        var findingId = linkParent.find("input.findingId").val();

        var currFinding;

        for (var i in window.yellowFindings) {
            if (window.yellowFindings[i].uid == findingId) {
                currFinding = window.yellowFindings[i];
            }
        }

        jqFindingSelector.append("<option value='"+findingId+"'>"+ currFinding['date_create'] + " " + currFinding['finding'] +"</option>");

        linkParent.remove();

        renumberFindings("yellow");
    }

    function checkFindingSelected (type) {
        var jqFindingSelector;
        if (type == "red") {
            jqFindingSelector = $("#redFindingSelector");
        } else {
            jqFindingSelector = $("#yellowFindingSelector");
        }

        var val = jqFindingSelector.find("option:selected").val();

        if (!parseInt(val)){
            alert("Необходимо выбрать НЯ!");
            return false;
        } else {
            return true;
        }
    }

    function renumberFindings(type) {
        var jqFindingContainer;
        if (type == "red") {
            jqFindingContainer = $("#redFindingsContainer");
        } else {
            jqFindingContainer = $("#yellowFindingsContainer");
        }

        $.each(jqFindingContainer.find("span.finding_number"), function (i, el) {
            $(el).text(i + 1);
        });
    }


});

