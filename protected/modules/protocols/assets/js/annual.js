/**
 * Created by Дамир on 27.11.13.
 */
$(function () {
    //удаление болезни из осложнений
    $(document).on("click", ".remove_etiology_row", function() {
        $(this).parent().parent().parent().remove();
        return false;
    });

});

function processComorbidities (id, name) {
    var container = $("#comorbidities_container");
    var existingRow = $("input[class=custom_etiology][value="+id+"]");

    if (existingRow.length) {
        alert("Такое заболевание уже обозначено");
        return false;
    }

    var templData = {};
    templData.id = id;
    templData.name = name;

    container.append(Mustache.render(window.deseaseRelTemplate, templData));

    return true;
}
