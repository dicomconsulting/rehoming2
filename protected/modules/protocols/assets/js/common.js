$(function () {

    //подтверждение удаление протокола
    $(".deleteProtocolLink").click(function () {
        var question = "Удалить протокол \"" + window.protocolName + "\" от " + $(this).parent().parent().find("td.date_create_cell").eq(0).text() + "?";
        if (confirm(question)){
            return true;
        }
        return false;
    });

    //прописываем слушателей на чекбоксы для изменения активности зависимых полей
    function updateFieldsDependencies(subscribeOnEvents) {
        if (window.fieldsDependency !== undefined) {
            var prefix = window.fieldsPrefix;
            var fieldsToDisable = [];
            var fieldsToShow = [];
            $.each(window.fieldsDependency, function (i, el) {

                //сначала проверяем текущий статус чекбокса, если офф, то дизейблим поля
                var fieldName = "#" + prefix + i;
                if (!$(fieldName).is(':checked')) {
                    $.each(el, function (i, subel) {
                        fieldsToDisable.push("#" + prefix + subel);
                        fieldsToDisable.push("#" + prefix + subel + "_datepicker");//обработка для календаря
                    });
                } else {
                    $.each(el, function (i, subel) {
                        fieldsToShow.push("#" + prefix + subel);
                        fieldsToShow.push("#" + prefix + subel + "_datepicker");//обработка для календаря
                    });
                }

                //на изменении состояния переключаем поля
                if (subscribeOnEvents) {
                    $(fieldName).on("change", function(){
                        updateFieldsDependencies(false);
                    });
                }
            });

            for (i in fieldsToDisable) {
                $(fieldsToDisable[i]).attr("disabled", "disabled");
                $(fieldsToDisable[i]).attr('readOnly', 'true');
            }

            for (i in fieldsToShow) {
                $(fieldsToShow[i]).removeAttr("disabled");
                $(fieldsToShow[i]).removeAttr('readOnly');
            }
        }
    }

    updateFieldsDependencies(true);

    /**
     * Отмечаем для форм факт того что она была изменена
     */
    $("form :input").change(function() {
        $(this).closest('form').data('changed', true);
    });

    $("input.form_cancel_button").click(function() {
        var form = $(this).closest('form');

        if(form.data('changed') || $(form).find(":input").length != form.data('input_count')) {
            if(!confirm("Протокол не сохранится. Вы действительно хотите отменить изменения?")){
                return false;
            }
        }
        window.location.href = $(this).data("return_location");
    });

    /**
     * сохраним общее количество элементов на тот случай если пользователь удалит какое-либо, например, удалит
     * принимаемые препараты
     */
    $.each($("form"), function(i, el) {
        $(el).data("input_count", $(el).find(":input").length);
    });
});