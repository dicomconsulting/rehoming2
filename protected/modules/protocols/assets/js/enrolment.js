/**
 * Created with JetBrains PhpStorm.
 * User: Дамир
 * Date: 14.10.13
 * Time: 12:24
 *
 */

$(function () {

    //добавление операции
    $("#add_surgery").on("click", function() {
        renderNewSurgeryRow();

        return false;
    });

    //удаление операции
    $(document).on("click", "#surgeryContainer .remove_surgery", function() {
        removeSurgeryRow(this);

        return false;
    });

    //добавление лекарственного препарата из предопределенных списков
    $(".add_pharm").click(function () {
        renderNewPharm(this);
        return false;
    });

    //удаление лекарственного средства
    $(document).on("click", ".remove_pharm", function() {
        removePharmRow(this);

        return false;
    });

    //удаление болезни из этиологии
    $(document).on("click", ".remove_etiology_row", function() {
        $(this).parent().remove();
        return false;
    });

    $('#' + window.fieldsPrefix + "enrolment_pacer").change(function() {
        $('#' + window.fieldsPrefix + 'impl_pacer').prop('checked', $(this).is(':checked')).trigger('change');
    });
    $('#' + window.fieldsPrefix + "enrolment_crt_p").change(function() {
        $('#' + window.fieldsPrefix + 'impl_crt_p').prop('checked', $(this).is(':checked')).trigger('change');
    });
    $('#' + window.fieldsPrefix + "enrolment_crt_d").change(function() {
        $('#' + window.fieldsPrefix + 'impl_crt_d').prop('checked', $(this).is(':checked'));
        $('#' + window.fieldsPrefix + 'sve').prop('checked', $(this).is(':checked') || $('#' + window.fieldsPrefix + "enrolment_icd").is(':checked')).trigger('change');
    });
    $('#' + window.fieldsPrefix + "enrolment_icd").change(function() {
        $('#' + window.fieldsPrefix + 'impl_idc').prop('checked', $(this).is(':checked'));
        $('#' + window.fieldsPrefix + 'sve').prop('checked', $(this).is(':checked') || $('#' + window.fieldsPrefix + "enrolment_crt_d").is(':checked')).trigger('change');
    });
    $('#' + window.fieldsPrefix + 'impl_idc, #' + window.fieldsPrefix + 'impl_crt_d').change(function () {
        $('#' + window.fieldsPrefix + 'sve').prop('checked', $(this).is(':checked') || $('#' + window.fieldsPrefix + "impl_crt_d").is(':checked')).trigger('change');
    });
    $('#' + window.fieldsPrefix + 'sve').prop('checked', $(this).is(':checked') || $('#' + window.fieldsPrefix + "impl_crt_d").is(':checked')).trigger('change');


    function renderNewSurgeryRow () {

        var jqSurgerySelector = $("#surgeryTypeSelector");
        var jqSurgeryContainer = $("#surgeryContainer");

        var surgeryId = parseInt(jqSurgerySelector.find("option:selected").val());
        var surgeryName = jqSurgerySelector.find("option:selected").text();

        if (!surgeryId) {
            alert("Необходимо выбрать операцию");
            return false;
        }

        var rnd = Math.floor(Math.random() * 10000);

        var templData = {};
        templData.surgeryName = surgeryName;
        templData.rnd = rnd;
        templData.surgeryId = surgeryId;

        jqSurgeryContainer.append(Mustache.render(window.surgeryTemplate, templData));

        attachDatepickerToFields(jqSurgeryContainer);
    }

    function removeSurgeryRow (link) {
        $(link).parent().parent().parent().remove();
    }

    function renderNewPharm (link) {
        var cover = $(link).parents("div.pharm_cover");
        var jqPharmSelector = cover.find("select.pharm_selector");
        var jqPharmContainer = cover.find("div.pharm_container");

        var pharmId = parseInt(jqPharmSelector.find("option:selected").val());
        var pharmName = jqPharmSelector.find("option:selected").text();

        if (!pharmId) {
            alert("Необходимо выбрать препарат");
            return false;
        }

        var templData = {};
        templData.pharmName = pharmName;
        templData.pharmId = pharmId;
        templData.measure = [];

        $.each(window.pharmUnits, function (i, el) {
            templData.measure[templData.measure.length] = {"id" : i, "val": el}
        });

        jqPharmContainer.append(Mustache.render(window.pharmProductTemplate, templData));
    }


    function removePharmRow (link) {
        $(link).parent().parent().remove();
    }

});

function processDisease(id, name) {

    var existingRow = $("input[class=diagnosis][value="+id+"]");

    if (existingRow.length) {
        alert("Такое заболевание уже обозначено");
        return;
    }

    var jqContainer = $("#diagnosis_container");

    var templData = {};
    templData.id = id;
    templData.name = name;

    jqContainer.append(Mustache.render(window.diagnosisTemplate, templData));
}

function processEtiology(id, name) {

    var existingRow = $("input[class=custom_etiology][value="+id+"]");

    if (existingRow.length) {
        alert("Такое заболевание уже обозначено");
        return;
    }

    var jQetiologycontainer = $("#etiology_container");

    var templData = {};
    templData.id = id;
    templData.name = name;

    jQetiologycontainer.append(Mustache.render(window.etiologyTemplate, templData));
}

/**
 * Обработка выбора лекарства из древовидного справочника
 * @returns {boolean}
 */
function processPharmSelected(id, name) {
    var jqPharmContainer = $("#otherPharmContainer");

    var templData = {};
    templData.pharmName = name;
    templData.pharmId = id;
    templData.measure = [];

    $.each(window.pharmUnits, function (i, el) {
        templData.measure[templData.measure.length] = {"id" : i, "val": el}
    });

    jqPharmContainer.append(Mustache.render(window.pharmProductTemplate, templData));

    return true;
}

function afterValidate(form) {

    //проверяем заполненность дозировки
    var doses = $(form).find("input.pharm_dose_input");
    var reg = /^\d+(,\d+)?$/i;

    var found = false;

    $.each(doses, function (i, el) {
        if (found) {
            return;
        }

        var val = $(el).val();
        if (val == "" || !reg.test(val)) {
            alert("Необходимо заполнить дозировку, например, 33.6");
            found = true;
            $('html, body').animate({
                scrollTop: $(el).offset().top
            }, 1000);
            $(el).focus();
        }
    });

    //проверяем заполненность даты операции
    var surgDates = $(form).find("input.surgery_date_input");

    $.each(surgDates, function (i, el) {
        if (found) {
            return;
        }

        var val = $(el).val();
        if (val == "") {
            alert("Необходимо заполнить дату операции");
            found = true;
            $('html, body').animate({
                scrollTop: $(el).offset().top
            }, 1000);
            $(el).focus();
        }
    });

    return !found;
}


function checkEpisodes(shortest, longest) {
    var prefix = window.fieldsPrefix;
    var shortestVal = parseInt($("#" + prefix + shortest).val());
    var longestVal = parseInt($("#" + prefix + longest).val());

    if (longestVal && shortestVal && (longestVal < shortestVal)) {
        alert("Вы уверены?");
    }
    return true;
}

function checkDevices(pacer, icd) {
        var prefix = window.fieldsPrefix;
    var pacerField = $("#" + prefix + pacer);
    var pacerChecked = pacerField.is(":checked");
    var icdField = $("#" + prefix + icd);
    var icdChecked = icdField.is(":checked");

    if (pacerChecked && icdChecked) {
        if (!confirm("Вы уверены в том, что пациенту одновременно были импланированы " + $('label[for="' + prefix + pacer +'"]').text() + " и " + $('label[for="' + prefix + icd +'"]').text() + "?")) {
            pacerField.attr('checked', false).trigger('change');
            icdField.attr('checked', false).trigger('change');
        }
    }

    return true;
}
