<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 09.01.14
 * Time: 13:43
 *
 * @var Protocol $protocol
 * @var Research $research
 * @var ProtocolFooterButtons $this
 */
?>

<div class="row buttons margin30 noprint">
    <div class="span6">
        <?php if ($protocol->isOpened() && Yii::app()->user->checkAccess("editProtocols")):?>
            <?php echo CHtml::submitButton(Yii::t('ProtocolsModule.protocols', "Сохранить")); ?>
        <?php endif;?>

        <?php echo CHtml::button(
            Yii::t('ProtocolsModule.protocols', "Отменить"),
            array("data-return_location" => $this->owner->createUrl($this->owner->id . "/index", array("id" => $this->patient->uid)),
                'class' => 'form_cancel_button'
            )
        ); ?>

        <?php if ($protocol->isOpened() && Yii::app()->user->checkAccess("signProtocols")):?>
            <?php echo CHtml::submitButton(Yii::t('ProtocolsModule.protocols', "Подписать"), array("name" => "sign")); ?>
        <?php elseif($research->isOpened() && Yii::app()->user->checkAccess("signProtocols")):?>
            <?php echo CHtml::button(
                Yii::t('ProtocolsModule.protocols', "Отменить подпись"),
                array("onclick" => 'window.location.href=\'' .
                    $this->owner->createUrl($this->owner->id . "/unsign", array("id" => $this->patient->uid, "protocolId" => $protocol->uid)) . '\'')
            ); ?>
        <?php endif;?>
    </div>
</div>