<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 11.11.13
 * Time: 17:32
 */

interface PrintableInterface {

    /**
     * Рендерит виджет в файл и возвращает строку со ссылкой на срендеренный файл
     *
     * @param $data
     * @return mixed
     */
    public function renderToFile($data);
} 