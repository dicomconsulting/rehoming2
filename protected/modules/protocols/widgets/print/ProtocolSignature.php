<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 13.11.13
 * Time: 10:50
 */

class ProtocolSignature extends CWidget{

    /**
     * @var Protocol
     */
    public $protocol;

    public function run()
    {
        $this->render("protocol-signature", array("protocol" => $this->protocol));
    }
}