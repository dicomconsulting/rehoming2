<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 11.11.13
 * Time: 17:03
 */

class ProtocolsFirstPageHeader extends CWidget {

    public $title;
    public $patient;
    public $protocolId;

    public function run()
    {
        $this->render("protocols-first-page-header", array("title" => $this->title));
    }
}