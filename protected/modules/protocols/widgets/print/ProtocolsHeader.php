<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 11.11.13
 * Time: 17:03
 */

class ProtocolsHeader extends CWidget {

    public $title;
    public $patient;
    public $protocolId;

    public function run()
    {
        $protocol = Protocol::model()->findByPk($this->protocolId);
        $this->render("protocols-header", array("title" => $this->title, "protocol" => $protocol));
    }
}