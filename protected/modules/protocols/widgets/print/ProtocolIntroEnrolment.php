<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 13.11.13
 * Time: 10:50
 */

class ProtocolIntroEnrolment extends CWidget{

    /**
     * @var Protocol
     */
    public $protocol;

    public function run()
    {
        $this->render("protocol-intro-enrolment", array("protocol" => $this->protocol));
    }
}