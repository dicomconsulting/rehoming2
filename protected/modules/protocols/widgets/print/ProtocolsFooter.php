<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 11.11.13
 * Time: 17:03
 */

class ProtocolsFooter extends CWidget {

    public $title;
    public $patient;
    public $protocolId;
    public $page;
    public $pageTotal;

    public function run()
    {
        $protocol = Protocol::model()->findByPk($this->protocolId);
        $this->render("protocols-footer", array("title" => $this->title, "protocol" => $protocol, "page" => $this->page, "pageTotal" => $this->pageTotal));
    }
}