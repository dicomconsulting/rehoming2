<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 13.11.13
 * Time: 10:56
 *
 * @var Protocol $protocol
 * @var ProtocolSignature $this
 */
?>
<div class="only_for_print no_page_break margin30">
    <div class="row">
        <div class="span8">
            <h4><?php echo Yii::t('ProtocolsModule.protocols', "Подпись исследователя") ?></h4>

            <p><?php echo Yii::t(
                    "protocols",
                    "Настоящим я подтверждаю достоверность информации, указанной выше в соответствии с моим знанием."
                ) ?></p>
        </div>
    </div>
    <div class="row">
        <div class="span1">
            <p><?php echo Yii::t('ProtocolsModule.protocols', "Дата") ?>:
        </div>
        <div class="span2">
            <?=$protocol->date?></p>
        </div>
        <div class="span5">
            <table style="width: 100%; height:50px;" class="only_for_print">
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr style="height: 15px; border-top: 1px #000000 solid;text-align: center">
                    <td><?php echo Yii::t('ProtocolsModule.protocols', "(подпись)") ?></td>
                    <td><?php echo Yii::t('ProtocolsModule.protocols', "(ФИО исследователя)") ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>