<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 11.11.13
 * Time: 17:04
 *
 * @var string $title
 * @var Protocol $protocol
 */
?>
<!DOCTYPE html>
<html lang="en"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" /></head>
<body style="margin: 0; padding: 0;">
<table style="border: 0; border-collapse: collapse; width: 100%; border-bottom: 1px #000000 solid;font-size: 14px;">
    <tr style="font-weight: bold">
        <td colspan="2">ReHoming</td>
        <td colspan="2" style="text-align: right;"><?=$title?></td>
    </tr>
    <tr style="text-align: center;">
        <td>код центра</td>
        <td>инициалы</td>
        <td>№ скрининга</td>
        <td>дата отчета</td>
    </tr>
    <tr style="text-align: center;">
        <td><?=$protocol->research->medicalCenter->code?></td>
        <td><?=$protocol->research->patient->getCode()?></td>
        <td><?=$protocol->research->getScreeningNumber()?></td>
        <td><?=$protocol->date?></td>
    </tr>
</table>
</body>
</html>