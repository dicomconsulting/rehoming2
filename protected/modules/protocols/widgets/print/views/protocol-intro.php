<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 13.11.13
 * Time: 10:56
 *
 * @var Protocol $protocol
 * @var ProtocolSignature $this
 */
?>
<div class="only_for_print no_page_break">
    <div class="row">
        <div class="span4">
            <h4><?php echo Yii::t('ProtocolsModule.protocols', "Код пациента") ?></h4>
        </div>
        <div class="span2 margin10 center">
            <?=$protocol->research->medicalCenter->code?><br/>
            <?php echo Yii::t('ProtocolsModule.protocols', "код центра") ?>

        </div>
        <div class="span2 margin10 center">
            <?=$protocol->research->patient->getCode()?><br/>
            <?php echo Yii::t('ProtocolsModule.protocols', "инициалы") ?>

        </div>
        <div class="span2 margin10 center">
            <?=$protocol->research->getScreeningNumber()?><br/>
            <?php echo Yii::t('ProtocolsModule.protocols', "номер отбора") ?>

        </div>
    </div>

    <div class="row">
        <div class="span3">
            <h4><?php echo Yii::t('ProtocolsModule.protocols', "Медицинский центр") ?></h4>
        </div>
        <div class="span5 margin10 center">
            <?=$protocol->research->medicalCenter->name?><br/>
        </div>
        <div class="span2 margin10 center">
            <?=$protocol->research->medicalCenter->code?><br/>
            <?php echo Yii::t('ProtocolsModule.protocols', "код центра") ?>
        </div>
    </div>
    <div class="row margin10">
        <div class="span2">
            <?php echo Yii::t('ProtocolsModule.protocols', "Исследователь") ?>:
        </div>
        <div class="span4">
            <?=$protocol->user->getDisplayName()?>
        </div>
        <div class="span3">
            <?php echo Yii::t('ProtocolsModule.protocols', "Дата отчета") ?>:
            <?=$protocol->date?>
        </div>
    </div>
    <hr style="width: 940px;" />
</div>
