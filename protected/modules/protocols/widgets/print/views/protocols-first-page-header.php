<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 11.11.13
 * Time: 17:04
 *
 * @var string $title
 */
?>
<!DOCTYPE html>
<html lang="en"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" /></head>
<body style="margin: 0; padding: 0;">
<table style="border: 0; border-collapse: collapse; width: 100%; border-bottom: 1px #000000 solid;">
    <tr>
        <td><span style="font-size: 14pt;font-weight: bold;">Исследование ReHoming</span> - <?=$title?></td>
        <td style="text-align: right;"><img src="<?=Yii::app()->request->getBaseUrl(true)?>/images/logo_vnoa.jpg" width="75" height="63"></td>
    </tr>
</table>
</body>
</html>