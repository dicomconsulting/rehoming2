<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 09.01.14
 * Time: 13:39
 */

class ProtocolFooterButtons extends CWidget
{
    /** @var Protocol */
    public $protocol;

    /** @var Research */
    public $research;

    /** @var Patient */
    public $patient;

    public function run()
    {
        $this->render(
            "protocol-footer-buttons",
            [
                'protocol' => $this->protocol,
                'research' => $this->research,
                'patient' => $this->patient,
            ]
        );
    }
}
