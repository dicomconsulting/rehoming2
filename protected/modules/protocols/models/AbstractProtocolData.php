<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 14.01.14
 * Time: 14:56
 */

abstract class AbstractProtocolData extends CActiveRecord
{
    /**
     * Прописывает в модели связь с внешними сущностями, неактуальные сущности удаляются
     *
     * @param $relName
     * @param $relPkName
     * @param $data
     */
    public function assignRelation($relName, $relPkName, $data)
    {
        $description = $this->relations()[$relName];
        $relsToSave = [];
        $relClassName = $description[1];

        /** @var CActiveRecord[] $existingRel */
        $existingRel = $this->getRelated($relName, true);

        foreach ($data as $rel) {
            $found = false;

            foreach ($existingRel as $exRel) {
                if ($exRel->{$relPkName} == $rel[$relPkName]) {
                    $exRel->attributes = $rel;
                    $relsToSave[] = $exRel;
                    $found = true;
                }
            }

            if (!$found) {
                $newRelObj = new $relClassName();
                $newRelObj->attributes = $rel;
                $relsToSave[] = $newRelObj;
            }
        }

        foreach ($existingRel as $exRel) {
            $found = false;

            foreach ($data as $rel) {
                if ($exRel->{$relPkName} == $rel[$relPkName]) {
                    $found = true;
                }
            }

            if (!$found) {
                $exRel->delete();
            }
        }

        $this->{$relName} = $relsToSave;
    }
}
