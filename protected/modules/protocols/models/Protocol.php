<?php

/**
 * This is the model class for table "{{protocol}}".
 *
 * The followings are the available columns in table '{{protocol}}':
 * @property integer $uid
 * @property RhDateTime $date
 * @property integer $protocol_dictionary_uid
 * @property integer $research_uid
 * @property integer $user_uid
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property ProtocolDictionary $protocolDictionary
 * @property User $user
 * @property ProtocolEnrolment $protocolEnrolment
 * @property ProtocolInithm $protocolInithm
 * @property ProtocolEvolution $protocolEvolution
 * @property ProtocolAnnual $protocolAnnual
 * @property ProtocolAdverse $protocolAdverse
 * @property ProtocolFinish $protocolFinish
 * @property ProtocolRegular $protocolRegular
 * @property Research $research
 */
class Protocol extends CActiveRecord
{
    const STATUS_OPENED = 0;
    const STATUS_CLOSED = 1;

    protected $statusNames = array(
        0 => "Рабочий",
        1 => "Закрыт",
    );

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{protocol}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('date, protocol_dictionary_uid, research_uid, user_uid', 'required'),
            array('protocol_dictionary_uid, research_uid, user_uid', 'numerical', 'integerOnly' => true),
        );
    }

    public function init()
    {
        if ($this->scenario == "insert") {
            //при создании нового исследования
            $this->date = date("Y-m-d");
        }

        parent::init();
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'protocolDictionary' => array(self::BELONGS_TO, 'ProtocolDictionary', 'protocol_dictionary_uid'),
            'user' => array(self::BELONGS_TO, 'User', 'user_uid'),
            'research' => array(self::BELONGS_TO, 'Research', 'research_uid'),
            'protocolEnrolment' => array(self::HAS_ONE, 'ProtocolEnrolment', 'protocol_uid'),
            'protocolInithm' => array(self::HAS_ONE, 'ProtocolInithm', 'protocol_uid'),
            'protocolEvolution' => array(self::HAS_ONE, 'ProtocolEvolution', 'protocol_uid'),
            'protocolAnnual' => array(self::HAS_ONE, 'ProtocolAnnual', 'protocol_uid'),
            'protocolAdverse' => array(self::HAS_ONE, 'ProtocolAdverse', 'protocol_uid'),
            'protocolFinish' => array(self::HAS_ONE, 'ProtocolFinish', 'protocol_uid'),
            'protocolRegular' => array(self::HAS_ONE, 'ProtocolRegular', 'protocol_uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid' => Yii::t('ProtocolsModule.protocols', 'Uid'),
            'date' => Yii::t('ProtocolsModule.protocols', 'Дата создания протокола'),
            'protocol_dictionary_uid' => Yii::t('ProtocolsModule.protocols', 'Ссылка на тип протокола'),
            'research_uid' => Yii::t('ProtocolsModule.protocols', 'Ссылка на исследование'),
            'user_uid' => Yii::t('ProtocolsModule.protocols', 'Исследователь'),
            'status' => Yii::t('ProtocolsModule.protocols', 'Статус протокола'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Protocol the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        return array(
            'RhDateTimeBehavior' => array(
                'class' => 'application.extensions.RhDateTimeBehavior'
            ),
            'LoggingBehavior' => array(
                'class' => 'protocols.components.ProtocolLoggingBehavior'
            ),
            'UserGroupSeparation' => array(
                'class' => 'admin.components.UserGroupSeparationBehavior'
            )
        );
    }

    protected function afterFind()
    {
        parent::afterFind();

        if ($this->scenario == "update" && $this->date instanceof RhDateTime) {
            $this->date->setDefaultOutputFormat("Y-m-d");
        }
    }


    /**
     * Возвращает отформатированный список НЯ для формы, исключая те НЯ которые уже привязаны к протоколу
     *
     * @param CActiveRecord $model
     * @param $findings
     * @return array
     */
    public static function filterAndFormatFindingList($model, $findings)
    {
        $list = array();

        $attachedFindings = array();

        if ($model->finding) {
            foreach ($model->finding as $rel) {
                $attachedFindings[] = $rel->finding_uid;
            }
        }

        foreach ($findings as $finding) {
            /** @var Message $finding */

            if (!in_array($finding->getUid(), $attachedFindings)) {
                $list[$finding->getUid()] = $finding->data->date_create . " " . $finding->formatMessage(true);
            }
        }

        return $list;
    }

    /**
     * На какой статус было переведен статус событий
     *
     * @return array
     */
    public static function getFromRedOptions()
    {
        return array(
            0 => Yii::t('ProtocolsModule.protocols', "зеленый"),
            1 => Yii::t('ProtocolsModule.protocols', "желтый"),
        );
    }

    /**
     * Возвращает список протоколов по пациенту определенного типа
     *
     * @param $type
     * @param Patient $patient
     * @return Protocol[]
     */
    public static function getProtocolsByTypeAndPatient($type, Patient $patient)
    {
        $criteria = new CDbCriteria();
        $criteria->order = "t.date DESC";
        $criteria->condition = "protocol_dictionary_uid = :dict_uid AND patient.uid = :patient_uid";
        $criteria->params = array(":dict_uid" => $type, ":patient_uid" => $patient->uid);

        $name = ProtocolDictionary::model()->findByPk($type)->getProtocolName();

        $protocols = Protocol::model()
            ->with(
                array(
                    "research" => array(
                        "with" => array("patient"),
                    ),
                    $name,
                    "user"
                )
            )
            ->findAll($criteria);

        return $protocols;
    }

    /**
     * Возвращает статус протокола
     * true - значит протокол находится в работе
     * false - протокол закрыт и не может больше изменяться
     *
     * @return bool
     */
    public function isOpened()
    {
        return !$this->status;
    }

    public function getStatusName()
    {
        return $this->statusNames[$this->status];
    }

    protected function beforeDelete()
    {
        if (!$this->isOpened()) {
            //удалять можно только рабочие протоколы
            return false;
        }
        return parent::beforeDelete();
    }
}
