<?php
Yii::import('protocols.models.*');

/**
 * This is the model class for table "{{protocol_history}}".
 *
 * The followings are the available columns in table '{{protocol_history}}':
 * @property integer $uid
 * @property integer $research_id
 * @property integer $protocol_id
 * @property integer $protocol_dictionary_id
 * @property integer $operation_id
 * @property RhDateTime $event_time
 * @property integer $user_name
 * @property integer $archive
 * @property Research $research
 * @property ProtocolDictionary $protocolDictionary
 * @property Protocol $protocol
 * @property User $user
 */
class ProtocolHistory extends CActiveRecord
{

    const EVENT_CREATED = 1;
    const EVENT_UPDATED = 2;
    const EVENT_SIGNED = 3;
    const EVENT_UNSIGNED = 4;
    const EVENT_DELETED = 5;

    /**
     * Добавляет событие в историю
     *
     * @param integer $event
     * @param Protocol $protocol
     * @param RhDateTime $eventTime
     * @param integer $userId
     */
    public static function addEvent ($event, Protocol $protocol, RhDateTime $eventTime, $userId)
    {
        $className = __CLASS__;

        /** @var ProtocolHistory $logger */
        $logger = new $className;

        $logger->operation_id = $event;
        $logger->research_id = $protocol->research_uid;
        $logger->protocol_id = $protocol->uid;
        $logger->protocol_dictionary_id = $protocol->protocol_dictionary_uid;
        $logger->user_name = $userId;
        $logger->event_time = $eventTime;
        $logger->save();
    }

    public function getEvents()
    {
        return [
            1 => Yii::t("ProtocolsModule.protocols", "Протокол создан"),
            2 => Yii::t("ProtocolsModule.protocols", "Протокол обновлен"),
            3 => Yii::t("ProtocolsModule.protocols", "Протокол подписан"),
            4 => Yii::t("ProtocolsModule.protocols", "Подпись отменена"),
            5 => Yii::t("ProtocolsModule.protocols", "Протокол удален"),
        ];
    }

    public function getEventLabel($event)
    {
        return $this->getEvents()[$event];
    }

    public function archiveEvents($protocolId)
    {
        $this->updateAll(['archive' => 1], "protocol_id = :protocol_uid", [":protocol_uid" => $protocolId]);
    }

    public static function getHistoryByProtocol($protocolId)
    {
        $criteria = new CDbCriteria();
        $criteria->order = "uid DESC";

        return self::model()->findAllByAttributes(["protocol_id" => $protocolId], $criteria);
    }

    public static function getHistoryByResearch($researchId)
    {
        $criteria = new CDbCriteria();
        $criteria->order = "uid DESC";

        return self::model()->findAllByAttributes(["research_id" => $researchId], $criteria);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{protocol_history}}';
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'research' => array(self::BELONGS_TO, 'Research', 'research_id'),
            'protocolDictionary' => array(self::BELONGS_TO, 'ProtocolDictionary', 'protocol_dictionary_id'),
            'protocol' => array(self::BELONGS_TO, 'Protocol', 'protocol_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_name'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid' => Yii::t('ProtocolsModule.protocols', 'Uid'),
            'research_id' => Yii::t('ProtocolsModule.protocols', 'Ссылка на исследование'),
            'protocol_id' => Yii::t('ProtocolsModule.protocols', 'Ссылка на протокол'),
            'protocol_dictionary_id' => Yii::t('ProtocolsModule.protocols', 'Ссылка на тип протокола'),
            'operation_id' => Yii::t('ProtocolsModule.protocols', 'Операция'),
            'event_time' => Yii::t('ProtocolsModule.protocols', 'Дата внесения изменений'),
            'user_name' => Yii::t('ProtocolsModule.protocols', 'Имя пользователя'),
            'archive' => Yii::t('ProtocolsModule.protocols', 'Статус протокола'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ProtocolHistory the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        return array(
            'RhDateTimeBehavior' => array(
                'class' => 'application.extensions.RhDateTimeBehavior'
            ),
        );
    }
}
