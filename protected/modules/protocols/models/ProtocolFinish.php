<?php

/**
 * This is the model class for table "{{protocol_finish}}".
 *
 * The followings are the available columns in table '{{protocol_finish}}':
 * @property integer $protocol_uid
 * @property integer $regular
 * @property integer $cause
 * @property string $comment
 * @property string $last_survey_date
 * @property string $general_comment
 *
 * The followings are the available model relations:
 * @property Protocol $protocol
 */
class ProtocolFinish extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{protocol_finish}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('protocol_uid, last_survey_date', 'required'),
            array('protocol_uid, regular, cause', 'numerical', 'integerOnly' => true),
            array('comment, last_survey_date, general_comment', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'protocol' => array(self::BELONGS_TO, 'Protocol', 'protocol_uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'protocol_uid' => Yii::t('ProtocolsModule.protocols', 'Протокол Uid'),
            'regular' => Yii::t('ProtocolsModule.protocols', 'Регулярное окончание исследования'),
            'cause' => Yii::t('ProtocolsModule.protocols', 'Причина окончания исследования'),
            'comment' => Yii::t('ProtocolsModule.protocols', 'Примечание'),
            'last_survey_date' => Yii::t('ProtocolsModule.protocols', 'Дата последнего обследования'),
            'general_comment' => Yii::t('ProtocolsModule.protocols', 'Примечание'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ProtocolFinish the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Возвращает список вариантов причин завершения обследования
     *
     * @return array
     */
    public function getFinishCause()
    {
        return array(
            0 => Yii::t('ProtocolsModule.protocols', "потеряна связь с пациентом"),
            1 => Yii::t('ProtocolsModule.protocols', "пациент отозвал свое согласие"),
            2 => Yii::t('ProtocolsModule.protocols', "эксплантация прибора"),
            3 => Yii::t('ProtocolsModule.protocols', "пациент умер"),
        );
    }

    public function behaviors()
    {
        return array(
            'NullableAttrsBehavior' => array(
                'class' => 'NullableAttrsBehavior'
            ),
            'LoggingBehavior' => array(
                'class' => 'protocols.components.ProtocolDataLoggingBehavior'
            ),
        );
    }
}
