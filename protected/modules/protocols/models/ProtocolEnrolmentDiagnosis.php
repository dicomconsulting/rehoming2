<?php

/**
 * This is the model class for table "{{protocol_enrolment_diagnosis}}".
 *
 * The followings are the available columns in table '{{protocol_enrolment_diagnosis}}':
 * @property integer $protocol_uid
 * @property integer $icd10_uid
 */
class ProtocolEnrolmentDiagnosis extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ProtocolEnrolmentDiagnosis the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{protocol_enrolment_diagnosis}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('protocol_uid, icd10_uid', 'required'),
            array('protocol_uid, icd10_uid', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('protocol_uid, icd10_uid', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'protocol_uid' => 'Protocol Uid',
            'icd10_uid' => 'Icd10 Uid',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('protocol_uid', $this->protocol_uid);
        $criteria->compare('icd10_uid', $this->icd10_uid);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}
