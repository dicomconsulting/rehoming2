<?php

/**
 * This is the model class for table "{{protocol_dictionary_i18n}}".
 *
 * The followings are the available columns in table '{{protocol_dictionary_i18n}}':
 * @property string $name
 * @property integer $uid
 * @property string $locale
 *
 * The followings are the available model relations:
 * @property ProtocolDictionary $u
 */
class ProtocolDictionaryI18n extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{protocol_dictionary_i18n}}';
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'u' => array(self::BELONGS_TO, 'ProtocolDictionary', 'uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'name' => 'Name',
            'uid' => 'Uid',
            'locale' => 'Locale',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ProtocolDictionaryI18n the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
