<?php

/**
 * This is the model class for table "{{protocol_regular}}".
 *
 * The followings are the available columns in table '{{protocol_regular}}':
 * @property integer $protocol_uid
 * @property integer $survey_period
 * @property integer $visit_deflection
 * @property integer $visit_missed
 * @property integer $clinic_distance
 * @property integer $clinic_time
 * @property string $stimul_mode
 * @property integer $idc_vt1
 * @property string $idc_vt1_time
 * @property integer $idc_vt2
 * @property string $idc_vt2_time
 * @property integer $idc_vf
 * @property string $idc_vf_time
 * @property string $ae_pacing_threshold
 * @property string $ae_pacing_threshold_dur
 * @property string $ae_pulse_ampl
 * @property integer $ae_pacing_imp
 * @property string $ve_stimul_threshold
 * @property string $ve_stimul_dur
 * @property string $ve_wave_apl
 * @property integer $ve_imp
 * @property integer $ve_imp_shock
 * @property integer $arr_vt1_cnt
 * @property integer $arr_vt2_cnt
 * @property integer $arr_vf_cnt
 * @property integer $atc_began_vt
 * @property integer $atc_compl_vt
 * @property integer $atc_began_vf
 * @property integer $atc_compl_vf
 * @property integer $shock_began
 * @property integer $shock_cancel
 * @property integer $shock_compl
 * @property integer $svt_total_cnt
 * @property integer $svt_12h_cnt
 * @property DateInterval $af_episode_time
 * @property integer $af_episode_hr_max
 * @property integer $af_episode_hr_avg
 * @property integer $vent_contr_prop
 * @property integer $vent_rate_24
 * @property integer $vent_rate_fu
 * @property integer $vent_rate_24_rest
 * @property integer $vent_rate_fu_rest
 * @property integer $patient_act
 * @property integer $patient_act_fu
 * @property integer $vent_contr_24
 * @property integer $vent_contr_fu
 * @property integer $ekg_needed
 * @property integer $ekg_supplied
 * @property integer $program_change_needed
 * @property integer $drugs_change_needed
 * @property string $program_changes
 * @property string $elect_displ_detect_date
 * @property string $elect_displ_hosp_date
 * @property integer $elect_displ_hosp_emerg
 * @property string $hf_detect_date
 * @property string $hf_hosp_date
 * @property integer $hf_hosp_emerg
 * @property string $idc_fail_detect_date
 * @property string $idc_fail_hosp_date
 * @property integer $idc_fail_hosp_emerg
 * @property integer $option_changed
 * @property string $option_changed_new_value
 * @property string $option_changed_reason
 * @property integer $red_finding
 * @property string $red_finding_date
 * @property string $red_finding_message
 * @property integer $red_finding_changed
 * @property string $red_finding_changed_date
 * @property integer $red_finding_changed_to
 * @property string $red_finding_changed_message
 * @property string $red_finding_changed_comment
 * @property string $comment
 *
 * The followings are the available model relations:
 * @property Protocol $protocol
 * @property ProtocolFindingRelation[] $finding
 */
class ProtocolRegular extends AbstractProtocolData implements
    ProtocolFillableInterface,
    FieldDependencyDescriptorInterface
{

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ProtocolRegular the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{protocol_regular}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('protocol_uid', 'required'),
            array(
                'protocol_uid, survey_period, visit_deflection, visit_missed, idc_vt1, idc_vt2, idc_vf, ae_pacing_imp,
                 ve_imp, ve_imp_shock, arr_vt1_cnt, arr_vt2_cnt, arr_vf_cnt, atc_began_vt, atc_compl_vt, atc_began_vf' .
                ', atc_compl_vf,shock_began, shock_cancel, shock_compl, svt_total_cnt, svt_12h_cnt,af_episode_hr_max,' .
                'af_episode_hr_avg, vent_contr_prop, vent_rate_24, vent_rate_fu, vent_rate_24_rest, vent_rate_fu_rest' .
                ', patient_act, patient_act_fu, vent_contr_24, vent_contr_fu, ekg_needed, ekg_supplied, ' .
                'program_change_needed, drugs_change_needed, elect_displ_hosp_emerg, hf_hosp_emerg, ' .
                'idc_fail_hosp_emerg, option_changed, red_finding, red_finding_changed, red_finding_changed_to,' .
                'idc_vt1_time, idc_vt2_time, idc_vf_time',
                'numerical',
                'integerOnly' => true
            ),
            array('stimul_mode', 'length', 'max' => 255),
            array(
                'ae_pacing_threshold, ae_pacing_threshold_dur, ae_pulse_ampl, ve_stimul_threshold, ' .
                've_stimul_dur, ve_wave_apl',
                'length',
                'max' => 10
            ),
            array(
                'af_episode_time, program_changes, elect_displ_detect_date, elect_displ_hosp_date, hf_detect_date,' .
                ' hf_hosp_date, idc_fail_detect_date, idc_fail_hosp_date, option_changed_new_value, ' .
                'option_changed_reason, red_finding_date, red_finding_message, red_finding_changed_date, ' .
                'red_finding_changed_message, red_finding_changed_comment, finding,comment',
                'safe'
            ),
            array(
                'clinic_distance,clinic_time,idc_vt1_time,idc_vt2_time,idc_vf_time,ae_pacing_threshold,' .
                'ae_pacing_threshold_dur,ae_pulse_ampl,ve_stimul_threshold,ve_stimul_dur,ve_wave_apl',
                'CustomFloatValidator',
                'message' => Yii::t(
                    'ProtocolsModule.protocols',
                    'Поле "{attribute}" должно быть заполнено числом, например "36,6"'
                )
            ),
            array(
                'option_changed_new_value,option_changed_reason,red_finding_date,red_finding_message,' .
                'red_finding_changed_date,red_finding_changed_message',
                'ProtocolsDependencyValidator',
                'message' => Yii::t('ProtocolsModule.protocols', 'Поле "{attribute}" должно быть заполнено')
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'protocol' => array(self::BELONGS_TO, 'Protocol', 'protocol_uid'),
            'finding'  => array(self::HAS_MANY, 'ProtocolFindingRelation', array('protocol_uid' => 'protocol_uid')),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'protocol_uid'                => Yii::t('ProtocolsModule.protocols', 'Протокол Uid'),
            'survey_period'               => Yii::t('ProtocolsModule.protocols', 'Обследование через'),
            'visit_deflection'            => Yii::t('ProtocolsModule.protocols', 'Отклонение даты от расписания, дней (+/-)'),
            'visit_missed'                => Yii::t('ProtocolsModule.protocols', 'Пропущен визит, месяцев'),
            'clinic_distance'             => Yii::t('ProtocolsModule.protocols', 'Расстояние (км)'),
            'clinic_time'                 => Yii::t('ProtocolsModule.protocols', 'Время в пути (часы:минуты)'),
            'stimul_mode'                 => Yii::t('ProtocolsModule.protocols', 'Режим стимуляции'),
            'idc_vt1'                     => Yii::t('ProtocolsModule.protocols', 'ЖТ1'),
            'idc_vt1_time'                => Yii::t('ProtocolsModule.protocols', 'время VT1'),
            'idc_vt2'                     => Yii::t('ProtocolsModule.protocols', 'ЖТ2'),
            'idc_vt2_time'                => Yii::t('ProtocolsModule.protocols', 'время VT2'),
            'idc_vf'                      => Yii::t('ProtocolsModule.protocols', 'ФЖ'),
            'idc_vf_time'                 => Yii::t('ProtocolsModule.protocols', 'время VF'),
            'ae_pacing_threshold'         => Yii::t('ProtocolsModule.protocols', 'Порог стимуляции (В)'),
            'ae_pacing_threshold_dur'     => Yii::t('ProtocolsModule.protocols', 'Длительность импульса (мс)'),
            'ae_pulse_ampl'               => Yii::t('ProtocolsModule.protocols', 'Амплитуда волны P (мВ)'),
            'ae_pacing_imp'               => Yii::t('ProtocolsModule.protocols', 'Импеданс (Ом)'),
            've_stimul_threshold'         => Yii::t('ProtocolsModule.protocols', 'Порог стимуляции (В)'),
            've_stimul_dur'               => Yii::t('ProtocolsModule.protocols', 'Длительность стимула (мс)'),
            've_wave_apl'                 => Yii::t('ProtocolsModule.protocols', 'Амплитуда волны R (мВ)'),
            've_imp'                      => Yii::t('ProtocolsModule.protocols', 'Импеданс (Ом)'),
            've_imp_shock'                => Yii::t('ProtocolsModule.protocols', 'Импеданс шоковый (Ом)'),
            'arr_vt1_cnt'                 => Yii::t('ProtocolsModule.protocols', 'ЖТ1'),
            'arr_vt2_cnt'                 => Yii::t('ProtocolsModule.protocols', 'ЖТ2'),
            'arr_vf_cnt'                  => Yii::t('ProtocolsModule.protocols', 'ФЖ'),
            'atc_began_vt'                => Yii::t('ProtocolsModule.protocols', 'АТС начато в зонах ЖТ1/ЖТ2'),
            'atc_compl_vt'                => Yii::t('ProtocolsModule.protocols', 'АТС успешно в зонах ЖТ1/ЖТ2'),
            'atc_began_vf'                => Yii::t('ProtocolsModule.protocols', 'АТС начато в зоне ФЖ'),
            'atc_compl_vf'                => Yii::t('ProtocolsModule.protocols', 'АТС успешно в зоне ФЖ'),
            'shock_began'                 => Yii::t('ProtocolsModule.protocols', 'Шоков начато'),
            'shock_cancel'                => Yii::t('ProtocolsModule.protocols', 'Шоков отменено'),
            'shock_compl'                 => Yii::t('ProtocolsModule.protocols', 'Шоков успешно'),
            'svt_total_cnt'               => Yii::t('ProtocolsModule.protocols', 'Количество эпизодов НЖТ'),
            'svt_12h_cnt'                 => Yii::t('ProtocolsModule.protocols', 'длительностью более 12 часов'),
            'af_episode_time'             => Yii::t('ProtocolsModule.protocols', 'Продолжительность самого длинного эпизода ФП'),
            'af_episode_hr_max'           => Yii::t('ProtocolsModule.protocols', 'Максимальная ЧСС при ФП (ударов в минуту)'),
            'af_episode_hr_avg'           => Yii::t('ProtocolsModule.protocols', 'Средняя ЧСС при ФП (ударов в минуту)'),
            'vent_contr_prop'             => Yii::t(
                'ProtocolsModule.protocols',
                'Доля стимулиролированных сокращений желудочков (Vp) (%)'
            ),
            'vent_rate_24'                => Yii::t('ProtocolsModule.protocols', 'за 24 часа'),
            'vent_rate_fu'                => Yii::t('ProtocolsModule.protocols', 'с последнего FU'),
            'vent_rate_24_rest'           => Yii::t('ProtocolsModule.protocols', 'за 24 часа'),
            'vent_rate_fu_rest'           => Yii::t('ProtocolsModule.protocols', 'с последнего FU'),
            'patient_act'                 => Yii::t('ProtocolsModule.protocols', 'за 24 часа'),
            'patient_act_fu'              => Yii::t('ProtocolsModule.protocols', 'с последнего FU'),
            'vent_contr_24'               => Yii::t('ProtocolsModule.protocols', 'за 24 часа'),
            'vent_contr_fu'               => Yii::t('ProtocolsModule.protocols', 'с последнего FU'),
            'ekg_needed'                  => Yii::t(
                'ProtocolsModule.protocols',
                'Необходимо обследование пациента методом эхокардиографии?'
            ),
            'ekg_supplied'                => Yii::t(
                'ProtocolsModule.protocols',
                'Протокол эхокардиографического обследования прилагается?'
            ),
            'program_change_needed'       => Yii::t(
                'ProtocolsModule.protocols',
                'Необходимо изменение в программе имплантата?'
            ),
            'drugs_change_needed'         => Yii::t(
                'ProtocolsModule.protocols',
                'Необходимо изменение в медикаментозной антиаритмической терапии?'
            ),
            'program_changes'             => Yii::t(
                'ProtocolsModule.protocols',
                'Какие изменения были внесены в программу аппарата?'
            ),
            'elect_displ_detect_date'     => Yii::t('ProtocolsModule.protocols', 'Дата выявления'),
            'elect_displ_hosp_date'       => Yii::t('ProtocolsModule.protocols', 'Дата госпитализации'),
            'elect_displ_hosp_emerg'      => Yii::t('ProtocolsModule.protocols', 'Экстренность госпитализации'),
            'hf_detect_date'              => Yii::t('ProtocolsModule.protocols', 'Дата выявления'),
            'hf_hosp_date'                => Yii::t('ProtocolsModule.protocols', 'Дата госпитализации'),
            'hf_hosp_emerg'               => Yii::t('ProtocolsModule.protocols', 'Экстренность госпитализации'),
            'idc_fail_detect_date'        => Yii::t('ProtocolsModule.protocols', 'Дата выявления'),
            'idc_fail_hosp_date'          => Yii::t('ProtocolsModule.protocols', 'Дата осмотра'),
            'idc_fail_hosp_emerg'         => Yii::t('ProtocolsModule.protocols', 'Экстренность осмотра'),
            'option_changed'              => Yii::t(
                'ProtocolsModule.protocols',
                'Опция Home Monitoring изменена со времени включения'
            ),
            'option_changed_new_value'    => Yii::t('ProtocolsModule.protocols', 'Укажите новую опцию'),
            'option_changed_reason'       => Yii::t('ProtocolsModule.protocols', 'Причина изменения'),
            'red_finding'                 => Yii::t(
                'ProtocolsModule.protocols',
                'Статус сообщения Home Monitoring изменен на “red” (красный)'
            ),
            'red_finding_date'            => Yii::t('ProtocolsModule.protocols', 'Дата изменения'),
            'red_finding_message'         => Yii::t('ProtocolsModule.protocols', 'Сообщение'),
            'red_finding_changed'         => Yii::t(
                'ProtocolsModule.protocols',
                'Статус сообщения Home Monitoring изменен с “red” (красный) на другой'
            ),
            'red_finding_changed_date'    => Yii::t('ProtocolsModule.protocols', 'Дата изменения'),
            'red_finding_changed_to'      => Yii::t('ProtocolsModule.protocols', 'На какой цвет'),
            'red_finding_changed_message' => Yii::t('ProtocolsModule.protocols', 'Сообщение'),
            'red_finding_changed_comment' => Yii::t('ProtocolsModule.protocols', 'Примечание'),
            'comment'                     => Yii::t('ProtocolsModule.protocols', 'Комментарий\Примечание'),
        );
    }

    /**
     * Возвращает массив возможных значений периода обследования
     *
     * @return array
     */
    public function getSurveyPeriods()
    {
        return array(
            "3" => Yii::t('ProtocolsModule.protocols', "3 месяца"),
            "6" => Yii::t('ProtocolsModule.protocols', "6 месяцев"),
            "9" => Yii::t('ProtocolsModule.protocols', "9 месяцев"),
        );
    }

    public function afterFind()
    {
        $this->af_episode_time = !empty($this->af_episode_time)
            ? DateIntervalHelper::createDateIntervalFromSeconds($this->af_episode_time)
            : null;

        parent::afterFind();
    }

    public function getFillingMap()
    {
        return [
            'vent_contr_24'     => [
                'param'     => 'mean_pvc_h',
                'transform' => 'intFilter',
            ],
            'vent_contr_fu'     => [
                'param'     => 'mean_pvc_h',
                'transform' => 'intFilter',
                'statValue' => 'meanByCountValue'
            ],
            'af_episode_time'   => [
                'param'     => 'episode_duration',
                'statValue' => 'maxAllTime'
            ],
            'vent_rate_24'      => [
                'param'     => 'mean_ventricular_heart_rate',
                'transform' => 'integer'
            ],
            'vent_rate_fu'      => [
                'param'     => 'mean_ventricular_heart_rate',
                'transform' => 'integer',
                'statValue' => 'meanFu',
            ],
            'vent_rate_24_rest' => [
                'param'     => 'mean_ventricular_heart_rate_at_rest',
                'transform' => 'integer',
            ],
            'vent_rate_fu_rest' => [
                'param'     => 'mean_ventricular_heart_rate_at_rest',
                'transform' => 'integer',
                'statValue' => 'meanFu',
            ],
            'patient_act'       => [
                'param'     => 'patient_activity',
                'transform' => 'integer',
            ],
            'patient_act_fu'    => [
                'param'     => 'patient_activity',
                'transform' => 'integer',
                'statValue' => 'meanFu',
            ],
            'arr_vt1_cnt'       => [
                'param'     => 'vt1_episodes',
                'transform' => 'roundInteger',
            ],
            'arr_vt2_cnt'       => [
                'param'     => 'vt2_episodes',
                'transform' => 'roundInteger',
            ],
            'arr_vf_cnt'        => [
                'param'     => 'vf_episodes',
                'transform' => 'roundInteger',
            ],
            'atc_began_vt'      => [
                'param'     => 'atp_in_vt_zones_started',
                'transform' => 'roundInteger',
            ],
            'atc_compl_vt'      => [
                'param'     => 'atp_in_vt_zones_successful',
                'transform' => 'roundInteger',
            ],
            'atc_began_vf'      => [
                'param'     => 'atp_one_shot_started',
                'statValue' => 'meanByCountValue',
                'transform' => 'roundInteger',
            ],
            'atc_compl_vf'      => [
                'param'     => 'atp_one_shot_successful',
                'statValue' => 'meanByCountValue',
                'transform' => 'roundInteger',
            ],
            'shock_began'       => [
                'param'     => 'shocks_started',
                'transform' => 'roundInteger',
            ],
            'shock_cancel'      => [
                'param'     => 'shocks_aborted',
                'transform' => 'roundInteger',
            ],
            'shock_compl'       => [
                'param'     => 'shocks_successful',
                'transform' => 'roundInteger',
            ],
            'svt_total_cnt'     => [
                'param'     => 'svt_episodes_total',
                'transform' => 'roundInteger',
            ],
            'svt_12h_cnt'       => [
                'param'  => 'Finding "Long atrial episode detected" initially detected',
                'source' => 'historyMessage',
                'filter' => 'isPermanentFormPatient'
            ],
            'af_episode_hr_max' => [
                'param'     => 'max_ven_heart_rate_during_atr_burden',
                'statValue' => 'maxFu',
            ],
            'af_episode_hr_avg' => [
                'param'     => 'mean_ven_heart_rate_during_atr_burden',
                'statValue' => 'maxFu',
            ],
            'vent_contr_prop'   => [
                'param'  => 'vent_contr_prop',
                'source' => 'protocolRegularParameter',
                'transform' => 'roundInteger',
            ],
            'stimul_mode'       => [
                'param' => 'mode',
            ],
            'idc_vt1_time'      => [
                'param' => 'vt1_zone_limit',
            ],
            'idc_vt2_time'      => [
                'param' => 'vt2_zone_limit' , 'transform' => 'roundInteger'
            ],
            'idc_vf_time'       => [
                'param' => 'vf_zone_limit',
            ]
        ];
    }

    public function init()
    {
        parent::init();
        if ($this->scenario == 'insert') {
            $this->af_episode_time = new DateInterval('P0000-00-00T00:00:00');
            $this->idc_vt1         = 1;
            $this->idc_vt2         = 1;
            $this->idc_vf          = 1;
        }
    }

    /**
     * Возвращает массив с полями, возможность заполнения которых зависит от поставленного чекбокса
     * Ключ - название поля-чекбокса
     * Значение - массив названий зависимых полей
     *
     * @return array
     */
    public function getFieldsDependency()
    {
        return [
            'idc_vt1'             => ['idc_vt1_time'],
            'idc_vt2'             => ['idc_vt2_time'],
            'idc_vf'              => ['idc_vf_time'],
            'option_changed'      => ['option_changed_new_value', 'option_changed_reason'],
            'red_finding'         => ['red_finding_date', 'red_finding_message'],
            'red_finding_changed' => [
                'red_finding_changed_date',
                'red_finding_changed_to',
                'red_finding_changed_message',
                'red_finding_changed_comment'
            ],
        ];
    }

    public function behaviors()
    {
        return array(
            'NullableAttrsBehavior'  => array(
                'class' => 'NullableAttrsBehavior'
            ),
            'FloatDelimiterBehavior' => ['class' => 'FloatDelimiterBehavior'],
            'EAdvancedArBehavior'    => array(
                'class' => 'application.extensions.EAdvancedArBehavior'
            ),
            'LoggingBehavior'        => array(
                'class' => 'protocols.components.ProtocolDataLoggingBehavior'
            ),
        );
    }

    /**
     * Превращаем пустые строки в null
     *
     * @return bool
     */
    protected function beforeSave()
    {
        $this->af_episode_time = $this->af_episode_time
            ? DateIntervalHelper::toSeconds($this->af_episode_time)
            : null;

        return parent::beforeSave();
    }
}
