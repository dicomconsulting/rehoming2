<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 29.10.13
 * Time: 14:08
 */

interface ProtocolFillableInterface
{

    public function getFillingMap();
}
