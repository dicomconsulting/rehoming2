<?php

Yii::import("device.models.Transmitter");

/**
 * This is the model class for table "{{protocol_enrolment}}".
 *
 * The followings are the available columns in table '{{protocol_enrolment}}':
 * @property integer $protocol_uid
 * @property integer $patient_height
 * @property string $patient_weight
 * @property string $patient_nyha
 * @property integer $enrolment_pacer
 * @property integer $enrolment_crt_p
 * @property integer $enrolment_crt_d
 * @property integer $enrolment_icd
 * @property integer $enrolment_able
 * @property integer $enrolment_agree
 * @property integer $enrolment_gsm
 * @property string $visit_date
 * @property Icd10[] $diagnosis
 * @property string $diagnosis_comment
 * @property integer $ca_block
 * @property integer $ca_block_grade
 * @property integer $ca_pause
 * @property string $ca_pause_duration
 * @property integer $atrial_arr
 * @property integer $atrial_tachy
 * @property integer $atrial_fibrillation
 * @property integer $atrial_flutter
 * @property integer $tachy_history_duration
 * @property string $tachy_latest_date
 * @property integer $tachy_longest_episode
 * @property integer $tachy_shortest_episode
 * @property integer $tachy_frequency
 * @property string $arr_cupped
 * @property integer $ab_block
 * @property integer $ab_block_grade
 * @property integer $ab_pause
 * @property string $ab_pause_duration
 * @property integer $left_branch_block
 * @property integer $right_branch_block
 * @property integer $both_branch_block
 * @property integer $qrs_duration
 * @property integer $vent_arr
 * @property integer $vent_fibrillation
 * @property integer $vent_tachy
 * @property integer $vent_tachy_type
 * @property integer $consciousness
 * @property integer $syncope_unknown_origin
 * @property string $pharm_custom
 * @property integer $surg_arr_rfa
 * @property integer $surg_arr
 * @property integer $hispital_days
 * @property integer $sick_days
 * @property integer $ambulance_calls
 * @property string $disease_complications
 * @property integer $impl_sssu
 * @property integer $impl_av_block_perm
 * @property integer $impl_av_block_trans
 * @property integer $impl_brady
 * @property integer $impl_binodal_av_perm
 * @property integer $impl_binodal_av_trans
 * @property integer $impl_vaso_syncope
 * @property string $impl_other_ind
 * @property integer $impl_prime_prevent
 * @property integer $impl_second_prevent
 * @property integer $impl_heart_failure
 * @property integer $impl_pacer
 * @property integer $impl_crt_p
 * @property integer $impl_crt_d
 * @property integer $impl_idc
 * @property string $impl_model
 * @property string $impl_model_sn
 * @property string $impl_model_idc
 * @property string $impl_model_sn_idc
 * @property string $ae_model
 * @property string $ae_sn
 * @property integer $ae_conf
 * @property string $ae_pacing_threshold
 * @property string $ae_pulse_ampl
 * @property integer $ae_pacing_imp
 * @property string $ve_model
 * @property string $ve_sn
 * @property integer $ve_conf
 * @property string $ve_stimul_threshold
 * @property string $ve_stimul_dur
 * @property string $ve_wave_apl
 * @property integer $ve_imp
 * @property integer $sve
 * @property integer $sve_imp
 * @property integer $sve_success
 * @property string $pacer_model
 * @property string $pacer_sn
 * @property integer $pacer_conf
 * @property string $pacer_stimul_threshold
 * @property string $pacer_stimul_dur
 * @property string $pacer_ampl
 * @property integer $pacer_imp
 * @property integer $pacer_placement
 * @property integer $trans_uid
 * @property string $trans_sn
 * @property string $trans_reg_date
 * @property integer $trans_guide_granted
 * @property integer $form_rh_signed
 * @property integer $form_hm_signed
 * @property integer $atrial_permanent
 * @property integer $excl_crit_pregnancy
 * @property integer $excl_crit_nomobile
 * @property integer $excl_crit_busy
 * @property integer $excl_crit_unable
 * @property integer $excl_crit_infection
 * @property integer $excl_crit_hardware
 * @property integer $exclude_shocks
 * @property integer $exclude_heartattack
 * @property integer $exclude_surgery
 * @property integer $enrolment_patient_accessible
 * @property integer $enrolment_age_over_18
 *
 * The followings are the available model relations:
 * @property Protocol $protocol
 * @property Icd10[] $etiology Массив объектов справочника МКБ10
 * @property Surgery[] $surgery
 * @property ProtocolEnrolmentSurgery[] $surgeryRel
 * @property ProtocolEnrolmentPharm[] $pharm
 *
 */
class ProtocolEnrolment extends AbstractProtocolData implements
    ProtocolFillableInterface,
    FieldDependencyDescriptorInterface
{

    public function primaryKey()
    {
        return 'protocol_uid';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ProtocolEnrolment the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{protocol_enrolment}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            [
                'protocol_uid, visit_date, diagnosis, trans_sn, trans_uid, patient_weight, enrolment_age_over_18, ' .
                ' enrolment_able, enrolment_agree, enrolment_gsm, enrolment_patient_accessible, disease_complications, ' .
                ' trans_guide_granted, form_hm_signed, form_rh_signed, ' .
                ' ve_conf, ve_stimul_threshold, ve_stimul_dur, ve_wave_apl, ve_imp',
                'required'
            ],
            [
                'protocol_uid, patient_height, enrolment_pacer, enrolment_crt_p,enrolment_crt_d, ' .
                'enrolment_icd, ' .
                'enrolment_able, enrolment_agree, enrolment_gsm, ca_block, ca_block_grade, atrial_arr, ' .
                'atrial_tachy, atrial_fibrillation, atrial_flutter, tachy_history_duration, tachy_longest_episode, ' .
                'tachy_shortest_episode, tachy_frequency, ab_block, ab_block_grade, left_branch_block, ' .
                'right_branch_block, both_branch_block, qrs_duration, vent_arr, vent_fibrillation, vent_tachy, ' .
                'vent_tachy_type, syncope_unknown_origin, surg_arr_rfa, surg_arr, hispital_days, sick_days, ' .
                'ambulance_calls, impl_sssu, impl_av_block_perm, impl_av_block_trans, impl_brady, ' .
                'impl_binodal_av_perm, impl_binodal_av_trans, impl_vaso_syncope, impl_prime_prevent, ' .
                'impl_second_prevent, impl_heart_failure, impl_pacer, impl_crt_p,impl_crt_d, impl_idc, ae_conf,' .
                've_conf, ve_imp, sve, sve_imp, sve_success, pacer_conf, pacer_imp, trans_uid, ' .
                'trans_guide_granted,pacer_placement,consciousness,arr_cupped,form_rh_signed,form_hm_signed,' .
                'atrial_permanent,excl_crit_pregnancy,excl_crit_nomobile,excl_crit_busy,excl_crit_unable,' .
                'excl_crit_infection,excl_crit_hardware,exclude_shocks,exclude_heartattack,exclude_surgery,' .
                'enrolment_patient_accessible,enrolment_age_over_18',
                'numerical',
                'integerOnly' => true
            ],
            ['ae_pacing_imp, ve_imp', 'numerical', 'integerOnly' => true, 'min' => '0'],
            [
                'patient_weight, ca_pause_duration, ab_pause_duration, ' .
                'ae_pacing_threshold, ae_pulse_ampl, ve_stimul_threshold, ve_stimul_dur, ve_wave_apl, ' .
                'pacer_stimul_threshold, pacer_stimul_dur, pacer_ampl, ae_pacing_threshold_dur',
                'length',
                'max' => 10
            ],
            [
                'patient_nyha',
                'length',
                'max' => 4
            ],
            [
                'impl_model, impl_model_sn, ve_model, impl_model_sn_idc, impl_model_idc, impl_pacer_model, impl_pacer_sn, ',
                'length',
                'max' => 255
            ],
            [
                've_sn, pacer_sn, trans_sn',
                'length',
                'max' => 100
            ],
            [
                'visit_date, diagnosis, diagnosis_comment, tachy_latest_date, disease_complications, impl_other_ind,' .
                ' ae_model, ae_sn, pacer_model, pacer_placement, trans_reg_date, etiology, surgery, surgeryRel, ' .
                'pharm,ab_pause,ca_pause, etiology, pharm_custom',
                'safe'
            ],
            [
                'patient_weight,ca_pause_duration,ab_pause_duration,ae_pacing_threshold,ae_pacing_threshold_dur,' .
                'ae_pulse_ampl,ve_stimul_threshold,ve_stimul_dur,ve_wave_apl,pacer_stimul_threshold,pacer_stimul_dur,' .
                'pacer_ampl',
                'CustomFloatValidator',
                'message' => Yii::t(
                    'ProtocolsModule.protocols',
                    'Поле "{attribute}" должно быть заполнено числом, например "36,6"'
                )
            ],
            [
                'ca_block_grade,ab_block_grade,vent_tachy_type,ab_pause_duration,ca_pause_duration,impl_model,' .
                'impl_model_sn,impl_model_idc,impl_model_sn_idc,ve_conf,ve_stimul_threshold,' .
                've_stimul_dur,ve_wave_apl,ve_imp,ae_conf,ae_pacing_threshold,ae_pacing_threshold_dur,ae_pulse_ampl,' .
                'ae_pacing_imp,pacer_imp',
                'ProtocolsDependencyValidator',
                'message' => Yii::t('ProtocolsModule.protocols', 'Поле "{attribute}" должно быть заполнено')
            ],
            [
                'enrolment_pacer,enrolment_crt_p,enrolment_crt_d,enrolment_icd',
                'AtLeastOneRequiredValidator',
                'message' => Yii::t('ProtocolsModule.protocols', 'Должно быть выбрано по крайней мере одно устройство')
            ],
            [
                'impl_sssu, impl_av_block_perm, impl_av_block_trans, impl_brady, impl_binodal_av_perm, impl_binodal_av_trans, impl_vaso_syncope, impl_prime_prevent, impl_second_prevent, impl_heart_failure',
                'AtLeastOneRequiredValidator',
                'message' => Yii::t('ProtocolsModule.protocols', 'Должно быть выбрано по крайней мере одно показание')
            ],
            [
                'sve_imp',
                'sveImpValidator'
            ],
            [
                'pacer_conf, pacer_stimul_threshold, pacer_stimul_dur, pacer_ampl, pacer_imp, pacer_placement',
                'crtDevicesValidator'
            ]
        );
    }

    /**
     * Валидация поля Шоковый импеданс (Ом)
     *
     * @param $attribute
     */
    public function sveImpValidator($attribute)
    {
        $deviceTypeAlias = $this->protocol->research->patient->device->model->type->alias;
        if (!$this->sve_imp && ($deviceTypeAlias === 'icd' || $deviceTypeAlias === 'crtd')) {
            $this->addError('sve_imp', Yii::t('ProtocolsModule.protocols', 'Обязательное для заполнения поле'));
        }
    }

    /**
     * Валидация протоколов, у которых у пациентов устройства типа CRT-D и CRD-T
     *
     * @param $attribute
     */
    public function crtDevicesValidator($attribute)
    {
        $deviceTypeAlias = $this->protocol->research->patient->device->model->type->alias;
        $deviceTypeAlias = substr($deviceTypeAlias, 0, strlen('crt'));
        if (!$this->$attribute && $deviceTypeAlias === 'crt' && $this->sve_success) {
            $this->addError($attribute, Yii::t('ProtocolsModule.protocols', 'Обязательное для заполнения поле: ') . $this->getAttributeLabel($attribute));
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'protocol'   => array(self::BELONGS_TO, 'Protocol', 'protocol_uid'),
            'surgeryRel' => array(self::HAS_MANY, 'ProtocolEnrolmentSurgery', 'protocol_uid'),
            'etiology'   => array(
                self::MANY_MANY,
                'Icd10',
                '{{protocol_enrolment_etiology}}(protocol_uid,disease_uid)'
            ),
            'diagnosis'  => array(self::MANY_MANY, 'Icd10', '{{protocol_enrolment_diagnosis}}(protocol_uid,icd10_uid)'),
            'pharm'      => array(self::HAS_MANY, 'ProtocolEnrolmentPharm', array('protocol_uid' => 'protocol_uid')),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        //TODO при создании протокола допущена ошибка в именовании полей таблицы, вместо pacer_sn, pacer_model и т.п. должны быть cs_lead_sn и т.д.
        //TODO необходимо переименовать поля.
        return array(
            'protocol_uid'                 => Yii::t('ProtocolsModule.protocols', 'Протокол Uid'),
            'patient_height'
            => Yii::t('ProtocolsModule.protocols', 'Рост (см)'),
            'patient_weight'               => Yii::t('ProtocolsModule.protocols', 'Вес (кг)'),
            'patient_nyha'                 => Yii::t('ProtocolsModule.protocols', 'Класс CH NYHA'),
            'enrolment_pacer'              => Yii::t('ProtocolsModule.protocols', 'ЭКС'),
            'enrolment_crt_p'              => Yii::t('ProtocolsModule.protocols', 'CRT-P'),
            'enrolment_crt_d'              => Yii::t('ProtocolsModule.protocols', 'CRT-D'),
            'enrolment_icd'                => Yii::t('ProtocolsModule.protocols', 'ИКД'),
            'enrolment_age_over_18'        => Yii::t(
                'ProtocolsModule.protocols',
                'Пациент старше 18 лет'
            ),
            'enrolment_able'               => Yii::t(
                'ProtocolsModule.protocols',
                'Пациент желает и может выполнять требования протокола клинического исследования'
            ),
            'enrolment_agree'              => Yii::t(
                'ProtocolsModule.protocols',
                'Пациент дал письменное информированное согласие на участие в исследовании'
            ),
            'enrolment_gsm'                => Yii::t(
                'ProtocolsModule.protocols',
                'Наличие сети мобильной связи GSM в месте проживания пациента'
            ),
            'enrolment_patient_accessible' => Yii::t(
                'ProtocolsModule.protocols',
                'Пациент не предполагает смену места жительства в течение периода участия в исследовании'
            ),
            'exclude_surgery'              => Yii::t(
                'ProtocolsModule.protocols',
                'Операция на сердце в течение последнего месяца'
            ),
            'exclude_heartattack'          => Yii::t(
                'ProtocolsModule.protocols',
                'Инфаркт миокарда в течение последнего месяца'
            ),
            'exclude_shocks'               => Yii::t(
                'ProtocolsModule.protocols',
                'Пациент с ИКД получил более двух шоковых разрядов за последние 6 месяцев'
            ),
            'excl_crit_hardware'           => Yii::t(
                'ProtocolsModule.protocols',
                'Дислокация, перелом, нарушение изоляции электродов, сообщение об ошибке в связи с импедансом' .
                ' электрода или амплитудой внутрисердечного сигнала, отсутствие захвата миокарда' .
                ' на периодической ВЭГМ, неадекватные срабатывания ИКД'
            ),
            'excl_crit_infection'          => Yii::t(
                'ProtocolsModule.protocols',
                'Инфекция, связанная с системой стимуляции'
            ),
            'excl_crit_unable'             => Yii::t(
                'ProtocolsModule.protocols',
                'Пациент не в состоянии управляться с системой “Home Monitoring”'
            ),
            'excl_crit_busy'               => Yii::t(
                'ProtocolsModule.protocols',
                'Пациент участвует в другом клиническом исследовании'
            ),
            'excl_crit_nomobile'           => Yii::t(
                'ProtocolsModule.protocols',
                'Отсутствие сети мобильной телефонной связи GSM в районе проживания пациента'
            ),
            'excl_crit_pregnancy'          => Yii::t(
                'ProtocolsModule.protocols',
                'Беременная или кормящая женщина'
            ),
            'visit_date'                   => Yii::t('ProtocolsModule.protocols', 'Дата предварительного визита'),
            'diagnosis'                    => Yii::t('ProtocolsModule.protocols', 'Клинический диагноз'),
            'diagnosis_comment'            => Yii::t('ProtocolsModule.protocols', 'Примечание к диагнозу'),
            'ca_block'                     => Yii::t('ProtocolsModule.protocols', 'СА блокада'),
            'ca_block_grade'               => Yii::t('ProtocolsModule.protocols', 'Степень'),
            'ca_pause'                     => Yii::t(
                'ProtocolsModule.protocols',
                'Документированная максимальная пауза'
            ),
            'ca_pause_duration'            => Yii::t(
                'ProtocolsModule.protocols',
                'Документированная максимальная пауза'
            ),
            'atrial_arr'                   => Yii::t(
                'ProtocolsModule.protocols',
                'Предсердная экстрасистолия (за сутки)'
            ),
            'atrial_tachy'                 => Yii::t('ProtocolsModule.protocols', 'Предсердная тахикардия (за сутки)'),
            'atrial_fibrillation'          => Yii::t('ProtocolsModule.protocols', 'Фибрилляция'),
            'atrial_flutter'               => Yii::t('ProtocolsModule.protocols', 'Трепетание предсердий'),
            'atrial_permanent'             => Yii::t('ProtocolsModule.protocols', 'Постоянная форма'),
            'tachy_history_duration'       => Yii::t(
                'ProtocolsModule.protocols',
                'Длительность анамнеза тахикардии (месяцев)'
            ),
            'tachy_latest_date'            => Yii::t('ProtocolsModule.protocols', 'Дата последнего эпизода'),
            'tachy_longest_episode'        => Yii::t(
                'ProtocolsModule.protocols',
                'Продолжительность эпизодов (минут) максимум'
            ),
            'tachy_shortest_episode'       => Yii::t(
                'ProtocolsModule.protocols',
                'Продолжительность эпизодов (минут) минимум'
            ),
            'tachy_frequency'              => Yii::t(
                'ProtocolsModule.protocols',
                'Частота возникновения эпизодов за последние 3 месяца'
            ),
            'arr_cupped'                   => Yii::t('ProtocolsModule.protocols', 'Аритмия купируется'),
            'ab_block'                     => Yii::t('ProtocolsModule.protocols', 'АВ блокада'),
            'ab_block_grade'               => Yii::t('ProtocolsModule.protocols', 'Степень'),
            'ab_pause'                     => Yii::t(
                'ProtocolsModule.protocols',
                'Документированная максимальная пауза'
            ),
            'ab_pause_duration'            => Yii::t(
                'ProtocolsModule.protocols',
                'Документированная максимальная пауза'
            ),
            'left_branch_block'            => Yii::t('ProtocolsModule.protocols', 'Блокада левой ножки пучка Гиса'),
            'right_branch_block'           => Yii::t('ProtocolsModule.protocols', 'Блокада правой ножки пучка Гиса'),
            'both_branch_block'            => Yii::t('ProtocolsModule.protocols', 'Двухпучковая блокада'),
            'qrs_duration'                 => Yii::t('ProtocolsModule.protocols', 'Длительность комплекса QRS (мс)'),
            'vent_arr'                     => Yii::t('ProtocolsModule.protocols', 'Желудочковая экстрасистолия'),
            'vent_fibrillation'            => Yii::t('ProtocolsModule.protocols', 'Фибрилляция желудочков'),
            'vent_tachy'                   => Yii::t('ProtocolsModule.protocols', 'Желудочковая тахикардия'),
            'vent_tachy_type'              => Yii::t('ProtocolsModule.protocols', 'Тип желудочковой тахикардии'),
            'consciousness'                => Yii::t('ProtocolsModule.protocols', 'Сознание при приступе'),
            'syncope_unknown_origin'       => Yii::t('ProtocolsModule.protocols', 'Синкопы неизвестного генеза'),
            'pharm_custom'                 => Yii::t('ProtocolsModule.protocols', 'Другие препараты'),
            'surg_arr_rfa'                 => Yii::t('ProtocolsModule.protocols', 'РЧА'),
            'surg_arr'                     => Yii::t('ProtocolsModule.protocols', 'Операция'),
            'hispital_days'                => Yii::t(
                'ProtocolsModule.protocols',
                'Количество койко-дней, проведенных в стационаре за год'
            ),
            'sick_days'                    => Yii::t(
                'ProtocolsModule.protocols',
                'Количество дней нетрудоспособности (на больничном) за год'
            ),
            'ambulance_calls'              => Yii::t(
                'ProtocolsModule.protocols',
                'Количество обращений к службе скорой помощи за год'
            ),
            'disease_complications'        => Yii::t(
                'ProtocolsModule.protocols',
                'Осложнения основного заболевания, которые возникли за год'
            ),
            'impl_sssu'                    => Yii::t('ProtocolsModule.protocols', 'СССУ, включая тахи-бради синдром'),
            'impl_av_block_perm'           => Yii::t(
                'ProtocolsModule.protocols',
                'Постоянная АВ-блокада высокой степени'
            ),
            'impl_av_block_trans'          => Yii::t(
                'ProtocolsModule.protocols',
                'Преходящая АВ-блокада высокой степени'
            ),
            'impl_brady'                   => Yii::t('ProtocolsModule.protocols', 'Брадикардия с постоянной формой ФП'),
            'impl_binodal_av_perm'         => Yii::t(
                'ProtocolsModule.protocols',
                'Бинодальное заболевание + постоянная АВ-блокада'
            ),
            'impl_binodal_av_trans'        => Yii::t(
                'ProtocolsModule.protocols',
                'Бинодальное заболевание + преходящая АВ-блокада'
            ),
            'impl_vaso_syncope'            => Yii::t('ProtocolsModule.protocols', 'Вазовагальные синкопы'),
            'impl_other_ind'               => Yii::t('ProtocolsModule.protocols', 'Другие показания'),
            'impl_prime_prevent'           => Yii::t('ProtocolsModule.protocols', 'Первичная профилактика'),
            'impl_second_prevent'          => Yii::t('ProtocolsModule.protocols', 'Вторичная профилактика'),
            'impl_heart_failure'           => Yii::t('ProtocolsModule.protocols', 'Сердечная недостаточность'),
            'impl_pacer'                   => Yii::t('ProtocolsModule.protocols', 'ЭКС'),
            'impl_crt_p'                   => Yii::t('ProtocolsModule.protocols', 'CRT-P'),
            'impl_crt_d'                   => Yii::t('ProtocolsModule.protocols', 'CRT-D'),
            'impl_idc'                     => Yii::t('ProtocolsModule.protocols', 'ИКД'),
            'impl_model'                   => Yii::t('ProtocolsModule.protocols', 'Модель CRT'),
            'impl_model_sn'                => Yii::t('ProtocolsModule.protocols', 'Серийный номер CRT'),
            'impl_model_idc'               => Yii::t('ProtocolsModule.protocols', 'Модель ИКД'),
            'impl_model_sn_idc'            => Yii::t('ProtocolsModule.protocols', 'Серийный номер ИКД'),
            'impl_pacer_sn'                => Yii::t('ProtocolsModule.protocols', 'Серийный номер ЭКС'),
            'impl_pacer_model'             => Yii::t('ProtocolsModule.protocols', 'Модель ЭКС'),
            'ae_model'                     => Yii::t('ProtocolsModule.protocols', 'Модель'),
            'ae_sn'                        => Yii::t('ProtocolsModule.protocols', 'Серийный номер'),
            'ae_conf'                      => Yii::t('ProtocolsModule.protocols', 'Конфигурация'),
            'ae_pacing_threshold'          => Yii::t('ProtocolsModule.protocols', 'Порог стимула (В)'),
            'ae_pacing_threshold_dur'      => Yii::t('ProtocolsModule.protocols', 'Длительность импульса (мс)'),
            'ae_pulse_ampl'                => Yii::t('ProtocolsModule.protocols', 'Амплитуда волны P (мВ)'),
            'ae_pacing_imp'                => Yii::t('ProtocolsModule.protocols', 'Импеданс (Ом)'),
            've_model'                     => Yii::t('ProtocolsModule.protocols', 'Желудочковый электрод (модель)'),
            've_sn'                        => Yii::t('ProtocolsModule.protocols', 'Серийный номер'),
            've_conf'                      => Yii::t('ProtocolsModule.protocols', 'Конфигурация'),
            've_stimul_threshold'          => Yii::t('ProtocolsModule.protocols', 'Порог стимула (В)'),
            've_stimul_dur'                => Yii::t('ProtocolsModule.protocols', 'Длительность стимула (мс)'),
            've_wave_apl'                  => Yii::t('ProtocolsModule.protocols', 'Амплитуда волны R (мВ)'),
            've_imp'                       => Yii::t('ProtocolsModule.protocols', 'Импеданс (Ом)'),
            'sve'                          => Yii::t('ProtocolsModule.protocols', 'Шоковый желудочковый электрод'),
            'sve_imp'                      => Yii::t('ProtocolsModule.protocols', 'Шоковый импеданс (Ом)'),
            'sve_success'                  => Yii::t('ProtocolsModule.protocols', 'Имплантация успешна'),
            'pacer_model'                  => Yii::t('ProtocolsModule.protocols', 'Электрод КС (модель)'),
            'pacer_sn'                     => Yii::t('ProtocolsModule.protocols', 'Серийный номер'),
            'pacer_conf'                   => Yii::t('ProtocolsModule.protocols', 'Конфигурация'),
            'pacer_stimul_threshold'       => Yii::t('ProtocolsModule.protocols', 'Порог стимула (В)'),
            'pacer_stimul_dur'             => Yii::t('ProtocolsModule.protocols', 'Длительность стимула (мс)'),
            'pacer_ampl'                   => Yii::t('ProtocolsModule.protocols', 'Амплитуда волны R (мВ)'),
            'pacer_imp'                    => Yii::t('ProtocolsModule.protocols', 'Импеданс (Ом)'),
            'pacer_placement'              => Yii::t('ProtocolsModule.protocols', 'Положение'),
            'trans_uid'                    => Yii::t('ProtocolsModule.protocols', 'Трансмиттер (модель)'),
            'trans_sn'                     => Yii::t('ProtocolsModule.protocols', 'Серийный номер'),
            'trans_reg_date'               => Yii::t(
                'ProtocolsModule.protocols',
                'Дата регистрации пациента и трансмиттера на сайте сервисного центра'
            ),
            'trans_guide_granted'          => Yii::t(
                'ProtocolsModule.protocols',
                'Пациенту выдана инструкция по использованию трансмиттера'
            ),
            'form_hm_signed'               => Yii::t(
                'ProtocolsModule.protocols',
                'Форма согласия на использование BIOTRONIK Home Monitoring подписана пациентом'
            ),
            'form_rh_signed'               => Yii::t(
                'ProtocolsModule.protocols',
                'Форма информированного согласия пациента на участие в исследовании “ReHoming” подписана пациентом'
            ),
        );
    }

    public function behaviors()
    {
        return array(
            'NullableAttrsBehavior'  => array(
                'class' => 'NullableAttrsBehavior'
            ),
            'FloatDelimiterBehavior' => ['class' => 'FloatDelimiterBehavior'],
            'EAdvancedArBehavior'    => array(
                'class' => 'application.extensions.EAdvancedArBehavior'
            ),
            'LoggingBehavior'        => array(
                'class' => 'protocols.components.ProtocolDataLoggingBehavior'
            ),
        );
    }

    /**
     * Возвращает возможные варианты NYHA класса
     *
     * @return array
     */
    public static function getNyhaOptions()
    {
        return NyhaClass::model()->findAll();
    }

    /**
     * Возвращает возможные варианты степени блокады
     *
     * @return array
     */
    public static function getBlockadeDegreeOptions()
    {
        return BlockadeDegree::model()->findAll();
    }

    /**
     * Возвращает возможные варианты расположения электрода
     *
     * @return array
     */
    public static function getPlacementOptions()
    {
        ElectrodePlacement::enableOnlyActive();
        return ElectrodePlacement::model()->withI18n(Yii::app()->language)->findAll();
    }

    /**
     * Как купируется аритмия
     *
     * @return array
     */
    public function getArrCuppingOptions()
    {
        ArrCupping::enableOnlyActive();
        $data = ArrCupping::model()->findAll();
        return CHtml::listData($data, 'uid', 'name');
    }

    /**
     * Виды желудочковой тахикаркии
     *
     * @return array
     */
    public function getVentTachyOptions()
    {
        $data = TachyType::model()->findAll();
        return CHtml::listData($data, 'uid', 'name');
    }

    /**
     * Возвращает опции сознания при приступе
     *
     * @return array
     */
    public function getConsciousnessOptions()
    {
        $data = AttackConsciousness::model()->findAll();
        return CHtml::listData($data, 'uid', 'name');
    }

    /**
     * Возвращает опции конфигурации имплантируемого устройства
     *
     * @return array
     */
    public function getImplConfOptions()
    {
        DevicePolarity::enableOnlyActive();
        $data = DevicePolarity::model()->findAll();
        return CHtml::listData($data, 'uid', 'name');
    }

    /**
     * Список болезней, которые выводятся в форме по умолчанию
     *
     * @return array
     */
    public function getDefaultDiseases()
    {
        $diseases = array(
            14911 => Yii::t('ProtocolsModule.protocols', "Артериальная гипертензия"),
            14802 => Yii::t('ProtocolsModule.protocols', "ИБС"),
            14459 => Yii::t('ProtocolsModule.protocols', "Сахарный диабет"),
            14502 => Yii::t('ProtocolsModule.protocols', "ВПС"),
            14815 => Yii::t('ProtocolsModule.protocols', "ППС"),
            14837 => Yii::t('ProtocolsModule.protocols', "Кардиомиопатия")
        );

        $idsToFetch = [];

        foreach ($this->etiology as $etiology) {
            if (!in_array($etiology->uid, $diseases)) {
                $idsToFetch[] = $etiology->uid;
            }
        }

        /** @var Icd10[] $diseasesMod */
        $diseasesMod = Icd10::model()->findAllByPk($idsToFetch);

        foreach ($diseasesMod as $dis) {
            if (!isset($diseases[$dis->uid])) {
                $diseases[$dis->uid] = $dis->code . " " . $dis->name;
            }
        }

        return $diseases;
    }

    /**
     * Список групп препаратов, которые выводятся в форме по умолчанию
     *
     * @return array
     */
    public function getPharmGroups()
    {
        return array(
            315,
            296,
            188,
            239,
            388,
            569
        );
    }

    /**
     * Возвращает группы и препараты для формы, которые должны выводиться в виде селектов. Временное решение для
     * доменстрационных целей
     * TODO перевести на настоящий словарь фарм. средств
     */
    public static function getPharmsTmp()
    {
        $groups = [
            [
                'name'   => 'Антиаритмики I класса',
                'groups' => ['C01BA', 'C01BB', 'C01BC', 'C01BG']
            ],
            [
                'name'   => 'b-блокаторы',
                'groups' => ['C07']
            ],
            [
                'name'   => 'Антиаритмики III класса',
                'groups' => ['C01BD']
            ],
            [
                'name'   => 'Ca-антагонисты',
                'groups' => ['C08']
            ],
            [
                'name'   => 'Сердечные гликозиды',
                'groups' => ['C01A']
            ],
            [
                'name'   => 'Ингибиторы АПФ',
                'groups' => ['C09A']
            ],
            [
                'name'   => 'Мочегонные',
                'groups' => ['C03']
            ],
            [
                'name'   => 'Нитраты',
                'groups' => ['C01DA']
            ],
//            [
//                'name' => 'Аспирин',
//                'groups' => array('N02BA01')
//            ],
            [
                'name'   => 'Антикоагулянты',
                'groups' => ['B01A']
            ],
        ];

        foreach ($groups as &$group) {
            $products = [];

            foreach ($group['groups'] as $code) {
                $products = array_merge(PharmGroupTmp::getProductsRecursivelyByCode($code), $products);
            }

            usort(
                $products,
                function ($a, $b) {
                    return strcmp($a->name, $b->name);
                }
            );

            $group['products'] = $products;
        }

        return $groups;
    }

    public function getFillingMap()
    {
        return array(
            'enrolment_pacer'         => ['source' => 'device_type', 'param' => 'pm'],
            'enrolment_crt_p'         => ['source' => 'device_type', 'param' => 'crtp'],
            'enrolment_crt_d'         => ['source' => 'device_type', 'param' => 'crtd'],
            'enrolment_icd'           => ['source' => 'device_type', 'param' => 'icd'],
            'impl_crt_p'              => ['source' => 'device_type', 'param' => 'crtp'],
            'impl_crt_d'              => ['source' => 'device_type', 'param' => 'crtd'],
            'impl_idc'                => ['source' => 'device_type', 'param' => 'icd'],
            'impl_pacer'              => ['source' => 'device_type', 'param' => 'pm'],
            // Шоковый желудочковый электрод (Шоковый импеданс (Ом))
            'sve_imp'                 => ['param' => 'daily_shock_lead_impedance'],
            // Модель и серийный номер
            'impl_model'              => ['source' => 'device', 'param' => 'device_model', 'deviceTypeFilter' => 'crt'],
            'impl_model_idc'          => ['source' => 'device', 'param' => 'device_model', 'deviceTypeFilter' => 'icd'],
            'impl_pacer_model'        => ['source' => 'device', 'param' => 'device_model', 'deviceTypeFilter' => 'pm'],
            'impl_model_sn'           => ['source' => 'device', 'param' => 'serial_number', 'deviceTypeFilter' => 'crt'],
            'impl_model_sn_idc'       => ['source' => 'device', 'param' => 'serial_number', 'deviceTypeFilter' => 'icd'],
            'impl_pacer_sn'           => ['source' => 'device', 'param' => 'serial_number', 'deviceTypeFilter' => 'pm'],
            //AE - RA - предсердие
            'ae_conf'                 => ['param' => 'ra_pacing_polarity', 'transform' => 'polarity'],
            'ae_pacing_threshold'     => ['param' => 'ra_pacing_threshold_capture_control', 'transform' => 'numeric'],
            'ae_pacing_threshold_dur' => ['param' => 'ra_pulse_width', 'transform' => 'numeric'],
            'ae_pulse_ampl'           => ['param' => 'ra_sensing_amplitude_daily_mean', 'transform' => 'numeric', 'statValue' => 'meanByCountValue'],
            'ae_pacing_imp'           => ['param' => 'ra_pacing_impedance', 'transform' => 'roundInteger', 'statValue' => 'meanByCountValue'],
            //VE - RV - правый желудочек
            've_conf'                 => ['param' => 'rv_sensing_polarity', 'transform' => 'polarity'],
            've_stimul_dur'           => ['param' => 'rv_pulse_width', 'transform' => 'numeric'],
            've_wave_apl'             => ['param' => 'rv_sensing_amplitude_daily_mean', 'transform' => 'numeric'],
            've_imp'                  => ['param' => 'rv_pacing_impedance', 'transform' => 'roundInteger', 'statValue' => 'meanByCountValue'],
            //pacer - LV - левый желудочек
            'pacer_conf'              => ['param' => 'lv_sensing_polarity', 'transform' => 'polarity'],
            'pacer_stimul_threshold'  => ['param' => 'lv_pacing_threshold', 'transform' => 'numeric'],
            'pacer_stimul_dur'        => ['param' => 'lv_pulse_width', 'transform' => 'numeric'],
            'pacer_ampl'              => ['param' => 'lv_sensing_amplitude_daily_mean', 'transform' => 'numeric'],
            'pacer_imp'               => ['param' => 'lv_pacing_impedance', 'transform' => 'numeric'],
            'trans_reg_date'          => ['source' => 'device', 'param' => 'active_since_date', 'transform' => 'rhDate'],
            'trans_sn'                => [
                'source'    => 'device',
                'param'     => 'transmitter_serial_number',
                'trans_uid' => ['source' => 'transmitter', 'param' => 'transmitter_type']
            ],
        );
    }

    public function init()
    {
        parent::init();

        if ($this->scenario == "insert") {
            $this->sve_success                  = 1;
            $this->trans_guide_granted          = 1;
            $this->enrolment_able               = 1;
            $this->enrolment_agree              = 1;
            $this->enrolment_gsm                = 1;
            $this->enrolment_age_over_18        = 1;
            $this->enrolment_patient_accessible = 1;
        }
    }

    public function initDefaultValues()
    {
        // Значение по умолчанию
        $this->ae_pacing_threshold = '0,5';
        // Желудочковый электрод (модель) - Порог стимула (В)
        $this->ve_stimul_threshold = '0,5';
    }


    /**
     * Возвращает массив с полями, возможность заполнения которых зависит от поставленного чекбокса
     * Ключ - название поля-чекбокса
     * Значение - массив названий зависимых полей
     *
     * @return array
     */
    public function getFieldsDependency()
    {
        return [
            'ca_block'   => ['ca_block_grade'],
            'ab_block'   => ['ab_block_grade'],
            'ab_pause'   => ['ab_pause_duration'],
            'ca_pause'   => ['ca_pause_duration'],
            'vent_tachy' => ['vent_tachy_type'],
            'sve'        => ['sve_imp'],
            'impl_pacer' => [
                'impl_pacer_model',
                'impl_pacer_sn',
                've_conf',
                've_stimul_threshold',
                've_stimul_dur',
                've_wave_apl',
                've_imp',
                'ae_model',
                'ae_sn',
                'ae_conf',
                'ae_pacing_threshold',
                'ae_pacing_threshold_dur',
                'ae_pulse_ampl',
                'ae_pacing_imp',
            ],
            'impl_crt_p' => [
                'impl_model',
                'impl_model_sn',
                'ae_model',
                'ae_sn',
                'ae_conf',
                'ae_pacing_threshold',
                'ae_pacing_threshold_dur',
                'ae_pulse_ampl',
                'ae_pacing_imp',
                've_conf', #Желудочковый электрод (модель)
                've_stimul_threshold',
                've_stimul_dur',
                've_wave_apl',
                've_imp',
                'sve_success',
                'pacer_conf',
                'pacer_stimul_threshold',
                'pacer_stimul_dur',
                'pacer_ampl',
                'pacer_imp',
                'pacer_placement',
            ],
            'impl_crt_d' => [
                'impl_model',
                'impl_model_sn',
                'ae_model',
                'ae_sn',
                'ae_conf',
                'ae_pacing_threshold',
                'ae_pacing_threshold_dur',
                'ae_pulse_ampl',
                'ae_pacing_imp',
                'pacer_conf',
                'pacer_stimul_threshold',
                'pacer_stimul_dur',
                'pacer_ampl',
                'pacer_imp',
                'pacer_placement',
                've_conf', #Желудочковый электрод (модель)
                've_stimul_threshold',
                've_stimul_dur',
                've_wave_apl',
                've_imp',
                'sve_success',
                'sve_imp',
            ],
            'impl_idc'   => [
                've_conf',
                've_stimul_threshold',
                've_stimul_dur',
                've_wave_apl',
                've_imp',
                'sve_success',
                'impl_model_idc',
                'impl_model_sn_idc',
                'ae_model',
                'ae_sn',
                'ae_conf',
                'ae_pacing_threshold',
                'ae_pacing_threshold_dur',
                'ae_pulse_ampl',
                'ae_pacing_imp',
                /*'pacer_model', #Электрод КС (модель)
                'pacer_sn',
                'pacer_conf',
                'pacer_stimul_threshold',
                'pacer_stimul_dur',
                'pacer_ampl',
                'pacer_imp'*/
            ]
        ];
    }

    /**
     * Возвращает протокол включения в исследование
     *
     * @param Research $research
     * @return ProtocolEnrolment
     */
    public function getByResearch(Research $research)
    {
        /** @var Protocol $protocol */
        $protocol = Protocol::model()->findByAttributes(
            array("research_uid" => $research->uid, "protocol_dictionary_uid" => ProtocolDictionary::PROTOCOL_ENROLMENT)
        );
        return $protocol->protocolEnrolment;
    }

    /**
     * "Магический" геттер для etiologyIds
     *
     * @return array
     */
    public function getEtiologyIds()
    {
        $ids = [];
        if ($this->etiology) {
            foreach ($this->etiology as $desease) {
                $ids[] = $desease->uid;
            }
        }
        return $ids;
    }
}
