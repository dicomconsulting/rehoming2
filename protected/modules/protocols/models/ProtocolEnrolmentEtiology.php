<?php

/**
 * This is the model class for table "{{protocol_enrolment_etiology}}".
 *
 * The followings are the available columns in table '{{protocol_enrolment_etiology}}':
 * @property integer $protocol_uid
 * @property integer $disease_uid
 */
class ProtocolEnrolmentEtiology extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ProtocolEnrolmentEtiology the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{protocol_enrolment_etiology}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('protocol_uid, disease_uid', 'required'),
            array('protocol_uid, disease_uid', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('protocol_uid, disease_uid', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'protocol_uid' => 'Ид протокола',
            'disease_uid' => 'Ид заболевания',
        );
    }
}
