<?php

/**
 * This is the model class for table "{{protocol_inithm}}".
 *
 * The followings are the available columns in table '{{protocol_inithm}}':
 * @property integer $protocol_uid
 * @property integer $patient_regged
 * @property string $hm_options_changed
 * @property integer $to_red
 * @property string $to_red_date
 * @property string $to_red_descr
 * @property integer $from_red
 * @property string $from_red_date
 * @property integer $from_red_status
 * @property string $from_red_descr
 * @property string $comment
 *
 * The followings are the available model relations:
 * @property Protocol $protocol
 */
class ProtocolInithm extends CActiveRecord implements FieldDependencyDescriptorInterface
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{protocol_inithm}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('protocol_uid', 'required'),
            array(
                'protocol_uid, patient_regged, to_red, from_red, from_red_status',
                'numerical',
                'integerOnly' => true
            ),
            array('to_red_date, to_red_descr, from_red_date, from_red_descr, comment, hm_options_changed', 'safe'),
            array(
                'to_red_date,from_red_date',
                'ProtocolsDependencyValidator',
                'message' => Yii::t('ProtocolsModule.protocols', 'Поле "{attribute}" должно быть заполнено')
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'protocol' => array(self::BELONGS_TO, 'Protocol', 'protocol_uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'protocol_uid' => Yii::t('ProtocolsModule.protocols', 'Ссылка на протокол'),
            'patient_regged' => Yii::t(
                'ProtocolsModule.protocols',
                'Пациент зарегистрирован в HomeMonitoring до включения'
            ),
            'hm_options_changed' => Yii::t('ProtocolsModule.protocols', 'Конфигурация событий Home monitoring (опции)'),
            'to_red' => Yii::t('ProtocolsModule.protocols', 'Статус событий Home Monitoring изменен на red (красный)'),
            'to_red_date' => Yii::t('ProtocolsModule.protocols', 'Дата изменения'),
            'to_red_descr' => Yii::t('ProtocolsModule.protocols', 'Примечание'),
            'from_red' => Yii::t(
                'ProtocolsModule.protocols',
                'Статус событий Home Monitoring изменен с red (красный) на другой'
            ),
            'from_red_date' => Yii::t('ProtocolsModule.protocols', 'Дата изменения'),
            'from_red_status' => Yii::t('ProtocolsModule.protocols', 'На какой цвет'),
            'from_red_descr' => Yii::t('ProtocolsModule.protocols', 'Примечание'),
            'comment' => Yii::t('ProtocolsModule.protocols', 'Комментарий/Примечание'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ProtocolInithm the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * На какой статус было переведен статус событий
     *
     * @return array
     */
    public function getFromRedOptions()
    {
        return array(
            0 => Yii::t('ProtocolsModule.protocols', "зеленый"),
            1 => Yii::t('ProtocolsModule.protocols', "желтый"),
        );
    }

    /**
     * Варианты изменены ли настройки HomeMonitoring
     *
     * @return array
     */
    public function getOptionsChangedOptions()
    {
        return array(
            0 => Yii::t('ProtocolsModule.protocols', 'Default'),
            1 => Yii::t('ProtocolsModule.protocols', 'минимум установок'),
        );
    }

    /**
     * Возвращает массив с полями, возможность заполнения которых зависит от поставленного чекбокса
     * Ключ - название поля-чекбокса
     * Значение - массив названий зависимых полей
     *
     * @return array
     */
    public function getFieldsDependency()
    {
        return array(
            "to_red" => array("to_red_date", "to_red_descr"),
            "from_red" => array("from_red_date", "from_red_status", "from_red_descr"),
        );
    }

    public function init()
    {
        parent::init();

        if ($this->scenario == "insert") {
            $this->patient_regged = 1;
        }
    }

    public function behaviors()
    {
        return array(
            'NullableAttrsBehavior' => array(
                'class' => 'NullableAttrsBehavior'
            ),
            'LoggingBehavior' => array(
                'class' => 'protocols.components.ProtocolDataLoggingBehavior'
            ),
        );
    }
}
