<?php

/**
 * This is the model class for table "{{protocol_enrolment_pharm}}".
 *
 * The followings are the available columns in table '{{protocol_enrolment_pharm}}':
 * @property integer $protocol_uid
 * @property integer $pharm_product_uid
 * @property string $dose
 * @property string $measure
 *
 * The followings are the available model relations:
 * @property Protocol $protocol
 * @property PharmProductTmp $pharmProduct
 */
class ProtocolEnrolmentPharm extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ProtocolEnrolmentPharm the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{protocol_enrolment_pharm}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('protocol_uid, pharm_product_uid', 'required'),
            array('protocol_uid, pharm_product_uid', 'numerical', 'integerOnly' => true),
            array('dose', 'length', 'max' => 10),
            array('measure', 'length', 'max' => 100),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'protocol' => array(self::BELONGS_TO, 'Protocol', 'protocol_uid'),
            'pharmProduct' => array(self::BELONGS_TO, 'PharmProductTmp', 'pharm_product_uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'protocol_uid' => 'Ид протокола',
            'pharm_product_uid' => 'ид лекарственного средства',
            'dose' => 'Доза',
            'measure' => 'Ед.измерения',
        );
    }

    public function behaviors()
    {
        return array(
            'FloatDelimiterBehavior' => ['class' => 'FloatDelimiterBehavior']
        );
    }
}
