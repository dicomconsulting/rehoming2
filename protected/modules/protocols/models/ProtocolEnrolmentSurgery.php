<?php

/**
 * This is the model class for table "{{protocol_enrolment_surgery}}".
 *
 * The followings are the available columns in table '{{protocol_enrolment_surgery}}':
 * @property integer $protocol_uid
 * @property integer $surgery_uid
 * @property string $date
 * @property string $descr
 *
 * The followings are the available model relations:
 * @property Protocol $protocol
 * @property Surgery $surgery
 */
class ProtocolEnrolmentSurgery extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ProtocolEnrolmentSurgery the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{protocol_enrolment_surgery}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('protocol_uid, surgery_uid, date', 'required'),
            array('protocol_uid, surgery_uid', 'numerical', 'integerOnly' => true),
            array('descr', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'protocol' => array(self::BELONGS_TO, 'Protocol', 'protocol_uid'),
            'surgery' => array(self::BELONGS_TO, 'Surgery', 'surgery_uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'protocol_uid' => 'Ид протокола',
            'surgery_uid' => 'ид типа операции',
            'date' => 'дата операции',
            'descr' => 'Характер операции',
        );
    }
}
