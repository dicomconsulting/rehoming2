<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 30.10.13
 * Time: 15:33
 */

interface FieldDependencyDescriptorInterface
{
    /**
     * Возвращает массив с полями, возможность заполнения которых зависит от поставленного чекбокса
     * Ключ - название поля-чекбокса
     * Значение - массив названий зависимых полей
     *
     * @return array
     */
    public function getFieldsDependency();
} 