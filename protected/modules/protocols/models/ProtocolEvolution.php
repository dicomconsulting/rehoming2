<?php

/**
 * This is the model class for table "{{protocol_evolution}}".
 *
 * The followings are the available columns in table '{{protocol_evolution}}':
 * @property integer $protocol_uid
 * @property integer $option_changed
 * @property string $option_changed_new_value
 * @property string $option_changed_reason
 * @property integer $red_finding
 * @property string $red_finding_date
 * @property string $red_finding_message
 * @property integer $red_finding_changed
 * @property integer $red_finding_changed_to
 * @property string $red_finding_changed_date
 * @property string $red_finding_changed_message
 * @property string $red_finding_changed_comment
 *
 * The followings are the available model relations:
 * @property Protocol $protocol
 * @property ProtocolFindingRelation[] $finding
 */
class ProtocolEvolution extends AbstractProtocolData implements FieldDependencyDescriptorInterface
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{protocol_evolution}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('protocol_uid', 'required'),
            array('protocol_uid, option_changed, red_finding, red_finding_changed', 'numerical', 'integerOnly' => true),
            array(
                'option_changed_new_value, option_changed_reason, red_finding_date, red_finding_message,' .
                ' red_finding_changed_date, red_finding_changed_message, red_finding_changed_comment,' .
                ' red_finding_changed_to, finding',
                'safe'
            ),
            array(
                'option_changed_new_value,option_changed_reason,red_finding_date,red_finding_message,' .
                'red_finding_changed_date,red_finding_changed_message',
                'ProtocolsDependencyValidator',
                'message' => Yii::t('ProtocolsModule.protocols', 'Поле "{attribute}" должно быть заполнено')
            ),

        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'protocol' => array(self::BELONGS_TO, 'Protocol', 'protocol_uid'),
            'finding' => array(self::HAS_MANY, 'ProtocolFindingRelation', array('protocol_uid' => 'protocol_uid')),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'protocol_uid' => Yii::t('ProtocolsModule.protocols', 'Ссылка на описание протокола'),
            'option_changed' => Yii::t(
                'ProtocolsModule.protocols',
                'Опция Home Monitoring изменена со времени включения'
            ),
            'option_changed_new_value' => Yii::t('ProtocolsModule.protocols', 'Укажите новую опцию'),
            'option_changed_reason' => Yii::t('ProtocolsModule.protocols', 'Причина изменения'),
            'red_finding' => Yii::t(
                'ProtocolsModule.protocols',
                'Статус сообщения Home Monitoring изменен на “red” (красный)'
            ),
            'red_finding_date' => Yii::t('ProtocolsModule.protocols', 'Дата изменения'),
            'red_finding_message' => Yii::t('ProtocolsModule.protocols', 'Сообщение'),
            'red_finding_changed' => Yii::t(
                'ProtocolsModule.protocols',
                'Статус сообщения Home Monitoring изменен с “red” (красный) на другой'
            ),
            'red_finding_changed_date' => Yii::t('ProtocolsModule.protocols', 'Дата изменения'),
            'red_finding_changed_to' => Yii::t('ProtocolsModule.protocols', 'На какой цвет'),
            'red_finding_changed_message' => Yii::t('ProtocolsModule.protocols', 'Сообщение'),
            'red_finding_changed_comment' => Yii::t('ProtocolsModule.protocols', 'Примечание'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ProtocolEvolution the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Возвращает массив с полями, возможность заполнения которых зависит от поставленного чекбокса
     * Ключ - название поля-чекбокса
     * Значение - массив названий зависимых полей
     *
     * @return array
     */
    public function getFieldsDependency()
    {
        return array(
            "option_changed" => array("option_changed_new_value", "option_changed_reason"),
            "red_finding" => array("red_finding_date", "red_finding_message"),
            "red_finding_changed" => array(
                "red_finding_changed_date",
                "red_finding_changed_to",
                "red_finding_changed_message",
                "red_finding_changed_comment"
            ),
        );
    }

    public function behaviors()
    {
        return array(
            'NullableAttrsBehavior' => array(
                'class' => 'NullableAttrsBehavior'
            ),
            'EAdvancedArBehavior' => array(
                'class' => 'application.extensions.EAdvancedArBehavior'
            ),
            'LoggingBehavior' => array(
                'class' => 'protocols.components.ProtocolDataLoggingBehavior'
            ),
        );
    }
}
