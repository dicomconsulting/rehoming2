<?php

/**
 * This is the model class for table "{{protocol_finding_relation}}".
 *
 * The followings are the available columns in table '{{protocol_finding_relation}}':
 * @property integer $protocol_uid
 * @property integer $finding_uid
 * @property string $action
 * @property string $action_date
 * @property integer $clinic_visit
 * @property string $clinic_distance
 * @property string $clinic_time
 *
 */
class ProtocolFindingRelation extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ProtocolFindingRelation the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{protocol_finding_relation}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('protocol_uid, finding_uid', 'required'),
            array('protocol_uid, finding_uid, clinic_visit', 'numerical', 'integerOnly' => true),
            array('clinic_distance, clinic_time', 'length', 'max' => 10),
            array('action, action_date,clinic_visit', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'protocolEvolution' => array(self::BELONGS_TO, 'ProtocolEvolution', 'protocol_uid'),
            'protocolRegular' => array(self::BELONGS_TO, 'ProtocolRegular', 'protocol_uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'protocol_uid' => 'Ссылка на описание протокола',
            'finding_uid' => 'Ссылка на описание нежелательное явление',
            'action' => 'Предпринятые действия',
            'action_date' => 'Дата первого предпринятого действия',
            'clinic_visit' => 'Посещение клиники',
            'clinic_distance' => 'Расстояние (км)',
            'clinic_time' => 'Время в пути (часов)',
        );
    }

    public function behaviors()
    {
        return array(
            'NullableAttrsBehavior' => array(
                'class' => 'NullableAttrsBehavior'
            ),
        );
    }
}
