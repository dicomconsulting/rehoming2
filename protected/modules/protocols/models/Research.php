<?php

/**
 * This is the model class for table "{{research}}".
 *
 * The followings are the available columns in table '{{research}}':
 * @property integer $uid
 * @property RhDateTime $date_begin
 * @property RhDateTime $date_end
 * @property integer $patient_uid
 * @property string $patient_code
 * @property integer $medical_center_uid
 * @property integer $screening_number
 * @property integer $user_group_id
 *
 * The followings are the available model relations:
 * @property MedicalCenter $medicalCenter
 * @property Patient $patient
 * @property Protocol[] $protocol
 *
 * @method Research maxscreening
 */
class Research extends CActiveRecord
{

    /**
     * Начата работа над заведением исследования. Новые протоколы заводить нельзя, можно только редактировать протокол
     * включения.
     */
    const STATUS_BEGUN = 0;

    /**
     * Заведен протокол включения - можно инициировать исследование
     */
    const STATUS_ENROLLED = 1;

    /**
     * Исследование инициализировано - можно заводить все остальные протоколы
     */
    const STATUS_INITIATED = 2;

    /**
     * Исследование закрыто. Разрешена печать и просмотр данных.
     */
    const STATUS_CLOSED = 3;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{research}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('date_begin, patient_uid, medical_center_uid, screening_number, patient_code', 'required'),
            array('date_end', 'required', 'on' => 'finish'),
            array('patient_uid, medical_center_uid, screening_number', 'numerical', 'integerOnly' => true),
//            array('screening_number', 'checkNumberUnique'),
        );
    }

    /**
     * Проверка на уникальность скрининга в рамках одной группы пользователей
     *
     * @param $attribute
     * @param $params
     */
    public function checkNumberUnique($attribute, $params)
    {
        $criteria = new CDbCriteria();

        if (!$this->getIsNewRecord()) {
            $criteria->condition = 'uid <> :self_uid';
            $criteria->params = [':self_uid' => $this->uid];
        }

        $criteria->compare($attribute, $this->$attribute);

        if (self::model()->find($criteria)) {
            $this->addError(
                $attribute,
                Yii::t('AdminModule.messages', 'Номер отбора {{n}} уже занят.', ['{{n}}' => $this->$attribute])
            );
        };
    }

    public function init()
    {
        Yii::import('protocols.components.*');

        if ($this->scenario == "insert") {
            //при создании нового исследования
            $this->date_begin = date("Y-m-d");
        }

        parent::init();
    }

    public function assignScreeningNumber()
    {
        $maxScreening = $this->maxscreening()->findAll();
        if (isset($maxScreening[0])) {
            $this->screening_number = ++$maxScreening[0]->screening_number;
        } else {
            $this->screening_number = 1;
        }
    }

    public function scopes()
    {
        return array(
            'maxscreening' => array(
                'order' => 'screening_number DESC',
                'limit' => 1,
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'medicalCenter' => array(self::BELONGS_TO, 'MedicalCenter', 'medical_center_uid'),
            'patient'       => array(self::BELONGS_TO, 'Patient', 'patient_uid'),
            'protocol'      => array(self::HAS_MANY, 'Protocol', 'research_uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid'                => Yii::t('ProtocolsModule.protocols', 'Uid'),
            'date_begin'         => Yii::t('ProtocolsModule.protocols', 'Дата включения в исследование'),
            'date_end'           => Yii::t('ProtocolsModule.protocols', 'Дата окончания исследования'),
            'patient_uid'        => Yii::t('ProtocolsModule.protocols', 'Ссылка на пациента'),
            'patient_code'       => Yii::t('ProtocolsModule.protocols', 'Код пациента'),
            'medical_center_uid' => Yii::t('ProtocolsModule.protocols', 'Медицинский центр'),
            'screening_number'   => Yii::t('ProtocolsModule.protocols', 'Номер отбора'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Research the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        return array(
            'RhDateTimeBehavior'  => array(
                'class' => 'application.extensions.RhDateTimeBehavior'
            ),
            'UserGroupSeparation' => array(
                'class' => 'admin.components.UserGroupSeparationBehavior'
            )
        );
    }

    protected function afterFind()
    {
        parent::afterFind();

        if ($this->scenario == "update" && $this->date_begin instanceof RhDateTime) {
            $this->date_begin->setDefaultOutputFormat("Y-m-d");
        }

        if ($this->scenario == "update" && $this->date_end instanceof RhDateTime) {
            $this->date_end->setDefaultOutputFormat("Y-m-d");
        }

        $this->screening_number = str_pad($this->screening_number, 3, 0, STR_PAD_LEFT);
    }

    protected function beforeSave()
    {
        $this->patient_code = strtoupper($this->patient_code);
        return parent::beforeSave();
    }

    /**
     * Возвращает открытое исследование по пациенту. Оно может быть только одно
     *
     * @param $patientId
     * @return Research
     */
    public function getOpenedByPatient($patientId)
    {
        /** @var Research $research */
        $research = $this->getResearchInProgress($patientId);

        if (!empty($research)) {
            $protocolEnrolment = $research->getProtocolsByType(ProtocolDictionary::PROTOCOL_ENROLMENT);

            if ($protocolEnrolment) {
                $protocolEnrolment = $protocolEnrolment[0];
                # https://jira.kirkazan.ru/browse/REHOMING-304
                # 1. Сделать необязательным подписание протокола включения при создании протокола Инициализация НМ
                #if (!$protocolEnrolment->isOpened()) {
                    return $research;
                #}
            }
        }

        return null;
    }

    /**
     * Начаты ли работы по включению пациента в исследование
     *
     * @param Patient $patient
     * @return bool
     */
    public function isWorkflowBegun(Patient $patient)
    {
        /** @var Research $research */
        $research = $this->getResearchInProgress($patient->uid);

        if (!empty($research)) {
            return true;
        }

        return false;
    }

    /**
     * Начаты или проведены ли работы по инициированию
     *
     * @return bool
     */
    public function isInitiatingBegun()
    {
        /** @var Research $research */
        $research = $this->getResearchInProgress($this->patient_uid);

        if (!empty($research)) {
            $protocolInit = $this->getProtocolsByType(ProtocolDictionary::PROTOCOL_INITHM);
            if ($protocolInit) {
                return true;
            }
        }

        return false;
    }


    /**
     * Начаты ли работы по завершению исследования
     *
     * @return bool
     */
    public function isTerminationBegun()
    {
        /** @var Research $research */
        $research = $this->getResearchInProgress($this->patient_uid);

        if (!empty($research)) {
            $protocol = $this->getProtocolsByType(ProtocolDictionary::PROTOCOL_FINISH);
            if ($protocol) {
                return true;
            }
        }

        return false;
    }

    /**
     * Возвращает открыто ли данное исследование
     *
     * @return bool
     */
    public function isOpened()
    {
        $status = $this->getStatus();
        # https://jira.kirkazan.ru/browse/REHOMING-304 self::STATUS_BEGUN
        if (in_array($status, [self::STATUS_BEGUN, self::STATUS_ENROLLED, self::STATUS_INITIATED])) {
            return true;
        }

        return false;
    }

    public function getStatus()
    {
        if ($this->date_begin && $this->date_end) {
            $protocolFinish = $this->getProtocolsByType(ProtocolDictionary::PROTOCOL_FINISH);

            if ($protocolFinish) {
                /** @var Protocol $protocolFinish */
                $protocolFinish = $protocolFinish[0];

                if (!$protocolFinish->isOpened()) {
                    return self::STATUS_CLOSED;
                }
            }
        }

        $protocolInit = $this->getProtocolsByType(ProtocolDictionary::PROTOCOL_INITHM);

        # https://jira.kirkazan.ru/browse/REHOMING-304
        # 2. Сделать возможность редактировать протокол Включения в исследование при созданном протоколе Инициализация НМ
        /*$inited = false;
        foreach ($protocolInit as $protocol) {
            //если есть хотя бы один протокол инициализации НМ, считается что исследование инициализировано
            if ($protocol->status == Protocol::STATUS_CLOSED) {
                $inited = true;
            }
        }

        if ($inited) {
            return self::STATUS_INITIATED;
        }*/

        $protocolEnrolment = $this->getProtocolsByType(ProtocolDictionary::PROTOCOL_ENROLMENT);

        if ($protocolEnrolment) {
            /** @var Protocol $protocolEnrolment */
            $protocolEnrolment = $protocolEnrolment[0];

            if ($protocolEnrolment->isOpened()) {
                return self::STATUS_BEGUN;
            }
        }

        return self::STATUS_ENROLLED;
    }

    /**
     * Существует ли хотя бы одно исследование по пациенту
     *
     * @param Patient $patient
     * @return bool
     */
    public static function isAnyExists(Patient $patient)
    {
        $research = self::model()->findByAttributes(array("patient_uid" => $patient->uid));

        if ($research) {
            return true;
        } else {
            return false;
        }

    }

    public function getScreeningNumber()
    {
        return sprintf("%1$03d", $this->screening_number);
    }

    /**
     * Возвращает список протоколов по исследованию
     *
     * @return Protocol[]
     */
    public function getAllProtocols()
    {
        $protocols = Protocol::model()->findAllByAttributes(array("research_uid" => $this->uid));
        return $protocols;
    }

    /**
     * @param $protocolType
     * @return Protocol[]
     */
    public function getProtocolsByType($protocolType)
    {
        $protocols = Protocol::model()->findAllByAttributes(
            array("research_uid" => $this->uid, "protocol_dictionary_uid" => $protocolType)
        );
        return $protocols;
    }

    /**
     * Может ли текущее исследование быть закрыто, для этого все входящие протоколы должны быть закрыты.
     *
     * @return bool
     */
    public function canBeClosed()
    {
        $count = Protocol::model()->countByAttributes(
            array("research_uid" => $this->uid, "status" => Protocol::STATUS_OPENED),
            "protocol_dictionary_uid <> " . ProtocolDictionary::PROTOCOL_FINISH
        );

        if ($count) {
            return false;
        }

        return true;
    }

    /**
     * Возвращает список протоколов по исследованию, которые находят в статусе Рабочий (Открыт)
     *
     * @return Protocol[]
     */
    public function getOpenedProtocols()
    {
        $protocols = Protocol::model()->findAllByAttributes(
            array("research_uid" => $this->uid, "status" => Protocol::STATUS_OPENED),
            "protocol_dictionary_uid <> " . ProtocolDictionary::PROTOCOL_FINISH
        );

        return $protocols;
    }

    /**
     * Возвращает исследование, по которому начата или происходит какая-либо работа, но исследование не завершено
     *
     * @param $patientId
     * @return CActiveRecord
     */
    protected function getResearchInProgress($patientId)
    {
        $research = $this->with(
            array(
                "protocol" => array(
                    "on" => "protocol_dictionary_uid = " . ProtocolDictionary::PROTOCOL_FINISH
                )
            )
        )->find(
                "(patient_uid=:patient_uid AND date_end IS NULL) " .
                "OR (patient_uid=:patient_uid " .
                "AND date_end IS NOT NULL " .
                "AND (protocol.status = :status OR protocol.status IS NULL))",
                array(":patient_uid" => $patientId, ":status" => Protocol::STATUS_OPENED)
            );
        return $research;
    }

    /**
     * @param $protocolType
     * @return Protocol[]
     */
    public static function getClosedProtocols($protocolType)
    {
        $criteria = new CDbCriteria();
        $criteria->order = "date";

        $protocols = Protocol::model()->findAllByAttributes(
            array("status" => Protocol::STATUS_CLOSED, "protocol_dictionary_uid" => $protocolType),
            $criteria
        );

        return $protocols;

    }
}
