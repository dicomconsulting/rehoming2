<?php
Yii::import('i18n.models.I18nActiveRecord');
Yii::import('protocols.models.ProtocolDictionaryI18n');

/**
 * This is the model class for table "{{protocol_dictionary}}".
 *
 * The followings are the available columns in table '{{protocol_dictionary}}':
 * @property integer $uid
 * @property string $alias
 *
 * The followings are the available model relations:
 * @property Protocol[] $protocols
 * @property ProtocolDictionaryI18n[] $protocolDictionaryI18ns
 */
class ProtocolDictionary extends I18nActiveRecord
{

    const PROTOCOL_ENROLMENT = 1;
    const PROTOCOL_INITHM = 2;
    const PROTOCOL_EVOLUTION = 3;
    const PROTOCOL_ANNUAL = 4;
    const PROTOCOL_ADVERSE = 5;
    const PROTOCOL_REGULAR = 6;
    const PROTOCOL_FINISH = 7;

    public function primaryKey()
    {
        return 'uid';
    }


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{protocol_dictionary}}';
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'protocols' => array(self::HAS_MANY, 'Protocol', 'protocol_dictionary_uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid' => 'Uid',
            'alias' => Yii::t(
                'ProtocolsModule.protocols',
                'Псевдоним названия протокола на английском, для ссылок на дочерние таблицы'
            ),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ProtocolDictionary the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Возвращает название протокола по его алиасу
     *
     * @return string
     */
    public function getProtocolName()
    {
        return "protocol" . ucfirst($this->alias);
    }
}
