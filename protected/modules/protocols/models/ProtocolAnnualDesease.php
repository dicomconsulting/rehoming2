<?php

/**
 * This is the model class for table "{{protocol_annual_desease}}".
 *
 * The followings are the available columns in table '{{protocol_annual_desease}}':
 * @property integer $protocol_uid
 * @property integer $icd10_uid
 * @property string $descr
 * @property Icd10 $icd10
 */
class ProtocolAnnualDesease extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ProtocolAnnualDesease the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{protocol_annual_desease}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('protocol_uid, icd10_uid', 'required'),
            array('protocol_uid, icd10_uid', 'numerical', 'integerOnly' => true),
            array('descr', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'icd10' => array(self::BELONGS_TO, 'Icd10', 'icd10_uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'protocol_uid' => 'Ид протокола',
            'icd10_uid' => 'ид заболевания',
            'descr' => 'подробности',
        );
    }
}
