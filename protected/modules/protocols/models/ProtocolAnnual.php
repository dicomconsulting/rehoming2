<?php

/**
 * This is the model class for table "{{protocol_annual}}".
 *
 * The followings are the available columns in table '{{protocol_annual}}':
 * @property integer $protocol_uid
 * @property integer $findings_cnt
 * @property integer $medical_therapy_changed
 * @property string $medical_therapy_comment
 * @property integer $other_therapy_changed
 * @property string $other_therapy_comment
 * @property integer $estimated_eri_yesars
 * @property integer $estimated_eri_months
 * @property integer $atrial_arrhythmic_burden
 * @property integer $pacing_in_a
 * @property integer $pacing_in_b
 * @property integer $as_vs
 * @property integer $as_vp
 * @property integer $ap_vp
 * @property integer $ap_vs
 * @property integer $stimul_ddd_adi
 * @property integer $stimul_irs_on
 * @property integer $stimul_irs_off
 * @property integer $capt_ctrl_a
 * @property integer $capt_ctrl_v
 * @property integer $sens_a
 * @property integer $sens_v
 * @property integer $atr_conf
 * @property integer $ven_conf
 * @property string $req_atr_ampl
 * @property string $req_ven_ampl
 * @property integer $req_atr_stimul_thresh
 * @property integer $req_ven_stimul_thresh
 * @property integer $req_atr_el_imp
 * @property integer $req_ven_el_imp
 * @property integer $req_ven_shock_imp
 * @property string $atest_atr_ampl
 * @property string $atest_ven_ampl
 * @property integer $atest_atr_stimul_thresh
 * @property integer $atest_ven_stimul_thresh
 * @property integer $atest_atr_el_imp
 * @property integer $atest_ven_el_imp
 * @property string $test_atr_ampl
 * @property string $test_ven_ampl
 * @property integer $test_atr_stimul_thresh
 * @property integer $test_ven_stimul_thresh
 * @property integer $pacer
 * @property integer $pacer_conf
 * @property string $pacer_ampl
 * @property integer $pacer_stimul_thresh
 * @property integer $pacer_imp
 * @property string $placement
 * @property integer $rtg_time_hmsc
 * @property integer $rtg_time_semaphore
 * @property integer $rtg_time_iegm
 * @property integer $rtg_cl_hmsc
 * @property integer $rtg_cl_semaphore
 * @property integer $rtg_cl_iegm
 * @property integer $rtg_cl_data_suff
 * @property integer $cl_hosp_days
 * @property integer $cl_disability_days
 * @property integer $cl_amb_calls
 * @property string $comment
 *
 * @property ProtocolAnnualDesease[] $deseaseRel
 *
 */
class ProtocolAnnual extends AbstractProtocolData implements
    ProtocolFillableInterface,
    FieldDependencyDescriptorInterface
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ProtocolAnnual the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{protocol_annual}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('protocol_uid,as_vs,as_vp,ap_vp,ap_vs,capt_ctrl_a,capt_ctrl_v,ven_conf,' .
            'req_ven_ampl,req_ven_stimul_thresh,req_ven_el_imp,req_ven_shock_imp,'.
            'atest_ven_ampl,atest_ven_stimul_thresh,' .
            'atest_ven_el_imp,test_ven_ampl,test_ven_stimul_thresh,placement,' .
            'rtg_time_hmsc,rtg_time_semaphore,rtg_time_iegm,rtg_cl_hmsc,rtg_cl_semaphore,rtg_cl_iegm,rtg_cl_data_suff',
                'required'),
            array(
                'protocol_uid, findings_cnt, medical_therapy_changed, other_therapy_changed, estimated_eri_yesars, ' .
                'estimated_eri_months, atrial_arrhythmic_burden, pacing_in_a, pacing_in_b, as_vs, as_vp, ap_vp, ' .
                'ap_vs, stimul_ddd_adi, stimul_irs_on, stimul_irs_off, capt_ctrl_a, capt_ctrl_v, sens_a, sens_v, ' .
                'atr_conf, ven_conf, req_atr_el_imp, req_ven_el_imp, ' .
                'req_ven_shock_imp, atest_atr_el_imp, ' .
                'atest_ven_el_imp, pacer, pacer_conf, ' .
                'pacer_imp, rtg_time_hmsc, rtg_time_semaphore, rtg_time_iegm, rtg_cl_hmsc, ' .
                'rtg_cl_semaphore, rtg_cl_iegm, rtg_cl_data_suff, cl_hosp_days, cl_disability_days, cl_amb_calls',
                'numerical',
                'integerOnly' => true
            ),
            array(
                'req_atr_ampl, req_ven_ampl, atest_atr_ampl, atest_ven_ampl, test_atr_ampl, test_ven_ampl, pacer_ampl',
                'length',
                'max' => 10
            ),
            array(
                'medical_therapy_comment, other_therapy_comment, placement,comment',
                'safe'
            ),
            array(
                'req_atr_ampl,req_ven_ampl,atest_atr_ampl,atest_ven_ampl,test_atr_ampl,test_ven_ampl,pacer_ampl,
                req_atr_stimul_thresh, req_ven_stimul_thresh,atest_atr_stimul_thresh, ' .
                'atest_ven_stimul_thresh, test_atr_stimul_thresh, test_ven_stimul_thresh, pacer_stimul_thresh, ',
                'CustomFloatValidator',
                'message' => Yii::t(
                    'ProtocolsModule.protocols',
                    'Поле "{attribute}" должно быть заполнено числом, например "36,6"'
                )
            ),
            array(
                'stimul_ddd_adi,stimul_irs_on,stimul_irs_off',
                'AtLeastOneRequiredValidator'
            ),
            array(
                'medical_therapy_comment,pacer_ampl,pacer_stimul_thresh,pacer_imp',
                'ProtocolsDependencyValidator',
                'message' => Yii::t('ProtocolsModule.protocols', 'Поле "{attribute}" должно быть заполнено')
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'protocol' => array(self::BELONGS_TO, 'Protocol', 'protocol_uid'),
            'deseaseRel' => array(self::HAS_MANY, 'ProtocolAnnualDesease', array('protocol_uid' => 'protocol_uid')),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'protocol_uid' => Yii::t('ProtocolsModule.protocols', 'Протокол Uid'),
            'findings_cnt' => Yii::t(
                'ProtocolsModule.protocols',
                'Число нежелательных явлений или нежелательных явлений, ' .
                'связанных с прибором (с момента включения пациента в исследование)'
            ),
            'medical_therapy_changed' => Yii::t(
                'ProtocolsModule.protocols',
                'Изменения медикаментозной терапии с момента включения'
            ),
            'medical_therapy_comment' => Yii::t('ProtocolsModule.protocols', 'Примечание (укажите какие)'),
            'other_therapy_changed' => Yii::t(
                'ProtocolsModule.protocols',
                'Другие изменения терапии с момента включения'
            ),
            'other_therapy_comment' => Yii::t('ProtocolsModule.protocols', 'Примечание'),
            'estimated_eri_yesars' => Yii::t('ProtocolsModule.protocols', 'лет'),
            'estimated_eri_months' => Yii::t('ProtocolsModule.protocols', 'месяцев'),
            'atrial_arrhythmic_burden' => Yii::t('ProtocolsModule.protocols', 'Доля предсердных аритмий (%)'),
            'pacing_in_a' => Yii::t('ProtocolsModule.protocols', 'Предсердная стимуляция (%)'),
            'pacing_in_b' => Yii::t('ProtocolsModule.protocols', 'Желудочковая стимуляция  (%)'),
            'as_vs' => Yii::t('ProtocolsModule.protocols', 'AsVs (%)'),
            'as_vp' => Yii::t('ProtocolsModule.protocols', 'AsVp (%)'),
            'ap_vp' => Yii::t('ProtocolsModule.protocols', 'ApVp (%)'),
            'ap_vs' => Yii::t('ProtocolsModule.protocols', 'ApVs (%)'),
            'stimul_ddd_adi' => Yii::t('ProtocolsModule.protocols', 'подавление Vp / режим DDD(R) - ADI(R)'),
            'stimul_irs_on' => Yii::t('ProtocolsModule.protocols', 'другой режим и AV hysteresis = IRSplus On'),
            'stimul_irs_off' => Yii::t('ProtocolsModule.protocols', 'другой режим и AV hysteresis = IRSplus Off'),
            'capt_ctrl_a' => Yii::t('ProtocolsModule.protocols', 'Контроль захвата пердсердий'),
            'capt_ctrl_v' => Yii::t('ProtocolsModule.protocols', 'Контроль захвата желудочков'),
            'sens_a' => Yii::t('ProtocolsModule.protocols', 'Чувствительность предсердий'),
            'sens_v' => Yii::t('ProtocolsModule.protocols', 'Чувствительность желудочков'),
            'atr_conf' => Yii::t('ProtocolsModule.protocols', 'Конфигурация'),
            'ven_conf' => Yii::t('ProtocolsModule.protocols', 'Конфигурация'),
            'req_atr_ampl' => Yii::t('ProtocolsModule.protocols', 'Амплитуда волны P/R (мВ)'),
            'req_ven_ampl' => Yii::t('ProtocolsModule.protocols', 'Амплитуда волны P/R (мВ)'),
            'req_atr_stimul_thresh' => Yii::t('ProtocolsModule.protocols', 'Порог стимуляции (В)'),
            'req_ven_stimul_thresh' => Yii::t('ProtocolsModule.protocols', 'Порог стимуляции (В)'),
            'req_atr_el_imp' => Yii::t('ProtocolsModule.protocols', 'Импеданс электрода (Ом)'),
            'req_ven_el_imp' => Yii::t('ProtocolsModule.protocols', 'Импеданс электрода (Ом)'),
            'req_ven_shock_imp' => Yii::t('ProtocolsModule.protocols', 'Шоковый импеданс (Ом)'),
            'atest_atr_ampl' => Yii::t('ProtocolsModule.protocols', 'Амплитуда волны P/R (мВ)'),
            'atest_ven_ampl' => Yii::t('ProtocolsModule.protocols', 'Амплитуда волны P/R (мВ)'),
            'atest_atr_stimul_thresh' => Yii::t('ProtocolsModule.protocols', 'Порог стимуляции (В)'),
            'atest_ven_stimul_thresh' => Yii::t('ProtocolsModule.protocols', 'Порог стимуляции (В)'),
            'atest_atr_el_imp' => Yii::t('ProtocolsModule.protocols', 'Импеданс электрода (Ом)'),
            'atest_ven_el_imp' => Yii::t('ProtocolsModule.protocols', 'Импеданс электрода (Ом)'),
            'test_atr_ampl' => Yii::t('ProtocolsModule.protocols', 'Амплитуда волны P/R (мВ)'),
            'test_ven_ampl' => Yii::t('ProtocolsModule.protocols', 'Амплитуда волны P/R (мВ)'),
            'test_atr_stimul_thresh' => Yii::t('ProtocolsModule.protocols', 'Порог стимуляции (В)'),
            'test_ven_stimul_thresh' => Yii::t('ProtocolsModule.protocols', 'Порог стимуляции (В)'),
            'pacer' => Yii::t('ProtocolsModule.protocols', 'Электрод КС'),
            'pacer_conf' => Yii::t('ProtocolsModule.protocols', 'Конфигурация ЭКС'),
            'pacer_ampl' => Yii::t('ProtocolsModule.protocols', 'Амплитуда волны R (мВ)'),
            'pacer_stimul_thresh' => Yii::t('ProtocolsModule.protocols', 'Порог стимуляции (В)'),
            'pacer_imp' => Yii::t('ProtocolsModule.protocols', 'Импеданс (Ом)'),
            'placement' => Yii::t('ProtocolsModule.protocols', 'Положение'),
            'rtg_time_hmsc' => Yii::t('ProtocolsModule.protocols', 'Сервисный центр Домашнего Мониторинга'),
            'rtg_time_semaphore' => Yii::t('ProtocolsModule.protocols', 'Концепция "Светофор"'),
            'rtg_time_iegm' => Yii::t('ProtocolsModule.protocols', 'ВЭГМ онлайн'),
            'rtg_cl_hmsc' => Yii::t('ProtocolsModule.protocols', 'Сервисный центр Домашнего Мониторинга'),
            'rtg_cl_semaphore' => Yii::t('ProtocolsModule.protocols', 'Концепция "Светофор"'),
            'rtg_cl_iegm' => Yii::t('ProtocolsModule.protocols', 'ВЭГМ онлайн'),
            'rtg_cl_data_suff' => Yii::t('ProtocolsModule.protocols', 'Достаточность данных HM'),
            'cl_hosp_days' => Yii::t(
                'ProtocolsModule.protocols',
                'Количество койко-дней, проведенных в стационаре за год'
            ),
            'cl_disability_days' => Yii::t(
                'ProtocolsModule.protocols',
                'Количество дней нетрудоспособности (на больничном) за год'
            ),
            'cl_amb_calls' => Yii::t('ProtocolsModule.protocols', 'Количество обращений к службе скорой помощи за год'),
            'comment' => Yii::t('ProtocolsModule.protocols', 'Комментарий/Примечание'),
        );
    }

    /**
     * Возвращает допустимые значения количества НЯ
     *
     * @return array
     */
    public function getPossibleFindingsCnt()
    {
        return range(0, 10);
    }

    /**
     * Возвращает массив со значениями для Capture control
     *
     * @return array
     */
    public function getPossibleCaptureControl()
    {
        $data = CaptureControl::model()->findAll();

        $result = [0 => Yii::t('ProtocolsModule.protocols', "не применимо")] + CHtml::listData($data, 'uid', 'name');
        return $result;
    }

    /**
     * Возвращает массив со значениями для Sensitivity
     *
     * @return array
     */
    public function getPossibleSensivityValues()
    {
        return array(
            0 => Yii::t('ProtocolsModule.protocols', "не применимо"),
            1 => Yii::t('ProtocolsModule.protocols', "Auto"),
            2 => Yii::t('ProtocolsModule.protocols', "Другая установка"),
        );
    }

    /**
     * Возвращает опции конфигурации имплантируемого устройства
     *
     * @return array
     */
    public function getImplConfOptions()
    {
        DevicePolarity::enableOnlyActive();
        $data = DevicePolarity::model()->findAll();
        $result = [0 => Yii::t('ProtocolsModule.protocols', "не применимо")] + CHtml::listData($data, 'uid', 'name');
        return $result;
    }

    /**
     * Возвращает опции рейтинга HM
     *
     * @return array
     */
    public function getRatingOptions()
    {
        return array(
            "5" => 5,
            "4" => 4,
            "3" => 3,
            "2" => 2,
            "1" => 1,
        );
    }

    public function getFillingMap()
    {
        return array(
            "atrial_arrhythmic_burden" => array("param" => "atrial_burden", 'transform' => "integer"),
            "pacing_in_a" => array("param" => "atrial_pacing_ap", 'transform' => "integer"),
            "pacing_in_b" => array("param" => "ven_pacing_vp", 'transform' => "integer"),
            "as_vs" => array("param" => "intrinsic_rhythm_as_vs", 'transform' => "integer"),
            "as_vp" => array("param" => "vat_stimulation_as_vp", 'transform' => "integer"),
            "ap_vp" => array("param" => "dual_chamber_pacing_ap_vp", 'transform' => "integer"),
            "ap_vs" => array("param" => "conducted_atrial_pacing_ap_vs", 'transform' => "integer"),

            "atr_conf" => array("param" => "ra_pacing_polarity", 'transform' => "polarity"),
            "req_atr_ampl" => array("param" => "ra_sensing_amplitude_daily_mean", 'transform' => "numeric"),
            "req_atr_el_imp" => array("param" => "ra_pacing_impedance", 'transform' => "numeric"),

            "ven_conf" => array("param" => "rv_sensing_polarity", 'transform' => "polarity"),
            "req_ven_ampl" => array("param" => "rv_sensing_amplitude_daily_mean", 'transform' => "numeric"),
            "req_ven_el_imp" => array("param" => "rv_pacing_impedance", 'transform' => "numeric"),

            "pacer_conf" => array("param" => "lv_sensing_polarity", 'transform' => "polarity"),
            "pacer_ampl" => array("param" => "lv_sensing_amplitude_daily_mean", 'transform' => "numeric"),
            "pacer_imp" => array("param" => "lv_pacing_impedance", 'transform' => "numeric"),
        );
    }

    /**
     * Возвращает массив с полями, возможность заполнения которых зависит от поставленного чекбокса
     * Ключ - название поля-чекбокса
     * Значение - массив названий зависимых полей
     *
     * @return array
     */
    public function getFieldsDependency()
    {
        return array(
            "medical_therapy_changed" => array("medical_therapy_comment"),
            "other_therapy_changed" => array("other_therapy_comment"),
            "pacer" => array("pacer_conf", "pacer_ampl", "pacer_stimul_thresh", "pacer_imp"),
            "sve" => array("sve_imp"),
        );
    }

    public function behaviors()
    {
        return array(
            'NullableAttrsBehavior' => array(
                'class' => 'NullableAttrsBehavior'
            ),
            'FloatDelimiterBehavior' => ['class' => 'FloatDelimiterBehavior'],
            'EAdvancedArBehavior' => array(
                'class' => 'application.extensions.EAdvancedArBehavior'
            ),
            'LoggingBehavior' => array(
                'class' => 'protocols.components.ProtocolDataLoggingBehavior'
            ),
        );
    }
}
