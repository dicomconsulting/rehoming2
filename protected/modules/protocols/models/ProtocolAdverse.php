<?php

/**
 * This is the model class for table "{{protocol_adverse}}".
 *
 * The followings are the available columns in table '{{protocol_adverse}}':
 * @property integer $protocol_uid
 * @property integer $protocol_type
 * @property string $adv_begin_date
 * @property integer $adv_detect_by_hm
 * @property string $adv_detect_by_hm_date
 * @property integer $adv_result
 * @property string $adv_resolve_date
 * @property string $adv_descr
 * @property string $adv_symptoms
 * @property string $adv_actions
 * @property string $adv_actions_result
 * @property integer $adv_iml_relation
 * @property integer $adv_iml_relation_device
 * @property integer $adv_iml_relation_electr
 * @property string $impl_model
 * @property string $impl_sn
 * @property integer $impl_expl
 * @property integer $adv_lethal
 * @property integer $adv_injury
 * @property integer $adv_perm_damage
 * @property integer $adv_hospitalization
 * @property integer $adv_surg
 * @property integer $adv_distress
 * @property integer $adv_prevented
 * @property string $adv_death_date
 * @property string $adv_death_cause
 * @property integer $adv_death_cardio
 * @property integer $adv_death_arr
 * @property string $adv_comment
 *
 * The followings are the available model relations:
 * @property Protocol $protocol
 */
class ProtocolAdverse extends CActiveRecord implements FieldDependencyDescriptorInterface, ProtocolFillableInterface
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{protocol_adverse}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('protocol_uid', 'required'),
            array(
                'protocol_uid, protocol_type, adv_detect_by_hm, adv_result, adv_iml_relation,' .
                ' adv_iml_relation_device, adv_iml_relation_electr, impl_expl, adv_lethal, adv_injury,' .
                ' adv_perm_damage, adv_hospitalization, adv_surg, adv_distress, adv_prevented, adv_death_cardio,' .
                ' adv_death_arr',
                'numerical',
                'integerOnly' => true
            ),
            array('impl_model, impl_sn', 'length', 'max' => 255),
            array(
                'adv_begin_date, adv_detect_by_hm_date, adv_resolve_date, adv_descr, adv_symptoms, adv_actions,' .
                ' adv_actions_result, adv_death_date, adv_death_cause, adv_comment',
                'safe'
            ),
            array(
                'adv_detect_by_hm_date,',
                'ProtocolsDependencyValidator',
                'message' => Yii::t('ProtocolsModule.protocols', 'Поле "{attribute}" должно быть заполнено')
            ),

            array(
                'adv_resolve_date',
                'compare',
                'compareAttribute'=>'adv_begin_date',
                'operator'=>'>',
                'allowEmpty'=>true,
                'message'=>Yii::t('ProtocolsModule.protocols', '{attribute} должно быть больше чем "{compareValue}".')
            ),

            array(
                'adv_resolve_date',
                'compare',
                'compareAttribute'=>'adv_detect_by_hm_date',
                'operator'=>'>',
                'allowEmpty'=>true,
                'message'=>Yii::t('ProtocolsModule.protocols', '{attribute} должно быть больше чем "{compareValue}".')
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'protocol' => array(self::BELONGS_TO, 'Protocol', 'protocol_uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'protocol_uid' => Yii::t('ProtocolsModule.protocols', 'Протокол Uid'),
            'protocol_type' => Yii::t('ProtocolsModule.protocols', 'Тип отчета'),
            'adv_begin_date' => Yii::t('ProtocolsModule.protocols', 'Дата начала нежелательного явления'),
            'adv_detect_by_hm' => Yii::t(
                'ProtocolsModule.protocols',
                'НЯ обнаружено благодаря функции Home Monitoring'
            ),
            'adv_detect_by_hm_date' => Yii::t('ProtocolsModule.protocols', 'Дата обнаружения'),
            'adv_result' => Yii::t('ProtocolsModule.protocols', 'Исход'),
            'adv_resolve_date' => Yii::t('ProtocolsModule.protocols', 'Дата разрешения НЯ'),
            'adv_descr' => Yii::t('ProtocolsModule.protocols', 'Описание нежелательного явления'),
            'adv_symptoms' => Yii::t('ProtocolsModule.protocols', 'Симптомы'),
            'adv_actions' => Yii::t('ProtocolsModule.protocols', 'Предпринятые действия'),
            'adv_actions_result' => Yii::t('ProtocolsModule.protocols', 'Результат'),
            'adv_iml_relation' => Yii::t('ProtocolsModule.protocols', 'Связь нежелательного явления с имплантатом'),
            'adv_iml_relation_device' => Yii::t(
                'ProtocolsModule.protocols',
                'Укажите имплантат, связанный с нежелательным явлением'
            ),
            'adv_iml_relation_electr' => Yii::t('ProtocolsModule.protocols', 'Электрод'),
            'impl_model' => Yii::t('ProtocolsModule.protocols', 'Модель'),
            'impl_sn' => Yii::t('ProtocolsModule.protocols', 'Серийный номер'),
            'impl_expl' => Yii::t('ProtocolsModule.protocols', 'Прибор эксплантирован'),
            'adv_lethal' => '4.1 ' . Yii::t('ProtocolsModule.protocols', 'Нежелательное явление привело к смерти пациента'),
            'adv_injury' => '4.2.1 ' . Yii::t('ProtocolsModule.protocols', 'жизнеугрожающее заболевание или ранение'),
            'adv_perm_damage' => '4.2.2 ' . Yii::t(
                'ProtocolsModule.protocols',
                'хроническое или необратимое поражение структуры или функции организма'
            ),
            'adv_hospitalization' => '4.2.3 ' . Yii::t(
                'ProtocolsModule.protocols',
                'госпитализация или продление госпитализации'
            ),
            'adv_surg' => '4.2.4 ' . Yii::t(
                'ProtocolsModule.protocols',
                'медицинское или хирургическое вмешательство для предупреждения поражения структуры или ' .
                'функции организма'
            ),
            'adv_distress' => '4.3 ' . Yii::t(
                'ProtocolsModule.protocols',
                'НЯ привело к дистрессу, смерти плода или врожденной аномалии, или дефекту рождения'
            ),
            'adv_prevented' => '4.4 ' . Yii::t(
                'ProtocolsModule.protocols',
                'Только для НЯ, связанных с прибором: явление, вероятно, привело бы к одному из указанных ' .
                'выше результатов, если бы не были предприняты правильные действия, вмешательства, или, если бы ' .
                'обстоятельства складывались менее благоприятно'
            ),
            'adv_death_date' => Yii::t('ProtocolsModule.protocols', 'Дата смерти'),
            'adv_death_cause' => Yii::t('ProtocolsModule.protocols', 'Основная причина смерти'),
            'adv_death_cardio' => Yii::t('ProtocolsModule.protocols', 'Сердечно-сосудистая причина'),
            'adv_death_arr' => Yii::t('ProtocolsModule.protocols', 'Аритмическая причина'),
            'adv_comment' => Yii::t('ProtocolsModule.protocols', 'Примечание'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ProtocolAdverse the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Возвращает возможные типы отчета о НЯ
     *
     * @return array
     */
    public function getReportTypes()
    {
        return array(
            0 => Yii::t('ProtocolsModule.protocols', "предварительный"),
            1 => Yii::t('ProtocolsModule.protocols', "окончательный"),
        );
    }

    /**
     * Возвращает возможные исходы нежелательного явления
     *
     * @return array
     */
    public function getAdverseResults()
    {
        $data = AdverseResult::model()->findAll();
        return CHtml::listData($data, 'uid', 'name');
    }

    /**
     * Возвращает возможные варианты связи имплантата и НЯ
     *
     * @return array
     */
    public function getAdverseImplRelation()
    {
        return array(
            0 => Yii::t('ProtocolsModule.protocols', "Явное отсутствие связи (к разделу 4)"),
            1 => Yii::t('ProtocolsModule.protocols', "Связь возможна (комментарий в разделе 6)"),
            2 => Yii::t('ProtocolsModule.protocols', "Связь очевидна (комментарий в разделе 6)"),
        );
    }

    /**
     * Возвращает список устройств
     *
     * @return array
     */
    public function getAdverseDevice()
    {
        return array(
            0 => Yii::t('ProtocolsModule.protocols', "не применимо"),
            1 => Yii::t('ProtocolsModule.protocols', "ИКД"),
            2 => Yii::t('ProtocolsModule.protocols', "ЭКС"),
            3 => Yii::t('ProtocolsModule.protocols', "CRT-P"),
            4 => Yii::t('ProtocolsModule.protocols', "CRT-D"),
        );
    }

    /**
     * Возвращает список устройств
     *
     * @return array
     */
    public function getAdverseElectrode()
    {
        return array(
            0 => Yii::t('ProtocolsModule.protocols', "не применимо"),
            1 => Yii::t('ProtocolsModule.protocols', "ПП"),
            2 => Yii::t('ProtocolsModule.protocols', "ПЖ"),
            3 => Yii::t('ProtocolsModule.protocols', "КС"),
        );
    }

    /**
     * Возвращает список вариантов ответов о связи причин смерти
     *
     * @return array
     */
    public function getDeathCause()
    {
        return array(
            0 => Yii::t('ProtocolsModule.protocols', "неизвестно"),
            1 => Yii::t('ProtocolsModule.protocols', "да"),
            2 => Yii::t('ProtocolsModule.protocols', "нет"),
        );
    }


    /**
     * Возвращает массив с полями, возможность заполнения которых зависит от поставленного чекбокса
     * Ключ - название поля-чекбокса
     * Значение - массив названий зависимых полей
     *
     * @return array
     */
    public function getFieldsDependency()
    {
        return array(
            "adv_detect_by_hm" => array("adv_detect_by_hm_date"),
        );
    }

    /**
     * Возвращает текстовое значение исхода НЯ
     *
     * @return mixed
     */
    public function getResult()
    {
        $results = $this->getAdverseResults();
        return isset($results[$this->adv_result]) ? $results[$this->adv_result] : "";
    }

    public function behaviors()
    {
        return array(
            'NullableAttrsBehavior' => array(
                'class' => 'NullableAttrsBehavior'
            ),
            'LoggingBehavior' => array(
                'class' => 'protocols.components.ProtocolDataLoggingBehavior'
            ),
        );
    }

    public function getFillingMap()
    {
        return array(
            "impl_model" => array("source" => "device", "param" => "device_model"),
            "impl_sn" => array("source" => "device", "param" => "serial_number"),
        );
    }
}
