<?php


class ProtocolsModule extends CWebModule
{

    public function init()
    {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application

        // import the module-level models and components
        $this->setImport(
            array(
                'protocols.models.*',
                'protocols.components.*',
                'protocols.widgets.*',
                'protocols.widgets.print.*',
                'directory.models.*',
                'patient.models.*',
                'patient.components.*',
                'admin.models.User',
                'statisticsNew.helpers.PatientStats',
                'statisticsUpdate.helpers.PatientStats',
                'statistics2018.helpers.PatientStats',
            )
        );
    }

    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        } else {
            return false;
        }
    }
}
