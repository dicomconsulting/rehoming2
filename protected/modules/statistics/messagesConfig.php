<?php

return array(
    'sourcePath'  => YiiBase::getPathOfAlias("statistics"),
    'messagePath' => YiiBase::getPathOfAlias("statistics") . DIRECTORY_SEPARATOR . "messages",
    'languages'   => array('en','ru'),
    'fileTypes'   => array('php','phtml'),
    'exclude'     => array(),
    //'translator'  => '',
);