<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 18.12.13
 * Time: 18:08
 */

class ProcessedReportData
{
    /**
     * @var int
     */
    public $total;

    /**
     * @var mixed
     */
    public $value;
}
