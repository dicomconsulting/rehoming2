<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.12.13
 * Time: 11:48
 *
 * Преобразователь - заглушка
 */

class EmptyTransformer extends AbstractReportValueTransformer
{
    /**
     * @param SourceReportValue $value
     * @return SourceReportValue
     */
    public function transform(SourceReportValue $value)
    {
        return $value;
    }
}
