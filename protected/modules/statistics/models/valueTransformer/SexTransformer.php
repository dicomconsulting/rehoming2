<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.12.13
 * Time: 11:48
 *
 * Преобразует пол пациентов
 */

class SexTransformer extends AbstractReportValueTransformer
{
    /**
     * @param SourceReportValue $value
     * @return SourceReportValue
     */
    public function transform(SourceReportValue $value)
    {
        switch($value->value) {
            case Patient::FEMALE:
                $value->value = Yii::t("StatisticsModule.statistics", "Женщины");
                break;
            case Patient::MALE:
                $value->value = Yii::t("StatisticsModule.statistics", "Мужчины");
                break;
            case Patient::UNKNOWN:
            default:
                $value->value = Yii::t("StatisticsModule.statistics", "неизвестно");
                break;
        }

        return $value;
    }
}
