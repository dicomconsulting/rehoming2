<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.12.13
 * Time: 11:48
 *
 * Преобразует значения на данные из соответствующего словаря
 */

class DictionaryTransformer extends AbstractReportValueTransformer
{
    /**
     * @param SourceReportValue $value
     * @return SourceReportValue
     */
    public function transform(SourceReportValue $value)
    {
        $className = $this->param;

        if ($value->value) {
            /** @var CActiveRecord $row */
            $row = $className::model()->findByPk($value->value);

            if ($row) {
                $value->value = $row->name;
            }
        } else {
            $value->value = Yii::t("StatisticsModule.statistics", "Поле не заполнено");
        }

        return $value;
    }
}
