<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.12.13
 * Time: 11:48
 *
 * Преобразует булевые значения
 */

class BoolTransformer extends AbstractReportValueTransformer
{
    /**
     * @param SourceReportValue $value
     * @return SourceReportValue
     */
    public function transform(SourceReportValue $value)
    {
        if ($value->value) {
            $value->value = Yii::t("StatisticsModule.statistics", "Да");
        } else {
            $value->value = Yii::t("StatisticsModule.statistics", "Нет");
        }

        return $value;
    }
}
