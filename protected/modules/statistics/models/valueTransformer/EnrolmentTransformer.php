<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.12.13
 * Time: 11:48
 *
 * Преобразовывает значения, специфичные для протокола включения
 */

class EnrolmentTransformer extends AbstractReportValueTransformer
{
    /**
     * @param SourceReportValue $value
     * @return SourceReportValue
     */
    public function transform(SourceReportValue $value)
    {
        switch($this->param) {
            case "defaultDiseases":
                $this->tansformDisease($value);
                break;
        }

        return $value;
    }

    protected function tansformDisease(SourceReportValue $value)
    {
        $diseases = ProtocolEnrolment::model()->getDefaultDiseases();

        if (isset($diseases[$value->value])) {
            $value->value = $diseases[$value->value];
        }

        return $value;
    }
}
