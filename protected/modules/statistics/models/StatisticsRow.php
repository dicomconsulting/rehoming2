<?php

/**
 * This is the model class for table "{{statistics_row}}".
 *
 * The followings are the available columns in table '{{statistics_row}}':
 * @property integer $uid
 * @property string $source
 * @property string $source_field
 * @property string $value_transformer
 * @property string $value_transformer_param
 * @property integer $statistics_section_uid
 * @property string $data_processor
 * @property string $renderer
 * @property string $name
 *
 * The followings are the available model relations:
 * @property StatisticsRowI18n[] $statisticsRowI18ns
 */
class StatisticsRow extends I18nActiveRecord
{
    /**
     * @var AbstractReportSource
     */
    public $reportSource;

    /**
     * @var AbstractReportDataProcessor
     */
    public $dataProcessor;

    /**
     * @var AbstractReportRenderer
     */
    public $reportRenderer;

    /**
     * @var AbstractReportValueTransformer
     */
    public $valueTransformer;


    public function primaryKey()
    {
        return "uid";
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return StatisticsRow the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{statistics_row}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('source, source_field, statistics_section_uid, data_processor, renderer', 'required'),
            array('statistics_section_uid', 'numerical', 'integerOnly' => true),
            array(
                'source, source_field, value_transformer, value_transformer_param, data_processor, renderer',
                'length',
                'max' => 255
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'statisticsRowI18ns' => array(self::HAS_MANY, 'StatisticsRowI18n', 'uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid' => 'Uid',
            'source' => 'Source',
            'source_field' => 'Source Field',
            'value_transformer' => 'Value Transformer',
            'value_transformer_param' => 'Value Transformer Param',
            'statistics_section_uid' => 'Statistics Section Uid',
            'data_processor' => 'Data Processor',
            'renderer' => 'Renderer',
        );
    }

    protected function afterFind()
    {
        $this->reportSource = ReportBuilder::createSource($this->source, $this->source_field);

        $this->dataProcessor = ReportBuilder::createDataProcessor($this->data_processor);

        $this->reportRenderer = ReportBuilder::createRenderer($this->renderer, $this);

        $this->valueTransformer = ReportBuilder::createValueTransformer(
            $this->value_transformer,
            $this->value_transformer_param
        );
    }


}
