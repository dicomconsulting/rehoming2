<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 18.12.13
 * Time: 18:12
 */

class ReportBuilder
{

    /**
     * Возвращает отрендеренный отчет
     *
     * @return string
     */
    public function build()
    {
        $result = "";

        /** @var StatisticsRow[] $rows */
        $rows = StatisticsRow::model()->findAll();

        foreach ($rows as $row) {
            $values = $row->reportSource->getValues();

            foreach ($values as &$value) {
                $value = $row->valueTransformer->transform($value);
            }
            unset($value);

            $result .= $row->reportRenderer->renderReport(
                $row->dataProcessor->process($values)
            );
        }

        return $result;
    }

    /**
     * Создает экземлпяр источника данных
     *
     * @param string $source
     * @param string $param
     * @return AbstractReportSource
     */
    public static function createSource($source, $param)
    {
        Yii::import("statistics.models.source.*");

        $className = self::formatName($source) . "Source";

        return new $className($param);
    }

    /**
     * Создает экземлпяр обработчика данных
     *
     * @param string $dataProcessorName
     * @return AbstractReportDataProcessor
     */
    public static function createDataProcessor($dataProcessorName)
    {
        Yii::import("statistics.models.dataProcessor.*");

        $className = self::formatName($dataProcessorName) . "Processor";

        return new $className();
    }

    /**
     * Создает экземлпяр преобразовывателя данных
     *
     * @param string $transformerName
     * @param string $param
     * @return AbstractReportValueTransformer
     */
    public static function createValueTransformer($transformerName, $param)
    {
        Yii::import("statistics.models.valueTransformer.*");

        if (!$transformerName) {
            $transformerName = "empty";
        }

        $className = self::formatName($transformerName) . "Transformer";

        return new $className($param);
    }

    /**
     * Создает экземлпяр рендерера строки отчета
     *
     * @param string $rendererName
     * @param StatisticsRow $row
     * @return AbstractReportRenderer
     */
    public static function createRenderer($rendererName, StatisticsRow $row)
    {
        Yii::import("statistics.models.renderer.*");

        $className = self::formatName($rendererName);

        /** @var AbstractReportRenderer $renderer */
        $renderer = new $className();
        $renderer->setReportRow($row);
        return $renderer;
    }

    protected static function formatName($name)
    {
        return ucfirst($name);
    }
}
