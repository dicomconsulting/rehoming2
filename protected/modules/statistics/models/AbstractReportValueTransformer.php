<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 18.12.13
 * Time: 18:04
 */

abstract class AbstractReportValueTransformer
{

    /**
     * Служеный параметр, который может использоваться трансформером лдя соотнеседия значений с расшифровкой, например,
     * название словаря с данными
     *
     * @var
     */
    protected $param;

    public function __construct($param)
    {
        $this->param = $param;
    }

    /**
     * @param SourceReportValue $value
     * @return SourceReportValue
     */
    abstract public function transform(SourceReportValue $value);
}
