<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 18.12.13
 * Time: 18:01
 */

abstract class AbstractReportSource
{
    protected $field;

    public function __construct($field)
    {
        $this->field = $field;
    }

    /**
     * @return SourceReportValue[]
     */
    abstract public function getValues();
}
