<?php

/**
 * This is the model class for table "{{statistics_section_i18n}}".
 *
 * The followings are the available columns in table '{{statistics_section_i18n}}':
 * @property string $name
 * @property integer $uid
 * @property string $locale
 *
 * The followings are the available model relations:
 * @property StatisticsSection $u
 */
class StatisticsSectionI18n extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return StatisticsSectionI18n the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{statistics_section_i18n}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'u' => array(self::BELONGS_TO, 'StatisticsSection', 'uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'name' => 'Name',
            'uid' => 'Uid',
            'locale' => 'Locale',
        );
    }
}
