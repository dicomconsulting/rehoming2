<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 18.12.13
 * Time: 18:09
 */

abstract class AbstractReportRenderer extends CWidget
{

    /**
     * @var StatisticsRow
     */
    protected $reportRow;

    public function __construct($owner = null)
    {
        parent::__construct($owner);
    }

    public function getViewPath($checkTheme = false)
    {
        return Yii::getPathOfAlias("statistics.views.reportRenderer");
    }


    /**
     * @param ProcessedReportData $data
     * @return string
     */
    public function renderReport(ProcessedReportData $data)
    {
        $templateName = $this->getTemplateName();
        return $this->render($templateName, ['data' => $data, "row" => $this->reportRow], true);
    }

    protected function getTemplateName()
    {
        $calledClass = get_called_class();
        return lcfirst($calledClass);
    }

    /**
     * @param \StatisticsRow $reportRow
     */
    public function setReportRow($reportRow)
    {
        $this->reportRow = $reportRow;
    }
}
