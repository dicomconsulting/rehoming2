<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 18.12.13
 * Time: 18:20
 */

abstract class AbstractProtocolSource extends AbstractReportSource
{

    protected $protocolType;
    protected $protocols;

    protected function getDataByField($patientIds)
    {
        $result = [];

        foreach ($patientIds as $patientId => $patient) {
            /** @var Protocol $protocol */
            $protocol = Protocol::model()->findByPk($patient['protocol_uid']);

            $protocolName = ProtocolDictionary::model()
                ->findByPk($this->protocolType)->getProtocolName();

            $result[$patientId] = [
                'value' => $protocol->$protocolName->{$this->field}
            ];
        }

        return $result;
    }

    /**
     * @return array
     */
    protected function getPatientIds()
    {
        $this->protocols = Research::getClosedProtocols($this->protocolType);

        $patients = [];

        foreach ($this->protocols as $protocol) {
            $patients[] = [
                'date' => $protocol->research->date_begin,
                'protocol_uid' => $protocol->uid
            ];
        }

        return $patients;
    }

    /**
     * @param $values
     * @param $patients
     * @return array
     */
    protected function generateResult($values, $patients)
    {
        $result = [];

        foreach ($values as $id => $val) {
            $row = new SourceReportValue();
            $row->date = $patients[$id]['date'];
            $row->value = $val['value'];
            $row->patientUid = $id;
            $result[] = $row;
        }

        return $result;
    }
}
