<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 18.12.13
 * Time: 18:20
 */

class ProtocolRegularSource extends AbstractProtocolSource
{
    protected $protocolType = ProtocolDictionary::PROTOCOL_REGULAR;

    /**
     * @return SourceReportValue[]
     * @throws CException
     */
    public function getValues()
    {
        $patientIds = $this->getPatientIds();

        switch ($this->field) {
            case "total_detections":
                $result = $this->getDetectionsValues($patientIds);
                break;
            default:
                $values = $this->getDataByField($patientIds);
                $result = $this->generateResult($values, $patientIds);
        }

        return $result;
    }

    protected function getDetectionsValues($patientIds)
    {
        $values = [];

        $params = [
            'elect_displ_detect_date',
            'hf_detect_date',
            'idc_fail_detect_date',
        ];

        $protocolIds = [];

        foreach ($patientIds as $patientId => $patient) {
            $protocolIds[$patient['protocol_uid']] =  $patientId;
        }

        $protocolName = ProtocolDictionary::model()
            ->findByPk($this->protocolType)->getProtocolName();

        foreach ($this->protocols as $protocol) {
            /** @var Protocol $protocol */
            foreach ($params as $param) {
                if ($protocol->$protocolName->$param) {
                    $sv = new SourceReportValue();
                    $sv->date = $protocol->date;
                    $sv->patientUid = $protocolIds[$protocol->uid];
                    $sv->value = 1;
                    $values[] = $sv;
                }
            }
        }

        return $values;
    }

}
