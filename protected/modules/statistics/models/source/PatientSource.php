<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 18.12.13
 * Time: 18:20
 */

class PatientSource extends AbstractReportSource
{

    /**
     * @return SourceReportValue[]
     * @throws CException
     */
    public function getValues()
    {
        $patientIds = $this->getPatientIds();

        switch ($this->field) {
            case "enrolment_age":
                $values = $this->getPatientsAge($patientIds);
                break;
            case "sex":
                $values = $this->getPatientsSex($patientIds);
                break;
            default:
                throw new CException("Parameter '{$this->field}' can not be fetched.");
        }

        $result = $this->generateResult($values, $patientIds);

        return $result;
    }

    protected function getPatientsSex($patientIds)
    {
        /** @var Patient[] $patients */
        $patients = Patient::model()->findAllByPk(array_keys($patientIds));

        $result = [];

        foreach ($patients as $patient) {
            $result[$patient->uid] = ['value' => $patient->sex];
        }

        return $result;
    }

    protected function getPatientsAge($patientIds)
    {
        /** @var Patient[] $patients */
        $patients = Patient::model()->findAllByPk(array_keys($patientIds));

        $result = [];

        foreach ($patients as $patient) {
            $result[$patient->uid] = ['value' => $this->calculateAge(
                $patient->birth_date,
                $patientIds[$patient->uid]['date']
            )];
        }

        return $result;
    }

    /**
     * @param RhDateTime|boolean $birthDate
     * @param RhDateTime $currDate
     * @return null
     */
    protected function calculateAge($birthDate, RhDateTime $currDate)
    {
        if ($birthDate) {
            $dateB = $birthDate;
            $dateC = $currDate;

            $interval = $dateB->diff($dateC);

            return $interval->y;
        } else {
            return null;
        }
    }

    /**
     * @return array
     */
    protected function getPatientIds()
    {
        $protocols = Research::getClosedProtocols(ProtocolDictionary::PROTOCOL_ENROLMENT);

        $patients = [];

        foreach ($protocols as $protocol) {
            $patients[$protocol->research->patient_uid] = ['date' => $protocol->research->date_begin];
        }

        return $patients;
    }



    /**
     * @param $values
     * @param $patients
     * @return array
     */
    protected function generateResult($values, $patients)
    {
        $result = [];

        foreach ($values as $id => $val) {
            $row = new SourceReportValue();
            $row->date = $patients[$id]['date'];
            $row->value = $val['value'];
            $row->patientUid = $id;
            $result[] = $row;
        }

        return $result;
    }
}
