<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 18.12.13
 * Time: 18:20
 */

class ProtocolEvolutionSource extends AbstractProtocolSource
{

    protected static $messagesCache = [];

    protected $protocolType = ProtocolDictionary::PROTOCOL_EVOLUTION;

    /**
     * @return SourceReportValue[]
     * @throws CException
     */
    public function getValues()
    {
        $patientIds = $this->getPatientIds();

        switch ($this->field) {
            case "red_messages":
                $result = $this->getMessagesValues($patientIds, "red");
                break;
            case "yellow_messages":
                $result = $this->getMessagesValues($patientIds, "yellow");
                break;
            case "total_messages":
                $result = $this->getMessagesValues($patientIds);
                break;
            default:
                $values = $this->getDataByField($patientIds);
                $result = $this->generateResult($values, $patientIds);
        }

        return $result;
    }

    /**
     * @param $patientIds
     * @param string|null $level
     * @return SourceReportValue[]
     */
    public function getMessagesValues($patientIds, $level = null)
    {
        $values = [];
        $protocolIds = [];

        foreach ($patientIds as $patientId => $patient) {
            $protocolIds[$patient['protocol_uid']] =  $patientId;
        }

        $rows = ProtocolFindingRelation::model()->findAllByAttributes(
            ["protocol_uid" => array_keys($protocolIds)]
        );

        if ($level) {
            $tmpRows = [];
            foreach ($rows as $row) {
                $message = $this->getMessage($row['finding_uid']);
                switch ($level) {
                    case "red":
                        if ($message->getLevel() == AbstractOption::ALERT_LEVEL_RED_WITH_NOTICE) {
                            $tmpRows[] = $row;
                        }
                        break;
                    case "yellow":
                        if (in_array(
                            $message->getLevel(),
                            [AbstractOption::ALERT_LEVEL_YELLOW, AbstractOption::ALERT_LEVEL_YELLOW_WITH_NOTICE]
                        )) {
                            $tmpRows[] = $row;
                        }
                        break;
                }
            }

            $rows = $tmpRows;
        }

        foreach ($rows as $row) {
            $message = $this->getMessage($row['finding_uid']);
            $sv = new SourceReportValue();
            $sv->value = $row['finding_uid'];
            $sv->date = $message->data->date_create;
            $sv->patientUid = $protocolIds[$row['protocol_uid']];

            $values[] = $sv;
        }

        return $values;
    }

    /**
     * @param $uid
     * @return Message
     */
    protected function getMessage($uid)
    {
        if (!isset(self::$messagesCache[$uid])) {
            self::$messagesCache[$uid] = Message::createByUid($uid);
        }

        return self::$messagesCache[$uid];
    }
}
