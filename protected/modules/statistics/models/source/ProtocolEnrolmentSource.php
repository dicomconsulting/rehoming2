<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 18.12.13
 * Time: 18:20
 */

class ProtocolEnrolmentSource extends AbstractProtocolSource
{

    protected $protocolType = ProtocolDictionary::PROTOCOL_ENROLMENT;

    /**
     * @return SourceReportValue[]
     * @throws CException
     */
    public function getValues()
    {
        $patientIds = $this->getPatientIds();

        switch ($this->field) {
            case "etiology":
                $result = $this->getEtiologyValues($patientIds);
                break;
            case "surgery":
                $result = $this->getSurgeryValues($patientIds);
                break;
            case "pharm":
                $result = $this->getPharmValues($patientIds);
                break;
            default:
                $values = $this->getDataByField($patientIds);
                $result = $this->generateResult($values, $patientIds);
        }

        return $result;
    }

    public function getEtiologyValues($patientIds)
    {
        $result = [];

        $protocolIds = [];

        foreach ($patientIds as $patientId => $patient) {
            $protocolIds[$patient['protocol_uid']] =  $patientId;
        }

        $diseases = ProtocolEnrolment::model()->getDefaultDiseases();

        // выбираем только болезни по-умолчанию
        $rows = ProtocolEnrolmentEtiology::model()->findAllByAttributes(
            ["protocol_uid" => array_keys($protocolIds), "disease_uid" => array_keys($diseases)]
        );

        foreach ($patientIds as $patient) {
            foreach ($diseases as $disId => $dis) {
                $sv = new SourceReportValue();
                $sv->value = $disId;
                $sv->patientUid = $protocolIds[$patient['protocol_uid']];

                foreach ($rows as $row) {
                    if ($row['protocol_uid'] == $patient['protocol_uid'] && $row['disease_uid'] == $disId) {
                        $sv->date = $patientIds[$protocolIds[$row['protocol_uid']]]['date'];
                    }
                }

                $result[] = $sv;
            }
        }


        return $result;
    }

    public function getSurgeryValues($patientIds)
    {
        $result = [];

        $protocolIds = [];

        foreach ($patientIds as $patientId => $patient) {
            $protocolIds[$patient['protocol_uid']] =  $patientId;
        }

        $surgeries = Surgery::model()->findAll();
        $surgIds = [];

        foreach ($surgeries as $surg) {
            /** @var Surgery $surg */
            $surgIds[] = $surg->uid;
        }

        // выбираем только болезни по-умолчанию
        $rows = ProtocolEnrolmentSurgery::model()->findAllByAttributes(
            ["protocol_uid" => array_keys($protocolIds), "surgery_uid" => $surgIds]
        );

        foreach ($patientIds as $patient) {
            foreach ($surgIds as $surgId) {
                $sv = new SourceReportValue();
                $sv->value = $surgId;
                $sv->patientUid = $protocolIds[$patient['protocol_uid']];

                foreach ($rows as $row) {
                    if ($row['protocol_uid'] == $patient['protocol_uid'] && $row['surgery_uid'] == $surgId) {
                        $sv->date = $patientIds[$protocolIds[$row['protocol_uid']]]['date'];
                    }
                }

                $result[] = $sv;
            }
        }

        return $result;
    }

    /**
     * Возвращает значения с группами препаратов. Если пациент принимает препараты из этой группы, то дата прописана,
     * если нет - то нет.
     *
     * @param $patientIds
     * @return array
     */
    public function getPharmValues($patientIds)
    {
        $result = [];
        $protocolIds = [];

        foreach ($patientIds as $patientId => $patient) {
            $protocolIds[$patient['protocol_uid']] =  $patientId;
        }

        $pharmGroups = ProtocolEnrolment::getPharmsTmp();

        $acceptablePharmIds = [];
        foreach ($pharmGroups as $groupKey => $group) {
            foreach ($group['products'] as $product) {
                /** @var PharmProductTmp $product */
                $acceptablePharmIds[$product->id] = $groupKey;
            }
        }

        $rows = ProtocolEnrolmentPharm::model()->findAllByAttributes(
            ["protocol_uid" => array_keys($protocolIds), "pharm_product_uid" => array_keys($acceptablePharmIds)]
        );

        $relProtocolsGroup = [];

        foreach ($rows as $row) {
            $relProtocolsGroup[$row['protocol_uid']][$acceptablePharmIds[$row['pharm_product_uid']]]
                = $acceptablePharmIds[$row['pharm_product_uid']];
        }

        foreach ($patientIds as $patient) {
            foreach ($pharmGroups as $groupId => $groupData) {
                $sv = new SourceReportValue();
                $sv->value = $groupData['name'];
                $sv->patientUid = $protocolIds[$patient['protocol_uid']];

                if (isset($relProtocolsGroup[$patient['protocol_uid']][$groupId])) {
                    $sv->date = $patient['date'];
                }

                $result[] = $sv;
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    protected function getPatientIds()
    {
        $protocols = Research::getClosedProtocols(ProtocolDictionary::PROTOCOL_ENROLMENT);

        $patients = [];

        foreach ($protocols as $protocol) {
            $patients[$protocol->research->patient_uid] = [
                'date' => $protocol->research->date_begin,
                'protocol_uid' => $protocol->uid
            ];
        }

        return $patients;
    }
}
