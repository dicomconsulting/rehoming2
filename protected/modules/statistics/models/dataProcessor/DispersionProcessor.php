<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.12.13
 * Time: 11:12
 */

class DispersionProcessor extends AbstractReportDataProcessor
{
    /**
     * @param SourceReportValue[] $sourceValues
     * @return ProcessedReportData
     */
    public function process($sourceValues)
    {
        $vals = [];
        $cnt = 0;

        foreach ($sourceValues as $val) {
            $cnt++;

            if (!isset($vals[$val->value])) {
                $vals[$val->value] = 1;
            } else {
                $vals[$val->value]++;
            }
        }

        $result = new ProcessedReportData();
        $result->value = $vals;
        $result->total = $cnt;
        return $result;
    }
}
