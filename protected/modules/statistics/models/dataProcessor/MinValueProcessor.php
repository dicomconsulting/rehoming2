<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.12.13
 * Time: 11:12
 */

class MinValueProcessor extends AbstractReportDataProcessor
{
    /**
     * @param SourceReportValue[] $sourceValues
     * @return ProcessedReportData
     */
    public function process($sourceValues)
    {
        $min = null;
        $cnt = 0;

        foreach ($sourceValues as $val) {
            $cnt++;

            if (is_null($min)) {
                $min = $val->value;
                continue;
            }

            if ($val->value < $min) {
                $min = $val->value;
            }
        }

        $result = new ProcessedReportData();
        $result->value = $min;
        $result->total = $cnt;
        return $result;
    }
}
