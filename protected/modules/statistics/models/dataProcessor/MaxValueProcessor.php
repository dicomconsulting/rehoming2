<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.12.13
 * Time: 11:12
 */

class MaxValueProcessor extends AbstractReportDataProcessor
{
    /**
     * @param SourceReportValue[] $sourceValues
     * @return ProcessedReportData
     */
    public function process($sourceValues)
    {
        $max = null;
        $cnt = 0;

        foreach ($sourceValues as $val) {
            $cnt++;

            if (is_null($max)) {
                $max = $val->value;
                continue;
            }

            if ($val->value > $max) {
                $max = $val->value;
            }
        }

        $result = new ProcessedReportData();
        $result->value = $max;
        $result->total = $cnt;
        return $result;
    }
}
