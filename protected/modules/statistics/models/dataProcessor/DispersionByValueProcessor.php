<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.12.13
 * Time: 11:12
 *
 * Обрабатывает распределение среди каждого значения по принципу да/нет, если дата указана - считается что значение
 * присутствует, иначе - нет
 */

class DispersionByValueProcessor extends AbstractReportDataProcessor
{
    /**
     * @param SourceReportValue[] $sourceValues
     * @return ProcessedReportData
     */
    public function process($sourceValues)
    {
        $vals = [];
        $cnt = 0;

        foreach ($sourceValues as $val) {
            $cnt++;

            if (!isset($vals[$val->value])) {
                $vals[$val->value] = ['totalCnt' => 1, 'cnt' => 0];
            } else {
                $vals[$val->value]['totalCnt']++;
            }

            if ($val->date) {
                $vals[$val->value]['cnt']++;
            }
        }

        $result = new ProcessedReportData();
        $result->value = $vals;
        $result->total = $cnt;
        return $result;
    }
}
