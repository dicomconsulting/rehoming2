<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.12.13
 * Time: 11:12
 */

class CountProcessor extends AbstractReportDataProcessor
{
    /**
     * @param SourceReportValue[] $sourceValues
     * @return ProcessedReportData
     */
    public function process($sourceValues)
    {
        $cnt = count($sourceValues);

        $uniqPat = [];

        foreach ($sourceValues as $val) {
            $uniqPat[$val->patientUid] = 1;
        }

        $result = new ProcessedReportData();
        $result->value = $cnt;
        $result->total = count($uniqPat);
        return $result;
    }
}
