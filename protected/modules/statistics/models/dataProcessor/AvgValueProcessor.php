<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.12.13
 * Time: 11:12
 */

class AvgValueProcessor extends AbstractReportDataProcessor
{
    /**
     * @param SourceReportValue[] $sourceValues
     * @return ProcessedReportData
     */
    public function process($sourceValues)
    {
        $summ = null;
        $cnt = 0;

        foreach ($sourceValues as $val) {
            $cnt++;

            if (is_null($val->value)) {
                continue;
            }

            $summ += $val->value;
        }

        $result = new ProcessedReportData();
        $result->value = $cnt ? $summ/$cnt : 0;
        $result->total = $cnt;
        return $result;
    }
}
