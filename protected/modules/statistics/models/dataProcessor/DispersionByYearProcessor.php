<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.12.13
 * Time: 11:12
 *
 * Обрабатывает распределение среди каждого значения по принципу да/нет, если дата указана - считается что значение
 * присутствует, иначе - нет
 */

class DispersionByYearProcessor extends AbstractReportDataProcessor
{
    /**
     * @param SourceReportValue[] $sourceValues
     * @return ProcessedReportData
     */
    public function process($sourceValues)
    {
        $vals = [];
        $cnt = 0;

        foreach ($sourceValues as $val) {
            $cnt++;

            $year = date("Y", strtotime($val->date));

            if (!isset($vals[$year])) {
                $vals[$year] = ['сnt' => 1];
            } else {
                $vals[$year]['сnt']++;
            }
        }

        $result = new ProcessedReportData();
        $result->value = $vals;
        $result->total = $cnt;
        return $result;
    }
}
