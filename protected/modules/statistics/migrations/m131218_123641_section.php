<?php

class m131218_123641_section extends I18nDbMigration
{
    public function safeUp()
    {

        $this->createTableWithI18n(
            "{{statistics_section}}",
            array(
                'uid' => 'pk',
                'name' => 'varchar(255) NOT NULL',
                'order' => 'int(11) default 0',
                'alias' => 'varchar(255) NOT NULL COMMENT "алиас секции"'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT="Таблица разделов статистики"',
            array('name')
        );
    }

    public function safeDown()
    {
        $this->dropTableWithI18n("{{statistics_section}}");
    }
}
