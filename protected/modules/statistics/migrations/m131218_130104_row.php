<?php

class m131218_130104_row extends I18nDbMigration
{
    public function safeUp()
    {

        $this->createTableWithI18n(
            "{{statistics_row}}",
            array(
                'uid' => 'pk',
                'name' => 'varchar(255) NOT NULL',
                'source' => 'varchar(255) NOT NULL ',
                'source_field' => 'varchar(255) NOT NULL ',
                'value_transformer' => 'varchar(255) DEFAULT NULL ',
                'value_transformer_param' => 'varchar(255) DEFAULT NULL ',
                'statistics_section_uid' => 'int(11) not null',
                'data_processor' => 'varchar(255) NOT NULL ',
                'renderer' => 'varchar(255) NOT NULL ',
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT="Таблица описания ' .
            'исследуемых параметров статистики"',
            array('name')
        );
    }

    public function safeDown()
    {
        $this->dropTableWithI18n("{{statistics_row}}");
    }
}
