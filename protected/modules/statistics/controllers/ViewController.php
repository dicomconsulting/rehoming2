<?php

class ViewController extends Controller
{
    public function init()
    {
        $this->pageTitle = Yii::t("StatisticsModule.statistics", "Статистика");
        parent::init();

        //настройки вывода в pdf
        $pdfOptions = $this->getPdfPageOptions();
        $pdfOptions['header-html'] = Yii::app()->request->getBaseUrl(true) . $this->createUrl(
            $this->id . "/printHeader"
        );
        $pdfOptions['footer-html'] = Yii::app()->request->getBaseUrl(true) . $this->createUrl(
            $this->id . "/printFooter"
        );
        $this->setPdfPageOptions($pdfOptions);
    }

    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => ['printHeader', 'printFooter'],
                'users' => ['*'],
            ],
            [
                'allow',
                'roles' => ['viewStatistics']
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

    public function actionIndex()
    {
        $reportBuilder = new ReportBuilder();
        $report = $reportBuilder->build();

        $this->render('index', ["report" => $report]);
    }

    public function actionPrint()
    {
        $this->layout = '//layouts/print';

        $reportBuilder = new ReportBuilder();
        $report = $reportBuilder->build();

        $this->renderPdf(
            "index",
            [
                'report' => $report,
            ]
        );
    }

    public function behaviors()
    {
        return array(
            'pdfable' => array(
                'class' => 'ext.pdfable.Pdfable',
                // Global default options for wkhtmltopdf
                'pdfOptions' => array(
                    // Use binary path from module config
                    'bin' => Yii::app()->getModule("pdfable")->bin,
                    'use-xserver',
                    'dpi' => 600,
                    'margin-top' => 19,
                    'margin-right' => 5,
                    'margin-bottom' => 15,
                    'margin-left' => 10,
                    'header-spacing' => 3,
                    'footer-spacing' => 3,
                ),
                // Default page options
                'pdfPageOptions' => array(
                    'print-media-type',
                    'enable-javascript',
                    'javascript-delay' => 500,
                ),
            )
        );
    }

    public function actionPrintHeader(
        $page = 0,
        $title = "",
        $frompage = 0,
        $topage = 0,
        $doctitle = 0
    ) {

        $widgetName = "PrintHeader";
        echo $this->widget($widgetName, array("title" => $title), true);

        Yii::app()->end();
    }

    public function actionPrintFooter(
        $page = 0,
        $title = "",
        $frompage = 0,
        $topage = 0,
        $doctitle = 0
    ) {

        $widgetName = "PrintFooter";
        echo $this->widget($widgetName, array("title" => $title, "page" => $page, "pageTotal" => $topage), true);

        Yii::app()->end();
    }

}
