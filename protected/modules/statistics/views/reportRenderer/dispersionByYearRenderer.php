<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.12.13
 * Time: 12:03
 * @var ProcessedReportData $data
 * @var StatisticsRow $row
 */
?>

<table class = "table table-condensed table-bordered rh-main-table statistics">
    <col width="40%" valign="middle">
    <thead>
    <tr>
        <th>
            <?=$row->name; ?>:
        </th>
        <?php if($data->value ):?>
        <?php foreach($data->value as $year => $value):?>
            <th>
                <?=$year;?>:
            </th>
        <?php endforeach;?>
        <?php else:?>
            <td>
                &nbsp;
            </td>
        <?php endif;?>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <?=Yii::t("StatisticsModule.statistics", "Количество")?>:
        </td>
        <?php if($data->value ):?>
            <?php foreach($data->value as $year => $value):?>
                <td>
                    <?=($value['сnt'])?>
                </td>
            <?php endforeach;?>
        <?php else:?>
            <td>
                <?=Yii::t("StatisticsModule.statistics", "Данных не зарегистрировано")?>
            </td>
        <?php endif;?>
    </tr>
    </tbody>
</table>
