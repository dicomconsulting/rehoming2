<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.12.13
 * Time: 12:03
 * @var ProcessedReportData $data
 * @var StatisticsRow $row
 */
?>

<table class = "table table-condensed table-bordered rh-main-table statistics">
    <col width="40%" valign="middle">
    <col width="35%" valign="middle">
    <col width="25%" valign="middle">
    <thead>
    <tr>
        <th>
            <?=$row->name; ?>:
        </th>
        <td>
            <?=Yii::t("StatisticsModule.statistics", "Относительное значение");?>, %
        </td>
        <td>
            <?=Yii::t("StatisticsModule.statistics", "Всего пациентов");?>:
        </td>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($data->value as $name => $cnt):?>
        <tr>
            <td>
                <?=$name;?>:
            </td>
            <td>
            <?=round((($cnt/$data->total)*100), 2)?>
            </td>
            <td>
                <?=$cnt;?>
            </td>
        </tr>
    <?php endforeach;?>
    </tbody>
</table>
