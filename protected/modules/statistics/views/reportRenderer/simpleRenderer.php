<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.12.13
 * Time: 12:03
 * @var ProcessedReportData $data
 * @var StatisticsRow $row
 */
?>

<table class = "table table-condensed table-bordered rh-main-table statistics">
    <col width="40%" valign="middle">
    <col width="35%" valign="middle">
    <col width="25%" valign="middle">
    <thead>
    <tr>
        <th>
            <?=$row->name; ?>:
        </th>
        <td>
            <?=round($data->value, 2); ?>
        </td>
        <td>
            <?=Yii::t("StatisticsModule.statistics", "Всего пациентов");?>: <?=$data->total; ?>
        </td>
    </tr>
    </thead>
</table>
