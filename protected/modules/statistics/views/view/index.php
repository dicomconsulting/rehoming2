<?php
/* @var $this ViewController */
/* @var $report string */

?>
<h2 class="noprint"><?=Yii::t("StatisticsModule.statistics", "Статистика")?></h2>

<div class="right noprint" style="text-align: right; margin-bottom: 20px;margin-top: -35px;">
    <i class="icon-print"></i> <?=CHtml::link(Yii::t("StatisticsModule.statistics", "Печать"), $this->createUrl($this->id . "/print", $_GET))?>
</div>

<?=$report;?>
