<?php

/**
 * Class StatisticsTemplateFile
 *
 * @property integer $id
 * @property integer $group_id
 * @property string $name
 * @property string $fileData
 */
class StatisticsTemplateFile extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AdverseResultI18n the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{statistics_templates}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['id, group_id', 'numerical', 'integerOnly' => true],
            ['name', 'length', 'max' => 255],
            ['name', 'validateName'],
        ];
    }

    /**
     * Фильтр имени файла
     * TODO: Выделить в отдельный валидатор
     *
     * @param $name
     * @return string
     */
    public function filterFileName($name)
    { // Запрещенные последовательности /\:*?"<>|
        $newName = "";
        $nameLen = strlen($name);
        if ($nameLen > 300) {
            return '';
        }
        $last_char = '-=/-/-/-';
        for ($i = 0; $i < $nameLen; $i++) {
            if (($name[$i] == '/') || ($name[$i] == '\\') || ($name[$i] == '|') || ($name[$i] == ':') || ($name[$i] == '*') || ($name[$i] == '?') || ($name[$i] == '"') || ($name[$i] == '<') || ($name[$i] == '>') || ($name[$i] == '|')) {
                continue;
            }
            if ((ord($name[$i]) == 0) && (ord($last_char) == 0)) {
                return $newName;
            } // Конец строки
            if (($name[$i] == '\r') && ($last_char == '\n')) {
                return $newName;
            } // Перевод строки
            $newName .= $name[$i];
            $last_char = $name[$i];
        };
        return $newName;
    }

    public function validateName()
    {
        $fileName    = $this->filterFileName($this->name);
        $extPosition = strrpos($fileName, '.');
        $ext         = '';
        if ($extPosition) {
            $ext = strtolower(substr($fileName, $extPosition + 1));
        }
        if ($ext == 'xls' || $ext == 'xlsx') {
            $this->name = $fileName;
            return true;
        }
        $this->addError('name', Yii::t('StatisticsNewModule.messages', 'Неверное имя файла'));
        return false;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('StatisticsNewModule.messages', 'Название файла'),
        ];
    }
}

?>