<?php

/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 14.03.16
 * Time: 16:33
 */
class PatientXlsParameter implements XlsParameterInterface
{

    public function getParameterValue($name, $obj)
    {
        $methodName = 'get' . $name;
        if (method_exists($this, $methodName)) {
            return $this->$methodName($obj);
        } else {
            return 'Неизвестная переменная "' . $name . '"';
        }
    }

    public function getId(Patient $patient)
    {
        return $patient->uid;
    }

    public function getCode(Patient $patient)
    {
        return $patient->human_readable_id;
    }

    public function getImplantDate(Patient $patient)
    {
        $dt = new DateTime($patient->device->implantation_date);
        return $dt->format('d-m-Y');
    }

    public function getLastHospDate(Patient $patient)
    {
        if ($patient->device->lastState) {
            $dt = new DateTime($patient->device->lastState->last_follow_up);
            return $dt->format('d-m-Y');
        } else {
            return null;
        }
    }

    public function getObservationPeriod(Patient $patient)
    {
        return PatientStats::getPatientPeriod($patient);
    }

    public function getComment(Patient $patient)
    {
        return $patient->comment;
    }

    public function getSex(Patient $patient)
    {
        if ($patient->sex == Patient::MALE) {
            return Yii::t('StatisticsNewModule.statistics', 'M');
        }
        if ($patient->sex == Patient::FEMALE) {
            return Yii::t('StatisticsNewModule.statistics', 'Ж');
        }
        return null;
    }

    public function getAge(Patient $patient)
    {
        return $patient->birth_date ? $patient->getAgeYears() : '';
    }

    public function getStudyStartAge(Patient $patient)
    {
        if ($patient->birth_date && $patient->device && $patient->device->active_since_date) {
            return $patient->getAgeYears($patient->device->active_since_date);
        }
        return null;

    }

    public function getNyha(Patient $patient)
    {
        $protocolEnrolment = PatientStats::getFirstProtocolEnrolment($patient);
        if ($protocolEnrolment) {
            $nyha = $protocolEnrolment->patient_nyha;
            if ($nyha == 6) {
                return null;
            }
            if ($nyha == 5) {
                return '0';
            }
            return $nyha;
        } else {
            return null;

        }
    }

    public function getIsDead(Patient $patient)
    {
        return $this->getDieDate($patient) ? Yii::t('StatisticsNewModule.statistics', 'Да') : Yii::t('StatisticsNewModule.statistics', 'Нет');
    }

    public function getDieDate(Patient $patient)
    {
        $protocols = PatientStats::getAllProtocolAdverse($patient);
        if ($protocols) {
            foreach ($protocols as $protocolAdverse) {
                if ($protocolAdverse->adv_death_date) {
                    $dt = new DateTime($protocolAdverse->adv_death_date);
                    return $dt->format('d-m-Y');
                }
            }
        }
        return null;
    }

    public function getDieCause(Patient $patient)
    {
        $protocols = PatientStats::getAllProtocolAdverse($patient);
        if ($protocols) {
            foreach ($protocols as $protocolAdverse) {
                if ($protocolAdverse->adv_death_date) {
                    return $protocolAdverse->adv_death_cause;
                }
            }
        }
        return null;
    }

    public function getDeviceName(Patient $patient)
    {
        return $patient->device->device_model;
    }

    /**
     * Параметер: Нарушение ритма проводимости. Указывать Да или Нет. Ставить Да, если в протоколе включения отмечен хоты один из пунктов как Да:
     *    - СА блокада
     *    - Фибриллляция
     *    - Трепетание предсердий
     *    - Как купируется аритмия (медикаментозно/самостоятельно/ЭИТ)
     *    - АВ-блокада
     *    - Блокада левой/правой ножки пучка Гиса
     *    - Двухпучковая блокада
     *    - Фибрилляция желудочкой в анамнезе
     *    - Желудочковая тахикардия в анамнезе
     *    - Тип жел.тахикардии
     *    - Сознание при приступе
     *    - Синкопе неизвестного генеза
     *
     * @param Patient $patient
     * @return string
     *
     */
    public function getRhythmConductanceDisorders(Patient $patient)
    {
        $enrolmentProtocol = PatientStats::getFirstProtocolEnrolment($patient);

        if (!$enrolmentProtocol) {
            return null;
        }

        if (
            $enrolmentProtocol->ca_block ||               // СА блокада
            $enrolmentProtocol->atrial_fibrillation ||    // Фибриллляция
            $enrolmentProtocol->atrial_flutter ||         // Трепетание предсердий
            $enrolmentProtocol->ab_block ||               // АВ-блокада
            $enrolmentProtocol->left_branch_block ||      // Блокада левой/правой ножки пучка Гиса
            $enrolmentProtocol->both_branch_block ||      // Двухпучковая блокада
            $enrolmentProtocol->vent_fibrillation ||      // Фибрилляция желудочкой в анамнезе
            $enrolmentProtocol->vent_tachy ||      // Желудочковая тахикардия в анамнезе
            $enrolmentProtocol->vent_tachy_type ||      // Тип жел.тахикардии
            $enrolmentProtocol->consciousness ||      // Сознание при приступе
            $enrolmentProtocol->syncope_unknown_origin

        ) {
            return Yii::t('StatisticsNewModule.statistics', 'Да');
        } else {
            return Yii::t('StatisticsNewModule.statistics', 'Нет');
        }
    }

    /**
     * Параметер: Этиология. Брать из протокола включения, раздел Этиология. Выводить через знак ;, при этом сокращать наименования следующим образом:
     *    - АГ - артериальная гипертензия
     *    - ИБС
     *    - СД - сахарный диабет
     *    - ВПС
     *    - ППС
     *    - КМП - кардиомиопатия
     *
     * @param Patient $patient
     * @return string
     */
    public function getEtiology(Patient $patient)
    {
        $returnValue       = '';
        $enrolmentProtocol = PatientStats::getFirstProtocolEnrolment($patient);
        if (!$enrolmentProtocol) {
            return null;
        }
        if (in_array(14911, $enrolmentProtocol->etiologyIds)) {
            $returnValue .= Yii::t('StatisticsNewModule.statistics', 'АГ') . ';';
        }
        if (in_array(14802, $enrolmentProtocol->etiologyIds)) {
            $returnValue .= Yii::t('StatisticsNewModule.statistics', 'ИБС') . ';';
        }
        if (in_array(14459, $enrolmentProtocol->etiologyIds)) {
            $returnValue .= Yii::t('StatisticsNewModule.statistics', 'CД') . ';';
        }
        if (in_array(14502, $enrolmentProtocol->etiologyIds)) {
            $returnValue .= Yii::t('StatisticsNewModule.statistics', 'ВПС') . ';';
        }
        if (in_array(14815, $enrolmentProtocol->etiologyIds)) {
            $returnValue .= Yii::t('StatisticsNewModule.statistics', 'ППС') . ';';
        }
        if (in_array(14837, $enrolmentProtocol->etiologyIds)) {
            $returnValue .= Yii::t('StatisticsNewModule.statistics', 'КМП') . ';';
        }
        return $returnValue;
    }

    /**
     * Желудочковая тахикардия в анамнезе. Ставить Да или Нет. Брать из протокола включения, поле "Желудочковая тахикардия".
     *
     * @param Patient $patient
     * @return null|string
     */
    public function getVentricularTachycardia(Patient $patient)
    {
        $enrolmentProtocol = PatientStats::getFirstProtocolEnrolment($patient);
        if (!$enrolmentProtocol) {
            return null;
        }
        if ($enrolmentProtocol->vent_tachy) {
            return Yii::t('StatisticsNewModule.statistics', 'Да');
        } else {
            return Yii::t('StatisticsNewModule.statistics', 'Нет');
        }
    }

    /**
     * Тип желудочковой тахикардии. Брать из протокола включения. Сокращать:
     *   М - мономорфная
     *   П - полиморфная
     *
     * @param Patient $patient
     * @return null|string
     */
    public function getTypeVentricularTachycardia(Patient $patient)
    {
        $enrolmentProtocol = PatientStats::getFirstProtocolEnrolment($patient);
        if (!$enrolmentProtocol) {
            return null;
        }
        if ($enrolmentProtocol->vent_tachy_type == 1) {
            return Yii::t('StatisticsNewModule.statistics', 'М');
        } elseif ($enrolmentProtocol->vent_tachy_type == 2) {
            return Yii::t('StatisticsNewModule.statistics', 'П');
        }
        return null;
    }

    /**
     * Фибрилляция желудочков в анамнезе.* Ставить Да или Нет. Брать из протокола включения, поле "Фибрилляция желудочков".
     *
     * @param Patient $patient
     * @return null|string
     */
    public function getVentricularFibrillationAnamnesis(Patient $patient)
    {
        $enrolmentProtocol = PatientStats::getFirstProtocolEnrolment($patient);
        if (!$enrolmentProtocol) {
            return null;
        }
        if ($enrolmentProtocol->vent_fibrillation) {
            return Yii::t('StatisticsNewModule.statistics', 'Да');
        }
        return Yii::t('StatisticsNewModule.statistics', 'Нет');
    }

    /**
     * Сознание при приступе. Брать из протокола включения, сокращать при выводе:
     *    ПС- пресинкопе
     *    С - синкопе
     *   КС - клиническая смерть
     *  СНГ - синкопе неизвестного генеза
     *
     * @param Patient $patient
     * @return null|string
     */
    public function getConsciousnessAtAttack(Patient $patient)
    {
        $enrolmentProtocol = PatientStats::getFirstProtocolEnrolment($patient);
        if (!$enrolmentProtocol) {
            return null;
        }

        $returnValue = '';

        if ($enrolmentProtocol->syncope_unknown_origin) {
            $returnValue.= Yii::t('StatisticsNewModule.statistics', 'СНГ').'; ';
        }
        if ($enrolmentProtocol->consciousness == 1) {
            $returnValue.= Yii::t('StatisticsNewModule.statistics', 'КС').'; ';
        }
        if ($enrolmentProtocol->consciousness == 2) {
            $returnValue.= Yii::t('StatisticsNewModule.statistics', 'ПС').'; ';
        }
        if ($enrolmentProtocol->consciousness == 3) {
            $returnValue.= Yii::t('StatisticsNewModule.statistics', 'С').'; ';
        }
        return $returnValue;
    }

    /**
     * Причина имплантации ЭКС. Брать из протокола включения (раздел "Причины имплантации/ЭКС).Если значений несколько, то выводить через знак ;
     *
     * @param Patient $patient
     * @return null|string
     */
    public function getReasonImplantationEKS(Patient $patient)
    {
        $enrolmentProtocol = PatientStats::getFirstProtocolEnrolment($patient);
        if (!$enrolmentProtocol) {
            return null;
        }
        $returnValue = '';

        if ($enrolmentProtocol->impl_sssu) {
            $returnValue .= 'СССУ, включая тахи-бради синдром; ';
        }
        if ($enrolmentProtocol->impl_av_block_perm) {
            $returnValue .= 'Постоянная АВ-блокада высокой степени; ';
        }
        if ($enrolmentProtocol->impl_av_block_trans) {
            $returnValue .= 'Приходящяя АВ-блокада высокой степени; ';
        }
        if ($enrolmentProtocol->impl_brady) {
            $returnValue .= 'Брадикардия с постоянной формой ФП; ';
        }
        if ($enrolmentProtocol->impl_binodal_av_perm) {
            $returnValue .= 'Бинодальное заболевание + постоянная АВ-блокада; ';
        }
        if ($enrolmentProtocol->impl_binodal_av_trans) {
            $returnValue .= 'Бинодальное заболевание + преходящая АВ-блокада; ';
        }
        if ($enrolmentProtocol->impl_vaso_syncope) {
            $returnValue .= 'Вазовагальные синкопы; ';
        }
        return $returnValue;
    }

    /**
     * Причина имплантации IKD.
     *
     * @param Patient $patient
     * @return null|string
     */
    public function getReasonImplantationIKD(Patient $patient)
    {
        $enrolmentProtocol = PatientStats::getFirstProtocolEnrolment($patient);
        if (!$enrolmentProtocol) {
            return null;
        }
        $returnValue = '';

        if ($enrolmentProtocol->impl_prime_prevent) {
            $returnValue .= 'ПП; ';
        }
        if ($enrolmentProtocol->impl_second_prevent) {
            $returnValue .= 'ВТ; ';
        }
        if ($enrolmentProtocol->impl_heart_failure) {
            $returnValue .= 'СН; ';
        }
        return $returnValue;
    }

    /**
     * Операции на сердце. Брать из протокола включения, раздел "Операции на сердце". Если значений несколько, то выводить через знак ;
     *
     * @param Patient $patient
     * @return null|string
     */
    public function getHeartSurgery(Patient $patient)
    {
        $enrolmentProtocol = PatientStats::getFirstProtocolEnrolment($patient);
        if (!$enrolmentProtocol) {
            return null;
        }
        $returnValue = '';

        foreach ($enrolmentProtocol->surgeryRel as $oneSurgeryRel) {
            $surgery        = $oneSurgeryRel->surgery;
            $dt             = new DateTime($oneSurgeryRel->date);
            $txtDescription = $dt->format('d-m-Y') . ' ' . str_replace("\n", '', $surgery->name) . ' ' . $oneSurgeryRel->descr;
            /** @var $oneSurgery Surgery */
            $returnValue .= $txtDescription . '; ';
        }
        return $returnValue;
    }

    /**
     * НЖТ. Выводить Да или Нет. Значение Да выводится в случае, если на вкладке Статус/Пред.аритмия в поле "Количество переключения режимов в день" (вторая колонка) указано значение, кроме null и 0.
     *
     * @param Patient $patient
     * @return null|string
     */
    public function getSVT(Patient $patient)
    {
        $lastValue = PatientStats::getParameterValueAverage($patient, 'number_of_mode_switching_per_day'); // Параметер: Количество «переключения режимов» в день
        if ($lastValue > 0) {
            return Yii::t('StatisticsNewModule.statistics', 'Да');
        }
        if ($lastValue === 0) {
            return Yii::t('StatisticsNewModule.statistics', 'Нет');
        }
        return null;
    }

    /**
     * ЖТ. Указывать сумму значений VT1+VT2 (из вторых колонок). Значения берутся из Статус/Жел.аритмия, поля Эпизоды VT1 и Эпизоды VT2. У кого нет этих полей ставить null
     *
     * @param Patient $patient
     * @return null|string
     */
    public function getGT(Patient $patient)
    {
        $vt1 = PatientStats::getParameterValueLast($patient, 'vt1_episodes');
        $vt2 = PatientStats::getParameterValueLast($patient, 'vt2_episodes');

        if ($vt1 === null && $vt2 === null) {
            return null;
        }
        return $vt1 + $vt2;
    }

    /**
     * ФЖ. Указывать значение VF (из второй колонки).Значения берутся из Статус/Жел.аритмия, поля Эпизоды VF. У кого нет этих полей ставить null.
     *
     * @param Patient $patient
     * @return null|string
     */
    public function getFG(Patient $patient)
    {
        $vf = PatientStats::getParameterValueLast($patient, 'vf_episodes');
        return $vf;
    }

    /**
     * АТС в зоне ЖТ. Указывать значение поля "АТС в зоне VT проведена".Значения берутся из Статус/Жел.аритмия. У кого нет этих полей ставить null.
     *
     * @param Patient $patient
     * @return null|string
     */
    public function getAtsGTZone(Patient $patient)
    {
        $parameter = PatientStats::getParameterValueLast($patient, 'atp_in_vt_zones_started');
        return round($parameter,2);
    }

    /**
     * Эффективная АТС в зоне ЖТ. Рассчитывается как ("ATP in VT zones successful"/"АТР in VT zones startes") * 100%/ Значения берутся из Статус/Жел.аритмия (вторые колонки). У кого нет этих полей ставить null.
     *
     * @param Patient $patient
     * @return null|string
     */
    public function getEffectiveAtsGTZone(Patient $patient)
    {
        $started    = PatientStats::getParameterValueLast($patient, 'atp_in_vt_zones_started'); // Параметер: Эпизоды VF
        $successful = PatientStats::getParameterValueLast($patient, 'atp_in_vt_zones_successful'); // Параметер: Эпизоды VF
        if (!$started) {
            return null;
        } else {
            return $successful / $started * 100;
        }
    }

    /**
     * АТС в зоне ФЖ. Указывать значение поля "АТС «One Shot» проведена".Значения берутся из Статус/Жел.аритмия. У кого нет этих полей ставить null.
     *
     * @param Patient $patient
     * @return null|string
     */
    public function getAtsFGZone(Patient $patient)
    {
        $parameter = PatientStats::getParameterValueLast($patient, 'atp_one_shot_started');
        return $parameter;
    }

    /**
     * Эффективная АТС в зоне ФЖ. Рассчитывается как ("АТС «One Shot» успешна"/"АТС «One Shot» проведена") * 100%/ Значения берутся из Статус/Жел.аритмия (вторые колонки). У кого нет этих полей ставить null.
     *
     * @param Patient $patient
     * @return null|string
     */
    public function getEffectiveAtsFGZone(Patient $patient)
    {
        $started    = PatientStats::getParameterValueLast($patient, 'atp_one_shot_started');
        $successful = PatientStats::getParameterValueLast($patient, 'atp_one_shot_successful');
        if (!$started) {
            return null;
        } else {
            return $successful / $started * 100;
        }
    }

    /**
     * Количество полученных шоков. Рассчитывается как (Shocks started - Shocks aborted). Значения берутся из Статус/Жел.аритмия (вторые колонки). У кого нет этих полей ставить null.
     *
     * @param Patient $patient
     * @return null|string
     */
    public function getNumberShocksReceived(Patient $patient)
    {
        $started = PatientStats::getParameterValueLast($patient, 'shocks_started');
        $aborted = PatientStats::getParameterValueLast($patient, 'shocks_aborted');
        if (!$started) {
            return null;
        } else {
            return $started - $aborted;
        }
    }

    /**
     * Эффективность шоков. Рассчитывается как (Shocks successful/(Shocks started - Shocks aborted))*100%. Значения берутся из Статус/Жел.аритмия (вторые колонки). У кого нет этих полей ставить null.
     *
     * @param Patient $patient
     * @return null|string
     */
    public function getShocksEfficiency(Patient $patient)
    {
        $shocksSuccessful = PatientStats::getParameterValueLast($patient, 'shocks_successful');
        $started          = PatientStats::getParameterValueLast($patient, 'shocks_started');
        $aborted          = PatientStats::getParameterValueLast($patient, 'shocks_aborted');
        if (!$started || (($started - $aborted) == 0)) {
            return null;
        } else {
            return $shocksSuccessful / ($started - $aborted) * 100;
        }
    }

    /**
     * Мес. до 1 терапии. Рассчитывается количество дней с Даты имплантации до даты (самой первой даты), в который было получено одно из следующих сообщений (эти сообщения выводятся на вкладке История):
     *   - Finding "VT1 detected" initially detected.
     *   - Finding "VT2 detected" initially detected.
     *   - Finding "VF detected" initially detected.
     *
     * @param Patient $patient
     * @return null|string
     */
    public function getMonthBeforeTherapy(Patient $patient)
    {
        $criteria       = new CDbCriteria();
        $criteria->with = ['device' => ['together' => true]];
        $criteria->compare('device_id', $patient->device->uid);
        $criteria->addInCondition('text', [
            'Finding "VT1 detected" initially detected.',
            'Finding "VT2 detected" initially detected.',
            'Finding "VF detected" initially detected.'
        ]);
        $criteria->order = 'time asc';

        $historyMessage = HistoryMessage::model()->find($criteria);

        if ($historyMessage && $patient->device->implantation_date) {

            return Stats::smartCalcPeriodDays($patient->device->implantation_date, $historyMessage->time);
        } else {
            return null;
        }
    }

    /**
     * Серьезное нежелательное явление. Указывать Да или Нет. Ставить Да, если в протоколе "Нежелательное явление" в разделе "Серьезность нежелательного явления" указан хотя бы один параметр.
     * @param Patient $patient
     * @return null|string
     */
    public function getSeriousAdverseEvents(Patient $patient)
    {
        $adverseProtocols = PatientStats::getAllProtocolAdverse($patient);
        if ($adverseProtocols) {
            foreach ($adverseProtocols as $protocol) {
                /** @var  ProtocolAdverse $protocol */
                if ($protocol->adv_lethal || $protocol->adv_injury || $protocol->adv_perm_damage ||
                    $protocol->adv_hospitalization || $protocol->adv_surg || $protocol->adv_distress || $protocol->adv_prevented
                ) {
                    return Yii::t('StatisticsNewModule.statistics', 'Да');
                }
            }
            return Yii::t('StatisticsNewModule.statistics', 'Нет');
        } else {
            return null;
        }
    }
}

