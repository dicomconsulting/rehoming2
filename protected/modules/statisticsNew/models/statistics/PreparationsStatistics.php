<?php

/**
 * Препараты, принимаемые к моменту включения в исследование
 *
 * Class PreparationsStatistics
 */
class PreparationsStatistics implements StatisticsProviderInterface
{
    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $pharmCustom   = [];
        $patientsCount = [];

        // Массив: Имя группы => Список идентификаторов пацинтов
        $groupsStats       = [
            Yii::t('StatisticsNewModule.statistics', 'Антиаритмики I класса')   => [],
            Yii::t('StatisticsNewModule.statistics', 'b-блокаторы')             => [],
            Yii::t('StatisticsNewModule.statistics', 'Антиаритмики III класса') => [],
            Yii::t('StatisticsNewModule.statistics', 'Ca-антагонисты')          => [],
            Yii::t('StatisticsNewModule.statistics', 'Сердечные гликозиды')     => [],
            Yii::t('StatisticsNewModule.statistics', 'Ингибиторы АПФ')          => [],
            Yii::t('StatisticsNewModule.statistics', 'Мочегонные')              => [],
            Yii::t('StatisticsNewModule.statistics', 'Нитраты')                 => [],
            Yii::t('StatisticsNewModule.statistics', 'Антикоагулянты')          => [],
        ];
        $pharmGroups       = ProtocolEnrolment::getPharmsTmp();
        $pharmGroupsReform = [];
        foreach ($pharmGroups as $oneGroup) {
            foreach ($oneGroup['products'] as $oneProduct) {
                $pharmGroupsReform[$oneProduct->id] = $oneGroup['name'];
            }
        }

        foreach ($patients as $patient) {
            $patientId         = $patient->uid;
            $protocolEnrolment = PatientStats::getFirstProtocolEnrolment($patient);
            if ($protocolEnrolment) {
                $pharmElements = $protocolEnrolment->pharm;
                foreach ($pharmElements as $onePharm) {
                    $productId = $onePharm->pharmProduct->id;
                    if (array_key_exists($productId, $pharmGroupsReform)) {
                        $groupName = $pharmGroupsReform[$productId];
                        if (array_key_exists($groupName, $groupsStats)) {
                            $groupsStats[$groupName][$patientId] = $patientId;
                        } else {
                            $groupsStats[$groupName] = [
                                $patientId => $patientId
                            ];
                        }
                    } else {
                        $pharmCustom[$patientId] = $patientId;
                    }
                    $patientsCount[$patientId] = true;
                }
                if ($protocolEnrolment->pharm_custom) {
                    $pharmCustom[$patientId]   = $patientId;
                    $patientsCount[$patientId] = true;
                }
            }
        }

        $total = count($patientsCount);

        $groupsStats[Yii::t('StatisticsNewModule.statistics', 'Другие препараты')] = $pharmCustom;
        $returnValue                                                               = [];
        foreach ($groupsStats as $name => $value) {
            $returnValue[$name] = [
                'cnt'      => count($value),
                'totalCnt' => $total
            ];
        }
        $returnValue[Yii::t('StatisticsNewModule.statistics', 'Пациентов в выборке')] = [
            'cnt'      => $total,
            'totalCnt' => count($patients)
        ];
        return $returnValue;
    }
}
