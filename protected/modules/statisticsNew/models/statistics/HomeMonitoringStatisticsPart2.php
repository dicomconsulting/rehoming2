<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 29.02.16
 * Time: 13:25
 */

/**
 * Статистика раздела: Опции Home Monitoring
 *
 * Class HomeMonitoringStatisticsPart2
 */
class HomeMonitoringStatisticsPart2 implements StatisticsProviderInterface
{

    /**
     * Получить список текстовых сообщений помеченных как Red или Yellow
     */
    protected function getTextAlertMessages()
    {
        $returnValues  = [];
        $alertMessages = DeviceAlterMessage::model()->findAll();
        foreach ($alertMessages as $message) {
            $returnValues[] = $message->message;
        }
        return $returnValues;
    }

    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $filteredPatients = [];
        foreach ($patients as $patient) {
            if (PatientStats::getFirstProtocolEnrolment($patient)) {
                $filteredPatients[$patient->uid] = $patient;
            }
        }

        $criteria          = new CDbCriteria();
        $statDevices       = PatientStats::getStatDevices($filteredPatients);
        $textAlertMessages = $this->getTextAlertMessages();

        $messagesByPatients = [];

        $criteria       = new CDbCriteria();
        $criteria->with = ['device' => ['together' => true]];
        $criteria->addInCondition('device_id', $statDevices);
        $criteria->addInCondition('text', $textAlertMessages);
        $historyMessages = HistoryMessage::model()->findAll($criteria);
        foreach ($historyMessages as $oneMessage) {
            $patientUid = $oneMessage->device->patient_id;
            if (array_key_exists($patientUid, $messagesByPatients)) {
                $messagesByPatients[$patientUid]++;
            } else {
                $messagesByPatients[$patientUid] = 1;
            }
        }

        // Определяется сумма «общего количества сообщений в год» по всем пациентам выборки.
        // «Общее количество сообщений в год» рассчитывается с по формуле = (Количество всего сообщений/количество месяцев включения пациента в исследование)*12.
        // Например, пациент включен в исследование с 10.05.2015 по 15.12.2015, рассчитывается количество месяцев включения, получаем 8 месяцев (декабрь включен полностью (до 15 числа месяц не включается, после 15 – включается), май включен полностью (до 15 числа включается, после 15 не включается). Допустим за это время у пациента было 15 сообщений. Получаем в расчете на год 15*12/8 = 22,5 сообщений, округляем до целого числа, получаем 23 сообщений.
        $messagesInYear = 0;
        foreach ($messagesByPatients as $patientId => $count) {
            // Вычисляем период исследования для пациента
            $patient = $patients[$patientId];
            $period  = PatientStats::getPatientPeriod($patient);
            if ($period) {
                $messagesInYear += $count / $period * 12;
            }
        }

        return [
            Yii::t('StatisticsNewModule.statistics', 'Минимальное количество сообщений в год')                => [
                'cnt' => Stats::min($messagesByPatients),
            ],
            Yii::t('StatisticsNewModule.statistics', 'Максимальное количество сообщений в год')               => [
                'cnt' => Stats::max($messagesByPatients),
            ],
            Yii::t('StatisticsNewModule.statistics', 'Среднее количество сообщений на одного пациента в год') => [
                'cnt' => Stats::average($messagesByPatients),
            ],
            Yii::t('StatisticsNewModule.statistics', 'Стандартное отклонение')                                => [
                'cnt' => Stats::standardDeviation($messagesByPatients),
            ],
            Yii::t('StatisticsNewModule.statistics', 'Общее количество всех видов сообщений в год')           => [
                'cnt' => round($messagesInYear, 2)
            ],
        ];
    }
}
