<?php

/**
 * Статистика на основе параметров
 *
 * Class BaseParameterStatistics
 */
class BaseParameterStatistics
{
    /**
     * Сгруппировать параметры
     *
     * @param $startValues
     * @param $stopValues
     * @return array
     */
    protected function groupValues($startValues, $stopValues)
    {
        $groupValues = [];

        /** @var DeviceValue $oneValue */
        foreach ($startValues as $oneValue) {
            $groupValues[$oneValue->device_state_id] = [
                'start'    => $oneValue->value,
                'startObj' => $oneValue
            ];
        }
        foreach ($stopValues as $oneValue) {
            if (array_key_exists($oneValue->device_state_id, $groupValues)) {
                $groupValues[$oneValue->device_state_id]['stop']    = $oneValue->value;
                $groupValues[$oneValue->device_state_id]['stopObj'] = $oneValue;
            }
        }

        return $groupValues;
    }


    /**
     * @param Patient[] $patients
     * @return array
     */
    protected function getStatDevices($patients)
    {
        $statDevices = [];
        foreach ($patients as $patient) {
            if ($patient->device && $patient->device->model && $patient->device->model->type) {
                $type = $patient->device->model->type;
                if ($type->alias == 'icd' || $type->alias == 'crtd') {
                    $statDevices[$patient->uid] = $patient->device->uid;
                }
            }
        }
        return $statDevices;
    }

    /**
     * Получить id параметра по его системному имени
     *
     * @param $parameterAlias
     * @return int|null
     */
    protected function getParameterIdByAlias($parameterAlias)
    {
        $parameter = DeviceParameter::model()->findByAttributes(['system_name' => $parameterAlias]);
        if ($parameter) {
            return $parameter->uid;
        } else {
            return null;
        }
    }

    /**
     * Получить знаения по параметру по разным устройствам
     *
     * @param $parameterId
     * @return array
     */
    protected function getParameterValues($parameterId, $statDevices)
    {
        $parameterValues = [];
        foreach ($statDevices as $oneDevice) {
            // Выбираем количество
            $criteria = new CDbCriteria();
            $criteria->join .= ' LEFT JOIN {{device_state}} as device_state ON t.device_state_id=device_state.uid';
            $criteria->addCondition('device_state.device_id =' . $oneDevice);
            $criteria->addCondition('t.parameter_id =' . $parameterId);
            $criteria->addCondition('t.value is not null');

            $count = DeviceValue::model()->count($criteria);
            if ($count) {
                $criteria = new CDbCriteria();
                $criteria->join .= ' LEFT JOIN {{device_state}} as device_state ON t.device_state_id=device_state.uid';
                $criteria->addCondition('device_state.device_id =' . $oneDevice);
                $criteria->addCondition('t.parameter_id =' . $parameterId);
                $criteria->addCondition('t.value is not null');
                $criteria->select = 't.*, device_state.create_time AS maxCreateTime';
                $criteria->order  = 'device_state.create_time desc, uid desc';

                $deviceValue = DeviceValue::model()->find($criteria);

                $parameterValues[] = $deviceValue;
            }
        }
        return $parameterValues;
    }
}
