<?php


class PatientAgeStatistics implements StatisticsProviderInterface
{
    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $allAgeValues = [];

        foreach ($patients as $patient) {
            if ($patient->birth_date !== null) {
                if ($patient->device && $patient->device->active_since_date) {
                    $age = $patient->getAgeYears($patient->device->active_since_date);
                    $allAgeValues[] = $age;
                }
            }
        }

        return [
            // Определяется минимальный возраст среди пациентов выборки
            Yii::t('StatisticsNewModule.statistics', 'Минимальный возраст на дату регистрации')  => [
                'cnt' => Stats::min($allAgeValues),
            ],
            // Определяется максимальный возраст среди пациентов выборки
            Yii::t('StatisticsNewModule.statistics', 'Максимальный возраст на дату регистрации') => [
                'cnt' => Stats::max($allAgeValues),
            ],
            // Определяется средний возраст = [Сумма всех возрастов пациентов выборки]/Количество пациентов выборки
            Yii::t('StatisticsNewModule.statistics', 'Средний возраст на дату регистрации')      => [
                'cnt' => Stats::average($allAgeValues),
            ],
            // Определяется стандартное отклонение параметра «Возраст» по всем пациентам выборки
            Yii::t('StatisticsNewModule.statistics', 'Стандартное отклонение')                 => [
                'cnt' => Stats::standardDeviation($allAgeValues),
            ],
            Yii::t('StatisticsNewModule.statistics', 'Пациентов в выборке')                 => [
                'cnt' => count($allAgeValues),
            ]
        ];
    }
}
