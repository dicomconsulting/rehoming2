<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 01.03.16
 * Time: 14:23
 */

/**
 * Оценка клинической нагрузки
 *
 * Class ClinicalLoadStatistics
 */
class ClinicLoadStatistics implements StatisticsProviderInterface
{
    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $clHospDays       = [];
        $clDisabilityDays = [];
        $clAmbCalls       = [];
        $clCount          = [];

        $totalCnt = 0;
        foreach ($patients as $patient) {
            $patientUid = $patient->uid;
            $protocols  = PatientStats::getAllProtocolsAnnual($patient);
            if ($protocols) {
                foreach ($protocols as $protocol) {
                    $totalCnt++;
                    $clHospDays[]       = $protocol->cl_hosp_days;
                    $clDisabilityDays[] = $protocol->cl_disability_days;
                    $clAmbCalls[]       = $protocol->cl_amb_calls;

                    if ($protocol->cl_hosp_days || $protocol->cl_disability_days || $protocol->cl_amb_calls) {
                        $clCount[$patientUid] = true;
                    }
                }
            }
        }

        return [
            Yii::t('StatisticsNewModule.statistics', 'Количество койко-дней, проведенных в стационаре за год') => [
                'cnt'               => Stats::average($clHospDays),
                'standardDeviation' => Stats::standardDeviation($clHospDays)
            ],
            Yii::t('StatisticsNewModule.statistics', 'Количество дней нетрудоспособности за год')              => [
                'cnt'               => Stats::average($clDisabilityDays),
                'standardDeviation' => Stats::standardDeviation($clDisabilityDays)
            ],
            Yii::t('StatisticsNewModule.statistics', 'Количество обращений к службе скорой помощи за год')     => [
                'cnt'               => Stats::average($clAmbCalls),
                'standardDeviation' => Stats::standardDeviation($clAmbCalls)
            ],
            Yii::t('StatisticsNewModule.statistics', 'Количество пациентов у которых отмечен данный параметр') => [
                'cnt'                       => count($clCount),
                'noStandardDeviationСolumn' => true
            ],
        ];
    }
}
