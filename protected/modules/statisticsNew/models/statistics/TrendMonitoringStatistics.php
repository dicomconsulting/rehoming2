<?php

/**
 * Данные тренд-мониторинга имплантата
 *
 * Class TrendMonitoringStatistics
 */
class TrendMonitoringStatistics extends BaseParameterStatistics implements StatisticsProviderInterface
{
    /**
     * Наличие наджелудочковых аритмий
     *
     * @param $statDevices
     * @return array
     */
    public function getPresenceSupraventricularArrhythmias($statDevices)
    {
        // svt_episodes_total - Эпизоды НЖТ (общее количество)
        $parameterId     = $this->getParameterIdByAlias('svt_episodes_total');
        $parameterValues = $this->getParameterValues($parameterId, $statDevices);
        $totalSumm       = 0;
        $plusValues      = []; // Выбираем положительные значения
        foreach ($parameterValues as $oneValue) {
            if ($oneValue->value) {
                $plusValues[] = $oneValue->value;
            }
            $totalSumm += $oneValue->value;
        }

        $cnt       = count($plusValues);
        $average   = count($plusValues) ? round($totalSumm / count($plusValues), 2) : 0;
        $deviation = Stats::standardDeviation($plusValues);

        $stats = [
            // Определяется количество пациентов, у которых указано значение параметра «Количество эпизодов НЖТ»).
            Yii::t('StatisticsNewModule.statistics', 'Наличие наджелудочковых аритмий') => [
                'cnt'               => $cnt,
                'average'           => $average,
                'standardDeviation' => $deviation
            ]
        ];
        return $stats;
    }

    /**
     * Длительность более 12 часов в сутки
     *
     * @param $statDevices
     * @return array
     */
    public function getArrhythmiasDuration12Hours($statDevices)
    {
        /**
         * Вы совершенно правы, нет специальной графы для "Количества НЖТ длительностью более 12 часов в сутки", однако их количество все же
         * можно подсчитать. Это моё упущение что не дал вам разъяснения ранее. Количество таких эпизодов можно подсчитать на вкладке "History".
         * Нужно посчитать общее количество сообщений "Finding "Long atrial episode detected" initially detected".
         * Такое сообщение по умолчанию формируется при длительности НЖТ более 12 часов.
         */
        $allMessages   = [];
        $messagesCount = 0;

        foreach ($statDevices as $oneDevice) {
            $allMessages[$oneDevice] = 0;
        }


        $criteria = new CDbCriteria();
        $criteria->addInCondition('device_id', $statDevices);
        $criteria->addCondition("`text`='Finding \"Long atrial episode detected\" initially detected.'");
        $historyMessages = HistoryMessage::model()->findAll($criteria);
        foreach ($historyMessages as $oneMessage) {
            $allMessages[$oneMessage->device_id]++;
            $messagesCount++;
        }

        $stats = [
            // Определяется количество пациентов, у которых указано значение параметра «Количество эпизодов НЖТ»).
            Yii::t('StatisticsNewModule.statistics', 'Длительность более 12 часов в сутки') => [
                'cnt'               => $messagesCount,
                'average'           => Stats::average($allMessages),
                'standardDeviation' => Stats::standardDeviation($allMessages)
            ]
        ];
        return $stats;
    }

    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $patientsFiltered    = [];
        $patientsIkd         = [];
        $patientsIkdFiltered = [];
        foreach ($patients as $patient) {
            $protocolEnrolment = PatientStats::getFirstProtocolEnrolment($patient);
            if ($patient->device->model->device_type_uid == 2) {
                if ($protocolEnrolment) {
                    if ($protocolEnrolment->atrial_permanent) {
                        continue;
                    }
                }
                $patientsIkd[]         = $patient;
                $patientsIkdFiltered[] = $patient;
                $patientsFiltered[]    = $patient;
            } else {
                if ($protocolEnrolment) {
                    if ($protocolEnrolment->atrial_permanent) {
                        continue;
                    }
                }
                $patientsFiltered[] = $patient;
            }
        }

        // Устроойства по которым ведеться статистика
        $statIkdDevices     = $this->getStatDevices($patientsIkd);
        $filteredIkdDevices = $this->getStatDevices($patientsIkdFiltered);

        $total = count($patients);

        // По пациентам к IKD
        $ikdPresenceSupraventricularArrhythmiasStats = $this->getPresenceSupraventricularArrhythmias($statIkdDevices);
        $ikdArrhythmiasDuration12Hours               = $this->getArrhythmiasDuration12Hours($filteredIkdDevices);


        // Без пациентов к IKD
        $statDevices                              = $this->getStatDevices($patients);
        $filteredDevices                          = $this->getStatDevices($patientsFiltered);
        $presenceSupraventricularArrhythmiasStats = $this->getPresenceSupraventricularArrhythmias($statDevices);
        $arrhythmiasDuration12Hours               = $this->getArrhythmiasDuration12Hours($filteredDevices);


        /**
         * В выборку попадают пациенты, удовлетворяющие фильтрам и только те, у которых установлены устройства ИКД или CRT-D. Параметры VT1, VT2, VF необходимо брать Since implantation (нарастающий итог). Все параметры задаются на вкладке «Статус» в разделе «Желудочковая аритмия».
         */
        $ikdStats = array_merge(
            $ikdPresenceSupraventricularArrhythmiasStats,
            $ikdArrhythmiasDuration12Hours,
            [
                Yii::t('StatisticsNewModule.statistics', 'Всего') => [
                    'cnt'                       => count($statIkdDevices),
                    'average'                   => intval(count($statIkdDevices) / $total * 100) . '%',
                    'noStandardDeviationСolumn' => true,
                ]
            ]
        );
        $allStats = array_merge(
            $presenceSupraventricularArrhythmiasStats,
            $arrhythmiasDuration12Hours,
            [
                Yii::t('StatisticsNewModule.statistics', 'Всего') => [
                    'cnt'                       => count($statDevices),
                    'average'                   => intval(count($statDevices) / $total * 100) . '%',
                    'noStandardDeviationСolumn' => true,
                ]
            ]
        );
        return [
            'ikdStats' => $ikdStats,
            'allStats' => $allStats
        ];
    }
}
