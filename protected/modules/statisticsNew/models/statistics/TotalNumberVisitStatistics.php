<?php

/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 01.03.16
 * Time: 16:16
 */

/**
 * Общее количество посещений клиники
 *
 * Class TotalNumberVisitStatistics
 */
class TotalNumberVisitStatistics implements StatisticsProviderInterface
{
    /**
     *
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $patientsVisitsCount = [];

        foreach ($patients as $patient) {
            $protocols = PatientStats::getAllProtocolRegular($patient);
            if ($protocols) {
                $period                             = PatientStats::getPatientPeriod($patient);
                if ($period) {
                    $cnt                                = count($protocols);
                    $patientsVisitsCount[$patient->uid] = $cnt / $period * 12;
                }
            }
        }

        return [
            Yii::t('StatisticsNewModule.statistics', 'Общее количество посещений клиники в год') => [
                'cnt'               => Stats::average($patientsVisitsCount),
                'standardDeviation' => Stats::standardDeviation($patientsVisitsCount)
            ]
        ];
    }
}
