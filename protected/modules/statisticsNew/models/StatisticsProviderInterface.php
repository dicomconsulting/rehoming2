<?php

interface StatisticsProviderInterface
{
    /**
     * @return array
     */
    public function getStatistics($patients);
}