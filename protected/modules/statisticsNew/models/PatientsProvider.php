<?php

class PatientsProvider
{
    protected $filter;

    public function __construct($filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return array
     */
    public function getPatients()
    {
        if (!array_key_exists('formStatistics', $this->filter)) {
            return [];
        }
        $criteria = new CDbCriteria();
        if (array_key_exists('Patients', $this->filter) && !(array_key_exists('PatientsAll', $this->filter) && $this->filter['PatientsAll'])) {
            $criteria->addInCondition('t.uid', $this->filter['Patients']);
        }
        $deviceJoined = false;
        if (array_key_exists('DeviceModel', $this->filter) && !(array_key_exists('DeviceModelAll', $this->filter) && $this->filter['DeviceModelAll'])) {
            $criteria->join .= ' LEFT JOIN {{device}} as device ON device.patient_id=t.uid ';
            $criteria->addInCondition('device.device_model', $this->filter['DeviceModel']);
            $deviceJoined = true;
        }
        if (array_key_exists('DeviceType', $this->filter) && !(array_key_exists('DeviceTypeAll', $this->filter) && $this->filter['DeviceTypeAll'])) {
            if (!$deviceJoined) {
                $criteria->join .= ' LEFT JOIN {{device}} as device ON device.patient_id=t.uid ';
            }
            $criteria->join .= ' LEFT JOIN {{device_model}} as device_model ON device_model.name =device.device_model';
            $criteria->addInCondition('device_model.device_type_uid', $this->filter['DeviceType']);
        }

        $patients = Patient::model()->onlyAssigned()->findAll($criteria);
        $returnValues = [];
        foreach ($patients as $patient) {
            $returnValues[$patient->uid] = $patient;
        }
        return $returnValues;
    }
}

?>