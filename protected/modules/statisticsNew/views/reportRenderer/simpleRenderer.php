<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.12.13
 * Time: 12:03
 * @var string $header
 * @var array $data
 */
?>
<h5><?= $header; ?></h5>
<table class="table table-condensed table-bordered rh-main-table statistics">
    <col width="40%" valign="middle">
    <col width="35%" valign="middle">
    <col width="25%" valign="middle">
    <thead>
    <tr>
        <td><?= Yii::t('StatisticsModule.statistics', 'Параметр'); ?></td>
        <td><?= Yii::t('StatisticsModule.statistics', 'Значение'); ?></td>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($data as $name => $val) { ?>
        <tr>
            <td>
                <?= $name; ?>
            </td>
            <td>
                <?= ($val['cnt'] === null ? '' : $val['cnt']); ?>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
