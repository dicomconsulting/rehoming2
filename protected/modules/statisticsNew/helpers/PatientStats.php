<?php

/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 29.02.16
 * Time: 16:09
 */
class PatientStats
{
    /**
     * Получить период обследовния пациента в месяцах, либо null если получить значение не возможно
     *
     * @param $patient
     * @return null
     */
    public static function getPatientPeriod($patient)
    {
        if ($research = self::getFirstResearch($patient)) {
            $criteria       = new CDbCriteria();
            $criteria->with = ['device' => ['together' => true]];
            $criteria->compare('device_id', $patient->device->uid);
            $criteria->compare('text', 'Finding "First message received" initially detected.');
            $criteria->order = 'time desc';

            $historyMessage = HistoryMessage::model()->find($criteria);
            if ($historyMessage) {
                $dateBegin = $historyMessage->time;
                return Stats::smartCalcPeriod($dateBegin, $research->date_end);
            }
        } else {
            return null;
        }
    }


    /**
     * Получить первое исследование у пациента
     *
     * @param $patient
     * @return Research|null
     */
    public static function getFirstResearch($patient)
    {
        if ($patient->research) {
            return $patient->research[0];
        } else {
            return null;
        }
    }

    /**
     * Получить первый протокол, определенного типа
     *
     * @param $patient
     * @param $type
     * @return mixed|null
     */
    public static function getFirstProtocolByType($patient, $type)
    {
        if ($research = self::getFirstResearch($patient)) {
            if ($research->protocol) {
                foreach ($research->protocol as $oneProtocol) {
                    if ($oneProtocol->$type) {
                        return $oneProtocol->$type;
                    }
                }
                return null;
            }
            return null;
        }
        return null;
    }

    /**
     * Получить все протоколы, определенного типа
     *
     * @param $patient
     * @param $type
     * @return mixed|null
     */
    public static function getAllProtocolsByType($patient, $type)
    {
        if ($research = self::getFirstResearch($patient)) {
            if ($research->protocol) {
                $protocols = [];
                foreach ($research->protocol as $oneProtocol) {
                    if ($oneProtocol->$type) {
                        $protocols[] = $oneProtocol->$type;
                    }
                }
                return $protocols;
            }
            return null;
        }
        return null;
    }

    /**
     * Получить первый протокол включения в исследование, если он есть
     *
     * @param $patient
     * @return null|ProtocolEnrolment
     */
    public static function getFirstProtocolEnrolment(Patient $patient)
    {
        return self::getFirstProtocolByType($patient, 'protocolEnrolment');
    }

    /**
     * Получить список протоколов "Нежелательные последствия/явления"
     *
     * @param Patient $patient
     * @return array|ProtocolAdverse
     */
    public static function getAllProtocolAdverse(Patient $patient)
    {
        return self::getAllProtocolsByType($patient, 'protocolAdverse');
    }

    /**
     * Получить список протоколов "Регулярное амбулаторное обследование"
     *
     * @param Patient $patient
     * @return array|ProtocolRegular
     */
    public static function getAllProtocolRegular(Patient $patient)
    {
        return self::getAllProtocolsByType($patient, 'protocolRegular');
    }

    /**
     * Получить по первому исседованию первый протокол "Обследование через 12 месяцев"
     *
     * @param $patient
     * @return null|ProtocolAnnual
     */
    public static function getFirstProtocolAnnual($patient)
    {
        return self::getFirstProtocolByType($patient, 'protocolAnnual');
    }

    /**
     * Получить по первому исседованию первый протокол "Обследование через 12 месяцев"
     *
     * @param $patient
     * @return null|ProtocolAnnual
     */
    public static function getAllProtocolsAnnual($patient)
    {
        return self::getAllProtocolsByType($patient, 'protocolAnnual');
    }

    /**
     * Получить по первому исседованию первый протокол "Оценка функций Home Monitoring"
     *
     * @param $patient
     * @return null|ProtocolEvolution
     */
    public static function getFirstProtocolEvolution($patient)
    {
        return self::getFirstProtocolByType($patient, 'protocolEvolution');
    }

    /**
     * Получить список устройств у которых есть хотябы одно сообщение
     *
     * @param $patients
     * @return array
     */
    public static function getStatDevices($patients)
    {
        $patientIds = [];
        foreach ($patients as $patient) {
            $patientId              = $patient->uid;
            $patientIds[$patientId] = $patientId;
        }
        $criteria       = new CDbCriteria();
        $criteria->join = 'LEFT JOIN {{history_message}} as history_message ON history_message.device_id=t.uid';
        $criteria->addCondition("`text`<>''");
        $criteria->addInCondition('patient_id', $patientIds);
        $criteria->group = 'device_id';
        $devices         = Device::model()->findAll($criteria);
        $returnValues    = [];
        foreach ($devices as $device) {
            $returnValues[] = $device->uid;
        }
        return $returnValues;
    }

    /**
     * Получить значение какого либо параметра устройства у пациента (Первый столбец)
     *
     * @param $patient
     * @param $parameterName
     * @return null|string
     */
    public static function getParameterValueLast($patient, $parameterName)
    {
        $patientId = $patient->uid;
        $parameter = DeviceParameter::model()->findByAttributes(['system_name' => $parameterName]);
        $sql       = 'SELECT device_value.value FROM {{device_value}} as device_value, {{device_state}} as device_state, {{device}} as device ' .
            'WHERE device_value.device_state_id=device_state.uid AND device.uid=device_state.device_id AND device.patient_id=\'' . $patientId . '\' ' .
            'AND device_value.parameter_id=' . $parameter->uid . ' ORDER BY  device_state.create_time DESC LIMIT 1';
        $command   = Yii::app()->db->createCommand($sql);
        $rows      = $command->queryAll();
        if ($rows) {
            $lastValue   = $rows[0]['value'];
            $returnValue = $lastValue;
        } else {
            $returnValue = null;
        }

        return $returnValue;
    }

    /**
     * Получить значение какого либо параметра устройства у пациента (Первый столбец)
     *
     * @param $patient
     * @param $parameterName
     * @return null|string
     */
    public static function getParameterValueAverage($patient, $parameterName)
    {
        $parameterName = 'svt_episodes_total';
        $patientId = $patient->uid;
        $parameter = DeviceParameter::model()->findByAttributes(['system_name' => $parameterName]);
        $sql       = 'SELECT AVG(device_value.value) as value FROM {{device_value}} as device_value, {{device_state}} as device_state, {{device}} as device ' .
            'WHERE device_value.device_state_id=device_state.uid AND device.uid=device_state.device_id AND device.patient_id=\'' . $patientId . '\' ' .
            'AND device_value.parameter_id=' . $parameter->uid;
        $command   = Yii::app()->db->createCommand($sql);
        $rows      = $command->queryAll();
        if ($rows) {
            $lastValue   = $rows[0]['value'];
            $returnValue = $lastValue;
        } else {
            $returnValue = null;
        }
        return $returnValue;
    }

    /**
     * Получить значение какого либо параметра устройства у пациента (Второй столбец)
     *
     * @param Patient $patient
     * @param $parameterName
     * @return null|string
     */
    public static function getParameterValueSecond($patient, $parameterName)
    {
        if (!$patient->device) {
            return null;
        }
        if (!$patient->device->lastState) {
            return null;
        }
        if (!$patient->device->wasAssignStateValues) {
            $patient->device->assignStateValuesToDeviceModelParameters();
        }
        if (!array_key_exists($parameterName, $patient->device->model->parameters)) {
            return null;
        }
        $returnValue = $patient->device->model->parameters[$parameterName]->getMeanByCountValue(1);
        return $returnValue;
    }
}