<?php

class StatisticsNewModule extends CWebModule
{
    public function init()
    {
        Yii::app()->getModule('protocols');
        Yii::app()->getModule('patient');
        // import the module-level models and components
        $this->setImport(
            array(
                'statisticsNew.models.*',
                'statisticsNew.models.statistics.*',
                'statisticsNew.models.xlsSource.*',
                'statisticsNew.helpers.*',
                'statisticsNew.components.*',
                'statisticsNew.widgets.print.*',
            )
        );
    }

    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        } else {
            return false;
        }
    }
}
