<?php
/* @var $this MedicalCenterController */
/* @var $model MedicalCenter */
?>

    <h2><?=Yii::t("InfoModule.messages", "Инфостраница")?> №<?php echo $model->uid;?></h2>
    <?=CHtml::link(
        CHtml::tag('i', ['class' => 'icon-list'], ' ') . Yii::t("InfoModule.messages", "Вернуться к списку"),
        $this->createUrl($this->id . '/index'),
        ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 10px;']
    )?>
    <?=CHtml::link(
        CHtml::tag('i', ['class' => 'icon-edit'], ' ') . Yii::t("InfoModule.messages", "Редактировать"),
        $this->createUrl($this->id . '/update/', ['id' => $model->uid]),
        ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 10px;']
    )?>
    <?=CHtml::link(
        CHtml::tag('i', ['class' => 'icon-trash'], ' ') . Yii::t("InfoModule.messages", "Удалить"),
        $this->createUrl($this->id . '/delete/', ['id' => $model->uid]),
        ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 10px;']
    )?>
    <?php $this->widget('zii.widgets.CDetailView', array(
        'data' => $model,
        'attributes'=>array(
            [
                "name" => Yii::t("InfoModule.messages", "Название"),
                "value" => $model->name,
            ],
            [
                "name" => Yii::t("InfoModule.messages", "Содержание"),
                "value" => $model->content,
                "type" => "raw"
            ],
        ),
    )); ?>


