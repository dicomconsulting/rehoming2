<?php
/* @var $this InfoPageAdminController */
/* @var $model Infopage */
/* @var $form I18nActiveForm */
?>
<h2 class="offset1">
    <?= Yii::t("InfoModule.messages", "Информационные страницы") ?>
    <?= $model->getIsNewRecord() ? '' : '№' . $model->uid; ?>
</h2>
<div class="form">

    <?php $form = $this->beginWidget(
        'I18nActiveForm',
        array(
            'id' => 'infopage-form',
            'enableAjaxValidation' => false,
            'htmlOptions' => [
                'class' => 'form-horizontal'
            ]
        )
    ); ?>

    <p class="note controls">
        <?= Yii::t("InfoModule.messages", "Поля отмеченные"); ?>
        <span class="required">"*"</span>
        <?= Yii::t("InfoModule.messages", "обязательны для заполнения"); ?>.
    </p>

    <div class="controls">
        <?=$form->errorSummary($model); ?>
    </div>

    <div class="control-group">
        <?= $form->labelEx($model, 'name', ['class' => 'control-label']); ?>
        <div class="controls">
            <?= $form->textField($model, 'name'); ?>
            <?= $form->error($model, 'name'); ?>
        </div>
    </div>

    <div class="control-group">
        <?= $form->labelEx($model, 'content', ['class' => 'control-label']); ?>
        <div class="controls">
            <?php foreach (MultiLanguageHelper::languageList() as $localeName):?>
                <?=MultiLanguageHelper::generateLanguageFlag($localeName)?>
                <?php $this->widget('application.extensions.ckeditor.ECKEditor', array(
                        'model' => $model,
                        'attribute' => $localeName . I18nActiveRecord::PROPERTY_NAME_DELIMITER . 'content',
                        'language' => 'ru',
                        'editorTemplate' => 'full',
                        'height' => '500px'
                    )); ?>
            <?php endforeach;?>
        </div>
    </div>

    <div class="row buttons controls">
        <?=
        CHtml::submitButton(
            Yii::t('InfoModule.messages', $model->getIsNewRecord() ? 'Создать' : 'Обновить'),
            ['class' => 'btn btn-primary']
        );
        ?>
        <?=
        CHtml::link(
            Yii::t('InfoModule.messages', 'Отмена'),
            $this->createUrl($this->id . '/index'),
            ['class' => 'btn']
        );
        ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->