<?php
/* @var $this InfoPageAdminController */
/* @var $model Infopage */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $('#infopage-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");
?>

<?php $form=$this->beginWidget('CActiveForm', array(
        'enableAjaxValidation'=>true,
    )); ?>

<?php $this->widget('GridView', array(
    'id' => 'infopage-grid',
    'dataProvider' => $model->search(),
    'columns' => array(
        array(
            'id'=>'autoId',
            'class'=>'CCheckBoxColumn',
            'selectableRows' => '50',
        ),
        [
            "header" => Yii::t("InfoModule.messages", "№"),
            "value" => '$data->uid',
        ],
        [
            "header" => Yii::t("InfoModule.messages", "Название"),
            "value" => '$data->name',
        ],
        [
            "header" => Yii::t("InfoModule.messages", "Название (англ.)"),
            "value" => function ($data) {
                    /** @var Infopage $data */
                    $data->disableDefaultSourceTranslation();
                    $value = $data->en__name;
                    $data->enableDefaultSourceTranslation();
                    return $value;
            },
        ],
        array(
            'name'=>'sortOrder',
            'header' => 'Порядок вывода',
            'type'=>'raw',
            'value'=>'CHtml::textField("sortOrder[$data->uid]",$data->order ,array("style"=>"width:50px;margin:0;"))',
            'htmlOptions'=>array("width"=>"50px"),
        ),
        array(
            'name'=>'isActive',
            'header'=>'Акт.',
            'value'=>'($data->active=="1")?("Да"):("Нет")'
        ),
        array(
            'class'=>'CButtonColumn',
            'header' => CHtml::dropDownList(
                'pageSize',
                Yii::app()->user->getState('pageSize'),
                [10 => 10, 20 => 20, 50 => 50],
                [
                    'onchange' => "$.fn.yiiGridView.update('infopage-grid',{ data:{pageSize: $(this).val() }})",
                    'style' => 'width: 70px;'
                ]
            ),
        ),
    ),
)); ?>

<script>
    function reloadGrid(data) {
        $.fn.yiiGridView.update('infopage-grid');
    }
</script>

<?php echo CHtml::ajaxSubmitButton('Активировать',$this->createUrl($this->id . '/ajaxupdate', ['act'=>'doActive']), array('success'=>'reloadGrid')); ?>
<?php echo CHtml::ajaxSubmitButton('Деактивировать',$this->createUrl($this->id . '/ajaxupdate', ['act'=>'doInactive']), array('success'=>'reloadGrid')); ?>
<?php echo CHtml::ajaxSubmitButton('Сохранить порядок вывода', $this->createUrl($this->id . '/ajaxupdate', ['act'=>'doSortOrder']), array('success'=>'reloadGrid')); ?>

<?php $this->endWidget(); ?>

<?=CHtml::endForm();?>

<?=CHtml::link(
    CHtml::tag('i', ['class' => 'icon-plus-sign'], ' ') . Yii::t("InfoModule.messages", "Создать"),
    $this->createUrl($this->id . '/create'),
    ['class' => 'btn btn-primary', 'style' => 'float:right;']
);?>
