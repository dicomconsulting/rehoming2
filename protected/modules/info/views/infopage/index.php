<?php
/* @var $this InfopageController */
/* @var $dataProvider CActiveDataProvider */


$this->menu=array(
	array('label'=>'Create Infopage', 'url'=>array('create')),
	array('label'=>'Manage Infopage', 'url'=>array('admin')),
);
?>

<h1>Infopages</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
