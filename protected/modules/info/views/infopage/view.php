<?php
/* @var $this InfopageController */
/* @var $model Infopage */

$this->menu=array(
	array('label'=>'List Infopage', 'url'=>array('index')),
	array('label'=>'Create Infopage', 'url'=>array('create')),
	array('label'=>'Update Infopage', 'url'=>array('update', 'id'=>$model->uid)),
	array('label'=>'Delete Infopage', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Infopage', 'url'=>array('admin')),
);
?>

<h1>View Infopage #<?php echo $model->uid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'create_date',
	),
)); ?>
