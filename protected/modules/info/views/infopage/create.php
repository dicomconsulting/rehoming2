<?php
/* @var $this InfopageController */
/* @var $model Infopage */


$this->menu=array(
	array('label'=>'List Infopage', 'url'=>array('index')),
	array('label'=>'Manage Infopage', 'url'=>array('admin')),
);
?>

<h1>Create Infopage</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>