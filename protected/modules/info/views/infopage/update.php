<?php
/* @var $this InfopageController */
/* @var $model Infopage */


$this->menu=array(
	array('label'=>'List Infopage', 'url'=>array('index')),
	array('label'=>'Create Infopage', 'url'=>array('create')),
	array('label'=>'View Infopage', 'url'=>array('view', 'id'=>$model->uid)),
	array('label'=>'Manage Infopage', 'url'=>array('admin')),
);
?>

<h1>Update Infopage <?php echo $model->uid; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>