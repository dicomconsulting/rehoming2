<?php

class m131122_052755_info_page extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTableWithI18n(
            "{{infopage}}",
            array(
                "uid" => "INT(11) NOT NULL AUTO_INCREMENT",
                "name" => " VARCHAR(255) NOT NULL",
                "create_date" => "DATETIME NOT NULL",
                "content" => "TEXT NOT NULL",
                "PRIMARY KEY (`uid`)"
            ),
            "ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='Инфостраницы'",
            array("name", "content")
        );
    }

    public function safeDown()
    {
        $this->dropTableWithI18n("{{infopage}}");
    }
}
