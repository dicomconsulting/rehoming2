<?php

class m131223_101039_alter_infopages extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn(
            "{{infopage}}",
            "order",
            "int(11) default 0 COMMENT 'Порядок вывода'"
        );
        $this->addColumn(
            "{{infopage}}",
            "active",
            "tinyint(1) default 0 COMMENT 'Активность записи'"
        );
    }

    public function safeDown()
    {
        $this->dropColumn(
            "{{infopage}}",
            "order"
        );

        $this->dropColumn(
            "{{infopage}}",
            "active"
        );
    }
}
