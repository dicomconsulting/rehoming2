<?php

/**
 * This is the model class for table "{{infopage}}".
 *
 * The followings are the available columns in table '{{infopage}}':
 * @property integer $uid
 * @property integer $order
 * @property integer $active
 * @property string $create_date
 *
 * The followings are the available model relations:
 * @property InfopageI18n[] $infopageI18ns
 */
class Infopage extends I18nActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Infopage the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{infopage}}';
    }

    public function primaryKey()
    {
        return "uid";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('order, active', 'numerical', 'integerOnly' => true),
            array('create_date', 'required'),
            array('name, content', 'safe'),
            array('uid, create_date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'infopageI18ns' => array(self::HAS_MANY, 'InfopageI18n', 'uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid' => 'Uid',
            'create_date' => 'Create Date',
        );
    }

    public function init()
    {
        parent::init();

        if ($this->scenario == "insert") {
            $this->create_date = date("Y-m-d");
        }
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('uid', $this->uid);
        $criteria->compare('create_date', $this->create_date, true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort'=>array(
                'defaultOrder'=>'`order` DESC',
            ),
        ));
    }

    public static function getPagesToShow()
    {
        $criteria = new CDbCriteria();
        $criteria->order = "`order` DESC";

        $pages = self::model()
            ->findAllByAttributes(['active' => 1], $criteria);

        foreach ($pages as $key => $page) {
            /** @var $page Infopage */
            if (!$page->name) {
                unset($pages[$key]);
            }
        }

        return $pages;
    }
}
