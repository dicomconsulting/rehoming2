<?php

class ViewController extends Controller
{

    public function accessRules()
    {
        return [
            [
                'allow',
                'users' => ['*'],
            ],
        ];
    }

    public function actionIndex($id)
    {

        $infoPage = Infopage::model()->findByPk($id);

        if (!$infoPage) {
            throw new CHttpException(404, "Запись не найдена");
        }

        $this->render('index', array("info" => $infoPage));
    }
}
