<?php

class InfoPageAdminController extends BaseAdminController
{
    protected $pageTitle = 'Информационные страницы';

    public function accessRules()
    {
        return [
            [
                'allow',
                'roles' => ['infopagesManagement']
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

    /**
     * @return string
     */
    public function getModelClassName()
    {
        return 'Infopage';
    }

    public function actionAjaxupdate()
    {
        $act = $_GET['act'];
        if ($act == 'doSortOrder') {
            $sortOrderAll = $_POST['sortOrder'];
            if (count($sortOrderAll) > 0) {
                foreach ($sortOrderAll as $menuId => $sortOrder) {
                    $model = $this->loadModel($menuId);
                    /** @var Infopage order */
                    $model->order = $sortOrder;
                    $model->save();
                }
            }
        } else {
            $autoIdAll = $_POST['autoId'];
            if (count($autoIdAll) > 0) {
                foreach ($autoIdAll as $autoId) {
                    $model = $this->loadModel($autoId);
                    if ($act == 'doActive') {
                        $model->active = '1';
                    }
                    if ($act == 'doInactive') {
                        $model->active = '0';
                    }
                    if ($model->save()) {
                        echo 'ok';
                    } else {
                        throw new Exception("Sorry", 500);
                    }

                }
            }
        }
    }
}
