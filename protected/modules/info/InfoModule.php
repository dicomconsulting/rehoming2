<?php

class InfoModule extends CWebModule
{
    public function init()
    {
        $this->setImport(
            array(
                'info.models.*',
                'info.components.*',
            )
        );

        Yii::app()->getModule("i18n");
    }

    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action)) {
            return true;
        } else {
            return false;
        }
    }
}
