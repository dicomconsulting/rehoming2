<?php

class m131202_111324_alter_option_simple extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn(
            "{{option_simple}}",
            "condition",
            "tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Тип сравнения, 0 - меньше, 1 - больше'"
        );
    }

    public function safeDown()
    {
        $this->dropColumn(
            "{{option_simple}}",
            "condition"
        );
    }
}
