<?php

class m131203_061643_double_compare extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            "{{option_double}}",
            array(
                'option_uid' => "int(11) NOT NULL COMMENT 'Опция'",
                'param_1' => "varchar(64) NOT NULL COMMENT 'Первый параметр'",
                'param_2' => "varchar(64) NOT NULL COMMENT 'Второй параметр'",
                'condition_1' => "int(11) NOT NULL COMMENT 'Первое условие, ссылка на константу'",
                'condition_2' => "int(11) NOT NULL COMMENT 'Второе условие, ссылка на константу'",
                'PRIMARY KEY (`option_uid`)'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ' .
            'COMMENT="Таблица типа опций сравнения двух параметров"'
        );

        $this->addForeignKey(
            'fk_option_uid_option_double',
            '{{option_double}}',
            'option_uid',
            '{{option}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_option_uid_option_double',
            '{{option_double}}'
        );

        $this->dropTable('{{option_double}}');
    }
}
