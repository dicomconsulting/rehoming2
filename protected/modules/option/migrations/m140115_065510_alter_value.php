<?php

class m140115_065510_alter_value extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn(
            "{{option_value}}",
            "order",
            "INT(11) NULL AUTO_INCREMENT AFTER `level`, ADD KEY(`order`)"
        );

    }

    public function safeDown()
    {
        $this->dropColumn(
            "{{option_value}}",
            "order"
        );
    }
}
