<?php

class m131205_132954_alter_sum_field extends I18nDbMigration
{
    public function safeUp()
    {
        $this->dropForeignKey(
            'fk_option_sum_uid_option_sum_field',
            '{{option_sum_field}}'
        );
        $this->addForeignKey(
            'fk_option_sum_uid_option_sum_field',
            '{{option_sum_field}}',
            'option_sum_uid',
            '{{option_sum}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
    }
}
