<?php

class m131129_130652_option extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTableWithI18n(
            "{{section}}",
            array(
                'uid' => 'pk',
                'name' => 'varchar(255) NOT NULL',
                'alias' => 'varchar(255) NOT NULL COMMENT "контроллер/экшен секции"'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT="Таблица разделов сайта"',
            array('name')
        );

        $this->createTable(
            "{{option_type}}",
            array(
                'uid' => 'pk',
                'name' => 'varchar(255) NOT NULL'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT="Таблица типов опций"'
        );

        $this->createTableWithI18n(
            "{{option}}",
            array(
                'uid' => 'pk',
                'option_type_uid' => "int(11) NOT NULL COMMENT 'Тип опции'",
                'section_uid' => "int(11) NOT NULL COMMENT 'Раздел сайта'",
                'source_text' => "text NOT NULL COMMENT 'Текст для сопоставления опции с опцией на биотронике'",
                'name' => 'varchar(255) NOT NULL'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT="Таблица описания опций"',
            array('name')
        );

        $this->addForeignKey(
            'fk_option_type_uid_option',
            '{{option}}',
            'option_type_uid',
            '{{option_type}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_section_uid_option',
            '{{option}}',
            'section_uid',
            '{{section}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );

        $this->createTable(
            "{{option_simple}}",
            array(
                'option_uid' => 'INT(11) NOT NULL',
                'parameter_system_name' => 'varchar(64) NOT NULL COMMENT "Системное имя параметра"',
                'PRIMARY KEY (`option_uid`)'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci' .
            ' COMMENT="Таблица описания простых опций, которые обрабатываются как сравнение с параметром"'
        );

        $this->addForeignKey(
            'fk_option_uid_option_simple',
            '{{option_simple}}',
            'option_uid',
            '{{option}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );

        $this->createTable(
            "{{option_stateable}}",
            array(
                'option_uid' => 'INT(11) NOT NULL',
                'parameter_system_name' => 'varchar(64) NOT NULL COMMENT "Системное имя параметра"',
                'normal_state' => 'varchar(64) NOT NULL COMMENT "Измеряемое состояние параметра"',
                'rule' => 'tinyint(1) NOT NULL COMMENT "Тип сравнения. 0 - срабатывает если равен, ' .
                    '1 срабатывает если не равен"',
                'PRIMARY KEY (`option_uid`)'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci' .
            ' COMMENT="Таблица описания опций, которые обрабатываются как сравнение с конкретным значением"'
        );

        $this->addForeignKey(
            'fk_option_uid_option_stateable',
            '{{option_stateable}}',
            'option_uid',
            '{{option}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_section_uid_option", "{{option}}");
        $this->dropForeignKey("fk_option_type_uid_option", "{{option}}");
        $this->dropForeignKey("fk_option_uid_option_simple", "{{option_simple}}");
        $this->dropForeignKey("fk_option_uid_option_stateable", "{{option_stateable}}");

        $this->dropTableWithI18n('{{option_stateable}}');
        $this->dropTableWithI18n('{{option_simple}}');
        $this->dropTableWithI18n('{{option}}');
        $this->dropTable('{{option_type}}');
        $this->dropTableWithI18n('{{section}}');
    }
}
