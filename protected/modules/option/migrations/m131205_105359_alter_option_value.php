<?php

class m131205_105359_alter_option_value extends I18nDbMigration
{
    public function safeUp()
    {

        $this->execute("update {{option_value}} set val_1 = 0 where val_1 is null");
        $this->execute("update {{option_value}} set val_2 = 0 where val_2 is null");

        $this->alterColumn(
            "{{option_value}}",
            "val_1",
            "decimal(10,2) DEFAULT 0.00  NOT NULL COMMENT 'Первое значение'"
        );

        $this->alterColumn(
            "{{option_value}}",
            "val_2",
            "decimal(10,2) DEFAULT 0.00  NOT NULL COMMENT 'Второе значение'"
        );
    }

    public function safeDown()
    {
    }
}
