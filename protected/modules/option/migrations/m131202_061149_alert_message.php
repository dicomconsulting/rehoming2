<?php

class m131202_061149_alert_message extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTableWithI18n(
            "{{alert_message}}",
            array(
                'uid' => 'pk',
                'option_uid' => "int(11) NOT NULL COMMENT 'Опция'",
                'device_uid' => "int(11) NOT NULL COMMENT 'Устройство'",
                'date_create' => "datetime NOT NULL COMMENT 'Дата начала события'",
                'date_end' => "datetime DEFAULT NULL COMMENT 'Дата завершения длительности события'",
                'level' => "tinyint(1) NOT NULL COMMENT 'Уровень тревоги'",
                'active' => "tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Актуальность сообщения'",
                'acknowledged' => "tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Признак уведомления'",
                'count' => 'int(11) NOT NULL default 1 comment "Количество срабатываний"'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT="Таблица сообщений"',
            array('name')
        );

        $this->addForeignKey(
            'fk_option_uid_alert_message',
            '{{alert_message}}',
            'option_uid',
            '{{option}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_device_uid_alert_message',
            '{{alert_message}}',
            'device_uid',
            '{{device}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );

    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_option_uid_alert_message", "{{alert_message}}");
        $this->dropForeignKey("fk_device_uid_alert_message", "{{alert_message}}");
        $this->dropTable("{{alert_message}}");
    }
}
