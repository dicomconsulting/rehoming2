<?php

class m131203_065548_sum_compare extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            "{{option_sum}}",
            array(
                'uid' => "pk",
                'option_uid' => "int(11) NOT NULL COMMENT 'Опция'",
                'UNIQUE `option` (`option_uid`)'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ' .
            'COMMENT="Таблица типа опций сравнения суммы нескольких параметров"'
        );

        $this->addForeignKey(
            'fk_option_uid_option_sum',
            '{{option_sum}}',
            'option_uid',
            '{{option}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );

        $this->createTable(
            "{{option_sum_field}}",
            array(
                'option_sum_uid' => "int(11) NOT NULL COMMENT 'Опция'",
                'parameter_name' => "varchar(64) NOT NULL COMMENT 'параметр'",
                'UNIQUE `option` (`option_sum_uid`, `parameter_name`)'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ' .
            'COMMENT="Параметры опции суммы"'
        );

        $this->addForeignKey(
            'fk_option_sum_uid_option_sum_field',
            '{{option_sum_field}}',
            'option_sum_uid',
            '{{option}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_option_uid_option_sum',
            '{{option_sum}}'
        );

        $this->dropTable('{{option_sum}}');

        $this->dropForeignKey(
            'fk_option_sum_uid_option_sum_field',
            '{{option_sum_field}}'
        );

        $this->dropTable('{{option_sum_field}}');
    }
}
