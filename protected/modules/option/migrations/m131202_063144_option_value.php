<?php

class m131202_063144_option_value extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTableWithI18n(
            "{{option_value}}",
            array(
                'option_uid' => "int(11) NOT NULL COMMENT 'Опция'",
                'device_uid' => "int(11) NOT NULL COMMENT 'Устройство'",
                'val_1' => "int(11) NOT NULL COMMENT 'Первое значение'",
                'val_2' => "int(11) NOT NULL COMMENT 'Второе значение'",
                'condition_1' => "int(11) NOT NULL COMMENT 'Первое условие, ссылка на константу, зависит от типа'",
                'condition_2' => "int(11) NOT NULL COMMENT 'Второе условие, ссылка на константу, зависит от типа'",
                'level' => "tinyint(1) NOT NULL COMMENT 'Уровень тревоги, выбрасываемый при срабатывании'",
                'PRIMARY KEY (`option_uid`,`device_uid`)'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ' .
            'COMMENT="Таблица значений опций для конкретного устройства"',
            array('name')
        );

        $this->addForeignKey(
            'fk_option_uid_option_value',
            '{{option_value}}',
            'option_uid',
            '{{option}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_device_uid_option_value',
            '{{option_value}}',
            'device_uid',
            '{{device}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );

    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_option_uid_option_value", "{{option_value}}");
        $this->dropForeignKey("fk_device_uid_option_value", "{{option_value}}");
        $this->dropTable("{{option_value}}");
    }
}
