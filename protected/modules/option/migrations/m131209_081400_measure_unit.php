<?php

class m131209_081400_measure_unit extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn(
            "{{option_simple}}",
            "measure_unit_uid",
            "INT(11) DEFAULT NULL COMMENT 'Единицы изменения'"
        );
    }

    public function safeDown()
    {
        $this->dropColumn(
            "{{option_simple}}",
            "measure_unit_uid"
        );
    }
}
