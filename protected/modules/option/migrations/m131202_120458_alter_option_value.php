<?php

class m131202_120458_alter_option_value extends I18nDbMigration
{
    public function safeUp()
    {
        $this->execute(
            "ALTER TABLE {{option_value}}
  CHANGE `val_1` `val_1` INT(11) NULL  COMMENT 'Первое значение',
  CHANGE `val_2` `val_2` INT(11) NULL  COMMENT 'Второе значение',
  CHANGE `condition_1` `condition_1` INT(11) NULL  COMMENT 'Первое условие, ссылка на константу, зависит от типа',
  CHANGE `condition_2` `condition_2` INT(11) NULL  COMMENT 'Второе условие, ссылка на константу, зависит от типа';"
        );
    }

    public function safeDown()
    {
        $this->execute(
            "ALTER TABLE {{option_value}}
  CHANGE `val_1` `val_1` INT(11) NOT NULL  COMMENT 'Первое значение',
  CHANGE `val_2` `val_2` INT(11) NOT NULL  COMMENT 'Второе значение',
  CHANGE `condition_1` `condition_1` INT(11) NOT NULL  COMMENT 'Первое условие, ссылка на константу, зависит от типа',
  CHANGE `condition_2` `condition_2` INT(11) NOT NULL  COMMENT 'Второе условие, ссылка на константу, зависит от типа';"
        );
    }
}
