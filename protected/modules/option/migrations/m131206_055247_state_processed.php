<?php

class m131206_055247_state_processed extends I18nDbMigration
{
    public function safeUp()
    {

        $this->createTable(
            "{{option_state_processed}}",
            [
                "option_uid" => "int(11) NOT NULL",
                "state_uid" => "int(11) NOT NULL",
                "date_create" => "datetime NOT NULL",
                "PRIMARY KEY (`option_uid`, `state_uid`)"
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci' .
            ' COMMENT="Таблица фиксации проверки опции по конкретному стейту"'
        );

        $this->addForeignKey(
            'fk_option_uid_state_processed',
            '{{option_state_processed}}',
            'option_uid',
            '{{option}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_state_uid_state_processed',
            '{{option_state_processed}}',
            'state_uid',
            '{{device_state}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );

    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_state_uid_state_processed',
            '{{option_state_processed}}'
        );
        $this->dropForeignKey(
            'fk_option_uid_state_processed',
            '{{option_state_processed}}'
        );

        $this->dropTable(
            "{{option_state_processed}}"
        );
    }
}
