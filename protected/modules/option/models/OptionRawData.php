<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 02.12.13
 * Time: 16:21
 *
 * @property integer $selectedAlertLevel
 * @property string $optionText
 * @property string $selectedVal1
 * @property string $selectedVal2
 */

class OptionRawData extends CModel
{
    public $selectedAlertLevel;

    public $optionText;

    public $selectedVal1;

    public $selectedVal2;

    /**
     * Returns the list of attribute names of the model.
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        return array(
            'selectedAlertLevel',
            'optionText',
            'selectedVal1',
            'selectedVal2'
        );
    }
}
