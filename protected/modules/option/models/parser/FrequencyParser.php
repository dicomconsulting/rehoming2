<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 02.12.13
 * Time: 16:31
 *
 * Парсер, анализирующий периодичность срабатывания
 */

class FrequencyParser extends AbstractOptionParser
{
    protected function getVal1()
    {
        if (trim(strtolower($this->rawData->selectedVal1)) == "every") {
            return 0;
        } else {
            return preg_replace("/[^0-9]/", "", $this->rawData->selectedVal1);
        }

    }

    protected function getVal2()
    {
        $val = trim(strtolower($this->rawData->selectedVal1));

        if ($val == "every") {
            return FrequencyCompare::PERIOD_EVERY;
        } elseif (strpos($val, "month") !== false) {
            return FrequencyCompare::PERIOD_MONTH;
        } elseif (strpos($val, "week") !== false) {
            return FrequencyCompare::PERIOD_WEEK;
        }

        throw new OptionException("Unexpected value token");
    }
}
