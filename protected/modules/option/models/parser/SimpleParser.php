<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 02.12.13
 * Time: 16:31
 *
 * Простой парсер, убирает всё кроме цифр
 */

class SimpleParser extends AbstractOptionParser
{
    protected function getVal1()
    {
        return preg_replace("/[^0-9.]*/", "", $this->rawData->selectedVal1);
    }

    protected function getVal2()
    {
        return preg_replace("/[^0-9.]*/", "", $this->rawData->selectedVal2);
    }
}
