<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 02.12.13
 * Time: 16:31
 *
 * Парсер, анализирующий периодичность срабатывания в течении дня
 */

class FrequencyPerDayParser extends AbstractOptionParser
{
    protected function getVal1()
    {
        if (trim(strtolower($this->rawData->selectedVal1)) == "at least one") {
            return 0;
        } else {
            return preg_replace("/[^0-9]/", "", $this->rawData->selectedVal1);
        }

    }

    protected function getVal2()
    {
        return null;
    }
}
