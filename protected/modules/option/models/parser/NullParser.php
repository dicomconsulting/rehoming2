<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 02.12.13
 * Time: 16:31
 *
 * В значения прописывает null - может использоваться для тех опций, которые не нуждаются в конкретных значениях,
 * например SpecialDeviceStatusCompare
 */

class NullParser extends AbstractOptionParser
{
    protected function getVal1()
    {
        return null;
    }

    protected function getVal2()
    {
        return null;
    }
}
