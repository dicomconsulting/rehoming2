<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 02.12.13
 * Time: 15:31
 */

abstract class AbstractOption
{
    const ALERT_LEVEL_OFF = 0;
    const ALERT_LEVEL_YELLOW = 1;
    const ALERT_LEVEL_YELLOW_WITH_NOTICE = 2;
    const ALERT_LEVEL_RED_WITH_NOTICE = 3;
    /**
     * @var OptionValue
     */
    public $value;
    /**
     * @var Option
     */
    protected $data;
    /**
     * Сбрасывается ли статус активности действием пользователя
     * @var
     */
    protected $isResetByAcknowlegde;
    /**
     * Ссылка на идентификатор опции в БД
     *
     * @var
     */
    protected $uid;

    final public function __construct($uid)
    {
        $this->uid = $uid;

        if (is_null($this->isResetByAcknowlegde)) {
            throw new CException(
                "You should specify the isResetByAcknowlegde varialble in concrete option class - " . get_called_class()
            );
        }

        $this->init();
    }

    /**
     * Инициализации опции, здесь в опцию должны быть подгружены
     * названия сравниваемых параметров, условия и т.п.
     *
     * @return mixed
     */
    abstract public function init();

    public static function install(Option $option, $data)
    {
        throw new CException("You should declare static function install");
    }

    /**
     * Проверяет значения по конкретным условиям
     *
     * @param Device $device
     * @return Message
     */
    abstract public function checkByDevice(Device $device);

    /**
     *
     *
     * @return AbstractOptionParser
     */
    abstract public function createParser();

    /**
     * Сохраняет конкретные значения для сравнения для конкретного устройства
     *
     * @param OptionData $data
     * @param Device $device
     * @return bool
     * @throws CException
     */
    public function saveValue(OptionData $data, Device $device)
    {
        OptionValue::model()->deleteByPk(
            array("option_uid" => $this->uid, "device_uid" => $device->uid)
        );

        $optionValue = new OptionValue();
        $optionValue->option_uid = $this->uid;
        $optionValue->device_uid = $device->uid;
        $optionValue->level = $data->selectedAlertLevel;

        if (!is_null($data->val_1)) {
            $optionValue->val_1 = $data->val_1;
        }

        if (!is_null($data->val_2)) {
            $optionValue->val_2 = $data->val_2;
        }

        $optionValue->save();

        if (count($optionValue->getErrors())) {
            throw new CException(
                "There some errors happened during saving option value, option_uid - {$this->uid}, " .
                "device_uid  - " . $device->uid
            );
        }

        return true;
    }

    /**
     * Сбрасывается ли статус актуальности опции действием пользователя
     * или необходимо чтобы значения вернулись в норму
     *
     * @return mixed
     */
    public function getIsResetByAcknowledge()
    {
        return $this->isResetByAcknowlegde;
    }

    /**
     * Загружает в опцию значения для конкретного устройства
     *
     * @param Device $device
     */
    public function loadValue(Device $device)
    {
        $value = Option::model()->findByPk($this->uid)->valueByDevice(['params' => [':device_uid' => $device->uid]]);
        $this->value = $value;
    }

    /**
     * Возвращает название опции с описанием изменения
     *
     * @return mixed
     */
    abstract public function getMessage();

    public function getSection()
    {
        return $this->getData()->section_uid;
    }

    /**
     * Возвращает объект Option с основными данными опции
     *
     * @return Option
     */
    protected function getData()
    {
        if (!$this->data) {
            $this->data = Option::model()->findByPk($this->uid);
        }
        return $this->data;
    }

    public function markProcessed(Device $device)
    {
        if (!$this->isProcessed($device)) {
            $stateProcessed = new OptionStateProcessed();
            $stateProcessed->option_uid = $this->getUid();
            $stateProcessed->state_uid = $device->getStateUid();
            $stateProcessed->date_create = date("Y-m-d H:i:s");
            $stateProcessed->save();
        }
    }

    /**
     * Проверяет не была ли проверена данная опция по значениям из текущего звгруженного статуса устройства.
     *
     * @param Device $device
     * @return bool
     */
    public function isProcessed(Device $device)
    {
        return !empty(
        OptionStateProcessed::model()->findByPk(
            array("option_uid" => $this->getUid(), "state_uid" => $device->getStateUid())
        )
        );
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Возвращает отформатированное название опции с выбранными для конкретного устройства значениями для сравнения
     * Используется для показа на странице опций
     *
     * @return string
     */
    abstract public function getFormattedName();

    /**
     * Инициализирует и возвращает экземпляр класса Message
     *
     * @param RhDateTime $date
     * @return Message
     */
    protected function createMessage(RhDateTime $date = null)
    {
        return new Message($this->value->level, $this, $date);
    }

    /**
     * Возвращает значение параметра из данных по устройству
     *
     * @param Device $device
     * @param $paramName
     * @return null|string
     */
    protected function getParamValue(Device $device, $paramName)
    {
        if (isset($device->model->parameters[$paramName])) {
            $value = $device->model->parameters[$paramName]->getCurrentValue()->value;
            if ($value instanceof I18NValue) {
                $value = $value->getDbValue();
            }
            return $value;
        }

        return null;
    }

    /**
     * Возвращает, может ли значение участвовать в сравнении
     *
     * @param $value
     * @return bool
     */
    protected function isValueComparable($value)
    {
        return !(is_null($value) || $value == "OFF");
    }

    /**
     * @return int
     */
    public function getLevel()
    {
        return $this->value->level;
    }


}
