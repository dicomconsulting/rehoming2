<?php

/**
 * This is the model class for table "{{option_value}}".
 *
 * The followings are the available columns in table '{{option_value}}':
 * @property integer $option_uid
 * @property integer $device_uid
 * @property double $val_1
 * @property double $val_2
 * @property integer $condition_1
 * @property integer $condition_2
 * @property integer $level
 */
class OptionValue extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{option_value}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('option_uid, device_uid, level', 'required'),
            array(
                'option_uid, device_uid, condition_1, condition_2, level',
                'numerical',
                'integerOnly' => true
            ),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('option_uid, device_uid, val_1, val_2, condition_1, condition_2, level', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'option_uid' => 'Опция',
            'device_uid' => 'Устройство',
            'val_1' => 'Первое значение',
            'val_2' => 'Второе значение',
            'condition_1' => 'Первое условие, ссылка на константу, зависит от типа',
            'condition_2' => 'Второе условие, ссылка на константу, зависит от типа',
            'level' => 'Уровень тревоги, выбрасываемый при срабатывании',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('option_uid', $this->option_uid);
        $criteria->compare('device_uid', $this->device_uid);
        $criteria->compare('val_1', $this->val_1);
        $criteria->compare('val_2', $this->val_2);
        $criteria->compare('condition_1', $this->condition_1);
        $criteria->compare('condition_2', $this->condition_2);
        $criteria->compare('level', $this->level);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return OptionValue the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        return array(
            'NullableAttrsBehavior' => array(
                'class' => 'NullableAttrsBehavior'
            ),
        );
    }

    protected function afterFind()
    {
        parent::afterFind();

        if ((int) $this->val_1 == $this->val_1) {
            $this->val_1 = (int) $this->val_1;
        }

        if ((int) $this->val_2 == $this->val_2) {
            $this->val_2 = (int) $this->val_2;
        }
    }
}
