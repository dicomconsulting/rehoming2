<?php

/**
 * This is the model class for table "{{option_simple}}".
 *
 * The followings are the available columns in table '{{option_simple}}':
 * @property integer $option_uid
 * @property string $parameter_system_name
 * @property integer $condition
 * @property integer $measure_unit_uid
 *
 * The followings are the available model relations:
 * @property Option $optionU
 * @property MeasurementUnit $measurementUnit
 */
class OptionSimple extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{option_simple}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('option_uid, parameter_system_name, condition', 'required'),
            array('option_uid, condition, measure_unit_uid', 'numerical', 'integerOnly' => true),
            array('parameter_system_name', 'length', 'max' => 64),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'optionU' => array(self::BELONGS_TO, 'Option', 'option_uid'),
            'measurementUnit' => array(self::BELONGS_TO, 'MeasurementUnit', 'measure_unit_uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'option_uid' => 'Option Uid',
            'parameter_system_name' => 'Системное имя параметра',
            'condition' => 'Тип сравнения, 0 - меньше, 1 - больше',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return OptionSimple the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
