<?php

/**
 * This is the model class for table "{{alert_message}}".
 *
 * The followings are the available columns in table '{{alert_message}}':
 * @property integer $uid
 * @property integer $option_uid
 * @property integer $device_uid
 * @property string $date_create
 * @property string $date_end
 * @property integer $level
 * @property integer $active
 * @property integer $acknowledged
 * @property integer $count
 *
 * The followings are the available model relations:
 * @property Device $deviceU
 * @property Option $optionU
 */
class AlertMessage extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AlertMessage the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function init()
    {
        parent::init();

        if ($this->scenario == "insert") {
            $this->date_create = new RhDateTime();
        }
    }


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{alert_message}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('option_uid, device_uid, date_create, level', 'required'),
            array('option_uid, device_uid, level, active, acknowledged, count', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array(
                'uid, option_uid, device_uid, date_create, date_end, level, active, acknowledged, count',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'device' => array(self::BELONGS_TO, 'Device', 'device_uid'),
            'option' => array(self::BELONGS_TO, 'Option', 'option_uid'),
            'section' => array(self::BELONGS_TO, 'Section', ['section_uid' => 'uid'], 'through'=>'option')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid' => 'Uid',
            'option_uid' => 'Опция',
            'device_uid' => 'Устройство',
            'date_create' => 'Дата начала события',
            'date_end' => 'Дата завершения длительности события',
            'level' => 'Уровень тревоги',
            'active' => 'Актуальность сообщения',
            'acknowledged' => 'Признак уведомления',
            'count' => 'Количество срабатываний',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('uid', $this->uid);
        $criteria->compare('option_uid', $this->option_uid);
        $criteria->compare('device_uid', $this->device_uid);
        $criteria->compare('date_create', $this->date_create, true);
        $criteria->compare('date_end', $this->date_end, true);
        $criteria->compare('level', $this->level);
        $criteria->compare('active', $this->active);
        $criteria->compare('acknowledged', $this->acknowledged);
        $criteria->compare('count', $this->count);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function behaviors()
    {
        return [
            'RhDateTimeBehavior' => array(
                'class' => 'application.extensions.RhDateTimeBehavior'
            ),
        ];
    }


}
