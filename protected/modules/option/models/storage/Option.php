<?php

/**
 * This is the model class for table "{{option}}".
 *
 * The followings are the available columns in table '{{option}}':
 * @property integer $uid
 * @property integer $option_type_uid
 * @property integer $section_uid
 * @property string $source_text
 * @property string $name
 * @property OptionType $type
 * @property OptionValue[] $optionValue
 * @property OptionValue $valueByDevice
 */
class Option extends I18nActiveRecord
{
    public function primaryKey()
    {
        return "uid";
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Option the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{option}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('option_type_uid, section_uid, source_text', 'required'),
            array('option_type_uid, section_uid', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('uid, option_type_uid, section_uid, source_text', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'optionValue' => array(self::HAS_MANY, 'OptionValue', 'option_uid'),
            'type' => array(self::BELONGS_TO, 'OptionType', 'option_type_uid'),
            'valueByDevice' => array(
                self::HAS_ONE,
                'OptionValue',
                'option_uid',
                'condition' => 'device_uid=:device_uid'
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid' => 'Uid',
            'option_type_uid' => 'Тип опции',
            'section_uid' => 'Раздел сайта',
            'source_text' => 'Текст для сопоставления опции с опцией на биотронике',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('uid', $this->uid);
        $criteria->compare('option_type_uid', $this->option_type_uid);
        $criteria->compare('section_uid', $this->section_uid);
        $criteria->compare('source_text', $this->source_text, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}
