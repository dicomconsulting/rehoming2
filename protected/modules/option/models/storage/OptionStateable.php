<?php

/**
 * This is the model class for table "{{option_stateable}}".
 *
 * The followings are the available columns in table '{{option_stateable}}':
 * @property integer $option_uid
 * @property string $parameter_system_name
 * @property string $normal_state
 * @property integer $rule
 *
 * The followings are the available model relations:
 * @property Option $optionU
 */
class OptionStateable extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{option_stateable}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('option_uid, parameter_system_name, normal_state, rule', 'required'),
            array('option_uid, rule', 'numerical', 'integerOnly' => true),
            array('parameter_system_name, normal_state', 'length', 'max' => 64),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('option_uid, parameter_system_name, normal_state, rule', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'optionU' => array(self::BELONGS_TO, 'Option', 'option_uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'option_uid' => 'Option Uid',
            'parameter_system_name' => 'Системное имя параметра',
            'normal_state' => 'Измеряемое состояние параметра',
            'rule' => 'Тип сравнения. 0 - срабатывает если равен, 1 срабатывает если не равен',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('option_uid', $this->option_uid);
        $criteria->compare('parameter_system_name', $this->parameter_system_name, true);
        $criteria->compare('normal_state', $this->normal_state, true);
        $criteria->compare('rule', $this->rule);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return OptionStateable the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
