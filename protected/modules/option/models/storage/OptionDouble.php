<?php

/**
 * This is the model class for table "{{option_double}}".
 *
 * The followings are the available columns in table '{{option_double}}':
 * @property integer $option_uid
 * @property integer $param_1
 * @property integer $param_2
 * @property integer $condition_1
 * @property integer $condition_2
 *
 * The followings are the available model relations:
 * @property Option $optionU
 */
class OptionDouble extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{option_double}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('option_uid, param_1, param_2, condition_1, condition_2', 'required'),
            array('option_uid, condition_1, condition_2', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('option_uid, param_1, param_2, condition_1, condition_2', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'optionU' => array(self::BELONGS_TO, 'Option', 'option_uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'option_uid' => 'Опция',
            'param_1' => 'Первый параметр',
            'param_2' => 'Второй параметр',
            'condition_1' => 'Первое условие, ссылка на константу',
            'condition_2' => 'Второе условие, ссылка на константу',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('option_uid', $this->option_uid);
        $criteria->compare('param_1', $this->param_1);
        $criteria->compare('param_2', $this->param_2);
        $criteria->compare('condition_1', $this->condition_1);
        $criteria->compare('condition_2', $this->condition_2);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return OptionDouble the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
