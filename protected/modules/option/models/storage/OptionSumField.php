<?php

/**
 * This is the model class for table "{{option_sum_field}}".
 *
 * The followings are the available columns in table '{{option_sum_field}}':
 * @property integer $option_sum_uid
 * @property string $parameter_name
 *
 * The followings are the available model relations:
 * @property Option $optionSumU
 */
class OptionSumField extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return OptionSumField the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{option_sum_field}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('option_sum_uid, parameter_name', 'required'),
            array('option_sum_uid', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('option_sum_uid, parameter_name', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'optionSumU' => array(self::BELONGS_TO, 'Option', 'option_sum_uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'option_sum_uid' => 'Опция',
            'parameter_name' => 'параметр',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('option_sum_uid', $this->option_sum_uid);
        $criteria->compare('parameter_name', $this->parameter_name);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}
