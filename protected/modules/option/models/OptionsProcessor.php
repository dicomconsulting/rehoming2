<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 04.12.13
 * Time: 10:30
 */

class OptionsProcessor
{
    protected $patient;

    public function __construct(Patient $patient)
    {
        $this->patient = $patient;
    }

    public function process($forceProcess = false)
    {
        $device = $this->patient->device;
        $options = OptionFactory::getOptionsByDevice($device);

        //генерируем новые сообщения
        //если опция вернула нам сообщение, значит его надо добавить
        //иначе ищем существующие сообщения по этой опции и закрываем те, которые были просмотрены
        foreach ($options as $option) {
            /** @var Message $message */
            if ($forceProcess || !$option->isProcessed($device)) {
                if ($message = $option->checkByDevice($device)) {
                    $message->add();
                } elseif ($message = Message::find($option, $device)) {
                    $message->setInactive();
                    $message->close();
                }
                $option->markProcessed($device);
            }
        }
    }
}
