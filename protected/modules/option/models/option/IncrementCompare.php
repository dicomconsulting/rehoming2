<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 02.12.13
 * Time: 16:30
 *
 * Сравнение частоты происшествий во времени
 */

class IncrementCompare extends AbstractOption
{
    protected $isResetByAcknowlegde = false;

    protected $paramName;

    /**
     * Возвращает название опции с описанием изменения
     *
     * @return mixed
     */
    public function getMessage()
    {
        return Yii::t(
            "option",
            "Частота происхождения эпизода \"{episode}\" превысила порог срабатывания",
            ['{episode}' => $this->getData()->name]
        );
    }

    /**
     * Возвращает отформатированное название опции с выбранными для конкретного устройства значениями для сравнения
     * Используется для показа на странице опций
     *
     * @return string
     */
    public function getFormattedName()
    {
        return $this->data->name;
    }


    /**
     * Инициализации опции
     *
     * @return mixed
     */
    public function init()
    {
        /** @var OptionSimple $optionSimple */
        $optionSimple = OptionSimple::model()->findByPk($this->uid);
        $this->paramName = $optionSimple->parameter_system_name;
    }

    /**
     * @return AbstractOptionParser
     */
    public function createParser()
    {
        return new NullParser();
    }

    /**
     * Проверяет значения по конкретным условиям
     * @param Device $device
     * @return Message|void
     */
    public function checkByDevice(Device $device)
    {
        $currStateUid = $device->getStateUid();

        $states = $device->getStatesDownTo(2);

        $prevValue = null;
        $cnt = 0;

        foreach ($states as $state) {
            $device->assignStateValuesToDeviceModelParameters($state);
            if (is_null($prevValue)) {
                $prevValue = (int) $this->getParamValue($device, $this->paramName);
            } else {
                $val = (int) $this->getParamValue($device, $this->paramName);
                if ($val > $prevValue) {
                    $cnt += $val - $prevValue;
                }
                $prevValue = $val;
            }
        }

        $device->assignStateValuesToDeviceModelParameters(DeviceState::model()->findByPk($currStateUid));

        if ($cnt > 0 && $cnt >= $this->value->val_1) {
            return $this->createMessage(DeviceState::getStateDateByUid($device->getStateUid()));
        }

        return false;
    }

    public static function install(Option $option, $data)
    {
        OptionSimple::model()->deleteByPk($option->uid);

        $optionData = new OptionSimple();
        $optionData->option_uid = $option->uid;
        $optionData->parameter_system_name = trim($data['parameter_name']);
        $optionData->save(false);
    }
}
