<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 02.12.13
 * Time: 16:30
 */

class SpecialDeviceStatusCompare extends AbstractOption
{

    protected $isResetByAcknowlegde = true;
    protected $devAlerts = array(
        'EOS',
        'Backup Mode active',
        'Emergency brady pacing active',
        'Special device status'
    );
    protected $batAlerts = array(
        'EOS'
    );

    public static function install(Option $option, $data)
    {
        return true;
    }

    /**
     * Возвращает название опции с описанием изменения
     *
     * @return mixed
     */
    public function getMessage()
    {
        return $this->getData()->name;
    }

    /**
     * Возвращает отформатированное название опции с выбранными для конкретного устройства значениями для сравнения
     * Используется для показа на странице опций
     *
     * @return string
     */
    public function getFormattedName()
    {
        return $this->getData()->name;
    }


    /**
     * Инициализации опции
     *
     * @return mixed
     */
    public function init()
    {
        return;
    }

    /**
     * @return AbstractOptionParser
     */
    public function createParser()
    {
        return new NullParser;
    }

    /**
     * Проверяет значения по конкретным условиям
     *
     * @param Device $device
     * @return mixed
     */
    public function checkByDevice(Device $device)
    {
        $batStatus = $this->getParamValue($device, 'battery_status');
        $devStatus = $this->getParamValue($device, 'device_status');

        if (in_array($batStatus, $this->batAlerts) || in_array($devStatus, $this->devAlerts)) {
            return $this->createMessage(DeviceState::getStateDateByUid($device->getStateUid()));
        }

        return false;
    }
}
