<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 02.12.13
 * Time: 16:30
 *
 * Сравнение параметра со значением OK, ON или YES - если не соответствует, то происходит срабатывание сигнализации
 */

class StateCompare extends AbstractOption
{

    protected $isResetByAcknowlegde = true;

    const RULE_EQUAL = 0;
    const RULE_UNEQUAL = 1;

    protected $compareWith;
    protected $rule;
    protected $paramName;


    /**
     * Инициализации опции
     *
     * @return mixed
     */
    public function init()
    {
        /** @var OptionStateable $optionStatable */
        $optionStatable = OptionStateable::model()->findByPk($this->uid);
        $this->compareWith = $optionStatable->normal_state;
        $this->paramName = $optionStatable->parameter_system_name;
        $this->rule = $optionStatable->rule;
    }

    /**
     * Возвращает название опции с описанием изменения
     *
     * @return mixed
     */
    public function getMessage()
    {
        return Yii::t(
            "general",
            "Значение по измерению \"{param_name}\" вышло за пределы допустимых",
            ['{param_name}' => $this->getData()->name]
        );
    }

    /**
     * Возвращает отформатированное название опции с выбранными для конкретного устройства значениями для сравнения
     * Используется для показа на странице опций
     *
     * @return string
     */
    public function getFormattedName()
    {
        return $this->data->name;
    }


    /**
     * @return AbstractOptionParser
     */
    public function createParser()
    {
        return new NullParser();
    }

    /**
     * Проверяет значения по конкретным условиям
     *
     * @param Device $device
     * @return mixed
     */
    public function checkByDevice(Device $device)
    {
        $paramValue = $this->getParamValue($device, $this->paramName);

        if ($this->checkCondition($paramValue)) {
            return $this->createMessage(DeviceState::getStateDateByUid($device->getStateUid()));
        }

        return false;
    }

    protected function checkCondition($paramVal)
    {
        return $this->rule == self::RULE_EQUAL
            && $this->isValueComparable($paramVal) && strtolower($paramVal) == strtolower($this->compareWith)
        || $this->rule == self::RULE_UNEQUAL
            && strtolower($paramVal) != strtolower($this->compareWith);
    }

    public static function install(Option $option, $data)
    {
        OptionStateable::model()->deleteByPk($option->uid);

        $optionData = new OptionStateable();
        $optionData->option_uid = $option->uid;
        $optionData->parameter_system_name = trim($data['parameter_name']);
        $optionData->rule = $data['state_rule'];
        $optionData->normal_state = trim($data['normal_state']);
        $optionData->save(false);
    }
}
