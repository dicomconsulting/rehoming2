<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 03.12.13
 * Time: 11:16
 *
 * Не получали сообщений от пациента больше чем определенное количество дней
 */

class NoMessagesRecievedSinceCompare extends AbstractOption
{

    protected $isResetByAcknowlegde = true;

    /**
     * Проверяет значения по конкретным условиям
     *
     * @param Device $device
     * @return mixed
     */
    public function checkByDevice(Device $device)
    {
        $dt = new DateInterval("P" . (int) $this->value->val_1 . "D");

        $from = new RhDateTime();
        $from->sub($dt);

        $states = $device->getStatesByPeriod($from, new RhDateTime());

        if (!$states) {
            return $this->createMessage(DeviceState::getStateDateByUid($device->getStateUid()));
        }

        return false;
    }

    /**
     * Возвращает название опции с описанием изменения
     *
     * @return mixed
     */
    public function getMessage()
    {
        return $this->getData()->name . " " . Yii::t(
            'general',
            '{n} дня|{n} дней|{n} дней|{n} дня',
            $this->value->val_1
        );
    }

    /**
     * Возвращает отформатированное название опции с выбранными для конкретного устройства значениями для сравнения
     * Используется для показа на странице опций
     *
     * @return string
     */
    public function getFormattedName()
    {
        return $this->getData()->name . " " . Yii::t(
            'general',
            '{n} дня|{n} дней|{n} дней|{n} дня',
            $this->value->val_1
        );
    }


    /**
     * Инициализации опции
     *
     * @return mixed
     */
    public function init()
    {
        return true;
    }

    /**
     * @return AbstractOptionParser
     */
    public function createParser()
    {
        return new SimpleParser();
    }

    public static function install(Option $option, $data)
    {
        //никаких дополнительных данных не сохраняется
        return true;
    }
}
