<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 02.12.13
 * Time: 16:30
 *
 * Вылет за значения двух параметров
 */

class DoubleCompare extends SimpleCompare
{
    protected $paramName_1;
    protected $paramName_2;
    protected $condition_1;
    protected $condition_2;

    /**
     * Инициализации опции
     *
     * @return mixed
     */
    public function init()
    {
        /** @var OptionDouble $optionDouble */
        $optionDouble = OptionDouble::model()->findByPk($this->uid);
        $this->condition_1 = $optionDouble->condition_1;
        $this->condition_2 = $optionDouble->condition_2;
        $this->paramName_1 = $optionDouble->param_1;
        $this->paramName_2 = $optionDouble->param_2;
    }

    /**
     * Возвращает название опции с описанием изменения
     *
     * @return mixed
     */
    public function getMessage()
    {
        /** @var Option $data */
        $data = $this->getData();
        return $data->name . " " . Yii::t("option", "вне допустимых значений.");
    }

    /**
     * Возвращает отформатированное название опции с выбранными для конкретного устройства значениями для сравнения
     * Используется для показа на странице опций
     *
     * @return string
     */
    public function getFormattedName()
    {
        return $this->getData()->name . " " . $this->getConditionTextRepresentation($this->condition_1) . " "
            . $this->value->val_1  . " " . Yii::t("option", "за") . " "
            . $this->getConditionTextRepresentation($this->condition_2) . " "
            . $this->value->val_2  . " " . Yii::t("option", "% дня");
    }

    protected function getConditionTextRepresentation($cond)
    {
        if ($cond == self::CONDITION_LESS) {
            return "<";
        } else {
            return ">";
        }
    }


    /**
     * @return AbstractOptionParser
     */
    public function createParser()
    {
        return new SimpleParser;
    }

    /**
     * Проверяет значения по конкретным условиям
     *
     * @param Device $device
     * @return mixed
     */
    public function checkByDevice(Device $device)
    {
        $param1Value = $this->getParamValue($device, $this->paramName_1);
        $param2Value = $this->getParamValue($device, $this->paramName_2);

        if (!$this->isValueComparable($param1Value) || !$this->isValueComparable($param2Value)) {
            return false;
        }

        $triggered = false;

        if ($this->checkCondition($this->condition_1, $param1Value, $this->value->val_1)
            && $this->checkCondition($this->condition_2, $param2Value, $this->value->val_2)) {
            $triggered = true;
        }

        if ($triggered) {
            return $this->createMessage(DeviceState::getStateDateByUid($device->getStateUid()));
        }

        return false;
    }

    public static function install(Option $option, $data)
    {
        OptionDouble::model()->deleteByPk($option->uid);

        $optionData = new OptionDouble();
        $optionData->option_uid = $option->uid;

        $params = explode(",", $data['parameter_name']);
        $conditions = explode(",", $data['condition']);

        if (count($params) != 2 || count($conditions) != 2) {
            throw new OptionException("There should be 2 parameters and 2 conditions with a comma as a delimiter");
        }

        $optionData->param_1 = trim($params[0]);
        $optionData->param_2 = trim($params[1]);
        $optionData->condition_1 = self::$conditionAliases[$conditions[0]];
        $optionData->condition_2 = self::$conditionAliases[$conditions[1]];

        $optionData->save(false);
    }
}
