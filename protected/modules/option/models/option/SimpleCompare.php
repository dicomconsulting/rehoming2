<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 02.12.13
 * Time: 16:30
 *
 * Простое сравнение значения параметра с его значением, больше или меньше
 */

class SimpleCompare extends AbstractOption
{
    const CONDITION_LESS = 0;
    const CONDITION_MORE = 1;

    protected $isResetByAcknowlegde = false;

    protected static $conditionAliases = array(
        "less" => self::CONDITION_LESS,
        "more" => self::CONDITION_MORE
    );

    protected $condition;
    protected $paramName;
    protected $measureUnit;

    /**
     * Инициализации опции
     *
     * @return mixed
     */
    public function init()
    {
        /** @var OptionSimple $optionSimple */
        $optionSimple = OptionSimple::model()->findByPk($this->uid);
        $this->condition = $optionSimple->condition;
        $this->paramName = $optionSimple->parameter_system_name;
        if ($optionSimple->measure_unit_uid) {
            $this->measureUnit = $optionSimple->measurementUnit->name;
        }
    }

    /**
     * @return AbstractOptionParser
     */
    public function createParser()
    {
        return new SimpleParser;
    }

    /**
     * Возвращает название опции с описанием изменения
     *
     * @return mixed
     */
    public function getMessage()
    {
        $parameter = DeviceParameter::model()->findByAttributes(['system_name' => $this->paramName]);
        $condition = ($this->condition == self::CONDITION_LESS)
            ? Yii::t("option", "меньше")
            : Yii::t("option", "больше");

        return Yii::t(
            "option",
            "Значение параметра \"{param_name}\" стало {condition} чем {value}",
            [
                '{param_name}' => $parameter->name,
                '{condition}' => $condition,
                '{value}' => $this->value->val_1 . ($this->measureUnit ? " " . $this->measureUnit : "")
            ]
        );
    }

    /**
     * Возвращает отформатированное название опции с выбранными для конкретного устройства значениями для сравнения
     * Используется для показа на странице опций
     *
     * @return string
     */
    public function getFormattedName()
    {
        $condition = "";
        switch ($this->condition) {
            case self::CONDITION_LESS:
                $condition = "< " . $this->value->val_1;
                break;
            case self::CONDITION_MORE:
                $condition = "> " . $this->value->val_1;
                break;
        }

        return $this->data->name . ": " . $condition ." ". $this->measureUnit;
    }


    /**
     * Проверяет значения по конкретным условиям
     *
     * @param Device $device
     * @return mixed
     */
    public function checkByDevice(Device $device)
    {
        $paramValue = $this->getParamValue($device, $this->paramName);

        if (!$this->isValueComparable($paramValue)) {
            return false;
        }

        if ($this->checkCondition($this->condition, $paramValue, $this->value->val_1)) {
            return $this->createMessage(DeviceState::getStateDateByUid($device->getStateUid()));
        }

        return false;
    }

    protected function checkCondition($condition, $paramVal, $controlVal)
    {
        return $condition == self::CONDITION_LESS && $paramVal < $controlVal
        || $condition == self::CONDITION_MORE && $paramVal > $controlVal;
    }

    public static function install(Option $option, $data)
    {
        OptionSimple::model()->deleteByPk($option->uid);

        $optionData = new OptionSimple();
        $optionData->option_uid = $option->uid;
        $optionData->parameter_system_name = trim($data['parameter_name']);

        if (!$data['condition']) {
            throw new OptionException(
                "SimpleCompare should have Condition setted, parameter - {$data['parameter_name']}"
            );
        }
        $optionData->condition = self::$conditionAliases[$data['condition']];

        if (!empty($data["measure_unit"])) {
            /** @var MeasurementUnit $measurementUnit */
            $measurementUnit = MeasurementUnit::model()
                ->withI18n('en')
                ->find('name = :name', [':name' => $data["measure_unit"]]);
            $optionData->measure_unit_uid = $measurementUnit->uid;
        }

        $optionData->save(false);
    }

    protected function getParamValue(Device $device, $paramName)
    {
        //этот тип опций обрабатывает только числовые значения
        $rawValue =  parent::getParamValue($device, $paramName);
        $value = preg_replace("/[^0-9.]*/", "", $rawValue);

        if ($value === "") {
            $value = null;
        }

        return $value;
    }
}
