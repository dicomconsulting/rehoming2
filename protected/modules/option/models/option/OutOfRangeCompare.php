<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 02.12.13
 * Time: 16:30
 *
 * Сравнение одного параметра с тем, чтобы он находился в пределах двух значений
 */

class OutOfRangeCompare extends AbstractOption
{

    protected $isResetByAcknowlegde = false;

    protected $paramName;
    protected $measureUnit;

    /**
     * Инициализации опции
     *
     * @return mixed
     */
    public function init()
    {
        /** @var OptionSimple $optionSimple */
        $optionSimple = OptionSimple::model()->findByPk($this->uid);
        $this->paramName = $optionSimple->parameter_system_name;
        if ($optionSimple->measure_unit_uid) {
            $this->measureUnit = $optionSimple->measurementUnit->name;
        }
    }

    /**
     * Возвращает название опции с описанием изменения
     *
     * @return mixed
     */
    public function getMessage()
    {
        return Yii::t(
            "general",
            "Значение по измерению \"{param_name}\" вышло за пределы допустимых",
            ['{param_name}' => $this->getData()->name]
        );
    }

    /**
     * Возвращает отформатированное название опции с выбранными для конкретного устройства значениями для сравнения
     * Используется для показа на странице опций
     *
     * @return string
     */
    public function getFormattedName()
    {
        return $this->data->name . ": " . Yii::t(
            'option',
            "< {low_val} {$this->measureUnit} или > {high_val} {$this->measureUnit}",
            array(
                "{low_val}" => $this->value->val_1,
                "{high_val}" => $this->value->val_2
            )
        );
    }

    /**
     * @return AbstractOptionParser
     */
    public function createParser()
    {
        return new SimpleParser;
    }

    /**
     * Проверяет значения по конкретным условиям
     *
     * @param Device $device
     * @return mixed
     */
    public function checkByDevice(Device $device)
    {
        $paramValue = $this->getParamValue($device, $this->paramName);

        if (!$this->isValueComparable($paramValue)) {
            return false;
        }

        if ($paramValue < $this->value->val_1 || $paramValue > $this->value->val_2) {
            return $this->createMessage(DeviceState::getStateDateByUid($device->getStateUid()));
        }

        return false;
    }

    public static function install(Option $option, $data)
    {
        OptionSimple::model()->deleteByPk($option->uid);

        $optionData = new OptionSimple();
        $optionData->option_uid = $option->uid;
        $optionData->parameter_system_name = $data['parameter_name'];

        if (!empty($data["measure_unit"])) {
            /** @var MeasurementUnit $measurementUnit */
            $measurementUnit = MeasurementUnit::model()
                ->withI18n('en')
                ->find('name = :name', [':name' => $data["measure_unit"]]);
            $optionData->measure_unit_uid = $measurementUnit->uid;
        }

        $optionData->save(false);
    }

    protected function getParamValue(Device $device, $paramName)
    {
        //этот тип опций обрабатывает только числовые значения
        $rawValue =  parent::getParamValue($device, $paramName);
        $value = preg_replace("/[^0-9.]*/", "", $rawValue);

        if ($value === "") {
            $value = null;
        }

        return $value;
    }
}
