<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 03.12.13
 * Time: 11:16
 *
 * Профиль пациента не просматривался дней
 */

class PatientNotViewedForCompare extends AbstractOption
{
    protected $isResetByAcknowlegde = true;

    /**
     * Возвращает название опции с описанием изменения
     *
     * @return mixed
     */
    public function getMessage()
    {
        return Yii::t("OptionModule::messages", "Профиль пациента не просматривался длительное время");
    }

    /**
     * Возвращает отформатированное название опции с выбранными для конкретного устройства значениями для сравнения
     * Используется для показа на странице опций
     *
     * @return string
     */
    public function getFormattedName()
    {
        return $this->data->name . " " . $this->value->val_1 . " " .  Yii::t("OptionModule::messages", "месяцев");
    }

    /**
     * Проверяет значения по конкретным условиям
     *
     * @param Device $device
     * @return mixed
     */
    public function checkByDevice(Device $device)
    {
        $controlPeriod = strtotime("-{$this->value->val_1} month");

        if ($device->patient->last_view_time) {
            $lastViewTime = strtotime($device->patient->last_view_time);

            if ($lastViewTime < $controlPeriod) {
                return $this->createMessage(DeviceState::getStateDateByUid($device->getStateUid()));
            }
        }

        return false;
    }

    /**
     * Инициализации опции
     *
     * @return mixed
     */
    public function init()
    {
        return true;
    }

    /**
     * @return AbstractOptionParser
     */
    public function createParser()
    {
        return new SimpleParser();
    }

    public static function install(Option $option, $data)
    {
        //никаких дополнительных данных не сохраняется
        return true;
    }
}
