<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 02.12.13
 * Time: 16:30
 *
 * Сравнение частоты происшествий во времени
 */

class FrequencyCompare extends AbstractOption
{
    const PERIOD_EVERY = 0;
    const PERIOD_WEEK = 1;
    const PERIOD_MONTH = 2;

    protected $isResetByAcknowlegde = false;

    protected $period;
    protected $count;
    protected $paramName;

    /**
     * Возвращает название опции с описанием изменения
     *
     * @return mixed
     */
    public function getMessage()
    {
        return Yii::t(
            "option",
            "Частота происхождения эпизода \"{episode}\" превысила порог срабатывания",
            ['{episode}' => $this->getData()->name]
        );
    }

    /**
     * Возвращает отформатированное название опции с выбранными для конкретного устройства значениями для сравнения
     * Используется для показа на странице опций
     *
     * @return string
     */
    public function getFormattedName()
    {
        $frequency = "";
        switch ($this->value->val_2) {
            case self::PERIOD_EVERY:
                $frequency = Yii::t("option", "каждое событие");
                break;
            case self::PERIOD_WEEK:
                $frequency = Yii::t("option", "не менее {count} в неделю", array("{count}", $this->value->val_1));
                break;
            case self::PERIOD_MONTH:
                $frequency = Yii::t("option", "не менее {count} в месяц", array("{count}", $this->value->val_1));
                break;
        }

        return $this->data->name . ": " . $frequency;
    }


    /**
     * Инициализации опции
     *
     * @return mixed
     */
    public function init()
    {
        /** @var OptionSimple $optionSimple */
        $optionSimple = OptionSimple::model()->findByPk($this->uid);
        $this->paramName = $optionSimple->parameter_system_name;
    }

    /**
     * @return AbstractOptionParser
     */
    public function createParser()
    {
        return new FrequencyParser();
    }

    /**
     * Проверяет значения по конкретным условиям
     * @param Device $device
     * @return Message|void
     */
    public function checkByDevice(Device $device)
    {
        $currStateUid = $device->getStateUid();

        if ($this->value->val_2 == self::PERIOD_EVERY) {
            //сравниваем последние два статуса
            $states = $device->getStatesDownTo(2);
        } else {
            $dt = $this->getStartDate();
            $states = $device->getStatesByPeriod($dt, new RhDateTime());
        }

        $prevValue = null;
        $cnt = 0;

        foreach ($states as $state) {
            $device->assignStateValuesToDeviceModelParameters($state);
            if (is_null($prevValue)) {
                $prevValue = (int) $this->getParamValue($device, $this->paramName);
            } else {
                $val = (int) $this->getParamValue($device, $this->paramName);
                if ($val > $prevValue) {
                    $cnt += $val - $prevValue;
                }
                $prevValue = $val;
            }
        }

        $device->assignStateValuesToDeviceModelParameters(DeviceState::model()->findByPk($currStateUid));

        if ($cnt > 0 && $cnt >= $this->value->val_1) {
            return $this->createMessage(DeviceState::getStateDateByUid($device->getStateUid()));
        }

        return false;
    }

    protected function getStartDate()
    {
        $dt = new RhDateTime();

        switch (true) {
            case $this->value->val_2 == self::PERIOD_WEEK:
                $interval = new DateInterval("P1W");
                break;
            case $this->value->val_2 == self::PERIOD_MONTH:
                $interval = new DateInterval("P1M");
                break;
        }

        $dt->sub($interval);
        return $dt;
    }

    public static function install(Option $option, $data)
    {
        OptionSimple::model()->deleteByPk($option->uid);

        $optionData = new OptionSimple();
        $optionData->option_uid = $option->uid;
        $optionData->parameter_system_name = trim($data['parameter_name']);
        $optionData->save(false);
    }
}
