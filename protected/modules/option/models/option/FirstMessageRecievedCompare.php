<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 03.12.13
 * Time: 11:16
 *
 * Пришли первые данные от пациента
 */

class FirstMessageRecievedCompare extends AbstractOption
{

    protected $isResetByAcknowlegde = true;

    /**
     * Возвращает название опции с описанием изменения
     *
     * @return mixed
     */
    public function getMessage()
    {
        return $this->getData()->name;
    }

    /**
     * Возвращает отформатированное название опции с выбранными для конкретного устройства значениями для сравнения
     * Используется для показа на странице опций
     *
     * @return string
     */
    public function getFormattedName()
    {
        return $this->getData()->name;
    }


    /**
     * Проверяет значения по конкретным условиям
     *
     * @param Device $device
     * @return mixed
     */
    public function checkByDevice(Device $device)
    {
        $count = DeviceState::model()->countByAttributes(["device_id" => $device->uid]);
        if ($count == 1) {
            return $this->createMessage(DeviceState::getStateDateByUid($device->getStateUid()));
        }

        return false;
    }

    /**
     * Инициализации опции
     *
     * @return mixed
     */
    public function init()
    {
        return;
    }

    /**
     * @return AbstractOptionParser
     */
    public function createParser()
    {
        return new NullParser();
    }

    public static function install(Option $option, $data)
    {
        //никаких дополнительных данных не сохраняется
        return true;
    }
}
