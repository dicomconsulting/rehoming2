<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 02.12.13
 * Time: 16:30
 *
 * Сравнение суммы значений нескольких полей
 */

class SumCompare extends AbstractOption
{
    protected $paramsToSum = array();

    protected $isResetByAcknowlegde = false;

    public static function install(Option $option, $data)
    {
        OptionSum::model()->deleteAllByAttributes(array("option_uid" => $option->uid));

        $optionData = new OptionSum();
        $optionData->option_uid = $option->uid;
        $optionData->save(false);

        $params = explode(",", $data['parameter_name']);

        foreach ($params as $param) {
            $optionField = new OptionSumField();
            $optionField->option_sum_uid = $optionData->uid;
            $optionField->parameter_name = $param;
            $optionField->save(false);
        }
    }

    /**
     * Инициализации опции
     *
     * @return mixed
     */
    public function init()
    {
        /** @var OptionSum $optionData */
        $optionData = OptionSum::model()->findByAttributes(array("option_uid" => $this->getUid()));
        foreach ($optionData->fields as $field) {
            $this->paramsToSum[] = $field->parameter_name;
        }
    }

    /**
     * @return AbstractOptionParser
     */
    public function createParser()
    {
        return new FrequencyPerDayParser;
    }

    /**
     * Возвращает название опции с описанием изменения
     *
     * @return mixed
     */
    public function getMessage()
    {
        return Yii::t(
            "general",
            "Значение по измерению \"{param_name}\" вышло за пределы допустимых",
            ['param_name' => $this->getData()->name]
        );
    }

    /**
     * Возвращает отформатированное название опции с выбранными для конкретного устройства значениями для сравнения
     * Используется для показа на странице опций
     *
     * @return string
     */
    public function getFormattedName()
    {
        return $this->data->name . ": " . $this->value->val_1;
    }


    /**
     * Проверяет значения по конкретным условиям
     *
     * @param Device $device
     * @return mixed
     */
    public function checkByDevice(Device $device)
    {
        $sum = 0;

        foreach ($this->paramsToSum as $param) {
            $paramValue = $this->getParamValue($device, $param);
            if ($this->isValueComparable($paramValue)) {
                $sum += (int) $paramValue;
            }
        }

        if ($sum > $this->value->val_1) {
            return $this->createMessage(DeviceState::getStateDateByUid($device->getStateUid()));
        }

        return false;
    }
}
