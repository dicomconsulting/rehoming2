<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 02.12.13
 * Time: 16:20
 */

abstract class AbstractOptionParser
{
    /**
     * @var OptionRawData
     */
    protected $rawData;

    /**
     * @param OptionRawData $data
     * @return OptionData
     */
    public function parse(OptionRawData $data)
    {
        $this->rawData = $data;

        $optionData = new OptionData();
        $optionData->selectedAlertLevel = $data->selectedAlertLevel;
        $optionData->val_1 = $this->getVal1();
        $optionData->val_2 = $this->getVal2();
        return $optionData;
    }

    abstract protected function getVal1();

    abstract protected function getVal2();
}
