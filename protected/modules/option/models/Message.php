<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 04.12.13
 * Time: 11:11
 */

class Message
{
    /**
     * @var  AlertMessage
     */
    public $data;
    /**
     * @var integer
     */
    protected $level;
    /**
     * @var AbstractOption
     */
    protected $option;

    /**
     *
     * @var RhDateTime
     */
    protected $date;

    public function __construct($level, AbstractOption $option, RhDateTime $date = null)
    {
        $this->level = $level;
        $this->option = $option;
        $this->date = $date ? $date : new RhDateTime();
    }

    /**
     * Добавляет запись о новом
     */
    public function add()
    {
        $deviceUid = $this->option->value->device_uid;
        $optionUid = $this->option->getUid();

        $alertMessage = self::findExistingRecord($deviceUid, $optionUid);
        if ($alertMessage) {
            $this->data = $alertMessage;
        }

        if ($this->level == AbstractOption::ALERT_LEVEL_OFF) {
            //на всякий случай пытаемся закрыть сообщения по этой опции, если они были
            if ($this->data) {
                $this->close();
            }
            return;
        }

        if ($this->option->getIsResetByAcknowledge()) {
            //в этом случае записываем каждое сообщение как новое
            $this->addNewRecord($deviceUid, $optionUid, $this->date);
        } else {
            //в этом случае ищем существующее актуальное сообщение, если оно есть, увеличиваем счетчик срабатываний
            //иначе создаем новое событие
            if ($this->data) {
                $this->data->count = $this->data->count + 1;
                $this->data->save();
            } else {
                $this->addNewRecord($deviceUid, $optionUid, $this->date);
            }
        }
    }

    /**
     * Пытается найти существующее сообщение по переданной опции и устройству
     *
     * @param AbstractOption $option
     * @param Device $device
     * @return bool|Message
     */
    public static function find(AbstractOption $option, Device $device)
    {
        if ($record = self::findExistingRecord($device->uid, $option->getUid())) {
            $message = new Message($record->level, $option);
            $message->data = $record;
            return $message;
        } else {
            return false;
        }
    }

    /**
     * @param $deviceUid
     * @param $optionUid
     * @return AlertMessage
     */
    protected static function findExistingRecord($deviceUid, $optionUid)
    {
        return AlertMessage::model()->findByAttributes(
            ["device_uid" => $deviceUid, "option_uid" => $optionUid, "active" => 1]
        );
    }

    /**
     * @param Patient $patient
     * @param string $section
     * @param string $level
     * @param bool $all
     * @return Message[]
     */
    public static function getMessagesByPatient(Patient $patient, $section = null, $level = null, $all = false)
    {
        /** @var AlertMessage[] $rawMessages */
        $alertMessage = AlertMessage::model();

        if ($section) {
            $alertMessage->with(
                [
                    "section" => [
                        'condition' => 'section.alias = :alias',
                        'params' => [':alias' => $section],
                        'joinType' => 'INNER JOIN'
                    ]
                ]
            );
        }

        $condition = ['device_uid' => $patient->device->uid];

        if ($level) {
            switch ($level) {
                case "red":
                    $level = [AbstractOption::ALERT_LEVEL_RED_WITH_NOTICE];
                    break;
                case "yellow":
                    $level = [AbstractOption::ALERT_LEVEL_YELLOW, AbstractOption::ALERT_LEVEL_YELLOW_WITH_NOTICE];
                    break;
            }

            $condition['level'] = $level;
        }

        if (!$all) {
            $condition['date_end'] = null;
        }

        $rawMessages = $alertMessage->findAllByAttributes(
            $condition
        );

        $messages = [];

        foreach ($rawMessages as $messageRow) {
            $option = OptionFactory::createOption($messageRow->option_uid, $patient->device);
            $message = new Message($messageRow->level, $option);
            $message->data = $messageRow;
            $messages[] = $message;
        }

        return $messages;
    }

    public static function createByUid($uid)
    {
        /** @var AlertMessage $am */
        $am = AlertMessage::model()->findByPk($uid);
        /** @var Device $device */
        $device = Device::model()->findByPk($am->device_uid);

        $option = OptionFactory::createOption($am->option_uid, $device);

        $message = new Message($am->level, $option);
        $message->data = $am;

        return $message;
    }

    public static function getAlertLevelBySection($section, Patient $patient)
    {
        return 0;//отключаем подсветку пунктов меню
        /** @var AlertMessage[] $rawMessages */
        $alertMessage = AlertMessage::model();

        $alertMessage->with(
            [
                "section" => [
                    'condition' => 'section.alias = :alias',
                    'params' => [':alias' => $section],
                    'joinType' => 'INNER JOIN'
                ]
            ]
        );

        $criteria = new CDbCriteria();
        $criteria->order = "t.level DESC";
        $criteria->limit = 1;

        /** @var AlertMessage $rawMessage */
        $rawMessage = $alertMessage->findByAttributes(
            ['device_uid' => $patient->device->uid, 'active' => 1],
            $criteria
        );

        if ($rawMessage) {
            return $rawMessage->level;
        } else {
            return 0;
        }
    }

    /**
     * Закрытие (снятие активности) сообщения
     */
    public function close()
    {
        if ($this->data->active == 0 && $this->data->acknowledged) {
            $this->data->date_end = date("Y-m-d H:i:s");
            $this->data->save();
        }
    }

    /**
     * Сбрасывает активность сообщения, показывая то что по измеряемому значению опция вошла в норму
     */
    public function setInactive()
    {
        $this->data->active = 0;
        $this->data->save();
    }


    /**
     * Переоткрытие сообщения
     */
    public function reopen()
    {
        $this->data->date_end = null;
        $this->data->save();
    }

    /**
     * @param $deviceUid
     * @param $optionUid
     * @param RhDateTime $date
     */
    protected function addNewRecord($deviceUid, $optionUid, $date)
    {
        $alertMessage = new AlertMessage();
        $alertMessage->device_uid = $deviceUid;
        $alertMessage->option_uid = $optionUid;
        $alertMessage->level = $this->level;
        $alertMessage->date_create = $date;
        $alertMessage->save();

        $this->data = $alertMessage;
    }

    /**
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @var bool $short
     * @return mixed|string
     */
    public function formatMessage($short = false)
    {
        $message = $this->option->getMessage();

        if (!$short) {
            $message .= ". " . Yii::t("general", "Начало события - ") . $this->data->date_create . ". ";

            if ($this->data->count > 1) {
                $message .= Yii::t("option", "С момента начала событие зафиксировано ")
                    . Yii::t(
                        'option',
                        '{n} раз|{n} раза|{n} раз|{n} раза',
                        $this->data->count
                    );
            }
        }

        return $message;
    }

    /**
     * @return int
     */
    public function isActual()
    {
        return $this->data->active;
    }

    /**
     * @return int
     */
    public function isAcknowledged()
    {
        return $this->data->acknowledged;
    }

    /**
     * @return int
     */
    public function getUid()
    {
        return $this->data->uid;
    }

    public function toggleAcknowledge()
    {
        if ($this->data->acknowledged) {
            $this->data->acknowledged = 0;
        } else {
            $this->data->acknowledged = 1;
        }

        $this->data->save();

        if ($this->data->acknowledged) {
            if ($this->option->getIsResetByAcknowledge()) {
                $this->setInactive();
            }
            $this->close();
        } else {
            $this->reopen();
        }

        return $this->data->acknowledged;
    }

    public static function getLatestByDevice(Device $device)
    {
        $criteria = new CDbCriteria();
        $criteria->condition = "device_uid = :device_uid AND (active = 1 OR acknowledged = 0)";
        $criteria->params = [":device_uid" => $device->uid];
        $criteria->order = "date_create DESC";
        $criteria->limit = 1;

        /** @var AlertMessage $am */
        $am = AlertMessage::model()->findByAttributes([], $criteria);

        if ($am) {
            return self::createByUid($am->uid);
        } else {
            return null;
        }
    }

    /**
     * @param Message[] $findings
     * @return string
     */
    public static function encodeFindings($findings)
    {
        $tmpData = array();

        foreach ($findings as $finding) {
            $attrs = $finding->data->getAttributes();
            if ($attrs['date_create']) {
                $createDate = $attrs['date_create'];
                $attrs['date_create'] = $createDate;
                $attrs['finding'] = $finding->formatMessage(true);
            }
            $tmpData[] = $attrs;
        }

        return CJSON::encode($tmpData);
    }
}
