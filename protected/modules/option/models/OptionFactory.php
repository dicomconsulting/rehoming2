<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 02.12.13
 * Time: 16:36
 */

class OptionFactory
{
    /**
     * @param OptionRawData $data
     * @return AbstractOption
     * @throws OptionException
     */
    public static function createOptionByData(OptionRawData $data)
    {
        /** @var Option[] $options */
        $optionText = trim($data->optionText, " :");
        $options = Option::model()->findAllByAttributes(array('source_text' => $optionText));

        if (!count($options)) {
            throw new OptionException("Can't resolve Option - no rows was found. '$optionText'");
        }

        if (count($options) > 1) {
            throw new OptionException("Can't resolve Option - multiple rows was found. '$optionText'");
        }

        Yii::import("option.models.option.*");

        $optionData = $options[0];

        /** @var OptionType $type */
        $type = OptionType::model()->findByPk($optionData->option_type_uid);

        $optionClass = $type->name;
        /** @var AbstractOption $option */
        $option = new $optionClass($optionData->uid);

        return $option;
    }

    /**
     * @param Device $device
     * @return AbstractOption[]
     */
    public static function getOptionsByDevice(Device $device)
    {
        /** @var Option[] $optionsData */
        $optionsData = Option::model()->with(
            ["optionValue" =>
                [
                    'joinType'=>'INNER JOIN',
                    'condition' => "device_uid = :device_uid",
                    'params' => [':device_uid' => $device->uid],
                    'order' => 'optionValue.`order`',
                ]
            ]
        )->findAll();

        $options = array();

        foreach ($optionsData as $optionDataRow) {
            $options[] = self::createConcreteClass($device, $optionDataRow);
        }

        return $options;
    }

    /**
     * @param integer $optionUid
     * @param Device $device
     * @return AbstractOption|bool
     */
    public static function createOption($optionUid, $device)
    {
        /** @var Option $optionData */
        $optionData = Option::model()->with(
            ["optionValue" =>
                [
                    'joinType'=>'INNER JOIN',
                    'condition' => "device_uid = :device_uid",
                    'params' => [':device_uid' => $device->uid]
                ]
            ]
        )->findByPk($optionUid);

        if ($optionData) {
            $option = self::createConcreteClass($device, $optionData);
            return $option;
        }

        return false;
    }

    /**
     * @param Device $device
     * @param Option $optionData
     * @return AbstractOption
     */
    protected static function createConcreteClass($device, $optionData)
    {
        $optionClass = $optionData->type->name;
        /** @var AbstractOption $option */
        $option = new $optionClass($optionData->uid);
        $option->loadValue($device);
        return $option;
    }
}
