<h5><?= $header; ?></h5>
<table class="table table-condensed table-bordered rh-main-table statistics">
    <col width="40%" valign="middle">
    <col width="20%" valign="middle">
    <col width="20%" valign="middle">
    <col width="20%" valign="middle">
    <thead>
    <tr>
        <td><?= Yii::t('Statistics2018Module.statistics', 'Параметр'); ?> </td>
        <td><?= $column1Header ? $column1Header : Yii::t('Statistics2018Module.statistics', 'Количество') ?></td>
        <td><?= !empty($column3Header) ? $column1Header : Yii::t('Statistics2018Module.statistics', 'Общее количество приступов'); ?></td>
        <td><?= $column2Header ? $column2Header : Yii::t('Statistics2018Module.statistics', 'Среднее значение'); ?></td>
        <td><?= Yii::t('Statistics2018Module.statistics', 'Стандартное отклонение'); ?></td>
        <td><?= Yii::t('Statistics2018Module.statistics', 'Относительное значение'); ?></td>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($data as $name => $val) { ?>
        <tr>
            <td><?= $name; ?></td>
            <td><?= $val['cnt']; ?></td>
            <td><?= $val['occurrences']; ?></td>
            <?php
            if (array_key_exists('noStandardDeviationСolumn', $val)) {
                ?>
                <td colspan="3">
                    <?= $val['average']; ?>
                </td>
                <?php
            } else {
                ?>
                <td><?= $val['average']; ?></td>
                <td><?= $val['standardDeviation']; ?></td>
                <td><?= $val['relative']; ?>%</td>
                <?php
            }
            ?>
        </tr>
    <?php } ?>
    </tbody>
</table>
