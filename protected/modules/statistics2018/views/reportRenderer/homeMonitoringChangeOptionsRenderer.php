<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 24.03.16
 * Time: 11:36
 */
?>
<table class="table table-condensed table-bordered rh-main-table statistics">
    <col width="40%" valign="middle">
    <col width="35%" valign="middle">
    <col width="25%" valign="middle">
    <col width="25%" valign="middle">
    <thead>
    <tr>
        <td><?= Yii::t('Statistics2018Module.statistics', 'Параметр'); ?></td>
        <td><?= Yii::t('Statistics2018Module.statistics', 'Количественное значение по протоколу'); ?></td>
        <td><?= Yii::t('Statistics2018Module.statistics', 'Количественное значение реальное'); ?></td>
        <td><?= Yii::t('Statistics2018Module.statistics', 'Относительное значение'); ?></td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><?= Yii::t('Statistics2018Module.statistics', 'Изменение опций'); ?></td>
        <td>
            <?= ($data['cntProtocol'] === null ? '' : $data['cntProtocol']); ?>
        </td>
        <td>
            <?= ($data['cntReal'] === null ? '' : $data['cntReal']); ?>
        </td>
        <td>
            <?= ($data['relativeValue'] === null ? '' : $data['relativeValue']); ?>
        </td>
    </tr>
    </tbody>
</table>