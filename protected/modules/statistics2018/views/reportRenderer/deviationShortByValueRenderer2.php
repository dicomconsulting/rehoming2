<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.12.13
 * Time: 12:03
 * @var string $header
 * @var array $data
 */
?>

<h5><?= $header; ?></h5>
<table class="table table-condensed table-bordered rh-main-table statistics">
    <col width="40%" valign="middle">
    <col width="20%" valign="middle">
    <col width="20%" valign="middle">
    <col width="20%" valign="middle">
    <thead>
    <tr>
        <td><?= Yii::t('Statistics2018Module.statistics', 'Параметр'); ?> </td>
        <td><?= !empty($column1Header) ? $column1Header : Yii::t('Statistics2018Module.statistics', 'Количество'); ?></td>
        <td><?= Yii::t('Statistics2018Module.statistics', 'Стандартное отклонение'); ?></td>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($data as $name => $val) { ?>
        <tr>
            <td><?= $name; ?></td>
            <td><?= $val['cnt']; ?></td>
            <td><?= !empty($val['standardDeviation']) ? $val['standardDeviation'] : null; ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>
