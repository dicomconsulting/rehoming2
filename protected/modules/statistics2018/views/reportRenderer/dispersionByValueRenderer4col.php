<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.12.13
 * Time: 12:03
 * @var string $header
 * @var array $data
 */

?>

<h5><?= $header; ?></h5>
<table class="table table-condensed table-bordered rh-main-table statistics">
    <col width="40%" valign="middle">
    <col width="35%" valign="middle">
    <col width="25%" valign="middle">
    <thead>
    <tr>
        <td>
            <?php
            $parameterHeader =  Yii::t('Statistics2018Module.statistics', 'Параметр');
            if (isset($parameterName)) {
                $parameterHeader = $parameterName;
            }
            echo $parameterHeader;
            ?>
        </td>
        <td><?= Yii::t('Statistics2018Module.statistics', 'Относительное значение'); ?></td>
        <td><?= !empty($column1Header) ? $column1Header : Yii::t('Statistics2018Module.statistics', 'Количественное значение'); ?></td>
        <td><?= !empty($column2Header) ? $column2Header : Yii::t('Statistics2018Module.statistics', 'Количество пациентов'); ?></td>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($data as $name => $val): ?>
        <tr>
            <td>
                <?= $name; ?>
            </td>
            <?php
            if (array_key_exists('noRelativeValue', $val)) {
                ?>
                <td colspan="3">
                    <?= $val['cnt']; ?>
                </td>
            <?php } else { ?>
                <td>
                    <?= !empty($totalPatients) ? number_format(($val['cnt'] / $totalPatients) * 100, 2, '.', ' ') : 0; ?>%
                </td>
                <td>
                    <?= $val['cnt']; ?>
                </td>
                <td>
                    <?= $val['patients_count']; ?>
                </td>
            <?php } ?>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
