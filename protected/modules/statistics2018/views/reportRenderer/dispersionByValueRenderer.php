<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.12.13
 * Time: 12:03
 * @var string $header
 * @var array $data
 */

$tableValueName = !empty($tableValueName) ? $tableValueName : 'Количественное значение';
?>

<h5><?= $header; ?></h5>
<table class="table table-condensed table-bordered rh-main-table statistics">
    <col width="40%" valign="middle">
    <col width="35%" valign="middle">
    <col width="25%" valign="middle">
    <thead>
    <tr>
        <td>
            <?php
            $parameterHeader =  Yii::t('Statistics2018Module.statistics', 'Параметр');
            if (isset($parameterName)) {
                $parameterHeader = $parameterName;
            }
            echo $parameterHeader;
            ?>
        </td>
        <td><?= Yii::t('Statistics2018Module.statistics', 'Относительное значение'); ?></td>
        <td><?= Yii::t('Statistics2018Module.statistics', $tableValueName); ?></td>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($data as $name => $val): ?>
        <tr>
            <td>
                <?= $name; ?>
            </td>
            <?php
            if (array_key_exists('noRelativeValue', $val)) {
                ?>
                <td colspan="2">
                    <?= $val['cnt']; ?>
                </td>
            <?php } else { ?>
                <td>
                    <?= $val['totalCnt'] ? number_format(($val['cnt'] / $val['totalCnt']) * 100, 2, '.', ' ') : 0; ?>%
                </td>
                <td>
                    <?= $val['cnt']; ?>
                </td>
            <?php } ?>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
