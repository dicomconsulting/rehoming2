<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 26.01.16
 * Time: 17:19
 */
/* @var $this ViewController */
/* @var $filter */
/* @var $totalPatientsStatistics */
/* @var $patientSexStatistics */
/* @var $savedFilterId */
?>
<span class="statisticsFilterGridContainer noprint">
        <form method=get>
            <table class="statisticsFilterGrid noprint">
                <tr>
                    <th class="patientColumn">
                        <?= Yii::t('Statistics2018Module.statistics', 'Пациенты') ?>
                    </th>
                    <th class="deviceTypeColumn">
                        <?= Yii::t('Statistics2018Module.statistics', 'Типы аппаратов') ?>
                    </th>
                    <th class="deviceNameColumn">
                        <?= Yii::t('Statistics2018Module.statistics', 'Аппараты') ?>
                    </th>
                </tr>
                <td>
                    <div class="statisticsSelectPatientDiv">
                        <?php
                        $showAllPatientsCheckBox = false;
                        if ((array_key_exists('PatientsAll', $filter) && $filter['PatientsAll']) ||
                            (!array_key_exists('Patients', $filter)) || (count($filter['Patients']) == 0)
                        ) {
                            $showAllPatientsCheckBox = true;
                            $filter['Patients']      = [];
                        }
                        echo CHtml::checkBox('Filter[PatientsAll]', $showAllPatientsCheckBox);
                        ?>
                        <label for="Filter_PatientsAll"><?= Yii::t('Statistics2018Module.statistics', 'Все') ?></label>
                        <br>
                        <?php
                        $allPatients = Patient::model()->onlyAssigned()->findAll();
                        // Сортируем пациентов, выделенные наверх.
                        $patientsList = [];
                        $patientsList = $this->formCheckboxList($filter, 'Patients', $allPatients, 'uid', 'human_readable_id');
                        echo CHtml::checkBoxList(
                            'Filter[Patients]',
                            array_key_exists('Patients', $filter) ? $filter['Patients'] : [],
                            $patientsList,
                            [
                                'onclick' => '$("#Filter_PatientsAll").prop("checked", false);'
                            ]
                        );
                        ?>
                    </div>
                </td>
                <td class="statisticsSelectDeviceType deviceTypeColumn">
                    <?php
                    $showAllDeviceType = false;
                    if ((array_key_exists('DeviceTypeAll', $filter) && $filter['DeviceTypeAll']) ||
                        (!array_key_exists('DeviceType', $filter)) || (count($filter['DeviceType']) == 0)
                    ) {
                        $showAllDeviceType    = true;
                        $filter['DeviceType'] = [];
                    }
                    echo CHtml::checkBox('Filter[DeviceTypeAll]', $showAllDeviceType);
                    ?>
                    <label for="Filter_DeviceTypeAll"><?= Yii::t('Statistics2018Module.statistics', 'Все') ?></label>
                    <br>
                    <?php
                    $allDeviceTypes = DeviceType::model()->findAll();
                    echo CHtml::checkBoxList(
                        'Filter[DeviceType]',
                        array_key_exists('DeviceType', $filter) ? $filter['DeviceType'] : [],
                        CHtml::listData($allDeviceTypes, 'uid', function (DeviceType $deviceType) {
                            return $deviceType->name;
                        }),
                        [
                            'onclick' => '$("#Filter_DeviceTypeAll").prop("checked", false);'
                        ]
                    );
                    ?>
                </td>
                <td class="statisticsSelectDevice">
                    <div class="statisticsSelectDeviceDiv">
                        <?php
                        $showAllDeviceModel = false;
                        if ((array_key_exists('DeviceModelAll', $filter) && $filter['DeviceModelAll']) ||
                            (!array_key_exists('DeviceModel', $filter)) || (count($filter['DeviceModel']) == 0)
                        ) {
                            $showAllDeviceModel    = true;
                            $filter['DeviceModel'] = [];
                        }
                        echo CHtml::checkBox('Filter[DeviceModelAll]', $showAllDeviceModel);
                        ?>
                        <label for="Filter_DeviceModelAll"><?= Yii::t('Statistics2018Module.statistics', 'Все') ?></label>
                        <br>
                        <?php
                        $allDeviceModels  = DeviceModel::model()->findAll();
                        $deviceModelsList = $this->formCheckboxList($filter, 'DeviceModel', $allDeviceModels, 'name', 'name');
                        echo CHtml::checkBoxList(
                            'Filter[DeviceModel]',
                            array_key_exists('DeviceModel', $filter) ? $filter['DeviceModel'] : [],
                            $deviceModelsList
                            ,
                            [
                                'onclick' => '$("#Filter_DeviceModelAll").prop("checked", false);'
                            ]);
                        ?>
                    </div>
                </td>
                <tr>
                    <td colspan="3">
                        <br>
                        <input type="submit" name="Filter[formStatistics]" value="<?= Yii::t('Statistics2018Module.statistics', 'Сформировать статистику') ?>">
                        <input type="button" onclick="window.location.href = '/statistics2018/view/index'" value="<?= Yii::t('Statistics2018Module.statistics', 'Сбросить фильтры') ?>">
                    </td>
                </tr>
            </table>
        </form>
    </span>