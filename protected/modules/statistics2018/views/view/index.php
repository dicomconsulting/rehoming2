<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 26.01.16
 * Time: 17:19
 */
/* @var $this ViewController */
/* @var $filter */
/* @var $totalPatientsStatistics */
/* @var $patientSexStatistics */
/* @var $patientAgeStatistics */
/* @var $observationPeriodStatistics */
/* @var $nyhaStatistics */
/* @var $rhythmDisturbancesStatistics */
/* @var $rhythmDisturbancesCRTAndICDOnlyStatistics */
/* @var $seriousAdverseEvents */
/* @var $etiologyStatistics */
/* @var $preparationsStatistics */
/* @var $heartSurgeryStatistics */
/* @var $eksStatistics */
/* @var $ikdCrtStatistics */
/* @var $implantationStimulatingStatistics */
/* @var $trendMonitoringStatistics */
/* @var $trendMonitoringAdditionalStatistics */
/* @var $adverseEventsStatistics */
/* @var $adverseImplantStatistics */
/* @var $shockDischargesStatistics */
/* @var $homeMonitoringStatistics */
/* @var $homeMonitoringStatisticsPart2 */
/* @var $functionRatingsStatistics */
/* @var $implantWithAdverseEventStatistics */
/* @var $totalNumberVisitStatistics */
/* @var $visitsNumberStatistics */
/* @var $clinicLoadStatistics */
/* @var $daysBeforeTherapyStatistics */
/* @var $ventricularArrhythmiasStatistics */
/* @var $countershockStatistics */


Yii::app()->clientScript->registerCssFile(
    Yii::app()->assetManager->publish(
        Yii::getPathOfAlias('statistics2018.assets') . '/css/statistics.css'
    )
);
?>
<h2 class="noprint"><?= Yii::t('Statistics2018Module.statistics', 'Статистика') ?></h2>

<div class="right noprint" style="text-align: right; margin-bottom: 20px;margin-top: -35px;">
    <i class="icon-xls"></i> <?= CHtml::link(Yii::t('Statistics2018Module.statistics', 'Экспорт'), $this->createUrl('/statistics2018/export/xls', $_GET)) ?>
    &nbsp;&nbsp;&nbsp;
    <i class="icon-print"></i> <?= CHtml::link(Yii::t('Statistics2018Module.statistics', 'Печать'), $this->createUrl($this->id . "/print", $_GET)) ?>
</div>
<?php
echo $this->renderPartial('savedFilter', $_data_);
echo $this->renderPartial('filter', $_data_);

if (array_key_exists('formStatistics', $filter)) {
    if (!isset($totalPatientsStatistics)) {
        echo Yii::t('Statistics2018Module.statistics', 'Нет пациентов удолетворяющих условиям. <br>');
    } else {
        echo Yii::t('Statistics2018Module.statistics', 'Всего пациентов в выборке: ') . $totalPatientsStatistics . '<br>';

        $total = $patientSexStatistics['total'];
        unset($patientSexStatistics['total']);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('Statistics2018Module.statistics', sprintf('Пол пациента (количество пациентов в выборке – %s)', $total)),
            'data'   => $patientSexStatistics,
            'tableValueName' => 'Количество пациентов',
        ]);
        $total = $patientAgeStatistics['total'];
        unset($patientAgeStatistics['total']);
        echo $this->renderPartial('../reportRenderer/simpleRenderer', [
            'header' => Yii::t('Statistics2018Module.statistics', sprintf('Возраст пациента (количество пациентов в выборке – %s)', $total)),
            'data'   => $patientAgeStatistics,
            'tableValueName' => 'Количество лет',
        ]);
        $total = $observationPeriodStatistics['total'];
        unset($observationPeriodStatistics['total']);
        echo $this->renderPartial('../reportRenderer/simpleRenderer', [
            'header' => Yii::t('Statistics2018Module.statistics', sprintf('Период наблюдения (количество пациентов в выборке – %s)', $total)),
            'tableValueName' => 'Количество месяцев',
            'data'   => $observationPeriodStatistics
        ]);
        $total = $nyhaStatistics['total'];
        unset($nyhaStatistics['total']);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('Statistics2018Module.statistics', sprintf('NYHA класс СН (количество пациентов в выборке – %s)', $total)),
            'data'   => $nyhaStatistics,
            'tableValueName' => 'Количество пациентов',
        ]);
        $rhythmDisturbancesStatisticsTotal = $rhythmDisturbancesStatistics['total'];
        unset($rhythmDisturbancesStatistics['total']);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('Statistics2018Module.statistics', sprintf('Нарушения проводимости (общая группа), количество пациентов в выборке – %s', $rhythmDisturbancesStatisticsTotal)),
            'data'   => $rhythmDisturbancesStatistics['blockade'],
            'tableValueName' => 'Количество пациентов',
        ]);
        $rhythmDisturbancesCRTAndICDOnlyStatisticsTotal = $rhythmDisturbancesCRTAndICDOnlyStatistics['total'];
        unset($rhythmDisturbancesCRTAndICDOnlyStatistics['total']);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('Statistics2018Module.statistics', sprintf('Нарушения проводимости (для пациентов с ИКД и CRT), количество пациентов в выборке – %s', $rhythmDisturbancesCRTAndICDOnlyStatisticsTotal)),
            'data'   => $rhythmDisturbancesCRTAndICDOnlyStatistics['blockade'],
            'tableValueName' => 'Количество пациентов',
        ]);
        $total = $trendMonitoringStatistics['allStats']['total']['cnt'];
        $percent = $trendMonitoringStatistics['allStats']['total']['average'];
        $all = $trendMonitoringStatistics['allStats']['total']['all'];
        unset($trendMonitoringStatistics['allStats']['total']);
        echo $this->renderPartial('../reportRenderer/supraventricularArrhythmiasRenderer', [
            'header' => Yii::t('Statistics2018Module.statistics', sprintf('Наджелудочковые аритмии (по всем пациентам), количество пациентов с наджелудочковыми аритмиями – %s (%s) от общего количества пациентов – %s', $total, $percent, $all)),
            'column1Header' => Yii::t('Statistics2018Module.statistics', 'Количество пациентов'),
            'column2Header' => Yii::t('Statistics2018Module.statistics', 'Среднее количество приступов на одного пациента'),
            'data'   => $trendMonitoringStatistics['allStats']
        ]);
        $total = $trendMonitoringStatistics['ikdStats']['total']['cnt'];
        $percent = $trendMonitoringStatistics['ikdStats']['total']['average'];
        $all = $trendMonitoringStatistics['ikdStats']['total']['all'];
        unset($trendMonitoringStatistics['ikdStats']['total']);
        echo $this->renderPartial('../reportRenderer/supraventricularArrhythmiasRenderer', [
            'header' => Yii::t('Statistics2018Module.statistics', sprintf('Наджелудочковые аритмии (по пациентам ИКД и CRT-D), количество пациентов с наджелудочковыми аритмиями – %s (%s) от общего количества пациентов с ИКД и CRT-D – %s', $total, $percent, $all)),
            'column1Header' => Yii::t('Statistics2018Module.statistics', 'Количество пациентов'),
            'column2Header' => Yii::t('Statistics2018Module.statistics', 'Среднее количество приступов на одного пациента'),
            'data'   => $trendMonitoringStatistics['ikdStats']
        ]);
        $total = $ventricularArrhythmiasStatistics['total']['cnt'];
        $percent = $ventricularArrhythmiasStatistics['total']['average'];
        $all = $ventricularArrhythmiasStatistics['total']['all'];
        unset($ventricularArrhythmiasStatistics['total']);
        echo $this->renderPartial('../reportRenderer/deviationShortByValueRenderer', [
            'header' => Yii::t('Statistics2018Module.statistics', sprintf('Желудочковые нарушения ритма, количество пациентов – %s (%s) от общего количества пациентов с ИКД и CRT – %s', $total, $percent, $all)),
            'column1Header' => Yii::t('Statistics2018Module.statistics', 'Количество пациентов'),
            'column2Header' => Yii::t('Statistics2018Module.statistics', 'Среднее количество приступов'),
            'data'   => $ventricularArrhythmiasStatistics
        ]);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('Statistics2018Module.statistics', sprintf('Наджелудочковые нарушения ритма в анамнезе (общая группа), количество пациентов в выборке – %s', $rhythmDisturbancesStatisticsTotal)),
            'data'   => $rhythmDisturbancesStatistics['rhythmAnamnesis'],
            'tableValueName' => 'Количество пациентов',
        ]);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('Statistics2018Module.statistics', sprintf('Наджелудочковые нарушения ритма в анамнезе (для пациентов с ИКД и CRT), количество пациентов в выборке – %s', $rhythmDisturbancesCRTAndICDOnlyStatisticsTotal)),
            'data'   => $rhythmDisturbancesCRTAndICDOnlyStatistics['rhythmAnamnesis'],
            'tableValueName' => 'Количество пациентов',
        ]);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('Statistics2018Module.statistics', sprintf('Желудочковые нарушения ритма в анамнезе (пациенты с ИКД и CRT), количество пациентов в выборке – %s', $rhythmDisturbancesCRTAndICDOnlyStatisticsTotal)),
            'data'   => $rhythmDisturbancesCRTAndICDOnlyStatistics['anamnesis'],
            'tableValueName' => 'Количество пациентов',
        ]);
        $total = $etiologyStatistics['total'];
        unset($etiologyStatistics['total']);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('Statistics2018Module.statistics', sprintf('Этиология (количество пациентов в выборке – %s)', $total)),
            'data'   => $etiologyStatistics,
            'tableValueName' => 'Количество пациентов',
        ]);
        $total = $preparationsStatistics['total']['cnt'];
        $percent = $preparationsStatistics['total']['average'];
        $all = $preparationsStatistics['total']['all'];
        unset($preparationsStatistics['total']);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('Statistics2018Module.statistics', sprintf('Препараты, принимаемые к моменту включения в исследование (количество пациентов, принимающих лекарства – %s (%s) от общего количества пациентов – %s)', $total, $percent, $all)),
            'data'   => $preparationsStatistics,
            'tableValueName' => 'Количество пациентов',
        ]);
        $total = $heartSurgeryStatistics['total']['cnt'];
        $percent = $heartSurgeryStatistics['total']['average'];
        $all = $heartSurgeryStatistics['total']['all'];
        unset($heartSurgeryStatistics['total']);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('Statistics2018Module.statistics', sprintf('Операции на сердце (количество пациентов, которым проведена операция – %s (%s), от общего количества пациентов – %s', $total, $percent, $all)),
            'data'   => $heartSurgeryStatistics,
            'tableValueName' => 'Количество пациентов',
        ]);
        $total = $eksStatistics['total']['cnt'];
        $percent = $eksStatistics['total']['average'];
        $all = $eksStatistics['total']['all'];
        unset($eksStatistics['total']);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('Statistics2018Module.statistics', sprintf('Причина имплантации ЭКС (количество пациентов в выборке – %s (%s), от общего количества пациентов – %s', $total, $percent, $all)),
            'data'   => $eksStatistics,
            'tableValueName' => 'Количество пациентов',
        ]);
        $total = $ikdCrtStatistics['total']['cnt'];
        $percent = $ikdCrtStatistics['total']['average'];
        $all = $ikdCrtStatistics['total']['all'];
        unset($ikdCrtStatistics['total']);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('Statistics2018Module.statistics', sprintf('Причина имплантации ИКД, CRT (количество пациентов в выборке – %s (%s), от общего количества пациентов – %s', $total, $percent, $all)),
            'data'   => $ikdCrtStatistics,
            'tableValueName' => 'Количество пациентов',
        ]);
        $total = $implantationStimulatingStatistics['total'];
        unset($implantationStimulatingStatistics['total']);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('Statistics2018Module.statistics', sprintf('Имплантация антиаритмического устройства (количество пациентов в выборке – %s)', $total)),
            'data'   => $implantationStimulatingStatistics,
            'tableValueName' => 'Количество пациентов',
        ]);
        $total = $countershockStatistics['total'];
        unset($countershockStatistics['total']);
        echo $this->renderPartial('../reportRenderer/countershockRenderer', [
            'header'        => Yii::t('Statistics2018Module.statistics', sprintf('Электроимпульсная терапия (для пациентов с ИКД и CRT), количество пациентов в выборке – %s', $total)),
            'data'          => $countershockStatistics
        ]);
        $total = $adverseEventsStatistics['total'];
        unset($adverseEventsStatistics['total']);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('Statistics2018Module.statistics', sprintf('Нежелательные явления (количество пациентов в выборке – %s)', $total)),
            'data'   => $adverseEventsStatistics,
            'tableValueName' => Yii::t('Statistics2018Module.statistics', 'Количество пациентов'),
        ]);
        $total = $seriousAdverseEvents['total'];
        unset($seriousAdverseEvents['total']);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer4col', [
            'header' => Yii::t('Statistics2018Module.statistics', sprintf('Серьезность нежелательного явления (количество пациентов в выборке – %s)', $total)),
            'data'   => $seriousAdverseEvents,
            'totalPatients' => $total,
            'column1Header' => Yii::t('Statistics2018Module.statistics', 'Количество протоколов'),
            'column2Header' => Yii::t('Statistics2018Module.statistics', 'Количество пациентов'),
        ]);
        $total = $adverseImplantStatistics['total'];
        unset($adverseImplantStatistics['total']);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('Statistics2018Module.statistics', sprintf('Связь нежелательного явления с имплантатом (количество протоколов в выборке – %s)', $total)),
            'data'   => $adverseImplantStatistics,
            'tableValueName' => Yii::t('Statistics2018Module.statistics', 'Количество протоколов'),
        ]);
        $total = $implantWithAdverseEventStatistics['total'];
        unset($implantWithAdverseEventStatistics['total']);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('Statistics2018Module.statistics', sprintf('Имплантат, связанный с нежелательным явлением (количество пациентов в выборке – %s)', $total)),
            'data'   => $implantWithAdverseEventStatistics
        ]);



//        echo $this->renderPartial('../reportRenderer/simpleRenderer', [
//            'header' => Yii::t('Statistics2018Module.statistics', 'Нанесенные шоковые разряды'),
//            'data'   => $shockDischargesStatistics
//        ]);
?>
        <h5><?= Yii::t('Statistics2018Module.statistics', 'Опции Home Monitoring') ?></h5>
<?php
//        echo $this->renderPartial('../reportRenderer/homeMonitoringChangeOptionsRenderer', [
//            'data'   => $homeMonitoringStatistics
//        ]);
//        echo $this->renderPartial('../reportRenderer/simpleRenderer', [
//            'header' => '',
//            'data'   => $homeMonitoringStatisticsPart2
//        ]);
        echo $this->renderPartial('../reportRenderer/simpleRenderer', [
            'header' => '',
            'data'   => array_merge($homeMonitoringStatistics, $homeMonitoringStatisticsPart2),
        ]);

        $total = $functionRatingsStatistics['total'];
        echo '<h5>' . Yii::t('Statistics2018Module.statistics', sprintf('Оценка функций (экономия времени), общее количество заполненных протоколов – %s', $total)) . '</h5>';
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header'        => '',
            'parameterName' => Yii::t('Statistics2018Module.statistics', 'Home Monitoring Service Center'),
            'data'          => $functionRatingsStatistics['rtgTimeHmsc']
        ]);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header'        => '',
            'parameterName' => Yii::t('Statistics2018Module.statistics', 'Концепция "Светофор"'),
            'data'          => $functionRatingsStatistics['rtgTimeSemaphore']
        ]);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header'        => '',
            'parameterName' => Yii::t('Statistics2018Module.statistics', 'IEGM online'),
            'data'          => $functionRatingsStatistics['rtgTimeIegm']
        ]);

        echo '<h5>' . Yii::t('Statistics2018Module.statistics', 'Оценка функций (клиника)') . '</h5>';
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header'        => '',
            'parameterName' => Yii::t('Statistics2018Module.statistics', 'Home Monitoring Service Center'),
            'data'          => $functionRatingsStatistics['rtgClHmsc']
        ]);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header'        => '',
            'parameterName' => Yii::t('Statistics2018Module.statistics', 'Концепция "Светофор"'),
            'data'          => $functionRatingsStatistics['rtgClSemaphore']
        ]);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header'        => '',
            'parameterName' => Yii::t('Statistics2018Module.statistics', 'IEGM online'),
            'data'          => $functionRatingsStatistics['rtgClIegm']
        ]);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header'        => '',
            'parameterName' => Yii::t('Statistics2018Module.statistics', 'Достаточность данных HM'),
            'data'          => $functionRatingsStatistics['rtgClDataSuff']
        ]);

        echo $this->renderPartial('../reportRenderer/deviationShortByValueRenderer2', [
            'header'        => Yii::t('Statistics2018Module.statistics', 'Общее количество посещений клиники'),
            'column1Header' => Yii::t('Statistics2018Module.statistics', 'Среднее количество'),
            'data'          => $totalNumberVisitStatistics
        ]);
        $total = $visitsNumberStatistics['total'];
        unset($visitsNumberStatistics['total']);
        echo $this->renderPartial('../reportRenderer/simpleRenderer', [
            'header'        => Yii::t('Statistics2018Module.statistics', sprintf('Количество посещений клиники (количество пациентов в выборке – %s)', $total)),
            'data'          => $visitsNumberStatistics
        ]);
        echo $this->renderPartial('../reportRenderer/deviationShortByValueRenderer2', [
            'header'        => Yii::t('Statistics2018Module.statistics', 'Оценка клинической нагрузки'),
            'column1Header' => Yii::t('Statistics2018Module.statistics', 'Среднее количество'),
            'data'          => $clinicLoadStatistics
        ]);
        echo $this->renderPartial('../reportRenderer/simpleRenderer', [
            'header'        => Yii::t('Statistics2018Module.statistics', 'Количество дней до 1 терапии'),
            'data'          => $daysBeforeTherapyStatistics
        ]);
    }
}
?>

