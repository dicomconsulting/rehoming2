<?php

include_once dirname(__FILE__) . '/../../../helpers/Stats.php';

class StatsTest extends PHPUnit_Framework_TestCase
{

    public function testSmartCalcPeriod()
    {
        $this->assertEquals(7, Stats::smartCalcPeriod('2015-05-10', '2015-12-14'));
        $this->assertEquals(8, Stats::smartCalcPeriod('2015-05-10', '2015-12-15'));
        $this->assertEquals(7, Stats::smartCalcPeriod('2015-05-15', '2015-12-15'));
        $this->assertEquals(0, Stats::smartCalcPeriod('2014-06-30', '2014-07-03'));
        $this->assertEquals(1, Stats::smartCalcPeriod('2014-06-30', '2014-07-15'));
        $this->assertEquals(2, Stats::smartCalcPeriod('2014-06-14', '2014-07-15'));
    }
}
