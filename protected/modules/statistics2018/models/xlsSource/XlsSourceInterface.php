<?php

interface XlsSourceInterface {

    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getVarsValues($patients);
}
