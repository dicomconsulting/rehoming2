<?php

/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 14.03.16
 * Time: 16:37
 */
abstract class BaseXlsSource
{
    abstract public function getVarPrefix();

    abstract public function getVarNames();

    protected function getParameterObjects()
    {
        $parameters      = [];
        $parametersDescr = $this->getVarNames();

        foreach ($parametersDescr as $parameterName => $parameterClass) {
            $parameters[$parameterName] = new $parameterClass();
        }
        return $parameters;
    }

    protected function baseFormVarsValues($objects)
    {
        $allVarsData = [];
        $parameters  = $this->getParameterObjects();
        $prefix      = $this->getVarPrefix();

        if ($objects) {
            foreach ($objects as $oneObject) {
                /** @var CActiveRecord $oneObject */
                foreach ($parameters as $parameterName => $parameterObject) {
                    /** @var XlsParameterInterface $parameterObject */
                    $objKey                                               = $oneObject->getPrimaryKey();
                    $allVarsData[$prefix . '.' . $parameterName][$objKey] = $parameterObject->getParameterValue($parameterName, $oneObject);
                }
            }
        } else {
            // Пустой список

        }
        return $allVarsData;
    }
}