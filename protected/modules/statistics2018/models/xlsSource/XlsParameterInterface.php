<?php

/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 14.03.16
 * Time: 16:35
 */
interface XlsParameterInterface
{
    /**
     * Получить значение одного параметра
     *
     * @param $name
     * @param $obj
     * @return mixed
     */
    public function getParameterValue($name, $obj);
}