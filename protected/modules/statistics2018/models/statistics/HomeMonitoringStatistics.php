<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 29.02.16
 * Time: 13:25
 */

/**
 * Статистика раздела: Опции Home Monitoring
 *
 * Class HeartSurgeryStatistics
 */
class HomeMonitoringStatistics implements StatisticsProviderInterface
{
    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $cntProtocol = 0;
        $cntReal     = 0;

        foreach ($patients as $patient) {
            $protocolEvolution = PatientStats::getFirstProtocolEvolution($patient);
            if ($protocolEvolution) {
                if ($protocolEvolution->option_changed || $protocolEvolution->red_finding || $protocolEvolution->red_finding_changed) {
                    $cntProtocol++;
                }
            }

            // Считать количество пациентов, у которых есть сообщение вида «Patient options changed by…» (после By указывается текст разный) на вкладке История
//            $criteria       = new CDbCriteria();
//            $criteria->with = ['device' => ['together' => true]];
//            $criteria->compare('device_id', $patient->device->uid);
//            $criteria->addCondition('text LIKE \'Patient options changed by%\'');
//            $criteria->order = 'time desc';
//
//            $historyMessage = HistoryMessage::model()->find($criteria);
//            if ($historyMessage) {
//                $cntReal++;
//            }
        }
        //Yii::t('Statistics2018Module.statistics', '')

        return [
            // Определяется % пациентов, у которых хотя бы в одном из параметров указано значение = Да.
            Yii::t('Statistics2018Module.statistics', 'Изменение опций')                => [
                'cnt' => $cntProtocol, //. ' ' . Yii::t('Statistics2018Module.statistics', '(из протокола инициализация HomeMonitoring)'),

            ],
//            'cntProtocol'   => $cntProtocol,
//            'cntReal'       => $cntReal,
//            'relativeValue' => $cntReal ? (round($cntProtocol / $cntReal * 100) . '%') : null
        ];
    }
}
