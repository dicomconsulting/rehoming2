<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 26.01.16
 * Time: 15:17
 */

class TotalPatientsStatistics implements StatisticsProviderInterface
{
    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        return count($patients);
    }
}
