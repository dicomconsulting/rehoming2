<?php

/**
 * Количество посещений клиники (количество протоколов "Регулярное амбулаторное обследование")
 * Class VisitsNumberStatistics
 */
class VisitsNumberStatistics implements StatisticsProviderInterface
{
    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $patientsProtocolsNumber = [];

        foreach ($patients as $patient) {
            $protocols = PatientStats::getAllProtocolRegular($patient);

            if ($protocols) {
                $patientsProtocolsNumber[$patient->uid] = count($protocols);
            }
        }

        return [
            Yii::t('Statistics2018Module.statistics', 'Количество посещений клиники') => [
                'cnt' => Stats::sum($patientsProtocolsNumber),
            ],
            'total' => count($patients),
        ];
    }
}