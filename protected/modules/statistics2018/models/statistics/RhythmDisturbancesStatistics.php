<?php


class RhythmDisturbancesStatistics implements StatisticsProviderInterface
{
    protected function testIsFibrillation(Patient $patient)
    {
        $protocolEnrolment = PatientStats::getFirstProtocolEnrolment($patient);;

        if ($protocolEnrolment) {
            return $protocolEnrolment->ca_block ? true : false;
        } else {
            return null;
        }
    }

    protected function testIsPermanentForm(Patient $patient)
    {
        $protocolEnrolment = PatientStats::getFirstProtocolEnrolment($patient);

        if ($protocolEnrolment) {
            return $protocolEnrolment->ca_block ? true : false;
        } else {
            return null;
        }
    }

    protected function testIsAuricularFlutter(Patient $patient)
    {
        $protocolEnrolment = PatientStats::getFirstProtocolEnrolment($patient);

        if ($protocolEnrolment) {
            return $protocolEnrolment->ca_block ? true : false;
        } else {
            return null;
        }
    }

    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $caBlockade           = 0;
        $fibrillation         = 0;
        $atrialPermanent      = 0;
        $fibrillationCount    = 0;
        $atrialFlutter        = 0;
        $allArrCupped         = [];
        $abBlock              = 0;
        $leftBranchBlock      = 0;
        $rightBranchBlock     = 0;
        $bothBranchBlock      = 0;
        $ventFibrillation     = 0;
        $ventTachy            = 0;
        $ventTachyTypes       = [];
        $syncopeUnknownOrigin = 0;
        $consciousness        = [];
        $total                = 0;
        $anyParameterChecked  = [];

        foreach ($patients as $patient) {
            $patientUid = $patient->uid;
            $protocolEnrolment = PatientStats::getFirstProtocolEnrolment($patient);
            $total++;
            if ($protocolEnrolment) {
                if ($protocolEnrolment->ca_block) {
                    $caBlockade++;
                    $anyParameterChecked[$patientUid] = true;
                }
                if ($protocolEnrolment->atrial_fibrillation) {
                    $fibrillation++;
                    $fibrillationCount++;

                    if ($protocolEnrolment->atrial_permanent) {
                        $atrialPermanent++;
                    }
                    $anyParameterChecked[$patientUid] = true;
                }
                if ($protocolEnrolment->atrial_flutter) {
                    $atrialFlutter++;
                    $anyParameterChecked[$patientUid] = true;
                }
                if ($protocolEnrolment->ab_block) {
                    $abBlock++;
                    $anyParameterChecked[$patientUid] = true;
                }
                if ($protocolEnrolment->left_branch_block) {
                    $leftBranchBlock++;
                    $anyParameterChecked[$patientUid] = true;
                }
                if ($protocolEnrolment->right_branch_block) {
                    $rightBranchBlock++;
                    $anyParameterChecked[$patientUid] = true;
                }
                if ($protocolEnrolment->both_branch_block) {
                    $bothBranchBlock++;
                    $anyParameterChecked[$patientUid] = true;
                }
                if ($protocolEnrolment->vent_fibrillation) {
                    $ventFibrillation++;
                    $anyParameterChecked[$patientUid] = true;
                }
                if ($protocolEnrolment->vent_tachy) {
                    $ventTachy++;
                    $ventTachyTypes[] = $protocolEnrolment->vent_tachy_type;
                    $anyParameterChecked[$patientUid] = true;
                }
                $allArrCupped[] = $protocolEnrolment->arr_cupped;
                if ($protocolEnrolment->syncope_unknown_origin) {
                    $syncopeUnknownOrigin++;
                    $anyParameterChecked[$patientUid] = true;
                }
                if ($protocolEnrolment->consciousness) {
                    $anyParameterChecked[$patientUid] = true;
                }

                $consciousness[] = $protocolEnrolment->consciousness;
            }
        }

        return [
            'blockade' => [
                // Определяется % пациентов, у которых значение параметра «CA блокада» = Да
                Yii::t('Statistics2018Module.statistics', 'CA блокада') => [
                    'cnt'      => $caBlockade,
                    'totalCnt' => $total
                ],
                // АВ-блокада
                Yii::t('Statistics2018Module.statistics', 'АВ-блокада') => [
                    'cnt'      => $abBlock,
                    'totalCnt' => $total
                ],
                // Блокада левой ножки пучка Гиса
                // Определяется % пациентов, у которых значение параметра «Блокада левой ножки пучка Гиса» = Да.
                Yii::t('Statistics2018Module.statistics', 'Блокада левой ножки пучка Гиса') => [
                    'cnt'      => $leftBranchBlock,
                    'totalCnt' => $total
                ],
                // Блокада правой ножки пучка Гиса
                // Определяется % пациентов, у которых значение параметра «Блокада правой ножки пучка Гиса» = Да.
                Yii::t('Statistics2018Module.statistics', 'Блокада правой ножки пучка Гиса') => [
                    'cnt'      => $rightBranchBlock,
                    'totalCnt' => $total
                ],
                // Двухпучковая блокада
                // Определяется % пациентов, у которых значение параметра «Двухпучковая блокада» = Да.
                Yii::t('Statistics2018Module.statistics', 'Двухпучковая блокада') => [
                    'cnt'      => $bothBranchBlock,
                    'totalCnt' => $total
                ],
            ],
            'rhythmAnamnesis' => [
                //Определяется % пациентов, у которых значение параметра «Фибрилляция» = Да
                Yii::t('Statistics2018Module.statistics', 'Фибрилляция предсердий') => [
                    'cnt'      => $fibrillation,
                    'totalCnt' => $total
                ],
                // Определяется % пациентов, у которых значение параметра «Постоянная форма» = Да.
                // % высчитывается по отношению к пациентам, у которых параметр Фибрилляция» = Да
                Yii::t('Statistics2018Module.statistics', '- Из них постоянная форма') => [
                    'cnt'      => $atrialPermanent,
                    'totalCnt' => $fibrillationCount
                ],
                // Определяется % пациентов, у которых значение параметра «Трепетание предсердий» = Да
                Yii::t('Statistics2018Module.statistics', 'Трепетание предсердий') => [
                    'cnt'      => $atrialFlutter,
                    'totalCnt' => $total
                ],
                // Как купируется аритмия
                Yii::t('Statistics2018Module.statistics', 'Аритмия купируется Медикаментозно') => [
                    'cnt'      => Stats::equalValuesCount($allArrCupped, '2'),
                    'totalCnt' => $total
                ],
                Yii::t('Statistics2018Module.statistics', 'Аритмия купируется Самостоятельно') => [
                    'cnt'      => Stats::equalValuesCount($allArrCupped, '1'),
                    'totalCnt' => $total
                ],
                Yii::t('Statistics2018Module.statistics', 'Аритмия купируется ЭИТ') => [
                    'cnt'      => Stats::equalValuesCount($allArrCupped, '3'),
                    'totalCnt' => $total
                ],
            ],










            // Фибрилляция желудочков в анамнезе
            // Определяется % пациентов, у которых значение параметра «Фибрилляция желудочков» = Да.
//            Yii::t('Statistics2018Module.statistics', 'Фибрилляция желудочков в анамнезе')         => [
//                'cnt'      => $ventFibrillation,
//                'totalCnt' => $total
//            ],
            // Желудочковая тахикардия в анамнезе
            // Определяется % пациентов, у которых значение параметра «Желудочковая тахикардия» = Да.
//            Yii::t('Statistics2018Module.statistics', 'Желудочковая тахикардия в анамнезе')        => [
//                'cnt'      => $ventTachy,
//                'totalCnt' => $total
//            ],
            // Тип желудочковой тахикардии:
            // В выборку попадают пациенты, к которых параметр «Желудочковая тахикардия» = Да.
            // Мономорфная
            // Полиморфная
//            Yii::t('Statistics2018Module.statistics', 'Тип желудочковой тахикардии Мономорфная')   => [
//                'cnt'      => Stats::equalValuesCount($ventTachyTypes, '1'),
//                'totalCnt' => $total
//            ],
//            Yii::t('Statistics2018Module.statistics', 'Тип желудочковой тахикардии Полиморфная')   => [
//                'cnt'      => Stats::equalValuesCount($ventTachyTypes, '2'),
//                'totalCnt' => $total
//            ],
            // Сознание при приступе
            // Пресинкопе
            // Синкопе
            // Клиническая смерть
            // По каждому варианту (Пресинкопе/Синкопе/Клиническая смерть) определяется % пациентов, у которых значение соответствующего параметра  = Да.
//            Yii::t('Statistics2018Module.statistics', 'Сознание при приступе: пресинкопе')         => [
//                'cnt'      => Stats::equalValuesCount($consciousness, '2'),
//                'totalCnt' => $total
//            ],
//            Yii::t('Statistics2018Module.statistics', 'Сознание при приступе: cинкопе')            => [
//                'cnt'      => Stats::equalValuesCount($consciousness, '3'),
//                'totalCnt' => $total
//            ],
//            Yii::t('Statistics2018Module.statistics', 'Сознание при приступе: клиническая смерть') => [
//                'cnt'      => Stats::equalValuesCount($consciousness, '1'),
//                'totalCnt' => $total
//            ],
            // Синкопе неизвестного генеза
            // Определяется % пациентов, у которых значение параметра «Синкопе неизвестного генеза» = Да.
//            Yii::t('Statistics2018Module.statistics', 'Синкопе неизвестного генеза')               => [
//                'cnt'      => $syncopeUnknownOrigin,
//                'totalCnt' => $total
//            ],
//            Yii::t('Statistics2018Module.statistics', 'Всего')                       => [
//                'cnt'      => count($anyParameterChecked),
//                'totalCnt' => $total
//            ],
            'total' => $total,
        ];
    }
}
