<?php


class PatientSexStatistics implements StatisticsProviderInterface
{
    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $femaleCount = 0;
        $maleCount   = 0;
        $unknown     = 0;
        $total       = count($patients);
        foreach ($patients as $patient) {
            if ($patient->sex == Patient::MALE) {
                $maleCount++;
            }
            if ($patient->sex == Patient::FEMALE) {
                $femaleCount++;
            }
            if ($patient->sex == Patient::UNKNOWN) {
                $unknown++;
            }

        }
        return [
            Yii::t('Statistics2018Module.statistics', 'Женщины')          => [
                'cnt'      => $femaleCount,
                'totalCnt' => $total
            ],
            Yii::t('Statistics2018Module.statistics', 'Мужчины')          => [
                'cnt'      => $maleCount,
                'totalCnt' => $total
            ],
            Yii::t('Statistics2018Module.statistics', 'Пол не определен') => [
                'cnt'      => $unknown,
                'totalCnt' => $total
            ],
//            Yii::t('Statistics2018Module.statistics', 'Всего')            => [
//                'cnt'             => $total,
//                'noRelativeValue' => true,
//            ],
            'total' => $total,
        ];
    }
}
