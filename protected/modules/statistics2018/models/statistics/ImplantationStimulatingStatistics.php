<?php


/**
 * Имплантация стимулирующей системы
 *
 * Class ImplantationStimulatingStatistics
 */
class ImplantationStimulatingStatistics implements StatisticsProviderInterface
{
    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $enrolmentPacer = 0;
        $enrolmentCrtP  = 0;
        $enrolmentCrtD  = 0;
        $enrolmentIcd   = 0;
        $sveSuccess     = 0;
        $total          = 0;
        $totalCrtPAndCrdD = 0;

        foreach ($patients as $patient) {
            $protocolEnrolment = PatientStats::getFirstProtocolEnrolment($patient);
            $total++;
            if ($protocolEnrolment) {
                if ($protocolEnrolment->enrolment_pacer) {
                    $enrolmentPacer++;
                }
                if ($protocolEnrolment->enrolment_crt_p) {
                    $enrolmentCrtP++;
                    $totalCrtPAndCrdD++;
                }
                if ($protocolEnrolment->enrolment_crt_d) {
                    $enrolmentCrtD++;
                    $totalCrtPAndCrdD++;
                }
                if ($protocolEnrolment->enrolment_icd) {
                    $enrolmentIcd++;
                }
                if ($protocolEnrolment->sve_success && $protocolEnrolment->pacer_model) {
                    $sveSuccess++;
                }

            }
        }

        return [
            // Определяется количество пациентов, у которых значение параметра «ЭКС» = Да.
            Yii::t('Statistics2018Module.statistics', 'ЭКС')                               => [
                'cnt'      => $enrolmentPacer,
                'totalCnt' => $total
            ],
            // Определяется количество пациентов, у которых значение параметра «CRT-P» = Да.
            Yii::t('Statistics2018Module.statistics', 'CRT-P')                             => [
                'cnt'      => $enrolmentCrtP,
                'totalCnt' => $total
            ],
            // Определяется количество пациентов, у которых значение параметра «ИКД» = Да.
            Yii::t('Statistics2018Module.statistics', 'ИКД')                               => [
                'cnt'      => $enrolmentIcd,
                'totalCnt' => $total
            ],
            // Определяется количество пациентов, у которых значение параметра «CRT-D» = Да.
            Yii::t('Statistics2018Module.statistics', 'CRT-D')                             => [
                'cnt'      => $enrolmentCrtD,
                'totalCnt' => $total
            ],
            // Определяется количество пациентов среди тех, у кого указана электрод КС(модель), для которых значение параметра «Имплантация успешна» = Да.
            Yii::t('Statistics2018Module.statistics', 'Успешная имплантация электрода КС (CRT-P, CRT-D)') => [
                'cnt'      => $sveSuccess,
                'totalCnt' => $totalCrtPAndCrdD
            ],
            'total' => $total,
        ];
    }
}
