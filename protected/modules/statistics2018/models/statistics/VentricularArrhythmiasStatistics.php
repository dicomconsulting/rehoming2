<?php

/**
 * Желудочковые нарушения ритма
 *
 * Class VentricularArrhythmiasStatistics
 */
class VentricularArrhythmiasStatistics extends BaseParameterStatistics implements StatisticsProviderInterface
{
    protected $totalCnt = [];

    /**
     * Получить статистику по Желудочковой Аритмии (Общее)
     *
     * @param $statDevices
     * @return array
     */
    public function getVentricularFibrillationTotal($statDevices)
    {
        $vt1ParameterId = $this->getParameterIdByAlias('vt1_episodes');
        $vt2ParameterId = $this->getParameterIdByAlias('vt2_episodes');
        $vfParameterId  = $this->getParameterIdByAlias('vf_episodes');

        $vt1Values = $this->getParameterValues($vt1ParameterId, $statDevices);
        $vt2Values = $this->getParameterValues($vt2ParameterId, $statDevices);
        $vfValues  = $this->getParameterValues($vfParameterId, $statDevices);

        // Объединяем случае, общего случая пришедших данных в одном пучке
        $allValues = [];

        /** @var DeviceValue $oneValue */
        foreach ($vt1Values as $oneValue) {
            if ($oneValue->value) {
                $patientId = $oneValue->deviceState->device->patient_id;
                if (!array_key_exists($patientId, $allValues)) {
                    $allValues[$patientId] = $oneValue->value;
                } else {
                    $allValues[$patientId] += $oneValue->value;
                }
                $this->totalCnt[$patientId] = true;
            }
        }
        foreach ($vt2Values as $oneValue) {
            if ($oneValue->value) {
                $patientId = $oneValue->deviceState->device->patient_id;
                if (!array_key_exists($patientId, $allValues)) {
                    $allValues[$patientId] = $oneValue->value;
                } else {
                    $allValues[$patientId] += $oneValue->value;
                }
                $this->totalCnt[$patientId] = true;
            }
        }
        foreach ($vfValues as $oneValue) {
            if ($oneValue->value) {
                $patientId = $oneValue->deviceState->device->patient_id;
                if (!array_key_exists($patientId, $allValues)) {
                    $allValues[$patientId] = $oneValue->value;
                } else {
                    $allValues[$patientId] += $oneValue->value;
                }
                $this->totalCnt[$patientId] = true;
            }
        }

        $cnt       = count($allValues);
        $average   = Stats::average($allValues);
        $deviation = Stats::standardDeviation($allValues);

        return [
            Yii::t('Statistics2018Module.statistics', 'Желудочковая аритмия (общее)') => [
                'cnt'               => $cnt,
                'average'           => $average,
                'standardDeviation' => $deviation,
                'occurrences'       => array_sum($allValues),
            ]
        ];
    }


    /**
     * Желудочковая тахикардия
     * @param $statDevices
     * @return array
     */
    public function getVentricularTachycardia($statDevices)
    {
        $vt1ParameterId = $this->getParameterIdByAlias('vt1_episodes');
        $vt2ParameterId = $this->getParameterIdByAlias('vt2_episodes');

        $vt1Values = $this->getParameterValues($vt1ParameterId, $statDevices);
        $vt2Values = $this->getParameterValues($vt2ParameterId, $statDevices);

        // Объединяем случае, общего случая пришедших данных в одном пучке
        $allValues = [];

        /** @var DeviceValue $oneValue */
        foreach ($vt1Values as $oneValue) {
            if ($oneValue->value) {
                $patientId = $oneValue->deviceState->device->patient_id;
                if (!array_key_exists($patientId, $allValues)) {
                    $allValues[$patientId] = $oneValue->value;
                } else {
                    $allValues[$patientId] += $oneValue->value;
                }
                $this->totalCnt[$patientId] = true;
            }
        }
        foreach ($vt2Values as $oneValue) {
            if ($oneValue->value) {
                $patientId = $oneValue->deviceState->device->patient_id;
                if (!array_key_exists($patientId, $allValues)) {
                    $allValues[$patientId] = $oneValue->value;
                } else {
                    $allValues[$patientId] += $oneValue->value;
                }
                $this->totalCnt[$patientId] = true;
            }
        }


        $cnt       = count($allValues);
        $average   = Stats::average($allValues);
        $deviation = Stats::standardDeviation($allValues);

        return [
            Yii::t('Statistics2018Module.statistics', 'Желудочковая тахикардия') => [
                'cnt'               => $cnt,
                'average'           => $average,
                'standardDeviation' => $deviation,
                'occurrences'       => array_sum($allValues),
            ]
        ];
    }

    /**
     * Фибрилляция желудочков
     *
     * @param $statDevices
     * @return array
     */
    public function getVentricularFibrillation($statDevices)
    {
        $vfParameterId = $this->getParameterIdByAlias('vf_episodes');
        $vfValues      = $this->getParameterValues($vfParameterId, $statDevices);

        // Объединяем случае, общего случая пришедших данных в одном пучке
        $allValues = [];

        /** @var DeviceValue $oneValue */
        foreach ($vfValues as $oneValue) {
            if ($oneValue->value) {
                $patientId = $oneValue->deviceState->device->patient_id;
                if (!array_key_exists($patientId, $allValues)) {
                    $allValues[$patientId] = $oneValue->value;
                } else {
                    $allValues[$patientId] += $oneValue->value;
                }
                $this->totalCnt[$patientId] = true;
            }
        }


        $cnt       = count($allValues);
        $average   = Stats::average($allValues);
        $deviation = Stats::standardDeviation($allValues);

        return [
            Yii::t('Statistics2018Module.statistics', 'Фибрилляция желудочков') => [
                'cnt'               => $cnt,
                'average'           => $average,
                'standardDeviation' => $deviation,
                'occurrences'       => array_sum($allValues),
            ]
        ];
    }

    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $patientsFiltered = [];
        foreach ($patients as $patient) {
//            $protocolEnrolment = PatientStats::getFirstProtocolEnrolment($patient);
            if (in_array($patient->device->model->device_type_uid, [2, 3, 4])) {
                $patientsFiltered[] = $patient;
            }
        }

        // Устроойства по которым ведеться статистика
        $statDevicesFiltered = $this->getStatDevices($patients, ['icd', 'crtp', 'crtd']);

        // Остальные параметры выделяем в отдельном разделе "Желудочковые нарушения ритма"
        $ventricularFibrillationTotalStats = $this->getVentricularFibrillationTotal($statDevicesFiltered);
        $ventricularTachycardiaStats       = $this->getVentricularTachycardia($statDevicesFiltered);
        $ventricularFibrillation           = $this->getVentricularFibrillation($statDevicesFiltered);

        return array_merge(
            $ventricularFibrillationTotalStats,
            $ventricularTachycardiaStats,
            $ventricularFibrillation,
            [
                'total' => [
                    'cnt'                       => count($this->totalCnt),
                    'average'                   => count($patientsFiltered) ? intval(count($this->totalCnt) / count($patientsFiltered) * 100) . '%' : null,
                    'all'                       => count($patientsFiltered),
                ]
            ]
        );
    }
}
