<?php

/**
 * Cтатистика: Количество дней до 1 терапии
 *
 * Class DaysBeforeTherapyStatistics
 */
class DaysBeforeTherapyStatistics implements StatisticsProviderInterface
{
    public function getPatientVTDate($patient)
    {
        $criteria       = new CDbCriteria();
        $criteria->with = ['device' => ['together' => true]];
        $criteria->compare('device_id', $patient->device->uid);
        $criteria->addInCondition('text', [
            'Finding "VT1 detected" initially detected.',
            'Finding "VT2 detected" initially detected.',
            'Finding "VF detected" initially detected.'
        ]);
        $criteria->order = 'time asc';

        $historyMessage = HistoryMessage::model()->find($criteria);
        if ($historyMessage) {
            return $historyMessage->time;
        } else {
            return null;
        }
    }


    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $daysStats = [];

        foreach ($patients as $patient) {
            $messageDateStart = $this->getPatientVTDate($patient);
            if ($messageDateStart) {
                $daysStats[$patient->uid] = Stats::smartCalcPeriodDays($patient->device->implantation_date, $messageDateStart);
            }
        }

        return [
            Yii::t('Statistics2018Module.statistics', 'Минимальное количество')                     => [
                'cnt'      => Stats::min($daysStats),
            ],
            Yii::t('Statistics2018Module.statistics', 'Максимальное количество')                    => [
                'cnt'      => Stats::max($daysStats),
            ],
            Yii::t('Statistics2018Module.statistics', 'Среднее количество')                   => [
                'cnt'      => Stats::average($daysStats),
            ],
            Yii::t('Statistics2018Module.statistics', 'Стандартное отклонение')                    => [
                'cnt'      => Stats::standardDeviation($daysStats)
            ],
        ];
    }
}
