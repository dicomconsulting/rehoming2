<?php

/**
 * Данные тренд-мониторинга имплантата
 *
 * Class TrendMonitoringStatistics
 */
class TrendMonitoringStatistics extends BaseParameterStatistics implements StatisticsProviderInterface
{
    private $allDevices;

    /**
     * Наличие наджелудочковых аритмий
     *
     * @param $statDevices
     * @return array
     */
    public function getPresenceSupraventricularArrhythmias($statDevices, $allDevicesCount)
    {
        // svt_episodes_total - Эпизоды НЖТ (общее количество)
        $parameterId     = $this->getParameterIdByAlias('svt_episodes_total');
        $parameterValues = $this->getParameterValues($parameterId, $statDevices, true);
        $plusValues      = []; // Выбираем положительные значения

        foreach ($parameterValues as $oneValue) {
            if ($oneValue->value) {
                $plusValues[] = $oneValue->value;
            }
        }

        $cnt       = count($plusValues);
        $average   = ($allDevicesCount > 0) ? number_format(array_sum($plusValues) / $allDevicesCount, 2, '.', ' ') : null;
        $deviation = Stats::standardDeviation($plusValues);

        $relative = ($allDevicesCount > 0) ? number_format($cnt / $allDevicesCount * 100, 2, '.', ' ') : null;

        $stats = [
            // Определяется количество пациентов, у которых указано значение параметра «Количество эпизодов НЖТ»).
            Yii::t('Statistics2018Module.statistics', 'Наличие наджелудочковых аритмий') => [
                'cnt'               => $cnt,
                'occurrences'       => array_sum($plusValues),
                'average'           => $average,
                'standardDeviation' => $deviation,
                'relative'          => $relative
            ]
        ];
        return $stats;
    }

    /**
     * Длительность более 12 часов в сутки
     *
     * @param $statDevices
     * @return array
     */
    public function getArrhythmiasDuration12Hours($statDevices, $allDevicesCount)
    {
        /**
         * Вы совершенно правы, нет специальной графы для "Количества НЖТ длительностью более 12 часов в сутки", однако их количество все же
         * можно подсчитать. Это моё упущение что не дал вам разъяснения ранее. Количество таких эпизодов можно подсчитать на вкладке "History".
         * Нужно посчитать общее количество сообщений "Finding "Long atrial episode detected" initially detected".
         * Такое сообщение по умолчанию формируется при длительности НЖТ более 12 часов.
         */
        $allMessages   = [];
        $messagesCount = 0;

        foreach ($statDevices as $oneDevice) {
            $allMessages[$oneDevice] = 0;
        }

        $uniqueDevices = [];

        $criteria = new CDbCriteria();
        $criteria->addInCondition('device_id', $statDevices);
        $criteria->addCondition("`text`='Finding \"Long atrial episode detected\" initially detected.'");
        $historyMessages = HistoryMessage::model()->findAll($criteria);

        foreach ($historyMessages as $oneMessage) {
            if (!in_array($oneMessage->device_id, $uniqueDevices)) {
                $uniqueDevices[] = $oneMessage->device_id;
            }
            $allMessages[$oneMessage->device_id]++;
            $messagesCount++;
        }

        $relative = ($allDevicesCount > 0) ? number_format(count($uniqueDevices) / $allDevicesCount * 100, 2, '.', ' ') : null;

        $stats = [
            // Определяется количество пациентов, у которых указано значение параметра «Количество эпизодов НЖТ»).
            Yii::t('Statistics2018Module.statistics', 'Длительность более 12 часов в сутки') => [
                'cnt'               => count($uniqueDevices),
                'occurrences'       => $messagesCount,
                'average'           => number_format($messagesCount / $allDevicesCount, 2, '.', ' '),
                'standardDeviation' => Stats::standardDeviation($allMessages),
                'relative'          => $relative
            ]
        ];
        return $stats;
    }

    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $patientsFiltered    = [];
        $patientsIkdFiltered = [];

        foreach ($patients as $patient) {
            $protocolEnrolment = PatientStats::getFirstProtocolEnrolment($patient);
            if ($protocolEnrolment) {
                if ($protocolEnrolment->atrial_permanent) {
                    continue;
                }
            }
            if (in_array($patient->device->model->device_type_uid, [2, 4])) {
                $patientsIkdFiltered[] = $patient;
            }
            $patientsFiltered[] = $patient;
        }

        // Устройства, по которым ведется статистика
        $filteredIkdDevices = $this->getStatDevices($patientsIkdFiltered, ['icd', 'crtd']);
        $filteredDevices    = $this->getStatDevices($patientsFiltered, ['pm', 'icd', 'crtp', 'crtd']);

        $this->allDevices   = $this->getStatDevices($patients, ['pm', 'icd', 'crtp', 'crtd']);
        $allDevicesCount    = count($this->allDevices);
        $ikdDevicesCount    = count($filteredIkdDevices);

        // По всем пациентам
        $presenceSupraventricularArrhythmiasStats = $this->getPresenceSupraventricularArrhythmias($filteredDevices, $allDevicesCount);
        $arrhythmiasDuration12Hours               = $this->getArrhythmiasDuration12Hours($filteredDevices, $allDevicesCount);

        // По пациентам с IKD
        $ikdPresenceSupraventricularArrhythmiasStats = $this->getPresenceSupraventricularArrhythmias($filteredIkdDevices, $ikdDevicesCount);
        $ikdArrhythmiasDuration12Hours               = $this->getArrhythmiasDuration12Hours($filteredIkdDevices, $ikdDevicesCount);

        /**
         * В выборку попадают пациенты, удовлетворяющие фильтрам и только те,
         * у которых установлены устройства ИКД или CRT-D.
         * Параметры VT1, VT2, VF необходимо брать Since implantation (нарастающий итог).
         * Все параметры задаются на вкладке «Статус» в разделе «Желудочковая аритмия».
         */
        $count = $ikdPresenceSupraventricularArrhythmiasStats[Yii::t('Statistics2018Module.statistics', 'Наличие наджелудочковых аритмий')]['cnt'];
        $ikdStats = array_merge(
            $ikdPresenceSupraventricularArrhythmiasStats,
            $ikdArrhythmiasDuration12Hours,
            [
                'total' => [
                    'cnt'     => $count,
                    'average' => (count($filteredIkdDevices) > 0) ? intval($count / count($filteredIkdDevices) * 100) . '%' : null,
                    'all'     => count($filteredIkdDevices),
                ]
            ]
        );

        $count = $presenceSupraventricularArrhythmiasStats[Yii::t('Statistics2018Module.statistics', 'Наличие наджелудочковых аритмий')]['cnt'];
        $allStats = array_merge(
            $presenceSupraventricularArrhythmiasStats,
            $arrhythmiasDuration12Hours,
            [
                'total' => [
                    'cnt'     => $count,
                    'average' => ($allDevicesCount > 0) ? intval($count / $allDevicesCount * 100) . '%' : null,
                    'all'     => $allDevicesCount,
                ]
            ]
        );
        return [
            'ikdStats' => $ikdStats,
            'allStats' => $allStats
        ];
    }
}
