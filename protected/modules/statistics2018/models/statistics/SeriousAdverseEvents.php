<?php

/**
 * Серьезность нежелательного явления
 *
 * Created by PhpStorm.
 * User: analitic
 * Date: 04.02.16
 * Time: 11:43
 */
class SeriousAdverseEvents implements StatisticsProviderInterface
{
    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        // Нежелательные явления
        $advLethal               = 0;
        $advDistress             = 0;
        $advSeriousDeterioration = 0;
        $advPrevented            = 0;
        $advDeath                = 0;
        $total                   = 0;

        $patientsCount = [
            'distress' => [],
            'lethal' => [],
            'serious_deterioration' => [],
            'prevented' => [],
        ];

        foreach ($patients as $patient) {
            // Протокол нежелательных явлений
            $protocols = PatientStats::getAllProtocolAdverse($patient);
            if ($protocols) {
                foreach ($protocols as $protocolAdverse) {
                    $naResult[] = $protocolAdverse->adv_result;
                    if ($protocolAdverse->adv_distress) {
                        if (!in_array($patient->uid, $patientsCount['distress'])) {
                            $patientsCount['distress'][] = $patient->uid;
                        }
                        $advDistress++;
                    }
                    if ($protocolAdverse->adv_lethal) {
                        if (!in_array($patient->uid, $patientsCount['lethal'])) {
                            $patientsCount['lethal'][] = $patient->uid;
                        }
                        $advLethal++;
                    }
                    if ($protocolAdverse->adv_injury || $protocolAdverse->adv_perm_damage || $protocolAdverse->adv_hospitalization || $protocolAdverse->adv_surg) {
                        if (!in_array($patient->uid, $patientsCount['serious_deterioration'])) {
                            $patientsCount['serious_deterioration'][] = $patient->uid;
                        }
                        $advSeriousDeterioration++;
                    }
                    if ($protocolAdverse->adv_prevented) {
                        if (!in_array($patient->uid, $patientsCount['prevented'])) {
                            $patientsCount['prevented'][] = $patient->uid;
                        }
                        $advPrevented++;
                    }
                    if ($protocolAdverse->adv_death_date) {
                        $advDeath++;
                    }
                    $total++;
                }
            }
        }

        // Примечание: в выборку попадают все протоколы «Нежелательное явление» по пациентам, удовлетворяющим фильтрам.
        // Все параметры задаются в протоколе «Нежелательное явление» в разделе «Серьезность нежелательного явления».
        // Поле Adv_result
        // Без последствий (полное восстановление) 1
        // Разрешилось с последствиями 2
        // Смерть пациента 3
        // Явление продолжается 4
        // Неизвестно (актуализируйте отчет) 5
        // Ухудшение явления 6
        return [
            // Нежелательное явление привело к смерти пациента
            Yii::t('Statistics2018Module.statistics', 'Нежелательное явление привело к смерти пациента') => [
                'cnt'      => $advLethal,
                'totalCnt' => $total,
                'patients_count' => count($patientsCount['lethal']),
            ],
            // Нежелательное явление привело к серьезному ухудшению состояния здоровья пациента
            Yii::t('Statistics2018Module.statistics', 'Нежелательное явление привело к серьезному ухудшению состояния здоровья пациента') => [
                'cnt'      => $advSeriousDeterioration,
                'totalCnt' => $total,
                'patients_count' => count($patientsCount['serious_deterioration']),
            ],
            // Нежелательное явление привело к дистрессу, смерти плода или врожденной аномалии, или дефекту рождения
            Yii::t('Statistics2018Module.statistics', 'Нежелательное явление привело к дистрессу, смерти плода или врожденной аномалии, или дефекту рождения') => [
                'cnt'      => $advDistress,
                'totalCnt' => $total,
                'patients_count' => count($patientsCount['distress']),
            ],
            // Явление, вероятно, привело бы к одному из указанных выше результатов, если бы не были предприняты правильные действия, вмешательства, или, если бы обстоятельства складывались менее благоприятно.
            Yii::t('Statistics2018Module.statistics',
                'Явление, вероятно, привело бы к одному из указанных выше результатов, если бы не были предприняты правильные действия, вмешательства, или, если бы обстоятельства складывались менее благоприятно.') => [
                'cnt'      => $advPrevented,
                'totalCnt' => $total,
                'patients_count' => count($patientsCount['prevented']),
            ],
            // Смерть пациента
//            Yii::t('Statistics2018Module.statistics', 'Смерть пациента') => [
//                'cnt'      => $advDeath,
//                'totalCnt' => $total
//            ],
            'total' => count($patients),
        ];
    }
}
