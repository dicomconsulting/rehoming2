<?php

/**
 * Препараты, принимаемые к моменту включения в исследование
 *
 * Class PreparationsStatistics
 */
class PreparationsStatistics implements StatisticsProviderInterface
{
    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $pharmCustom   = [];
        $patientsCount = [];

        // Массив: Имя группы => Список идентификаторов пацинтов
        $groupsStats       = [
            Yii::t('Statistics2018Module.statistics', 'Антиаритмики I класса')   => [],
            Yii::t('Statistics2018Module.statistics', 'b-блокаторы')             => [],
            Yii::t('Statistics2018Module.statistics', 'Антиаритмики III класса') => [],
            Yii::t('Statistics2018Module.statistics', 'Ca-антагонисты')          => [],
            Yii::t('Statistics2018Module.statistics', 'Сердечные гликозиды')     => [],
            Yii::t('Statistics2018Module.statistics', 'Ингибиторы АПФ')          => [],
            Yii::t('Statistics2018Module.statistics', 'Мочегонные')              => [],
            Yii::t('Statistics2018Module.statistics', 'Нитраты')                 => [],
            Yii::t('Statistics2018Module.statistics', 'Антикоагулянты')          => [],
        ];
        $pharmGroups       = ProtocolEnrolment::getPharmsTmp();
        $pharmGroupsReform = [];
        foreach ($pharmGroups as $oneGroup) {
            foreach ($oneGroup['products'] as $oneProduct) {
                $pharmGroupsReform[$oneProduct->id] = $oneGroup['name'];
            }
        }

        foreach ($patients as $patient) {
            $patientId         = $patient->uid;
            $protocolEnrolment = PatientStats::getFirstProtocolEnrolment($patient);
            if ($protocolEnrolment) {
                $pharmElements = $protocolEnrolment->pharm;
                foreach ($pharmElements as $onePharm) {
                    $productId = $onePharm->pharmProduct->id;
                    if (array_key_exists($productId, $pharmGroupsReform)) {
                        $groupName = $pharmGroupsReform[$productId];
                        if (array_key_exists($groupName, $groupsStats)) {
                            $groupsStats[$groupName][$patientId] = $patientId;
                        } else {
                            $groupsStats[$groupName] = [
                                $patientId => $patientId
                            ];
                        }
                    } else {
                        $pharmCustom[$patientId] = $patientId;
                    }
                    $patientsCount[$patientId] = true;
                }
                if ($protocolEnrolment->pharm_custom) {
                    $pharmCustom[$patientId]   = $patientId;
                    $patientsCount[$patientId] = true;
                }
            }
        }

        $total = count($patientsCount);

        $groupsStats[Yii::t('Statistics2018Module.statistics', 'Другие препараты')] = $pharmCustom;
        $returnValue                                                               = [];
        foreach ($groupsStats as $name => $value) {
            $returnValue[$name] = [
                'cnt'      => count($value),
                'totalCnt' => $total
            ];
        }
//        $returnValue[Yii::t('Statistics2018Module.statistics', 'Пациентов в выборке')] = [
//            'cnt'      => $total,
//            'totalCnt' => count($patients)
//        ];

        $returnValue['total'] = [
            'cnt' => $total,
            'average' => (count($patients) > 0 ? number_format(($total / count($patients)) * 100, 2, '.', ' ') : 0) . '%',
            'all' => count($patients),
        ];
        return $returnValue;
    }
}
