<?php

/**
 * Статистика раздела: Операции на сердце
 *
 * Class HeartSurgeryStatistics
 */
class HeartSurgeryStatistics implements StatisticsProviderInterface
{
    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $surgeyList = [];
        $surgArr    = 0;
        $surgArrRfa = 0;
        $surgCount  = [];
        $total      = 0;

        foreach ($patients as $patient) {
            $protocolEnrolment = PatientStats::getFirstProtocolEnrolment($patient);
            $total++;
            if ($protocolEnrolment) {
                $surgery = $protocolEnrolment->surgeryRel;
                foreach ($surgery as $oneSurgery) {
                    $surgeyList[]             = $oneSurgery->surgery_uid;
                    $surgCount[$patient->uid] = true;
                }
                if ($protocolEnrolment->surg_arr) {
                    $surgArr++;
                    $surgCount[$patient->uid] = true;
                }
                if ($protocolEnrolment->surg_arr_rfa) {
                    $surgArrRfa++;
                    $surgCount[$patient->uid] = true;
                }
            }
        }

        return [
            // Определяется количество пациентов, у которых значение параметра «Коронарное шунтирование» = Да.
            Yii::t('Statistics2018Module.statistics', 'Коронарное шунтирование')  => [
                'cnt'      => Stats::equalValuesCount($surgeyList, '1'),
                'totalCnt' => $total
            ],
            // Определяется количество пациентов, у которых значение параметра «Коронарная ангиопластика = Да.
            Yii::t('Statistics2018Module.statistics', 'Коронарная ангиопластика') => [
                'cnt'      => Stats::equalValuesCount($surgeyList, '2'),
                'totalCnt' => $total
            ],
            // Определяется количество пациентов, у которых значение параметра «Операции по поводу ВПС = Да.
            Yii::t('Statistics2018Module.statistics', 'Операции по поводу ВПС')   => [
                'cnt'      => Stats::equalValuesCount($surgeyList, '3'),
                'totalCnt' => $total
            ],
            // Определяется количество пациентов, у которых значение параметра «Операции по поводу ППС» = Да
            Yii::t('Statistics2018Module.statistics', 'Операции по поводу ППС')   => [
                'cnt'      => Stats::equalValuesCount($surgeyList, '4'),
                'totalCnt' => $total
            ],
            // По каждому варианту РЧА определяется количество пациентов, у которых значение соответствующего параметра  = Да.
            Yii::t('Statistics2018Module.statistics', 'РЧА аритмий')              => [
                'cnt'      => $surgArrRfa,
                'totalCnt' => $total
            ],
            // По каждому варианту Операция определяется количество пациентов, у которых значение соответствующего параметра  = Да.
            Yii::t('Statistics2018Module.statistics', 'Открытое операционное вмешательство при аритмиях')         => [
                'cnt'      => $surgArr,
                'totalCnt' => $total
            ],
            // По каждому варианту Операция определяется количество пациентов, у которых значение соответствующего параметра  = Да.
//            Yii::t('Statistics2018Module.statistics', 'Пациентов в выборке')      => [
//                'cnt'      => count($surgCount),
//                'totalCnt' => $total
//            ],
            'total' => [
                'cnt' => count($surgCount),
                'average' => ($total > 0 ? number_format((count($surgCount) / $total) * 100, 2, '.', ' ') : 0) . '%',
                'all' => $total, //count($patients),
            ],
        ];
    }
}
