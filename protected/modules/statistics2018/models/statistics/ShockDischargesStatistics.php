<?php

/**
 * Нанесенные шоковые разряды
 *
 * Class ShockDischargesStatistics
 */
class ShockDischargesStatistics extends BaseParameterStatistics implements StatisticsProviderInterface
{
    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $maxCount       = 0;
        $totalCount     = 0;
        $successCount   = 0;
        $patientsShocks = [];

        $startId   = $this->getParameterIdByAlias('shocks_started');
        $abortId   = $this->getParameterIdByAlias('shocks_aborted');
        $successId = $this->getParameterIdByAlias('shocks_successful');

        // Устроойства по которым ведеться статистика
        $statDevices = $this->getStatDevices($patients);


        /** @var Device $statDevice */
        foreach ($statDevices as $statDeviceId) {

            $deviceObj = Device::model()->findByPk($statDeviceId);
            $patientId = $deviceObj->patient_id;
            $patientsShocks[$patientId] = 0;

            // Перебираем всех пациентов, получаем у них значения параметров
            $startValues   = $this->getParameterValues($startId, [$statDeviceId]);
            $abortValues   = $this->getParameterValues($abortId, [$statDeviceId]);
            $successValues = $this->getParameterValues($successId, [$statDeviceId]);

            if (count($startValues) && count($abortValues)) {
                $cnt = $startValues[0]->value - $abortValues[0]->value; // Количество шоковых разрядов

                if ($maxCount < $cnt) {
                    $maxCount = $cnt;
                }
                $totalCount += $cnt;
                $patientsShocks[$patientId] = $cnt;
            }
            if (count($successValues)) {
                $successCount += $successValues[0]->value;
            }
        }

        $patientsCount = count($statDevices);
        if ($patientsCount) {
            $averageCount = round($totalCount / $patientsCount, 2);
            if ($successCount) {
                $effectivity = round($successCount / $totalCount /  $patientsCount * 100, 2);
            } else {
                $effectivity = null;
            }
        } else {
            $averageCount = null;
            $effectivity  = null;
        }
        $deviation = Stats::standardDeviation($patientsShocks);

        return [
            // Определяется максимальное количество нанесенных шоковых разрядов среди пациентов, попавших в выборку.
            Yii::t('Statistics2018Module.statistics', 'Максимальное количество нанесенных шоковых разрядов')                          => [
                'cnt' => $maxCount,
            ],
            // Определяется среднее количество нанесенных шоковых разрядов = ∑(Количество нанесенных шоковых разрядов)/Количество пациентов выборки
            Yii::t('Statistics2018Module.statistics', 'Среднее количество нанесенных шоковых разрядов')                               => [
                'cnt' => $averageCount,
            ],
            // Определяется стандартное отклонение параметра «Количество нанесенных шоковых разрядов» по всем пациентам выборки
            Yii::t('Statistics2018Module.statistics', 'Стандартное отклонение по параметру «Количество нанесенных шоковых разрядов»') => [
                'cnt' => $deviation,
            ],
            // Определяется по формуле: (∑(Количество нанесенных шоковых разрядов)/∑(Shocks successfull)/Количество пациентов выборки)*100%
            Yii::t('Statistics2018Module.statistics', 'Эффективность нанесенных шоковых разрядов')                                    => [
                'cnt' => $effectivity.' %',
            ],
            Yii::t('Statistics2018Module.statistics', 'Пациентов в выборке')  => [
                'cnt' => count($statDevices).' чел. '.intval(count($statDevices)/count($patients)*100).'%',
            ],
        ];
    }
}
