<?php


class ObservationPeriodStatistics implements StatisticsProviderInterface
{
    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $allPeriods = [];

        foreach ($patients as $patient) {
            $period = PatientStats::getPatientPeriod($patient);
            if ($period !== null) {
                $allPeriods[] = $period;
            }
        }

        return [
            // Определяется минимальный период наблюдения среди пациентов, попавших в выборку.
            Yii::t('Statistics2018Module.statistics', 'Минимальный период наблюдения')  => [
                'cnt' => Stats::min($allPeriods),
            ],
            // Определяется максимальный период наблюдения среди пациентов, попавших в выборку.
            Yii::t('Statistics2018Module.statistics', 'Максимальный период наблюдения') => [
                'cnt' => Stats::max($allPeriods),
            ],
            // Определяется средний период наблюдения = [Сумма всех периодов наблюдения пациентов выборки]/Количество пациентов выборки
            Yii::t('Statistics2018Module.statistics', 'Средний период наблюдения')      => [
                'cnt' => Stats::average($allPeriods),
            ],
            // Определяется стандартное отклонение параметра «Период наблюдения» по всем пациентам выборки
            Yii::t('Statistics2018Module.statistics', 'Стандартное отклонение')         => [
                'cnt' => Stats::standardDeviation($allPeriods),
            ],
            'total' => count($allPeriods),
//            Yii::t('Statistics2018Module.statistics', 'Пациентов в выборке')         => [
//                'cnt' => count($allPeriods),
//            ],
        ];
    }
}
