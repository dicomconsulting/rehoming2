<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 11.11.13
 * Time: 17:03
 */

class PrintFooter extends CWidget
{

    public $title;
    public $page;
    public $pageTotal;

    public function run()
    {
        $this->render(
            "print-footer",
            array("title" => $this->title, "page" => $this->page, "pageTotal" => $this->pageTotal)
        );
    }
}