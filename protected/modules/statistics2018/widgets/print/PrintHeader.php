<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 11.11.13
 * Time: 17:03
 */

class PrintHeader extends CWidget
{

    public $title;

    public function run()
    {
        $date = new RhDateTime();

        $this->render(
            "print-header",
            array("title" => $this->title, "date" => $date)
        );
    }
}
