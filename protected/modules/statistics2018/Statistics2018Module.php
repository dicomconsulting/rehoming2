<?php

class Statistics2018Module extends CWebModule
{
    public function init()
    {
        Yii::app()->getModule('protocols');
        Yii::app()->getModule('patient');
        // import the module-level models and components
        $this->setImport(
            array(
                'statistics2018.models.*',
                'statistics2018.models.statistics.*',
                'statistics2018.models.xlsSource.*',
                'statistics2018.helpers.*',
                'statistics2018.components.*',
                'statistics2018.widgets.print.*',
            )
        );
    }

    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        } else {
            return false;
        }
    }
}
