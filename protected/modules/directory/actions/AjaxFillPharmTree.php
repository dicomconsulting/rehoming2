<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 10.01.14
 * Time: 15:51
 */

class AjaxFillPharmTree extends CAction
{
    public function run()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            exit();
        }

        $parentId = null;
        if (isset($_GET['root']) && $_GET['root'] !== 'source') {
            $parentId = (int) $_GET['root'];
        }

        Yii::app()->getModule('directory');
        $children = PharmGroupTmp::model()->getDataForTree($parentId);

        $formattedData = [];

        foreach ($children as $child) {
            if ($child['group'] == 1) {
                $nodeText = "&nbsp;";
                $nodeText.= $child['text'];
                $child['text'] = CHtml::openTag(
                    "div",
                    array("style" => "margin-top:-4px;", "id" => 'pharmSelectorParent_' . $child['id'])
                ) . $nodeText . CHtml::closeTag("div");
                $formattedData[]=$child;

            } else {
                $options=array("class" => "inline");
                $nodeText = "&nbsp;";
                $nodeText .= CHtml::radioButton(
                    'pharmSelector',
                    false,
                    array(
                        "class" => "inline",
                        "value" => $child['id'],
                        "id" => 'pharmSelector_' . $child['id'],
                    )
                );
                $nodeText.= "&nbsp;";
                $nodeText.= CHtml::label($child['text'], 'pharmSelector_' . $child['id'], $options);
                $child['text'] = CHtml::openTag(
                    "div",
                    array("style" => "margin-top:-4px;")
                ) . $nodeText . CHtml::closeTag("div");
                $formattedData[]=$child;

            }
        }

        echo str_replace(
            '"hasChildren":"0"',
            '"hasChildren":false',
            CTreeView::saveDataAsJson($formattedData)
        );
    }
}
