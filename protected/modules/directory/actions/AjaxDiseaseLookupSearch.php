<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 10.01.14
 * Time: 15:51
 */

class AjaxDiseaseLookupSearch extends CAction
{
    public function run()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            exit();
        }

        $model = Icd10::model();
        $criteria = new CDbCriteria();
        $criteria->compare("t.name", $_GET['term'], true, "OR");
        $criteria->compare("t.code", $_GET['term'], true, "OR");

        $results = array();

        foreach ($model->findAll($criteria) as $m) {
            /** @var Icd10 $m */
            $results[] = array(
                "id" => $m->uid,
                "label" => $m->code . " " . $m->name,
                "value" => $m->code . " " . $m->name
            );
        }
        echo CJSON::encode($results);
    }
}
