<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 10.01.14
 * Time: 15:51
 */

class AjaxFillDiseaseTree extends CAction
{
    public function run()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            exit();
        }

        $parentId = null;
        if (isset($_GET['root']) && $_GET['root'] !== 'source') {
            $parentId = (int) $_GET['root'];
        }

        Yii::app()->getModule("directory");
        $children = Icd10::model()->getDataForTree($parentId);

        $formattedData = [];

        foreach ($children as $child) {
            $options=array("class" => "inline");
            $nodeText = "&nbsp;";
            $nodeText .= CHtml::radioButton(
                'etiologySelector',
                false,
                array(
                    "class" => "inline",
                    "value" => $child['id'],
                    "id" => 'etiologySelector_' . $child['id'],
                    "style" => "margin-top:-8px;"
                )
            );
            $nodeText.= "&nbsp;";
            $nodeText.= CHtml::label($child['text'], 'etiologySelector_' . $child['id'], $options);
            $child['text'] = $nodeText;
            $formattedData[]=$child;
        }

        echo str_replace(
            '"hasChildren":"0"',
            '"hasChildren":false',
            CTreeView::saveDataAsJson($formattedData)
        );
    }
}
