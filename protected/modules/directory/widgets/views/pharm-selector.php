<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 10.01.14
 * Time: 15:33
 */

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->assetManager->publish(
        Yii::getPathOfAlias('directory.assets').'/js/pharmSelector.js'
    ),
    CClientScript::POS_END
);


$this->beginWidget(
    'zii.widgets.jui.CJuiDialog',
    array(
        'id' => 'pharmSelector',
        'options' => array(
            'title' => Yii::t('ProtocolsModule.protocols', "Выберите лекарственный препарат"),
            'autoOpen' => false,
            'height' => '700',
            'width' => '800',
            'modal' => true,
            'buttons' => array(
                array(
                    'text' => Yii::t('ProtocolsModule.protocols', "Выбрать"),
                    "click" => 'js:function(){if(!PharmSelector.getInstance().processSelected()){return false};$(this).dialog("close");}'
                ),
                array(
                    "text" => Yii::t('ProtocolsModule.protocols', "Отмена"),
                    "click" => 'js:function(){$(this).dialog("close");}'
                )
            )
        ),
    )
);

$this->widget(
    'CTreeView',
    array('url' => array('pharmSelector.ajaxFillPharmTree', 'id' => $this->patient->uid))
);

$this->endWidget('zii.widgets.jui.CJuiDialog');

