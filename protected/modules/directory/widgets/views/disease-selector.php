<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 10.01.14
 * Time: 15:33
 */

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->assetManager->publish(
        Yii::getPathOfAlias('directory.assets').'/js/diseaseSelector.js'
    ),
    CClientScript::POS_END
);


$this->beginWidget(
    'zii.widgets.jui.CJuiDialog',
    array(
        'id' => 'etiologySelector',
        'options' => array(
            'title' => Yii::t('ProtocolsModule.protocols', "Выберите заболевание"),
            'autoOpen' => false,
            'height' => '700',
            'width' => '800',
            'modal' => true,
            'buttons' => array(
                array(
                    'text' => Yii::t('ProtocolsModule.protocols', "Выбрать"),
                    "click" => 'js:function(){if(!DiseaseSelector.getInstance().processSelected()){return false};$(this).dialog("close");}'
                ),
                array(
                    "text" => Yii::t('ProtocolsModule.protocols', "Отмена"),
                    "click" => 'js:function(){$(this).dialog("close");}'
                )
            )
        ),
    )
);

$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
        'attribute'=>'name',
        'model'=> Icd10::model(),
        'sourceUrl'=> $this->owner->createUrl(
            $this->owner->id . "/diseaseSelector.ajaxLookupSearch",
            array('id' => $this->patient->uid)
        ),
        'name'=>'desease_lookup',
        'options'=>array(
            'minLength'=>'3',
            'showAnim'=>'fold',
            'select'=>'js:function(event, ui) {$("#desease_lookup_selected").val(ui.item.id)}'
        ),
        'htmlOptions'=>array(
            'class'=> 'input-xxlarge',
        ),
    ));
?>
    <input type="hidden" id="desease_lookup_selected">
<?php
$this->widget(
    'CTreeView',
    array('url' => array('diseaseSelector.ajaxFillEtiologyTree', 'id' => $this->patient->uid))
);

$this->endWidget('zii.widgets.jui.CJuiDialog');
