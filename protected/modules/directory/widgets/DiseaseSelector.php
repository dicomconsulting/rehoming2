<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 10.01.14
 * Time: 15:16
 */

class DiseaseSelector extends CWidget
{
    public $patient;

    public function run()
    {
        $this->render("disease-selector");
    }

    public static function actions()
    {
        return [
            'ajaxFillEtiologyTree' => 'directory.actions.AjaxFillDiseaseTree',
            'ajaxLookupSearch' => 'directory.actions.AjaxDiseaseLookupSearch',
        ];
    }
}
