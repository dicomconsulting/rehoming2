<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 10.01.14
 * Time: 15:16
 */

class PharmSelector extends CWidget
{
    public $patient;

    public function run()
    {
        $this->render("pharm-selector");
    }

    public static function actions()
    {
        return [
            'ajaxFillPharmTree' => 'directory.actions.AjaxFillPharmTree',
        ];
    }
}
