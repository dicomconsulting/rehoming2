<?php

class ElectrodePlacementController extends BaseAdminController
{
    protected $pageTitle = 'Положение электрода';

    /**
     * @return string
     */
    public function getModelClassName()
    {
        return 'ElectrodePlacement';
    }
}

