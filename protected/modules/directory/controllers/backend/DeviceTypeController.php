<?php

class DeviceTypeController extends BaseAdminController
{
    protected $pageTitle = 'Типы устройств';

    /**
     * @return string
     */
    public function getModelClassName()
    {
        return 'DeviceType';
    }
}

