<?php

class DeviceAlterMessageController extends BaseAdminController
{
    protected $pageTitle = 'Сообщения Red и Yellow';

    /**
     * @return string
     */
    public function getModelClassName()
    {
        return DeviceAlterMessage::class;
    }
}

