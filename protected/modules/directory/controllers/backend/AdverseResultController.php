<?php

class AdverseResultController extends BaseAdminController
{
    protected $pageTitle = 'Исходы нежелательных явлений';

    /**
     * @return string
     */
    public function getModelClassName()
    {
        return 'AdverseResult';
    }
}

