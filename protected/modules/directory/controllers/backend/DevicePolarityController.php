<?php

class DevicePolarityController extends BaseAdminController
{
    protected $pageTitle = 'Полярность устройства';

    /**
     * @return string
     */
    public function getModelClassName()
    {
        return 'DevicePolarity';
    }
}

