<?php

class MeasurementUnitController extends BaseAdminController
{
    protected $pageTitle = 'Единицы измерения';

    /**
     * @return string
     */
    public function getModelClassName()
    {
        return 'MeasurementUnit';
    }
}

