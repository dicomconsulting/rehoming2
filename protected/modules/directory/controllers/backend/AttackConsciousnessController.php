<?php

class AttackConsciousnessController extends BaseAdminController
{
    protected $pageTitle = 'Опции сознания при приступе';

    /**
     * @return string
     */
    public function getModelClassName()
    {
        return 'AttackConsciousness';
    }
}

