<?php

class CountryController extends BaseAdminController
{
    protected $pageTitle = 'Страны';

    /**
     * @return string
     */
    public function getModelClassName()
    {
        return 'Country';
    }
}
