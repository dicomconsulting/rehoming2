<?php

class MedicalCenterController extends BaseAdminController
{
    protected $pageTitle = 'Медицинские центры';

    /**
     * @return string
     */
    public function getModelClassName()
    {
        return 'MedicalCenter';
    }
}

