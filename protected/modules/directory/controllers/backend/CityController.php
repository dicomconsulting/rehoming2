<?php

class CityController extends BaseAdminController
{
    protected $pageTitle = 'Города';

    /**
     * @return string
     */
    public function getModelClassName()
    {
        return 'City';
    }
}

