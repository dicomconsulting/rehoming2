<?php

class CaptureControlController extends BaseAdminController
{
    protected $pageTitle = 'Контроль захвата';

    /**
     * @return string
     */
    public function getModelClassName()
    {
        return 'CaptureControl';
    }
}

