<?php

class DeviceModelController extends BaseAdminController
{
    protected $pageTitle = 'Модели устройств';

    /**
     * @return string
     */
    public function getModelClassName()
    {
        return 'DeviceModel';
    }

    protected function edit(CActiveRecord $model)
    {
        if(isset($_POST[$this->getModelClassName()]))
        {
            $modelClassName = $this->getModelClassName();
            $model->attributes = $_POST[$modelClassName];
            if($model->save()) {
                $this->redirect(array('view','id' => $model->name));
            }
        }
        $this->render('edit', ['model' => $model]);
    }
}
