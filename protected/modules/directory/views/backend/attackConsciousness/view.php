<?php
/* @var $this MedicalCenterController */
/* @var $model MedicalCenter */
?>

    <h2><?=Yii::t("DirectoryModule.messages", "Опция сознания при приступе")?> №<?php echo $model->uid;?></h2>
    <?=CHtml::link(
        CHtml::tag('i', ['class' => 'icon-list'], ' ') . Yii::t("DirectoryModule.messages", "Вернуться к списку"),
        $this->createUrl('/directory/backend/attackConsciousness/index'),
        ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 10px;']
    )?>
    <?=CHtml::link(
        CHtml::tag('i', ['class' => 'icon-edit'], ' ') . Yii::t("DirectoryModule.messages", "Редактировать"),
        $this->createUrl('/directory/backend/attackConsciousness/update/', ['id' => $model->uid]),
        ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 10px;']
    )?>
    <?=CHtml::link(
        CHtml::tag('i', ['class' => 'icon-trash'], ' ') . Yii::t("DirectoryModule.messages", "Удалить"),
        $this->createUrl('/directory/backend/attackConsciousness/delete/', ['id' => $model->uid]),
        ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 10px;']
    )?>
    <?php $this->widget('zii.widgets.CDetailView', array(
        'data' => $model,
        'attributes'=>array(
            [
                "name" => Yii::t("DirectoryModule.messages", "Название"),
                "value" => $model->name,
            ]
        ),
    )); ?>


