<?php
/* @var $this MedicalCenterController */
/* @var $model MedicalCenter */
/* @var $form CActiveForm */
?>
<h2 class="offset1">
    <?=Yii::t("DirectoryModule.messages", "Исход")?>
    <?=$model->getIsNewRecord() ? '' : '№' . $model->uid;?>
</h2>
<div class="form">

    <?php $form = $this->beginWidget('I18nActiveForm', array(
        'id' => 'adverse-result-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => [
            'class' => 'form-horizontal'
        ]
    )); ?>

    <p class="note controls">
        <?=Yii::t("DirectoryModule.messages", "Поля отмеченные");?>
        <span class="required">"*"</span>
        <?=Yii::t("DirectoryModule.messages", "обязательны для заполнения");?>.
    </p>

    <div class="controls">
        <?=$form->errorSummary($model); ?>
    </div>

    <div class="control-group">
        <?=$form->labelEx($model, 'name', ['class' => 'control-label']); ?>
        <div class="controls">
            <?=$form->textField($model, 'name'); ?>
            <?=$form->error($model, 'name');?>
        </div>
    </div>

    <div class="row buttons controls">
        <?=CHtml::submitButton(
            Yii::t('DirectoryModule.messages', $model->getIsNewRecord() ? 'Создать' : 'Обновить'),
            ['class' => 'btn btn-primary']);
        ?>
        <?=CHtml::link(
            Yii::t('DirectoryModule.messages', 'Отмена'),
            $this->createUrl('/directory/backend/adverseResult/index'),
            ['class' => 'btn']);
        ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->