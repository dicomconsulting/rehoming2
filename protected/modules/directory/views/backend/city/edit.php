<?php
/* @var $this MedicalCenterController */
/* @var $model MedicalCenter */
/* @var $form CActiveForm */
?>
<h2 class="offset1">
    <?=Yii::t("DirectoryModule.messages", "Город")?>
    <?=$model->getIsNewRecord() ? '' : '№' . $model->uid;?>
</h2>
<div class="form">

    <?php $form = $this->beginWidget('I18nActiveForm', array(
        'id' => 'city-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => [
            'class' => 'form-horizontal'
        ]
    )); ?>

    <p class="note controls">
        <?=Yii::t("DirectoryModule.messages", "Поля отмеченные");?>
        <span class="required">"*"</span>
        <?=Yii::t("DirectoryModule.messages", "обязательны для заполнения");?>.
    </p>

    <div class="controls">
        <?=$form->errorSummary($model); ?>
    </div>

    <div class="control-group">
        <?=$form->labelEx($model, 'name', ['class' => 'control-label']); ?>
        <div class="controls">
            <?=$form->textField($model, 'name'); ?>
            <?=$form->error($model, 'name');?>
        </div>
    </div>

    <div class="control-group">
        <?=$form->labelEx($model, 'country_id', ['class' => 'control-label']); ?>
        <?php Country::enableOnlyActive(); ?>
        <div class="controls">
            <?=$form->dropDownList(
                $model,
                'country_id',
                CHtml::listData(
                    Country::model()->findAll(["order" => "name"]),
                    "uid" ,
                    function($country){
                        return $country->name;
                    }
                ),
                [
                    'empty' => Yii::t("DirectoryModule.messages", 'Выберите страну')
                ]
            ); ?>
            <?=$form->error($model, 'country_id');?>
        </div>
    </div>

    <div class="row buttons controls">
        <?=CHtml::submitButton(
            Yii::t('DirectoryModule.messages', $model->getIsNewRecord() ? 'Создать' : 'Обновить'),
            ['class' => 'btn btn-primary']);
        ?>
        <?=CHtml::link(
            Yii::t('DirectoryModule.messages', 'Отмена'),
            $this->createUrl('/directory/backend/city/index'),
            ['class' => 'btn']);
        ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->