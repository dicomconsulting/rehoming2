<?php
/* @var $this MedicalCenterController */
/* @var $model MedicalCenter */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $('#city-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");
?>

<?php $this->widget('GridView', array(
    'id' => 'city-grid',
    'dataProvider' => $model->search(),
    'columns' => array(
        [
            "header" => Yii::t("DirectoryModule.messages", "№"),
            "value" => '$data->uid',
        ],
        [
            "header" => Yii::t("DirectoryModule.messages", "Название"),
            "value" => '$data->name',
        ],
        [
            "header" => Yii::t("DirectoryModule.messages", "Страна"),
            "value" => '$data->country->name',
        ],
        [
            "header" => Yii::t("DirectoryModule.messages", "Акт."),
            'type' => 'raw',
            'htmlOptions' => ['style' => 'text-align: center;'],
            "value" => '!$data->is_delete ? "Да" : "Нет"',
        ],
        array(
            'class'=>'CButtonColumn',
            'template' => '{view} {update} {delete} {restore}',
            'header' => CHtml::dropDownList(
                'pageSize',
                Yii::app()->user->getState('pageSize'),
                [10 => 10, 20 => 20, 50 => 50],
                [
                    'onchange' => "$.fn.yiiGridView.update('city-grid',{ data:{pageSize: $(this).val() }})",
                    'style' => 'width: 70px;'
                ]
            ),
            'buttons' => [
                'update' => [
                    'visible' => '$data->isActive();'
                ],
                'delete' => [
                    'visible' => '$data->isActive();'
                ],
                'restore' => [
                    'label' => Yii::t("AdminModule.messages", "Восстановить"),
                    'imageUrl' => '/images/restore.png',
                    'url' => 'Yii::app()->controller->createUrl("/directory/backend/city/restore", ["id" => $data->uid])',
                    'visible' => '!$data->isActive();'
                ]
            ]
        ),
    ),
)); ?>

<?=CHtml::link(
    CHtml::tag('i', ['class' => 'icon-plus-sign'], ' ') . Yii::t("DirectoryModule.messages", "Создать"),
    $this->createUrl('/directory/backend/city/create'),
    ['class' => 'btn btn-primary', 'style' => 'float:right;']
);?>
