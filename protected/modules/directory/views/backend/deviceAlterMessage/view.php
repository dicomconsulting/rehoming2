<?php
/* @var $this MedicalCenterController */
/* @var $model MedicalCenter */
?>

    <h2><?=Yii::t('DirectoryModule.messages', 'Cообщение')?> №<?php echo $model->uid;?></h2>
    <?=CHtml::link(
        CHtml::tag('i', ['class' => 'icon-list'], ' ') . Yii::t('DirectoryModule.messages', 'Вернуться к списку'),
        $this->createUrl('/directory/backend/deviceAlterMessage/index'),
        ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 10px;']
    )?>
    <?=CHtml::link(
        CHtml::tag('i', ['class' => 'icon-edit'], ' ') . Yii::t('DirectoryModule.messages', 'Редактировать'),
        $this->createUrl('/directory/backend/deviceAlterMessage/update/', ['id' => $model->uid]),
        ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 10px;']
    )?>
    <?=CHtml::link(
        CHtml::tag('i', ['class' => 'icon-trash'], ' ') . Yii::t('DirectoryModule.messages', 'Удалить'),
        $this->createUrl('/directory/backend/deviceAlterMessage/delete/', ['id' => $model->uid]),
        ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 10px;']
    )?>
    <?php $this->widget('zii.widgets.CDetailView', array(
        'data' => $model,
        'attributes'=>array(
            [
                'name' => Yii::t('DirectoryModule.messages', 'Сообщение'),
                'value' => $model->message,
            ]
        ),
    )); ?>


