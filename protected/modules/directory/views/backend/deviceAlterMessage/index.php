<?php
/* @var $this MedicalCenterController */
/* @var $model MedicalCenter */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $('#city-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");
?>

<?php $this->widget('GridView', array(
    'id'           => 'deviceAlterMessage-grid',
    'dataProvider' => $model->search(),
    'columns'      => array(
        [
            'header' => Yii::t('DirectoryModule.messages', '№'),
            'value'  => '$data->uid',
        ],
        [
            'header' => Yii::t('DirectoryModule.messages', 'Сообщение'),
            'value'  => '$data->message',
        ],
        [
            'class'=>'CButtonColumn',
            'template' => '{view} {update} {delete}',
            'buttons' => [
                'update',
                'delete'
            ]
        ],
    ),
)); ?>

<?= CHtml::link(
    CHtml::tag('i', ['class' => 'icon-plus-sign'], ' ') . Yii::t('DirectoryModule.messages', 'Создать'),
    $this->createUrl('/directory/backend/deviceAlterMessage/create'),
    ['class' => 'btn btn-primary', 'style' => 'float:right;']
); ?>
