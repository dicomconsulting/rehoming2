<?php
/* @var $this MedicalCenterController */
/* @var $model MedicalCenter */
?>

    <h2><?=Yii::t("DirectoryModule.messages", "Медицинский центр")?> №<?php echo $model->uid;?></h2>
    <?=CHtml::link(
        CHtml::tag('i', ['class' => 'icon-list'], ' ') . Yii::t("DirectoryModule.messages", "Вернуться к списку"),
        $this->createUrl('/directory/backend/medicalCenter/index'),
        ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 10px;']
    )?>
    <?=CHtml::link(
        CHtml::tag('i', ['class' => 'icon-edit'], ' ') . Yii::t("DirectoryModule.messages", "Редактировать"),
        $this->createUrl('/directory/backend/medicalCenter/update/', ['id' => $model->uid]),
        ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 10px;']
    )?>
    <?=CHtml::link(
        CHtml::tag('i', ['class' => 'icon-trash'], ' ') . Yii::t("DirectoryModule.messages", "Удалить"),
        $this->createUrl('/directory/backend/medicalCenter/delete/', ['id' => $model->uid]),
        ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 10px;']
    )?>
    <?php $this->widget('zii.widgets.CDetailView', array(
        'data' => $model,
        'attributes'=>array(
            [
                "name" => Yii::t("DirectoryModule.messages", "Название"),
                "value" => $model->name,
            ],
            [
                "name" => Yii::t("DirectoryModule.messages", "ИНН"),
                "value" => $model->inn,
            ],
            [
                "name" => Yii::t("DirectoryModule.messages", "Город"),
                "value" => $model->city->name,
            ],
            [
                "name" => Yii::t("DirectoryModule.messages", "Код"),
                "value" => $model->code,
            ]
        ),
    )); ?>


