<?php
/* @var $this MedicalCenterController */
/* @var $model MedicalCenter */
/* @var $form CActiveForm */
?>
<h2 class="offset1">
    <?=Yii::t("DirectoryModule.messages", "Модель устройства")?>
    <?=$model->getIsNewRecord() ? '' : '№' . $model->name;?>
</h2>
<div class="form">

    <?php $form = $this->beginWidget('I18nActiveForm', array(
        'id' => 'device-model-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => [
            'class' => 'form-horizontal'
        ]
    )); ?>

    <p class="note controls">
        <?=Yii::t("DirectoryModule.messages", "Поля отмеченные");?>
        <span class="required">"*"</span>
        <?=Yii::t("DirectoryModule.messages", "обязательны для заполнения");?>.
    </p>

    <div class="controls">
        <?=$form->errorSummary($model); ?>
    </div>

    <div class="control-group">
        <?=$form->labelEx($model, 'name', ['class' => 'control-label']); ?>
        <div class="controls">
            <?=$form->textField($model, 'name'); ?>
            <?=$form->error($model, 'name');?>
        </div>
    </div>

    <div class="control-group">
        <?=$form->labelEx($model, 'device_type_uid', ['class' => 'control-label']); ?>
        <?php DeviceType::enableOnlyActive(); ?>
        <div class="controls">
            <?=$form->dropDownList(
                $model,
                'device_type_uid',
                CHtml::listData(
                    DeviceType::model()->findAll(["order" => "name"]),
                    "uid" ,
                    function ($type) {
                        return $type->name;
                    }
                ),
                [
                    'empty' => Yii::t("DirectoryModule.messages", 'Выберите тип устройства')
                ]
            ); ?>
            <?=$form->error($model, 'device_type_uid');?>
        </div>
    </div>

    <div class="row buttons controls">
        <?=CHtml::submitButton(
            Yii::t('DirectoryModule.messages', $model->getIsNewRecord() ? 'Создать' : 'Обновить'),
            ['class' => 'btn btn-primary']);
        ?>
        <?=CHtml::link(
            Yii::t('DirectoryModule.messages', 'Отмена'),
            $this->createUrl($this->id . '/index'),
            ['class' => 'btn']);
        ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->