<?php
/* @var $this MedicalCenterController */
/* @var $model MedicalCenter */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $('#device-model-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");
?>

<?php $this->widget('GridView', array(
    'id'=>'device-model-grid',
    'dataProvider' => $model->search(),
    'columns' => array(
        [
            "header" => Yii::t("DirectoryModule.messages", "Название"),
            "value" => '$data->name',
        ],
        [
            "header" => Yii::t("DirectoryModule.messages", "Акт."),
            'type' => 'raw',
            'htmlOptions' => ['style' => 'text-align: center;'],
            "value" => '!$data->is_delete ? "Да" : "Нет"',
        ],
        array(
            'class'=>'CButtonColumn',
            'template' => '{view} {update} {delete} {restore}',
            'header' => CHtml::dropDownList(
                'pageSize',
                Yii::app()->user->getState('pageSize'),
                [10 => 10, 20 => 20, 50 => 50],
                [
                    'onchange' => "$.fn.yiiGridView.update('device-model-grid',{ data:{pageSize: $(this).val() }})",
                    'style' => 'width: 70px;'
                ]
            ),
            'buttons' => [
                'update' => [
                    'visible' => '$data->isActive();'
                ],
                'delete' => [
                    'visible' => '$data->isActive();'
                ],
                'restore' => [
                    'label' => Yii::t("AdminModule.messages", "Восстановить"),
                    'imageUrl' => '/images/restore.png',
                    'url' => 'Yii::app()->controller->createUrl("/directory/backend/deviceModel/restore", ["id" => $data->name])',
                    'visible' => '!$data->isActive();'
                ]
            ]
        ),
    ),
)); ?>

<?=CHtml::link(
    CHtml::tag('i', ['class' => 'icon-plus-sign'], ' ') . Yii::t("DirectoryModule.messages", "Создать"),
    $this->createUrl($this->id . '/create'),
    ['class' => 'btn btn-primary', 'style' => 'float:right;']
);