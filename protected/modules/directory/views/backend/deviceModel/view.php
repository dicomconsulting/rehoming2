<?php
/* @var $this MedicalCenterController */
/* @var $model MedicalCenter */
?>

    <h2><?=Yii::t("DirectoryModule.messages", "Модель устройства")?> №<?php echo $model->name;?></h2>
    <?=CHtml::link(
        CHtml::tag('i', ['class' => 'icon-list'], ' ') . Yii::t("DirectoryModule.messages", "Вернуться к списку"),
        $this->createUrl($this->id . '/index'),
        ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 10px;']
    )?>
    <?=CHtml::link(
        CHtml::tag('i', ['class' => 'icon-edit'], ' ') . Yii::t("DirectoryModule.messages", "Редактировать"),
        $this->createUrl($this->id . '/update', ['id' => $model->name]),
        ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 10px;']
    )?>
    <?=CHtml::link(
        CHtml::tag('i', ['class' => 'icon-trash'], ' ') . Yii::t("DirectoryModule.messages", "Удалить"),
        $this->createUrl($this->id . '/delete', ['id' => $model->name]),
        ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 10px;']
    )?>
    <?php $this->widget('zii.widgets.CDetailView', array(
        'data' => $model,
        'attributes'=>array(
            [
                "name" => Yii::t("DirectoryModule.messages", "Название"),
                "value" => $model->name,
            ],
            [
                "name" => Yii::t("DirectoryModule.messages", "Тип устройства"),
                "value" => $model->type->name,
            ],
        ),
    ));
