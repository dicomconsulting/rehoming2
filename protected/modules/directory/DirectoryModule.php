<?php

class DirectoryModule extends CWebModule
{
    /**
     * Initializes the module.
     */
    public function init()
    {
        Yii::app()->getModule("i18n");

        $this->setImport(
            array(
                'directory.models.*',
                'device.models.*',
            )
        );
    }

    public function beforeControllerAction($controller, $action)
    {
        /** @var CWebApplication $app */
        $app = Yii::app();
        /** @var RhWebUser $user */
        $user = $app->getUser();
        if (!$user->checkAccess('dictManagement')) {
            if ($user->isGuest) {
                $user->loginRequired();
            } else {
                throw new CHttpException(403, 'Доступ запрещен!');
            }
        }

        return parent::beforeControllerAction($controller, $action);
    }
}
