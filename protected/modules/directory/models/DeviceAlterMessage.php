<?php

/**
 * This is the model class for table "{{device_alter_message}}".
 *
 * The followings are the available columns in table '{{device_polarity}}':
 * @property integer $uid
 * @property string $message
 *
 */
class DeviceAlterMessage extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return DevicePolarity the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{device_alter_messages}}';
    }


    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['message', 'required'],
            ['message', 'length', 'max' => 255]
        ];
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        return new CActiveDataProvider($this, ['criteria' => $criteria]);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid'      => Yii::t("DirectoryModule.messages", 'ID'),
            'message' => Yii::t("DirectoryModule.messages", 'Сообщение'),
        );
    }
}
