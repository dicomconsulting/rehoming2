<?php
Yii::import('i18n.models.I18nActiveRecord');
Yii::import('directory.models.*');

/**
 * This is the model class for table "{{pharm_product}}".
 *
 * The followings are the available columns in table '{{pharm_product}}':
 * @property integer $uid
 * @property string $name_latin
 *
 * The followings are the available model relations:
 * @property PharmProductClinicGroupRelation[] $pharmProductClinicGroupRelations
 */
class PharmProduct extends I18nActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{pharm_product}}';
    }

    public function primaryKey()
    {
        return "uid";
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'groupRelation' => array(self::HAS_MANY, 'PharmProductClinicGroupRelation', 'pharm_product_uid'),
            'group' => array(
                self::MANY_MANY,
                'PharmClinicGroup',
                '{{pharm_product_clinic_group_relation}}(pharm_product_uid, pharm_group_uid)'
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid' => 'Uid',
            'name_latin' => 'Латинское назввание',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('uid', $this->uid);
        $criteria->compare('name_latin', $this->name_latin, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Возвращает список идентификаторов мед. препапатов по группе
     *
     * @param PharmClinicGroup $group
     * @return array
     */
    public function getUniqueProductsByGroup(PharmClinicGroup $group)
    {

        $criteria = new CDbCriteria(array("group" => "name_latin"));

        $products = PharmProduct::model()->withI18n("ru")
            ->with(
                array(
                    "group" => array(
                        "condition" => "group.uid = " . $group->uid,
                        "order" => "pharm_product_uid DESC"
                    )
                )
            )
            ->findAll($criteria);

        return $products;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PharmProduct the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
