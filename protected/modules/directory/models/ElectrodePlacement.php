<?php
Yii::import('i18n.models.I18nActiveRecord');
Yii::import('admin.components.DeactivationDataTrait');

/**
 * This is the model class for table "{{electrode_placement}}".
 *
 * The followings are the available columns in table '{{electrode_placement}}':
 * @property integer $uid
 *
 * The followings are the available model relations:
 * @property ElectrodePlacementI18n[] $electrodePlacementI18ns
 */
class ElectrodePlacement extends I18nActiveRecord
{
    use DeactivationDataTrait;

    public function rules()
    {
        return array(
            array('name, uid', 'required'),
            array('uid', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 100),
        );
    }

    public function primaryKey()
    {
        return "uid";
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ElectrodePlacement the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{electrode_placement}}';
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid' => Yii::t("DirectoryModule.messages", 'Номер'),
            'name' => Yii::t("DirectoryModule.messages", 'Название'),
        );
    }

}
