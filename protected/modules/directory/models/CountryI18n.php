<?php

class CountryI18n extends CActiveRecord
{
    /**
    * @return string the associated database table name
    */
    public function tableName()
    {
        return '{{country_i18n}}';
    }
    /**
    *@return array primary key column
    */
    public function primaryKey()
    {
        return array('uid','locale');
    }
}
