<?php
Yii::import('i18n.models.I18nActiveRecord');
Yii::import('directory.models.*');

/**
 * This is the model class for table "{{pharm_clinic_group}}".
 *
 * The followings are the available columns in table '{{pharm_clinic_group}}':
 * @property integer $uid
 * @property string $code
 *
 * The followings are the available model relations:
 * @property PharmProductClinicGroupRelation[] $pharmProductClinicGroupRelations
 */
class PharmClinicGroup extends I18nActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PharmClinicGroup the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{pharm_clinic_group}}';
    }

    public function primaryKey()
    {
        return "uid";
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'pharmProductClinicGroupRelations' => array(
                self::HAS_MANY,
                'PharmProductClinicGroupRelation',
                'pharm_group_uid'
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid' => 'Uid',
            'code' => 'Code',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('uid', $this->uid);
        $criteria->compare('code', $this->code, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}
