<?php

/**
 * This is the model class for table "{{adverse_result_i18n}}".
 *
 * The followings are the available columns in table '{{adverse_result_i18n}}':
 * @property string $name
 * @property integer $uid
 * @property string $locale
 *
 * The followings are the available model relations:
 * @property AdverseResult $u
 */
class AdverseResultI18n extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AdverseResultI18n the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{adverse_result_i18n}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('uid', 'required'),
            array('uid', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 50),
            array('locale', 'length', 'max' => 2),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'u' => array(self::BELONGS_TO, 'AdverseResult', 'uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'name' => 'Name',
            'uid' => 'Uid',
            'locale' => 'Locale',
        );
    }
}
