<?php
Yii::import('i18n.models.I18nActiveRecord');
Yii::import('admin.components.DeactivationDataTrait');

/**
 * This is the model class for table "{{country}}".
 *
 * The followings are the available columns in table '{{country}}':
 * @property integer $uid
 * @property string $alpha2_code
 * @property string $alpha3_code
 * @property string $iso_code
 * @property integer $region_id
 */
class Country extends I18nActiveRecord
{
    use DeactivationDataTrait;

    public function attributeLabels()
    {
        return array(
            'uid' => Yii::t("DirectoryModule.messages", 'Номер'),
            'name' => Yii::t("DirectoryModule.messages", 'Название'),
            'region_id' => Yii::t("DirectoryModule.messages", 'Регион'),
        );
    }

    public function rules()
    {
        return array(
            array('name region_id', 'required'),
            array('region_id', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max' => 100),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'region' => array(self::BELONGS_TO, 'Region', 'region_id')
        );
    }

    public function primaryKey()
    {
        return 'uid';
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{country}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param  string $className active record class name.
     * @return Country  the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}

