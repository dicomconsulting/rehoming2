<?php

/**
 * This is the model class for table "{{pharm_group_tmp}}".
 *
 * The followings are the available columns in table '{{pharm_group_tmp}}':
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property string $code
 */
class PharmGroupTmp extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{pharm_group_tmp}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id', 'required'),
			array('id, parent_id', 'numerical', 'integerOnly'=>true),
			array('name, code', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, parent_id, name, code', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'parent_id' => 'Parent',
			'name' => 'Name',
			'code' => 'Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('code',$this->code,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PharmGroupTmp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function getProductsRecursivelyByCode($code)
    {
        $products = array();

        /** @var self $group */
        $group = self::model()->findByAttributes(array("code" => $code));

        if ($group) {
            $products = PharmProductTmp::model()->findAllByAttributes(array("group_id" => $group->id));

            /** @var self[] $childGroups */
            $childGroups = self::model()->findAllByAttributes(array("parent_id" => $group->id));

            if($childGroups) {
                foreach($childGroups as $child){
                    $products = array_merge($products, self::getProductsRecursivelyByCode($child->code));
                }
            }
        }

        return $products;
    }


    public static function getDataForTree($parentId)
    {
        if (!$parentId){
            $parentId = 'NULL';
        }

        $pharmGroups = Yii::app()->db->createCommand(
            "SELECT m1.id as id, CONCAT(m1.code, ' ',  m1.name) AS text, m2.id IS NOT NULL AS hasChildren, 1 as `group` "
            . " FROM {{pharm_group_tmp}} AS m1 LEFT JOIN {{pharm_group_tmp}} AS m2 ON m1.id=m2.parent_id "
            . " WHERE m1.parent_id <=> $parentId "
            . " GROUP BY m1.id"
            . " ORDER BY m1.`code`"
        )->queryAll();

        $pharmProducts = Yii::app()->db->createCommand(
            "SELECT m1.id as id, m1.name AS text, 0 AS hasChildren, 0 as `group`  "
            . " FROM {{pharm_product_tmp}} AS m1"
            . " WHERE m1.group_id = $parentId "
            . " ORDER BY m1.`name`"
        )->queryAll();


        return $pharmGroups + $pharmProducts;
    }
}
