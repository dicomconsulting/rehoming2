<?php
Yii::import('i18n.models.I18nActiveRecord');
Yii::import('admin.components.DeactivationDataTrait');

/**
 * This is the model class for table "{{measurement_unit}}".
 *
 * The followings are the available columns in table '{{measurement_unit}}':
 * @property integer $uid
 * @property string $name
 * @property string $full_name
 */
class MeasurementUnit extends I18nActiveRecord
{
    use DeactivationDataTrait;

    public function rules()
    {
        return array(
            array('name', 'required'),
            array('name', 'length', 'max' => 50),
            array('full_name', 'length', 'max' => 100),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid' => Yii::t("DirectoryModule.messages", 'Номер'),
            'name' => Yii::t("DirectoryModule.messages", 'Название'),
            'full_name' => Yii::t("DirectoryModule.messages", 'Полное название'),
        );
    }

    public function primaryKey()
    {
        return 'uid';
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{measurement_unit}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param  string $className active record class name.
     * @return MeasurementUnit the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'pharm' => array(self::HAS_ONE, 'PharmMeasure', 'measurement_unit_uid'),
        );
    }

    /**
     * Возвращает массив единиц измерений, используемых в мед. препаратах
     *
     * @return mixed
     */
    public function getPharmUnits ()
    {
        MeasurementUnit::enableOnlyActive();
        $units = MeasurementUnit::model()
            ->withI18n(Yii::app()->language)
            ->with(array(
                "pharm" => array(
                    "joinType" => "JOIN",
                    'order' => 'pharm.`order` DESC'
                )
            ))
            ->findAll();

        return $units;
    }
}
