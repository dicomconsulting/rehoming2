<?php
Yii::import('i18n.models.I18nActiveRecord');
Yii::import('admin.components.DeactivationDataTrait');

/**
 * This is the model class for table "{{device_polarity}}".
 *
 * The followings are the available columns in table '{{device_polarity}}':
 * @property integer $uid
 *
 * The followings are the available model relations:
 * @property DevicePolarityI18n[] $devicePolarityI18ns
 */
class DevicePolarity extends I18nActiveRecord
{
    use DeactivationDataTrait;

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return DevicePolarity the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function primaryKey()
    {
        return "uid";
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{device_polarity}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('uid name', 'required'),
            array('uid', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 50),
            array('uid', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid' => Yii::t("DirectoryModule.messages", 'Номер'),
            'name' => Yii::t("DirectoryModule.messages", 'Название'),
        );
    }
}
