<?php
Yii::import('i18n.models.I18nActiveRecord');
Yii::import('admin.components.DeactivationDataTrait');

/**
 * This is the model class for table "{{city}}".
 *
 * The followings are the available columns in table '{{city}}':
 * @property integer $uid
 * @property integer $country_id
 *
 * The followings are the available model relations:
 * @property Country $country
 * @property CityI18n[] $cityI18ns
 * @property MedicalCenter[] $medicalCenters
 */
class City extends I18nActiveRecord
{
    use DeactivationDataTrait;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{city}}';
    }

    public function primaryKey()
    {
        return "uid";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('country_id name', 'required'),
            array('country_id', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max' => 100),
            array('uid, country_id', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'country' => array(self::BELONGS_TO, 'Country', 'country_id'),
            'medicalCenters' => array(self::HAS_MANY, 'MedicalCenter', 'city_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid' => Yii::t("DirectoryModule.messages", 'Номер'),
            'name' => Yii::t("DirectoryModule.messages", 'Название'),
            'country_id' => Yii::t("DirectoryModule.messages", 'Страна'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return City the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
