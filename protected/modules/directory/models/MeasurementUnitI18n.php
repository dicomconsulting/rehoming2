<?php

class MeasurementUnitI18n extends CActiveRecord
{
    /**
    * @return string the associated database table name
    */
    public function tableName()
    {
        return '{{measurement_unit_i18n}}';
    }
    /**
    *@return array primary key column
    */
    public function primaryKey()
    {
        return array('uid','locale');
    }
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

}
