<?php
Yii::import('i18n.models.I18nActiveRecord');
Yii::import('admin.components.DeactivationDataTrait');

/**
 * This is the model class for table "{{arr_cupping}}".
 *
 * The followings are the available columns in table '{{arr_cupping}}':
 * @property integer $uid
 * @property string $alias
 *
 * The followings are the available model relations:
 * @property ArrCuppingI18n[] $arrCuppingI18ns
 */
class ArrCupping extends I18nActiveRecord
{
    use DeactivationDataTrait;

    public function primaryKey()
    {
        return "uid";
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ArrCupping the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{arr_cupping}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('alias', 'length', 'max' => 50),
            array('name', 'length', 'max' => 50),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid' => Yii::t("DirectoryModule.messages", 'Номер'),
            'alias' => Yii::t("DirectoryModule.messages", 'Псевдоним'),
            'name' => Yii::t("DirectoryModule.messages", 'Название'),
        );
    }
}
