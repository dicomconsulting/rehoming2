<?php

/**
 * This is the model class for table "{{surgery_i18n}}".
 *
 * The followings are the available columns in table '{{surgery_i18n}}':
 * @property string $name
 * @property integer $uid
 * @property string $locale
 *
 * The followings are the available model relations:
 * @property Surgery $u
 */
class SurgeryI18n extends CActiveRecord
{

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SurgeryI18n the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{surgery_i18n}}';
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(//			'u' => array(self::BELONGS_TO, 'Surgery', 'uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'name' => 'Name',
            'uid' => 'Uid',
            'locale' => 'Locale',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('name', $this->name, true);
        $criteria->compare('uid', $this->uid);
        $criteria->compare('locale', $this->locale, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}
