<?php
Yii::import('i18n.models.I18nActiveRecord');
Yii::import('admin.components.DeactivationDataTrait');

/**
 * This is the model class for table "{{medical_center}}".
 *
 * The followings are the available columns in table '{{medical_center}}':
 * @property integer $uid
 * @property string $inn
 * @property string $code
 * @property integer $city_id
 *
 * The followings are the available model relations:
 * @property City $city
 * @property Research[] $researches
 * @property User[] $users
 */
class MedicalCenter extends I18nActiveRecord
{
    use DeactivationDataTrait;

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('city_id', 'required'),
            array('name, code', 'required'),
            array('name', 'length', 'max'=>100),
            array('city_id', 'numerical', 'integerOnly' => true),
            array('inn', 'length', 'max' => 12),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('uid, inn, city_id, code', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'city' => array(self::BELONGS_TO, 'City', 'city_id'),
            'researches' => array(self::HAS_MANY, 'Research', 'medical_center_uid'),
            'users' => array(self::HAS_MANY, 'User', 'medical_center_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid' => Yii::t("DirectoryModule.messages", 'Номер'),
            'name' => Yii::t("DirectoryModule.messages", 'Название'),
            'inn' => Yii::t("DirectoryModule.messages", 'ИНН'),
            'city_id' => Yii::t("DirectoryModule.messages", 'Город'),
            'code' => Yii::t("DirectoryModule.messages", 'Код'),
        );
    }

    public function getDisplayName()
    {
        return $this->code . " " . $this->name;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return MedicalCenter the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function primaryKey()
    {
        return "uid";
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{medical_center}}';
    }
}
