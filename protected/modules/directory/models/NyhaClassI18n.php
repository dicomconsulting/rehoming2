<?php

/**
 * This is the model class for table "{{nyha_class_i18n}}".
 *
 * The followings are the available columns in table '{{nyha_class_i18n}}':
 * @property string $name
 * @property integer $uid
 * @property string $locale
 *
 * The followings are the available model relations:
 * @property NyhaClass $u
 */
class NyhaClassI18n extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return NyhaClassI18n the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{nyha_class_i18n}}';
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'u' => array(self::BELONGS_TO, 'NyhaClass', 'uid'),
        );
    }
}
