<?php
Yii::import('i18n.models.I18nActiveRecord');

/**
 * This is the model class for table "{{nyha_class}}".
 *
 * The followings are the available columns in table '{{nyha_class}}':
 * @property integer $uid
 *
 * The followings are the available model relations:
 * @property NyhaClassI18n[] $nyhaClassI18ns
 */
class NyhaClass extends I18nActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return NyhaClass the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function primaryKey()
    {
        return "uid";
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{nyha_class}}';
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'nyhaClassI18ns' => array(self::HAS_MANY, 'NyhaClassI18n', 'uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid' => 'Uid',
        );
    }
}
