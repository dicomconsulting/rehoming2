<?php

/**
 * This is the model class for table "{{pharm_product_clinic_group_relation}}".
 *
 * The followings are the available columns in table '{{pharm_product_clinic_group_relation}}':
 * @property integer $pharm_product_uid
 * @property integer $pharm_group_uid
 *
 * The followings are the available model relations:
 * @property PharmClinicGroup $pharmGroupU
 * @property PharmProduct $pharmProductU
 */
class PharmProductClinicGroupRelation extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{pharm_product_clinic_group_relation}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pharm_product_uid, pharm_group_uid', 'required'),
			array('pharm_product_uid, pharm_group_uid', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('pharm_product_uid, pharm_group_uid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
//			'pharmGroupU' => array(self::BELONGS_TO, 'PharmClinicGroup', 'pharm_group_uid'),
//			'pharmProductU' => array(self::BELONGS_TO, 'PharmProduct', 'pharm_product_uid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pharm_product_uid' => 'Ссылка на препарат',
			'pharm_group_uid' => 'Ссылка на группу препаратов',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pharm_product_uid',$this->pharm_product_uid);
		$criteria->compare('pharm_group_uid',$this->pharm_group_uid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PharmProductClinicGroupRelation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
