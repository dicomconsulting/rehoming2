<?php

/**
 * This is the model class for table "{{icd10}}".
 *
 * The followings are the available columns in table '{{icd10}}':
 * @property integer $uid
 * @property string $code
 * @property string $name
 * @property integer $parent_id
 * @property string $is_injury
 * @property string $record_id
 * @property integer $order
 * @property string $type
 * @property string $from
 * @property string $to
 */
class Icd10 extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Icd10 the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function getDataForTree($parentId)
    {
        if (!$parentId) {
            $parentId = 0;
        }

        return Yii::app()->db->createCommand(
            "SELECT m1.uid as id, CONCAT(m1.code, ' ',  m1.name) AS text, m2.uid IS NOT NULL AS hasChildren "
            . "FROM {{icd10}} AS m1 LEFT JOIN {{icd10}} AS m2 ON m1.uid=m2.parent_id "
            . "WHERE m1.parent_id <=> $parentId "
            . "GROUP BY m1.uid ORDER BY m1.`type`, m1.`from`"
        )->queryAll();
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{icd10}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('order, type, from, to', 'required'),
            array('parent_id, order', 'numerical', 'integerOnly' => true),
            array('code', 'length', 'max' => 7),
            array('name', 'length', 'max' => 255),
            array('is_injury, from, to', 'length', 'max' => 10),
            array('record_id', 'length', 'max' => 11),
            array('type', 'length', 'max' => 1),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('uid, code, name, parent_id, is_injury, record_id, order, type, from, to', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'etiology' => array(
                self::MANY_MANY,
                'ProtocolEnrolment',
                '{{protocol_enrolment_etiology}}(disease_uid,protocol_uid)'
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid' => 'Uid',
            'code' => 'Код МКБ-10',
            'name' => 'Название',
            'parent_id' => 'Ссылка на родительский элемент',
            'is_injury' => 'не исп-ся',
            'record_id' => 'не исп-ся',
            'order' => 'Порядок отображения',
            'type' => 'не исп-ся',
            'from' => 'не исп-ся',
            'to' => 'не исп-ся',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('uid', $this->uid);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('parent_id', $this->parent_id);
        $criteria->compare('is_injury', $this->is_injury, true);
        $criteria->compare('record_id', $this->record_id, true);
        $criteria->compare('order', $this->order);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('from', $this->from, true);
        $criteria->compare('to', $this->to, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}
