<?php
Yii::import('i18n.models.I18nActiveRecord');

/**
 * This is the model class for table "{{tachy_type}}".
 *
 * The followings are the available columns in table '{{tachy_type}}':
 * @property integer $uid
 *
 * The followings are the available model relations:
 * @property TachyTypeI18n[] $tachyTypeI18ns
 */
class TachyType extends I18nActiveRecord
{
    public function primaryKey()
    {
        return "uid";
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TachyType the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{tachy_type}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'tachyTypeI18ns' => array(self::HAS_MANY, 'TachyTypeI18n', 'uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid' => 'Uid',
        );
    }
}
