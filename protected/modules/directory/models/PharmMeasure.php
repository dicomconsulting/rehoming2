<?php

/**
 * This is the model class for table "{{pharm_measure}}".
 *
 * The followings are the available columns in table '{{pharm_measure}}':
 * @property integer $measurement_unit_uid
 *
 * The followings are the available model relations:
 * @property MeasurementUnit $measurementUnitU
 */
class PharmMeasure extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PharmMeasure the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function makeFlatArray($measurementUnits)
    {

        $res = array();

        foreach ($measurementUnits as $unit) {
            $res[$unit->uid . " "] = $unit->name;
        }

        return $res;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{pharm_measure}}';
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'measurementUnitU' => array(self::BELONGS_TO, 'MeasurementUnit', 'measurement_unit_uid'),
        );
    }

    public function primaryKey()
    {
        return "measurement_unit_uid";
    }


}
