<?php
Yii::import('i18n.models.I18nActiveRecord');
Yii::import('admin.components.DeactivationDataTrait');

/**
 * This is the model class for table "{{attack_consciousness}}".
 *
 * The followings are the available columns in table '{{attack_consciousness}}':
 * @property integer $uid
 *
 * The followings are the available model relations:
 * @property AttackConsciousnessI18n[] $attackConsciousnessI18ns
 */
class AttackConsciousness extends I18nActiveRecord
{
    use DeactivationDataTrait;

    public function primaryKey()
    {
        return "uid";
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AttackConsciousness the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{attack_consciousness}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('uid', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 50),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid' => Yii::t("DirectoryModule.messages", 'Номер'),
            'name' => Yii::t("DirectoryModule.messages", 'Название'),
        );
    }
}
