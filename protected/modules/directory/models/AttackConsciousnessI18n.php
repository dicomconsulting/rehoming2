<?php

/**
 * This is the model class for table "{{attack_consciousness_i18n}}".
 *
 * The followings are the available columns in table '{{attack_consciousness_i18n}}':
 * @property string $name
 * @property integer $uid
 * @property string $locale
 *
 * The followings are the available model relations:
 * @property AttackConsciousness $u
 */
class AttackConsciousnessI18n extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AttackConsciousnessI18n the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{attack_consciousness_i18n}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('uid', 'required'),
            array('uid', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 50),
            array('locale', 'length', 'max' => 2),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('name, uid, locale', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'u' => array(self::BELONGS_TO, 'AttackConsciousness', 'uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'name' => 'Name',
            'uid' => 'Uid',
            'locale' => 'Locale',
        );
    }
}
