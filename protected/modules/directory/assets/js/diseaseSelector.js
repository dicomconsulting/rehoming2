/**
 * Created by Дамир on 10.01.14.
 */


var DiseaseSelector = (function () {
    function DiseaseSelector() {
        var _self = this;
        _self.selector = $("#etiologySelector");
        _self.lookupInput = $("#desease_lookup");
        _self.lookupInputHiddenId = $("#desease_lookup_selected");
        _self.selectedCallback = function () {};
    }


    /**
     * Показывает контрол выбора заболевания из справочника МКБ10
     * @param callback
     * @returns {boolean}
     */

    DiseaseSelector.prototype.show = function (callback) {
        var _self = this;

        _self.selectedCallback = callback;
        _self.lookupInput.val("");
        _self.lookupInputHiddenId.val("");

        var radioInput = $("input[name=etiologySelector]:checked", _self.selector);
        radioInput.removeAttr('checked');

        _self.selector.dialog("open");

        return false;

    };

    /**
     * Обработка выбора заболевания
     * @returns {boolean}
     */
    DiseaseSelector.prototype.processSelected = function () {
        var _self = this;

        var radioInput = $("input[name=etiologySelector]:checked", _self.selector);
        radioInput.removeAttr('checked');

        if (!radioInput.length  && (!_self.lookupInputHiddenId.val() || !_self.lookupInput.val())){
            alert("Необходимо выбрать заболевание");
            return false;
        }

        var id, name;

        if (_self.lookupInput.val() && _self.lookupInputHiddenId.val()) {
            id =  _self.lookupInputHiddenId.val();
            name = _self.lookupInput.val();
        } else {
            id =  radioInput.val();
            name = $("label[for=" + radioInput.attr("id") + "]").text();
        }

        _self.selectedCallback(id, name);

        return true;
    };

    var instance;

    return {
        getInstance: function(){
            if (instance == null) {
                instance = new DiseaseSelector();
                instance.constructor = null;
            }
            return instance;
        }
    };
})();
