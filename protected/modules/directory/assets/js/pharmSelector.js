/**
 * Created by Дамир on 10.01.14.
 */


var PharmSelector = (function () {
    function PharmSelector() {
        var _self = this;
        _self.selector = $("#pharmSelector");
        _self.selectedCallback = function () {};
    }


    /**
     * Показывает контрол выбора лекарственных препаратов
     * @param callback
     * @returns {boolean}
     */

    PharmSelector.prototype.show = function (callback) {
        var _self = this;

        _self.selectedCallback = callback;

        var radioInput = $("input[name=pharmSelector]:checked", _self.selector);
        radioInput.removeAttr('checked');

        _self.selector.dialog("open");
        return false;
    };

    /**
     * Обработка выбора препарата
     * @returns {boolean}
     */
    PharmSelector.prototype.processSelected = function () {

        var _self = this;

        var radioInput = $("input[name=pharmSelector]:checked", _self.selector);
        radioInput.removeAttr('checked');

        if (!radioInput.length){
            alert("Необходимо выбрать лекарственный препарат!");
            return false;
        }

        var id, name;

        var selectedPharmId = radioInput.val();
        var pharmName = $("label[for="+radioInput.attr("id")+"]").text();

        _self.selectedCallback(selectedPharmId, pharmName);

        return true;
    };

    var instance;

    return {
        getInstance: function(){
            if (instance == null) {
                instance = new PharmSelector();
                instance.constructor = null;
            }
            return instance;
        }
    };
})();
