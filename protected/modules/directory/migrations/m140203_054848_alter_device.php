<?php

class m140203_054848_alter_device extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn(
            "{{device_model}}",
            'device_type_uid',
            'INT(11) DEFAULT NULL COMMENT "Ссылка на тип устройства"'
        );
    }

    public function safeDown()
    {
        $this->dropColumn(
            "{{device_model}}",
            'device_type_uid'
        );
    }
}
