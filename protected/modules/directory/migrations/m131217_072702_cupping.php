<?php

class m131217_072702_cupping extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTableWithI18n(
            "{{arr_cupping}}",
            array(
                "uid" => "pk",
                "alias" => "VARCHAR(50)",
                "name" => "VARCHAR(50)",
            ),
            "ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='Словарь купирования аритмии'",
            ['name']
        );
    }

    public function safeDown()
    {
        $this->dropTableWithI18n("{{arr_cupping}}");
    }
}
