<?php

class m131002_073317_create_city_table extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTableWithI18n(
            '{{city}}',
            array(
                'uid' => 'pk',
                'country_id' => 'INTEGER(11) NOT NULL',
                'name' => 'VARCHAR(100)'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci',
            array('name')
        );
        $this->addForeignKey(
            'fk_city_country_id_country_uid',
            '{{city}}',
            'country_id',
            '{{country}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_city_country_id_country_uid', '{{city}}');
        $this->truncateTableWithI18n('{{city}}');
        $this->dropTableWithI18n('{{city}}');
    }
}
