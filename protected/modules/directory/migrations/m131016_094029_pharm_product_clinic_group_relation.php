<?php

class m131016_094029_pharm_product_clinic_group_relation extends I18nDbMigration
{
    public function safeUp()
    {

        $this->createTable(
            "{{pharm_product_clinic_group_relation}}",
            array(
                "pharm_product_uid" => "INT(11) NOT NULL COMMENT 'Ссылка на препарат'",
                "pharm_group_uid" => "INT(11) NOT NULL COMMENT 'Ссылка на группу препаратов'",
            ),
            "ENGINE=InnoDB COMMENT='Связь лекарственных препаратов с группами препаратов'"
        );

        $this->addForeignKey(
            "fk_pharm_product_uid_pharm_product_clinic_group_relation",
            "{{pharm_product_clinic_group_relation}}",
            "pharm_product_uid",
            "{{pharm_product}}",
            "uid",
            "CASCADE",
            "CASCADE"
        );

        $this->addForeignKey(
            "fk_pharm_group_uid_pharm_product_clinic_group_relation",
            "{{pharm_product_clinic_group_relation}}",
            "pharm_group_uid",
            "{{pharm_clinic_group}}",
            "uid",
            "CASCADE",
            "CASCADE"
        );

        /*
         * INSERT rehoming.rh_pharm_product_clinic_group_relation (pharm_product_uid, `pharm_group_uid`)
           SELECT pharm_product_id, pharm_clinic_group_id FROM
           helterbook.pharm_product_clinic_group
         */
    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_pharm_product_uid_pharm_product_clinic_group_relation", "{{pharm_product_clinic_group_relation}}");
        $this->dropForeignKey("fk_pharm_group_uid_pharm_product_clinic_group_relation", "{{pharm_product_clinic_group_relation}}");
        $this->dropTable("{{pharm_product_clinic_group_relation}}");
    }
}
