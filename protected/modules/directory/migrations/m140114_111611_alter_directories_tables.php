<?php

class m140114_111611_alter_directories_tables extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn(
            "{{country}}",
            "is_delete",
            "TINYINT(1) not null DEFAULT 0"
        );
        $this->addColumn(
            "{{city}}",
            "is_delete",
            "TINYINT(1) not null DEFAULT 0"
        );
        $this->addColumn(
            "{{medical_center}}",
            "is_delete",
            "TINYINT(1) not null DEFAULT 0"
        );
        $this->addColumn(
            "{{adverse_result}}",
            "is_delete",
            "TINYINT(1) not null DEFAULT 0"
        );
        $this->addColumn(
            "{{arr_cupping}}",
            "is_delete",
            "TINYINT(1) not null DEFAULT 0"
        );
        $this->addColumn(
            "{{attack_consciousness}}",
            "is_delete",
            "TINYINT(1) not null DEFAULT 0"
        );
        $this->addColumn(
            "{{capture_control}}",
            "is_delete",
            "TINYINT(1) not null DEFAULT 0"
        );
        $this->addColumn(
            "{{device_polarity}}",
            "is_delete",
            "TINYINT(1) not null DEFAULT 0"
        );
        $this->addColumn(
            "{{electrode_placement}}",
            "is_delete",
            "TINYINT(1) not null DEFAULT 0"
        );
        $this->addColumn(
            "{{measurement_unit}}",
            "is_delete",
            "TINYINT(1) not null DEFAULT 0"
        );
    }

    public function safeDown()
    {
        $this->dropColumn("{{country}}", "is_delete");
        $this->dropColumn("{{city}}", "is_delete");
        $this->dropColumn("{{medical_center}}", "is_delete");
        $this->dropColumn("{{adverse_result}}", "is_delete");
        $this->dropColumn("{{arr_cupping}}", "is_delete");
        $this->dropColumn("{{attack_consciousness}}", "is_delete");
        $this->dropColumn("{{capture_control}}", "is_delete");
        $this->dropColumn("{{device_polarity}}", "is_delete");
        $this->dropColumn("{{electrode_placement}}", "is_delete");
        $this->dropColumn("{{measurement_unit}}", "is_delete");
    }
}
