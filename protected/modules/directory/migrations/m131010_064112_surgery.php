<?php

class m131010_064112_surgery extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTableWithI18n(
            '{{surgery}}',
            array(
                'uid' => 'pk',
                'name' => 'VARCHAR(100) NOT NULL',
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci',
            array('name')
        );
    }

    public function safeDown()
    {
        $this->dropTableWithI18n('{{surgery}}');
    }
}
