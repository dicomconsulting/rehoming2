<?php

class m131107_074444_alter_med_center extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn(
            "{{medical_center}}",
            "code",
            "varchar(255) null COMMENT 'Код медцентра'"
        );

    }

    public function safeDown()
    {
        $this->dropColumn(
            "{{medical_center}}",
            "code"
        );
    }
}
