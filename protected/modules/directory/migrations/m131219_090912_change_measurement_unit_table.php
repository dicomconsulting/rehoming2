<?php

class m131219_090912_change_measurement_unit_table extends I18nDbMigration
{
    public function safeUp()
    {
        $this->dropColumn('{{measurement_unit}}', 'flag');
    }

    public function safeDown()
    {
        $this->addColumn('{{measurement_unit}}', 'flag', 'TINYINT(1) DEFAULT 0');
    }
}
