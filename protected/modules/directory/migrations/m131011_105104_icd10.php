<?php

class m131011_105104_icd10 extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{icd10}}',
            array(
                'uid' => 'pk',
                'code' => "VARCHAR(7) DEFAULT NULL COMMENT 'Код МКБ-10'",
                'name' => "VARCHAR(255) DEFAULT NULL COMMENT 'Название'",
                'parent_id' => "INT(11) DEFAULT NULL COMMENT 'Ссылка на родительский элемент'",
                'is_injury' => "varchar(10) DEFAULT NULL COMMENT 'не исп-ся'",
                'record_id' => "INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'не исп-ся'",
                'order' => "INT(11) NOT NULL COMMENT 'Порядок отображения'",
                'type' => "VARCHAR(1) NOT NULL COMMENT 'не исп-ся'",
                'from' => "DECIMAL(10,2) NOT NULL COMMENT 'не исп-ся'",
                'to' => "DECIMAL(10,2) NOT NULL COMMENT 'не исп-ся'",
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT="МKB-10"'
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{icd10}}');
    }
}
