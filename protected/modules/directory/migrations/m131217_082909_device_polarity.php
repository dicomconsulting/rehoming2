<?php

class m131217_082909_device_polarity extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTableWithI18n(
            "{{device_polarity}}",
            array(
                "uid" => "pk",
                "name" => "VARCHAR(50)",
            ),
            "ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='Словарь возможных полярностей устройств'",
            ['name']
        );
    }

    public function safeDown()
    {
        $this->dropTableWithI18n("{{device_polarity}}");
    }
}
