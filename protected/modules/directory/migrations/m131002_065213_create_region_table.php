<?php

class m131002_065213_create_region_table extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTableWithI18n(
            '{{region}}',
            array(
                'uid' => 'pk',
                'name' => 'VARCHAR(100) NOT NULL',
                'full_name' => 'VARCHAR(100)'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci',
            array('name', 'full_name')
        );
    }

    public function safeDown()
    {
        $this->truncateTableWithI18n('{{region}}');
        $this->dropTableWithI18n('{{region}}');
    }
}
