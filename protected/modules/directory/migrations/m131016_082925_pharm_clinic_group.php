<?php

class m131016_082925_pharm_clinic_group extends I18nDbMigration
{
    public function safeUp()
    {

        $this->createTableWithI18n(
            "{{pharm_clinic_group}}",
            array(
                "uid" => "pk",
                "code" => "varchar(10)",
                "name" => "varchar(255) DEFAULT NULL COMMENT 'Название'",
            ),
            "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Справочник групп лекарственных препаратов'",
            array("name")
        );

        /**
        INSERT rehoming.rh_pharm_clinic_group (uid, `code`)
        SELECT pharm_clinic_group_id, pharm_clinic_code FROM
        helterbook.pharm_clinic_group;

        INSERT rehoming.rh_pharm_clinic_group_i18n (uid, `name`, locale)
        SELECT translation_id, pharm_clinic_group_name, 'ru' FROM
        helterbook.pharm_clinic_group_translation;
         */
    }

    public function safeDown()
    {
        $this->dropTableWithI18n("{{pharm_clinic_group}}");
    }
}
