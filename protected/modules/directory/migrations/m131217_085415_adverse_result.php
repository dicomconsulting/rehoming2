<?php

class m131217_085415_adverse_result extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTableWithI18n(
            "{{adverse_result}}",
            array(
                "uid" => "pk",
                "name" => "VARCHAR(50)",
            ),
            "ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='Словарь возможных исходов" .
            " нежелательного явления'",
            ['name']
        );
    }

    public function safeDown()
    {
        $this->dropTableWithI18n("{{adverse_result}}");
    }
}
