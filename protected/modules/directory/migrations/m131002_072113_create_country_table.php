<?php

class m131002_072113_create_country_table extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTableWithI18n(
            '{{country}}',
            array(
                'uid' => 'pk',
                'alpha2_code' => 'CHAR(2)',
                'alpha3_code' => 'CHAR(3)',
                'iso_code' => 'INTEGER(3)',
                'region_id' => 'INTEGER(11) NOT NULL',
                'name' => 'VARCHAR(100) NOT NULL',
                'full_name' => 'VARCHAR(100)'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci',
            array('name', 'full_name')
        );

        $this->addForeignKey(
            'fk_country_region_id_region_uid',
            '{{country}}',
            'region_id',
            '{{region}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_country_region_id_region_uid', '{{country}}');
        $this->truncateTableWithI18n('{{country}}');
        $this->dropTableWithI18n('{{country}}');
    }
}
