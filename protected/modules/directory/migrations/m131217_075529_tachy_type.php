<?php

class m131217_075529_tachy_type extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTableWithI18n(
            "{{tachy_type}}",
            array(
                "uid" => "pk",
                "name" => "VARCHAR(50)",
            ),
            "ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='Виды желудочковой тахикаркии'",
            ['name']
        );
    }

    public function safeDown()
    {
        $this->dropTableWithI18n("{{tachy_type}}");
    }
}
