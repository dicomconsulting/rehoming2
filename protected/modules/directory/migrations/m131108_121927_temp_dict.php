<?php

class m131108_121927_temp_dict extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            "{{pharm_group_tmp}}",
            array(
                "id" => "INT(11) NOT NULL",
                'parent_id' => "INT(11)",
                'name' => "VARCHAR(255)",
                'code' => "VARCHAR(255)",
                'PRIMARY KEY (`id`)',
                'INDEX `idx_parent_id` (`parent_id`)'
,            ),
            "CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='Таблица фрам. групп для демонстрационного справочника ЛС'"
        );

        $this->createTable(
            "{{pharm_product_tmp}}",
            array(
                "id" => "INT(11) NOT NULL",
                'group_id' => "INT(11)",
                'name' => "VARCHAR(255)",
                'latname' => "VARCHAR(255)",
                'PRIMARY KEY (`id`)',
                'INDEX `idx_group_id` (`group_id`)'
,            ),
            "CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='Таблица препаратов для демонстрационного справочника ЛС'"
        );

    }

    public function safeDown()
    {
        $this->dropTable("{{pharm_group_tmp}}");
        $this->dropTable("{{pharm_product_tmp}}");
    }
}
