<?php

class m131002_073417_create_medical_center_table extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTableWithI18n(
            '{{medical_center}}',
            array(
                'uid' => 'pk',
                'inn' => 'CHAR(12)',
                'city_id' => 'INTEGER(11) NOT NULL',
                'name' => 'VARCHAR(100) NOT NULL'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci',
            array('name')
        );
        $this->addForeignKey(
            'fk_medical_center_city_id_city_uid',
            '{{medical_center}}',
            'city_id',
            '{{city}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_medical_center_city_id_city_uid', '{{medical_center}}');
        $this->truncateTableWithI18n('{{medical_center}}');
        $this->dropTableWithI18n('{{medical_center}}');
    }
}
