<?php

class m131017_073316_measure extends I18nDbMigration
{
    public function safeUp()
    {

        $this->createTable(
            "{{pharm_measure}}",
            array("measurement_unit_uid" => "INT(11) NOT NULL COMMENT 'Ссылка на ед. изменения'"),
            "ENGINE=INNODB COMMENT='Список идентификаторов единиц измерения, которые могут использоваться в мед. препаратах'"
        );

        $this->addForeignKey(
            "fk_measurement_unit_uid_pharm_measure",
            "{{pharm_measure}}",
            "measurement_unit_uid",
            "{{measurement_unit}}",
            "uid",
            "CASCADE",
            "CASCADE"
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_measurement_unit_uid_pharm_measure", "{{pharm_measure}}");
        $this->dropTable("{{pharm_measure}}");
    }
}
