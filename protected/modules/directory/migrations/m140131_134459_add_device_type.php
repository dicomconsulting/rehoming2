<?php

class m140131_134459_add_device_type extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTableWithI18n(
            "{{device_type}}",
            [
                'uid' => 'pk',
                'name' => 'varchar(32) not null comment "Название"',
                'name_full' => 'varchar(255) not null comment "Полное название"'
            ],
            "CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='Таблица типов устройств'",
            ['name', 'name_full']
        );
    }

    public function safeDown()
    {
        $this->dropTableWithI18n("{{device_type}}");
    }
}
