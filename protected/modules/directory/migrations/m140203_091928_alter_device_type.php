<?php

class m140203_091928_alter_device_type extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn(
            "{{device_type}}",
            'is_delete',
            "tinyint(1) NOT NULL DEFAULT '0'"
        );
    }

    public function safeDown()
    {
        $this->dropColumn(
            "{{device_type}}",
            'is_delete'
        );
    }
}
