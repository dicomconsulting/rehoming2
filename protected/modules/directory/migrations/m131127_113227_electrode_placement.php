<?php

class m131127_113227_electrode_placement extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTableWithI18n(
            '{{electrode_placement}}',
            array(
                'uid' => 'pk',
                'name' => 'VARCHAR(100) NOT NULL',
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT="Вариант расположения электрода"',
            array('name')
        );
    }

    public function safeDown()
    {
        $this->dropTableWithI18n('{{electrode_placement}}');
    }

}
