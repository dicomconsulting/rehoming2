<?php

class m131002_130044_create_measurement_unit_table extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTableWithI18n(
            '{{measurement_unit}}',
            array(
                'uid' => 'pk',
                'flag' => 'TINYINT(1)',
                'name' => 'VARCHAR(50) NOT NULL',
                'full_name' => 'VARCHAR(100)'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci',
            array('name', 'full_name')
        );
    }

    public function safeDown()
    {
        $this->dropTableWithI18n('{{measurement_unit}}');
    }
}
