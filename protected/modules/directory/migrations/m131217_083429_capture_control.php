<?php

class m131217_083429_capture_control extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTableWithI18n(
            "{{capture_control}}",
            array(
                "uid" => "pk",
                "name" => "VARCHAR(50)",
            ),
            "ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='Словарь возможных настроек контроля захвата'",
            ['name']
        );
    }

    public function safeDown()
    {
        $this->dropTableWithI18n("{{capture_control}}");
    }
}
