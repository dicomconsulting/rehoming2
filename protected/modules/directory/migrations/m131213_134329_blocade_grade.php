<?php

class m131213_134329_blocade_grade extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            "{{blockade_degree}}",
            array(
                "uid" => "INT(11) NOT NULL AUTO_INCREMENT",
                "name" => "VARCHAR(50)",
                "PRIMARY KEY (`uid`)"
            ),
            "ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='Словарь степеней блокады'"
        );
    }

    public function safeDown()
    {
        $this->dropTable("{{blockade_degree}}");
    }
}
