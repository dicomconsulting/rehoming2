<?php

class m131203_083138_alter_measure_unit extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{pharm_measure}}', "order", "int(11) NOT NULL DEFAULT 0");

        //миллиграммы первыми
        $this->execute("UPDATE {{pharm_measure}} SET `order` = 1 WHERE measurement_unit_uid = 13");
    }

    public function safeDown()
    {
        $this->dropColumn('{{pharm_measure}}', "order");
    }
}
