<?php

class m131206_134146_alter_section extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn(
            "{{section}}",
            "order",
            "int(11) not null comment 'Порядок секций'"
        );
    }

    public function safeDown()
    {
        $this->dropColumn("{{section}}", "order");
    }
}
