<?php

class m140203_121733_alter_device_type extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn(
            "{{device_type}}",
            'alias',
            "varchar(64) NOT NULL DEFAULT ''"
        );
    }

    public function safeDown()
    {
        $this->dropColumn(
            "{{device_type}}",
            'alias'
        );
    }
}
