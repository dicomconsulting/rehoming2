<?php

class m160225_173021_create_device_alter_messages extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTableWithI18n(
            '{{device_alter_messages}}',
            [
                'uid'      => 'pk',
                'message' => 'VARCHAR(255) NOT NULL',
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{device_alter_messages}}');
    }
}
