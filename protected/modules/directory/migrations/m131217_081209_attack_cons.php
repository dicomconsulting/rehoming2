<?php

class m131217_081209_attack_cons extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTableWithI18n(
            "{{attack_consciousness}}",
            array(
                "uid" => "pk",
                "name" => "VARCHAR(50)",
            ),
            "ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='Словарь возможных вариантов сознания " .
            "пациента при приступе'",
            ['name']
        );
    }

    public function safeDown()
    {
        $this->dropTableWithI18n("{{attack_consciousness}}");
    }
}
