<?php

class m131120_072054_nyha_class extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTableWithI18n(
            "{{nyha_class}}",
            array(
                "uid" => "INT(11) NOT NULL AUTO_INCREMENT",
                "name" => "VARCHAR(50)",
                "PRIMARY KEY (`uid`)"
            ),
            "ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='Словарь классов NYHA'",
            "name"
        );
    }

    public function safeDown()
    {
        $this->dropTableWithI18n("{{nyha_class}}");
    }
}
