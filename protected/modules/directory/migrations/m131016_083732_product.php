<?php

class m131016_083732_product extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTableWithI18n(
            "{{pharm_product}}",
            array(
                "uid" => "pk",
                "name_latin" => "varchar(255) DEFAULT NULL COMMENT 'Латинское назввание'",
                "name" => "varchar(255) DEFAULT NULL COMMENT 'Название'",
                "composition" => "text COMMENT 'composition'",
                "zip_info" => "varchar(255) DEFAULT NULL",
            ),
            "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Справочник лекарственных препаратов'",
            array("name", "composition", "zip_info")
        );

        /**
        INSERT rehoming.rh_pharm_product (uid, name_latin)
        SELECT pharm_product_id, pharm_product_name_latin
        FROM helterbook.pharm_product ;

        INSERT rehoming.rh_pharm_product_i18n (uid, locale, `name`, composition,  zip_info)
        SELECT translation_id, 'ru', pharm_product_name, pharm_product_composition, pharm_product_zip_info  FROM
        helterbook.pharm_product_translation;
         */
    }

    public function safeDown()
    {
        $this->dropTableWithI18n("{{pharm_product}}");
    }
}
