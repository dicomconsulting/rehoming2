<?php

class m131024_130501_alter_phoduct_group_rel_add_unique extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createIndex(
            "idx_rel_unique",
            "{{pharm_product_clinic_group_relation}}",
            "pharm_product_uid, pharm_group_uid",
            true
        );
    }

    public function safeDown()
    {
        $this->dropIndex("idx_rel_unique", "{{pharm_product_clinic_group_relation}}");
    }
}
