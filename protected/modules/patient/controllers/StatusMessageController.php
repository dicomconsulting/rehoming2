<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Damir Garifullin
 * Date: 22.10.13
 * Time: 10:00
 */

class StatusMessageController extends Controller
{

    public function accessRules()
    {
        return [
            [
                'allow',
                'roles' => ['acknowledgeMessage']
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }


    public function actionToggleAcknowledge($id, $status_message_uid)
    {
        $request = Yii::app()->request->isAjaxRequest;

        $message = Message::createByUid($status_message_uid);
        if ($message) {
            $acknowledged = $message->toggleAcknowledge();

            $this->renderJSON(
                ['acknowledged' => $acknowledged, 'active' => $message->isActual(), 'uid' => $status_message_uid]
            );
        } else {
            $this->renderJSON(array("status" => "error", "message" => "No status message was found"));
        }
    }

    protected function renderJSON($data)
    {
        header('Content-type: application/json');
        echo CJSON::encode($data);

        foreach (Yii::app()->log->routes as $route) {
            if ($route instanceof CWebLogRoute) {
                $route->enabled = false; // disable any weblogroutes
            }
        }
        Yii::app()->end();
    }
}
