<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 06.11.13
 * Time: 10:30
 */

class RecordingController extends Controller {

    const DEFAULT_PAGE_SIZE = 10;

    public $layout = '//layouts/innerMenu';

    public function actionList($id, $ps = null)
    {
        $patient = $this->loadPatientModel($id);

        if (!$ps) {
            $ps = self::DEFAULT_PAGE_SIZE;
        } else {
            $ps = (int) $ps;
        }

        /* @TODO перенести всё в search */
        $dataProvider=new CActiveDataProvider('Recording', array(
            'criteria'=>array(
                "condition" => "device_id = :device_id",
                'params' => array(":device_id" => $patient->device->uid),
                'order'=>'state_create_time DESC, state_uid'
            ),
            'pagination'=>array(
                'pageSize' => $ps,
            ),
        ));

        $this->render(
            'list',
            array(
                "dataProvider" => $dataProvider,
                "ps" => $ps,
                'patient' => $patient
            )
        );
    }

    public function actionItem($id, $state_id)
    {
        $patient = $this->loadPatientModel($id);

        $record = Recording::model()->findByAttributes(array("state_uid" => $state_id));

        $this->render("item", array(
                "patient" => $patient,
                "record" => $record
            ));

    }

    public function actionEcgImage($deviceId, $episodeNumber)
    {
        $ecgImage = ECGImage::model()->findByPk(['device_id' => $deviceId, 'episode_number' => $episodeNumber]);
        if ($ecgImage instanceof ECGImage) {
            $ecgImage->send();
        }
        Yii::app()->end();
    }
}
