<?php

abstract class PatientCardController extends Controller
{
    const DEFAULT_PAGE_SIZE = 50;
    public $layout = '//layouts/innerMenu';
    protected $renderToPdf = false;
    protected $pageTitlePrefix = "ReHoming";


    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => ['printHeader', 'printFooter'],
                'users' => ['*'],
            ],
            [
                'allow',
                'roles' => ['viewPatientCard']
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

    public function init()
    {
        parent::init();


        $patientId = Yii::app()->getRequest()->getQuery('id');
        $state = UserGroupSeparationBehavior::disableDataSeparation();

        $this->loadPatientModel($patientId);

        //настройки вывода в pdf
        $pdfOptions = $this->getPdfPageOptions();
        $pdfOptions['header-html'] = Yii::app()->request->getBaseUrl(true) . $this->createUrl(
            $this->id . "/printHeader",
            array("id" => $this->patient->uid)
        );
        $pdfOptions['footer-html'] = Yii::app()->request->getBaseUrl(true) . $this->createUrl(
            $this->id . "/printFooter",
            array("id" => $this->patient->uid)
        );
        $this->setPdfPageOptions($pdfOptions);

        UserGroupSeparationBehavior::setState($state);
    }

    public function behaviors()
    {
        return array(
            'pdfable' => array(
                'class' => 'ext.pdfable.Pdfable',
                // Global default options for wkhtmltopdf
                'pdfOptions' => array(
                    // Use binary path from module config
                    'bin' => Yii::app()->getModule("pdfable")->bin,
                    'use-xserver',
                    'dpi' => 600,
                    'margin-top' => 19,
                    'margin-right' => 5,
                    'margin-bottom' => 15,
                    'margin-left' => 10,
                    'header-spacing' => 3,
                    'footer-spacing' => 3,
                ),
                // Default page options
                'pdfPageOptions' => array(
                    'print-media-type',
                    'enable-javascript',
                    'javascript-delay' => 500,
                ),
            )
        );
    }

    public function actionPrint($id, $action)
    {
        $state = UserGroupSeparationBehavior::disableDataSeparation();
        $patient = $this->loadPatientModel($id);

        $method = "action" . ucfirst(strtolower($action));

        if (!method_exists($this, $method)) {
            $this->redirect(array('profile/index', 'id' => $patient->uid));
        }

        $this->renderToPdf = true;

        $this->layout = '//layouts/print';
        // для печати необходима ширина не менее 1024, иначе виджеты "ползут"
        // параметр прописывается в лейауте
        Yii::app()->params['pageContainerWidth'] = "1024px";

        $this->$method($id);
        UserGroupSeparationBehavior::setState($state);
    }

    public function loadPatientModel($id)
    {
        parent::loadPatientModel($id);
        return $this->patient;
    }

    public function render($view, $data = null, $return = false)
    {
        if (!$this->renderToPdf) {
            $html = parent::render($view, $data, true);
            if ($return) {
                return $html;
            } else {
                echo $html;
            }
        } else {
            $this->renderToPdf = false;
            return $this->renderPdf($view, $data);
        }
    }

    public function actionPrintHeader(
        $page = 0,
        $title = "",
        $frompage = 0,
        $topage = 0,
        $doctitle = 0
    ) {

        $state = UserGroupSeparationBehavior::disableDataSeparation();
        $widgetName = "PatientPagesHeader";
        echo $this->widget($widgetName, array("title" => $title, "patient" => $this->patient), true);
        UserGroupSeparationBehavior::setState($state);

        Yii::app()->end();
    }

    public function actionPrintFooter(
        $page = 0,
        $title = "",
        $frompage = 0,
        $topage = 0,
        $doctitle = 0
    ) {

        $state = UserGroupSeparationBehavior::disableDataSeparation();

        $widgetName = "PatientListFooter";
        echo $this->widget($widgetName, array("title" => $title, "page" => $page, "pageTotal" => $topage), true);

        UserGroupSeparationBehavior::setState($state);
        Yii::app()->end();
    }

    public function assignPageTitle($titlePostfix)
    {
        $this->pageTitle = Yii::t("PatientModule.patient", $this->pageTitlePrefix) . " - " . $titlePostfix;
    }
}
