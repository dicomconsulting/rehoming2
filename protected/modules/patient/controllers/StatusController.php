<?php
Yii::import("patient.controllers.PatientCardController");
class StatusController extends PatientCardController
{
    protected $pageTitlePrefix = "Статус";

    public function actionSummary($id)
    {
        $this->assignPageTitle(Yii::t("PatientModule.patient", "Общий"));
        $patient = $this->loadPatientModel($id);
        $patient->device->assignStateValuesToDeviceModelParameters();
        $this->render("summary", ['patient' => $patient]);
    }

    public function actionDevice($id)
    {
        $this->assignPageTitle(Yii::t("PatientModule.patient", "Устройство"));
        $patient = $this->loadPatientModel($id);
        $patient->device->assignStateValuesToDeviceModelParameters();
        $this->render("device", ['patient' => $patient]);
    }

    public function actionLead($id)
    {
        $this->assignPageTitle(Yii::t("PatientModule.device", "Электрод"));
        $patient = $this->loadPatientModel($id);
        $patient->device->assignStateValuesToDeviceModelParameters();
        $this->render("lead", ['patient' => $patient]);
    }

    public function actionBradycardia($id)
    {
        $this->assignPageTitle(Yii::t("PatientModule.patient", "Брадикардия/РТС"));
        $patient = $this->loadPatientModel($id);
        $patient->device->assignStateValuesToDeviceModelParameters();
        $this->render("bradycardia", ['patient' => $patient]);
    }

    public function actionAtrial($id)
    {
        $this->assignPageTitle(Yii::t("PatientModule.patient", "Предсердная аритмия"));
        $patient = $this->loadPatientModel($id);
        $patient->device->assignStateValuesToDeviceModelParameters();
        $this->render("atrial", ['patient' => $patient]);
    }

    public function actionVentricular($id)
    {
        $this->assignPageTitle(Yii::t("PatientModule.arrhythmia", "Желудочковая аритмия"));
        $patient = $this->loadPatientModel($id);
        $patient->device->assignStateValuesToDeviceModelParameters();
        $this->render("ventricular", ['patient' => $patient]);
    }

    public function actionPhysiologic($id)
    {
        $this->assignPageTitle(Yii::t("PatientModule.patient", "Физиологические параметры"));
        $patient = $this->loadPatientModel($id);
        $patient->device->assignStateValuesToDeviceModelParameters();
        $this->render("physiologic", ['patient' => $patient]);
    }

    public function actionHf($id)
    {
        $this->assignPageTitle(Yii::t("PatientModule.patient", "Монитор нарушений ритма (HFmonitor)"));
        $patient = $this->loadPatientModel($id);
        $patient->device->assignStateValuesToDeviceModelParameters();
        $this->render("hf", ['patient' => $patient]);
    }
}
