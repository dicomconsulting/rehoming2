<?php
Yii::import("patient.controllers.PatientCardController");

class HistoryController extends PatientCardController
{
    public function actionList($id, $ps = null)
    {
        $patient = $this->loadPatientModel($id);

        if (!$ps) {
            $ps = self::DEFAULT_PAGE_SIZE;
        } else {
            $ps = (int) $ps;
        }

        $dataProvider=new CActiveDataProvider('HistoryMessage', array(
            'criteria'=>array(
                "condition" => "device_id = :device_id",
                'params' => array(":device_id" => $patient->device->uid),
                'order'=>'time DESC'
            ),
            'pagination'=>array(
                'pageSize' => $ps,
            ),
        ));

        $this->render('list',
            array(
                "dataProvider" => $dataProvider,
                "ps" => $ps,
            )
        );
    }
}