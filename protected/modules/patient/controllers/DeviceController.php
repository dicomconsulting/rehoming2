<?php
Yii::import("patient.controllers.PatientCardController");
class DeviceController extends PatientCardController
{
    protected $pageTitlePrefix = "Параметры устройства";

    public function actionOverview($id)
    {
        $this->assignPageTitle(Yii::t("PatientModule.patient", "Обзор"));
        $patient = $this->loadPatientModel($id);
        $patient->device->assignStateValuesToDeviceModelParameters();
        $this->render("overview", ['patient' => $patient]);
    }

    public function actionLead($id)
    {
        $this->assignPageTitle(Yii::t("PatientModule.device", "Электрод"));
        $patient = $this->loadPatientModel($id);
        $patient->device->assignStateValuesToDeviceModelParameters();
        $this->render("lead", ['patient' => $patient]);
    }

    public function actionBradycardia($id)
    {
        $this->assignPageTitle(Yii::t("PatientModule.patient", "Брадикардия/РТС"));
        $patient = $this->loadPatientModel($id);
        $patient->device->assignStateValuesToDeviceModelParameters();
        $this->render("bradycardia", ['patient' => $patient]);
    }

    public function actionAtrial($id)
    {
        $this->assignPageTitle(Yii::t("PatientModule.patient", "Предсердная аритмия"));
        $patient = $this->loadPatientModel($id);
        $patient->device->assignStateValuesToDeviceModelParameters();
        $this->render("atrial", ['patient' => $patient]);
    }

    public function actionVentricular($id)
    {
        $this->assignPageTitle(Yii::t("PatientModule.arrhythmia", "Желудочковая аритмия"));
        $patient = $this->loadPatientModel($id);
        $patient->device->assignStateValuesToDeviceModelParameters();
        $this->render("ventricular", ['patient' => $patient]);
    }

    public function actionHm($id)
    {
        $this->assignPageTitle(Yii::t("PatientModule.patient", "Домашний мониторинг"));
        $patient = $this->loadPatientModel($id);
        $patient->device->assignStateValuesToDeviceModelParameters();
        $this->render("hm", ['patient' => $patient]);
    }
}
