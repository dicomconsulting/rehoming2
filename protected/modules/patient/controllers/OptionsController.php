<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Damir Garifullin
 * Date: 22.10.13
 * Time: 10:00
 */

class OptionsController extends Controller
{

    public $layout = '//layouts/innerMenu';

    public function init()
    {
        parent::init();
        $this->pageTitle = Yii::t("patient", "Опции");

        $patientId = Yii::app()->getRequest()->getQuery('id');

        $state = UserGroupSeparationBehavior::disableDataSeparation();
        $this->loadPatientModel($patientId);
        UserGroupSeparationBehavior::setState($state);

        //настройки вывода в pdf
        $pdfOptions = $this->getPdfPageOptions();
        $pdfOptions['header-html'] = Yii::app()->request->getBaseUrl(true) . $this->createUrl(
            $this->id . "/printHeader",
            array("id" => $this->patient->uid)
        );
        $pdfOptions['footer-html'] = Yii::app()->request->getBaseUrl(true) . $this->createUrl(
            $this->id . "/printFooter",
            array("id" => $this->patient->uid)
        );
        $this->setPdfPageOptions($pdfOptions);
    }

    public function accessRules()
    {
        $accessRules = parent::accessRules();

        array_unshift(
            $accessRules,
            [
                'allow',
                'actions' => ['printHeader', 'printFooter'],
                'users'   => ['*'],
            ]
        );

        return $accessRules;
    }

    public function actionIndex($id)
    {
        $patient = $this->loadPatientModel($id);

        list($optionsGrouped, $sections) = $this->getRenderingData($patient);

        $this->render(
            "index",
            [
                'patient' => $patient,
                'options' => $optionsGrouped,
                'sections' => $sections,
                'imagesPath' => ""
            ]
        );
    }

    /**
     * @param $patient
     * @return array
     */
    protected function getRenderingData($patient)
    {
        $options = OptionFactory::getOptionsByDevice($patient->device);
        $optionsGrouped = $this->groupOptionBySections($options);

        $sections = Section::model()->getOrdered();
        return array($optionsGrouped, $sections);
    }

    /**
     * @param AbstractOption[] $options
     * @return array
     */
    public function groupOptionBySections($options)
    {
        $result = [];

        foreach ($options as $option) {
            if ($option->getLevel() != AbstractOption::ALERT_LEVEL_OFF) {
                $result[$option->getSection()][] = $option;
            }
        }

        return $result;
    }


    public function actionPrint($id)
    {
        $this->layout = '//layouts/print';

        $patient = $this->loadPatientModel($id);

        list($optionsGrouped, $sections) = $this->getRenderingData($patient);

        $imagesPath = Yii::getPathOfAlias("webroot");

        $this->renderPdf(
            "index",
            [
                'patient' => $patient,
                'options' => $optionsGrouped,
                'sections' => $sections,
                'imagesPath' => $imagesPath
            ]
        );
    }

    public function behaviors()
    {
        return array(
            'pdfable' => array(
                'class' => 'ext.pdfable.Pdfable',
                // Global default options for wkhtmltopdf
                'pdfOptions' => array(
                    // Use binary path from module config
                    'bin' => Yii::app()->getModule("pdfable")->bin,
                    'use-xserver',
                    'dpi' => 600,
                    'margin-top' => 19,
                    'margin-right' => 5,
                    'margin-bottom' => 15,
                    'margin-left' => 10,
                    'header-spacing' => 3,
                    'footer-spacing' => 3,
                ),
                // Default page options
                'pdfPageOptions' => array(
                    'print-media-type',
                    'enable-javascript',
                    'javascript-delay' => 500,
                ),
            )
        );
    }


    public function actionPrintHeader(
        $page = 0,
        $title = "",
        $frompage = 0,
        $topage = 0,
        $doctitle = 0
    ) {
        $state = UserGroupSeparationBehavior::disableDataSeparation();

        $widgetName = "PatientPagesHeader";
        echo $this->widget($widgetName, array("title" => $title, "patient" => $this->patient), true);
        UserGroupSeparationBehavior::setState($state);

        Yii::app()->end();
    }

    public function actionPrintFooter(
        $page = 0,
        $title = "",
        $frompage = 0,
        $topage = 0,
        $doctitle = 0
    ) {

        $state = UserGroupSeparationBehavior::disableDataSeparation();
        $widgetName = "PatientListFooter";
        echo $this->widget($widgetName, array("title" => $title, "page" => $page, "pageTotal" => $topage), true);

        UserGroupSeparationBehavior::setState($state);
        Yii::app()->end();
    }
}
