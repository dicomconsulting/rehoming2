<?php

class ViewController extends Controller
{
    public $layout = '//layouts/innerMenu';

    public function actionIndex($id)
    {
        $patient = $this->loadPatientModel($id);
        if ($patient->existDeviceData()) {
            $patient->device->assignStateValuesToDeviceModelParameters();
        }
        $this->render("index", ['patient' => $patient]);
    }
}
