<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.11.13
 * Time: 10:52
 */

class ClinicalController extends Controller
{
    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => ['edit', 'delete'],
                'roles' => ['editClinicalData']
            ],
            [
                'deny',
                'actions' => ['edit', 'delete'],
                'users' => ['*']
            ],
            [
                'allow',
                'roles' => ['viewPatientCard']
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

    public function actions()
    {
        return array(
            'diseaseSelector.'=>'directory.widgets.DiseaseSelector',
            'pharmSelector.'=>'directory.widgets.PharmSelector',
        );
    }

    public $layout = '//layouts/innerMenu';

    public function init()
    {
        Yii::app()->getModule("directory");
    }

    public function actionIndex($id, $recordId = 0)
    {
        $patient = $this->loadPatientModel($id);

        $latest = true;

        if (!$recordId) {
            $data = ClinicalData::getLatestDataByPatient($patient);
        } else {
            $latest = false;
            $data = ClinicalData::model()->findByPk($recordId);
        }

        $allRecords = ClinicalData::model()->findAllByAttributes(array("patient_uid" => $patient->uid));

        $this->render("index", array(
                "patient" => $patient,
                "latestData" => $data,
                "records" => $allRecords,
                "latest" => $latest,
            ));
    }

    public function actionEdit($id, $recordId = 0)
    {
        $patient = $this->loadPatientModel($id);

        if (!$recordId) {
            $model = new ClinicalData();
            $model->patient_uid = $patient->uid;
            $model->user_uid = Yii::app()->user->id;
            if (!isset($_POST['ClinicalData'])) {
                $model->loadDefaultData();
            }
        } else {
            $model = ClinicalData::model()->findByPk($recordId);
        }

        // Аякс-валидация
        if (Yii::app()->getRequest()->getIsAjaxRequest()) {
            echo CActiveForm::validate(array($model));
            Yii::app()->end();
        }

        if (isset($_POST['ClinicalData'])) {

            $model->attributes=$_POST['ClinicalData'];

            if (isset($_POST['ClinicalData']['icd10'])
                && !empty($_POST['ClinicalData']['icd10'])) {
                $model->icd10 = Icd10::model()
                    ->findAllByPk($_POST['ClinicalData']['icd10']);
            } else {
                $model->icd10 = array();
            }

            if (isset($_POST['ClinicalData']['icd10_main'])
                && !empty($_POST['ClinicalData']['icd10_main'])) {
                $model->icd10_main = Icd10::model()
                    ->findAllByPk($_POST['ClinicalData']['icd10_main']);
            } else {
                $model->icd10_main = array();
            }

            //добавляем в модель связь с лекарственными препаратами
            if (isset($_POST['ClinicalData']['therapy'])
                && !empty($_POST['ClinicalData']['therapy'])) {

                if ($model->scenario == "update") {
                    ClinicalDataTherapy::model()->deleteAllByAttributes(
                        array("clinical_data_uid" => $model->uid)
                    );
                }

                $pharmProducts = array();

                foreach ($_POST['ClinicalData']['therapy'] as $rel) {
                    $therapy = new ClinicalDataTherapy();
                    $therapy->attributes = $rel;
                    $pharmProducts[] = $therapy;
                }

                $model->therapy = $pharmProducts;
            } else {
                $model->therapy = array();
            }

            $surgeries = array();

            if ($model->scenario == "update") {
                ClinicalDataSurgery::model()->deleteAllByAttributes(
                    array("clinical_data_uid" => $model->uid)
                );
            }

            if (isset($_POST['ClinicalData']['surgeryRel'])
                && !empty($_POST['ClinicalData']['surgeryRel'])) {

                foreach ($_POST['ClinicalData']['surgeryRel'] as $rel) {
                    $surgery = new ClinicalDataSurgery();
                    $surgery->attributes = $rel;
                    $surgeries[] = $surgery;
                }
            }
            $model->surgeryRel = $surgeries;

            if ($model->validate()) {
                $model->save();
                $this->redirect($this->createUrl($this->id . "/index", array("id" => $id)));
                return;
            }
        }

        $pharmMeasureUnits = PharmMeasure::makeFlatArray(MeasurementUnit::model()->getPharmUnits());

        $this->render('edit', array(
                'model'=>$model,
                "pharmMeasureUnits" => $pharmMeasureUnits,
            ));
    }

    public function actionDelete($id, $recordId)
    {
        /** @var ClinicalData $model */
        $model = ClinicalData::model()->findByPk($recordId);

        if (!$model || $model->patient_uid != $id) {
            throw new CHttpException(404, "Запись не найдена");
        }

        $model->delete();
        $this->redirect($this->createUrl($this->id . "/index", array("id" => $id)));
    }


    public function actionHistory($id, $parameter)
    {
        $this->render("history", ['patient' => $this->loadPatientModel($id), 'parameter' => $parameter ]);
    }

    /**
     * @TODO справочники в отдельные виджеты, туда же перенести экшены по получению данных
     */
    /**
     * Преобразует ед. измерения лекарственных препаратов для использования в шаблоне mustache
     *
     * @param $units
     * @param $selected
     * @return array
     */
    public function preparePharmUnits($units, $selected)
    {
        $res = array();

        foreach ($units as $key => $unit) {
            $tmpArr = array(
                "id" => $key,
                "val" => $unit
            );

            if ($key == $selected) {
                $tmpArr['selected'] = true;
            }

            $res[] = $tmpArr;
        }

        return $res;
    }
}
