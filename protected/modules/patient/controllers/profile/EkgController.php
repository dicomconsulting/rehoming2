<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.11.13
 * Time: 10:52
 */

class EkgController extends Controller
{
    public $layout = '//layouts/innerMenu';

    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => ['edit', 'delete'],
                'roles' => ['editEkg']
            ],
            [
                'deny',
                'actions' => ['edit', 'delete'],
                'users' => ['*']
            ],
            [
                'allow',
                'roles' => ['viewPatientCard']
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

    public function actionIndex($id, $recordId = 0)
    {
        $patient = $this->loadPatientModel($id);

        $latest = true;

        if (!$recordId) {
            $data = PatientEkg::getLatestDataByPatient($patient);
        } else {
            $latest = false;
            $data = PatientEkg::model()->findByPk($recordId);
        }

        $allRecords = PatientEkg::model()->findAllByAttributes(array("patient_uid" => $patient->uid));

        $this->render("index", array(
                "patient" => $patient,
                "latestData" => $data,
                "records" => $allRecords,
                "latest" => $latest,
            ));
    }

    public function actionEdit($id, $recordId = 0)
    {
        $patient = $this->loadPatientModel($id);

        if (!$recordId) {
            $model = new PatientEkg;
            $model->patient_uid = $patient->uid;
            $model->user_uid = Yii::app()->user->id;
        } else {
            $model = PatientEkg::model()->findByPk($recordId);
        }


        // Аякс-валидация
        if (Yii::app()->getRequest()->getIsAjaxRequest()) {
            echo CActiveForm::validate(array($model));
            Yii::app()->end();
        }

        if (isset($_POST['PatientEkg'])) {
            $model->attributes=$_POST['PatientEkg'];

            if ($model->validate()) {
                $model->save();
                $this->redirect($this->createUrl($this->id . "/index", array("id" => $id)));
                return;
            }
        }

        $this->render('edit', array('model'=>$model));
    }

    public function actionDelete($id, $recordId)
    {
        /** @var PatientEkg $model */
        $model = PatientEkg::model()->findByPk($recordId);

        if (!$model || $model->patient_uid != $id) {
            throw new CHttpException(404, "Запись не найдена");
        }

        $model->delete();
        $this->redirect($this->createUrl($this->id . "/index", array("id" => $id)));
    }

    /**
     * Возвращает список полей для отображения в последних данных
     * @return array
     */
    public function getFieldsToShow()
    {
        return array(
            'ef_lv',
            'edv',
            'csr',
            'regurgitation_degree',
            'vti',
            'ivmd_intra_spont',
            'ivmd_intra_bivent',
            'ivmd_inter_spont',
            'ivmd_inter_bivent',
            'rv_pressure',
            'electric_delay'
        );
    }
}
