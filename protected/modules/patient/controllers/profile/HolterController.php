<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.11.13
 * Time: 10:52
 */

class HolterController extends Controller
{
    public $layout = '//layouts/innerMenu';

    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => ['edit', 'delete'],
                'roles' => ['editHolter']
            ],
            [
                'deny',
                'actions' => ['edit', 'delete'],
                'users' => ['*']
            ],
            [
                'allow',
                'roles' => ['viewPatientCard']
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

    public function actionIndex($id, $recordId = 0)
    {
        $patient = $this->loadPatientModel($id);

        $latest = true;

        if (!$recordId) {
            $data = PatientHolter::getLatestDataByPatient($patient);
        } else {
            $latest = false;
            $data = PatientHolter::model()->findByPk($recordId);
        }

        $allRecords = PatientHolter::model()->findAllByAttributes(array("patient_uid" => $patient->uid));

        $this->render("index", array(
                "patient" => $patient,
                "latestData" => $data,
                "records" => $allRecords,
                "latest" => $latest,
            ));
    }

    public function actionEdit($id, $recordId = 0)
    {
        $patient = $this->loadPatientModel($id);

        if (!$recordId) {
            $model = new PatientHolter;
            $model->patient_uid = $patient->uid;
            $model->user_uid = Yii::app()->user->id;
        } else {
            $model = PatientHolter::model()->findByPk($recordId);
        }


        // Аякс-валидация
        if (Yii::app()->getRequest()->getIsAjaxRequest()) {
            echo CActiveForm::validate(array($model));
            Yii::app()->end();
        }

        if (isset($_POST['PatientHolter'])) {
            $model->attributes=$_POST['PatientHolter'];

            if ($model->validate()) {
                $model->save();
                $this->redirect($this->createUrl($this->id . "/index", array("id" => $id)));
                return;
            }
        }

        $this->render('edit', array('model'=>$model));
    }

    public function actionDelete($id, $recordId)
    {
        /** @var PatientHolter $model */
        $model = PatientHolter::model()->findByPk($recordId);

        if (!$model || $model->patient_uid != $id) {
            throw new CHttpException(404, "Запись не найдена");
        }

        $model->delete();
        $this->redirect($this->createUrl($this->id . "/index", array("id" => $id)));
    }

    /**
     * Возвращает список полей для отображения в последних данных
     * @return array
     */
    public function getFieldsToShow()
    {
        return array(
            'hr_max',
            'hr_min',
            'hr_avg',
            'sv_extr',
            'vent_extr',
            'sv_runs',
            'vent_runs'
        );
    }
}
