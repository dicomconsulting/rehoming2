<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Damir Garifullin
 * Date: 22.10.13
 * Time: 10:00
 */

/**
 * Class ListController
 * @mixin Pdfable
 */
class ListController extends Controller
{

    const DEFAULT_PAGE_SIZE = 50;
    const PRINT_PAGE_SIZE = 10;

    public function init()
    {
        parent::init();
        $this->pageTitle = Yii::t("PatientModule.patient", "Список пациентов");

        //настройки вывода в pdf
        $pdfOptions                = $this->getPdfPageOptions();
        $pdfOptions['header-html'] = Yii::app()->request->getBaseUrl(true) . $this->createUrl(
                $this->id . "/printHeader"
            );
        $pdfOptions['footer-html'] = Yii::app()->request->getBaseUrl(true) . $this->createUrl(
                $this->id . "/printFooter"
            );
        $this->setPdfPageOptions($pdfOptions);

    }

    public function accessRules()
    {
        $accessRules = parent::accessRules();

        array_unshift(
            $accessRules,
            [
                'allow',
                'actions' => ['printHeader', 'printFooter'],
                'users'   => ['*'],
            ]
        );

        return $accessRules;
    }

    /**
     * @param integer $ps Количество элементов на страницу
     * @param bool $unacknowledged показывать пациентов, у которых есть непросмотренные сообщения
     */
    public function actionIndex($ps = null, $unacknowledged = false)
    {

        if (!$ps) {
            $ps = self::DEFAULT_PAGE_SIZE;
        } else {
            $ps = (int)$ps;
        }
        if (array_key_exists('savedFilterId', $_REQUEST)) {
            $savedFilterId = intval($_REQUEST['savedFilterId']);
        } else {
            $savedFilterId = 0;
        }

        $patient = new Patient("search");
        $patient->onlyAssigned();
        $patient->unsetAttributes();

        if (isset($_GET['Patient'])) {
            $patient->attributes = $_GET['Patient'];
        }
        if (array_key_exists('AdditionalFilter', $_GET)) {
            foreach ($_GET['AdditionalFilter'] as $key => $value) {
                $patient->$key = $value;
            }
        }

        if ($unacknowledged) {
            $patient->acknowledge_search = 1;
        }

        $dataProvider             = $patient->search();
        $dataProvider->pagination = array(
            'pageSize' => $ps,
        );

        $this->render(
            'index',
            array(
                'dataProvider'   => $dataProvider,
                'patientModel'   => $patient,
                'ps'             => $ps,
                'unacknowledged' => $unacknowledged,
                'savedFilterId'  => $savedFilterId
            )
        );
    }

    public function actionUnacknowledged($ps = null)
    {
        $this->actionIndex($ps, 1);
    }

    public function behaviors()
    {
        return array(
            'pdfable' => array(
                'class'          => 'ext.pdfable.Pdfable',
                // Global default options for wkhtmltopdf
                'pdfOptions'     => array(
                    // Use binary path from module config
                    'bin'            => Yii::app()->getModule("pdfable")->bin,
                    'use-xserver',
                    'dpi'            => 600,
                    'margin-top'     => 19,
                    'margin-right'   => 5,
                    'margin-bottom'  => 15,
                    'margin-left'    => 10,
                    'header-spacing' => 3,
                    'footer-spacing' => 3,
                ),
                // Default page options
                'pdfPageOptions' => array(
                    'print-media-type',
                    'enable-javascript',
                    'javascript-delay' => 500,
                ),
            )
        );
    }

    public function actionPrint($ps = null, $unacknowledged = false)
    {
        $this->layout = '//layouts/print';

        $patient = new Patient("search");
        $patient->onlyAssigned();
        $patient->unsetAttributes();

        if (isset($_GET['Patient'])) {
            $patient->attributes = $_GET['Patient'];
        }

        if ($unacknowledged) {
            $patient->acknowledge_search = 1;
        }

        $dataProvider = $patient->search();

        $totalCount = $dataProvider->getTotalItemCount();

        $pdf = $this->createPdf();

        $method = "renderPage";

        for ($i = 0; $i <= floor($totalCount / self::PRINT_PAGE_SIZE); $i++) {

            $dataProvider = $patient->search();

            $dataProvider->pagination = array(
                'pageSize'    => self::PRINT_PAGE_SIZE,
                'currentPage' => $i,
            );


            $pdf->$method(
                'index',
                array(
                    "dataProvider"   => $dataProvider,
                    "patientModel"   => $patient,
                    "ps"             => $ps,
                    "unacknowledged" => $unacknowledged,
                    'savedFilterId'  => 0
                )
            );
        }

        $pdf->send();
    }

    public function actionPrintHeader(
        $page = 0,
        $title = "",
        $frompage = 0,
        $topage = 0,
        $doctitle = 0
    ) {

        $widgetName = "PatientListHeader";
        echo $this->widget($widgetName, array("title" => $title), true);

        Yii::app()->end();
    }

    public function actionPrintFooter(
        $page = 0,
        $title = "",
        $frompage = 0,
        $topage = 0,
        $doctitle = 0
    ) {

        $widgetName = "PatientListFooter";
        echo $this->widget($widgetName, array("title" => $title, "page" => $page, "pageTotal" => $topage), true);

        Yii::app()->end();
    }

}