<?php

class PatientInfoWidget extends CWidget
{
    /**
     * @var Patient
     */
    public $patient;

    public function run()
    {
        Yii::app()->getModule("protocols");

        /*
        $latestMessage = $this->patient->device->getLatestStatusMessage();
        if ($latestMessage) {
            if (in_array(
                $latestMessage->getLevel(),
                array(AbstractOption::ALERT_LEVEL_YELLOW, AbstractOption::ALERT_LEVEL_YELLOW_WITH_NOTICE)
            )) {
                $bg = "yellow";
            } else {
                $bg = "red";
            }
        } else {
            $bg = "";
        }*/
        $bg = "";

        $this->render(
            'main-info',
            [
                'patient' => $this->patient,
                'bg_class' => $bg
            ]
        );
    }
}
