<?php
Yii::import("device.models.*");
Yii::import("directory.models.MeasurementUnit");

class GraphManager extends CWidget
{
    const DAYS_OF_ABSENCE_DATA = 2;

    const MILLISECONDS_IN_SECOND = 1000;

    /**
     * 30 days in milliseconds
     */
    const MAX_ZOOM = 2592000000;

    /**
     * 1 hour in milliseconds
     */
    const MIN_ZOOM = 3600000;

    const GRAPH_ID_PREFIX = 'FlotChart_';

    const CHART_SUFFIX = '_Chart';

    const ADD_Y_AXIS_POS = 'right';

    private static $graphCounter = 0;

    private static $chartCounter = 0;

    /**
     * @var array Array of added graphs.
     */
    private $graphs = [];

    /**
     * @var array Symbol types for points.
     */
    private $symbols = ["circle", "square", "diamond", "triangle", "cross"];

    private $defaultYAxisSettings = [
        'zoomRange' => false,
        'panRange' => false,
        'min' => 0
    ];

    public function buildGraph($parameters, $name = null, $updateGraphIfExist = false, $yAxisTicks = null)
    {
        $parameters = $this->_normalizeParameters($parameters);
        $countNewParameters = count($parameters);
        $name = $this->_calculateName($parameters, $name);
        $yAxisTicks = $this->_normalizeYAxisTicks($yAxisTicks);
        $seriesToYAxisMap = [];
        $allYAxisTicks = [];
        if (is_array($yAxisTicks)) {
            $allYAxisTicks[] = $yAxisTicks;
        }
        $alreadyExist = false;
        if ($updateGraphIfExist) {
            foreach ($this->graphs as $itemId => $itemOptions) {
                if ($itemOptions['name'] == $name) {
                    $id = $itemId;
                    $parameters = array_merge($itemOptions['parameters'], $parameters);
                    $countNewParameters = count($parameters) - count($itemOptions['parameters']);
                    $allYAxisTicks = $itemOptions['yAxisTicks'];
                    if ($countNewParameters > 0 && !is_null($yAxisTicks) && $this->_isNewYAxisTicks($allYAxisTicks, $yAxisTicks)) {
                        $yAxisTicks['position'] = self::ADD_Y_AXIS_POS;
                        $allYAxisTicks[] = $yAxisTicks;
                    }
                    $seriesToYAxisMap = $itemOptions['seriesToYAxisMap'];
                    $alreadyExist = true;
                    break;
                }
            }

        }
        if (!$updateGraphIfExist || !$alreadyExist) {
            $id = $this->_generateId();
        }

        $countAllParameters = count($parameters);
        for ($i = $countAllParameters-$countNewParameters; $i < $countAllParameters; $i++) {
            $seriesToYAxisMap[$i] = count($allYAxisTicks) ? : 1;
        }

        $this->graphs[$id] = [
            'name' => $name,
            'parameters' => $parameters,
            'yAxisTicks' => $allYAxisTicks,
            'seriesToYAxisMap' => $seriesToYAxisMap
        ];

        return $this->_generateAnchorLink($id);
    }

    public function getAnchor()
    {
        return key($this->graphs);
    }

    public function getName()
    {
        return current($this->graphs)['name'];
    }

    public function renderGraph()
    {
        return $this->_renderGraph(
            current($this->graphs)['parameters'],
            current($this->graphs)['yAxisTicks'],
            current($this->graphs)['seriesToYAxisMap']
        );
    }

    public function resetGraphs()
    {
        return reset($this->graphs);
    }

    public function nextGraph()
    {
        return next($this->graphs);
    }

    public function countGraphs()
    {
        return count($this->graphs);
    }

    private function _generateAnchorLink($id)
    {
        return CHtml::link(Yii::t('PatientModule.device', 'График'), '#' . $id);
    }

    private function _generateId()
    {
            return self::GRAPH_ID_PREFIX . ++self::$graphCounter;
    }

    private function _normalizeParameters($parameters)
    {
        $normalizeParameters = [];
        if (is_array($parameters)) {
            foreach ($parameters as $parameter) {
                if ($parameter instanceof DeviceParameter) {
                    $normalizeParameters[] = $parameter;
                }
            }
        } elseif ($parameters instanceof DeviceParameter) {
            $normalizeParameters = [$parameters];
        }
        return $normalizeParameters;
    }

    private function _normalizeYAxisTicks($tickArray)
    {
        if (is_array($tickArray)) {
            $tickArray = ['ticks' => $tickArray];
        }
        return $tickArray;
    }

    private function _isNewYAxisTicks($existYAxisTicks, $yAxisTicks)
    {
        $isNew = true;
        foreach ($existYAxisTicks as $existYAxisTick) {
            if (empty($this->_arrayRecursiveDiff($existYAxisTick, $yAxisTicks))) {
                $isNew = false;
                break;
            }
        }

        return $isNew;
    }

    private function _arrayRecursiveDiff($sourceArray, $compareArray)
    {
        $diffArray = [];
        foreach ($sourceArray as $sourceKey => $sourceValue) {
            if (array_key_exists($sourceKey, $compareArray)) {
                if (is_array($sourceValue)) {
                    $recursiveDiffArray = $this->_arrayRecursiveDiff($sourceValue, $compareArray[$sourceKey]);
                    if (count($recursiveDiffArray)) {
                        $diffArray[$sourceKey] = $recursiveDiffArray;
                    }
                } else {
                    if ($sourceValue != $compareArray[$sourceKey]) {
                        $diffArray[$sourceKey] = $sourceValue;
                    }
                }
            } else {
                $diffArray[$sourceKey] = $sourceValue;
            }
        }
        return $diffArray;
    }

    private function _calculateName($parameters, $name)
    {
        if (empty($name)) {
            if (is_array($parameters)) {
                $name = current($parameters)->name;
            } else {
                $name = $parameters->name;
            }
        }

        return $name;
    }

    private function _renderGraph($parameters, $yAxisTicks, $seriesToYAxisMap)
    {
        $data = $this->_createData($parameters, $seriesToYAxisMap);
        $limits = $this->_calculateRangeLimits($data);
        $yAxisLabel = $this->_generateYAxisLabel($parameters);
        return $this->_createGraph($data, $limits[0], $limits[1], $yAxisTicks, $yAxisLabel);
    }

    private function _generateYAxisLabel($parameters)
    {
        $label = $this->getName();
        if (current($parameters)->unit instanceof MeasurementUnit) {
            $label .= ' [' . current($parameters)->unit->name . ']';
        }
        return $label;
    }

    private function _createGraph($data, $minValue, $maxValue, $yAxisTicks, $yAxisLabel = '')
    {
        $chartDivId = $this->getAnchor() . self::CHART_SUFFIX;
        $yAxis = $this->defaultYAxisSettings;
        $yAxis['min'] = $this->_calculateYMin($yAxisTicks);
        $this->widget(
            'application.extensions.Flot.FlotGraphWidget',
            array(
                'data'=> $data,
                'options' => [
                    'legend' => [
                        'position' => 'nw',
                        'show' => true,
                        'margin' => 10,
                        'backgroundOpacity' => 0.8
                    ],
                    'xaxis' => [
                        'mode' => 'time',
                        'timeformat' => "%d/%m/%y",
                        'zoomRange' => [self::MIN_ZOOM, self::MAX_ZOOM],
                        'min' => $maxValue - self::MAX_ZOOM,
                        'max' => $maxValue,
                        'panRange' => [$minValue, $maxValue],
                        'tick' => 5
                    ],
                    'yaxis' => $yAxis,
                    'yaxes' => empty($yAxisTicks) ? [[]] : $yAxisTicks,
                    'zoom' => [
                        'interactive' => true
                    ],
                    'pan' => [
                        'interactive' => true
                    ],
                    'crosshair' => [
                        'mode' => 'x'
                    ],
                    'rehoming' => [
                        'active' => true
                    ]
                ],
                'htmlOptions' => [
                    'class' => 'span10 left-column flotChart'
                ],
                'id' => $chartDivId
            )
        );

        Yii::app()->getClientScript()->registerScript(
            __CLASS__ . "#" . $chartDivId,
            'var wrapElement = $("#' . $chartDivId . '").parent();
            wrapElement.find("div.xAxisLabel").text("' . Yii::t('PatientModule.device', 'Дни') . '");'
            . 'var yAxisLabelElement = wrapElement.find("div.yAxisLabel");'
            . 'yAxisLabelElement.text("' . $yAxisLabel . '");'
            . 'yAxisLabelElement.css("margin-top", yAxisLabelElement.width() / 2 - 20);'
        );
    }

    private function _calculateYMin($yAxisTicks)
    {
        $yMin = empty($yAxisTicks) ? 0 : null;
        foreach ($yAxisTicks as $yAxisTick) {
            $firstValue = $yAxisTick['ticks'][0];
            if (is_array($firstValue)) {
                $firstValue = $firstValue[0];
            }
            if (is_null($yMin) || $firstValue < $yMin) {
                $yMin = $firstValue;
            }
        }
        return $yMin;
    }

    private function _createData($parameters, $seriesToYAxisMap)
    {
        reset($this->symbols);

        $dataArray = [];
        foreach ($parameters as $num => $parameter) {
            $label = $parameter->name;
            if ($parameter->unit instanceof MeasurementUnit) {
                $label .= ' [' . $parameter->unit->name . ']';
            }
            $dataArray[] = [
                'label'=> $label,
                'data'=> $this->_getNormalizeData($parameter),
                'yaxis' => isset($seriesToYAxisMap[$num]) ? $seriesToYAxisMap[$num] : 1,
                'lines' => [
                    'show' => true
                ],
                'points' => [
                    'show' => true,
                    'symbol' => current($this->symbols)
                ],
            ];
            next($this->symbols);
        }

        return $dataArray;
    }

    private function _getNormalizeData($parameter)
    {
        $dataFromDb = $parameter->getAllValues();
        $dataForGraph = [];
        $prevTimestamp = null;

        foreach ($dataFromDb as $row) {
            $value = is_numeric($row['value']) ? $row['value'] : null;

            $currentTimestamp = new RhDateTime($row['time']);
            if ($prevTimestamp instanceof RhDateTime
                && ($currentTimestamp->diff($prevTimestamp)->format('%a') > self::DAYS_OF_ABSENCE_DATA)) {
                $dataForGraph[] = [$prevTimestamp->getTimestamp() * self::MILLISECONDS_IN_SECOND, null];

            }
            $dataForGraph[] = [$currentTimestamp->getTimestamp() * self::MILLISECONDS_IN_SECOND, $value];
            $prevTimestamp = $currentTimestamp;
        }
        return $dataForGraph;
    }

    private function _calculateRangeLimits($data)
    {
        $minTimestamp = null;
        $maxTimestamp = null;

        foreach ($data as $item) {
            $startRangeTimestamp = array_shift($item['data'])[0];
            $endRangeTimestamp = array_pop($item['data'])[0];
            if (empty($minTimestamp) || $startRangeTimestamp < $minTimestamp) {
                $minTimestamp = $startRangeTimestamp;
            }

            if (empty($maxTimestamp) || $endRangeTimestamp > $maxTimestamp) {
                $maxTimestamp = $endRangeTimestamp;
            }
        }
        return [$minTimestamp, $maxTimestamp];
    }
}
