<?php
/* @var $patient Patient */
/* @var $bg_class string */
?>

<h4>
<?=$patient->human_readable_id;?>
</h4>
<div>
    <?php
    if (Yii::app()->user->checkAccess('viewPersonalData') && (!Yii::app()->getModule('patient')->depersonalization)) { ?>
        <?php $issetBirthDate = ($patient->birth_date instanceof RhDateTime); ?>
        <?=$patient->surname;?>
        <?=$patient->name;?>
        <?=$patient->middle_name . ($issetBirthDate ? ', ' : '');?>
        <?php if ($issetBirthDate) { ?>
            <?=$patient->birth_date->format('d');?>
            <?=$patient->birth_date->getMonthName();?>
            <?=$patient->birth_date->format('Y');?>
        <?php
        } ?>
    <?php
    } ?>
</div>
<div class ="status-line <?=$bg_class?>">
    <div class = "span4 status-message">
        <strong>
        <?php if ($patient->device->getLastState()):?>
        <?=Yii::t('general', 'Статус на');?>
        <?=$patient->device->getLastState()->create_time;?>
        <?php endif;?>
        </strong>
    </div>
    <div class = "span4 device-data">
        <div>
            <strong>
            <?=$patient->device->device_model;?> /
            <?=Yii::t('PatientModule.device', '№');?>:
            <?=$patient->device->serial_number;?>
            </strong>
        </div>
        <span>
        <?php
        if ($patient->device->implantation_date instanceof RhDateTime) { ?>
            <?=Yii::t('PatientModule.device', 'Дата имплантации');?>:
            <?=$patient->device->implantation_date->format('d');?>
            <?=$patient->device->implantation_date->getMonthName();?>
            <?=$patient->device->implantation_date->format('Y');?>
        <?php
        } else { ?>
            &nbsp;
        <?php
        } ?>
        </span>
    </div>
    <?php
    /** @var Research $research */
    if ($research = Research::model()->getOpenedByPatient($patient->uid)): ?>
        <div class="span8 patient-data-mc">
            <?=Yii::t("general", "Мед. центр")?>: <?=$research->medicalCenter->getDisplayName();?>
            <div class="patient-data-code span4">
            <?=Yii::t("general", "Код пациента")?>: <?=$research->patient->getDepersName();?>
            </div>
        </div>
    <?php
    endif;?>

</div>