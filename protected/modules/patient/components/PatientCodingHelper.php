<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 11.11.13
 * Time: 11:25
 *
 * Хелпер для транслитерирования строки согласно правилам кодирования инициалов пациентов
 */

class PatientCodingHelper
{

    protected static $codingTableRu = array(
        "a", "б", "в", "г", "д", "е", "ё", "ж", "з",
        "и", "й", "к", "л", "м", "н", "о", "п", "р",
        "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ",
        "ъ", "ы", "ь", "э", "ю", "я"
    );
    protected static $codingTableEn = array(
        "a", "b", "v", "g", "d", "e", "j", "z", "z",
        "i", "j", "k", "l", "m", "n", "o", "p", "r",
        "s", "t", "u", "f", "k", "t", "c", "s", "s",
        "", "y", "", "e", "y", "y"
    );
//    protected static $codingTableEn = array(
//        "a", "b", "v", "g", "d", "e", "jo", "zh", "z",
//        "i", "j", "k", "l", "m", "n", "o", "p", "r",
//        "s", "t", "u", "f", "kh", "ts", "ch", "sh", "sh",
//        "", "y", "", "e", "yu", "ya"
//    );

    public static function encodeLetters($str)
    {
        return mb_strtoupper(str_replace(self::$codingTableRu, self::$codingTableEn, mb_strtolower($str)));
    }
} 