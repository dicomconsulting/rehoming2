<?php

class m131019_114833_alter_implantation_date_column extends I18nDbMigration
{
    public function safeUp()
    {
        $this->alterColumn('{{device}}', 'implantation_date', 'DATE');
    }

    public function safeDown()
    {
        $this->alterColumn('{{device}}', 'implantation_date', 'DATETIME');
    }
}
