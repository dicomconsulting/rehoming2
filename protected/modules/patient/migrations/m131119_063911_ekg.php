<?php

class m131119_063911_ekg extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            "{{patient_ekg}}",
            array(
                "uid" => "INT(11) NOT NULL AUTO_INCREMENT",
                "patient_uid" => "INT(11) NOT NULL",
                "user_uid" => "INT(11) NOT NULL",
                "date_create" => "DATETIME NOT NULL",
                "ef_lv" => "INT(11) COMMENT 'ФВ ЛЖ (%)'",
                "edv" => "DECIMAL(10,2) COMMENT 'КДО (по Симпсону) (мл)'",
                "csr" => "DECIMAL(10,2) COMMENT 'КСО (мл)'",
                "regurgitation_degree" => "INT(11) COMMENT 'Степень регургитации на МК'",
                "vti" => "INT(11) COMMENT 'VTI (м/сек)'",
                "ivmd_intra_spont" => "INT(11) COMMENT 'IVMD intra (при спонтанном ритме) (мс)'",
                "ivmd_intra_bivent" => "INT(11) COMMENT 'IVMD intra (при бивентрикулярной стимуляции) (мс)'",
                "ivmd_inter_spont" => "INT(11) COMMENT 'IVMD inter (при спонтанном ритме) (мс)'",
                "ivmd_inter_bivent" => "INT(11) COMMENT 'IVMD inter (при бивентрикулярной стимуляции) (мс)'",
                "rv_pressure" => "INT(11) COMMENT 'Расчетное давление в ПЖ (мм.рт.ст.)'",
                "electric_delay" => "INT(11) COMMENT 'Электрическая задержка между правым и левым желудочком (мc)'",
                "PRIMARY KEY (uid)"
            ),
            "ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='Таблица ЭКГ пациентов'"
        );
    }

    public function safeDown()
    {
        $this->dropTable("{{patient_ekg}}");
    }
}
