<?php

class m131016_071647_create_status_message_table extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{status_message}}',
            array(
                'uid' => 'pk',
                'device_id' => 'INTEGER',
                'patient_number_message' => 'INTEGER',
                'finding' => 'VARCHAR(100)',
                'type' => 'TINYINT(1)',
                'is_actual' => 'TINYINT(1)',
                'create_time' => 'DATETIME'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );

        $this->addForeignKey(
            'fk_status_message_relation_device',
            '{{status_message}}',
            'device_id',
            '{{device}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_status_message_relation_device', '{{status_message}}');
        $this->dropTable('{{status_message}}');
    }
}
