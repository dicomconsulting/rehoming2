<?php

class m131106_130833_add_history_message_table extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{history_message}}',
            array(
                'time' => 'DATETIME',
                'text' => 'VARCHAR(250)',
                'device_id' => 'INTEGER(11)',
                'PRIMARY KEY (`time`,`device_id`, `text`)'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );

        $this->addForeignKey(
            'fk_history_message_relation_device',
            '{{history_message}}',
            'device_id',
            '{{device}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_history_message_relation_device',
            '{{history_message}}'
        );
        $this->dropTable('{{history_message}}');
    }
}
