<?php

class m131019_113513_alter_patient_columns extends I18nDbMigration
{
    public function safeUp()
    {
        $this->alterColumn('{{patient}}', 'birth_date', 'DATE');
        $this->renameColumn('{{patient}}', 'last_view_date', 'last_view_time');
    }

    public function safeDown()
    {
        $this->alterColumn('{{patient}}', 'birth_date', 'DATETIME');
        $this->renameColumn('{{patient}}', 'last_view_time', 'last_view_date');
    }
}
