<?php

class m131119_135625_clinical_data extends I18nDbMigration
{
    public function safeUp()
    {

        $this->createTable(
            "{{clinical_data}}",
            array(
                "uid" => "INT(11) NOT NULL AUTO_INCREMENT",
                "patient_uid" => "INT(11) NOT NULL",
                "user_uid" => "INT(11) NOT NULL",
                "date_create" => "DATETIME",
                "nyha_class" => "INT(11) COMMENT 'NYHA класс СН'",
                "termination_reason" => "TINYINT(1) COMMENT 'Причина окончания мониторинга'",
                "termination_date" => "DATE COMMENT 'Дата окончания мониторинга'",
                "icd10_uid" => "INT(11) NOT NULL COMMENT 'Основное заболевание'",
                "PRIMARY KEY (`uid`)"
            ),
            "ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci " .
            "COMMENT='Таблица клинических данных пациента'"
        );

        $this->addForeignKey(
            'fk_patient_uid_clinical_data',
            '{{clinical_data}}',
            'patient_uid',
            '{{patient}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );

        $this->createTable(
            "{{clinical_data_diagnosis}}",
            array(
                "clinical_data_uid" => "INT(11) NOT NULL",
                "icd10_uid" => "INT(11) NOT NULL",
                "PRIMARY KEY (`clinical_data_uid`, `icd10_uid`)"
            ),
            "ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci " .
            "COMMENT='Связь клинических данных с заболеваниями'"
        );

        $this->addForeignKey(
            'fk_clinical_data_uid_clinical_data_diagnosis',
            '{{clinical_data_diagnosis}}',
            'clinical_data_uid',
            '{{clinical_data}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_icd10_uid_clinical_data_diagnosis',
            '{{clinical_data_diagnosis}}',
            'icd10_uid',
            '{{icd10}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );

        $this->createTable(
            "{{clinical_data_therapy}}",
            array(
                "clinical_data_uid" => "INT(11) NOT NULL",
                "product_uid" => "INT(11) NOT NULL",
                "dose" => "decimal(10,2) DEFAULT NULL COMMENT 'Доза'",
                "measure" => "INT(11) DEFAULT NULL COMMENT 'Ед.измерения'",
                "PRIMARY KEY (`clinical_data_uid`, `product_uid`)"
            ),
            "ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci " .
            "COMMENT='Связь клинических данных с принимаемыми препаратами'"
        );

        $this->addForeignKey(
            'fk_clinical_data_uid_clinical_data_therapy',
            '{{clinical_data_therapy}}',
            'clinical_data_uid',
            '{{clinical_data}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );

        $this->createTable(
            "{{clinical_data_surgery}}",
            array(
                "clinical_data_uid" => "INT(11) NOT NULL",
                "surgery_uid" => "INT(11) NOT NULL",
                "date" => "date NOT NULL COMMENT 'дата операции'",
                "PRIMARY KEY (`clinical_data_uid`, `surgery_uid`, `date`)"
            ),
            "ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci " .
            "COMMENT='Связь клинических данных с проведенными операциями'"
        );

        $this->addForeignKey(
            'fk_clinical_data_uid_clinical_data_surgery',
            '{{clinical_data_surgery}}',
            'clinical_data_uid',
            '{{clinical_data}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_surgery_uid_clinical_data_surgery',
            '{{clinical_data_surgery}}',
            'surgery_uid',
            '{{surgery}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_clinical_data_uid_clinical_data_surgery", "{{clinical_data_surgery}}");
        $this->dropForeignKey("fk_surgery_uid_clinical_data_surgery", "{{clinical_data_surgery}}");
        $this->dropTable("{{clinical_data_surgery}}");

        $this->dropForeignKey("fk_clinical_data_uid_clinical_data_therapy", "{{clinical_data_therapy}}");
        $this->dropTable("{{clinical_data_therapy}}");

        $this->dropForeignKey("fk_clinical_data_uid_clinical_data_diagnosis", "{{clinical_data_diagnosis}}");
        $this->dropForeignKey("fk_icd10_uid_clinical_data_diagnosis", "{{clinical_data_diagnosis}}");
        $this->dropTable("{{clinical_data_diagnosis}}");

        $this->dropForeignKey("fk_patient_uid_clinical_data", "{{clinical_data}}");
        $this->dropTable("{{clinical_data}}");
    }
}
