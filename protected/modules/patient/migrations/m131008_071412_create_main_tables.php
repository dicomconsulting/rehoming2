<?php

class m131008_071412_create_main_tables extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{patient_group}}',
            array(
                'uid' => 'pk',
                'external_uid' => 'VARCHAR(100)',
                'name' => 'VARCHAR(100)',
                'comment' => 'TEXT',
                'UNIQUE `external_uid` (`external_uid`)'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );

        $this->createTable(
            '{{patient}}',
            array(
                'uid' => 'pk',
                'human_readable_id' => 'VARCHAR(50)',
                'name' => 'VARCHAR(100)',
                'middle_name' => 'VARCHAR(100)',
                'surname' => 'VARCHAR(100)',
                'external_uid' => 'VARCHAR(100)',
                'comment' => 'TEXT',
                'group_id' => 'INTEGER',
                'last_view_date' => 'DATETIME',
                'consent' => 'BOOLEAN',
                'birth_date' => 'DATETIME',
                'sex' => 'TINYINT(1)',
                'title' => 'VARCHAR(100)',
                'UNIQUE `human_readable_id` (`human_readable_id`)',
                'UNIQUE `external_uid` (`external_uid`)'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );
        $this->addForeignKey(
            'fk_patient_group_id_relation_patient_group_uid',
            '{{patient}}',
            'group_id',
            '{{patient_group}}',
            'uid',
            'RESTRICT',
            'CASCADE'
        );

        $this->createTable(
            '{{device}}',
            array(
                'uid' => 'pk',
                'serial_number' => 'VARCHAR(30)',
                'implantation_date' => 'DATETIME',
                'device_model' => 'VARCHAR(100)',
                'patient_id' => 'INTEGER',
                'transmitter_type' => 'VARCHAR(100)',
                'transmitter_serial_number' => 'VARCHAR(30)',
                'UNIQUE `serial_number` (`serial_number`)',
                'UNIQUE `transmitter_serial_number` (`transmitter_serial_number`)'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );
        $this->addForeignKey(
            'fk_device_patient_id_relation_patient_uid',
            '{{device}}',
            'patient_id',
            '{{patient}}',
            'uid',
            'RESTRICT',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_device_device_model_relation_device_model',
            '{{device}}',
            'device_model',
            '{{device_model}}',
            'name',
            'CASCADE',
            'CASCADE'
        );

        $this->createTable(
            '{{device_value}}',
            array(
                'device_id' => 'INTEGER',
                'parameter_id' => 'INTEGER',
                'value' => 'VARCHAR(100)',
                'create_time' => 'DATETIME',
                'PRIMARY KEY (`device_id`, `parameter_id`)'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );
        $this->addForeignKey(
            'fk_device_value_parameter_id_relation_parameter_uid',
            '{{device_value}}',
            'parameter_id',
            '{{parameter}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_device_value_device_id_relation_device_uid',
            '{{device_value}}',
            'device_id',
            '{{device}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );

        $this->createTable(
            '{{contact_person}}',
            array(
                'uid' => 'pk',
                'name' => 'VARCHAR(100)',
                'owner_id' => 'INTEGER',
                'type' => 'TINYINT(1)'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );
        $this->addForeignKey(
            'fk_contact_person_owner_id_relation_patient_uid',
            '{{contact_person}}',
            'owner_id',
            '{{patient}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );

        $this->createTable(
            '{{phone}}',
            array(
                'uid' => 'pk',
                'number' => 'VARCHAR(50)',
                'owner_id' => 'INTEGER',
                'type' => 'TINYINT(1)'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_patient_group_id_relation_patient_group_uid',
            '{{patient}}'
        );
        $this->dropForeignKey(
            'fk_device_patient_id_relation_patient_uid',
            '{{device}}'
        );
        $this->dropForeignKey(
            'fk_device_device_model_relation_device_model',
            '{{device}}'
        );
        $this->dropForeignKey(
            'fk_device_value_parameter_id_relation_parameter_uid',
            '{{device_value}}'
        );
        $this->dropForeignKey(
            'fk_device_value_device_id_relation_device_uid',
            '{{device_value}}'
        );
        $this->dropForeignKey(
            'fk_contact_person_owner_id_relation_patient_uid',
            '{{contact_person}}'
        );

        $this->truncateTable('{{patient_group}}');
        $this->truncateTable('{{patient}}');
        $this->truncateTable('{{device}}');
        $this->truncateTable('{{device_value}}');
        $this->truncateTable('{{contact_person}}');
        $this->truncateTable('{{phone}}');

        $this->dropTableWithI18n('{{patient_group}}');
        $this->dropTableWithI18n('{{patient}}');
        $this->dropTableWithI18n('{{device}}');
        $this->dropTableWithI18n('{{device_value}}');
        $this->dropTableWithI18n('{{contact_person}}');
        $this->dropTableWithI18n('{{phone}}');
    }
}
