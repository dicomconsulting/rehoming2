<?php

class m131119_105420_holter extends I18nDbMigration
{
    public function safeUp()
    {

        $this->createTable(
            "{{patient_holter}}",
            array(
                "uid" => "INT(11) NOT NULL AUTO_INCREMENT",
                "patient_uid" => "INT(11) NOT NULL",
                "user_uid" => "INT(11) NOT NULL",
                "date_create" => "DATETIME NOT NULL",
                "hr_max" => "INT(11) COMMENT 'ЧСС (макс.) (кол-во уд.в мин.)'",
                "hr_min" => "INT(11) COMMENT 'ЧСС (миним.) (кол-во уд.в мин.)'",
                "hr_avg" => "INT(11) COMMENT 'ЧСС (средняя) (кол-во уд.в мин.)'",
                "sv_extr" => "INT(11) COMMENT 'Экстрасистолы наджелудочковые'",
                "vent_extr" => "INT(11) COMMENT 'Экстрасистолы желудочковые'",
                "sv_runs" => "INT(11) COMMENT 'Пробежки наджелудочковые (> 5 комплексов)'",
                "vent_runs" => "INT(11) COMMENT 'Пробежки желудочковые (> 5 комплексов)'",
                "PRIMARY KEY (uid)"
            ),
            "ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='Данные для страницы Холтер'"
        );


    }

    public function safeDown()
    {
        $this->dropTable("{{patient_holter}}");
    }
}
