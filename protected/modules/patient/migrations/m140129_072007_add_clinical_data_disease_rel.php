<?php

class m140129_072007_add_clinical_data_disease_rel extends I18nDbMigration
{
    public function safeUp()
    {

        $this->createTable(
            '{{clinical_data_diagnosis_main}}',
            array(
                'clinical_data_uid' => "int(11) NOT NULL COMMENT 'Ид протокола'",
                'icd10_uid' => "int(11) NOT NULL COMMENT 'ид заболевания'",
                'PRIMARY KEY (`clinical_data_uid`,`icd10_uid`)',
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT="Клинический диагноз,' .
            ' отражаемый в клинических данных"'
        );

        $this->addForeignKey(
            'fk_clinical_data_uid_clinical_data_diagnosis_main',
            '{{clinical_data_diagnosis_main}}',
            'clinical_data_uid',
            '{{clinical_data}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_icd10_uid_clinical_data_diagnosis_main',
            '{{clinical_data_diagnosis_main}}',
            'icd10_uid',
            '{{icd10}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );

        $this->dropColumn("{{clinical_data}}", "icd10_uid");
    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_clinical_data_uid_clinical_data_diagnosis_main", "{{clinical_data_diagnosis_main}}");
        $this->dropForeignKey("fk_icd10_uid_clinical_data_diagnosis_main", "{{clinical_data_diagnosis_main}}");
        $this->dropTable("{{clinical_data_diagnosis_main}}");

        $this->addColumn("{{clinical_data}}", "icd10_uid", "int(11) DEFAULT NULL COMMENT 'Клинический диагноз'");
    }
}
