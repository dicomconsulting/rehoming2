<?php

class m131121_054342_alter_clinical_data extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn(
            "{{clinical_data}}",
            "height",
            "int(11) DEFAULT NULL COMMENT 'Рост'"
        );
        $this->addColumn(
            "{{clinical_data}}",
            "weight",
            "decimal(10,2) DEFAULT NULL COMMENT 'Вес'"
        );
    }

    public function safeDown()
    {
        $this->dropColumn("{{clinical_data}}", "height");
        $this->dropColumn("{{clinical_data}}", "weight");
    }
}
