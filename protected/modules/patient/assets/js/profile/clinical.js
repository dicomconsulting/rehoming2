
$(function() {
    //удаление болезни из этиологии
    $(document).on("click", ".remove_etiology_row", function() {
        $(this).parent().remove();
        return false;
    });

    //добавление операции
    $("#add_surgery").on("click", function() {
        renderNewSurgeryRow();

        return false;
    });

    //удаление операции
    $(document).on("click", "#surgeryContainer .remove_surgery", function() {
        removeSurgeryRow(this);

        return false;
    });

    //удаление лекарственного средства
    $(document).on("click", ".remove_pharm", function() {
        removePharmRow(this);

        return false;
    });

    function removePharmRow (link) {
        $(link).parent().parent().remove();
    }
});

function renderNewSurgeryRow () {

    var jqSurgerySelector = $("#surgeryTypeSelector");
    var jqSurgeryContainer = $("#surgeryContainer");

    var surgeryId = parseInt(jqSurgerySelector.find("option:selected").val());
    var surgeryName = jqSurgerySelector.find("option:selected").text();

    if (!surgeryId) {
        alert("Необходимо выбрать операцию");
        return false;
    }

    var rnd = Math.floor(Math.random() * 10000);

    var templData = {};
    templData.surgeryName = surgeryName;
    templData.rnd = rnd;
    templData.surgeryId = surgeryId;

    jqSurgeryContainer.append(Mustache.render(window.surgeryTemplate, templData));

    attachDatepickerToFields(jqSurgeryContainer);
}

function removeSurgeryRow (link) {
    $(link).parent().parent().parent().remove();
}

function processMainDesease(id, name) {
    var existingRow = $("input[class=main_disease][value="+id+"]");

    if (existingRow.length) {
        alert("Такое заболевание уже обозначено");
        return;
    }

    var jqContainer = $("#main_container");

    var templData = {};
    templData.id = id;
    templData.name = name;

    jqContainer.append(Mustache.render(window.icd10MainTemplate, templData));
}

function processComorbidities (id, name) {
    var container = $("#comorbidities_container");
    var existingRow = $("input[class=custom_etiology][value="+id+"]");

    if (existingRow.length) {
        alert("Такое заболевание уже обозначено");
        return false;
    }

    var templData = {};
    templData.id = id;
    templData.name = name;

    container.append(Mustache.render(window.etiologyTemplate, templData));

    return true;
}

/**
 * Обработка выбора лекарства из древовидного справочника
 * @returns {boolean}
 */
function processPharmSelected(id, name) {
    var jqPharmContainer = $("#otherPharmContainer");

    var templData = {};
    templData.pharmName = name;
    templData.pharmId = id;
    templData.measure = [];

    $.each(window.pharmUnits, function (i, el) {
        templData.measure[templData.measure.length] = {"id" : i, "val": el}
    });

    jqPharmContainer.append(Mustache.render(window.pharmProductTemplate, templData));

    return true;
}

function afterValidate(form) {

    //проверяем заполненность дозировки
    var doses = $(form).find("input.pharm_dose_input");
    var reg = /^\d+(,\d+)?$/i;

    var found = false;

    $.each(doses, function (i, el) {
        if (found) {
            return;
        }

        var val = $(el).val();
        if (val == "" || !reg.test(val)) {
            alert("Необходимо заполнить дозировку, например, 33,6");
            found = true;
            $('html, body').animate({
                scrollTop: $(el).offset().top
            }, 1000);
            $(el).focus();
        }
    });

    //проверяем заполненность даты операции
    var surgDates = $(form).find("input.surgery_date_input");

    $.each(surgDates, function (i, el) {
        if (found) {
            return;
        }

        var val = $(el).val();
        if (val == "") {
            alert("Необходимо заполнить дату операции");
            found = true;
            $('html, body').animate({
                scrollTop: $(el).offset().top
            }, 1000);
            $(el).focus();
        }
    });


    if (found) {
        return false;
    }

    return true;
}