/**
 * Created by Дамир on 22.11.13.
 */

$(function () {
    $("img.yellow_status").parent().addClass("yellow_status");
    $("img.red_status").parent().addClass("red_status");
});

function fixSearchPatientFilter(value) {
    url = window.location.href;
    currentPosition = url.indexOf('AdditionalFilter%5BshowPatientFilterValue%5D');
    if (currentPosition == -1) {
        p1 = url.indexOf('?');
        isFullPath = (url.indexOf('/patient/list/index') != -1);
        if (p1 == -1) {
            if (!isFullPath) {
                if (url.indexOf('/') == -1) {
                    url += '/'
                }
                url += 'patient/list/index';
            }
            url += '?AdditionalFilter%5BshowPatientFilterValue%5D=' + value
        } else {
            if (!isFullPath) {
                url = url.substr(0, p1 - 1) + '/patient/list/index?AdditionalFilter%5BshowPatientFilterValue%5D=' + value + '&' + url.substr(p1 + 1, 4024);
            } else {
                url = url.substr(0, p1 + 1) + 'AdditionalFilter%5BshowPatientFilterValue%5D=' + value + '&' + url.substr(p1 + 1, 4024);
            }
        }
    } else {
        // Уже есть значение
        parameterEnd = url.indexOf('&', currentPosition + 1);
        if (parameterEnd == -1) {
            url = url.substr(0, currentPosition) + 'AdditionalFilter%5BshowPatientFilterValue%5D=' + value + '&'
        } else {
            url = url.substr(0, currentPosition) + 'AdditionalFilter%5BshowPatientFilterValue%5D=' + value + url.substr(parameterEnd, 4024)
        }
    }

    window.location.href = url;
}

// Сброс фильтров пациента
function resetFilters()
{
    window.location.href = '/patient/list/index';
}