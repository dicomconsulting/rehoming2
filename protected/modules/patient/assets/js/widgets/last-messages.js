/**
 * Created by Дамир on 07.11.13.
 */

function handleAcknowledgeToggled(data){
    if (data.status !== undefined && data.status == "error") {
        alert(data.message);
        return;
    }

    if (data.acknowledged !== undefined) {
        var row = $("#sm_" + data.uid);
        row.toggleClass("actual-finding", data.acknowledged);
        var find = row.find(".status_icon_wrapper");
        find.toggleClass("hidden", !parseInt(data.active));
        if (data.acknowledged) {
            row.find("input[type=button]").val("Отменить уведомление");
        } else {
            row.find("input[type=button]").val("Уведомлен");
        }
    }
}
