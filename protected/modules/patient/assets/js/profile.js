/**
 * Created by Дамир on 27.11.13.
 */
$(function () {
    //подтверждение удаления
    $(".deleteLink").click(function () {
        var question = "Удалить запись от " + $(this).parent().parent().find("td.date_create_cell").eq(0).text() + "?";
        if (confirm(question)){
            return true;
        }
        return false;
    });

});