<?php

class PatientModule extends CWebModule
{
    private $assetsUrl;
    public  $depersonalization = false;

    /**
     * Initializes the module.
     */
    public function init()
    {
        parent::init();
        Yii::app()->getModule('admin');
        Yii::app()->getModule('option');
        Yii::app()->getModule('protocols');
        Yii::setPathOfAlias('patient', dirname(__FILE__));
        $this->setImport(
            array(
                'patient.components.*',
                'patient.models.*',
                'device.models.*',
                'patient.widgets.*',
                'patient.widgets.print.*',
                'patient.widgets.clinicalData.*',
                'patient.widgets.recording.*',
            )
        );
    }

    public function getAssetsUrl()
    {
        if ($this->assetsUrl === null) {
            $this->assetsUrl = Yii::app()->getAssetManager()->publish(
                Yii::getPathOfAlias('patient.assets'),
                false,
                -1,
                true
            );
        }
        return $this->assetsUrl;
    }

    public function beforeControllerAction($controller, $action)
    {
        Yii::app()->clientScript->registerCssFile($this->getAssetsUrl() . '/patient_card.css');
        return parent::beforeControllerAction($controller, $action);
    }


}
