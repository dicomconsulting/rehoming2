<?php

/**
 * This is the model class for table "{{patient_ekg}}".
 *
 * The followings are the available columns in table '{{patient_ekg}}':
 * @property integer $uid
 * @property integer $patient_uid
 * @property integer $user_uid
 * @property RhDateTime $date_create
 * @property integer $ef_lv
 * @property string $edv
 * @property string $csr
 * @property integer $regurgitation_degree
 * @property integer $vti
 * @property integer $ivmd_intra_spont
 * @property integer $ivmd_intra_bivent
 * @property integer $ivmd_inter_spont
 * @property integer $ivmd_inter_bivent
 * @property integer $rv_pressure
 * @property integer $electric_delay
 *
 * @property User $user
 * @property Patient $patient
 *
 *
 */
class PatientEkg extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PatientEkg the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{patient_ekg}}';
    }

    public function init()
    {
        Yii::app()->getModule("admin");

        if ($this->scenario == "insert") {
            $this->date_create = new RhDateTime();
        }

        parent::init();
    }

    protected function beforeSave()
    {
        if (is_array($this->date_create)) {
            $this->date_create = $this->date_create['date'] . " " . $this->date_create['time'] . ":00";
        }

        return parent::beforeSave();
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('patient_uid, user_uid, date_create', 'required'),
            array(
                'patient_uid, user_uid, ef_lv, regurgitation_degree, vti, ivmd_intra_spont, ivmd_intra_bivent, ' .
                'ivmd_inter_spont, ivmd_inter_bivent, rv_pressure, electric_delay',
                'numerical',
                'integerOnly' => true
            ),
            array('edv, csr', 'length', 'max' => 10),
            array('edv, csr',
                'CustomFloatValidator',
                'message'=>'Поле "{attribute}" должно быть заполнено числом, например "36,6"'
            ),
            array('date_create', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_uid'),
            'patient' => array(self::BELONGS_TO, 'Patient', 'patient_uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid' => 'Uid',
            'patient_uid' => 'Patient Uid',
            'user_uid' => 'User Uid',
            'date_create' => 'Время получения данных',
            'ef_lv' => 'ФВ ЛЖ (%)',
            'edv' => 'КДО (по Симпсону) (мл)',
            'csr' => 'КСО (мл)',
            'regurgitation_degree' => 'Степень регургитации на МК',
            'vti' => 'VTI (м/сек)',
            'ivmd_intra_spont' => 'IVMD intra (при спонтанном ритме) (мс)',
            'ivmd_intra_bivent' => 'IVMD intra (при бивентрикулярной стимуляции) (мс)',
            'ivmd_inter_spont' => 'IVMD inter (при спонтанном ритме) (мс)',
            'ivmd_inter_bivent' => 'IVMD inter (при бивентрикулярной стимуляции) (мс)',
            'rv_pressure' => 'Расчетное давление в ПЖ (мм.рт.ст.)',
            'electric_delay' => 'Электрическая задержка между правым и левым желудочком (мc)',
        );
    }

    public function behaviors()
    {
        return array(
            'EAdvancedArBehavior' => array(
                'class' => 'application.extensions.EAdvancedArBehavior'
            ),
            'FloatDelimiterBehavior' => ['class' => 'FloatDelimiterBehavior'],
            'RhDateTimeBehavior' => array(
                'class' => 'application.extensions.RhDateTimeBehavior'
            ),
        );
    }

    /**
     * Возвращает последние данные по ЭКГ по пациенту
     *
     * @param Patient $patient
     * @return CActiveRecord
     */
    public static function getLatestDataByPatient(Patient $patient)
    {
        $criteria = new CDbCriteria();
        $criteria->condition = "patient_uid = :patient_uid";
        $criteria->params = array(":patient_uid" => $patient->uid);
        $criteria->order = "date_create DESC";

        return self::model()->find($criteria);
    }

    public function defaultScope()
    {
        return array(
            'order'=>"t.date_create DESC",
        );
    }
}
