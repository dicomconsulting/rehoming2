<?php

/**
 * This is the model class for table "{{contact_person}}".
 *
 * The followings are the available columns in table '{{contact_person}}':
 * @property integer $uid
 * @property string $name
 * @property integer $owner_id
 * @property integer $type
 */
abstract class AbstractContactPerson extends CActiveRecord
{
    /**
     * Return type's value of object
     *
     * @return int
     */
    public function getType()
    {
        return static::TYPE_ID;
    }

    /**
     * Add phone number to contact person
     *
     * @param string $number
     */
    public function setPhone($number)
    {
        $phone = new Phone();
        $phone->number = $number;
        $phone->type = $this->getType();
        $this->type = $this->getType();
        $this->phone = $phone;
    }

    /**
     * Return default find's conditions
     *
     * @return array
     */
    public function defaultScope()
    {
        return array(
            'condition'=>"type = " . $this->getType()
        );
    }

    /**
     * The associated database table name.
     *
     * @return string
     */
    public function tableName()
    {
        return '{{contact_person}}';
    }

    public function behaviors()
    {
        return array(
            'EAdvancedArBehavior' => array(
                'class' => 'application.extensions.EAdvancedArBehavior'
            )
        );
    }

    public function primaryKey()
    {
        return 'uid';
    }

    /**
     * Return relational rules.
     *
     * @return array
     */
    public function relations()
    {
        return array(
            'phone'=>array(self::HAS_ONE, 'Phone', 'owner_id', 'condition'=>'phone.type = '. $this->getType()),
        );
    }
}
