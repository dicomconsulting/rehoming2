<?php

Yii::import("device.models.Device");
Yii::import("device.models.DeviceState");
Yii::import("admin.models.*");
Yii::import("protocols.models.Research");

/**
 * This is the model class for table "{{patient}}".
 *
 * The followings are the available columns in table '{{patient}}':
 * @property integer $uid
 * @property string $human_readable_id
 * @property string $name
 * @property string $middle_name
 * @property string $surname
 * @property string $external_uid
 * @property string $comment
 * @property integer $group_id
 * @property integer $user_group_id
 * @property null|RhDateTime $last_view_time
 * @property integer $consent
 * @property null|RhDateTime $birth_date
 * @property null|RhDateTime $active_since_date
 * @property integer $sex
 * @property string $title
 * @property Device $device
 * @property Phone[] $phones
 * @property ContactPerson[] $contact_persons
 * @property FamilyDoctor[] family_doctors
 * @property PatientGroup group
 *
 * @mixin UserGroupSeparationBehavior
 */
class Patient extends CActiveRecord
{

    const UNKNOWN = 0;
    const MALE = 1;
    const FEMALE = 2;
    /**
     * Поля для поиска
     */
    public $device_search;
    public $implantation_date_search;
    public $acknowledge_search;
    public $doctor_id;
    public $combo_search;
    public $lastMessageDateSearch;
    public $showPatientFilterValue;


    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param  string $className active record class name.
     * @return Patient
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * The associated database table name.
     *
     * @return string
     */
    public function tableName()
    {
        return '{{patient}}';
    }

    public function addPhone(\Phone $phone)
    {
        if (array_key_exists($phone->number, $this->phones) == false) {
            $phones                 = $this->phones;
            $phones[$phone->number] = $phone;
            $this->phones           = $phones;
        }
    }

    /**
     * Validation rules for model attributes.
     *
     * @return array
     */
    public function rules()
    {
        return array(
            ['group_id, consent', 'numerical', 'integerOnly' => true],
            ['human_readable_id', 'required'],
            ['human_readable_id', 'length', 'max' => 50],
            ['name, surname, external_uid, title', 'length', 'max' => 100],
            ['comment, last_view_time, birth_date', 'safe'],
            [
                'uid, human_readable_id, name, surname, external_uid, group_id, last_view_time, consent, birth_date, ' .
                'title, comment, device_search,implantation_date_search,combo_search, lastMessageDateSearch, showPatientFilterValue',
                'safe',
                'on' => 'search'
            ],
        );
    }

    /**
     * Return relational rules.
     *
     * @return array
     */
    public function relations()
    {
        return array(
            'device'          => [self::HAS_ONE, 'Device', 'patient_id'],
            'phones'          => [self::HAS_MANY, 'Phone', 'owner_id', 'condition' => 'phones.type = 0', 'index' => 'number'],
            'contact_persons' => [self::HAS_MANY, 'ContactPerson', 'owner_id', 'index' => 'name'],
            'family_doctors'  => [self::HAS_MANY, 'FamilyDoctor', 'owner_id', 'index' => 'name'],
            'group'           => [self::BELONGS_TO, 'PatientGroup', 'group_id'],
            'research'        => [self::HAS_MANY, Research::class, 'patient_uid']
        );
    }

    /**
     *  Customized attribute labels.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'uid'               => 'Uid',
            'human_readable_id' => 'ИД пациента',
            'name'              => 'Name',
            'surname'           => 'Surname',
            'external_uid'      => 'External Uid',
            'comment'           => 'Комментарий',
            'group_id'          => 'Группа пациента',
            'last_view_time'    => 'Last View Date',
            'consent'           => 'Consent',
            'birth_date'        => 'Birth Date',
            'title'             => 'Title',
        );
    }

    public function updateLastViewTime()
    {
        $lastViewTime = new RhDateTime();
        $this->updateByPk(
            $this->uid,
            ['last_view_time' => $lastViewTime->format('Y-m-d H:i:s')]
        );
    }

    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('uid', $this->uid);
        $criteria->compare('human_readable_id', trim($this->human_readable_id), true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('surname', $this->surname, true);
        $criteria->compare('external_uid', $this->external_uid, true);
        $criteria->compare('group_id', $this->group_id);
        $criteria->compare('consent', $this->consent);
        $criteria->compare('birth_date', $this->birth_date, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('t.comment', $this->comment, true);
        if ($this->last_view_time) {
            $criteria->addCondition('last_view_time >= "' . date('Y-m-d 00:00:00', strtotime($this->last_view_time)) . '"');
            $criteria->addCondition('last_view_time <= "' . date('Y-m-d 23:59:59', strtotime($this->last_view_time)) . '"');
        }

        //поиск по устройству
        $criteria->compare('device.device_model', trim($this->device_search), true, "OR");
        $criteria->compare('device.serial_number', trim($this->device_search), true, "OR");

        //поиск по нескольким полям
        $criteria->compare('human_readable_id', trim($this->combo_search), true, "OR");
        $criteria->compare('device.device_model', trim($this->combo_search), true, "OR");
        $criteria->compare('device.serial_number', trim($this->combo_search), true, "OR");

        if (Yii::app()->user->checkAccess('viewPersonalData')) {
            $criteria->compare('t.name', trim($this->combo_search), true, "OR");
            $criteria->compare('t.surname', trim($this->combo_search), true, "OR");
        }


        if ($this->implantation_date_search) {
            $criteria->compare('device.implantation_date', date("Y-m-d", strtotime($this->implantation_date_search)));
        }

        if ($this->showPatientFilterValue) {
            // Фильтрация пациента по исследованию
            $condition = '(SELECT patient_uid FROM {{research}} as research WHERE research.date_begin is not null and research.date_begin <= "' . date('Y-m-d') . '" and (research.date_end is null or research.date_end>= "' . date('Y-m-d') . '" )) ';
            if ($this->showPatientFilterValue == 1) {
                $criteria->addCondition(' t.uid in  ' . $condition);
            } elseif ($this->showPatientFilterValue == 2) {
                $criteria->addCondition(' t.uid not in  ' . $condition);
            } elseif ($this->showPatientFilterValue == 3) {
                $condition = '(SELECT patient_uid FROM {{research}} as research )';
                $this->getDbCriteria()->addCondition(' t.uid in  ' . $condition);
            } elseif ($this->showPatientFilterValue == 4) {
                $condition = '(SELECT patient_uid FROM {{research}} as research )';
                $this->getDbCriteria()->addCondition(' t.uid not in  ' . $condition);
            }
        }

        //подключаем статусы
        $criteria->scopes = ['device_last_status'];

        if ($this->acknowledge_search) {
            $criteria->compare('status_message.active', 1);
            $criteria->compare('status_message.acknowledged', 0, true, "OR");
        }

        // поиск по последнему сообщению устройства
        if ($this->lastMessageDateSearch) {
            $criteria->scopes[] = 'device_last_state';
            $criteria->addCondition('device_last_state.max_create_time >= "' . date('Y-m-d 00:00:00', strtotime($this->lastMessageDateSearch)) . '"');
            $criteria->addCondition('device_last_state.max_create_time <= "' . date('Y-m-d 23:59:59', strtotime($this->lastMessageDateSearch)) . '"');
        }

        $dataProvider = new CActiveDataProvider($this, array(
            'criteria'   => $criteria,
            'pagination' => ['pageSize' => Yii::app()->user->getState('pageSize')]
        ));


        $dataProvider->sort = array(
            "defaultOrder" => "human_readable_id",
            'attributes'   => array(
                'comment'                  => array(
                    'asc'  => 'ISNULL(t.comment) ASC, t.comment',
                    'desc' => 'ISNULL(t.comment) ASC, t.comment DESC',
                ),
                'human_readable_id',
                'implantation_date_search' => array(
                    'asc'  => 'ISNULL(device.implantation_date) ASC, device.implantation_date',
                    'desc' => 'device.implantation_date DESC',
                ),
                'device_search'            => array(
                    'asc'  => 'ISNULL(device.device_model) ASC, device.device_model',
                    'desc' => 'ISNULL(device.device_model) ASC, device.device_model DESC',
                ),
                'group_id'                 => array(
                    'asc'  => 'group.name',
                    'desc' => 'group.name DESC',
                ),
                'last_view_time'           => array(
                    'asc'  => 'ISNULL(last_view_time) ASC, last_view_time',
                    'desc' => 'last_view_time DESC',
                ),
            ),
        );

        return $dataProvider;
    }

    public function behaviors()
    {
        return array(
            'RhDateTimeBehavior'  => array(
                'class' => 'application.extensions.RhDateTimeBehavior'
            ),
            'EAdvancedArBehavior' => array(
                'class' => 'application.extensions.EAdvancedArBehavior'
            ),
            'UserGroupSeparation' => array(
                'class' => 'admin.components.UserGroupSeparationBehavior'
            )
        );
    }

    public function existDeviceData()
    {
        return !empty($this->device->lastState);
    }

    public function scopes()
    {
        return [
            'device_last_status' => [ // Статус последний устройства по алертам
                'with' => [
                    'device' => [
                        "join"  => "LEFT JOIN rh_alert_message as status_message " .
                            "ON status_message.device_uid = device.uid AND status_message.date_end IS NULL",
                        "group" => "t.uid",
                    ],
                    'group'
                ],
            ],
            'device_last_state'  => [ // Последний статус устройства
                'with' => [
                    'device' => [
                        'join' => 'LEFT JOIN (SELECT device_state_sub_select.*, max(device_state_sub_select.create_time) as max_create_time FROM {{device_state}} device_state_sub_select GROUP BY device_state_sub_select.device_id) device_last_state' .
                            ' ON device_last_state.device_id=device.uid',
                    ],
                ],
            ]

        ];
    }

    public function onlyAssigned($doctorId = null, $alias = null)
    {
        if (Yii::app()->user->isGuest) {
            $doctorId = 0;
        }

        if (!isset($doctorId)) {
            $user     = User::model()->findByPk(Yii::app()->user->id);
            $doctorId = $user->doctor_id;
        }

        if (!empty($doctorId)) {
            $alias = empty($alias) ? $this->getTableAlias(false) : $alias;

            if (empty($alias)) {
                $join = " JOIN {{doctor_patient_group_relation}} dpgr ON dpgr.patient_group_id = group_id";
            } else {
                $join = " JOIN {{doctor_patient_group_relation}} dpgr ON dpgr.patient_group_id = {$alias}.group_id";
            }

            $condition = 'dpgr.doctor_id = :doctor_id'
                . ' AND (dpgr.access_level = ' . PatientGroupAccess::ACCESS_FULL
                . ' OR dpgr.access_level = ' . PatientGroupAccess::ACCESS_READ . ')';

            $this->getDbCriteria()->mergeWith(
                [
                    'join'      => $join,
                    'condition' => $condition,
                    'params'    => [':doctor_id' => $doctorId]
                ]
            );
        }

        return $this;
    }

    /**
     * Только пациенты у которых есть исследование
     */
    public function onlyResearchExists()
    {
        $condition = '(SELECT patient_uid FROM {{research}} as research )';
        $this->getDbCriteria()->addCondition(' t.uid in  ' . $condition);
        return $this;
    }

    /**
     * Возвращает деперсонифицированное обозначение пациента
     *
     * @return bool|string
     */
    public function getDepersName()
    {
        $research = Research::model()->getOpenedByPatient($this->uid);
        if ($research) {
            return $research->medicalCenter->code . " " . $this->getCode() . " " . $research->getScreeningNumber();
        } else {
            return false;
        }
    }

    /**
     * Возвращает закодированное имя пациента, для отображения в обезличенном виде
     * Сначала пытается вытащить из протокола включения, иначе из инициалов
     *
     * @return string
     */
    public function getCode()
    {
        Yii::app()->getModule('protocols');

        if ($research = Research::model()->getOpenedByPatient($this->uid)) {
            return $research->patient_code;
        }

        return PatientCodingHelper::encodeLetters(
            mb_substr($this->surname, 0, 1)
            . mb_substr($this->name, 0, 1)
            . mb_substr($this->middle_name, 0, 1)
        );
    }

    public function getAgeYears($date = null)
    {
        $birthDate = new DateTime($this->birth_date);
        $dateObj   = $birthDate->diff(new DateTime($date ? $date : 'now'));
        $years     = $dateObj->y;
        return $years;
    }

    /**
     * Сформировать frame для вывода перс данных
     *
     * @param $code
     * @param $object
     * @return string
     */
    protected function formPersonalFrame($code, $object)
    {
        $url       = Yii::app()->user->getUser()->settings['depersonalizationUrl'] . 'code=' . $code . '&get=' . $object;
        $frameHtml = '<iframe src="' . $url . '" class="depersonalizationFrame patient' . ucfirst($object) . 'Frame" frameborder="0"  scrolling="no" seamless="seamless"></iframe>';
        return $frameHtml;
    }

    public function getFioIframe()
    {
        return $this->formPersonalFrame($this->human_readable_id, 'fio');
    }

    public function getPhoneIframe()
    {
        return $this->formPersonalFrame($this->human_readable_id, 'phone');
    }

    public function getProfileIframe()
    {
        return $this->formPersonalFrame($this->human_readable_id, 'fullProfile');
    }

    public function getFioText()
    {
        if (Yii::app()->getModule('patient')->depersonalization) {
            return $this->getFioIframe();
        }
        return $this->surname . ' ' . $this->name . $this->middle_name;

    }

    public function getPhoneText()
    {
        if (Yii::app()->getModule('patient')->depersonalization) {
            return $this->getPhoneIframe();
        }
        $returnValue = '';
        foreach ($this->phones as $phone) {
            $returnValue .= $phone->number . ' ';
        }
        return $returnValue;
    }
}
