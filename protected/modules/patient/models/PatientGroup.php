<?php
Yii::import('patient.models.Patient');

/**
 * This is the model class for table "{{patient_group}}".
 *
 * The followings are the available columns in table '{{patient_group}}':
 * @property integer $uid
 * @property string $name
 * @property string $external_uid
 * @property string $comment
 * @property integer $user_group_id
 *
 * @property Patient[] patients
 * @property integer patientCount
 */
class PatientGroup extends CActiveRecord
{
    /**
     * The associated database table name.
     *
     * @return string
     */
    public function tableName()
    {
        return '{{patient_group}}';
    }

    /**
     * Return relational rules.
     *
     * @return array
     */
    public function relations()
    {
        return array(
            'patients'     => [
                self::HAS_MANY,
                'Patient',
                'group_id'
            ],
            'patientCount' => [
                self::STAT,
                'Patient',
                'group_id'
            ],
            'userGroup'    => [
                self::BELONGS_TO,
                UserGroup::class,
                'user_group_id'
            ]
        );
    }


    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param  string $className active record class name.
     * @return Patient the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        return array(
            'EAdvancedArBehavior' => array(
                'class' => 'application.extensions.EAdvancedArBehavior'
            ),
            'UserGroupSeparation' => array(
                'class' => 'admin.components.UserGroupSeparationBehavior'
            )
        );
    }

    public function primaryKey()
    {
        return 'uid';
    }

    public function search()
    {
        return new CActiveDataProvider(
            $this,
            array(
                'criteria'   => new CDbCriteria,
                'pagination' => [
                    'pageSize' => Yii::app()->user->getState('pageSize')
                ]
            )
        );
    }
}
