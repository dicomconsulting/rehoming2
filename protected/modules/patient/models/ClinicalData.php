<?php

/**
 * This is the model class for table "{{clinical_data}}".
 *
 * The followings are the available columns in table '{{clinical_data}}':
 * @property integer $uid
 * @property integer $patient_uid
 * @property integer $user_uid
 * @property RhDateTime $date_create
 * @property integer $nyha_class
 * @property integer $termination_reason
 * @property integer $height
 * @property double $weight
 * @property string $termination_date
 *
 * The followings are the available model relations:
 * @property Patient $patient
 * @property Icd10[] $icd10
 * @property Icd10[] $icd10_main
 * @property Surgery[] $surgery
 * @property ClinicalDataSurgery[] $surgeryRel
 * @property ClinicalDataTherapy[] $therapy
 * @property NyhaClass $nyha
 */
class ClinicalData extends CActiveRecord
{

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ClinicalData the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{clinical_data}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('patient_uid, user_uid, date_create', 'required'),
            array(
                'patient_uid, user_uid, nyha_class, termination_reason, height',
                'numerical',
                'integerOnly' => true
            ),
            array('weight',
                'CustomFloatValidator',
                'message'=>'Поле "{attribute}" должно быть заполнено числом, например "36,6"'
            ),
            array('uid, date_create, termination_date, icd10, icd10_main, therapy, surgeryRel, height, weight', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'patient' => array(self::BELONGS_TO, 'Patient', 'patient_uid'),
            'icd10' => array(self::MANY_MANY, 'Icd10', '{{clinical_data_diagnosis}}(clinical_data_uid, icd10_uid)'),
            'icd10_main' => array(
                self::MANY_MANY,
                'Icd10',
                '{{clinical_data_diagnosis_main}}(clinical_data_uid, icd10_uid)'
            ),
            'surgeryRel'=>array(self::HAS_MANY, 'ClinicalDataSurgery', 'clinical_data_uid'),
            'therapy' => array(self::HAS_MANY, 'ClinicalDataTherapy', 'clinical_data_uid'),
            'user' => array(self::BELONGS_TO, 'User', 'user_uid'),
            'nyha' => array(self::BELONGS_TO, 'NyhaClass', 'nyha_class'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid' => 'Uid',
            'patient_uid' => 'Patient Uid',
            'user_uid' => 'User Uid',
            'icd10_uid' => Yii::t("patient", 'Клинический диагноз'),
            'icd10_main' => Yii::t("patient", "Клинический диагноз"),
            'date_create' => Yii::t("patient", 'Дата и время создания'),
            'nyha_class' => Yii::t("patient", 'NYHA класс СН'),
            'termination_reason' => Yii::t("patient", 'Причина окончания мониторинга'),
            'termination_date' => Yii::t("patient", 'Дата окончания мониторинга'),
            'height' => Yii::t("patient", 'Рост (см)'),
            'weight' => Yii::t("patient", 'Вес (кг)'),
        );
    }

    public function behaviors()
    {
        return array(
            'EAdvancedArBehavior' => array(
                'class' => 'application.extensions.EAdvancedArBehavior'
            ),
            'RhDateTimeBehavior' => array(
                'class' => 'application.extensions.RhDateTimeBehavior'
            ),
            'FloatDelimiterBehavior' => ['class' => 'FloatDelimiterBehavior']
        );
    }


    public function init()
    {
        Yii::app()->getModule("admin");
        Yii::app()->getModule("protocols");

        if ($this->scenario == "insert") {
            $this->date_create = new RhDateTime();
        }
        parent::init();
    }

    protected function beforeSave()
    {
        if (is_array($this->date_create)) {
            $this->date_create = $this->date_create['date'] . " " . $this->date_create['time'] . ":00";
        }

        return parent::beforeSave();
    }

    /**
     * Возвращает возможные варианты NYHA класса
     *
     * @return array
     */
    public static function getNyhaOptions()
    {
        return NyhaClass::model()->withI18n(Yii::app()->language)->findAll();
    }

    /**
     * Возвращает возможные варианты NYHA класса
     *
     * @return array
     */
    public static function getTerminationReason()
    {
        return array(
            0 => "не применимо",
            1 => "Эксплантация",
            2 => "Смерть",
        );
    }

    /**
     * Возвращает последние данные по ЭКГ по пациенту
     *
     * @param Patient $patient
     * @return ClinicalData
     */
    public static function getLatestDataByPatient(Patient $patient)
    {
        $criteria = new CDbCriteria();
        $criteria->condition = "patient_uid = :patient_uid";
        $criteria->params = array(":patient_uid" => $patient->uid);
        $criteria->order = "date_create DESC";

        return self::model()->find($criteria);
    }

    public function defaultScope()
    {
        return array(
            'order'=>"t.date_create DESC",
        );
    }

    /**
     * Пытается загрузить данные либо из предыдущих клинических данных, либо из существующего протокола включения
     *
     * @throws CException
     */
    public function loadDefaultData()
    {
        if (!$this->patient_uid) {
            throw new CException("Необходимо указать свойство patient_id");
        }

        if ($latestData = $this->getLatestDataByPatient($this->patient)) {
            //сначала пытаемся получить данные из предыдущей записи
            $this->attributes = $latestData->attributes;
            $this->uid = null;

            $this->therapy = $latestData->therapy;
            $this->surgeryRel = $latestData->surgeryRel;
            $this->icd10 = $latestData->icd10;

            $this->init();

        } else {
            //иначе вытаскиваем данные из протокола включения
            $research = Research::model()->getOpenedByPatient($this->patient_uid);

            if ($research) {
                foreach ($research->getAllProtocols() as $protocol) {
                    if ($protocol->protocol_dictionary_uid == ProtocolDictionary::PROTOCOL_ENROLMENT) {
                        /** @var ProtocolEnrolment $enrolmentProtocol */
                        $enrolmentProtocol = $protocol->protocolEnrolment;
                    }
                }

                if (isset($enrolmentProtocol)) {
                    $this->nyha_class = $enrolmentProtocol->patient_nyha;
                    $this->icd10_main = $enrolmentProtocol->diagnosis;
                    $this->weight = $enrolmentProtocol->patient_weight;
                    $this->height = $enrolmentProtocol->patient_height;

                    $therapyArr = array();
                    foreach ($enrolmentProtocol->pharm as $row) {
                        $therapy = new ClinicalDataTherapy();
                        $therapy->dose = $row->dose;
                        $therapy->measure = $row->measure;
                        $therapy->product_uid = $row->pharm_product_uid;

                        $therapyArr[] = $therapy;
                    }
                    $this->therapy = $therapyArr;

                    $surgArr = array();
                    foreach ($enrolmentProtocol->surgeryRel as $row) {
                        $surgRel = new ClinicalDataSurgery();
                        $surgRel->surgery_uid = $row->surgery_uid;
                        $surgRel->date = $row->date;
                        $surgArr[] = $surgRel;
                    }
                    $this->surgeryRel = $surgArr;
                }
            }
        }
    }

    /**
     * @param Patient $patient
     * @param $parameter
     * @return array
     */
    public function getHistoryByPatient(Patient $patient, $parameter)
    {
        $criteria = new CDbCriteria();
        $criteria->order = "t.date_create DESC";
        $criteria->condition = "t.patient_uid = :patient_uid";
        $criteria->params = [':patient_uid' => $patient->uid];
        return $this->findAllByAttributes([], $criteria);
    }

    /**
     * Возвращает название параметра по имени, будь то атрибут класса или связь
     *
     * @param $parameter
     * @return string
     * @throws CException
     */
    public function getParameterName($parameter)
    {
        if (array_key_exists($parameter, $this->getAttributes())) {
            return $this->getAttributeLabel($parameter);
        }

        $relNames = $this->getRelNames();
        if (array_key_exists($parameter, $relNames)) {
            return $relNames[$parameter];
        } else {
            throw new CException("Parameter name '{$parameter}' not found. Please determine parameters names.");
        }
    }

    /**
     * @return array
     */
    protected function getRelNames()
    {
        return [
            'icd10' => Yii::t("patient", "Сопутствующие хронические заболевания"),
            'icd10_main' => Yii::t("patient", "Клинический диагноз"),
            'surgeryRel' => Yii::t("patient", "Перенесенные операции"),
            'therapy' => Yii::t("patient", "Антиаритмическая терапия"),
            'nyha' => Yii::t("patient", "NYHA класс СН"),
        ];
    }
}
