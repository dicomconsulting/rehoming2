<?php

/**
 * This is the model class for table "{{phone}}".
 *
 * The followings are the available columns in table '{{phone}}':
 * @property integer $uid
 * @property string  $number
 * @property integer $owner_id
 * @property integer $type
 */
class Phone extends CActiveRecord
{
    /**
     * Default phone's type
     *
     * @var integer
     */
    public $type = 0;

    /**
     * The associated database table name.
     *
     * @return string
     */
    public function tableName()
    {
        return '{{phone}}';
    }

    public function behaviors()
    {
        return array(
            'EAdvancedArBehavior' => array(
                'class' => 'application.extensions.EAdvancedArBehavior'
            )
        );
    }

    public function primaryKey()
    {
        return 'uid';
    }
}
