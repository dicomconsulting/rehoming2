<?php

/**
 * This is the model class for table "{{contact_person}}".
 *
 * The followings are the available columns in table '{{contact_person}}':
 * @property integer $uid
 * @property string $name
 * @property integer $owner_id
 * @property integer $type
 * @property Phone $phone
 */
class FamilyDoctor extends AbstractContactPerson
{
    const TYPE_ID = 2;

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param  string  $className active record class name.
     * @return Patient the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
