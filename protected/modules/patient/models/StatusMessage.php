<?php
Yii::import('device.models.*');

/**
 * This is the model class for table "{{patient_group}}".
 *
 * The followings are the available columns in table '{{patient_group}}':
 * @property integer $uid
 * @property integer $device_id
 * @property integer $patient_number_message
 * @property string $finding
 * @property string $type
 * @property string $is_actual
 * @property RhDateTime $create_time
 */
class StatusMessage extends CActiveRecord
{
    const STATUS_GREEN = 0;

    const STATUS_YELLOW = 1;

    const STATUS_RED = 2;

    private $statusNameToTypeMap = [
        self::STATUS_GREEN => 'green',
        self::STATUS_YELLOW => 'yellow',
        self::STATUS_RED => 'red'
    ];

    /**
     * The associated database table name.
     *
     * @return string
     */
    public function tableName()
    {
        return '{{status_message}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param  string  $className active record class name.
     * @return StatusMessage the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'device' => array(self::BELONGS_TO, 'Device', 'device_id'),
        );
    }

    public function primaryKey()
    {
        return 'uid';
    }

    public function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->isNewRecord) {
                $this->patient_number_message = new CDbExpression(
                    '(SELECT count(*)+1 FROM {{status_message}} as alias'
                    . ' WHERE alias.device_id = :device_id AND alias.type = :type)',
                    array('device_id' => $this->device_id, 'type' => $this->type)
                );
                $this->is_actual = 1;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Return status type id by status message type name.
     *
     * @param string $statusName
     * @return integer
     */
    public function getStatusTypeByName($statusName)
    {
        return array_search(strtolower($statusName), $this->statusNameToTypeMap);
    }

    /**
     * Return status message name by status type id.
     *
     * @param integer $typeId
     * @return boolean
     */
    public function getStatusNameByType($typeId)
    {
        if (array_key_exists($typeId, $this->statusNameToTypeMap)) {
            return $this->statusNameToTypeMap[$typeId];
        } else {
            return false;
        }
    }

    /**
     * Returns messages list by patient and message type
     *
     * @param Patient $patient
     * @param integer $type
     * @return CActiveRecord[]
     */
    public function getMessagesByPatient ($patient, $type = null)
    {

        $obj = $this->with(array(
            "device" => array(
                "with" => array(
                    "patient" => array(
                        "condition" => "patient.uid = " . $patient->uid
                    )
                )
            )
        ));

        if ($type) {
            $messages = $obj->findAllByAttributes(array("type" => $type));
        } else {
            $messages = $obj->findAll();
        }

        return $messages;
    }

    public function behaviors()
    {
        return array(
            'RhDateTimeBehavior' => array(
                'class' => 'application.extensions.RhDateTimeBehavior'
            ),
            'EAdvancedArBehavior' => array(
                'class' => 'application.extensions.EAdvancedArBehavior'
            )
        );
    }

    public function toggleActual()
    {
        if ($this->is_actual) {
            $this->is_actual = 0;
        } else {
            $this->is_actual = 1;
        }
        $this->save();
        return $this;
    }

    /**
     * @param StatusMessage[] $findings
     * @return string
     */
    public static function encodeFindings($findings)
    {
        $tmpData = array();

        foreach ($findings as $finding) {
            $attrs = $finding->getAttributes();
            if ($attrs['create_time']) {
                $createDate = $attrs['create_time']->format();
                $attrs['create_time'] = $createDate;
            }
            $tmpData[] = $attrs;
        }

        return CJSON::encode($tmpData);
    }

    public function defaultScope()
    {
        return array(
            'order'=>"t.create_time DESC",
        );
    }
}
