<?php

/**
 * This is the model class for table "{{clinical_data_therapy}}".
 *
 * The followings are the available columns in table '{{clinical_data_therapy}}':
 * @property integer $clinical_data_uid
 * @property integer $product_uid
 * @property string $dose
 * @property integer $measure
 *
 * The followings are the available model relations:
 * @property ClinicalData $clinicalDataU
 * @property PharmProductTmp $pharmProduct
 * @property MeasurementUnit $measureUnit
 */
class ClinicalDataTherapy extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ClinicalDataTherapy the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{clinical_data_therapy}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('clinical_data_uid, product_uid', 'required'),
            array('clinical_data_uid, product_uid, measure', 'numerical', 'integerOnly' => true),
            array('dose', 'length', 'max' => 10),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('clinical_data_uid, product_uid, dose, measure', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'clinicalData' => array(self::BELONGS_TO, 'ClinicalData', 'clinical_data_uid'),
            'pharmProduct' => array(self::BELONGS_TO, 'PharmProductTmp', 'product_uid'),
            'measureUnit' => array(self::BELONGS_TO, 'MeasurementUnit', 'measure'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'clinical_data_uid' => 'Clinical Data Uid',
            'product_uid' => 'Product Uid',
            'dose' => 'Доза',
            'measure' => 'Ед.измерения',
        );
    }

    public function behaviors()
    {
        return [
            'FloatDelimiterBehavior' => ['class' => 'FloatDelimiterBehavior']
        ];
    }


}
