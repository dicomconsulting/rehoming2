<?php

/**
 * This is the model class for table "{{clinical_data_surgery}}".
 *
 * The followings are the available columns in table '{{clinical_data_surgery}}':
 * @property integer $clinical_data_uid
 * @property integer $surgery_uid
 * @property string $date
 * The followings are the available model relations:
 * @property ClinicalData $clinicalData
 * @property Surgery $surgery
 */
class ClinicalDataSurgery extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ClinicalDataSurgery the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{clinical_data_surgery}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('clinical_data_uid, surgery_uid, date', 'required'),
            array('clinical_data_uid, surgery_uid', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('clinical_data_uid, surgery_uid, date', 'safe', 'on' => 'search'),
        );
    }


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'clinicalData' => array(self::BELONGS_TO, 'ClinicalData', 'clinical_data_uid'),
            'surgery' => array(self::BELONGS_TO, 'Surgery', 'surgery_uid'),
        );
    }


    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'clinical_data_uid' => 'Clinical Data Uid',
            'surgery_uid' => 'Surgery Uid',
            'date' => 'дата операции',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('clinical_data_uid', $this->clinical_data_uid);
        $criteria->compare('surgery_uid', $this->surgery_uid);
        $criteria->compare('date', $this->date, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}
