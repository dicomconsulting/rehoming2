<?php

/**
 * This is the model class for table "{{patient_holter}}".
 *
 * The followings are the available columns in table '{{patient_holter}}':
 * @property integer $uid
 * @property integer $patient_uid
 * @property integer $user_uid
 * @property RhDateTime $date_create
 * @property integer $hr_max
 * @property integer $hr_min
 * @property integer $hr_avg
 * @property integer $sv_extr
 * @property integer $vent_extr
 * @property integer $sv_runs
 * @property integer $vent_runs
 *
 * @property User $user
 * @property Patient $patient*
 */
class PatientHolter extends CActiveRecord
{
    /**
     * Возвращает последние данные по Холтеру по пациенту
     *
     * @param Patient $patient
     * @return CActiveRecord
     */
    public static function getLatestDataByPatient(Patient $patient)
    {
        $criteria = new CDbCriteria();
        $criteria->condition = "patient_uid = :patient_uid";
        $criteria->params = array(":patient_uid" => $patient->uid);
        $criteria->order = "date_create DESC";

        return self::model()->find($criteria);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PatientHolter the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{patient_holter}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('patient_uid, user_uid, date_create', 'required'),
            array(
                'patient_uid, user_uid, hr_max, hr_min, hr_avg, ' .
                'sv_extr, vent_extr, sv_runs, vent_runs',
                'numerical',
                'integerOnly' => true
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_uid'),
            'patient' => array(self::BELONGS_TO, 'Patient', 'patient_uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid' => 'Uid',
            'patient_uid' => 'Patient Uid',
            'user_uid' => 'User Uid',
            'date_create' => 'Время получения данных',
            'hr_max' => 'ЧСС (макс.) (кол-во уд.в мин.)',
            'hr_min' => 'ЧСС (миним.) (кол-во уд.в мин.)',
            'hr_avg' => 'ЧСС (средняя) (кол-во уд.в мин.)',
            'sv_extr' => 'Экстрасистолы наджелудочковые',
            'vent_extr' => 'Экстрасистолы желудочковые',
            'sv_runs' => 'Пробежки наджелудочковые (> 5 комплексов)',
            'vent_runs' => 'Пробежки желудочковые (> 5 комплексов)',
        );
    }

    public function behaviors()
    {
        return array(
            'EAdvancedArBehavior' => array(
                'class' => 'application.extensions.EAdvancedArBehavior'
            ),
            'RhDateTimeBehavior' => array(
                'class' => 'application.extensions.RhDateTimeBehavior'
            ),
        );
    }

    public function defaultScope()
    {
        return array(
            'order' => "t.date_create DESC",
        );
    }

    public function init()
    {
        Yii::app()->getModule("admin");

        if ($this->scenario == "insert") {
            $this->date_create = new RhDateTime();
        }

        parent::init();
    }

    protected function beforeSave()
    {
        if (is_array($this->date_create)) {
            $this->date_create = $this->date_create['date'] . " " . $this->date_create['time'] . ":00";
        }

        return parent::beforeSave();
    }
}
