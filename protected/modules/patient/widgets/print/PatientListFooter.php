<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 11.11.13
 * Time: 17:03
 */

class PatientListFooter extends CWidget {

    public $title;
    public $patient;
    public $protocolId;
    public $page;
    public $pageTotal;

    public function run()
    {
        $this->render("patient-list-footer", array("title" => $this->title, "page" => $this->page, "pageTotal" => $this->pageTotal));
    }
}