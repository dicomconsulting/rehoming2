<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 11.11.13
 * Time: 17:03
 */

class PatientListHeader extends CWidget {

    public $title;
    public $patient;
    public $protocolId;

    public function run()
    {
        $this->render("patient-list-header", array("title" => $this->title));
    }
}