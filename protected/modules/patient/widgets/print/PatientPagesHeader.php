<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 11.11.13
 * Time: 17:03
 */

class PatientPagesHeader extends CWidget
{

    public $title;
    /** @var  Patient */
    public $patient;

    public function run()
    {
        $research = Research::model()->getOpenedByPatient($this->patient->uid);
        $this->render(
            "patient-pages-header",
            array("title" => $this->title, "research" => $research, "patient" => $this->patient)
        );
    }
}
