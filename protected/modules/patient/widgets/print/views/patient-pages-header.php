<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 11.11.13
 * Time: 17:04
 *
 * @var string $title
 * @var Research $research
 * @var Patient $patient
 */
?>
<!DOCTYPE html>
<html lang="en"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" /></head>
<body style="margin: 0; padding: 0;">
<table style="border: 0; border-collapse: collapse; width: 100%; border-bottom: 1px #000000 solid;font-size: 14px;">
    <tr style="font-weight: bold">
        <td style="text-align: center;font-size: 16px;">ReHoming</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="2" style="text-align: right; padding-right: 52px;"><?=$title?></td>
        <td rowspan="3" style="text-align: right;"><img src="<?=Yii::app()->request->getBaseUrl(true)?>/images/logo_vnoa.jpg" width="75" height="63"></td>
    </tr>
    <tr style="text-align: center;">
        <td>пациент</td>
        <td>код центра</td>
        <td>инициалы</td>
        <td>№ скрининга</td>
        <td>дата включения</td>
    </tr>
    <tr style="text-align: center;">
        <?php if($research):?>
        <td><?=$patient->human_readable_id?></td>
        <td><?=$research->medicalCenter->code?></td>
        <td><?=$research->patient->getCode()?></td>
        <td><?=$research->getScreeningNumber()?></td>
        <td><?=$research->date_begin?></td>
        <?php else:?>
            <td colspan="5" style="text-align: left;"><?=$patient->human_readable_id?></td>
        <?php endif;?>
    </tr>
</table>
</body>
</html>