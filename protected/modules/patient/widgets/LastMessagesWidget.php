<?php

class LastMessagesWidget extends BaseWidget
{
    public $viewName = 'last-messages';

    const DEFAULT_SECTION = "status.summary";

    public function run()
    {
        return;//отключаем вывод статусов

        /** @var PatientCardController $owner */
        $owner = $this->getOwner();
        $section = $owner->id . "." . $owner->action->id;

        if ($section == self::DEFAULT_SECTION) {
            $section = null;
        }

        $messages = Message::getMessagesByPatient($this->patient, $section);

        $this->render($this->viewName, ['patient' => $this->patient, 'messages' => $messages]);
    }
}
