<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 11.12.13
 * Time: 10:36
 *
 * @var ClinicalData[] $data
 * @var string $parameter
 */
?>

    <div class="row">
        <div class="span1">
            <strong><?= Yii::t("patient", "Номер") ?></strong>
        </div>
        <div class="span2">
            <strong><?= Yii::t("patient", "Дата") ?></strong>
        </div>
        <div class="span5">
            <strong><?= Yii::t("patient", "Значение") ?></strong>
        </div>
    </div>
<?php if ($data): ?>
    <?php $i = 0; ?>
    <?php foreach ($data as $row): ?>
        <div class="row margin10">
            <div class="span1">
                <?= ++$i ?>
            </div>
            <div class="span2">
                <?= $row->date_create ?>
            </div>
            <div class="span5">
                <?php if ($row->therapy): ?>
                    <?php foreach ($row->therapy as $pharm): ?>
                        <div class="row">
                            <div class="span2">
                                <?= $pharm->pharmProduct->name ?>
                            </div>
                            <div class="span2">
                                <?= $pharm->dose ?> <?= $pharm->measureUnit->name ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <p><?= Yii::t("patient", "данные не заполнены") ?></p>
                <?php endif; ?>
            </div>
        </div>
    <?php endforeach; ?>
<?php else: ?>
    <p>Данных по этому параметру не найдено</p>
<?php endif; ?>