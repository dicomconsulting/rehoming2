<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 11.12.13
 * Time: 9:52
 */

class HistoryWidget extends CWidget
{

    public $patient;
    public $parameter;

    protected $view = "base-history-widget";

    public function run()
    {
        $data = $this->loadHistoryData();
        $view = $this->getViewName();
        $this->render($view, ['data' => $data, 'parameter' => $this->parameter]);
    }

    protected function loadHistoryData()
    {
        return ClinicalData::model()->getHistoryByPatient($this->patient, $this->parameter);
    }

    protected function getViewName()
    {
        if (array_key_exists($this->parameter, ClinicalData::model()->getAttributes())) {
            return $this->view;
        } else {
            return $this->parameter . "-history";
        }
    }
}
