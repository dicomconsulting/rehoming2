<?php

abstract class BaseWidget extends CWidget
{
    /**
     * @var Patient
     */
    public $patient;

    /**
     * @var GraphManager
     */
    public $graphManager;

    public $viewName = '';

    public function run()
    {
        $this->render($this->viewName, ['patient' => $this->patient]);
    }

    public function render($view, $data = null, $return = false)
    {
        $customViewName = $this->_getCustomViewFileName($view);
        if (($viewFile=$this->getViewFile($customViewName))!==false) {
            $view = $customViewName;
        }
        return parent::render($view, $data, $return);
    }

    public function printDateTime($dateTime, $onlyDate = false)
    {
        $output = DeviceValue::NULL_PLACEHOLDER;
        if ($dateTime instanceof RhDateTime) {
            $output = $dateTime->format('d') . '&nbsp;' . $dateTime->getMonthName() . '&nbsp;' . $dateTime->format('Y');
            if (!$onlyDate) {
                $output .= ' ' . $dateTime->format('H:i:s');
            }
        }

        return $output;
    }

    public function sinceDateTimeString()
    {
        return $this->printDateTime(
            $this->getFirstDeviceStateFromPeriod()->create_time
        );
    }

    public function getFirstDeviceStateFromPeriod()
    {
        return $this->patient->device->first_state_from_period(
            ['params' => [':follow_up_date' => $this->patient->device->lastState->last_follow_up->format('Y-m-d H:i:s')]]
        );
    }

    public function getGraphManager()
    {
        if (empty($this->graphManager)) {
            $this->graphManager = new GraphManager();
        }
        return $this->graphManager;
    }

    private function _getCustomViewFileName($view)
    {
        return $view . '-' . $this->_transformModelNameToViewPrefix(
            $this->patient->device->device_model
        );

    }

    private function _transformModelNameToViewPrefix($modelName)
    {
        return str_replace(
            ['/\s+/', '(', ')', '-', ' '],
            ['', '', '', '', '_'],
            strtolower($modelName)
        );
    }
}
