<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
?>
<table class = "table table-condensed table-bordered rh-main-table">
    <col width="60%" valign="middle">
    <col width="40%" valign="middle">
    <thead>
        <tr>
            <th colspan="2">
                <?=CHtml::link(Yii::t('PatientModule.brady', 'Бради/РТС/Настройки ФП'), ['device/bradycardia', 'id' => $patient->uid]); ?>
            </th>
        </tr>
    </thead>
    <tbody>
        <?php if (isset($parameters['mode'])) { ?>
        <tr>
            <td>
            <?=$parameters['mode']->name;?>
            </td>
            <td>
            <?=$parameters['mode']->getCurrentValue();?>
            <?php if (isset($parameters['ven_pacing'])) { ?>
                /<?=$parameters['ven_pacing']->getCurrentValue();?>
            <?php } ?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['basic_rate'])) { ?>
        <tr>
            <td>
            <?=$parameters['basic_rate']->name;?>
            <?php if (isset($parameters['upper_rate'])) {?>
                / <?=$parameters['upper_rate']->name;?>
            <?php } ?>
            [<?=$parameters['basic_rate']->unit->name;?>]
            </td>
            <td>
            <?=$parameters['basic_rate']->getCurrentValue();?>
            <?php if (isset($parameters['upper_rate'])) {?>
                / <?=$parameters['upper_rate']->getCurrentValue();?>
            <?php } ?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['av_delay_1'])) { ?>
            <tr>
                <td>
                    <?=Yii::t('PatientModule.brady', 'АВ задержка');?>
                </td>
                <td>
                    <?=$parameters['av_delay_1']->getCurrentValue();?>
                    <?=$parameters['av_delay_1']->getCurrentValue()->value ? $parameters['av_delay_1']->unit->name : '';?>
                </td>
            </tr>
        <?php } ?>
        <?php if ((isset($parameters['av_delay_1']) && $parameters['av_delay_1_rate']->getCurrentValue()->value)
            || (isset($parameters['av_delay_2']) && $parameters['av_delay_2_rate']->getCurrentValue()->value)) { ?>
        <tr>
            <td>
            <?=Yii::t('PatientModule.brady', 'АВ задержка на');?>
            <?=$parameters['av_delay_1_rate']->getCurrentValue();?> /
            <?=$parameters['av_delay_2_rate']->getCurrentValue() . '&nbsp;' . $parameters['av_delay_2_rate']->unit->name;?>
            </td>
            <td>
            <?php if ($parameters['av_delay_1']->getCurrentValue()->value) {
                echo $parameters['av_delay_1']->getCurrentValue()->value;
                echo ' / ';
            } ?>
            <?php if ($parameters['av_delay_2']->getCurrentValue()->value) {
                echo $parameters['av_delay_2']->getCurrentValue()->value;
                echo '&nbsp;' . $parameters['av_delay_2']->unit->name;
            } ?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['mode_switching_mode'])) { ?>
        <tr>
            <td><?=$parameters['mode_switching_mode']?></td>
            <td>
                <?php if ($parameters['mode_switching_intervention_rate']->getCurrentValue()->value) {
                    echo $parameters['mode_switching_intervention_rate']->getCurrentValue() . '&nbsp;';
                    echo $parameters['mode_switching_intervention_rate']->unit->name;
                    echo ' / ';
                } ?>
                <?=$parameters['mode_switching_mode']->getCurrentValue();?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
