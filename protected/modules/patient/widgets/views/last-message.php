<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
?>
<table class = "table table-bordered rh-main-table">
    <thead>
        <tr>
            <th>
                <?=Yii::t('PatientModule.device', 'Последнее сообщение получено');?>
                <?=$this->printDateTime($patient->device->lastState->create_time);?>
            </th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
