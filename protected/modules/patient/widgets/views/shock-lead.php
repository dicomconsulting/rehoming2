<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
?>
<table class = "table table-condensed table-bordered rh-main-table">
    <col width="60%" valign="middle">
    <col width="40%" valign="middle">
    <thead>
        <tr>
            <th colspan="2">
                <?=CHtml::link(Yii::t('PatientModule.device', 'Шоковый электрод'), ['status/lead', 'id' => $patient->uid]); ?>
            </th>
        </tr>
    </thead>
    <tbody>
        <?php if (isset($parameters['daily_shock_lead_impedance'])) { ?>
        <tr>
            <td>
            <?=$parameters['daily_shock_lead_impedance']->name;?>
            [<?=$parameters['daily_shock_lead_impedance']->unit->name;?>]
            </td>
            <td>
            <?=$parameters['daily_shock_lead_impedance']->getCurrentValue();?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['latest_available_impedance_of_a_delivered_shock'])) { ?>
        <tr>
            <td>
            <?=$parameters['latest_available_impedance_of_a_delivered_shock']->name;?>
            [<?=$parameters['latest_available_impedance_of_a_delivered_shock']->unit->name;?>]
            </td>
            <td>
            <?=$parameters['latest_available_impedance_of_a_delivered_shock']->getCurrentValue();?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
