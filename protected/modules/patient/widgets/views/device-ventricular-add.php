<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$parametersForOutput = [
    'VF зона' => [],
    'Желудочковая терапия - основная' => []
];

if (isset($parameters['vf_zone_limit'])) {
    $parametersForOutput['VF зона'][
        $parameters['vf_zone_limit']->name . ' [' . $parameters['vf_zone_limit']->unit->name . ']'
    ] = $parameters['vf_zone_limit']->getCurrentValue();
}
if (isset($parameters['atp_one_shot'])) {
    $parametersForOutput['VF зона'][$parameters['atp_one_shot']->name]
        = $parameters['atp_one_shot']->getCurrentValue();
}
if (isset($parameters['vf_1st_shock'])) {
    $parametersForOutput['VF зона'][
        $parameters['vf_1st_shock']->name . ' [' . $parameters['vf_1st_shock']->unit->name . ']'
    ] = $parameters['vf_1st_shock']->getCurrentValue();
}
if (isset($parameters['vf_2nd_shock'])) {
    $parametersForOutput['VF зона'][
        $parameters['vf_2nd_shock']->name . ' [' . $parameters['vf_2nd_shock']->unit->name . ']'
    ] = $parameters['vf_2nd_shock']->getCurrentValue();
}
if (isset($parameters['vf_3rd_nth_shock'])) {
    $parametersForOutput['VF зона'][$parameters['vf_3rd_nth_shock']->name]
        = $parameters['vf_3rd_nth_shock']->getCurrentValue();
}

if (isset($parameters['shock_path'])) {
    $parametersForOutput['Желудочковая терапия - основная'][$parameters['shock_path']->name]
        = $parameters['shock_path']->getCurrentValue();
}
if (isset($parameters['progressive_course_of_therapy'])) {
    $parametersForOutput['Желудочковая терапия - основная'][$parameters['progressive_course_of_therapy']->name]
        = $parameters['progressive_course_of_therapy']->getCurrentValue();
}
if (isset($parameters['sustained_vt'])) {
    $parametersForOutput['Желудочковая терапия - основная'][$parameters['sustained_vt']->name]
        = $parameters['sustained_vt']->getCurrentValue();
}


?>
<table class = "table table-condensed table-bordered rh-main-table">
    <col width="5%" valign="middle">
    <col width="55%" valign="middle">
    <col width="40%" valign="middle">
    <tbody>
        <?php foreach ($parametersForOutput as $headerCell => $parameters) { ?>
            <?php if(empty($parameters)) {
                continue;
            } ?>
            <tr>
                <th colspan="3">
                    <?=Yii::t('PatientModule.device', $headerCell); ?>
                </th>
            </tr>
            <?php foreach ($parameters as $label => $value) { ?>
                <tr>
                    <td>&nbsp;</td>
                    <td><?=$label;?></td>
                    <td class="text-center"><?=$value;?></td>
                </tr>
            <?php } ?>
        <?php } ?>
    </tbody>
</table>