<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$graphManager = $this->getGraphManager();

$monitoringEpisodesYAxis = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20];
$svtEpisodesYAxis = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20];

$parametersForOutput = [
    'Мониторируемые предсердные аритмии' => [],
    'НЖТ' => [],
    'Детали эпизода SMART' => []
];

if (isset($parameters['atrial_monitoring_episodes'])) {
    $parametersForOutput['Мониторируемые предсердные аритмии'][
        $parameters['atrial_monitoring_episodes']->name
    ] = [
        $graphManager->buildGraph(
            $parameters['atrial_monitoring_episodes'],
            Yii::t('PatientModule.device', 'Мониторируемые предсердные аритмии'),
            false,
            $monitoringEpisodesYAxis
        ),
        $parameters['atrial_monitoring_episodes']->getCountByCurrentPeriod(),
        ($parameters['atrial_monitoring_episodes']->getCurrentValue()->value ? : 0)
    ];
}
if (isset($parameters['long_ongoing_atrial_episode_detected'])) {
    $parametersForOutput['Мониторируемые предсердные аритмии'][
        $parameters['long_ongoing_atrial_episode_detected']->name
    ] = $parameters['long_ongoing_atrial_episode_detected']->getCurrentValue();
}
if (isset($parameters['svt_episodes_total'])) {
    $parametersForOutput['НЖТ'][
        $parameters['svt_episodes_total']->name
    ] = [
        $graphManager->buildGraph(
            $parameters['svt_episodes_total'],
            Yii::t('PatientModule.device', 'Эпизоды НЖТ'),
            true,
            $svtEpisodesYAxis
        ),
        $parameters['svt_episodes_total']->getCountByCurrentPeriod(),
        ($parameters['svt_episodes_total']->getCurrentValue()->value ? : 0)
    ];
}
if (isset($parameters['svt_episodes_smart_only'])) {
    $parametersForOutput['НЖТ'][
        $parameters['svt_episodes_smart_only']->name
    ] = [
        $graphManager->buildGraph(
            $parameters['svt_episodes_smart_only'],
            Yii::t('PatientModule.device', 'Эпизоды НЖТ'),
            true,
            $svtEpisodesYAxis
        ),
        $parameters['svt_episodes_smart_only']->getCountByCurrentPeriod(),
        ($parameters['svt_episodes_smart_only']->getCurrentValue()->value ? : 0)
    ];
}

if (isset($parameters['svt_episodes_atrial_fibrillation'])) {
    $parametersForOutput['Детали эпизода SMART'][
        $parameters['svt_episodes_atrial_fibrillation']->name
    ] = [
        '',
        $parameters['svt_episodes_atrial_fibrillation']->getCountByCurrentPeriod(),
        ($parameters['svt_episodes_atrial_fibrillation']->getCurrentValue()->value ? : 0)
    ];
}
if (isset($parameters['svt_episodes_atrial_flutter'])) {
    $parametersForOutput['Детали эпизода SMART'][
        $parameters['svt_episodes_atrial_flutter']->name
    ] = [
        '',
        $parameters['svt_episodes_atrial_flutter']->getCountByCurrentPeriod(),
        ($parameters['svt_episodes_atrial_flutter']->getCurrentValue()->value ? : 0)
    ];
}
if (isset($parameters['svt_episodes_sinus_tachycardia'])) {
    $parametersForOutput['Детали эпизода SMART'][
        $parameters['svt_episodes_sinus_tachycardia']->name
    ] = [
        '',
        $parameters['svt_episodes_sinus_tachycardia']->getCountByCurrentPeriod(),
        ($parameters['svt_episodes_sinus_tachycardia']->getCurrentValue()->value ? : 0)
    ];
}
if (isset($parameters['svt_episodes_1_1'])) {
    $parametersForOutput['Детали эпизода SMART'][
        $parameters['svt_episodes_1_1']->name
    ] = [
        '',
        $parameters['svt_episodes_1_1']->getCountByCurrentPeriod(),
        ($parameters['svt_episodes_1_1']->getCurrentValue()->value ? : 0)
    ];
}

$printFirstHeader = false;
?>

<table class = "table table-condensed rh-main-table">
    <col width="5%" valign="middle">
    <col width="30%" valign="middle">
    <col width="15%" valign="middle">
    <col width="25%" valign="middle">
    <col width="35%" valign="middle">
    <tbody>
    <?php foreach ($parametersForOutput as $headerCell => $parameters) { ?>
        <?php if(empty($parameters)) {
            continue;
        } ?>
        <tr>
            <th colspan="3">
                <?=Yii::t('PatientModule.device', $headerCell);?>
            </th>
            <?php if (!$printFirstHeader) { ?>
            <?php $printFirstHeader = true; ?>
            <th class="text-center">
                <?=Yii::t('PatientModule.device', 'C');?>
                <?=$this->sinceDateTimeString();?>
            </th>
            <th class="text-center">
                <?=Yii::t('PatientModule.device', 'C даты имплантации');?>
            </th>
            <?php } else { ?>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            <?php } ?>
        </tr>
        <?php foreach ($parameters as $label => $value) { ?>
            <tr>
                <td>&nbsp;</td>
                <td><?=$label?></td>
                <?php if (is_array($value)) {?>
                    <td class="text-center noprint_hidden"><?=$value[0]?></td>
                    <td class="text-center"><?=$value[1]?></td>
                    <td class="text-center"><?=$value[2]?></td>
                <?php } else { ?>
                    <td class="text-center">&nbsp;</td>
                    <td class="text-center" colspan="2"><?=$value?></td>
                <?php } ?>
            </tr>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>
