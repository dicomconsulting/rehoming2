<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
?>
<table class = "table table-condensed table-bordered rh-main-table">
    <col width="60%" valign="middle">
    <col width="40%" valign="middle">
    <thead>
        <tr>
            <th colspan="2">
                <?=Yii::t('PatientModule.device', 'Электрод ПЖ'); ?>
            </th>
        </tr>
    </thead>
    <tbody>
        <?php if (isset($parameters['rv_pulse_amplitude'])) { ?>
        <tr>
            <td>
                <?=$parameters['rv_pulse_amplitude']->name;?>
                [<?=$parameters['rv_pulse_amplitude']->unit->name;?>]
            </td>
            <td>
                <?=$parameters['rv_pulse_amplitude']->getCurrentValue();?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['rv_pulse_width'])) { ?>
        <tr>
            <td>
                <?=$parameters['rv_pulse_width']->name;?>
                [<?=$parameters['rv_pulse_width']->unit->name;?>]
            </td>
            <td>
                <?=$parameters['rv_pulse_width']->getCurrentValue();?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
