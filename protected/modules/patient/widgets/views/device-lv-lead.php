<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
?>
<?php if (isset($parameters['lv_pulse_amplitude'])
    || isset($parameters['lv_pulse_width'])
    || isset($parameters['lv_pacing_polarity'])
    || isset($parameters['lv_sensing_polarity'])) { ?>
<table class = "table table-condensed table-bordered rh-main-table">
    <col width="60%" valign="middle">
    <col width="40%" valign="middle">
    <thead>
        <tr>
            <th colspan="2">
                <?=Yii::t('PatientModule.device', 'Электрод ЛЖ'); ?>
            </th>
        </tr>
    </thead>
    <tbody>
        <?php if (isset($parameters['lv_pulse_amplitude'])) { ?>
        <tr>
            <td>
                <?=$parameters['lv_pulse_amplitude']->name;?>
                [<?=$parameters['lv_pulse_amplitude']->unit->name;?>]
            </td>
            <td>
                <?=$parameters['lv_pulse_amplitude']->getCurrentValue();?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['lv_pulse_width'])) { ?>
        <tr>
            <td>
                <?=$parameters['lv_pulse_width']->name;?>
                [<?=$parameters['lv_pulse_width']->unit->name;?>]
            </td>
            <td>
                <?=$parameters['lv_pulse_width']->getCurrentValue();?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['lv_pacing_polarity'])) { ?>
        <tr>
            <td>
                <?=$parameters['lv_pacing_polarity']->name;?>
            </td>
            <td>
                <?=$parameters['lv_pacing_polarity']->getCurrentValue();?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['lv_sensing_polarity'])) { ?>
        <tr>
            <td>
                <?=$parameters['lv_sensing_polarity']->name;?>
            </td>
            <td>
                <?=$parameters['lv_sensing_polarity']->getCurrentValue();?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<?php } ?>