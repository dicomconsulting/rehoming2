<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$graphManager = $this->getGraphManager();

$percentageYAxis = [0, 20, 40, 60, 80, 100];

$parametersForOutput = [
    'Навязанный ритм' => [],
    'AV-последовательности (кроме моментов переключения режима)' => [],
    'CRT' => [],
    'ЛЖ-ПЖ последовательности' => [],
    'ПВПРП' => []
];

if (isset($parameters['vv_sequences_vx_vx'])) {
    $parametersForOutput['Навязанный ритм'][
    $parameters['vv_sequences_vx_vx']->name . ' [' . $parameters['vv_sequences_vx_vx']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['vv_sequences_vx_vx'],
            null,
            false,
            $percentageYAxis
        ),
        $parameters['vv_sequences_vx_vx']->getCurrentValue(),
        ($parameters['vv_sequences_vx_vx']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['pacing'])) {
    $parametersForOutput['Навязанный ритм'][
    $parameters['pacing']->name . ' [' . $parameters['pacing']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['pacing'], Yii::t('PatientModule.device', 'Навязанный ритм'),
            Yii::t('PatientModule.device', 'Навязанный ритм'),
            true,
            $percentageYAxis
        ),
        $parameters['pacing']->getCurrentValue(),
        ($parameters['pacing']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['atrial_pacing_ap'])) {
    $parametersForOutput['Навязанный ритм'][
        $parameters['atrial_pacing_ap']->name . ' [' . $parameters['atrial_pacing_ap']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['atrial_pacing_ap'],
            Yii::t('PatientModule.device', 'Навязанный ритм'),
            true,
            $percentageYAxis
        ),
        $parameters['atrial_pacing_ap']->getCurrentValue(),
        ($parameters['atrial_pacing_ap']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['ven_pacing_vp'])) {
    $parametersForOutput['Навязанный ритм'][
        $parameters['ven_pacing_vp']->name . ' [' . $parameters['ven_pacing_vp']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['ven_pacing_vp'],
            Yii::t('PatientModule.device', 'Навязанный ритм'),
            true,
            $percentageYAxis
        ),
        $parameters['ven_pacing_vp']->getCurrentValue(),
        ($parameters['ven_pacing_vp']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}

if (isset($parameters['intrinsic_rhythm_as_vs'])) {
    $parametersForOutput['AV-последовательности (кроме моментов переключения режима)'][
        $parameters['intrinsic_rhythm_as_vs']->name . ' [' . $parameters['intrinsic_rhythm_as_vs']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['intrinsic_rhythm_as_vs'],
            Yii::t('PatientModule.device', 'AV-последовательность'),
            true,
            $percentageYAxis
        ),
        $parameters['intrinsic_rhythm_as_vs']->getCurrentValue(),
        ($parameters['intrinsic_rhythm_as_vs']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['vat_stimulation_as_vp'])) {
    $parametersForOutput['AV-последовательности (кроме моментов переключения режима)'][
        $parameters['vat_stimulation_as_vp']->name . ' [' . $parameters['vat_stimulation_as_vp']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['vat_stimulation_as_vp'],
            Yii::t('PatientModule.device', 'AV-последовательность'),
            true,
            $percentageYAxis
        ),
        $parameters['vat_stimulation_as_vp']->getCurrentValue(),
        ($parameters['vat_stimulation_as_vp']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['conducted_atrial_pacing_ap_vs'])) {
    $parametersForOutput['AV-последовательности (кроме моментов переключения режима)'][
        $parameters['conducted_atrial_pacing_ap_vs']->name . ' [' . $parameters['conducted_atrial_pacing_ap_vs']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['conducted_atrial_pacing_ap_vs'],
            Yii::t('PatientModule.device', 'AV-последовательность (стимул А)'),
            true,
            $percentageYAxis
        ),
        $parameters['conducted_atrial_pacing_ap_vs']->getCurrentValue(),
        ($parameters['conducted_atrial_pacing_ap_vs']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['dual_chamber_pacing_ap_vp'])) {
    $parametersForOutput['AV-последовательности (кроме моментов переключения режима)'][
        $parameters['dual_chamber_pacing_ap_vp']->name . ' [' . $parameters['dual_chamber_pacing_ap_vp']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['dual_chamber_pacing_ap_vp'],
            Yii::t('PatientModule.device', 'AV-последовательность (стимул А)'),
            true,
            $percentageYAxis
        ),
        $parameters['dual_chamber_pacing_ap_vp']->getCurrentValue(),
        ($parameters['dual_chamber_pacing_ap_vp']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['vv_sequence_vx_vx'])) {
    $parametersForOutput['AV-последовательности (кроме моментов переключения режима)'][
        $parameters['vv_sequence_vx_vx']->name . ' [' . $parameters['vv_sequence_vx_vx']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['vv_sequence_vx_vx'],
            Yii::t('PatientModule.device', 'AV-последовательность (VV-последовательность)'),
            true,
            $percentageYAxis
        ),
        $parameters['vv_sequence_vx_vx']->getCurrentValue(),
        ($parameters['vv_sequence_vx_vx']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}

if (isset($parameters['crt_pacing'])) {
    $parametersForOutput['CRT'][
        $parameters['crt_pacing']->name . ' [' . $parameters['crt_pacing']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['crt_pacing'],
            Yii::t('PatientModule.device', 'CRT стимуляция'),
            true,
            $percentageYAxis
        ),
        $parameters['crt_pacing']->getCurrentValue(),
        ($parameters['crt_pacing']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}

if (isset($parameters['biv_stimulation'])) {
    $parametersForOutput['ЛЖ-ПЖ последовательности'][
        $parameters['biv_stimulation']->name . ' [' . $parameters['biv_stimulation']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['biv_stimulation'],
            Yii::t('PatientModule.device', 'События стимуляции ПЖ'),
            true,
            $percentageYAxis
        ),
        $parameters['biv_stimulation']->getCurrentValue(),
        ($parameters['biv_stimulation']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['rvp_without_lvp'])) {
    $parametersForOutput['ЛЖ-ПЖ последовательности'][
        $parameters['rvp_without_lvp']->name . ' [' . $parameters['rvp_without_lvp']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['rvp_without_lvp'],
            Yii::t('PatientModule.device', 'События стимуляции ПЖ'),
            true,
            $percentageYAxis
        ),
        $parameters['rvp_without_lvp']->getCurrentValue(),
        ($parameters['rvp_without_lvp']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['rvs_triggered_lvp'])) {
    $parametersForOutput['ЛЖ-ПЖ последовательности'][
        $parameters['rvs_triggered_lvp']->name . ' [' . $parameters['rvs_triggered_lvp']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['rvs_triggered_lvp'],
            Yii::t('PatientModule.device', 'События детекции ПЖ (не ЖЭС)'),
            true,
            $percentageYAxis
        ),
        $parameters['rvs_triggered_lvp']->getCurrentValue(),
        ($parameters['rvs_triggered_lvp']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['rvs_without_lvp'])) {
    $parametersForOutput['ЛЖ-ПЖ последовательности'][
        $parameters['rvs_without_lvp']->name . ' [' . $parameters['rvs_without_lvp']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['rvs_without_lvp'],
            Yii::t('PatientModule.device', 'События детекции ПЖ (не ЖЭС)'),
            true,
            $percentageYAxis
        ),
        $parameters['rvs_without_lvp']->getCurrentValue(),
        ($parameters['rvs_without_lvp']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['lvp_exclusive'])) {
    $parametersForOutput['ЛЖ-ПЖ последовательности'][
        $parameters['lvp_exclusive']->name . ' [' . $parameters['lvp_exclusive']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['lvp_exclusive'],
            Yii::t('PatientModule.device', 'Изолированная стимуляция ЛЖ'),
            true,
            $percentageYAxis
        ),
        $parameters['lvp_exclusive']->getCurrentValue(),
        ($parameters['lvp_exclusive']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['lvp_exclusive_inhibited'])) {
    $parametersForOutput['ЛЖ-ПЖ последовательности'][
        $parameters['lvp_exclusive_inhibited']->name . ' [' . $parameters['lvp_exclusive_inhibited']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['lvp_exclusive_inhibited'],
            Yii::t('PatientModule.device', 'Изолированная стимуляция ЛЖ'),
            true,
            $percentageYAxis
        ),
        $parameters['lvp_exclusive_inhibited']->getCurrentValue(),
        ($parameters['lvp_exclusive_inhibited']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['pvc_triggered_lvp'])) {
    $parametersForOutput['ЛЖ-ПЖ последовательности'][
        $parameters['pvc_triggered_lvp']->name . ' [' . $parameters['pvc_triggered_lvp']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['pvc_triggered_lvp'],
            Yii::t('PatientModule.device', 'Синхронизация ЖЭС'),
            true,
            $percentageYAxis
        ),
        $parameters['pvc_triggered_lvp']->getCurrentValue(),
        ($parameters['pvc_triggered_lvp']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['pvc_without_lvp'])) {
    $parametersForOutput['ЛЖ-ПЖ последовательности'][
        $parameters['pvc_without_lvp']->name . ' [' . $parameters['pvc_without_lvp']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['pvc_without_lvp'],
            Yii::t('PatientModule.device', 'Синхронизация ЖЭС'),
            true,
            $percentageYAxis
        ),
        $parameters['pvc_without_lvp']->getCurrentValue(),
        ($parameters['pvc_without_lvp']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}

if (isset($parameters['pvarp_setting'])) {
    $parametersForOutput['ПВПРП'][
        $parameters['pvarp_setting']->name . ' [' . $parameters['pvarp_setting']->unit->name . ']'
    ] = [
        '',
        $parameters['pvarp_setting']->getCurrentValue(),
        ($parameters['pvarp_setting']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}

$printFirstHeader = false;
?>

<table class = "table table-condensed rh-main-table">
    <col width="5%" valign="middle">
    <col width="30%" valign="middle">
    <col width="15%" valign="middle">
    <col width="25%" valign="middle">
    <col width="35%" valign="middle">
    <tbody>
        <tr>
            <th colspan ="4"></th>
            <th class="text-center">
                <?=Yii::t('PatientModule.device', 'C');?>
                <?=$this->sinceDateTimeString();?>
            </th>
        </tr>
    <?php foreach ($parametersForOutput as $headerCell => $parameters) { ?>
        <?php if(empty($parameters)) {
            continue;
        } ?>
        <tr>
            <th colspan="3">
                <?=Yii::t('PatientModule.device', $headerCell);?>
            </th>
            <?php if (!$printFirstHeader) { ?>
            <?php $printFirstHeader = true; ?>
            <th class="text-center">
                24<?=Yii::t('PatientModule.device', 'ч');?>
            </th>
            <th class="text-center">
                <?=Yii::t('PatientModule.device', 'Средние значения');?>
            </th>
            <?php } else { ?>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            <?php } ?>
        </tr>
        <?php foreach ($parameters as $label => $value) { ?>
            <tr>
                <td>&nbsp;</td>
                <td><?=$label?></td>
                <?php if (is_array($value)) {?>
                    <td class="text-center noprint_hidden"><?=$value[0]?></td>
                    <td class="text-center"><?=$value[1]?></td>
                    <td class="text-center"><?=$value[2]?></td>
                <?php } else { ?>
                    <td class="text-center">&nbsp;</td>
                    <td class="text-center" colspan="2"><?=$value?></td>
                <?php } ?>
            </tr>
        <?php } ?>
    <?php } ?>
    <?php if ($graphManager->countGraphs()) { ?>
        <?php $graphManager->resetGraphs(); ?>
        <?php do { ?>
            <tr id = '<?=$graphManager->getAnchor();?>' class="noprint">
                <th colspan="5">
                    <?=$graphManager->getName();?>
                </th>
            </tr>
            <tr class="noprint">
                <td colspan="5">
                    <?php $graphManager->renderGraph(); ?>
                </td>
            </tr>
        <?php } while ($graphManager->nextGraph() !== false); ?>
    <?php } ?>
    </tbody>
</table>
