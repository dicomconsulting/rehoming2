<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;

$implantationDate = $this->printDateTime($patient->device->implantation_date, true);
$createdTime = $this->printDateTime($patient->device->lastState->create_time);
$transmissionDate = $this->printDateTime($parameters['transmission_received_on']->getCurrentValue()->value);
$lastFollowUpDate = $this->printDateTime($patient->device->lastState->last_follow_up);

$parametersForOutput = [
    'Батарея' => [
        $parameters['battery_status']->name => $parameters['battery_status']->getCurrentValue(),
    ],
    'Трансмиттер' => [
        Yii::t('PatientModule.device', 'Серийный номер трансмиттера') => $patient->device->transmitter_serial_number,
        $parameters['transmission_received_on']->name => $transmissionDate,
        $parameters['message_type']->name => $parameters['message_type']->getCurrentValue(),
        Yii::t('PatientModule.device', 'Дата сообщения о последнем осмотре') => $lastFollowUpDate,
    ]
];
?>

<table class = "table table-condensed rh-main-table">
    <col width="5%" valign="middle">
    <col width="60%" valign="middle">
    <col width="35%" valign="middle">
    <tbody>
    <?php foreach ($parametersForOutput as $headerCell => $parameters) { ?>
        <tr>
            <th colspan="2">
                <?=Yii::t('PatientModule.device', $headerCell);?>
            </th>
            <th>
                &nbsp;
            </th>
        </tr>
        <?php foreach ($parameters as $label => $value) { ?>
            <tr>
                <td>&nbsp;</td>
                <td><?=$label?></td>
                <td><?=$value?></td>
            </tr>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>
