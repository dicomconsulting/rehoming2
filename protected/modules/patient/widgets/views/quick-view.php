<?php
/* @var $patient Patient */
$isAllowedViewPersonalData = Yii::app()->user->checkAccess('viewPersonalData');
Yii::app()->getModule('patient')->depersonalization;
?>
<table class="table table-bordered rh-main-table">
    <col width="30%" valign="middle" span="2"/>
    <col width="40%" valign="middle"/>
    <thead>
    <tr>
        <th colspan="3"><?= Yii::t('PatientModule.patient', 'Общие данные'); ?></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <?php if ($isAllowedViewPersonalData) { ?>
                <span class="noprint top-align">
                    <?= Yii::t('PatientModule.patient', 'ФИО'); ?>:
                    <?=$patient->getFioText()?>
                </span>
            <?php } ?>
        </td>
        <td>
            <?= $patient->device->device_model; ?>
            (<?= Yii::t('PatientModule.device', '№'); ?>
            <?= $patient->device->serial_number; ?>)
        </td>
        <td>
            <?= Yii::t('PatientModule.device', 'Последнее сообщение'); ?>:
            <?= $patient->device->lastState->create_time->format('d'); ?>
            <?= $patient->device->lastState->create_time->getMonthName(); ?>
            <?= $patient->device->lastState->create_time->format('Y'); ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php if ($isAllowedViewPersonalData) { ?>
                <span class="noprint top-align">
                    <?= Yii::t('PatientModule.patient', 'Телефон'); ?>:
                    <?=$patient->getPhoneText();?>
                </span>
            <?php } ?>
        </td>
        <td>
            <?php if ($patient->device->implantation_date instanceof RhDateTime) { ?>
                <?= Yii::t('PatientModule.device', 'имплантирован'); ?>
                <?= $patient->device->implantation_date->format('d'); ?>
                <?= $patient->device->implantation_date->getMonthName(); ?>
                <?= $patient->device->implantation_date->format('Y'); ?>
            <?php } ?>
        </td>
        <td>
            <?= Yii::t('PatientModule.patient', 'Последнее обследование'); ?>:
            <?= $patient->device->lastState->last_follow_up->format('d'); ?>
            <?= $patient->device->lastState->last_follow_up->getMonthName(); ?>
            <?= $patient->device->lastState->last_follow_up->format('Y'); ?>
        </td>
    </tr>
    </tbody>
</table>
