<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$sinceDate = $patient->device->lastState->getStartTimeOfCurrentValuePeriod(
    $parameters['last_follow_up'],
    true
);
?>
<table class = "table table-condensed rh-main-table">
    <col width="80%" valign="middle">
    <col width="20%" valign="middle">
    <thead>
        <tr>
            <th>
                <?=Yii::t('PatientModule.arrhythmia', 'Аритмии');?>
                <?php
                if ($sinceDate instanceof RhDateTime) {
                    echo Yii::t('PatientModule.arrhythmia', 'с') . " " . $this->printDateTime($sinceDate, true);
                }
                echo ' (' . Yii::t('PatientModule.arrhythmia', 'средние значения') .')';
                ?>
            </th>
            <th class="text-center">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?=$parameters['high_ven_rate_episodes_per_day']->name;?></td>
            <td class="text-center"><?=($parameters['high_ven_rate_episodes_per_day']->getCurrentValue() ? : 0 );?></td>
        </tr>
    </tbody>
</table>
