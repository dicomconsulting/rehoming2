<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$parametersForOutput = [
    'VT1 зона' => [],
    'VT2 зона' => []
];

if (isset($parameters['vt1_zone_limit'])) {
    $parametersForOutput['VT1 зона'][
        $parameters['vt1_zone_limit']->name . ' [' . $parameters['vt1_zone_limit']->unit->name . ']'
    ] = $parameters['vt1_zone_limit']->getCurrentValue();
}
if (isset($parameters['vt1_1st_atp'])) {
    $parametersForOutput['VT1 зона'][$parameters['vt1_1st_atp']->name] = $parameters['vt1_1st_atp']->getCurrentValue();
}
if (isset($parameters['vt1_2nd_atp'])) {
    $parametersForOutput['VT1 зона'][$parameters['vt1_2nd_atp']->name] = $parameters['vt1_2nd_atp']->getCurrentValue();
}
if (isset($parameters['vt1_1st_shock'])) {
    $parametersForOutput['VT1 зона'][
        $parameters['vt1_1st_shock']->name . ' [' . $parameters['vt1_1st_shock']->unit->name . ']'
    ] = $parameters['vt1_1st_shock']->getCurrentValue();
}
if (isset($parameters['vt1_2nd_shock'])) {
    $parametersForOutput['VT1 зона'][
        $parameters['vt1_2nd_shock']->name . ' [' . $parameters['vt1_2nd_shock']->unit->name . ']'
    ] = $parameters['vt1_2nd_shock']->getCurrentValue();
}
if (isset($parameters['vt1_3rd_nth_shock'])) {
    $parametersForOutput['VT1 зона'][$parameters['vt1_3rd_nth_shock']->name] = $parameters['vt1_3rd_nth_shock']->getCurrentValue();
}
if (isset($parameters['vt1_smart_detection'])) {
    $parametersForOutput['VT1 зона'][$parameters['vt1_smart_detection']->name] = $parameters['vt1_smart_detection']->getCurrentValue();
}
if (isset($parameters['vt1_smart_redetection'])) {
    $parametersForOutput['VT1 зона'][$parameters['vt1_smart_redetection']->name] = $parameters['vt1_smart_redetection']->getCurrentValue();
}

if (isset($parameters['vt2_zone_limit'])) {
    $parametersForOutput['VT2 зона'][
        $parameters['vt2_zone_limit']->name . ' [' . $parameters['vt2_zone_limit']->unit->name . ']'
    ] = $parameters['vt2_zone_limit']->getCurrentValue();
}
if (isset($parameters['vt2_1st_atp'])) {
    $parametersForOutput['VT2 зона'][$parameters['vt2_1st_atp']->name] = $parameters['vt2_1st_atp']->getCurrentValue();
}
if (isset($parameters['vt2_2nd_atp'])) {
    $parametersForOutput['VT2 зона'][$parameters['vt2_2nd_atp']->name] = $parameters['vt2_2nd_atp']->getCurrentValue();
}
if (isset($parameters['vt2_1st_shock'])) {
    $parametersForOutput['VT2 зона'][
        $parameters['vt2_1st_shock']->name . ' [' . $parameters['vt2_1st_shock']->unit->name . ']'
    ] = $parameters['vt2_1st_shock']->getCurrentValue();
}
if (isset($parameters['vt2_2nd_shock'])) {
    $parametersForOutput['VT2 зона'][
        $parameters['vt2_2nd_shock']->name . ' [' . $parameters['vt2_2nd_shock']->unit->name . ']'
    ] = $parameters['vt2_2nd_shock']->getCurrentValue();
}
if (isset($parameters['vt2_3rd_nth_shock'])) {
    $parametersForOutput['VT2 зона'][$parameters['vt2_3rd_nth_shock']->name] = $parameters['vt2_3rd_nth_shock']->getCurrentValue();
}
if (isset($parameters['vt2_smart_detection'])) {
    $parametersForOutput['VT2 зона'][$parameters['vt2_smart_detection']->name] = $parameters['vt2_smart_detection']->getCurrentValue();
}
if (isset($parameters['vt2_smart_redetection'])) {
    $parametersForOutput['VT2 зона'][$parameters['vt2_smart_redetection']->name] = $parameters['vt2_smart_redetection']->getCurrentValue();
}

?>
<table class = "table table-condensed table-bordered rh-main-table">
    <col width="5%" valign="middle">
    <col width="55%" valign="middle">
    <col width="40%" valign="middle">
    <tbody>
        <?php foreach ($parametersForOutput as $headerCell => $parameters) { ?>
            <?php if(empty($parameters)) {
                continue;
            } ?>
            <tr>
                <th colspan="3">
                    <?=Yii::t('PatientModule.device', $headerCell); ?>
                </th>
            </tr>
            <?php foreach ($parameters as $label => $value) { ?>
                <tr>
                    <td>&nbsp;</td>
                    <td><?=$label;?></td>
                    <td class="text-center"><?=$value;?></td>
                </tr>
            <?php } ?>
        <?php } ?>
    </tbody>
</table>