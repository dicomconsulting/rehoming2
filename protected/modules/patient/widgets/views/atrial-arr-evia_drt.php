<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$sinceDate = $patient->device->lastState->getStartTimeOfCurrentValuePeriod(
    $parameters['last_follow_up'],
    true
);
?>
<table class = "table table-condensed table-bordered rh-main-table">
    <col width="80%" valign="middle">
    <col width="20%" valign="middle">
    <thead>
        <tr>
            <th colspan="2"><?php
                if ($sinceDate instanceof RhDateTime) {
                    echo CHtml::link(
                        Yii::t('PatientModule.arrhythmia', 'Предсердная аритмия') . " " .
                        Yii::t('PatientModule.arrhythmia', 'с') . " " .
                        $this->printDateTime($sinceDate, true),
                        ['status/atrial', 'id' => $patient->uid]
                    );
                } else {
                    echo CHtml::link(
                        Yii::t('PatientModule.arrhythmia', 'Предсердная аритмия'),
                        ['status/atrial', 'id' => $patient->uid]
                    );
                }
            ?></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <?=$parameters['atrial_burden']->name;?>
            </td>
            <td>
                <?=($parameters['atrial_burden']->getCurrentValue()->value ? : 0) . $parameters['atrial_burden']->unit->name;?>
            </td>
        </tr>
        <tr>
            <td>
                <?=$parameters['atrial_arrhythmia_episodes_per_day']->name;?>
            </td>
            <td>
                <?=$parameters['atrial_arrhythmia_episodes_per_day']->getCurrentValue();?>
            </td>
        </tr>
        <tr>
            <td>
                <?=$parameters['new_long_atrial_episode_detected_ongoing_at_end_of_mon_interv']->name;?>
            </td>
            <td>
                <?=$parameters['new_long_atrial_episode_detected_ongoing_at_end_of_mon_interv']->getCurrentValue();?>
            </td>
        </tr>
        <tr>
            <td>
                <?=$parameters['number_of_mode_switching_per_day']->name;?>
            </td>
            <td>
                <?=$parameters['number_of_mode_switching_per_day']->getMeanByCountValue();?>
            </td>
        </tr>
        <tr>
            <td>
                <?=$parameters['duration_of_mode_switching']->name;?>
            </td>
            <td>
                <?=($parameters['duration_of_mode_switching']->getCurrentValue()->value ? : 0) . $parameters['duration_of_mode_switching']->unit->name;?>
            </td>
        </tr>
        <tr>
            <td>
                <?=$parameters['mean_ventricular_rate_during_mode_switching']->name;?>
            </td>
            <td>
                <?=($parameters['mean_ventricular_rate_during_mode_switching']->getMeanByCountValue() ?
                    $parameters['mean_ventricular_rate_during_mode_switching']->getMeanByCountValue() . '&nbsp;' . $parameters['mean_ventricular_rate_during_mode_switching']->unit->name
                    : DeviceValue::NULL_PLACEHOLDER);
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?=$parameters['new_long_msw_episode_detected_ongoing_at_end_of_mon_interv']->name;?>
            </td>
            <td>
                <?=$parameters['new_long_msw_episode_detected_ongoing_at_end_of_mon_interv']->getCurrentValue();?>
            </td>
        </tr>
    </tbody>
</table>
