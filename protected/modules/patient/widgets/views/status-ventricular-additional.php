<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$graphManager = $this->getGraphManager();

$pvcYAxis = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20];

$headerCell = 'ЖЭС';
$cellValues = [];

if (isset($parameters['mean_pvc_h'])) {
    $cellValues[$parameters['mean_pvc_h']->name] = [
        $graphManager->buildGraph(
            $parameters['mean_pvc_h'],
            null,
            false,
            $pvcYAxis
        ),
        $parameters['mean_pvc_h']->getCurrentValue()->value? : 0,
        ($parameters['mean_pvc_h']->getMeanByCountValue()? : 0)
    ];
}
?>
<?php if (!empty($cellValues)) { ?>
    <table class = "table table-condensed rh-main-table">
        <col width="5%" valign="middle">
        <col width="30%" valign="middle">
        <col width="15%" valign="middle">
        <col width="25%" valign="middle">
        <col width="35%" valign="middle">
        <tbody>
            <tr>
                <th colspan ="4"></th>
                <th class="text-center">
                    <?=Yii::t('PatientModule.device', 'C');?>
                    <?=$this->sinceDateTimeString();?>
                </th>
            </tr>
            <tr>
                <th colspan="3">
                    <?=Yii::t('PatientModule.device', $headerCell);?>
                </th>
                <th class="text-center">
                    24<?=Yii::t('PatientModule.device', 'ч');?>
                </th>
                <th class="text-center">
                    <?=Yii::t('PatientModule.device', 'Средние значения');?>
                </th>
            </tr>
            <?php foreach ($cellValues as $label => $value) { ?>
                <tr>
                    <td>&nbsp;</td>
                    <td><?=$label?></td>
                    <td class="text-center noprint_hidden"><?=$value[0]?></td>
                    <td class="text-center"><?=$value[1]?></td>
                    <td class="text-center"><?=$value[2]?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
<?php } ?>
