<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;

$ra = isset($parameters['ra_pulse_amplitude']);
$rv = isset($parameters['rv_pulse_amplitude']);
$lv = isset($parameters['lv_pulse_amplitude']);

$rapw = isset($parameters['ra_pulse_width']);
$rvpw = isset($parameters['rv_pulse_width']);
$lvpw = isset($parameters['lv_pulse_width']);
?>
<table class = "table table-condensed rh-main-table">
    <col width="30%" valign="middle">
    <col width="20%" valign="middle">
    <col width="26%" valign="middle">
    <col width="8%"  valign="middle">
    <col width="8%"  valign="middle">
    <col width="8%"  valign="middle">
    <thead>
        <tr><th colspan="6"><?=Yii::t('PatientModule.brady', 'Бради');?></th></tr>
    </thead>
    <tbody>
        <tr>
            <td style = "border:0; border-top: 2px solid #CCCCCC;">
                <?=isset($parameters['mode'])?$parameters['mode']:'&nbsp;'?>
            </td>
            <td style = "border:0; border-top: 2px solid #CCCCCC;">
                <?=isset($parameters['mode'])?$parameters['mode']->getCurrentValue():'&nbsp;'?>
            </td>
            <th style = "border:0; border-top: 2px solid #CCCCCC;">&nbsp;</th>
            <th style = "border:0; border-top: 2px solid #CCCCCC;"><?=Yii::t('PatientModule.brady', 'ПП');?></th>
            <th style = "border:0; border-top: 2px solid #CCCCCC;"><?=Yii::t('PatientModule.brady', 'ПЖ');?></th>
            <th style = "border:0; border-top: 2px solid #CCCCCC;"><?=($lv && $lvpw) ? Yii::t('PatientModule.brady', 'ЛЖ') : '';?></th>
        </tr>
        <tr>
            <td>
                <?=isset($parameters['basic_rate'])?$parameters['basic_rate']:'&nbsp;'?>
            </td>
            <td>
                <?=isset($parameters['basic_rate'])?$parameters['basic_rate']->getCurrentValue():'&nbsp;'?>
            </td>
            <td>
                <?=Yii::t('PatientModule.brady', 'Амплитуда импульса');?>
                [<?=$ra?$parameters['ra_pulse_amplitude']->unit->name:''?>]
            </td>
            <td>
                <?=$ra ? $parameters['ra_pulse_amplitude']->getCurrentValue() : '';?>
            </td>
            <td>
                <?=$rv ? $parameters['rv_pulse_amplitude']->getCurrentValue() : '';?>
            </td>
            <td>
                <?=$lv ? $parameters['lv_pulse_amplitude']->getCurrentValue() : '';?>
            </td>
        </tr>
        <tr>
            <td>
                <?=isset($parameters['upper_rate'])?$parameters['upper_rate']:'&nbsp;'?>
            </td>
            <td>
                <?=isset($parameters['upper_rate'])?$parameters['upper_rate']->getCurrentValue():'&nbsp;'?>
            </td>
            <td>
                <?=Yii::t('PatientModule.brady', 'Длительность импульса');?>
                [<?=$rapw?$parameters['ra_pulse_width']->unit->name:''?>]
            </td>
            <td>
                <?=$rapw ? $parameters['ra_pulse_width']->getCurrentValue() : '';?>
            </td>
            <td>
                <?=$rvpw ? $parameters['rv_pulse_width']->getCurrentValue() : '';?>
            </td>
            <td>
                <?=$lvpw ? $parameters['lv_pulse_width']->getCurrentValue() : '';?>
            </td>
        </tr>
        <tr>
            <?php if (isset($parameters['ven_pacing'])) {?>
                <td>
                    <?=$parameters['ven_pacing'];?>
                </td>
                <td>
                    <?=$parameters['ven_pacing']->getCurrentValue()?>
                </td>
                <td colspan ="4"></td>
            <?php }?>
        </tr>

    </tbody>
</table>
