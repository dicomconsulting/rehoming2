<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;

$sinceDate = $patient->device->lastState->getStartTimeOfCurrentValuePeriod(
    $parameters['last_follow_up'],
    true
);

$asVsExist = isset($parameters['intrinsic_rhythm_as_vs']);
$asVpExist = isset($parameters['vat_stimulation_as_vp']);
$apVsExist = isset($parameters['conducted_atrial_pacing_ap_vs']);
$apVpExist = isset($parameters['dual_chamber_pacing_ap_vp']);
$vxVxExist = isset($parameters['vv_sequence_vx_vx']);
$crtExist  = isset($parameters['crt_pacing']);

if ($asVsExist && $asVpExist && $apVsExist && $apVpExist && $vxVxExist) {
    ?>
    <table id="event-episodes" class="table table-condensed rh-main-table">
        <col width="10%" valign="middle" span="6">
        <col width="5%" valign="middle">
        <col width="10%" valign="middle">
        <col width="5%" valign="middle">
        <col width="10%" valign="middle" span='2'>
        <thead>
        <tr>
            <th colspan="11"><?php
                if ($sinceDate instanceof RhDateTime) {
                    echo CHtml::link(
                        Yii::t('PatientModule.arrhythmia', 'Навязанный ритм') . " " .
                        Yii::t('PatientModule.arrhythmia', 'с') . " " .
                        $this->printDateTime($sinceDate, true),
                        ['status/bradycardia', 'id' => $patient->uid]
                    );
                } else {
                    echo CHtml::link(
                        Yii::t('PatientModule.arrhythmia', 'Навязанный ритм'),
                        ['status/bradycardia', 'id' => $patient->uid]
                    );
                }
                ?></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td></td>
            <td colspan="5"></td>
            <td rowspan="4"></td>
            <td rowspan="<?= $crtExist ? 1 : 4; ?>"></td>
            <td rowspan="4"></td>
            <td colspan="2">
                <strong><?= Yii::t('PatientModule.arrhythmia', 'Стимуляция'); ?></strong>
            </td>
        </tr>
        <?php
        $cellHeight  = 58;//высота ячейки с графиком
        $countHeight = function ($height, $percent) {
            return floor(($height / 100) * (int)$percent);
        };

        $asVs = $parameters['intrinsic_rhythm_as_vs']->getMeanByCountValue() ?: 0;
        $asVp = $parameters['vat_stimulation_as_vp']->getMeanByCountValue() ?: 0;
        $apVs = $parameters['conducted_atrial_pacing_ap_vs']->getMeanByCountValue() ?: 0;
        $apVp = $parameters['dual_chamber_pacing_ap_vp']->getMeanByCountValue() ?: 0;
        $vxVx = $parameters['vv_sequence_vx_vx']->getMeanByCountValue() ?: 0;
        if ($crtExist) {
            $crt = $parameters['crt_pacing']->getMeanByCountValue() ?: 0;
        }
        ?>
        <tr>
            <td rowspan="2" class="text-y-axis">
                100%
            </td>
            <td rowspan="2" class="chart-column left-border">
                <?=$asVs;?>%<div style="height:<?=$countHeight($cellHeight, $asVs)?>px;"></div>
            </td>
            <td rowspan="2" class="chart-column">
                <?=$asVp;?>%<div style="height:<?=$countHeight($cellHeight, $asVp);?>px;"></div>
            </td>
            <td rowspan="2" class="chart-column">
                <?=$apVs;?>%<div style="height:<?=$countHeight($cellHeight, $apVs);?>px;"></div>
            </td>
            <td rowspan="2" class="chart-column">
                <?=$apVp;?>%<div style="height:<?=$countHeight($cellHeight, $apVp);?>px;"></div>
            </td>
            <td rowspan="2" class="chart-column right-border">
                <?=$vxVx;?>%<div style="height:<?=$countHeight($cellHeight, $vxVx);?>px;"></div>
            </td>
            <?php if ($crtExist) { ?>
                <td rowspan="2" class="chart-column left-border right-border">
                    <?=$crt;?>%<div style="height:<?=$countHeight($cellHeight, $crt);?>px;"></div>
                </td>
            <?php } ?>
            <td>
                <acronym title="<?= $parameters['atrial_pacing_ap']; ?>">Ap</acronym>
            </td>
            <td>
                <?= ceil($parameters['atrial_pacing_ap']->getMeanByCountValue(1)); ?>&nbsp;<?= $parameters['atrial_pacing_ap']->unit->name; ?>
            </td>
        </tr>
        <?php
        if (array_key_exists('ven_pacing_vp', $parameters)) { ?>
            <tr>
                <td>
                    <acronym title="<?= $parameters['ven_pacing_vp']; ?>">Vp</acronym>
                </td>
                <td>
                    <?= ceil($parameters['ven_pacing_vp']->getMeanByCountValue(1)); ?>&nbsp;<?= $parameters['ven_pacing_vp']->unit->name; ?>
                </td>
            </tr>
        <?php } ?>
        <tr>
            <td class="text-y-axis">0</td>
            <td>
                <acronym title="<?= $parameters['intrinsic_rhythm_as_vs']; ?>">
                    As-Vs
                </acronym>
            </td>
            <td>
                <acronym title="<?= $parameters['vat_stimulation_as_vp']; ?>">
                    As-Vp
                </acronym>
            </td>
            <td>
                <acronym title="<?= $parameters['conducted_atrial_pacing_ap_vs']; ?>">
                    Ap-Vs
                </acronym>
            </td>
            <td>
                <acronym title="<?= $parameters['dual_chamber_pacing_ap_vp']; ?>">
                    Ap-Vp
                </acronym>
            </td>
            <td>
                <acronym title="<?= $parameters['vv_sequence_vx_vx']; ?>">
                    Vx-Vx
                </acronym>
            </td>
            <?php if ($crtExist) { ?>
                <td class="text-center">
                    <acronym title="<?= $parameters['crt_pacing']; ?>">
                        CRT
                    </acronym>
                </td>
            <?php } ?>
            <td colspan="2"></td>
        </tr>
        </tbody>
    </table>
<?php } ?>