<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
?>
<table id='tachy-table' class = "table table-condensed table-bordered rh-main-table">
    <col width="10%" valign="middle">
    <col width="15%" valign="middle" span='6'>
    <thead>
        <tr>
            <th colspan="7">
                <?=Yii::t('PatientModule.tachy', 'Тахи'); ?>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th style="border:0; border-top: 2px solid #CCCCCC;">&nbsp;</th>
            <th style="border:0; border-top: 2px solid #CCCCCC;"><strong>
                <?=Yii::t('PatientModule.tachy', 'Границы зоны');?>
                [<?=isset($parameters['vt1_zone_limit']) ? $parameters['vt1_zone_limit']->unit->name : ''?>]
            </strong></th>
            <th style="border:0; border-top: 2px solid #CCCCCC;"><strong><?=Yii::t('PatientModule.tachy', '1-я АТС');?></strong></th>
            <th style="border:0; border-top: 2px solid #CCCCCC;"><strong><?=Yii::t('PatientModule.tachy', '2-я АТС');?></strong></th>
            <th style="border:0; border-top: 2px solid #CCCCCC;"><strong>
                <?=Yii::t('PatientModule.tachy', '1-й разряд');?>
                [<?=isset($parameters['vt1_1st_shock']) ? $parameters['vt1_1st_shock']->unit->name : ''?>]
            </strong></th>
            <th style="border:0; border-top: 2px solid #CCCCCC;"><strong>
                <?=Yii::t('PatientModule.tachy', '2-й разряд');?>
                    [<?=isset($parameters['vt1_2nd_shock']) ? $parameters['vt1_2nd_shock']->unit->name : ''?>]
            </strong></th>
            <th style="border:0; border-top: 2px solid #CCCCCC;"><strong><?=Yii::t('PatientModule.tachy', '3-й разряд');?></strong></th>
        </tr>
        <?php if (isset($parameters['vt1_zone_limit'])) { ?>
        <tr>
            <th class='border-right' style="border:0;"><strong>VT1</strong></th>
            <td><?=$parameters['vt1_zone_limit']->getCurrentValue();?></td>
            <td><?=$parameters['vt1_1st_atp']->getCurrentValue();?></td>
            <td><?=$parameters['vt1_2nd_atp']->getCurrentValue();?></td>
            <td><?=$parameters['vt1_1st_shock']->getCurrentValue();?></td>
            <td><?=$parameters['vt1_2nd_shock']->getCurrentValue();?></td>
            <td><?=$parameters['vt1_3rd_nth_shock']->getCurrentValue();?></td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['vt2_zone_limit'])) { ?>
        <tr>
            <th class='border-right' style="border:0;"><strong>VT2</strong></th>
            <td><?=$parameters['vt2_zone_limit']->getCurrentValue();?></td>
            <td><?=$parameters['vt2_1st_atp']->getCurrentValue();?></td>
            <td><?=$parameters['vt2_2nd_atp']->getCurrentValue();?></td>
            <td><?=$parameters['vt2_1st_shock']->getCurrentValue();?></td>
            <td><?=$parameters['vt2_2nd_shock']->getCurrentValue();?></td>
            <td><?=$parameters['vt2_3rd_nth_shock']->getCurrentValue();?></td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['vf_zone_limit'])) { ?>
        <tr>
            <th class='border-right' style="border:0;"><strong>VF</strong></th>
            <td><?=$parameters['vf_zone_limit']->getCurrentValue();?></td>
            <td colspan ="2"><?=$parameters['atp_one_shot']->getCurrentValue();?></td>
            <td><?=$parameters['vf_1st_shock']->getCurrentValue();?></td>
            <td><?=$parameters['vf_2nd_shock']->getCurrentValue();?></td>
            <td><?=$parameters['vf_3rd_nth_shock']->getCurrentValue();?></td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['progressive_course_of_therapy'])) { ?>
        <tr>
            <td colspan ="4" style="border-bottom:0;border-left:0;">&nbsp;</td>
            <td colspan ="2" class="left-border"><?=$parameters['progressive_course_of_therapy']->name;?></td>
            <td><?=$parameters['progressive_course_of_therapy']->getCurrentValue();?></td>
        </tr>
        <?php } ?>
    </tbody>
</table>
