<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;

$rv = isset($parameters['rv_pulse_amplitude']);
$rvpw = isset($parameters['rv_pulse_width']);
?>
<table class = "table table-condensed rh-main-table">
    <col width="30%" valign="middle">
    <col width="20%" valign="middle">
    <col width="38%" valign="middle">
    <col width="12%" valign="middle">
    <thead>
        <tr><th colspan="4"><?=Yii::t('PatientModule.brady', 'Бради');?></th></tr>
    </thead>
    <tbody>
        <tr>
            <td style = "border:0; border-top: 2px solid #CCCCCC;">
                <?=isset($parameters['mode'])?$parameters['mode']:'&nbsp;'?>
            </td>
            <td style = "border:0; border-top: 2px solid #CCCCCC;">
                <?=isset($parameters['mode'])?$parameters['mode']->getCurrentValue():'&nbsp;'?>
            </td>
            <th style = "border:0; border-top: 2px solid #CCCCCC;">&nbsp;</th>
            <th style = "border:0; border-top: 2px solid #CCCCCC;"><?=Yii::t('PatientModule.brady', 'ПЖ');?></th>
        </tr>
        <tr>
            <td>
                <?=isset($parameters['basic_rate'])?$parameters['basic_rate']:'&nbsp;'?>
            </td>
            <td>
                <?=isset($parameters['basic_rate'])?$parameters['basic_rate']->getCurrentValue():'&nbsp;'?>
            </td>
            <td>
                <?=Yii::t('PatientModule.brady', 'Амплитуда импульса');?>
                [<?=$rv ? $parameters['rv_pulse_amplitude']->unit->name:''?>]
            </td>
            <td>
                <?=$rv ? $parameters['rv_pulse_amplitude']->getCurrentValue() : '';?>
            </td>
        </tr>
        <tr>
            <td>
                <?=isset($parameters['upper_rate']) ? $parameters['upper_rate']:'&nbsp;'?>
            </td>
            <td>
                <?=isset($parameters['upper_rate']) ? $parameters['upper_rate']->getCurrentValue():'&nbsp;'?>
            </td>
            <td>
                <?=Yii::t('PatientModule.brady', 'Длительность импульса');?>
                [<?=$rvpw ? $parameters['rv_pulse_width']->unit->name:''?>]
            </td>
            <td>
                <?=$rvpw ? $parameters['rv_pulse_width']->getCurrentValue() : '';?>
            </td>
        </tr>
        <tr>
            <?php if (isset($parameters['ven_pacing'])) {?>
                <td>
                    <?=$parameters['ven_pacing'];?>
                </td>
                <td>
                    <?=$parameters['ven_pacing']->getCurrentValue()?>
                </td>
                <td colspan ="2"></td>
            <?}?>
        </tr>

    </tbody>
</table>
