<?php
/* @var $this LeadWidget*/
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$graphManager = $this->getGraphManager();

$pacingThresholdYAxis = [[0, ''], 0.5, 1, 2, 3, 4, 5];
$pacingImpedanceYAxis = [[0, ''], 150, 500, 1000, 1500, 2000, [2500, '']];
$raSensingAmplitudeYAxis = [0, 1, 2, 3, 4, 5, [6, '']];
$rvSensingAmplitudeYAxis = [[0, ''], 1, 5, 10, 15, 20, [25, '']];

$parametersForOutput = [
    'Электрод правого предсердия' => [],
    'Электрод правого желудочка' => [],
];
if (isset($parameters['ra_pacing_impedance'])) {
    $parametersForOutput['Электрод правого предсердия'][
        $parameters['ra_pacing_impedance']->name . ' [' . $parameters['ra_pacing_impedance']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['ra_pacing_impedance'],
            Yii::t('PatientModule.device', 'Импеданс стимула'),
            true,
            $pacingImpedanceYAxis
        ),
        $parameters['ra_pacing_impedance']->getCurrentValue(),
        ($parameters['ra_pacing_impedance']->getMeanByCountValue(0) ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['ra_pacing_threshold_capture_control'])) {
    $parametersForOutput['Электрод правого предсердия'][
    $parameters['ra_pacing_threshold_capture_control']->name . ' [' . $parameters['ra_pacing_threshold_capture_control']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['ra_pacing_threshold_capture_control'],
            Yii::t('PatientModule.device', 'Порог стимула'),
            true,
            $pacingThresholdYAxis
        ),
        $parameters['ra_pacing_threshold_capture_control']->getCurrentValue(),
        ($parameters['ra_pacing_threshold_capture_control']->getMeanByCountValue(1)? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['last_threshold_measurement_ra'])) {
    $parametersForOutput['Электрод правого предсердия'][$parameters['last_threshold_measurement_ra']->name] =
        $this->printDateTime($parameters['last_threshold_measurement_ra']->getCurrentValue()->value);
}
if (isset($parameters['capture_control_status_ra'])) {
    $parametersForOutput['Электрод правого предсердия'][$parameters['capture_control_status_ra']->name] = $parameters['capture_control_status_ra']->getCurrentValue();
}
if (isset($parameters['ra_pulse_amplitude'])) {
    $parametersForOutput['Электрод правого предсердия'][
        $parameters['ra_pulse_amplitude']->name . ' [' . $parameters['ra_pulse_amplitude']->unit->name . ']'
    ] = $parameters['ra_pulse_amplitude']->getCurrentValue();
}

if (isset($parameters['ra_sensing_amplitude_daily_mean'])) {
    $parametersForOutput['Электрод правого предсердия'][
        $parameters['ra_sensing_amplitude_daily_mean']->name . ' [' . $parameters['ra_sensing_amplitude_daily_mean']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['ra_sensing_amplitude_daily_mean'],
            Yii::t('PatientModule.device', 'Амплитуда сигнала правого предсердия'),
            true,
            $raSensingAmplitudeYAxis
        ),
        $parameters['ra_sensing_amplitude_daily_mean']->getCurrentValue(),
        ($parameters['ra_sensing_amplitude_daily_mean']->getMeanByCountValue(1) ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['ra_sensing_amplitude_daily_min'])) {
    $parametersForOutput['Электрод правого предсердия'][
        $parameters['ra_sensing_amplitude_daily_min']->name . ' [' . $parameters['ra_sensing_amplitude_daily_min']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['ra_sensing_amplitude_daily_min'],
            Yii::t('PatientModule.device', 'Амплитуда сигнала правого предсердия'),
            true,
            $raSensingAmplitudeYAxis
        ),
        $parameters['ra_sensing_amplitude_daily_min']->getCurrentValue(),
        ($parameters['ra_sensing_amplitude_daily_min']->getMinByCountValue(1) ? : DeviceValue::NULL_PLACEHOLDER) . '**'
    ];
}
if (isset($parameters['ra_lead_check'])) {
    $parametersForOutput['Электрод правого предсердия'][$parameters['ra_lead_check']->name] = $parameters['ra_lead_check']->getCurrentValue();
}
if (isset($parameters['ra_pacing_polarity'])) {
    $parametersForOutput['Электрод правого предсердия'][$parameters['ra_pacing_polarity']->name] = $parameters['ra_pacing_polarity']->getCurrentValue();
}
if (isset($parameters['ra_sensing_polarity'])) {
    $parametersForOutput['Электрод правого предсердия'][$parameters['ra_sensing_polarity']->name] = $parameters['ra_sensing_polarity']->getCurrentValue();
}

//RV Leads
if (isset($parameters['rv_pacing_impedance'])) {
    $parametersForOutput['Электрод правого желудочка'][
    $parameters['rv_pacing_impedance']->name . ' [' . $parameters['rv_pacing_impedance']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['rv_pacing_impedance'],
            Yii::t('PatientModule.device', 'Импеданс стимула'),
            true,
            $pacingImpedanceYAxis
        ),
        $parameters['rv_pacing_impedance']->getCurrentValue(),
        ($parameters['rv_pacing_impedance']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['rv_pacing_threshold'])) {
    $parametersForOutput['Электрод правого желудочка'][
    $parameters['rv_pacing_threshold']->name . ' [' . $parameters['rv_pacing_threshold']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['rv_pacing_threshold'],
            Yii::t('PatientModule.device', 'Порог стимула'),
            true,
            $pacingThresholdYAxis
        ),
        $parameters['rv_pacing_threshold']->getCurrentValue(),
        ($parameters['rv_pacing_threshold']->getMeanByCountValue(1) ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['last_threshold_measurement_rv'])) {
    $parametersForOutput['Электрод правого желудочка'][$parameters['last_threshold_measurement_rv']->name] =
        $this->printDateTime($parameters['last_threshold_measurement_rv']->getCurrentValue()->value);
}
if (isset($parameters['capture_control_status_rv'])) {
    $parametersForOutput['Электрод правого желудочка'][$parameters['capture_control_status_rv']->name] = $parameters['capture_control_status_rv']->getCurrentValue();
}
if (isset($parameters['rv_pulse_amplitude'])) {
    $parametersForOutput['Электрод правого желудочка'][
    $parameters['rv_pulse_amplitude']->name . ' [' . $parameters['rv_pulse_amplitude']->unit->name . ']'
    ] = $parameters['rv_pulse_amplitude']->getCurrentValue();
}

if (isset($parameters['rv_sensing_amplitude_daily_mean'])) {
    $parametersForOutput['Электрод правого желудочка'][
    $parameters['rv_sensing_amplitude_daily_mean']->name . ' [' . $parameters['rv_sensing_amplitude_daily_mean']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['rv_sensing_amplitude_daily_mean'],
            Yii::t('PatientModule.device', 'Амплитуда сигнала правого желудочка'),
            true,
            $rvSensingAmplitudeYAxis
        ),
        $parameters['rv_sensing_amplitude_daily_mean']->getCurrentValue(),
        ($parameters['rv_sensing_amplitude_daily_mean']->getMeanByCountValue(1) ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['rv_sensing_amplitude_daily_min'])) {
    $parametersForOutput['Электрод правого желудочка'][
    $parameters['rv_sensing_amplitude_daily_min']->name . ' [' . $parameters['rv_sensing_amplitude_daily_min']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['rv_sensing_amplitude_daily_min'],
            Yii::t('PatientModule.device', 'Амплитуда сигнала правого желудочка'),
            true,
            $rvSensingAmplitudeYAxis
        ),
        $parameters['rv_sensing_amplitude_daily_min']->getCurrentValue(),
        ($parameters['rv_sensing_amplitude_daily_min']->getMinByCountValue(1) ? : DeviceValue::NULL_PLACEHOLDER) . '**'
    ];
}
if (isset($parameters['rv_lead_check'])) {
    $parametersForOutput['Электрод правого желудочка'][$parameters['rv_lead_check']->name] = $parameters['rv_lead_check']->getCurrentValue();
}
if (isset($parameters['rv_pacing_polarity'])) {
    $parametersForOutput['Электрод правого желудочка'][$parameters['rv_pacing_polarity']->name] = $parameters['rv_pacing_polarity']->getCurrentValue();
}
if (isset($parameters['rv_sensing_polarity'])) {
    $parametersForOutput['Электрод правого желудочка'][$parameters['rv_sensing_polarity']->name] = $parameters['rv_sensing_polarity']->getCurrentValue();
}

$printFirstHeader = false;
?>

<table class = "table table-condensed rh-main-table">
    <col width="5%" valign="middle">
    <col width="30%" valign="middle">
    <col width="15%" valign="middle">
    <col width="25%" valign="middle">
    <col width="35%" valign="middle">
    <tbody>
        <tr>
            <th colspan ="4"></th>
            <th class="text-center">
                <?=Yii::t('PatientModule.device', 'C');?>
                <?=$this->sinceDateTimeString();?>
            </th>
        </tr>
    <?php foreach ($parametersForOutput as $headerCell => $parameters) { ?>
        <?php if(empty($parameters)) {
            continue;
        } ?>
        <tr>
            <th colspan="3">
                <?=Yii::t('PatientModule.device', $headerCell);?>
            </th>
            <?php if (!$printFirstHeader) { ?>
            <?php $printFirstHeader = true; ?>
            <th class="text-center">
                24<?=Yii::t('PatientModule.device', 'ч');?>
            </th>
            <th class="text-center">
                <?=Yii::t('PatientModule.device', 'Средние значения');?>,
                **<?=Yii::t('PatientModule.device', 'Мин. значения');?>
            </th>
            <?php } else { ?>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            <?php } ?>
        </tr>
        <?php foreach ($parameters as $label => $value) { ?>
            <tr>
                <td>&nbsp;</td>
                <td><?=$label?></td>
                <?php if (is_array($value)) {?>
                    <td class="text-center noprint_hidden"><?=$value[0]?></td>
                    <td class="text-center"><?=$value[1]?></td>
                    <td class="text-center"><?=$value[2]?></td>
                <?php } else { ?>
                    <td class="text-center">&nbsp;</td>
                    <td class="text-center" colspan="2"><?=$value?></td>
                <?php } ?>
            </tr>
        <?php } ?>
    <?php } ?>
    <?php if ($graphManager->countGraphs()) { ?>
        <?php $graphManager->resetGraphs(); ?>
        <?php do { ?>
            <tr id = '<?=$graphManager->getAnchor();?>' class="noprint">
                <th colspan="5">
                    <?=$graphManager->getName();?>
                </th>
            </tr>
            <tr class="noprint">
                <td colspan="5">
                    <?php $graphManager->renderGraph(); ?>
                </td>
            </tr>
        <?php } while ($graphManager->nextGraph() !== false); ?>
    <?php } ?>
    </tbody>
</table>
