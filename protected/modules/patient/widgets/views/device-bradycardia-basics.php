<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
?>
<table class = "table table-condensed table-bordered rh-main-table">
    <col width="60%" valign="middle">
    <col width="40%" valign="middle">
    <thead>
        <tr>
            <th colspan="2">
                <?=Yii::t('PatientModule.device', 'Основные параметры'); ?>
            </th>
        </tr>
    </thead>
    <tbody>
        <?php if (isset($parameters['mode'])) { ?>
        <tr>
            <td>
                <?=$parameters['mode']->name;?>
            </td>
            <td class="text-center">
                <?=$parameters['mode']->getCurrentValue();?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['basic_rate'])) { ?>
        <tr>
            <td>
                <?=$parameters['basic_rate']->name;?>
                [<?=$parameters['basic_rate']->unit->name;?>]
            </td>
            <td class="text-center">
                <?=$parameters['basic_rate']->getCurrentValue();?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['upper_rate'])) { ?>
        <tr>
            <td>
                <?=$parameters['upper_rate']->name;?>
                [<?=$parameters['upper_rate']->unit->name;?>]
            </td>
            <td class="text-center">
                <?=$parameters['upper_rate']->getCurrentValue();?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['maximum_sensor_rate'])) { ?>
        <tr>
            <td>
                <?=$parameters['maximum_sensor_rate']->name;?>
                [<?=$parameters['maximum_sensor_rate']->unit->name;?>]
            </td>
            <td class="text-center">
                <?=$parameters['maximum_sensor_rate']->getCurrentValue();?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['pvarp'])) { ?>
        <tr>
            <td>
                <?=$parameters['pvarp']->name;?>
                [<?=$parameters['pvarp']->unit->name;?>]
            </td>
            <td class="text-center">
                <?=$parameters['pvarp']->getCurrentValue();?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
