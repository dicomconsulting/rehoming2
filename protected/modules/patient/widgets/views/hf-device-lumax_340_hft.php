<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
?>
<table class = "table table-condensed table-bordered rh-main-table">
    <col width="30%" valign="middle">
    <col width="70%" valign="middle">
    <thead>
        <tr>
            <th colspan="2">
                <?=Yii::t('PatientModule.patient', 'Статус устройства');?>
                <?=$this->printDateTime($patient->device->lastState->create_time, true)?>
            </th>
        </tr>
    </thead>
    <tbody>
        <?php if (isset($parameters['device_status'])) { ?>
        <tr>
            <td><?=$parameters['device_status']->name;?></td>
            <td><?=$parameters['device_status']->getCurrentValue();?></td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['battery_status'])) { ?>
        <tr>
            <td><?=$parameters['battery_status']->name;?></td>
            <td>
                <?php
                echo isset($parameters['device_status']) ? $parameters['device_status']->getCurrentValue() . ' (' : '';
                echo $parameters['battery_status']->getCurrentValue();
                echo isset($parameters['device_status']) ? ')' : '';
                ?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['mode'])) { ?>
        <tr>
            <td><?=$parameters['mode']->name;?></td>
            <td>
                <?=$parameters['mode']->getCurrentValue();?>
                (<?=$parameters['ven_pacing']->getCurrentValue() . '-' . $parameters['initially_paced_chamber']->getCurrentValue();?>)
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
