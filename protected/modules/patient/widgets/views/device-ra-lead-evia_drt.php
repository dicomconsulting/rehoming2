<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;

$parametersForOutput = [
    'Электрод ПП' => [
        'progr_atrial_pulse_amplitude',
        'atrial_pulse_width',
        'progr_atrial_pacing_polarity',
        'progr_atrial_sensing_polarity',
        'atrial_sensitivity'


    ],
    'Контроль захвата ПП' => [
        'ra_capture_control_mode',
        'min_amplitude_ra_capture_control',
        'threshold_test_start_ra_capture_control',
        'ra_safety_margin_capture_control',
        'search_type_ra_capture_control',
        'search_interval_ra_capture_control',
        'time_of_day_ra_capture_control'
,    ]
];
?>
<table class = "table table-condensed rh-main-table">
    <col width="5%" valign="middle">
    <col width="55%" valign="middle">
    <col width="40%" valign="middle">
    <tbody>
    <?php foreach ($parametersForOutput as $headerCell => $parameterNames) { ?>
        <tr>
            <th colspan="3">
                <?=Yii::t('PatientModule.device', $headerCell);?>
            </th>
        </tr>
        <?php foreach ($parameterNames as $parameterName) { ?>
            <?php if (isset($parameters[$parameterName])) { ?>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <?=$parameters[$parameterName]?>
                        <?php if (!empty($parameters[$parameterName]->unit)) { ?>
                            [<?=$parameters[$parameterName]->unit->name?>]
                        <?php } ?>
                    </td>
                    <td class="text-center">
                        <?=$parameters[$parameterName]->getCurrentValue();?>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>
