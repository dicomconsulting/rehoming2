<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;

$parametersForOutput = [
    'Основные параметры' => [
        'mode',
        'basic_rate',
        'night_rate',
        'night_begins',
        'night_ends',
        'magnet_response',
        'hysteresis',
        'repetitive',
        'scan'
    ],
    'Сенсор/сглаживание ритма' => [
        'max_activity_rate',
        'sensor_gain',
        'auto_gain',
        'sensor_threshold',
        'rate_fading',
        'rate_increase',
        'rate_decrease'
    ],
    'Верхний ритм отклика' => [
        'upper_tracking_rate',
        'atrial_upper_rate',
        '2_1_rate'
    ],
    'Подавление Vp' => [
        'suppress_pacing_after_x_consecutive_vs',
        'support_pacing_after_x_out_of_8_cycles_without_vs'
    ]
];
?>
<table class = "table table-condensed rh-main-table">
    <col width="5%" valign="middle">
    <col width="55%" valign="middle">
    <col width="40%" valign="middle">
    <tbody>
    <?php foreach ($parametersForOutput as $headerCell => $parameterNames) { ?>
        <tr>
            <th colspan="3">
                <?=Yii::t('PatientModule.device', $headerCell);?>
            </th>
        </tr>
        <?php foreach ($parameterNames as $parameterName) { ?>
            <?php if (isset($parameters[$parameterName])) { ?>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <?=$parameters[$parameterName]?>
                        <?php if (!empty($parameters[$parameterName]->unit)) { ?>
                            [<?=$parameters[$parameterName]->unit->name?>]
                        <?php } ?>
                    </td>
                    <td class="text-center">
                        <?=$parameters[$parameterName]->getCurrentValue();?>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>
