<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
?>
<?php if (isset($parameters['at_af_rate'])) { ?>
    <table class = "table table-condensed table-bordered rh-main-table">
        <col width="60%" valign="middle">
        <col width="40%" valign="middle">
        <thead>
            <tr>
                <th colspan="2">
                    <?=Yii::t('PatientModule.device', 'Зона мониторинга предсердных аритмий'); ?>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <?=$parameters['at_af_rate']->name;?>
                    [<?=$parameters['at_af_rate']->unit->name;?>]
                </td>
                <td>
                    <?=$parameters['at_af_rate']->getCurrentValue();?>
                </td>
            </tr>
        </tbody>
    </table>
<?php } ?>