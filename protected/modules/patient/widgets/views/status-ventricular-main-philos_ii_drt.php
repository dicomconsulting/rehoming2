<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$graphManager = $this->getGraphManager();

$ventricularHeartRateYAxis = [[60, ''], 90, 100, 120, 140, 160, 180, 200, [230, '']];
$maxPvcYAxis = [0, 10, 30, [40, '']];
$venRunsYAxis = [0, 1, 2, 3, 4, 5, 10, [15, '']];
$venEpisodesYAxis = [0, 1, 2, [3, '']];

$parametersForOutput = [
    'Эпизоды высокого желудочкового ритма' => [],
    'ЖЭС' => []
];

if (isset($parameters['max_ventricular_heart_rate'])) {
    $parametersForOutput['Эпизоды высокого желудочкового ритма'][
    $parameters['max_ventricular_heart_rate']->name . ' [' . $parameters['max_ventricular_heart_rate']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['max_ventricular_heart_rate'],
            Yii::t('PatientModule.device', 'Эпизоды высокого желудочкового ритма'),
            true,
            $ventricularHeartRateYAxis
        ),
        $parameters['max_ventricular_heart_rate']->getCurrentValue(),
        ($parameters['max_ventricular_heart_rate']->getMaxByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['duration_of_max_ventricular_heart_rate_episode'])) {
    $parametersForOutput['Эпизоды высокого желудочкового ритма'][
    $parameters['duration_of_max_ventricular_heart_rate_episode']->name . ' [' . $parameters['duration_of_max_ventricular_heart_rate_episode']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['duration_of_max_ventricular_heart_rate_episode'],
            Yii::t('PatientModule.device', 'Эпизоды высокого желудочкового ритма'),
            true,
            $ventricularHeartRateYAxis
        ),
        $parameters['duration_of_max_ventricular_heart_rate_episode']->getCurrentValue(),
        ($parameters['duration_of_max_ventricular_heart_rate_episode']->getMaxByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}


if (isset($parameters['max_pvc_h'])) {
    $parametersForOutput['ЖЭС'][
    $parameters['max_pvc_h']->name
    ] = [
        $graphManager->buildGraph(
            $parameters['max_pvc_h'],
            null,
            false,
            $maxPvcYAxis
        ),
        $parameters['max_pvc_h']->getCurrentValue(),
        ($parameters['max_pvc_h']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['no_of_ven_runs_4_8_consec_pvc_per_day'])) {
    $parametersForOutput['ЖЭС'][
    $parameters['no_of_ven_runs_4_8_consec_pvc_per_day']->name
    ] = [
        $graphManager->buildGraph(
            $parameters['no_of_ven_runs_4_8_consec_pvc_per_day'],
            null,
            false,
            $venRunsYAxis
        ),
        $parameters['no_of_ven_runs_4_8_consec_pvc_per_day']->getCurrentValue(),
        ($parameters['no_of_ven_runs_4_8_consec_pvc_per_day']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['no_of_ven_episodes_8_consec_pvc_per_day'])) {
    $parametersForOutput['ЖЭС'][
    $parameters['no_of_ven_episodes_8_consec_pvc_per_day']->name
    ] = [
        $graphManager->buildGraph(
            $parameters['no_of_ven_episodes_8_consec_pvc_per_day'],
            null,
            false,
            $venEpisodesYAxis
        ),
        $parameters['no_of_ven_episodes_8_consec_pvc_per_day']->getCurrentValue(),
        ($parameters['no_of_ven_episodes_8_consec_pvc_per_day']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}

$printFirstHeader = false;
?>
<table class = "table table-condensed rh-main-table">
    <col width="5%" valign="middle">
    <col width="30%" valign="middle">
    <col width="15%" valign="middle">
    <col width="25%" valign="middle">
    <col width="35%" valign="middle">
    <tbody>
    <tr>
        <th colspan ="4"></th>
        <th class="text-center">
            <?=Yii::t('PatientModule.device', 'C');?>
            <?=$this->sinceDateTimeString();?>
        </th>
    </tr>
    <?php foreach ($parametersForOutput as $headerCell => $parameters) { ?>
        <?php if(empty($parameters)) {
            continue;
        } ?>
        <tr>
            <th colspan="3">
                <?=Yii::t('PatientModule.device', $headerCell);?>
            </th>
            <?php if (!$printFirstHeader) { ?>
                <?php $printFirstHeader = true; ?>
                <th class="text-center">
                    24<?=Yii::t('PatientModule.device', 'ч');?>
                </th>
                <th class="text-center">
                    *<?=Yii::t('PatientModule.device', 'Макс. значения');?>
                </th>
            <?php } else { ?>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            <?php } ?>
        </tr>
        <?php foreach ($parameters as $label => $value) { ?>
            <tr>
                <td>&nbsp;</td>
                <td><?=$label?></td>
                <?php if (is_array($value)) {?>
                    <td class="text-center noprint_hidden"><?=$value[0]?></td>
                    <td class="text-center"><?=$value[1]?></td>
                    <td class="text-center"><?=$value[2]?>*</td>
                <?php } else { ?>
                    <td class="text-center">&nbsp;</td>
                    <td class="text-center" colspan="2"><?=$value?></td>
                <?php } ?>
            </tr>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>
