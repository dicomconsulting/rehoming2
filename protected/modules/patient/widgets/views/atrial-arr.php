<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$sinceDate = $patient->device->lastState->getStartTimeOfCurrentValuePeriod(
    $parameters['last_follow_up'],
    true
);
?>
<table class = "table table-condensed table-bordered rh-main-table">
    <col width="80%" valign="middle">
    <col width="20%" valign="middle">
    <thead>
        <tr>
            <th colspan="2"><?php
                if ($sinceDate instanceof RhDateTime) {
                    echo CHtml::link(
                        Yii::t('PatientModule.arrhythmia', 'Предсердная аритмия') . " " .
                        Yii::t('PatientModule.arrhythmia', 'с') . " " .
                        $this->printDateTime($sinceDate, true),
                        ['status/atrial', 'id' => $patient->uid]
                    );
                } else {
                    echo CHtml::link(
                        Yii::t('PatientModule.arrhythmia', 'Предсердная аритмия'),
                        ['status/atrial', 'id' => $patient->uid]
                    );
                }
            ?></th>
        </tr>
    </thead>
    <tbody>
        <?php if (isset($parameters['atrial_burden'])) { ?>
        <tr>
            <td>
            <?=$parameters['atrial_burden']->name;?>
            </td>
            <td>
            <?=($parameters['atrial_burden']->getCurrentValue()->value?:0) . $parameters['atrial_burden']->unit->name;?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['mean_ven_heart_rate_during_atr_burden'])) { ?>
        <tr>
            <td>
            <?=$parameters['mean_ven_heart_rate_during_atr_burden']->name;?>
            </td>
            <td>
            <?php $atrBurden = $parameters['mean_ven_heart_rate_during_atr_burden']->getMeanByCountValue();
                echo $parameters['mean_ven_heart_rate_during_atr_burden']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER;
                echo $atrBurden ? ' ' . $parameters['mean_ven_heart_rate_during_atr_burden']->unit->name: '';?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['atrial_arrhythmia_ongoing_at_end_of_mon_interv'])) { ?>
        <tr>
            <td>
            <?=$parameters['atrial_arrhythmia_ongoing_at_end_of_mon_interv']->name;?>
            </td>
            <td>
            <?=$parameters['atrial_arrhythmia_ongoing_at_end_of_mon_interv']->getCurrentValue()?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['atrial_monitoring_episodes'])) { ?>
        <tr>
            <td>
            <?=$parameters['atrial_monitoring_episodes']->name;?>
            </td>
            <td>
            <?=$parameters['atrial_monitoring_episodes']->getCountByCurrentPeriod()?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['number_of_mode_switching_per_day'])) { ?>
        <tr>
            <td>
            <?=$parameters['number_of_mode_switching_per_day']->name;?>
            </td>
            <td>
            <?=$parameters['number_of_mode_switching_per_day']->getMeanByCountValue();?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['svt_episodes_total'])) { ?>
        <tr>
            <td>
            <?=$parameters['svt_episodes_total']->name;?>
            </td>
            <td>
            <?=$parameters['svt_episodes_total']->getCountByCurrentPeriod() ? : 0?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
