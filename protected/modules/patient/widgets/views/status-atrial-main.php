<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$graphManager = $this->getGraphManager();

$atrialBurdenYAxis = [0, 5, 10, 15, 20, 25];
$venHeartRate = [[0, ''], 40, 60, 80, 100, 120, 140, 160, [180, '']];
$modeSwitchingYAxis = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20];

$headerCell = 'Доля предсердных сокращений';
$cellValues = [];


if (isset($parameters['atrial_burden'])) {
    $cellValues[
        $parameters['atrial_burden']->name . ' [' . $parameters['atrial_burden']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['atrial_burden'],
            Yii::t('PatientModule.device', 'Доля предсердных сокращений'),
            true,
            $atrialBurdenYAxis
        ),
        $parameters['atrial_burden']->getCurrentValue(),
        ($parameters['atrial_burden']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['atrial_arrhythmia_ongoing_at_end_of_mon_interv'])) {
    $cellValues[
        $parameters['atrial_arrhythmia_ongoing_at_end_of_mon_interv']->name
    ] = $parameters['atrial_arrhythmia_ongoing_at_end_of_mon_interv']->getCurrentValue();
}
if (isset($parameters['mean_ven_heart_rate_during_atr_burden'])) {
    $cellValues[
        $parameters['mean_ven_heart_rate_during_atr_burden']->name . ' [' . $parameters['mean_ven_heart_rate_during_atr_burden']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['mean_ven_heart_rate_during_atr_burden'],
            Yii::t('PatientModule.device', 'Желудочковая частота во время предсердной аритмии'),
            true,
            $venHeartRate
        ),
        $parameters['mean_ven_heart_rate_during_atr_burden']->getCurrentValue(),
        ($parameters['mean_ven_heart_rate_during_atr_burden']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['max_ven_heart_rate_during_atr_burden'])) {
    $cellValues[
        $parameters['max_ven_heart_rate_during_atr_burden']->name . ' [' . $parameters['max_ven_heart_rate_during_atr_burden']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['max_ven_heart_rate_during_atr_burden'],
            Yii::t('PatientModule.device', 'Желудочковая частота во время предсердной аритмии'),
            true,
            $venHeartRate
        ),
        $parameters['max_ven_heart_rate_during_atr_burden']->getCurrentValue(),
        ($parameters['max_ven_heart_rate_during_atr_burden']->getMaxValue()? : DeviceValue::NULL_PLACEHOLDER) . '*'
    ];
}
if (isset($parameters['number_of_mode_switching_per_day'])) {
    $cellValues[$parameters['number_of_mode_switching_per_day']->name] = [
        $graphManager->buildGraph(
            $parameters['number_of_mode_switching_per_day'],
            Yii::t('PatientModule.device', 'Переключение режимов'),
            true,
            $modeSwitchingYAxis
        ),
        $parameters['number_of_mode_switching_per_day']->getCurrentValue(),
        ($parameters['number_of_mode_switching_per_day']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
?>
<?php if (!empty($cellValues)) { ?>
    <table class = "table table-condensed rh-main-table">
        <col width="5%" valign="middle">
        <col width="30%" valign="middle">
        <col width="15%" valign="middle">
        <col width="25%" valign="middle">
        <col width="35%" valign="middle">
        <tbody>
            <tr>
                <th colspan ="4"></th>
                <th class="text-center">
                    <?=Yii::t('PatientModule.device', 'C');?>
                    <?=$this->sinceDateTimeString();?>
                </th>
            </tr>
            <tr>
                <th colspan="3">
                    <?=Yii::t('PatientModule.device', $headerCell);?>
                </th>
                <th class="text-center">
                    24<?=Yii::t('PatientModule.device', 'ч');?>
                </th>
                <th class="text-center">
                    <?=Yii::t('PatientModule.device', 'Средние значения');?>,<br/>
                    *<?=Yii::t('PatientModule.device', 'Макс. значения');?>
                </th>
            </tr>
            <?php foreach ($cellValues as $label => $value) { ?>
                <tr>
                    <td>&nbsp;</td>
                    <td><?=$label?></td>
                    <?php if (is_array($value)) {?>
                        <td class="text-center noprint_hidden"><?=$value[0]?></td>
                        <td class="text-center"><?=$value[1]?></td>
                        <td class="text-center"><?=$value[2]?></td>
                    <?php } else { ?>
                        <td class="text-center">&nbsp;</td>
                        <td class="text-center" colspan="2"><?=$value?></td>
                    <?php } ?>
                </tr>
            <?php } ?>
        </tbody>
    </table>
<?php } ?>
