<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$rowArray = [];
$rowArray[] = [
    $parameters['mode']->name =>
        $parameters['mode']->getCurrentValue(),
    $parameters['pulse_amplitude']->name . '&nbsp;[' . $parameters['pulse_amplitude']->unit->name . ']' =>
        $parameters['pulse_amplitude']->getCurrentValue()
];
$rowArray[] = [
    $parameters['basic_rate']->name . '&nbsp;[' . $parameters['basic_rate']->unit->name . ']' =>
        $parameters['basic_rate']->getCurrentValue(),
    $parameters['pulse_width']->name . '&nbsp;[' . $parameters['pulse_width']->unit->name . ']' =>
        $parameters['pulse_width']->getCurrentValue()
];
$rowArray[] = [
    $parameters['night_rate']->name . '&nbsp;[' . $parameters['night_rate']->unit->name . ']' => $parameters['night_rate']->getCurrentValue(),
    $parameters['rv_capture_control_mode']->name =>
        $parameters['rv_capture_control_mode']->getCurrentValue()
];
$rowArray[] = [
    $parameters['max_activity_rate']->name . '&nbsp;[' . $parameters['max_activity_rate']->unit->name . ']' => $parameters['max_activity_rate']->getCurrentValue(),
    $parameters['sensitivity']->name . '&nbsp;[' . $parameters['sensitivity']->unit->name . ']' =>
        $parameters['sensitivity']->getCurrentValue()
];
$rowArray[] = [
    $parameters['rate_fading']->name => $parameters['rate_fading']->getCurrentValue(),
    $parameters['refractory_period']->name . '&nbsp;[' . $parameters['refractory_period']->unit->name . ']' =>
        $parameters['refractory_period']->getCurrentValue()
];
$rowArray[] = [
    '&nbsp;' => '&nbsp;',
    $parameters['progr_pacing_polarity']->name =>
        $parameters['progr_pacing_polarity']->getCurrentValue()
];
$rowArray[] = [
    '&nbsp;' => '&nbsp;',
    $parameters['progr_sensing_polarity']->name =>
        $parameters['progr_sensing_polarity']->getCurrentValue()
];

?>
<table class = "table table-condensed rh-main-table">
    <col width="35%" valign="middle">
    <col width="15%" valign="middle">
    <col width="35%" valign="middle">
    <col width="15%" valign="middle">
    <thead>
        <tr>
            <th colspan="4">&nbsp</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($rowArray as $row) { ?>
        <tr>
            <td>
                <?=key($row)?>
            </td>
            <td style = "border-right: 1px solid #CCCCCC;">
                <?=current($row);?>
            </td>
            <?php next($row); ?>
            <td>
                <?=key($row)?>
            </td>
            <td>
                <?=current($row);?>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
