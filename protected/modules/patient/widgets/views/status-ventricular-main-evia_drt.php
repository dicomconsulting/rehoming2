<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$graphManager = $this->getGraphManager();

$rateEpisodesYAxis = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20];
$meanPvcYAxis = [0, 10, 20, 30, 40, 50];

$parametersForOutput = [
    'Эпизоды высокого желудочкового ритма' => [],
    'ЖЭС' => []
];

if (isset($parameters['high_ven_rate_episodes_per_day'])) {
    $parametersForOutput['Эпизоды высокого желудочкового ритма'][
    $parameters['high_ven_rate_episodes_per_day']->name
    ] = [
        $graphManager->buildGraph(
            $parameters['high_ven_rate_episodes_per_day'],
            Yii::t('PatientModule.device', 'Эпизоды высокого желудочкового ритма'),
            false,
            $rateEpisodesYAxis
        ),
        $parameters['high_ven_rate_episodes_per_day']->getCurrentValue(),
        ($parameters['high_ven_rate_episodes_per_day']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['duration_of_longest_high_ven_rate_episode_per_day'])) {
    $parametersForOutput['Эпизоды высокого желудочкового ритма'][
        $parameters['duration_of_longest_high_ven_rate_episode_per_day']->name
    ] = [
        '',
        $parameters['duration_of_longest_high_ven_rate_episode_per_day']->getCurrentValue(),
        ($parameters['duration_of_longest_high_ven_rate_episode_per_day']->getMaxByCountValue() ? : DeviceValue::NULL_PLACEHOLDER) . '*'
    ];
}
if (isset($parameters['start_of_ongoing_high_ven_rate_episode'])) {
    $parametersForOutput['Эпизоды высокого желудочкового ритма'][
        $parameters['start_of_ongoing_high_ven_rate_episode']->name
    ] = $parameters['start_of_ongoing_high_ven_rate_episode']->getCurrentValue();
}


if (isset($parameters['mean_pvc_h'])) {
    $parametersForOutput['ЖЭС'][
        $parameters['mean_pvc_h']->name
    ] = [
        $graphManager->buildGraph(
            $parameters['mean_pvc_h'],
            null,
            false,
            $meanPvcYAxis
        ),
        $parameters['mean_pvc_h']->getCurrentValue(),
        ($parameters['mean_pvc_h']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}

$printFirstHeader = false;
?>
<table class = "table table-condensed rh-main-table">
    <col width="5%" valign="middle">
    <col width="30%" valign="middle">
    <col width="15%" valign="middle">
    <col width="25%" valign="middle">
    <col width="35%" valign="middle">
    <tbody>
    <tr>
        <th colspan ="4"></th>
        <th class="text-center">
            <?=Yii::t('PatientModule.device', 'C');?>
            <?=$this->sinceDateTimeString();?>
        </th>
    </tr>
    <?php foreach ($parametersForOutput as $headerCell => $parameters) { ?>
        <?php if(empty($parameters)) {
            continue;
        } ?>
        <tr>
            <th colspan="3">
                <?=Yii::t('PatientModule.device', $headerCell);?>
            </th>
            <?php if (!$printFirstHeader) { ?>
                <?php $printFirstHeader = true; ?>
                <th class="text-center">
                    24<?=Yii::t('PatientModule.device', 'ч');?>
                </th>
                <th class="text-center">
                    <?=Yii::t('PatientModule.device', 'Средние значения');?><br/>
                    *<?=Yii::t('PatientModule.device', 'Макс. значения');?>
                </th>
            <?php } else { ?>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            <?php } ?>
        </tr>
        <?php foreach ($parameters as $label => $value) { ?>
            <tr>
                <td>&nbsp;</td>
                <td><?=$label?></td>
                <?php if (is_array($value)) {?>
                    <td class="text-center noprint_hidden"><?=$value[0]?></td>
                    <td class="text-center"><?=$value[1]?></td>
                    <td class="text-center"><?=$value[2]?></td>
                <?php } else { ?>
                    <td class="text-center">&nbsp;</td>
                    <td class="text-center" colspan="2"><?=$value?></td>
                <?php } ?>
            </tr>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>
