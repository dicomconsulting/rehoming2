<?php
/* @var $patient Patient */
/* @var $messages Message[] */
/* @var $this CWidget */
$isAllowedViewPersonalData =  Yii::app()->user->checkAccess('viewPersonalData');

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->assetManager->publish(
        Yii::getPathOfAlias('patient.assets').'/js/widgets/last-messages.js'
    ),
    CClientScript::POS_END
);
?>
<?php if(count($messages)): ?>
<table class="table table-bordered rh-main-table rh-no-padding-table noprint">
    <?php foreach ($messages as $message):?>
        <tr id="sm_<?=$message->getUid()?>" class="finding-row<?php if(!$message->isAcknowledged()):?> actual-finding<?php endif?>">
            <td style="vertical-align: top; background-color: #fdf06d;"><div class="status_icon_wrapper<?php if(!$message->isActual() && $message->isAcknowledged()):?> hidden<?php endif?>">
                <?php if($message->getLevel() == AbstractOption::ALERT_LEVEL_YELLOW || $message->getLevel() == AbstractOption::ALERT_LEVEL_YELLOW_WITH_NOTICE ): ?>
                    <img src="/images/AlertLargeAmber.gif" width="32" height="50">
                <?php elseif($message->getLevel() == AbstractOption::ALERT_LEVEL_RED_WITH_NOTICE):?>
                    <img src="/images/AlertLargeRed.gif" width="32" height="50">
                <?php else:?>
                    <img src="/images/AlertLargeWhite.gif" width="32" height="50">
                <?php endif;?>
                 </div>
            </td>
            <td style="padding-left: 20px;padding-right20px; font-size: 12px;"><?=$message->formatMessage();?></td>
            <td>
                <?php
                if (Yii::app()->user->checkAccess("acknowledgeMessage") && !$message->isAcknowledged()) {
                    $buttonTitle = Yii::t("general", "Уведомлен");
                    echo CHtml::ajaxButton($buttonTitle,
                        $this->controller->createUrl('statusMessage/toggleAcknowledge'),
                        array(
                            "data" => array("id" => $patient->uid, "status_message_uid" => $message->getUid()),
                            "success" => "js:function (data) {handleAcknowledgeToggled(data);}"
                        ));
                }
                ?>
            </td>
        </tr>
<?php endforeach;?>
</table>
<?php endif;?>