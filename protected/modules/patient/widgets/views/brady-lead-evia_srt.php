<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
?>
<table class = "table table-condensed rh-main-table">
    <col width="80%" valign="middle">
    <col width="20%" valign="middle" align="center">
    <thead>
        <tr>
            <th><?=Yii::t('PatientModule.brady', 'Бради электроды');?></th>
            <th class="text-center"><?=Yii::t('PatientModule.brady', 'Электрод');?></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <?=$parameters['pacing_impedance']->name .'&nbsp;[' . $parameters['pacing_impedance']->unit->name. ']';?>
            </td>
            <td class="text-center">
                <?=$parameters['pacing_impedance']->getCurrentValue();?>
            </td>
        </tr>
        <tr>
            <td>
                <?=$parameters['rv_pacing_threshold_capture_control']->name .'&nbsp;[' . $parameters['rv_pacing_threshold_capture_control']->unit->name. ']';?>
            </td>
            <td class="text-center">
                <?=$parameters['rv_pacing_threshold_capture_control']->getCurrentValue();?>
            </td>
        </tr>
        <tr>
            <td>
                <?=Yii::t('PatientModule.brady', 'Амплитуда сигнала средняя/минимальная') .'&nbsp;[' . $parameters['sensing_amplitude_daily_mean']->unit->name. ']';?>
            </td>
            <td class="text-center">
                <?=$parameters['sensing_amplitude_daily_mean']->getCurrentValue();?>
                /
                <?=$parameters['sensing_amplitude_daily_min']->getCurrentValue();?>
            </td>
        </tr>
    </tbody>
</table>
