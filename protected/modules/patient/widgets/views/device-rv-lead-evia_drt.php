<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;

$parametersForOutput = [
    'Электрод ПЖ' => [
        'progr_ven_pulse_amplitude',
        'ven_pulse_width',
        'progr_ven_pacing_polarity',
        'progr_ven_sensing_polarity',
        'ven_sensitivity'


    ],
    'Контроль захвата ПЖ' => [
        'rv_capture_control_mode',
        'min_amplitude_rv_capture_control',
        'threshold_test_start_rv_capture_control',
        'rv_safety_margin_capture_control',
        'search_type_rv_capture_control',
        'search_interval_rv_capture_control',
        'time_of_day_rv_capture_control'
    ]
];
?>
<table class = "table table-condensed rh-main-table">
    <col width="5%" valign="middle">
    <col width="55%" valign="middle">
    <col width="40%" valign="middle">
    <tbody>
    <?php foreach ($parametersForOutput as $headerCell => $parameterNames) { ?>
        <tr>
            <th colspan="3">
                <?=Yii::t('PatientModule.device', $headerCell);?>
            </th>
        </tr>
        <?php foreach ($parameterNames as $parameterName) { ?>
            <?php if (isset($parameters[$parameterName])) { ?>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <?=$parameters[$parameterName]?>
                        <?php if (!empty($parameters[$parameterName]->unit)) { ?>
                            [<?=$parameters[$parameterName]->unit->name?>]
                        <?php } ?>
                    </td>
                    <td class="text-center">
                        <?=$parameters[$parameterName]->getCurrentValue();?>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>
