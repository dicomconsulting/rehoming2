<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$sinceDate = $patient->device->lastState->getStartTimeOfCurrentValuePeriod(
    $parameters['last_follow_up'],
    true
);

if (isset($parameters['ven_pacing_vp'])) {
?>
<table id="event-episodes" class = "table table-condensed rh-main-table">
    <col width="65%" valign="middle">
    <col width="25%" valign="middle">
    <thead>
        <tr>
            <th colspan="2"><?php
                if ($sinceDate instanceof RhDateTime) {
                    echo CHtml::link(
                        Yii::t('PatientModule.arrhythmia', 'Навязанный ритм') . " " .
                        Yii::t('PatientModule.arrhythmia', 'с') . " " .
                        $this->printDateTime($sinceDate, true),
                        ['status/bradycardia', 'id' => $patient->uid]
                    );
                } else {
                    echo CHtml::link(
                        Yii::t('PatientModule.arrhythmia', 'Навязанный ритм'),
                        ['status/bradycardia', 'id' => $patient->uid]
                    );
                }
            ?></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <?=$parameters['ven_pacing_vp'];?>&nbsp;(Vp)
            </td>
            <td>
                <?=$parameters['ven_pacing_vp']->getMeanByCountValue();?>
                <?=$parameters['ven_pacing_vp']->unit->name;?>
            </td>
        </tr>
    </tbody>
</table>
<?php } ?>