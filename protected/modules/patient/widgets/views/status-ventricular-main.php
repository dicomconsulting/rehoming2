<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$graphManager = $this->getGraphManager();

$vtEpisodesYAxis = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20];
$vfEpisodesYAxis = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20];
$svtEpisodesYAxis = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20];
$venAtpYAxis = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20];
$atpOneShotYAxis = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20];
$shocksYAxis = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20];

$parametersForOutput = [
    'Желудочковая детекция' => [],
    'Желудочковая АТС' => [],
    'Желудочковые шоки' => []
];

if (isset($parameters['vt1_episodes'])) {
    $parametersForOutput['Желудочковая детекция'][
        $parameters['vt1_episodes']->name
    ] = [
        $graphManager->buildGraph(
            $parameters['vt1_episodes'],
            Yii::t('PatientModule.device', 'VT1/VT2 эпизоды'),
            true,
            $vtEpisodesYAxis
        ),
        $parameters['vt1_episodes']->getCountByCurrentPeriod(),
        ($parameters['vt1_episodes']->getCurrentValue()->value ? : 0)
    ];
}
if (isset($parameters['vt2_episodes'])) {
    $parametersForOutput['Желудочковая детекция'][
        $parameters['vt2_episodes']->name
    ] = [
        $graphManager->buildGraph(
            $parameters['vt2_episodes'],
            Yii::t('PatientModule.device', 'VT1/VT2 эпизоды'),
            true,
            $vtEpisodesYAxis
        ),
        $parameters['vt2_episodes']->getCountByCurrentPeriod(),
        ($parameters['vt2_episodes']->getCurrentValue()->value ? : 0)
    ];
}
if (isset($parameters['vf_episodes'])) {
    $parametersForOutput['Желудочковая детекция'][
        $parameters['vf_episodes']->name
    ] = [
        $graphManager->buildGraph(
            $parameters['vf_episodes'],
            Yii::t('PatientModule.device', 'VF эпизоды'),
            true,
            $vfEpisodesYAxis
        ),
        $parameters['vf_episodes']->getCountByCurrentPeriod(),
        ($parameters['vf_episodes']->getCurrentValue()->value ? : 0)
    ];
}
if (isset($parameters['episodes_during_temporary_program'])) {
    $parametersForOutput['Желудочковая детекция'][
        $parameters['episodes_during_temporary_program']->name
    ] = [
        '',
        $parameters['episodes_during_temporary_program']->getCountByCurrentPeriod(),
        ($parameters['episodes_during_temporary_program']->getCurrentValue()->value ? : 0)
    ];
}
if (isset($parameters['svt_episodes_total'])) {
    $parametersForOutput['Желудочковая детекция'][
        $parameters['svt_episodes_total']->name
    ] = [
        $graphManager->buildGraph(
            $parameters['svt_episodes_total'],
            Yii::t('PatientModule.device', 'Эпизоды НЖТ'),
            true,
            $svtEpisodesYAxis
        ),
        $parameters['svt_episodes_total']->getCountByCurrentPeriod(),
        ($parameters['svt_episodes_total']->getCurrentValue()->value ? : 0)
    ];
}

if (isset($parameters['atp_in_vt_zones_started'])) {
    $parametersForOutput['Желудочковая АТС'][
        $parameters['atp_in_vt_zones_started']->name
    ] = [
        $graphManager->buildGraph(
            $parameters['atp_in_vt_zones_started'],
            Yii::t('PatientModule.device', 'Желудочковая АТС'),
            true,
            $venAtpYAxis
        ),
        $parameters['atp_in_vt_zones_started']->getCountByCurrentPeriod(),
        ($parameters['atp_in_vt_zones_started']->getCurrentValue()->value ? : 0)
    ];
}
if (isset($parameters['atp_in_vt_zones_successful'])) {
    $parametersForOutput['Желудочковая АТС'][
        $parameters['atp_in_vt_zones_successful']->name
    ] = [
        $graphManager->buildGraph(
            $parameters['atp_in_vt_zones_successful'],
            Yii::t('PatientModule.device', 'Желудочковая АТС'),
            true,
            $venAtpYAxis
        ),
        $parameters['atp_in_vt_zones_successful']->getCountByCurrentPeriod(),
        ($parameters['atp_in_vt_zones_successful']->getCurrentValue()->value ? : 0)
    ];
}
if (isset($parameters['atp_one_shot_started'])) {
    $parametersForOutput['Желудочковая АТС'][
        $parameters['atp_one_shot_started']->name
    ] = [
        $graphManager->buildGraph(
            $parameters['atp_one_shot_started'],
            Yii::t('PatientModule.device', 'АТС «One Shot»'),
            true,
            $atpOneShotYAxis
        ),
        $parameters['atp_one_shot_started']->getCountByCurrentPeriod(),
        ($parameters['atp_one_shot_started']->getCurrentValue()->value ? : 0)
    ];
}
if (isset($parameters['atp_one_shot_successful'])) {
    $parametersForOutput['Желудочковая АТС'][
        $parameters['atp_one_shot_successful']->name
    ] = [
        $graphManager->buildGraph(
            $parameters['atp_one_shot_successful'],
            Yii::t('PatientModule.device', 'АТС «One Shot»'),
            true,
            $atpOneShotYAxis
        ),
        $parameters['atp_one_shot_successful']->getCountByCurrentPeriod(),
        ($parameters['atp_one_shot_successful']->getCurrentValue()->value ? : 0)
    ];
}

if (isset($parameters['shocks_started'])) {
    $parametersForOutput['Желудочковые шоки'][
        $parameters['shocks_started']->name
    ] = [
        $graphManager->buildGraph(
            $parameters['shocks_started'],
            Yii::t('PatientModule.device', 'Желудочковые шоки'),
            true,
            $shocksYAxis
        ),
        $parameters['shocks_started']->getCountByCurrentPeriod(),
        ($parameters['shocks_started']->getCurrentValue()->value ? : 0)
    ];
}
if (isset($parameters['shocks_aborted'])) {
    $parametersForOutput['Желудочковые шоки'][
        $parameters['shocks_aborted']->name
    ] = [
        $graphManager->buildGraph(
            $parameters['shocks_aborted'],
            Yii::t('PatientModule.device', 'Желудочковые шоки'),
            true,
            $shocksYAxis
        ),
        $parameters['shocks_aborted']->getCountByCurrentPeriod(),
        ($parameters['shocks_aborted']->getCurrentValue()->value ? : 0)
    ];
}
if (isset($parameters['shocks_successful'])) {
    $parametersForOutput['Желудочковые шоки'][
        $parameters['shocks_successful']->name
    ] = [
        $graphManager->buildGraph(
            $parameters['shocks_successful'],
            Yii::t('PatientModule.device', 'Желудочковые шоки'),
            true,
            $shocksYAxis
        ),
        $parameters['shocks_successful']->getCountByCurrentPeriod(),
        ($parameters['shocks_successful']->getCurrentValue()->value ? : 0)
    ];
}
if (isset($parameters['ineffective_ven_max_energy_shocks'])) {
    $parametersForOutput['Желудочковые шоки'][
        $parameters['ineffective_ven_max_energy_shocks']->name
    ] = [
        '',
        $parameters['ineffective_ven_max_energy_shocks']->getCountByCurrentPeriod(),
        ($parameters['ineffective_ven_max_energy_shocks']->getCurrentValue()->value ? : 0)
    ];
}



$printFirstHeader = false;
?>

<table class = "table table-condensed rh-main-table">
    <col width="5%" valign="middle">
    <col width="30%" valign="middle">
    <col width="15%" valign="middle">
    <col width="25%" valign="middle">
    <col width="35%" valign="middle">
    <tbody>
    <?php foreach ($parametersForOutput as $headerCell => $parameters) { ?>
        <?php if(empty($parameters)) {
            continue;
        } ?>
        <tr>
            <th colspan="3">
                <?=Yii::t('PatientModule.device', $headerCell);?>
            </th>
            <?php if (!$printFirstHeader) { ?>
            <?php $printFirstHeader = true; ?>
            <th class="text-center">
                <?=Yii::t('PatientModule.device', 'C');?>
                <?=$this->sinceDateTimeString();?>
            </th>
            <th class="text-center">
                <?=Yii::t('PatientModule.device', 'C даты имплантации');?>
            </th>
            <?php } else { ?>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            <?php } ?>
        </tr>
        <?php foreach ($parameters as $label => $value) { ?>
            <tr>
                <td>&nbsp;</td>
                <td><?=$label?></td>
                <?php if (is_array($value)) {?>
                    <td class="text-center noprint_hidden"><?=$value[0]?></td>
                    <td class="text-center"><?=$value[1]?></td>
                    <td class="text-center"><?=$value[2]?></td>
                <?php } else { ?>
                    <td class="text-center">&nbsp;</td>
                    <td class="text-center" colspan="2"><?=$value?></td>
                <?php } ?>
            </tr>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>
