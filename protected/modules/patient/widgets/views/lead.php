<?php
/* @var $this LeadWidget*/
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$graphManager = $this->getGraphManager();
$pacingImpedanceYAxis = [[0, ''], 200, 500, 1000, 1500, 2000, 2500, 3000, [3500, '']];
$raSensingAmplitudeYAxis = [[0, ''], 0.5, 1, 2, 3, 4, 5, [6, '']];
$pacingThresholdYAxis = [[0, ''], 0.5, 1, 2, 3, 4, 5, 6, 7, 7.5];
$rvSensingAmplitudeYAxis = [[0, ''], 1, 5, 10, 15, 20, [25, '']];
$lvSensingAmplitudeYAxis = [[0, ''], 1, 5, 10, 15, 20, [25, '']];
$shockLeadImpedanceYAxis = [[0, ''], 25, 50, 75, 100, 125, 150, [175, '']];

$parametersForOutput = [
    'Электрод правого предсердия' => [],
    'Электрод правого желудочка' => [],
    'Электрод левого желудочка' => [],
    'Шоковый электрод' => []
];
if (isset($parameters['ra_pacing_impedance'])) {
    $parametersForOutput['Электрод правого предсердия'][
        $parameters['ra_pacing_impedance']->name . ' [' . $parameters['ra_pacing_impedance']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['ra_pacing_impedance'],
            Yii::t('PatientModule.device', 'Импеданс стимула'),
            true,
            $pacingImpedanceYAxis
        ),
        $parameters['ra_pacing_impedance']->getCurrentValue(),
        ($parameters['ra_pacing_impedance']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}

if (isset($parameters['ra_sensing_amplitude_daily_mean'])) {
    $parametersForOutput['Электрод правого предсердия'][
        $parameters['ra_sensing_amplitude_daily_mean']->name . ' [' . $parameters['ra_sensing_amplitude_daily_mean']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['ra_sensing_amplitude_daily_mean'],
            Yii::t('PatientModule.device', 'Амплитуда сигнала правого предсердия'),
            true,
            $raSensingAmplitudeYAxis
        ),
        $parameters['ra_sensing_amplitude_daily_mean']->getCurrentValue(),
        ($parameters['ra_sensing_amplitude_daily_mean']->getMeanByCountValue(1) ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['ra_sensing_amplitude_daily_min'])) {
    $parametersForOutput['Электрод правого предсердия'][
        $parameters['ra_sensing_amplitude_daily_min']->name . ' [' . $parameters['ra_sensing_amplitude_daily_min']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['ra_sensing_amplitude_daily_min'],
            Yii::t('PatientModule.device', 'Амплитуда сигнала правого предсердия'),
            true,
            $raSensingAmplitudeYAxis
        ),
        $parameters['ra_sensing_amplitude_daily_min']->getCurrentValue(),
        ($parameters['ra_sensing_amplitude_daily_min']->getMinByCountValue(1) ? : DeviceValue::NULL_PLACEHOLDER) . '**'
    ];
}
if (isset($parameters['ra_sensing'])) {
    $parametersForOutput['Электрод правого предсердия'][$parameters['ra_sensing']->name] = $parameters['ra_sensing']->getCurrentValue();
}
//RV Leads
if (isset($parameters['rv_pacing_impedance'])) {
    $parametersForOutput['Электрод правого желудочка'][
        $parameters['rv_pacing_impedance']->name . ' [' . $parameters['rv_pacing_impedance']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['rv_pacing_impedance'],
            Yii::t('PatientModule.device', 'Импеданс стимула'),
            true,
            $pacingImpedanceYAxis
        ),
        $parameters['rv_pacing_impedance']->getCurrentValue(),
        ($parameters['rv_pacing_impedance']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['rv_pacing_threshold'])) {
    $parametersForOutput['Электрод правого желудочка'][
        $parameters['rv_pacing_threshold']->name . ' [' . $parameters['rv_pacing_threshold']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['rv_pacing_threshold'],
            Yii::t('PatientModule.device', 'Порог стимула'),
            true,
            $pacingThresholdYAxis
        ),
        $parameters['rv_pacing_threshold']->getCurrentValue(),
        ($parameters['rv_pacing_threshold']->getMeanByCountValue(1) ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['rv_sensing_amplitude_daily_mean'])) {
    $parametersForOutput['Электрод правого желудочка'][
        $parameters['rv_sensing_amplitude_daily_mean']->name . ' [' . $parameters['rv_sensing_amplitude_daily_mean']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['rv_sensing_amplitude_daily_mean'],
            Yii::t('PatientModule.device', 'Амплитуда сигнала правого желудочка'),
            true,
            $rvSensingAmplitudeYAxis
        ),
        $parameters['rv_sensing_amplitude_daily_mean']->getCurrentValue(),
        ($parameters['rv_sensing_amplitude_daily_mean']->getMeanByCountValue(1) ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['rv_sensing_amplitude_daily_min'])) {
    $parametersForOutput['Электрод правого желудочка'][
        $parameters['rv_sensing_amplitude_daily_min']->name . ' [' . $parameters['rv_sensing_amplitude_daily_min']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['rv_sensing_amplitude_daily_min'],
            Yii::t('PatientModule.device', 'Амплитуда сигнала правого желудочка'),
            true,
            $rvSensingAmplitudeYAxis
        ),
        $parameters['rv_sensing_amplitude_daily_min']->getCurrentValue(),
        ($parameters['rv_sensing_amplitude_daily_min']->getMinByCountValue(1) ? : DeviceValue::NULL_PLACEHOLDER) . '**'
    ];
}
if (isset($parameters['rv_sensing'])) {
    $parametersForOutput['Электрод правого желудочка'][$parameters['rv_sensing']->name] = $parameters['rv_sensing']->getCurrentValue();
}
//LV Leads
if (isset($parameters['lv_pacing_impedance'])) {
    $parametersForOutput['Электрод левого желудочка'][
        $parameters['lv_pacing_impedance']->name . ' [' . $parameters['lv_pacing_impedance']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['lv_pacing_impedance'],
            Yii::t('PatientModule.device', 'Импеданс стимула'),
            true,
            $pacingImpedanceYAxis
        ),
        $parameters['lv_pacing_impedance']->getCurrentValue(),
        ($parameters['lv_pacing_impedance']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['lv_pacing_threshold'])) {
    $parametersForOutput['Электрод левого желудочка'][
        $parameters['lv_pacing_threshold']->name . ' [' . $parameters['lv_pacing_threshold']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['lv_pacing_threshold'],
            Yii::t('PatientModule.device', 'Порог стимула'),
            true,
            $pacingThresholdYAxis
        ),
        $parameters['lv_pacing_threshold']->getCurrentValue(),
        ($parameters['lv_pacing_threshold']->getMeanByCountValue(1) ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['lv_sensing_amplitude_daily_mean'])) {
    $parametersForOutput['Электрод левого желудочка'][
        $parameters['lv_sensing_amplitude_daily_mean']->name . ' [' . $parameters['lv_sensing_amplitude_daily_mean']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['lv_sensing_amplitude_daily_mean'],
            Yii::t('PatientModule.device', 'Амплитуда сигнала левого желудочка'),
            true,
            $lvSensingAmplitudeYAxis
        ),
        $parameters['lv_sensing_amplitude_daily_mean']->getCurrentValue(),
        ($parameters['lv_sensing_amplitude_daily_mean']->getMeanByCountValue(1) ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['lv_sensing_amplitude_daily_min'])) {
    $parametersForOutput['Электрод левого желудочка'][
        $parameters['lv_sensing_amplitude_daily_min']->name . ' [' . $parameters['lv_sensing_amplitude_daily_min']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['lv_sensing_amplitude_daily_min'],
            Yii::t('PatientModule.device', 'Амплитуда сигнала левого желудочка'),
            true,
            $lvSensingAmplitudeYAxis
        ),
        $parameters['lv_sensing_amplitude_daily_min']->getCurrentValue(),
        ($parameters['lv_sensing_amplitude_daily_min']->getMinByCountValue(1) ? : DeviceValue::NULL_PLACEHOLDER) . '**'
    ];
}
if (isset($parameters['lv_sensing'])) {
    $parametersForOutput['Электрод левого желудочка'][$parameters['lv_sensing']->name] = $parameters['lv_sensing']->getCurrentValue();
}
//Shock lead
if (isset($parameters['daily_shock_lead_impedance'])) {
    $parametersForOutput['Шоковый электрод'][
            $parameters['daily_shock_lead_impedance']->name . ' [' . $parameters['daily_shock_lead_impedance']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['daily_shock_lead_impedance'],
            Yii::t('PatientModule.device', 'Импеданс шокового электрода'),
            true,
            $shockLeadImpedanceYAxis
        ),
        $parameters['daily_shock_lead_impedance']->getCurrentValue(),
        ($parameters['daily_shock_lead_impedance']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['latest_available_impedance_of_a_delivered_shock'])) {
    $parametersForOutput['Шоковый электрод'][
            $parameters['latest_available_impedance_of_a_delivered_shock']->name . ' [' . $parameters['latest_available_impedance_of_a_delivered_shock']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['latest_available_impedance_of_a_delivered_shock'],
            Yii::t('PatientModule.device', 'Импеданс шокового электрода'),
            true,
            $shockLeadImpedanceYAxis
        ),
        $parameters['latest_available_impedance_of_a_delivered_shock']->getCurrentValue(),
        (!is_null($parameters['latest_available_impedance_of_a_delivered_shock']->getCurrentValue()->value) ?
        Yii::t('PatientModule.device', 'Измерено') . ' ' . $this->printDateTime($parameters['timestamp_of_latest_available_shock_impedance']->getCurrentValue()->value)
        : DeviceValue::NULL_PLACEHOLDER)
    ];
}
$printFirstHeader = false;
?>

<table class = "table table-condensed rh-main-table">
    <col width="5%" valign="middle">
    <col width="30%" valign="middle">
    <col width="15%" valign="middle">
    <col width="25%" valign="middle">
    <col width="35%" valign="middle">
    <tbody>
        <tr>
            <th colspan ="4"></th>
            <th class="text-center">
                <?=Yii::t('PatientModule.device', 'C');?>
                <?=$this->sinceDateTimeString();?>
            </th>
        </tr>
    <?php foreach ($parametersForOutput as $headerCell => $parameters) { ?>
        <?php if(empty($parameters)) {
            continue;
        } ?>
        <tr>
            <th colspan="3">
                <?=Yii::t('PatientModule.device', $headerCell);?>
            </th>
            <?php if (!$printFirstHeader) { ?>
            <?php $printFirstHeader = true; ?>
            <th class="text-center">
                24<?=Yii::t('PatientModule.device', 'ч');?>
            </th>
            <th class="text-center">
                <?=Yii::t('PatientModule.device', 'Средние значения');?>,
                **<?=Yii::t('PatientModule.device', 'Мин. значения');?>
            </th>
            <?php } else { ?>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            <?php } ?>
        </tr>
        <?php foreach ($parameters as $label => $value) { ?>
            <tr>
                <td>&nbsp;</td>
                <td><?=$label?></td>
                <?php if (is_array($value)) {?>
                    <td class="text-center noprint_hidden"><?=$value[0]?></td>
                    <td class="text-center"><?=$value[1]?></td>
                    <td class="text-center"><?=$value[2]?></td>
                <?php } else { ?>
                    <td class="text-center">&nbsp;</td>
                    <td class="text-center" colspan="2"><?=$value?></td>
                <?php } ?>
            </tr>
        <?php } ?>
    <?php } ?>
    <?php if ($graphManager->countGraphs()) { ?>
        <?php $graphManager->resetGraphs(); ?>
        <?php do { ?>
            <tr id = '<?=$graphManager->getAnchor();?>' class="noprint">
                <th colspan="5">
                    <?=$graphManager->getName();?>
                </th>
            </tr>
            <tr class="noprint" style="position:relative;">
                <td colspan="5">
                    <?php $graphManager->renderGraph(); ?>
                </td>
            </tr>
        <?php } while ($graphManager->nextGraph() !== false); ?>
    <?php } ?>
    </tbody>
</table>
