<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
?>
<table class = "table table-condensed rh-main-table">
    <col width="65%" valign="middle">
    <col width="25%" valign="middle" span='1' align="center">
    <thead>
        <tr>
            <th><?=Yii::t('PatientModule.brady', 'Бради электроды');?></th>
            <th class="text-center"><?=Yii::t('PatientModule.brady', 'ПЖ');?></th>
        </tr>
    </thead>
    <tbody>
        <?php
        $rv = isset($parameters['rv_pacing_impedance']);
        if (($rv)) { ?>
        <tr>
            <td><?=Yii::t('PatientModule.brady', 'Стимуляционное сопротивление');?>
            [<?php if($rv) {
                    echo $parameters['rv_pacing_impedance']->unit->name;
                }
            ?>]</td>
            <td class="text-center"><?=$rv ? $parameters['rv_pacing_impedance']->getCurrentValue() : '';?></td>
        </tr>
        <?php } ?>
        <?php
        $rv = isset($parameters['rv_pacing_threshold']);
        if ($rv) { ?>
        <tr>
            <td><?=Yii::t('PatientModule.brady', 'Амплитуда импульса');?>
            [<?php if($rv) {
                    echo $parameters['rv_pacing_threshold']->unit->name;
                }
            ?>]
            </td>
            <td class="text-center"><?=$rv ? $parameters['rv_pacing_threshold']->getCurrentValue() : '';?></td>
        </tr>
        <?php } ?>
        <?php
        $rv = isset($parameters['rv_sensing_amplitude_daily_mean']);
        if ($rv) { ?>
        <tr>
            <td><?=Yii::t('PatientModule.brady', 'Амплитуда сигнала средняя/минимальная');?>
            [<?php if($rv) {
                    echo $parameters['rv_sensing_amplitude_daily_mean']->unit->name;
                }
            ?>]
            </td>
            <td class="text-center">
                <?=$rv ? $parameters['rv_sensing_amplitude_daily_mean']->getCurrentValue() : '';?>
                <?=$rv ? ' / ' . $parameters['rv_sensing_amplitude_daily_min']->getCurrentValue() : '';?>
            </td>
        </tr>
        <?php } ?>
        <?php
        $rv = isset($parameters['rv_pulse_amplitude']);
        if ($rv) { ?>
        <tr>
            <td><?=Yii::t('PatientModule.brady', 'Запрограммированный');?>
            [<?php if($rv) {
                    echo $parameters['rv_pulse_amplitude']->unit->name;
                    echo '@' . $parameters['rv_pulse_width']->unit->name;
                }
            ?>]
            </td>
            <td class="text-center">
                <?=$rv ? $parameters['rv_pulse_amplitude']->getCurrentValue() : '';?>
                <?=$rv ? ' @ ' . $parameters['rv_pulse_width']->getCurrentValue() : '';?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
