<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$graphManager = $this->getGraphManager();

$heartRateYAxis = [[20, ''], 40, 60, 80, 100, 120, 140, 160, [180, '']];
$ppVariabilityYAxis = [30, 40, 50, 60, 70, 80];
$patientActivityYAxis = [0, 5, 10, 15, 20, 25];
$thoracicImpedanceYAxis = [0, 5, 10, 15, 20, 25];

$parametersForOutput = [
    'Частота сердцебиения' => [],
    'Вариабельность сердечного ритма' => [],
    'Активность пациента' => [],
    'Грудное сопротивление' => []
];
if (isset($parameters['mean_atrial_heart_rate'])) {
    $parametersForOutput['Частота сердцебиения'][
        $parameters['mean_atrial_heart_rate']->name . ' [' . $parameters['mean_atrial_heart_rate']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['mean_atrial_heart_rate'],
            Yii::t('PatientModule.device', 'Средняя частота'),
            true,
            $heartRateYAxis
        ),
        $parameters['mean_atrial_heart_rate']->getCurrentValue(),
        ($parameters['mean_atrial_heart_rate']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['mean_heart_rate'])) {
    $parametersForOutput['Частота сердцебиения'][
        $parameters['mean_heart_rate']->name . ' [' . $parameters['mean_heart_rate']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['mean_heart_rate'],
            Yii::t('PatientModule.device', 'Средняя частота'),
            true,
            $heartRateYAxis
        ),
        $parameters['mean_heart_rate']->getCurrentValue(),
        ($parameters['mean_heart_rate']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['mean_ventricular_heart_rate'])) {
    $parametersForOutput['Частота сердцебиения'][
        $parameters['mean_ventricular_heart_rate']->name . ' [' . $parameters['mean_ventricular_heart_rate']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['mean_ventricular_heart_rate'],
            Yii::t('PatientModule.device', 'Средняя частота'),
            true,
            $heartRateYAxis
        ),
        $parameters['mean_ventricular_heart_rate']->getCurrentValue(),
        ($parameters['mean_ventricular_heart_rate']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['mean_ventricular_heart_rate_at_rest'])) {
    $parametersForOutput['Частота сердцебиения'][
        $parameters['mean_ventricular_heart_rate_at_rest']->name . ' [' . $parameters['mean_ventricular_heart_rate_at_rest']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['mean_ventricular_heart_rate_at_rest'],
            Yii::t('PatientModule.device', 'Средняя частота'),
            true,
            $heartRateYAxis
        ),
        $parameters['mean_ventricular_heart_rate_at_rest']->getCurrentValue(),
        ($parameters['mean_ventricular_heart_rate_at_rest']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}

if (isset($parameters['pp_variability'])) {
    $parametersForOutput['Вариабельность сердечного ритма'][
        $parameters['pp_variability']->name . ' [' . $parameters['pp_variability']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['pp_variability'],
            Yii::t('PatientModule.device', 'Вариабельность сердечного ритма'),
            true
        ),
        $parameters['pp_variability']->getCurrentValue(),
        ($parameters['pp_variability']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
} else {
    unset($parametersForOutput['Вариабельность сердечного ритма']);
}

if (isset($parameters['patient_activity'])) {
    $parametersForOutput['Активность пациента'][
        $parameters['patient_activity']->name . ' [' . $parameters['patient_activity']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['patient_activity'],
            Yii::t('PatientModule.device', 'Активность пациента'),
            true,
            $patientActivityYAxis
        ),
        $parameters['patient_activity']->getCurrentValue(),
        ($parameters['patient_activity']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
} else {
    unset($parametersForOutput['Активность пациента']);
}

if (isset($parameters['thoracic_impedance_daily_mean'])) {
    $parametersForOutput['Грудное сопротивление'][
        $parameters['thoracic_impedance_daily_mean']->name . ' [' . $parameters['thoracic_impedance_daily_mean']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['thoracic_impedance_daily_mean'],
            null,
            false,
            $thoracicImpedanceYAxis
        ),
        $parameters['thoracic_impedance_daily_mean']->getCurrentValue(),
        ($parameters['thoracic_impedance_daily_mean']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
} else {
    unset($parametersForOutput['Грудное сопротивление']);
}

$printFirstHeader = false;
?>

<table class = "table table-condensed rh-main-table">
    <col width="5%" valign="middle">
    <col width="30%" valign="middle">
    <col width="15%" valign="middle">
    <col width="25%" valign="middle">
    <col width="35%" valign="middle">
    <tbody>
        <tr>
            <th colspan ="4"></th>
            <th class="text-center">
                <?=Yii::t('PatientModule.device', 'C');?>
                <?=$this->sinceDateTimeString();?>
            </th>
        </tr>
    <?php foreach ($parametersForOutput as $headerCell => $parameters) { ?>
        <?php if(empty($parameters)) {
            continue;
        } ?>
        <tr>
            <th colspan="3">
                <?=Yii::t('PatientModule.device', $headerCell);?>
            </th>
            <?php if (!$printFirstHeader) { ?>
            <?php $printFirstHeader = true; ?>
            <th class="text-center">
                24<?=Yii::t('PatientModule.device', 'ч');?>
            </th>
            <th class="text-center">
                <?=Yii::t('PatientModule.device', 'Средние значения');?>
            </th>
            <?php } else { ?>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            <?php } ?>
        </tr>
        <?php foreach ($parameters as $label => $value) { ?>
            <tr>
                <td>&nbsp;</td>
                <td><?=$label?></td>
                <?php if (is_array($value)) {?>
                    <td class="text-center noprint_hidden"><?=$value[0]?></td>
                    <td class="text-center"><?=$value[1]?></td>
                    <td class="text-center"><?=$value[2]?></td>
                <?php } else { ?>
                    <td class="text-center">&nbsp;</td>
                    <td class="text-center" colspan="2"><?=$value?></td>
                <?php } ?>
            </tr>
        <?php } ?>
    <?php } ?>
    <?php if ($graphManager->countGraphs()) { ?>
        <?php $graphManager->resetGraphs(); ?>
        <?php do { ?>
            <tr id = '<?=$graphManager->getAnchor();?>' class="noprint">
                <th colspan="5">
                    <?=$graphManager->getName();?>
                </th>
            </tr>
            <tr class="noprint">
                <td colspan="5">
                    <?php $graphManager->renderGraph(); ?>
                </td>
            </tr>
        <?php } while ($graphManager->nextGraph() !== false); ?>
    <?php } ?>
    </tbody>
</table>
