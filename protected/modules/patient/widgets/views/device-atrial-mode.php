<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
?>
<table class = "table table-condensed table-bordered rh-main-table">
    <col width="60%" valign="middle">
    <col width="40%" valign="middle">
    <thead>
        <tr>
            <th colspan="2">
                <?=Yii::t('PatientModule.device', 'Переключение режимов'); ?>
            </th>
        </tr>
    </thead>
    <tbody>
        <?php if (isset($parameters['mode_switching_mode'])) { ?>
        <tr>
            <td>
                <?=$parameters['mode_switching_mode']->name;?>
            </td>
            <td>
                <?=$parameters['mode_switching_mode']->getCurrentValue();?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['mode_switching_intervention_rate'])) { ?>
        <tr>
            <td>
                <?=$parameters['mode_switching_intervention_rate']->name;?>
                [<?=$parameters['mode_switching_intervention_rate']->unit->name;?>]
            </td>
            <td>
                <?=$parameters['mode_switching_intervention_rate']->getCurrentValue();?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>