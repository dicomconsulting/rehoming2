<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;

$headerCell = Yii::t('PatientModule.device', 'Мониторинг ЗСН');
$renderParameters = [];

if (isset($parameters['resting_period_start'])) {
    $renderParameters[$parameters['resting_period_start']->name]
        = $parameters['resting_period_start']->getCurrentValue();
}
if (isset($parameters['resting_period_duration'])) {
    $renderParameters[$parameters['resting_period_duration']->name]
        = $parameters['resting_period_duration']->getCurrentValue();
}


?>
<?php if (!empty($renderParameters)) { ?>
    <table class = "table table-condensed table-bordered rh-main-table">
        <col width="5%" valign="middle">
        <col width="55%" valign="middle">
        <col width="40%" valign="middle">
        <tbody>
            <tr>
                <th colspan="3">
                    <?=Yii::t('PatientModule.device', $headerCell); ?>
            </th>
            </tr>
            <?php foreach ($renderParameters as $label => $value) { ?>
                <tr>
                    <td>&nbsp;</td>
                    <td><?=$label;?></td>
                    <td class="text-center"><?=$value;?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
<?php } ?>