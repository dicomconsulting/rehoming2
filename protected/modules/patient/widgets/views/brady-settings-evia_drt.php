<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
?>
<table class = "table table-condensed table-bordered rh-main-table">
    <col width="60%" valign="middle">
    <col width="40%" valign="middle">
    <thead>
        <tr>
            <th colspan="2">
                <?=CHtml::link(Yii::t('PatientModule.brady', 'Бради/РТС/Настройки ФП'), ['device/bradycardia', 'id' => $patient->uid]); ?>
            </th>
        </tr>
    </thead>
    <tbody>
        <?php if (isset($parameters['mode'])) { ?>
        <tr>
            <td>
            <?=$parameters['mode']->name;?>
            </td>
            <td>
            <?=$parameters['mode']->getCurrentValue();?>
            <?php if (isset($parameters['ven_pacing'])) { ?>
                /<?=$parameters['ven_pacing']->getCurrentValue();?>
            <?php } ?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['basic_rate'])) { ?>
        <tr>
            <td>
            <?=$parameters['basic_rate']->name;?>
            <?php if (isset($parameters['upper_tracking_rate'])) {?>
                / <?=$parameters['upper_tracking_rate']->name;?>
            <?php } ?>
            [<?=$parameters['basic_rate']->unit->name;?>]
            </td>
            <td>
            <?=$parameters['basic_rate']->getCurrentValue();?>
            <?php if (isset($parameters['upper_tracking_rate'])) {?>
                / <?=$parameters['upper_tracking_rate']->getCurrentValue();?>
            <?php } ?>
            </td>
        </tr>
        <?php } ?>
        <tr>
            <td>
                <?=Yii::t('PatientModule.brady', 'АВ задержка на 60/140 уд.мин');?>&nbsp;[<?=$parameters['av_delay_at_140_bpm']->unit->name?>]
            </td>
            <td>
               <?=$parameters['av_delay_at_60_bpm']->getCurrentValue();?>
                /
               <?=$parameters['av_delay_at_140_bpm']->getCurrentValue();?>
            </td>
        </tr>
        <tr>
            <td><?=Yii::t('PatientModule.brady', 'Переключение режимов');?></td>
            <td>
                <?=$parameters['intervention_rate']->getCurrentValue() . '&nbsp;[' . $parameters['intervention_rate']->unit->name . ']';?>
                /
                <?=$parameters['switch_to']->getCurrentValue();?>
            </td>
        </tr>
    </tbody>
</table>
