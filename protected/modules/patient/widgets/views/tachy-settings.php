<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
?>
<table id='tachy-table' class = "table table-condensed table-bordered rh-main-table">
    <col width="10%" valign="middle">
    <col width="15%" valign="middle" span='6'>
    <thead>
        <tr>
            <th colspan="7">
                <?=CHtml::link(Yii::t('PatientModule.tachy', 'Тахи'), ['device/ventricular', 'id' => $patient->uid]); ?>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr class='color-grey'>
            <td>&nbsp;</td>
            <td><strong><?=Yii::t('PatientModule.tachy', 'Границы зоны');?></strong></td>
            <td><strong><?=Yii::t('PatientModule.tachy', '1-я АТС');?></strong></td>
            <td><strong><?=Yii::t('PatientModule.tachy', '2-я АТС');?></strong></td>
            <td><strong><?=Yii::t('PatientModule.tachy', '1-й разряд');?></strong></td>
            <td><strong><?=Yii::t('PatientModule.tachy', '2-й разряд');?></strong></td>
            <td><strong><?=Yii::t('PatientModule.tachy', '3-й разряд');?></strong></td>
        </tr>
        <?php if (isset($parameters['vt1_zone_limit'])) { ?>
        <tr>
            <td class='color-grey border-right'><strong>VT1</strong></td>
            <td><?=$parameters['vt1_zone_limit']->getCurrentValue();?></td>
            <td><?=$parameters['vt1_1st_atp']->getCurrentValue();?></td>
            <td><?=$parameters['vt1_2nd_atp']->getCurrentValue();?></td>
            <td>
                <?=$parameters['vt1_1st_shock']->getCurrentValue();?>
                <?=$parameters['vt1_1st_shock']->getCurrentValue()->value ? $parameters['vt1_1st_shock']->unit->name : '';?>
            </td>
            <td>
                <?=$parameters['vt1_2nd_shock']->getCurrentValue();?>
                <?=$parameters['vt1_2nd_shock']->getCurrentValue()->value ? $parameters['vt1_2nd_shock']->unit->name : '';?>
            </td>
            <td><?=$parameters['vt1_3rd_nth_shock']->getCurrentValue();?></td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['vt2_zone_limit'])) { ?>
        <tr>
            <td class='color-grey border-right'><strong>VT2</strong></td>
            <td><?=$parameters['vt2_zone_limit']->getCurrentValue();?></td>
            <td><?=$parameters['vt2_1st_atp']->getCurrentValue();?></td>
            <td><?=$parameters['vt2_2nd_atp']->getCurrentValue();?></td>
            <td>
                <?=$parameters['vt2_1st_shock']->getCurrentValue();?>
                <?=$parameters['vt2_1st_shock']->getCurrentValue()->value ? $parameters['vt2_1st_shock']->unit->name : '';?>
            </td>
            <td>
                <?=$parameters['vt2_2nd_shock']->getCurrentValue();?>
                <?=$parameters['vt2_2nd_shock']->getCurrentValue()->value ? $parameters['vt2_2nd_shock']->unit->name : '';?>
            </td>
            <td><?=$parameters['vt2_3rd_nth_shock']->getCurrentValue();?></td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['vf_zone_limit'])) { ?>
        <tr>
            <td class='color-grey border-right'><strong>VF</strong></td>
            <td><?=$parameters['vf_zone_limit']->getCurrentValue();?></td>
            <td colspan ="2"><?=$parameters['atp_one_shot']->getCurrentValue();?></td>
            <td>
                <?=$parameters['vf_1st_shock']->getCurrentValue();?>
                <?=$parameters['vf_1st_shock']->getCurrentValue()->value ? $parameters['vf_1st_shock']->unit->name : '';?>
            </td>
            <td>
                <?=$parameters['vf_2nd_shock']->getCurrentValue();?>
                <?=$parameters['vf_2nd_shock']->getCurrentValue()->value ? $parameters['vf_2nd_shock']->unit->name : '';?>
            </td>
            <td><?=$parameters['vf_3rd_nth_shock']->getCurrentValue();?></td>
        </tr>
        <?php } ?>
    </tbody>
</table>
