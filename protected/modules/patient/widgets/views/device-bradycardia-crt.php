<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
?>
<table class = "table table-condensed table-bordered rh-main-table">
    <col width="60%" valign="middle">
    <col width="40%" valign="middle">
    <thead>
        <tr>
            <th colspan="2">
                <?=Yii::t('PatientModule.brady', 'РТС'); ?>
            </th>
        </tr>
    </thead>
    <tbody>
        <?php if (isset($parameters['ven_pacing'])) { ?>
        <tr>
            <td>
                <?=$parameters['ven_pacing']->name;?>
            </td>
            <td class="text-center">
                <?=$parameters['ven_pacing']->getCurrentValue();?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['initially_paced_chamber'])) { ?>
        <tr>
            <td>
                <?=$parameters['initially_paced_chamber']->name;?>
            </td>
            <td class="text-center">
                <?=$parameters['initially_paced_chamber']->getCurrentValue();?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['vv_delay_after_vp'])) { ?>
        <tr>
            <td>
                <?=$parameters['vv_delay_after_vp']->name;?>
                [<?=$parameters['vv_delay_after_vp']->unit->name;?>]
            </td>
            <td class="text-center">
                <?=$parameters['vv_delay_after_vp']->getCurrentValue()->value ? : 0;?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['triggering'])) { ?>
        <tr>
            <td>
                <?=$parameters['triggering']->name;?>
            </td>
            <td class="text-center">
                <?=$parameters['triggering']->getCurrentValue();?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['maximum_trigger_rate'])) { ?>
        <tr>
            <td>
                <?=$parameters['maximum_trigger_rate']->name;?>
                [<?=$parameters['maximum_trigger_rate']->unit->name;?>]
            </td>
            <td class="text-center">
                <?=$parameters['maximum_trigger_rate']->getCurrentValue();?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['lv_t_wave_protection'])) { ?>
        <tr>
            <td>
                <?=$parameters['lv_t_wave_protection']->name;?>
            </td>
            <td class="text-center">
                <?=$parameters['lv_t_wave_protection']->getCurrentValue();?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
