<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
?>
<table class="table table-condensed rh-main-table">
    <col width="40%" valign="middle">
    <col width="20%" valign="middle" span='3' align="center">
    <thead>
    <tr>
        <th><?= Yii::t('PatientModule.brady', 'Бради электроды'); ?></th>
        <th class="text-center"><?= Yii::t('PatientModule.brady', 'ПП'); ?></th>
        <th class="text-center"><?= Yii::t('PatientModule.brady', 'ПЖ'); ?></th>
        <th class="text-center"><?= Yii::t('PatientModule.brady', 'ЛЖ'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $ra = isset($parameters['ra_pacing_impedance']);
    $rv = isset($parameters['rv_pacing_impedance']);
    $lv = isset($parameters['lv_pacing_impedance']);
    if (($ra || $rv || $lv)) { ?>
        <tr>
            <td><?= Yii::t('PatientModule.brady', 'Стимуляционное сопротивление'); ?>
                [<?php if ($ra) {
                    echo $parameters['ra_pacing_impedance']->unit->name;
                } elseif ($rv) {
                    echo $parameters['rv_pacing_impedance']->unit->name;
                } elseif ($lv) {
                    echo $parameters['lv_pacing_impedance']->unit->name;
                }
                ?>]
            </td>
            <td class="text-center"><?= $ra ? $parameters['ra_pacing_impedance']->getCurrentValue() : ''; ?></td>
            <td class="text-center"><?= $rv ? $parameters['rv_pacing_impedance']->getCurrentValue() : ''; ?></td>
            <td class="text-center"><?= $lv ? $parameters['lv_pacing_impedance']->getCurrentValue() : ''; ?></td>
        </tr>
    <?php } ?>
    <?php
    $ra = isset($parameters['ra_pacing_threshold']);
    $rv = isset($parameters['rv_pacing_threshold']);
    $lv = isset($parameters['lv_pacing_threshold']);
    if ($ra || $rv || $lv) { ?>
        <tr>
            <td><?= Yii::t('PatientModule.brady', 'Амплитуда импульса'); ?>
                [<?php if ($ra) {
                    echo $parameters['ra_pacing_threshold']->unit->name;
                } elseif ($rv) {
                    echo $parameters['rv_pacing_threshold']->unit->name;
                } elseif ($lv) {
                    echo $parameters['lv_pacing_threshold']->unit->name;
                }
                ?>]
            </td>
            <td class="text-center"><?= $ra ? $parameters['ra_pacing_threshold']->getCurrentValue() : ''; ?></td>
            <td class="text-center"><?= $rv ? $parameters['rv_pacing_threshold']->getCurrentValue() : ''; ?></td>
            <td class="text-center"><?= $lv ? $parameters['lv_pacing_threshold']->getCurrentValue() : ''; ?></td>
        </tr>
    <?php } ?>
    <?php
    $ra = isset($parameters['ra_sensing_amplitude_daily_mean']);
    $rv = isset($parameters['rv_sensing_amplitude_daily_mean']);
    $lv = isset($parameters['lv_sensing_amplitude_daily_mean']);
    if ($ra || $rv || $lv) { ?>
        <tr>
            <td><?= Yii::t('PatientModule.brady', 'Амплитуда сигнала средняя/минимальная'); ?>
                [<?php if ($ra) {
                    echo $parameters['ra_sensing_amplitude_daily_mean']->unit->name;
                } elseif ($rv) {
                    echo $parameters['rv_sensing_amplitude_daily_mean']->unit->name;
                } elseif ($lv) {
                    echo $parameters['lv_sensing_amplitude_daily_mean']->unit->name;
                }
                ?>]
            </td>
            <td class="text-center">
                <?= $ra ? $parameters['ra_sensing_amplitude_daily_mean']->getCurrentValue() : ''; ?>
                <?= ($ra && array_key_exists('ra_sensing_amplitude_daily_min', $parameters)) ?
                    ' / ' . $parameters['ra_sensing_amplitude_daily_min']->getCurrentValue() : ''; ?>
            </td>
            <td class="text-center">
                <?= $rv ? $parameters['rv_sensing_amplitude_daily_mean']->getCurrentValue() : ''; ?>
                <?= ($rv && array_key_exists('lv_sensing_amplitude_daily_min', $parameters)) ?
                    ' / ' . $parameters['rv_sensing_amplitude_daily_min']->getCurrentValue() : ''; ?>
            </td>
            <td class="text-center">
                <?= $lv ? $parameters['lv_sensing_amplitude_daily_mean']->getCurrentValue() : ''; ?>
                <?= ($lv && array_key_exists('lv_sensing_amplitude_daily_min', $parameters)) ?
                    ' / ' . $parameters['lv_sensing_amplitude_daily_min']->getCurrentValue() : ''; ?>
            </td>
        </tr>
    <?php } ?>
    <?php
    $ra = isset($parameters['ra_pulse_amplitude']);
    $rv = isset($parameters['rv_pulse_amplitude']);
    $lv = isset($parameters['lv_pulse_amplitude']);
    if ($ra || $rv || $lv) { ?>
        <tr>
            <td><?= Yii::t('PatientModule.brady', 'Запрограммированный'); ?>
                [<?php if ($ra) {
                    echo $parameters['ra_pulse_amplitude']->unit->name;
                    if (array_key_exists('ra_pulse_width', $parameters)) {
                        echo '@' . $parameters['ra_pulse_width']->unit->name;
                    }
                } elseif ($rv) {
                    echo $parameters['rv_pulse_amplitude']->unit->name;
                    if (array_key_exists('rv_pulse_width', $parameters)) {
                        echo '@' . $parameters['rv_pulse_width']->unit->name;
                    }
                } elseif ($lv) {
                    echo $parameters['lv_pulse_amplitude']->unit->name;
                    if (array_key_exists('lv_pulse_width', $parameters)) {
                        echo '@' . $parameters['lv_pulse_width']->unit->name;
                    }
                }
                ?>]
            </td>
            <td class="text-center">
                <?= $ra ? $parameters['ra_pulse_amplitude']->getCurrentValue() : ''; ?>
                <?php
                if ($ra && array_key_exists('ra_pulse_width', $parameters)) {
                    echo ' @ ' . $parameters['ra_pulse_width']->getCurrentValue();
                }
                ?>
            </td>
            <td class="text-center">
                <?= $rv ? $parameters['rv_pulse_amplitude']->getCurrentValue() : ''; ?>
                <?php
                if ($ra && array_key_exists('ra_pulse_width', $parameters)) {
                    echo ' @ ' . $parameters['rv_pulse_width']->getCurrentValue();
                }
                ?>
            </td>
            <td class="text-center">
                <?= $lv ? $parameters['lv_pulse_amplitude']->getCurrentValue() : ''; ?>
                <?php
                if ($ra && array_key_exists('ra_pulse_width', $parameters)) {
                    echo ' @ ' . $parameters['rv_pulse_width']->getCurrentValue();
                }
                ?>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
