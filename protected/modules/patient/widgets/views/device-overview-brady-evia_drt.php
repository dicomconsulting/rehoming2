<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$rowArray = [];
$rowArray[] = [
    $parameters['mode']->name => $parameters['mode']->getCurrentValue(),
    $parameters['progr_atrial_pulse_amplitude']->name . '&nbsp;[' . $parameters['progr_atrial_pulse_amplitude']->unit->name . ']' =>
        [
            $parameters['progr_atrial_pulse_amplitude']->getCurrentValue(),
            $parameters['progr_ven_pulse_amplitude']->getCurrentValue()
        ]
];
$rowArray[] = [
    $parameters['basic_rate']->name . '&nbsp;[' . $parameters['basic_rate']->unit->name . ']' => $parameters['basic_rate']->getCurrentValue(),
    $parameters['atrial_pulse_width']->name . '&nbsp;[' . $parameters['atrial_pulse_width']->unit->name . ']' =>
        [
            $parameters['atrial_pulse_width']->getCurrentValue(),
            $parameters['ven_pulse_width']->getCurrentValue()
        ]
];
$rowArray[] = [
    $parameters['night_rate']->name . '&nbsp;[' . $parameters['night_rate']->unit->name . ']' => $parameters['night_rate']->getCurrentValue(),
    $parameters['ra_capture_control_mode']->name =>
        [
            $parameters['ra_capture_control_mode']->getCurrentValue(),
            $parameters['rv_capture_control_mode']->getCurrentValue()
        ]
];
$rowArray[] = [
    $parameters['max_activity_rate']->name . '&nbsp;[' . $parameters['max_activity_rate']->unit->name . ']' => $parameters['max_activity_rate']->getCurrentValue(),
    $parameters['atrial_sensitivity']->name =>
        [
            $parameters['atrial_sensitivity']->getCurrentValue(),
            $parameters['ven_sensitivity']->getCurrentValue()
        ]
];
$rowArray[] = [
    $parameters['rate_fading']->name => $parameters['rate_fading']->getCurrentValue(),
    $parameters['progr_atrial_pacing_polarity']->name =>
        [
            $parameters['progr_atrial_pacing_polarity']->getCurrentValue(),
            $parameters['progr_ven_pacing_polarity']->getCurrentValue()
        ]
];
$rowArray[] = [
    $parameters['upper_tracking_rate']->name . '&nbsp;[' . $parameters['upper_tracking_rate']->unit->name . ']' => $parameters['upper_tracking_rate']->getCurrentValue(),
    $parameters['progr_atrial_sensing_polarity']->name =>
        [
            $parameters['progr_atrial_sensing_polarity']->getCurrentValue(),
            $parameters['progr_ven_sensing_polarity']->getCurrentValue()
        ]
];
$rowArray[] = [
    $parameters['mode_switching']->name => $parameters['mode_switching']->getCurrentValue()
];
$rowArray[] = [
    $parameters['intervention_rate']->name . '&nbsp;[' . $parameters['intervention_rate']->unit->name . ']' => $parameters['intervention_rate']->getCurrentValue()
];
$rowArray[] = [
    $parameters['switch_to']->name => $parameters['switch_to']->getCurrentValue()
];
$rowArray[] = [
    $parameters['av_delay_at_60_bpm']->name . '&nbsp;[' . $parameters['av_delay_at_60_bpm']->unit->name . ']' => $parameters['av_delay_at_60_bpm']->getCurrentValue()
];
$rowArray[] = [
    $parameters['av_delay_at_140_bpm']->name . '&nbsp;[' . $parameters['av_delay_at_140_bpm']->unit->name . ']' => $parameters['av_delay_at_140_bpm']->getCurrentValue()
];

?>
<table class = "table table-condensed rh-main-table">
    <col width="30%" valign="middle">
    <col width="20%" valign="middle">
    <col width="30%" valign="middle">
    <col width="10%"  valign="middle">
    <col width="10%"  valign="middle">
    <thead>
        <tr>
            <th colspan="3">&nbsp</th>
            <th>A</th>
            <th>V</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($rowArray as $row) { ?>
        <tr>
            <td>
                <?=key($row)?>
            </td>
            <td style = "border-right: 1px solid #CCCCCC;">
                <?=current($row);?>
            </td>
            <?php if (next($row) !== false) { ?>
                <td>
                    <?=key($row)?>
                </td>
                <td>
                    <?=current($row)[0];?>
                </td>
                <td>
                    <?=current($row)[1];?>
                </td>
            <?php } else { ?>
                <td colspan="3">
                    &nbsp;
                </td>
            <?php } ?>
        </tr>
    <?php } ?>
    </tbody>
</table>
