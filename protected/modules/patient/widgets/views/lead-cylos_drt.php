<?php
/* @var $this LeadWidget*/
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$graphManager = $this->getGraphManager();

$safetyMarginYAxis = [[0,''], 50, 100 , [200,'']];
$pacingThresholdYAxis = [[0, ''], 0.5, 1, 2, 3, 4, 4.8, [5.6, '']];

$parametersForOutput = [
    'Электрод правого предсердия' => [],
    'Электрод правого желудочка' => []
];
if (isset($parameters['ra_lead_check'])) {
    $parametersForOutput['Электрод правого предсердия'][
        $parameters['ra_lead_check']->name
    ] = $parameters['ra_lead_check']->getCurrentValue();
}

if (isset($parameters['last_mean_p_wave_safety_margin'])) {
    $parametersForOutput['Электрод правого предсердия'][
        $parameters['last_mean_p_wave_safety_margin']->name . ' [' . $parameters['last_mean_p_wave_safety_margin']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['last_mean_p_wave_safety_margin'],
            Yii::t('PatientModule.device', 'Последний запас безопасности средней P-/R-волны'),
            true,
            $safetyMarginYAxis
        ),
        $parameters['last_mean_p_wave_safety_margin']->getCurrentValue()
    ];
}
//RV Leads
if (isset($parameters['rv_lead_check'])) {
    $parametersForOutput['Электрод правого желудочка'][
    $parameters['rv_lead_check']->name
    ] = $parameters['rv_lead_check']->getCurrentValue();
}
if (isset($parameters['atm_acc_status'])) {
    $parametersForOutput['Электрод правого желудочка'][
    $parameters['atm_acc_status']->name
    ] = $parameters['atm_acc_status']->getCurrentValue();
}
if (isset($parameters['rv_pacing_threshold'])) {
    $parametersForOutput['Электрод правого желудочка'][
    $parameters['rv_pacing_threshold']->name . ' [' . $parameters['rv_pacing_threshold']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['rv_pacing_threshold'],
            Yii::t('PatientModule.device', 'Порог стимула'),
            false,
            $pacingThresholdYAxis
        ),
        $parameters['rv_pacing_threshold']->getCurrentValue()
    ];
}
if (isset($parameters['last_mean_r_wave_safety_margin'])) {
    $parametersForOutput['Электрод правого желудочка'][
    $parameters['last_mean_r_wave_safety_margin']->name . ' [' . $parameters['last_mean_r_wave_safety_margin']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['last_mean_r_wave_safety_margin'],
            Yii::t('PatientModule.device', 'Последний запас безопасности средней P-/R-волны'),
            true,
            $safetyMarginYAxis
        ),
        $parameters['last_mean_r_wave_safety_margin']->getCurrentValue()
    ];
}
?>

<table class = "table table-condensed rh-main-table">
    <col width="5%" valign="middle">
    <col width="40%" valign="middle">
    <col width="25%" valign="middle">
    <col width="30%" valign="middle">
    <tbody>
    <?php foreach ($parametersForOutput as $headerCell => $parameters) { ?>
        <?php if(empty($parameters)) {
            continue;
        } ?>
        <tr>
            <th colspan="3">
                <?=Yii::t('patientModule.device', $headerCell);?>
            </th>
            <th>&nbsp;</th>
        </tr>
        <?php foreach ($parameters as $label => $value) { ?>
            <tr>
                <td>&nbsp;</td>
                <td><?=$label?></td>
                <?php if (is_array($value)) {?>
                    <td class="text-center noprint_hidden"><?=$value[0]?></td>
                    <td class="text-center"><?=$value[1]?></td>
                <?php } else { ?>
                    <td class="text-center">&nbsp;</td>
                    <td class="text-center"><?=$value?></td>
                <?php } ?>
            </tr>
        <?php } ?>
    <?php } ?>
    <?php if ($graphManager->countGraphs()) { ?>
        <?php $graphManager->resetGraphs(); ?>
        <?php do { ?>
            <tr id = '<?=$graphManager->getAnchor();?>' class="noprint">
                <th colspan="5">
                    <?=$graphManager->getName();?>
                </th>
            </tr>
            <tr class="noprint">
                <td colspan="5">
                    <?php $graphManager->renderGraph(); ?>
                </td>
            </tr>
        <?php } while ($graphManager->nextGraph() !== false); ?>
    <?php } ?>
    </tbody>
</table>
