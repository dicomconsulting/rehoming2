<?php
/* @var $this LeadWidget*/
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$graphManager = $this->getGraphManager();

$pacingImpedanceYAxis = [[0, ''], 150, 500, 1000, 1500, 2000, [2500, '']];
$pacingThresholdYAxis = [[0, ''], 0.5, 1, 2, 3, 4, 5];
$sensingAmplitudeYAxis = [0, 2, 4, 6, 8, 10, 12, 14, 15];

$parametersForOutput = [
    'Электрод' => [],
];
if (isset($parameters['pacing_impedance'])) {
    $parametersForOutput['Электрод'][
        $parameters['pacing_impedance']->name . ' [' . $parameters['pacing_impedance']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['pacing_impedance'],
            null,
            false,
            $pacingImpedanceYAxis
        ),
        $parameters['pacing_impedance']->getCurrentValue(),
        ($parameters['pacing_impedance']->getMeanByCountValue(0) ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['rv_pacing_threshold_capture_control'])) {
    $parametersForOutput['Электрод'][
        $parameters['rv_pacing_threshold_capture_control']->name . ' [' . $parameters['rv_pacing_threshold_capture_control']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['rv_pacing_threshold_capture_control'],
            Yii::t('PatientModule.device', 'Порог стимула'),
            false,
            $pacingThresholdYAxis
        ),
        $parameters['rv_pacing_threshold_capture_control']->getCurrentValue(),
        ($parameters['rv_pacing_threshold_capture_control']->getMeanByCountValue(0) ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['last_threshold_measurement_rv'])) {
    $parametersForOutput['Электрод'][$parameters['last_threshold_measurement_rv']->name] =
        $this->printDateTime($parameters['last_threshold_measurement_rv']->getCurrentValue()->value);
}
if (isset($parameters['capture_control_status_rv'])) {
    $parametersForOutput['Электрод'][$parameters['capture_control_status_rv']->name] = $parameters['capture_control_status_rv']->getCurrentValue();
}
if (isset($parameters['pulse_amplitude'])) {
    $parametersForOutput['Электрод'][$parameters['pulse_amplitude']->name . ' [' . $parameters['pulse_amplitude']->unit->name . ']']=
        $parameters['pulse_amplitude']->getCurrentValue();
}
if (isset($parameters['sensing_amplitude_daily_mean'])) {
    $parametersForOutput['Электрод'][
    $parameters['sensing_amplitude_daily_mean']->name . ' [' . $parameters['sensing_amplitude_daily_mean']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['sensing_amplitude_daily_mean'],
            Yii::t('PatientModule.device', 'Чувствительность'),
            true,
            $sensingAmplitudeYAxis
        ),
        $parameters['sensing_amplitude_daily_mean']->getCurrentValue(),
        ($parameters['sensing_amplitude_daily_mean']->getMeanByCountValue(1) ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['sensing_amplitude_daily_min'])) {
    $parametersForOutput['Электрод'][
    $parameters['sensing_amplitude_daily_min']->name . ' [' . $parameters['sensing_amplitude_daily_min']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['sensing_amplitude_daily_min'],
            Yii::t('PatientModule.device', 'Чувствительность'),
            true,
            $sensingAmplitudeYAxis
        ),
        $parameters['sensing_amplitude_daily_min']->getCurrentValue(),
        ($parameters['sensing_amplitude_daily_min']->getMinByCountValue(1) ? : DeviceValue::NULL_PLACEHOLDER) . '**'
    ];
}
if (isset($parameters['lead_check'])) {
    $parametersForOutput['Электрод'][$parameters['lead_check']->name]= $parameters['lead_check']->getCurrentValue();
}
if (isset($parameters['pacing_polarity'])) {
    $parametersForOutput['Электрод'][$parameters['pacing_polarity']->name]= $parameters['pacing_polarity']->getCurrentValue();
}
if (isset($parameters['sensing_polarity'])) {
    $parametersForOutput['Электрод'][$parameters['sensing_polarity']->name]= $parameters['sensing_polarity']->getCurrentValue();
}

$printFirstHeader = false;
?>

<table class = "table table-condensed rh-main-table">
    <col width="5%" valign="middle">
    <col width="30%" valign="middle">
    <col width="15%" valign="middle">
    <col width="25%" valign="middle">
    <col width="35%" valign="middle">
    <tbody>
        <tr>
            <th colspan ="4"></th>
            <th class="text-center">
                <?=Yii::t('PatientModule.device', 'C');?>
                <?=$this->sinceDateTimeString();?>
            </th>
        </tr>
    <?php foreach ($parametersForOutput as $headerCell => $parameters) { ?>
        <?php if(empty($parameters)) {
            continue;
        } ?>
        <tr>
            <th colspan="3">
                <?=Yii::t('PatientModule.device', $headerCell);?>
            </th>
            <?php if (!$printFirstHeader) { ?>
            <?php $printFirstHeader = true; ?>
            <th class="text-center">
                24<?=Yii::t('PatientModule.device', 'ч');?>
            </th>
            <th class="text-center">
                <?=Yii::t('PatientModule.device', 'Средние значения');?>,
                **<?=Yii::t('PatientModule.device', 'Мин. значения');?>
            </th>
            <?php } else { ?>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            <?php } ?>
        </tr>
        <?php foreach ($parameters as $label => $value) { ?>
            <tr>
                <td>&nbsp;</td>
                <td><?=$label?></td>
                <?php if (is_array($value)) {?>
                    <td class="text-center noprint_hidden"><?=$value[0]?></td>
                    <td class="text-center"><?=$value[1]?></td>
                    <td class="text-center"><?=$value[2]?></td>
                <?php } else { ?>
                    <td class="text-center">&nbsp;</td>
                    <td class="text-center" colspan="2"><?=$value?></td>
                <?php } ?>
            </tr>
        <?php } ?>
    <?php } ?>
    <?php if ($graphManager->countGraphs()) { ?>
        <?php $graphManager->resetGraphs(); ?>
        <?php do { ?>
            <tr id = '<?=$graphManager->getAnchor();?>' class="noprint">
                <th colspan="5">
                    <?=$graphManager->getName();?>
                </th>
            </tr>
            <tr class="noprint">
                <td colspan="5">
                    <?php $graphManager->renderGraph(); ?>
                </td>
            </tr>
        <?php } while ($graphManager->nextGraph() !== false); ?>
    <?php } ?>
    </tbody>
</table>
