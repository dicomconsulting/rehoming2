<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;

$parametersForOutput = [
    'Записи' => [
        'high_atrial_rate_recordings',
        'high_ven_rate_recordings',
        'patient_trigger',
        'pre_trigger_recording',
        'iegm_signal'
    ],
];
?>
<table class = "table table-condensed rh-main-table">
    <col width="5%" valign="middle">
    <col width="55%" valign="middle">
    <col width="40%" valign="middle">
    <tbody>
    <?php foreach ($parametersForOutput as $headerCell => $parameterNames) { ?>
        <tr>
            <th colspan="3">
                <?=Yii::t('PatientModule.device', $headerCell);?>
            </th>
        </tr>
        <?php foreach ($parameterNames as $parameterName) { ?>
            <?php if (isset($parameters[$parameterName])) { ?>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <?=$parameters[$parameterName]?>
                        <?php if (!empty($parameters[$parameterName]->unit)) { ?>
                            [<?=$parameters[$parameterName]->unit->name?>]
                        <?php } ?>
                    </td>
                    <td class="text-center">
                        <?=$parameters[$parameterName]->getCurrentValue();?>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>