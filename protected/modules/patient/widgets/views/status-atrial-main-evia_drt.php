<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$graphManager = $this->getGraphManager();

$atrialBurdenYAxis = [0, 20, 40 ,60 ,80, 100];
$atrialEpisodesYAxis = [0, 2, 4 ,6 ,8, 10, 12, 14, 16, 18, 20];
$modeSwitchingYAxis = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20];
$ventricularRateYAxis = [40, 60, 80, 100, 120, 140, 160];

$parametersForOutput = [
    'Доля предсердных сокращений' => [],
    'Эпизоды переключения режима' => []
];

if (isset($parameters['atrial_burden'])) {
    $parametersForOutput['Доля предсердных сокращений'][
    $parameters['atrial_burden']->name . ' [' . $parameters['atrial_burden']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['atrial_burden'],
            Yii::t('PatientModule.device', 'Доля предсердных сокращений'),
            false,
            $atrialBurdenYAxis
        ),
        $parameters['atrial_burden']->getCurrentValue(),
        ($parameters['atrial_burden']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['atrial_arrhythmia_episodes_per_day'])) {
    $parametersForOutput['Доля предсердных сокращений'][
    $parameters['atrial_arrhythmia_episodes_per_day']->name
    ] = [
        $graphManager->buildGraph(
            $parameters['atrial_arrhythmia_episodes_per_day'],
            null,
            false,
            $atrialEpisodesYAxis
        ),
        $parameters['atrial_arrhythmia_episodes_per_day']->getCurrentValue(),
        ($parameters['atrial_arrhythmia_episodes_per_day']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['new_long_atrial_episode_detected_ongoing_at_end_of_mon_interv'])) {
    $parametersForOutput['Доля предсердных сокращений'][
        $parameters['new_long_atrial_episode_detected_ongoing_at_end_of_mon_interv']->name
    ] = $parameters['new_long_atrial_episode_detected_ongoing_at_end_of_mon_interv']->getCurrentValue();
}
if (isset($parameters['start_of_ongoing_atrial_episode'])) {
    $parametersForOutput['Доля предсердных сокращений'][
        $parameters['start_of_ongoing_atrial_episode']->name
    ] = $parameters['start_of_ongoing_atrial_episode']->getCurrentValue();
}

if (isset($parameters['number_of_mode_switching_per_day'])) {
    $parametersForOutput['Эпизоды переключения режима'][
    $parameters['number_of_mode_switching_per_day']->name
    ] = [
        $graphManager->buildGraph(
            $parameters['number_of_mode_switching_per_day'],
            Yii::t('PatientModule.device', 'Эпизоды переключения режима'),
            true,
            $modeSwitchingYAxis
        ),
        $parameters['number_of_mode_switching_per_day']->getCurrentValue(),
        ($parameters['number_of_mode_switching_per_day']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['duration_of_mode_switching'])) {
    $parametersForOutput['Эпизоды переключения режима'][
    $parameters['duration_of_mode_switching']->name . ' [' . $parameters['duration_of_mode_switching']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['duration_of_mode_switching'],
            Yii::t('PatientModule.device', 'Эпизоды переключения режима'),
            true,
            $modeSwitchingYAxis
        ),
        $parameters['duration_of_mode_switching']->getCurrentValue(),
        ($parameters['duration_of_mode_switching']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['mean_ventricular_rate_during_mode_switching'])) {
    $parametersForOutput['Эпизоды переключения режима'][
    $parameters['mean_ventricular_rate_during_mode_switching']->name . ' [' . $parameters['mean_ventricular_rate_during_mode_switching']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['mean_ventricular_rate_during_mode_switching'],
            null,
            false,
            $ventricularRateYAxis
        ),
        $parameters['mean_ventricular_rate_during_mode_switching']->getCurrentValue(),
        ($parameters['mean_ventricular_rate_during_mode_switching']->getMeanByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['new_long_msw_episode_detected_ongoing_at_end_of_mon_interv'])) {
    $parametersForOutput['Эпизоды переключения режима'][
        $parameters['new_long_msw_episode_detected_ongoing_at_end_of_mon_interv']->name
    ] = $parameters['new_long_msw_episode_detected_ongoing_at_end_of_mon_interv']->getCurrentValue();
}
if (isset($parameters['start_of_ongoing_mode_switching_episode'])) {
    $parametersForOutput['Эпизоды переключения режима'][
    $parameters['start_of_ongoing_mode_switching_episode']->name
    ] = $parameters['start_of_ongoing_mode_switching_episode']->getCurrentValue();
}

$printFirstHeader = false;
?>
<table class = "table table-condensed rh-main-table">
    <col width="5%" valign="middle">
    <col width="30%" valign="middle">
    <col width="15%" valign="middle">
    <col width="25%" valign="middle">
    <col width="35%" valign="middle">
    <tbody>
    <tr>
        <th colspan ="4"></th>
        <th class="text-center">
            <?=Yii::t('PatientModule.device', 'C');?>
            <?=$this->sinceDateTimeString();?>
        </th>
    </tr>
    <?php foreach ($parametersForOutput as $headerCell => $parameters) { ?>
        <?php if(empty($parameters)) {
            continue;
        } ?>
        <tr>
            <th colspan="3">
                <?=Yii::t('PatientModule.device', $headerCell);?>
            </th>
            <?php if (!$printFirstHeader) { ?>
                <?php $printFirstHeader = true; ?>
                <th class="text-center">
                    24<?=Yii::t('PatientModule.device', 'ч');?>
                </th>
                <th class="text-center">
                    <?=Yii::t('PatientModule.device', 'Средние значения');?>
                </th>
            <?php } else { ?>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            <?php } ?>
        </tr>
        <?php foreach ($parameters as $label => $value) { ?>
            <tr>
                <td>&nbsp;</td>
                <td><?=$label?></td>
                <?php if (is_array($value)) {?>
                    <td class="text-center noprint_hidden"><?=$value[0]?></td>
                    <td class="text-center"><?=$value[1]?></td>
                    <td class="text-center"><?=$value[2]?></td>
                <?php } else { ?>
                    <td class="text-center">&nbsp;</td>
                    <td class="text-center" colspan="2"><?=$value?></td>
                <?php } ?>
            </tr>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>
