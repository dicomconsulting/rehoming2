<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$sinceDate = $patient->device->lastState->getStartTimeOfCurrentValuePeriod(
    $parameters['last_follow_up'],
    true
);
?>
<table class = "table table-condensed rh-main-table">
    <col width="55%" valign="middle">
    <col width="15%" valign="middle" span='3'>
    <thead>
        <tr>
            <th><?php
                if ($sinceDate instanceof RhDateTime) {
                    echo CHtml::link(
                        Yii::t('PatientModule.arrhythmia', 'Желудочковая аритмия') . " " .
                        Yii::t('PatientModule.arrhythmia', 'с') . "&nbsp;" .
                        $this->printDateTime($sinceDate, true),
                        ['status/ventricular', 'id' => $patient->uid]
                    );
                } else {
                    echo CHtml::link(
                        Yii::t('PatientModule.arrhythmia', 'Желудочковая аритмия'),
                        ['status/ventricular', 'id' => $patient->uid]
                    );
                }
            ?></th>
            <th class="text-center">VT1</th>
            <th class="text-center">VT2</th>
            <th class="text-center">VF</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $vt1 = isset($parameters['vt1_episodes']);
        $vt2 = isset($parameters['vt2_episodes']);
        $vf = isset($parameters['vf_episodes']);
        if ($vt1 || $vt2 || $vf) { ?>
        <tr>
            <td><?=Yii::t('PatientModule.arrhythmia', 'Эпизоды');?></td>
            <td class="text-center"><?=$vt1 ? ($parameters['vt1_episodes']->getCountByCurrentPeriod() ? : 0 ) : '';?></td>
            <td class="text-center"><?=$vt2 ? ($parameters['vt2_episodes']->getCountByCurrentPeriod() ? : 0 ) : '';?></td>
            <td class="text-center"><?=$vf  ? ($parameters['vf_episodes']->getCountByCurrentPeriod() ? : 0 ) : '';?></td>
        </tr>
        <?php } ?>
        <?php
        $zonesStarted = isset($parameters['atp_in_vt_zones_started']);
        $zonesSuccessful = isset($parameters['atp_in_vt_zones_successful']);
        $shotStarted = isset($parameters['atp_one_shot_started']);
        $shotSuccessful = isset($parameters['atp_one_shot_successful']);
        if ($zonesStarted || $zonesSuccessful || $shotStarted || $shotSuccessful) { ?>
        <tr>
            <td><?=Yii::t('PatientModule.arrhythmia', 'ATС проведена/успешна');?></td>
            <td class="text-center" colspan ="2">
                <?=$zonesStarted ? ($parameters['atp_in_vt_zones_started']->getCountByCurrentPeriod() ? : 0 ) : '';?> /
                <?=$zonesSuccessful ? ($parameters['atp_in_vt_zones_successful']->getCountByCurrentPeriod() ? : 0 ) : '';?>
            </td>
            <td class="text-center">
                <?=$shotStarted ? ($parameters['atp_one_shot_started']->getCountByCurrentPeriod() ? : 0 ) : '';?> /
                <?=$shotSuccessful ? ($parameters['atp_one_shot_successful']->getCountByCurrentPeriod() ? : 0 ) : '';?>
            </td>
        </tr>
        <?php } ?>
        <?php
        $started = isset($parameters['shocks_started']);
        $aborted = isset($parameters['shocks_aborted']);
        $successful = isset($parameters['shocks_successful']);
        if ($started || $aborted || $successful) { ?>
        <tr>
            <td><?=Yii::t('PatientModule.arrhythmia', 'Набор зарядов/Прерванные разряды/Эффективные разряды');?></td>
            <td class="text-center" colspan ="3" valign="middle">
                <?=$started ? ($parameters['shocks_started']->getCountByCurrentPeriod() ? : 0 ) : '';?> /
                <?=$started ? ($parameters['shocks_aborted']->getCountByCurrentPeriod() ? : 0 ) : '';?> /
                <?=$started ? ($parameters['shocks_successful']->getCountByCurrentPeriod() ? : 0 ) : '';?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['last_episode_type'])) { ?>
            <tr>
                <td colspan ="4" valign="middle">
                    <?=$parameters['last_episode_type'];?>:
                    <?=$parameters['last_episode_type']->getCurrentValue();?>
                    (<?=$this->printDateTime($parameters['last_episode_detection']->getCurrentValue()->value);?>)
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
