<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
?>
<table class = "table table-condensed table-bordered rh-main-table">
    <col width="25%" valign="middle" span="4">
    <thead>
        <tr>
            <th colspan="4">
                <?=Yii::t('PatientModule.device', 'Общее');?>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?=$parameters['last_follow_up'];?></td>
            <td class="border-right"><?=$this->printDateTime($patient->device->lastState->last_follow_up);?></td>
            <td><?=$parameters['device_settings_no']->name;?></td>
            <td><?=$parameters['device_settings_no']->getCurrentValue();?></td>
        </tr>
    </tbody>
</table>
