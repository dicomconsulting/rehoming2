<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$graphManager = $this->getGraphManager();

$numberModeSwitchingYAxis = [0, 10, 20, 30, 40, 50, [60, '']];
$durationModeSwitchingYAxis = [0, 20, 40, 60, 80, 100];
$maxVentricularYAxis = [[100, ''], 120, 140, 160, 180, 200, 220, [240, '']];
$atrialCounterYAxis = [0, 10, 20,[30, '']];

$parametersForOutput = [
    'Эпизоды переключения режима' => [],
    'Предсердный ритм' => [],
    'Эпизоды высокого желудочкового ритма' => []
];

if (isset($parameters['number_of_mode_switching_episodes_per_day'])) {
    $parametersForOutput['Эпизоды переключения режима'][
        $parameters['number_of_mode_switching_episodes_per_day']->name
    ] = [
        $graphManager->buildGraph(
            $parameters['number_of_mode_switching_episodes_per_day'],
            Yii::t('PatientModule.device', 'Эпизоды переключения режима'),
            true,
            $numberModeSwitchingYAxis
        ),
        $parameters['number_of_mode_switching_episodes_per_day']->getCurrentValue(),
        ($parameters['number_of_mode_switching_episodes_per_day']->getMaxByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['duration_of_mode_switching_episodes'])) {
    $parametersForOutput['Эпизоды переключения режима'][
        $parameters['duration_of_mode_switching_episodes']->name . ' [' . $parameters['duration_of_mode_switching_episodes']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['duration_of_mode_switching_episodes'],
            Yii::t('PatientModule.device', 'Эпизоды переключения режима'),
            true,
            $durationModeSwitchingYAxis
        ),
        $parameters['duration_of_mode_switching_episodes']->getCurrentValue(),
        ($parameters['duration_of_mode_switching_episodes']->getMaxByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['max_ventricular_rate_at_mode_switching_episode'])) {
        $parametersForOutput['Эпизоды переключения режима'][
    $parameters['max_ventricular_rate_at_mode_switching_episode']->name . ' [' . $parameters['max_ventricular_rate_at_mode_switching_episode']->unit->name . ']'
    ] = [
        $graphManager->buildGraph(
            $parameters['max_ventricular_rate_at_mode_switching_episode'],
            null,
            false,
            $maxVentricularYAxis
        ),
        $parameters['max_ventricular_rate_at_mode_switching_episode']->getCurrentValue(),
        ($parameters['max_ventricular_rate_at_mode_switching_episode']->getMaxByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}

if (isset($parameters['atrial_tachycardia_per_day'])) {
    $parametersForOutput['Предсердный ритм'][
        $parameters['atrial_tachycardia_per_day']->name
    ] = [
        $graphManager->buildGraph(
            $parameters['atrial_tachycardia_per_day'],
            Yii::t('PatientModule.device', 'Счетчик предсердных аритмий'),
            true,
            $atrialCounterYAxis
        ),
        $parameters['atrial_tachycardia_per_day']->getCurrentValue(),
        ($parameters['atrial_tachycardia_per_day']->getMaxByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['atrial_flutter_per_day'])) {
    $parametersForOutput['Предсердный ритм'][
        $parameters['atrial_flutter_per_day']->name
    ] = [
        $graphManager->buildGraph(
            $parameters['atrial_flutter_per_day'],
            Yii::t('PatientModule.device', 'Счетчик предсердных аритмий'),
            true,
            $atrialCounterYAxis
        ),
        $parameters['atrial_flutter_per_day']->getCurrentValue(),
        ($parameters['atrial_flutter_per_day']->getMaxByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['atrial_fibrillation_per_day'])) {
    $parametersForOutput['Предсердный ритм'][
        $parameters['atrial_fibrillation_per_day']->name
    ] = [
        $graphManager->buildGraph(
            $parameters['atrial_fibrillation_per_day'],
            Yii::t('PatientModule.device', 'Счетчик предсердных аритмий'),
            true,
            $atrialCounterYAxis
        ),
        $parameters['atrial_fibrillation_per_day']->getCurrentValue(),
        ($parameters['atrial_fibrillation_per_day']->getMaxByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}

if (isset($parameters['max_ventricular_heart_rate'])) {
    $parametersForOutput['Эпизоды высокого желудочкового ритма'][
        $parameters['max_ventricular_heart_rate']->name . ' [' . $parameters['max_ventricular_heart_rate']->unit->name . ']'
    ] = [
        '',
        $parameters['max_ventricular_heart_rate']->getCurrentValue(),
        ($parameters['max_ventricular_heart_rate']->getMaxByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}
if (isset($parameters['duration_of_max_ventricular_heart_rate_episode'])) {
    $parametersForOutput['Эпизоды высокого желудочкового ритма'][
        $parameters['duration_of_max_ventricular_heart_rate_episode']->name . ' [' . $parameters['duration_of_max_ventricular_heart_rate_episode']->unit->name . ']'
    ] = [
        '',
        $parameters['duration_of_max_ventricular_heart_rate_episode']->getCurrentValue(),
        ($parameters['duration_of_max_ventricular_heart_rate_episode']->getMaxByCountValue() ? : DeviceValue::NULL_PLACEHOLDER)
    ];
}

$printFirstHeader = false;
?>
<table class = "table table-condensed rh-main-table">
    <col width="5%" valign="middle">
    <col width="30%" valign="middle">
    <col width="15%" valign="middle">
    <col width="25%" valign="middle">
    <col width="35%" valign="middle">
    <tbody>
    <tr>
        <th colspan ="4"></th>
        <th class="text-center">
            <?=Yii::t('PatientModule.device', 'C');?>
            <?=$this->sinceDateTimeString();?>
        </th>
    </tr>
    <?php foreach ($parametersForOutput as $headerCell => $parameters) { ?>
        <?php if(empty($parameters)) {
            continue;
        } ?>
        <tr>
            <th colspan="3">
                <?=Yii::t('PatientModule.device', $headerCell);?>
            </th>
            <?php if (!$printFirstHeader) { ?>
                <?php $printFirstHeader = true; ?>
                <th class="text-center">
                    24<?=Yii::t('PatientModule.device', 'ч');?>
                </th>
                <th class="text-center">
                    *<?=Yii::t('PatientModule.device', 'Макс. значения');?>
                </th>
            <?php } else { ?>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            <?php } ?>
        </tr>
        <?php foreach ($parameters as $label => $value) { ?>
            <tr>
                <td>&nbsp;</td>
                <td><?=$label?></td>
                <?php if (is_array($value)) {?>
                    <td class="text-center noprint_hidden"><?=$value[0]?></td>
                    <td class="text-center"><?=$value[1]?></td>
                    <td class="text-center"><?=$value[2]?>*</td>
                <?php } else { ?>
                    <td class="text-center">&nbsp;</td>
                    <td class="text-center" colspan="2"><?=$value?></td>
                <?php } ?>
            </tr>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>
