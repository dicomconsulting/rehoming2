<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;

$headerCell = 'Последний эпизод';
$cellValues = [];

if (isset($parameters['last_episode_number'])) {
    $cellValues[$parameters['last_episode_number']->name]
        = ($parameters['last_episode_number']->getCurrentValue()->value ? : 0);
}
if (isset($parameters['last_episode_type'])) {
    $cellValues[$parameters['last_episode_type']->name]
        = $parameters['last_episode_type']->getCurrentValue();
}
if (isset($parameters['last_episode_detection'])) {
    $cellValues[$parameters['last_episode_detection']->name]
        = $this->printDateTime($parameters['last_episode_detection']->getCurrentValue()->value);
}


$printFirstHeader = false;
?>
<?php if (!empty($cellValues)) { ?>
<table class = "table table-condensed rh-main-table">
    <col width="5%"  valign="middle">
    <col width="55%" valign="middle">
    <col width="40%" valign="middle">
    <tbody>
        <tr>
            <th colspan="3">
                <?=Yii::t('PatientModule.device', $headerCell);?>
            </th>
        </tr>
        <?php foreach ($cellValues as $label => $value) { ?>
            <tr>
                <td>&nbsp;</td>
                <td><?=$label?></td>
                <td class="text-center"><?=$value?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<?php } ?>