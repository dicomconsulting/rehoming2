<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;

$parametersForOutput = [
    'Динамика АВ задержки' => [
        'av_delay_at_60_bpm',
        'av_delay_at_80_bpm',
        'av_delay_at_100_bpm',
        'av_delay_at_120_bpm',
        'av_delay_at_140_bpm',
        'sense_compensation',
        'av_hysteresis',
        'av_repetitive_cycles',
        'av_scan_cycles'
    ],
    'Учащающая стимуляция предсердий' => [
        'atrial_overdrive'
,    ],
    'Рефрактерный период/Бланкировка/ПМТ' => [
        'auto_pvarp',
        'pvarp_setting',
        'pvarp_after_pvc',
        'atrial_refractory_period',
        'ventricular_refractory_period',
        'far_field_blanking_after_vs',
        'far_field_blanking_after_vp',
        'far_field_blanking_after_ap',
        'pmt_protection',
        'va_criterion'
    ],
    'CLS' => [
        'cls_response',
        'cls_resting_rate_control',
        'cls_vp_required'
    ]
];
?>
<table class = "table table-condensed rh-main-table">
    <col width="5%" valign="middle">
    <col width="55%" valign="middle">
    <col width="40%" valign="middle">
    <tbody>
    <?php foreach ($parametersForOutput as $headerCell => $parameterNames) { ?>
        <tr>
            <th colspan="3">
                <?=Yii::t('PatientModule.device', $headerCell);?>
            </th>
        </tr>
        <?php foreach ($parameterNames as $parameterName) { ?>
            <?php if (isset($parameters[$parameterName])) { ?>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <?=$parameters[$parameterName]?>
                        <?php if (!empty($parameters[$parameterName]->unit)) { ?>
                            [<?=$parameters[$parameterName]->unit->name?>]
                        <?php } ?>
                    </td>
                    <td class="text-center">
                        <?=$parameters[$parameterName]->getCurrentValue();?>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>
