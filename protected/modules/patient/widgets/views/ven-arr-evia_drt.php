<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$sinceDate = $patient->device->lastState->getStartTimeOfCurrentValuePeriod(
    $parameters['last_follow_up'],
    true
);
?>
<table class = "table table-condensed rh-main-table">
    <col width="75%" valign="middle">
    <col width="25%" valign="middle">
    <thead>
        <tr>
            <th><?php
                if ($sinceDate instanceof RhDateTime) {
                    echo CHtml::link(
                        Yii::t('PatientModule.arrhythmia', 'Желудочковая аритмия') . " " .
                        Yii::t('PatientModule.arrhythmia', 'с') . "&nbsp;" .
                        $this->printDateTime($sinceDate, true),
                        ['status/ventricular', 'id' => $patient->uid]
                    );
                } else {
                    echo CHtml::link(
                        Yii::t('PatientModule.arrhythmia', 'Желудочковая аритмия'),
                        ['status/ventricular', 'id' => $patient->uid]
                    );
                }
            ?></th>
            <th class="text-center">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <?=$parameters['high_ven_rate_episodes_per_day']->name;?>
            </td>
            <td class="text-center">
                <?=$parameters['high_ven_rate_episodes_per_day']->getCountByCurrentPeriod() ? : 0;?>
            </td>
        </tr>
    </tbody>
</table>
