<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
?>
<table class = "table table-condensed rh-main-table">
    <col width="50%" valign="middle">
    <col width="20%" valign="middle" span='2' align="center">
    <thead>
        <tr>
            <th><?=Yii::t('PatientModule.brady', 'Бради электроды');?></th>
            <th class="text-center"><?=Yii::t('PatientModule.brady', 'ПП');?></th>
            <th class="text-center"><?=Yii::t('PatientModule.brady', 'ПЖ');?></th>
        </tr>
    </thead>
    <tbody>
        <?php
        $ra = isset($parameters['ra_pacing_impedance']);
        $rv = isset($parameters['rv_pacing_impedance']);
        if (($ra || $rv)) { ?>
        <tr>
            <td><?=Yii::t('PatientModule.brady', 'Стимуляционное сопротивление');?>
            [<?php if ($ra) {
                    echo $parameters['ra_pacing_impedance']->unit->name;
                } elseif($rv) {
                    echo $parameters['rv_pacing_impedance']->unit->name;
                }
            ?>]</td>
            <td class="text-center"><?=$ra ? $parameters['ra_pacing_impedance']->getCurrentValue() : '';?></td>
            <td class="text-center"><?=$rv ? $parameters['rv_pacing_impedance']->getCurrentValue() : '';?></td>
        </tr>
        <?php } ?>
        <?php
        $ra = isset($parameters['ra_pacing_threshold']);
        $rv = isset($parameters['rv_pacing_threshold']);
        if ($ra || $rv) { ?>
        <tr>
            <td><?=Yii::t('PatientModule.brady', 'Амплитуда импульса');?>
            [<?php if ($ra) {
                    echo $parameters['ra_pacing_threshold']->unit->name;
                } elseif($rv) {
                    echo $parameters['rv_pacing_threshold']->unit->name;
                }
            ?>]
            </td>
            <td class="text-center"><?=$ra ? $parameters['ra_pacing_threshold']->getCurrentValue() : '';?></td>
            <td class="text-center"><?=$rv ? $parameters['rv_pacing_threshold']->getCurrentValue() : '';?></td>
        </tr>
        <?php } ?>
        <?php
        $ra = isset($parameters['ra_sensing_amplitude_daily_mean']);
        $rv = isset($parameters['rv_sensing_amplitude_daily_mean']);
        if ($ra || $rv) { ?>
        <tr>
            <td><?=Yii::t('PatientModule.brady', 'Амплитуда сигнала средняя/минимальная');?>
            [<?php if ($ra) {
                    echo $parameters['ra_sensing_amplitude_daily_mean']->unit->name;
                } elseif($rv) {
                    echo $parameters['rv_sensing_amplitude_daily_mean']->unit->name;
                }
            ?>]
            </td>
            <td class="text-center">
                <?=$ra ? $parameters['ra_sensing_amplitude_daily_mean']->getCurrentValue() : '';?>
                <?=$ra ? ' / ' . $parameters['ra_sensing_amplitude_daily_min']->getCurrentValue() : '';?>
            </td>
            <td class="text-center">
                <?=$rv ? $parameters['rv_sensing_amplitude_daily_mean']->getCurrentValue() : '';?>
                <?=$rv ? ' / ' . $parameters['rv_sensing_amplitude_daily_min']->getCurrentValue() : '';?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
