<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
?>
<table class = "table table-condensed table-bordered rh-main-table">
    <col width="30%" valign="middle">
    <col width="70%" valign="middle">
    <thead>
        <tr>
            <th colspan="2">
                <?=CHtml::link(Yii::t('PatientModule.patient', 'Статус устройства'), ['status/device', 'id' => $patient->uid]); ?>
            </th>
        </tr>
    </thead>
    <tbody>
        <?php if (isset($parameters['device_status'])) { ?>
        <tr>
            <td>
            <?=$parameters['device_status']->name;?>
            </td>
            <td>
            <?=$parameters['device_status']->getCurrentValue();?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['battery_status'])) { ?>
        <tr>
            <td>
            <?=$parameters['battery_status']->name;?>
            </td>
            <td>
            <?=$parameters['battery_status']->getCurrentValue();?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['battery_voltage'])) { ?>
        <tr>
            <td>
            <?=$parameters['battery_voltage']->name;?>
            </td>
            <td>
            <?=$parameters['battery_voltage']->getCurrentValue();?>
            <?=$parameters['battery_voltage']->unit->name;?>
            <?php if (isset($parameters['date_of_last_battery_voltage_measurement'])) { ?>
                <?php $date = $parameters['date_of_last_battery_voltage_measurement']->getCurrentValue()->value;?>
                (<?=$date->format('d');?>
                <?=$date->getMonthName();?>
                <?=$date->format('Y');?>)
            <?php } ?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['last_charge_time'])) { ?>
        <tr>
            <td>
            <?=$parameters['last_charge_time'];?>
            </td>
            <td>
            <?=$parameters['last_charge_time']->getCurrentValue();?>
            <?=$parameters['last_charge_time']->unit->name;?>
            <?=$parameters['last_charge_energy']->getCurrentValue();?>
            <?=$parameters['last_charge_energy']->unit->name;?>
            <?php if ($date = $parameters['time_of_last_charging']->getCurrentValue()->value) { ?>
                (<?=$date->format('d');?>
                <?=$date->getMonthName();?>
                <?=$date->format('Y');?>
                <?=$date->format('H') . ":" . $date->format('i') . ":" . $date->format('s');?>)
            <?php } ?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
