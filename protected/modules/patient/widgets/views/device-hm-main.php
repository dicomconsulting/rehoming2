<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$parametersForOutput = [
    'Домашний мониторинг (Home Monitoring)' => [],
    'Записи' => [],
    'Автоматическое измерение' => []
];

if (isset($parameters['time_of_transmission'])) {
    $parametersForOutput['Домашний мониторинг (Home Monitoring)']
        [$parameters['time_of_transmission']->name] = $parameters['time_of_transmission']->getCurrentValue();
}
if (isset($parameters['iegm_for_therapy_episodes'])) {
    $parametersForOutput['Домашний мониторинг (Home Monitoring)']
        [$parameters['iegm_for_therapy_episodes']->name] = $parameters['iegm_for_therapy_episodes']->getCurrentValue();
}
if (isset($parameters['iegm_for_monitoring_episodes'])) {
    $parametersForOutput['Домашний мониторинг (Home Monitoring)']
        [$parameters['iegm_for_monitoring_episodes']->name] = $parameters['iegm_for_monitoring_episodes']->getCurrentValue();
}
if (isset($parameters['periodic_iegm'])) {
    $parametersForOutput['Домашний мониторинг (Home Monitoring)'][
        $parameters['periodic_iegm']->name . ' [' . $parameters['periodic_iegm']->unit->name . ']'
    ] = $parameters['periodic_iegm']->getCurrentValue();
}
if (isset($parameters['ongoing_atrial_episode'])) {
    $parametersForOutput['Домашний мониторинг (Home Monitoring)'][
        $parameters['ongoing_atrial_episode']->name . ' [' . $parameters['ongoing_atrial_episode']->unit->name . ']'
    ] = $parameters['ongoing_atrial_episode']->getCurrentValue();
}

if (isset($parameters['recording_episodes_for_svt'])) {
    $parametersForOutput['Записи']
        [$parameters['recording_episodes_for_svt']->name] = $parameters['recording_episodes_for_svt']->getCurrentValue();
}
if (isset($parameters['recording_episodes_for_at_af'])) {
    $parametersForOutput['Записи']
        [$parameters['recording_episodes_for_at_af']->name] = $parameters['recording_episodes_for_at_af']->getCurrentValue();
}

if (isset($parameters['automatic_impedance_measurement'])) {
    $parametersForOutput['Автоматическое измерение']
        [$parameters['automatic_impedance_measurement']->name] = $parameters['automatic_impedance_measurement']->getCurrentValue();
}
if (isset($parameters['daily_shock_lead_imped_measurement'])) {
    $parametersForOutput['Автоматическое измерение']
        [$parameters['daily_shock_lead_imped_measurement']->name] = $parameters['daily_shock_lead_imped_measurement']->getCurrentValue();
}
if (isset($parameters['rv_atm'])) {
    $parametersForOutput['Автоматическое измерение']
        [$parameters['rv_atm']->name] = $parameters['rv_atm']->getCurrentValue();
}
if (isset($parameters['lv_atm'])) {
    $parametersForOutput['Автоматическое измерение']
        [$parameters['lv_atm']->name] = $parameters['lv_atm']->getCurrentValue();
}
if (isset($parameters['av_delay_adj_sensing_test'])) {
    $parametersForOutput['Автоматическое измерение'][
        $parameters['av_delay_adj_sensing_test']->name . ' [' . $parameters['av_delay_adj_sensing_test']->unit->name . ']'
    ] = $parameters['av_delay_adj_sensing_test']->getCurrentValue();
}
if (isset($parameters['thoracic_impedance_measurement'])) {
    $parametersForOutput['Автоматическое измерение']
        [$parameters['thoracic_impedance_measurement']->name] = $parameters['thoracic_impedance_measurement']->getCurrentValue();
}
if (isset($parameters['thoracic_impedance_current_gain'])) {
    $parametersForOutput['Автоматическое измерение']
        [$parameters['thoracic_impedance_current_gain']->name] = $parameters['thoracic_impedance_current_gain']->getCurrentValue();
}


?>
<table class = "table table-condensed table-bordered rh-main-table">
    <col width="5%" valign="middle">
    <col width="55%" valign="middle">
    <col width="40%" valign="middle">
    <tbody>
        <?php foreach ($parametersForOutput as $headerCell => $parameters) { ?>
            <?php if(empty($parameters)) {
                continue;
            } ?>
            <tr>
                <th colspan="3">
                    <?=Yii::t('PatientModule.device', $headerCell); ?>
                </th>
            </tr>
            <?php foreach ($parameters as $label => $value) { ?>
                <tr>
                    <td>&nbsp;</td>
                    <td><?=$label;?></td>
                    <td class="text-center"><?=$value;?></td>
                </tr>
            <?php } ?>
        <?php } ?>
    </tbody>
</table>