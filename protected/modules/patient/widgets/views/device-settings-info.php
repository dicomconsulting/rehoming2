<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
?>
<table class = "table table-bordered rh-main-table">
    <thead>
        <tr>
            <th>
                <?=$parameters['device_settings_no']->name;?>
                <?=$parameters['device_settings_no']->getCurrentValue()?>
                (<?=$this->printDateTime(
                    $patient->device->lastState->getStartTimeOfCurrentValuePeriod($parameters['device_settings_no']
                ));?>)
            </th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
