<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;

$implantationDate = $this->printDateTime($patient->device->implantation_date, true);
$measureVoltageDate = $this->printDateTime($parameters['date_of_last_battery_voltage_measurement']->getCurrentValue()->value, true);
$lastCharging = $this->printDateTime($parameters['time_of_last_charging']->getCurrentValue()->value);
$createdTime = $this->printDateTime($patient->device->lastState->create_time);
$lastMonitoring = $this->printDateTime($parameters['end_of_last_monitoring_interval']->getCurrentValue()->value);
$transmissionDate = $this->printDateTime($parameters['transmission_received_on']->getCurrentValue()->value);

$parametersForOutput = [
    'Батарея' => [
        $parameters['battery_status']->name => $parameters['battery_status']->getCurrentValue(),
        $parameters['battery_voltage']->name . ' [' . $parameters['battery_voltage']->unit->name . ']' => $parameters['battery_voltage']->getCurrentValue(),
        $parameters['date_of_last_battery_voltage_measurement']->name => $measureVoltageDate
    ],
    'Устройство' => [
        $parameters['device_status']->name => $parameters['device_status']->getCurrentValue(),
        Yii::t('PatientModule.device', 'Модель') => $patient->device->device_model,
        Yii::t('PatientModule.device', 'Дата имплантации') => $implantationDate
    ],
    'Последняя запись о дефибрилляции' => [
        $parameters['time_of_last_charging']->name => $lastCharging,
        $parameters['last_charge_time']->name . ' [' . $parameters['last_charge_time']->unit->name . ']'
            => $parameters['last_charge_time']->getCurrentValue(),
        $parameters['last_charge_energy']->name . ' [' . $parameters['last_charge_energy']->unit->name . ']'
            => $parameters['last_charge_energy']->getCurrentValue(),
        $parameters['impedance_of_last_shock']->name . ' [' . $parameters['impedance_of_last_shock']->unit->name . ']'
            => $parameters['impedance_of_last_shock']->getCurrentValue(),
        $parameters['remark']->name => $parameters['remark']->getCurrentValue(),
    ],
    'Домашний мониторинг (Home Monitoring)' => [
        $parameters['message_type']->name => $parameters['message_type']->getCurrentValue(),
        $parameters['device_message_created_on']->name => $createdTime,
        $parameters['end_of_last_monitoring_interval']->name => $lastMonitoring
    ],
    'Трансмиттер' => [
        Yii::t('PatientModule.device', 'Серийный номер трансмиттера') => $patient->device->transmitter_serial_number,
        $parameters['transmission_received_on']->name => $transmissionDate
    ]
];
?>

<table class = "table table-condensed rh-main-table">
    <col width="5%" valign="middle">
    <col width="60%" valign="middle">
    <col width="35%" valign="middle">
    <tbody>
    <?php foreach ($parametersForOutput as $headerCell => $parameters) { ?>
        <tr>
            <th colspan="2">
                <?=Yii::t('PatientModule.device', $headerCell);?>
            </th>
            <th>
                &nbsp;
            </th>
        </tr>
        <?php foreach ($parameters as $label => $value) { ?>
            <tr>
                <td>&nbsp;</td>
                <td><?=$label?></td>
                <td><?=$value?></td>
            </tr>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>
