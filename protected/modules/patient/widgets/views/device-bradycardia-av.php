<?php
/* @var $patient Patient */
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
?>
<table class = "table table-condensed table-bordered rh-main-table">
    <col width="60%" valign="middle">
    <col width="40%" valign="middle">
    <thead>
        <tr>
            <th colspan="2">
                <?=Yii::t('PatientModule.brady', 'АВ задержка'); ?>
            </th>
        </tr>
    </thead>
    <tbody>
        <?php if (isset($parameters['av_delay_1'])) { ?>
        <tr>
            <td>
                <?=$parameters['av_delay_1']->name;?>
            </td>
            <td class="text-center">
                <?=$parameters['av_delay_1']->getCurrentValue();?>
                <?=$parameters['av_delay_1']->getCurrentValue()->value ? $parameters['av_delay_1']->unit->name : '';?>
                <?php if ($parameters['av_delay_1_rate']->getCurrentValue()->value) {
                    echo ' /';
                    echo $parameters['av_delay_1_rate']->getCurrentValue();
                    echo $parameters['av_delay_1_rate']->unit->name;
                } ?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['av_delay_2'])) { ?>
        <tr>
            <td>
                <?=$parameters['av_delay_2']->name;?>
            </td>
            <td class="text-center">
                <?=$parameters['av_delay_2']->getCurrentValue();?>
                <?=$parameters['av_delay_2']->getCurrentValue()->value ? $parameters['av_delay_2']->unit->name : '';?>
                <?php if ($parameters['av_delay_2_rate']->getCurrentValue()->value) {
                    echo ' /';
                    echo $parameters['av_delay_2_rate']->getCurrentValue();
                    echo $parameters['av_delay_2_rate']->unit->name;
                } ?>
            </td>
        </tr>
        <?php } ?>
        <?php if (isset($parameters['sense_compensation'])) { ?>
        <tr>
            <td>
                <?=$parameters['sense_compensation']->name;?>
                [<?=$parameters['sense_compensation']->unit->name;?>]
            </td>
            <td class="text-center">
                <?=$parameters['sense_compensation']->getCurrentValue();?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
