<?php $this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $dataProvider,
    'htmlOptions' => array('class' => 'rh_table_grid'),
    'columns' => array(
        array(
            "header" => Yii::t("PatientModule.patient", "Номер эпизода"),
            "type" => "html",
            "htmlOptions" => array("class" => "center-column"),
            "value" => function($data, $row, $dataColumn) {
                    if (!$data->isFollowUp()) {
                        return $data->episode_number;
                    }
                }
        ),
        array(
            "header" => Yii::t("PatientModule.patient", "Дата"),
            "type" => "html",
            "value" => function ($data, $row, $dataColumn) {
                    /**  @var $data Recording */
                    if ($data->detection) {
                        return $this->printDateTime($data->detection);
                    } else {
                        return "---";
                    }
                }
        ),
        array(
            "header" => Yii::t("PatientModule.patient", "Тип"),
            "type" => "html",
            "value" => function ($data, $row, $dataColumn) {
                    if (!$data->isFollowUp()) {
                        /**  @var $data Recording */
                        $urlParts = array("recording/item", "state_id" => $data->state_uid, "id" => $data->device->patient_id);
                        return CHtml::link($data->episode_type, $urlParts);
                    } else {
                        return Yii::t("PatientModule.patient", "Обследование");
                    }
                }
        ),
        array(
            "header" => Yii::t("PatientModule.patient", "АТС"),
            "type" => "html",
            "value" => function ($data, $row, $dataColumn) {
                    /**  @var $data Recording */
                    if (!$data->isFollowUp()) {
                        return $data->atp_one_shot_delivered;
                    }
                }
        ),
        array(
            "header" => Yii::t("PatientModule.patient", "Шок нанесен"),
            "type" => "html",
            "value" => function ($data, $row, $dataColumn) {
                    /**  @var $data Recording */
                    if (!$data->isFollowUp()) {
                        return $data->episode_shocks_delivered;
                    }
                }
        ),
        array(
            "header" => Yii::t("PatientModule.patient", "Шок прерван"),
            "type" => "html",
            "value" => function ($data, $row, $dataColumn) {
                    if (!$data->isFollowUp()) {
                        /**  @var $data Recording */
                        return $data->episode_shocks_aborted;
                    }
                }
        ),
        array(
            "header" => Yii::t("PatientModule.patient", "Предистория PP/RR"),
            "type" => "html",
            "value" => function ($data, $row, $dataColumn) {
                    if (!$data->isFollowUp()) {
                        /**  @var $data Recording */
                        return $data->mean_pp_at_initial_detection . ($data->mean_rr_at_initial_detection ? ("/" . $data->mean_rr_at_initial_detection) : "");
                    }
                }
        ),
        array(
            "header" => Yii::t("PatientModule.patient", "Перед купированием PP/RR"),
            "type" => "html",
            "value" => function ($data, $row, $dataColumn) {
                    if (!$data->isFollowUp()) {
                        /**  @var $data Recording */
                        return $data->mean_pp_at_termination . ($data->mean_rr_at_termination ? ("/" . $data->mean_rr_at_termination) : "");
                    }
                }
        ),
    ),
));