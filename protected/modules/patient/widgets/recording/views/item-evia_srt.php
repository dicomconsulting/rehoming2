<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 24.10.13
 * Time: 10:26
 *
 * @var $patient Patient
 * @var $record Recording
 *
 */
?>

<div class="margin30">
<div class="row">
<div class="span5">
    <table class = "table table-condensed table-bordered rh-main-table">
        <col width="50%" valign="middle">
        <col width="50%" valign="middle">
        <thead>
        <tr>
            <th colspan="2">
                <?=Yii::t('PatientModule.device', 'Общее'); ?>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <?=Yii::t('PatientModule.patient', 'Номер'); ?>
            </td>
            <td>
                <?=$record->episode_number;?>
            </td>
        </tr>
        <?php if($record->detection):?>
            <tr>
                <td>
                    <?=Yii::t('PatientModule.patient', 'Дата обнаружения'); ?>
                </td>
                <td>
                    <?=$this->printDateTime($record->detection);?>
                </td>
            </tr>
        <?php endif;?>
        <tr>
            <td>
                <?=Yii::t('PatientModule.patient', 'Тип'); ?>
            </td>
            <td>
                <?=$record->episode_type;?>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<?php if ($record->existECGImage()) { ?>
    <div class="span10">
        <table class = "table table-condensed table-bordered rh-main-table">
            <thead>
            <tr>
                <th>
                    <?=Yii::t('PatientModule.patient', 'ЭКГ'); ?>
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="text-center">
                    <?php $srcUrl = CHtml::normalizeUrl(["/patient/recording/ecgImage", 'deviceId' => $record->device_id, 'episodeNumber' => $record->episode_number]);?>
                    <?=CHtml::image($srcUrl, '');?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
<?php } ?>
</div>

</div>