<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 24.10.13
 * Time: 10:26
 *
 * @var $patient Patient
 * @var $record Recording
 *
 */
?>

<div class="margin30">
<div class="row">
<div class="span5">
    <table class = "table table-condensed table-bordered rh-main-table">
        <col width="50%" valign="middle">
        <col width="50%" valign="middle">
        <thead>
        <tr>
            <th colspan="2">
                <?=Yii::t('PatientModule.device', 'Общее'); ?>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <?=Yii::t('PatientModule.patient', 'Номер'); ?>
            </td>
            <td>
                <?=$record->episode_number;?>
            </td>
        </tr>
        <?php if($record->detection):?>
            <tr>
                <td>
                    <?=Yii::t('PatientModule.patient', 'Дата обнаружения'); ?>
                </td>
                <td>
                    <?=$this->printDateTime($record->detection);?>
                </td>
            </tr>
        <?php endif;?>
        <tr>
            <td>
                <?=Yii::t('PatientModule.patient', 'Купирование'); ?>
            </td>
            <td>
                <?=$record->termination ? $this->printDateTime($record->termination) : '---';?>
            </td>
        </tr>
        <tr>
            <td>
                <?=Yii::t('PatientModule.patient', 'Тип'); ?>
            </td>
            <td>
                <?=$record->episode_type;?>
            </td>
        </tr>
        <tr>
            <td>
                <?=Yii::t('PatientModule.patient', 'Длительность'); ?>
            </td>
            <td>
                <?=$record->episode_duration ? : '---';?>
            </td>
        </tr>
        <tr>
            <td>
                <?=Yii::t('PatientModule.patient', 'Номер настроек'); ?>
            </td>
            <td>
                <?=$record->device_settings_no ? : '---';?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong><?=Yii::t('PatientModule.patient', 'Обнаружение'); ?></strong>
            </td>
        </tr>


        <tr>
            <td>
                <?=Yii::t('PatientModule.patient', 'Предистория PP'); ?>
            </td>
            <td>
                <?=$record->mean_pp_at_initial_detection ? : '---';?>
            </td>
        </tr>
        <tr>
            <td>
                <?=Yii::t('PatientModule.patient', 'Предистория RR'); ?>
            </td>
            <td>
                <?=$record->mean_rr_at_initial_detection ? : '---';?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong><?=Yii::t('PatientModule.patient', 'Детали'); ?></strong>
            </td>
        </tr>
        <tr>
            <td>
                <?=Yii::t('PatientModule.patient', 'Начало [%]'); ?>
            </td>
            <td>
                <?=$record->onset ? : '---';?>
            </td>
        </tr>
        <tr>
            <td>
                <?=Yii::t('PatientModule.patient', 'Стабильность [мс]'); ?>
            </td>
            <td>
                <?=$record->stability ? : '---';?>
            </td>
        </tr>
        <tr>
            <td>
                <?=Yii::t('PatientModule.patient', 'Повторное обнаружение'); ?>
            </td>
            <td>
                <?=$record->redetection ? : '---';?>
            </td>
        </tr>
        </tbody>
    </table>
</div>


<div class="span5">
    <table class = "table table-condensed table-bordered rh-main-table">
        <col width="50%" valign="middle">
        <col width="50%" valign="middle">
        <thead>
        <tr>
            <th colspan="2">
                <?=Yii::t('PatientModule.patient', 'Терапия'); ?>
            </th>
        </tr>
        </thead>
        <tbody>

        <tr>
            <td>
                <?=Yii::t('PatientModule.patient', 'АТС'); ?>
            </td>
            <td>
                <?=$record->atp_in_vt_vf_delivered ? : '---';?>
            </td>
        </tr>

        <tr>
            <td>
                <?=Yii::t('PatientModule.patient', 'Нанесен «ATP One Shot delivered»'); ?>
            </td>
            <td>
                <?=$record->atp_one_shot_delivered ? : '---';?>
            </td>
        </tr>
        <tr>
            <td>
                <?=Yii::t('PatientModule.patient', 'Шок нанесен'); ?>
            </td>
            <td>
                <?=$record->episode_shocks_delivered ? : '---';?>
            </td>
        </tr>

        <tr>
            <td>
                <?=Yii::t('PatientModule.patient', 'Шок прерван'); ?>
            </td>
            <td>
                <?=$record->episode_shocks_aborted ? : '---';?>
            </td>
        </tr>

        <tr>
            <td>
                <?=Yii::t('PatientModule.patient', 'Максимальная энергия [Дж]'); ?>
            </td>
            <td>
                <?=$record->maximum_energy ? : '---';?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong><?=Yii::t('PatientModule.patient', 'Купирование'); ?></strong>
            </td>
        </tr>

        <tr>
            <td>
                <?=Yii::t('PatientModule.patient', 'Перед купированием PP'); ?>
            </td>
            <td>
                <?=$record->mean_pp_at_termination ? : '---';?>
            </td>
        </tr>


        <tr>
            <td>
                <?=Yii::t('PatientModule.patient', 'Перед купированием RR'); ?>
            </td>
            <td>
                <?=$record->mean_rr_at_termination ? : '---';?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong><?=Yii::t('PatientModule.patient', 'Remark'); ?></strong>
            </td>
        </tr>

        <tr>
            <td>
                <?=Yii::t('PatientModule.patient', 'Обнаружение'); ?>
            </td>
            <td>
                <?=$record->remark_detection ? : '---';?>
            </td>
        </tr>

        <tr>
            <td>
                <?=Yii::t('PatientModule.patient', 'Терапия'); ?>
            </td>
            <td>
                <?=$record->remark_therapy ? : '---';?>
            </td>
        </tr>
        <tr>
            <td>
                <?=Yii::t('PatientModule.patient', 'Купирование'); ?>
            </td>
            <td>
                <?=$record->remark_termination ? : '---';?>
            </td>
        </tr>


        </tbody>
    </table>
</div>
<?php if ($record->existECGImage()) { ?>
    <div class="span10">
        <table class = "table table-condensed table-bordered rh-main-table">
            <thead>
            <tr>
                <th>
                    <?=Yii::t('PatientModule.patient', 'ЭКГ'); ?>
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="text-center">
                    <?php $srcUrl = CHtml::normalizeUrl(["/patient/recording/ecgImage", 'deviceId' => $record->device_id, 'episodeNumber' => $record->episode_number]);?>
                    <?=CHtml::image($srcUrl, '');?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
<?php } ?>
</div>

</div>