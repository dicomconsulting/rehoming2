<?php $this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $dataProvider,
    'htmlOptions' => array('class' => 'rh_table_grid'),
    'columns' => array(
        array(
            "header" => Yii::t("PatientModule.patient", "Номер эпизода"),
            "type" => "html",
            "htmlOptions" => array("class" => "center-column"),
            "value" => function($data, $row, $dataColumn) {
                    if (!$data->isFollowUp()) {
                        return $data->episode_number;
                    }
                }
        ),
        array(
            "header" => Yii::t("PatientModule.patient", "Дата"),
            "type" => "html",
            "value" => function ($data, $row, $dataColumn) {
                    /**  @var $data Recording */
                    if ($data->detection) {
                        $date = $data->detection;
                        $str = $date->format('d') . " ";
                        $str .= $date->getMonthName() . " ";
                        $str .= $date->format('Y H:i:s');
                        return $this->printDateTime($data->detection);
                    } else {
                        return "---";
                    }
                }
        ),
        array(
            "header" => Yii::t("PatientModule.patient", "Тип"),
            "type" => "html",
            "value" => function ($data, $row, $dataColumn) {
                    if (!$data->isFollowUp()) {
                        /**  @var $data Recording */
                        $urlParts = array("recording/item", "state_id" => $data->state_uid, "id" => $data->device->patient_id);
                        return CHtml::link($data->episode_type, $urlParts);
                    } else {
                        return Yii::t("PatientModule.patient", "Обследование");
                    }
                }
        ),
    ),
));