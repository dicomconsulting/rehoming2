<?php

class RecordingItemWidget extends RecordingBaseWidget
{
    public $record;

    public $viewName = 'item';

    public function internalRun()
    {
        $this->render($this->viewName, ['patient' => $this->patient, 'record' => $this->record]);
    }
}
