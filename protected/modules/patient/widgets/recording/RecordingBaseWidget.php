<?php

abstract class RecordingBaseWidget extends CWidget
{
    /**
     * @var Patient
     */
    public $patient;

    public $viewName = '';

    abstract public function internalRun();

    public function run()
    {
        $this->internalRun();
    }

    public function render($view, $data = null, $return = false)
    {
        $customViewName = $this->_getCustomViewFileName($view);
        if (($viewFile=$this->getViewFile($customViewName))!==false) {
            $view = $customViewName;
        }
        return parent::render($view, $data, $return);
    }

    public function printDateTime($dateTime, $onlyDate = false)
    {
        $output = DeviceValue::NULL_PLACEHOLDER;
        if ($dateTime instanceof RhDateTime) {
            $output = $dateTime->format('d') . '&nbsp;' . $dateTime->getMonthName() . '&nbsp;' . $dateTime->format('Y');
            if (!$onlyDate) {
                $output .= ' ' . $dateTime->format('H:i:s');
            }
        }

        return $output;
    }

    private function _getCustomViewFileName($view)
    {
        return $view . '-' . $this->_transformModelNameToViewPrefix(
            $this->patient->device->device_model
        );

    }

    private function _transformModelNameToViewPrefix($modelName)
    {
        return str_replace(
            ['/\s+/', '(', ')', '-', ' '],
            ['', '', '', '', '_'],
            strtolower($modelName)
        );
    }
}
