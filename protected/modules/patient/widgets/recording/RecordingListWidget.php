<?php

class RecordingListWidget extends RecordingBaseWidget
{
    public $dataProvider;

    public $viewName = 'list';

    public function internalRun()
    {
        $this->render($this->viewName, ['patient' => $this->patient, 'dataProvider' => $this->dataProvider]);
    }
}
