<?php

return array (
  'ATС проведена/успешна' => 'ATP started / succ.',
  'Аритмии' => 'Arrhythmia',
  'Желудочковая аритмия' => 'Ventricular arrhythmia',
  'Набор зарядов/Прерванные разряды/Эффективные разряды' => 'Shocks started/aborted/succ.',
  'Навязанный ритм' => 'Event episodes',
  'Предсердная аритмия' => 'Atrial arrhythmias',
  'Стимуляция' => 'Pacing',
  'Эпизоды' => 'Episodes',
  'с' => 'since',
  'средние значения' => 'mean values',
);
