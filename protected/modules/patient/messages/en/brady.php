<?php

return array (
  'АВ задержка' => 'AV delay',
  'АВ задержка на' => 'AV delay at',
  'АВ задержка на 60/140 уд.мин' => 'AV delay at 60/140 bpm',
  'Амплитуда импульса' => 'Pacing threshold',
  'Амплитуда сигнала средняя/минимальная' => 'Sensing ampl. mean / min',
  'Бради' => 'Brady',
  'Бради электроды' => 'Brady leads',
  'Бради/РТС/Настройки ФП' => 'Brady/CRT/AF settings',
  'Длительность импульса' => 'Pulse width',
  'Запрограммированный' => 'Programmed',
  'ЛЖ' => 'LV lead',
  'ПЖ' => 'RV lead',
  'ПП' => 'RA lead',
  'РТС' => 'CRT',
  'Режим' => 'Mode',
  'Стимуляционное сопротивление' => 'Pacing impedance',
  'Электрод' => 'Lead',
);
