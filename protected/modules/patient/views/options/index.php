<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 24.10.13
 * Time: 10:26
 *
 * @var Patient $patient
 * @var Section[] $sections
 * @var AbstractOption[] $options
 *
 */
?>

<div class="margin30">
<div class="row">
<div class="span10">
<?php foreach($sections as $section):?>
    <?php if (!isset($options[$section->uid])) continue; ?>
    <table class = "table table-condensed table-bordered rh-main-table">
        <col width="10%" valign="middle">
        <col width="90%" valign="middle">
        <thead>
        <tr>
            <th colspan="2">
                <?=Yii::t('patient', $section->name); ?>
            </th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($options[$section->uid] as $option):?>
        <?php
            /** @var AbstractOption $option */
            ?>
        <tr>
            <td>
                <?php switch($option->getLevel()){
                    case AbstractOption::ALERT_LEVEL_RED_WITH_NOTICE:
                        ?>
                        <div class="noprint">
                        <img src="/images/IconAlertRed.gif" width="13" height="14"><img src="/images/IconPlus.gif" width="14" height="14"><img src="/images/IconMessage.gif" width="14" height="14">
                        </div>
                        <div class="only_for_print"><strong><?=Yii::t("patient", "красный c сообщением");?></strong></div>
                        <?php
                        break;
                    case AbstractOption::ALERT_LEVEL_YELLOW_WITH_NOTICE:
                        ?>
                        <div class="noprint">
                        <img src="/images/IconAlertAmber.gif" width="13" height="14"><img src="/images/IconPlus.gif" width="14" height="14"><img src="/images/IconMessage.gif" width="14" height="14">
                        </div>
                        <div class="only_for_print"><strong><?=Yii::t("patient", "желтый c сообщением");?></strong></div>
                        <?php
                        break;
                    case AbstractOption::ALERT_LEVEL_YELLOW:
                        ?>
                        <div class="noprint">
                        <img src="/images/IconAlertAmber.gif" width="13" height="14">
                        </div>
                        <div class="only_for_print"><strong><?=Yii::t("patient", "желтый");?></strong></div>
                        <?php
                        break;
                }
                ?>
            </td>
            <td>
                <?=$option->getFormattedName();?>
            </td>
        </tr>
        <?php endforeach;?>
        </tbody>
    </table>
<?php endforeach;?>

</div>
</div>

<div class="right noprint" style="text-align: right;">
    <i class="icon-print"></i> <?=CHtml::link(Yii::t("PatientModule.patient", "Печать"), $this->createUrl($this->id . "/print", $_GET))?>
</div>
</div>