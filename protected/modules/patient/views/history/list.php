<?php
/**
 * @var $patient Patient
 * @var $ps integer
 * @var $dataProvider CDataProvider *
 */
Yii::app()->clientScript->registerCssFile('/css/grid/style.css');

?>

<div style="float:left;">
    <?=Yii::t("PatientModule.patient", "Выводить")?>:
    <?php if ($ps == 10):?>10<?php else:?><?=CHtml::link(10, array("/patient/history/list", "ps" => 10, "id" => $this->patient->uid))?><?php endif;?>
    <?php if ($ps == 20):?>20<?php else:?><?=CHtml::link(20, array("/patient/history/list", "ps" => 20, "id" => $this->patient->uid))?><?php endif;?>
    <?php if ($ps == 50):?>50<?php else:?><?=CHtml::link(50, array("/patient/history/list", "ps" => 50, "id" => $this->patient->uid))?><?php endif;?>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
        'dataProvider' => $dataProvider,
        'htmlOptions' => array('class' => 'rh_table_grid'),
        'columns' => array(
            array(
                "header" => Yii::t("PatientModule.patient", "Дата"),
                "type" => "html",
                "value" => function ($data,$row,$dataColumn) {
                    $dateString = $data->time->format('d') . " ";
                    $dateString .= $data->time->getMonthName() . " ";
                    $dateString .= $data->time->format('Y H:i:s');
                    return $dateString;
                }
            ),
            array(
                "header" => Yii::t("PatientModule.patient", "Сообщение"),
                "type" => "html",
                "value" => '$data->text'
            )
        ),
    ));
