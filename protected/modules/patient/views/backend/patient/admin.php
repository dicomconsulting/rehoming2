<?php
/* @var $this PatientController */
/* @var $model Patient */

$this->menu=array(
    array('label'=>'List Patient', 'url'=>array('index')),
    array('label'=>'Create Patient', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript(
    'search',
    "$('.search-button').click(function(){
        $('.search-form').toggle();

        return false;
    });
    $('.search-form form').submit(function(){
        $('#patient-grid').yiiGridView('update', {
            data: $(this).serialize()
        });

        return false;
    });
    "
);
?>

<h1>Manage Patients</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search', '#', array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial(
    '_search',
    array(
        'model'=>$model,
    )
); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'patient-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'uid',
        'human_readable_id',
        'external_uid',
        'comment',
        'group_id',
        'last_view_time',
        'consent',
        'birth_date',
        'title',
        array(
            'class'=>'CButtonColumn',
        ),
    ),
));
