<?php
/* @var $this PatientController */
/* @var $model Patient */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id'                   => 'patient-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'human_readable_id'); ?>
        <?php echo $form->textField($model, 'human_readable_id', array('size' => 50, 'maxlength' => 50)); ?>
        <?php echo $form->error($model, 'human_readable_id'); ?>
    </div>
    <?php
    /*

    <div class="row">
        <?php echo $form->labelEx($model, 'name'); ?>
        <?php echo $form->textField($model, 'name', array('size'=>60, 'maxlength'=>100)); ?>
        <?php echo $form->error($model, 'name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'surname'); ?>
        <?php echo $form->textField($model, 'surname', array('size'=>60, 'maxlength'=>100)); ?>
        <?php echo $form->error($model, 'surname'); ?>
    </div>
     */
    ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'external_uid'); ?>
        <?php echo $form->textField($model, 'external_uid', array('size' => 60, 'maxlength' => 100)); ?>
        <?php echo $form->error($model, 'external_uid'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'comment'); ?>
        <?php echo $form->textArea($model, 'comment', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'comment'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'group_id'); ?>
        <?php echo $form->textField($model, 'group_id'); ?>
        <?php echo $form->error($model, 'group_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'last_view_time'); ?>
        <?php echo $form->textField($model, 'last_view_time'); ?>
        <?php echo $form->error($model, 'last_view_time'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'consent'); ?>
        <?php echo $form->textField($model, 'consent'); ?>
        <?php echo $form->error($model, 'consent'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'birth_date'); ?>
        <?php echo $form->textField($model, 'birth_date'); ?>
        <?php echo $form->error($model, 'birth_date'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'title'); ?>
        <?php echo $form->textField($model, 'title', array('size' => 60, 'maxlength' => 100)); ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
