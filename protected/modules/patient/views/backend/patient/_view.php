<?php
/* @var $this PatientController */
/* @var $data Patient */
?>

<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('uid')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->uid), array('view', 'id'=>$data->uid)); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('human_readable_id')); ?>:</b>
    <?php echo CHtml::encode($data->human_readable_id); ?>
    <br />
    <?php
    /* Перс данные пациента
    <b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
    <?php echo CHtml::encode($data->name); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('surname')); ?>:</b>
    <?php echo CHtml::encode($data->surname); ?>
    <br />
     */
    ?>

    <b><?php echo CHtml::encode($data->getAttributeLabel('external_uid')); ?>:</b>
    <?php echo CHtml::encode($data->external_uid); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('comment')); ?>:</b>
    <?php echo CHtml::encode($data->comment); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('group_id')); ?>:</b>
    <?php echo CHtml::encode($data->group_id); ?>
    <br />

    <?php /*
    <b><?php echo CHtml::encode($data->getAttributeLabel('last_view_time')); ?>:</b>
    <?php echo CHtml::encode($data->last_view_time); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('consent')); ?>:</b>
    <?php echo CHtml::encode($data->consent); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('birth_date')); ?>:</b>
    <?php echo CHtml::encode($data->birth_date); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
    <?php echo CHtml::encode($data->title); ?>
    <br />

    */ ?>

</div>
