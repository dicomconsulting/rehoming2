<?php
/* @var $this PatientController */
/* @var $model Patient */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>

    <div class="row">
        <?php echo $form->label($model, 'uid'); ?>
        <?php echo $form->textField($model, 'uid'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'human_readable_id'); ?>
        <?php echo $form->textField($model, 'human_readable_id', array('size'=>50, 'maxlength'=>50)); ?>
    </div>

    <?php
    /*

    <div class="row">
        <?php echo $form->label($model, 'name'); ?>
        <?php echo $form->textField($model, 'name', array('size'=>60, 'maxlength'=>100)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'surname'); ?>
        <?php echo $form->textField($model, 'surname', array('size'=>60, 'maxlength'=>100)); ?>
    </div>
*/
    ?>
    <div class="row">
        <?php echo $form->label($model, 'external_uid'); ?>
        <?php echo $form->textField($model, 'external_uid', array('size'=>60, 'maxlength'=>100)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'comment'); ?>
        <?php echo $form->textArea($model, 'comment', array('rows'=>6, 'cols'=>50)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'group_id'); ?>
        <?php echo $form->textField($model, 'group_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'last_view_time'); ?>
        <?php echo $form->textField($model, 'last_view_time'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'consent'); ?>
        <?php echo $form->textField($model, 'consent'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'birth_date'); ?>
        <?php echo $form->textField($model, 'birth_date'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'title'); ?>
        <?php echo $form->textField($model, 'title', array('size'=>60, 'maxlength'=>100)); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
