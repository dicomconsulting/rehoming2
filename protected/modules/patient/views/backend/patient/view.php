<?php
/* @var $this PatientController */
/* @var $model Patient */

$this->menu=array(
    array('label'=>'List Patient', 'url'=>array('index')),
    array('label'=>'Create Patient', 'url'=>array('create')),
    array('label'=>'Update Patient', 'url'=>array('update', 'id'=>$model->uid)),
    array('label'=>'Delete Patient', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?')),
    array('label'=>'Manage Patient', 'url'=>array('admin')),
);
?>

<h1>View Patient #<?php echo $model->uid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data'=>$model,
    'attributes'=>array(
        'uid',
        'human_readable_id',
        /*
        'name',
        'surname',
        */
        'external_uid',
        'comment',
        'group_id',
        'last_view_time',
        'consent',
        'birth_date',
        'title',
    ),
));
