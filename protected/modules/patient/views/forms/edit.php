<?php
/* @var $this ClinicalDataController */
/* @var $model ClinicalData */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'clinical-data-edit-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'patient_uid'); ?>
		<?php echo $form->textField($model,'patient_uid'); ?>
		<?php echo $form->error($model,'patient_uid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_uid'); ?>
		<?php echo $form->textField($model,'user_uid'); ?>
		<?php echo $form->error($model,'user_uid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nyha_class'); ?>
		<?php echo $form->textField($model,'nyha_class'); ?>
		<?php echo $form->error($model,'nyha_class'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'termination_reason'); ?>
		<?php echo $form->textField($model,'termination_reason'); ?>
		<?php echo $form->error($model,'termination_reason'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'create_date'); ?>
		<?php echo $form->textField($model,'create_date'); ?>
		<?php echo $form->error($model,'create_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'termination_date'); ?>
		<?php echo $form->textField($model,'termination_date'); ?>
		<?php echo $form->error($model,'termination_date'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->