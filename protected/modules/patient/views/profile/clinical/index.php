<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.11.13
 * Time: 11:07
 *
 * @var ClinicalController $this
 * @var Patient $patient
 * @var ClinicalData $latestData
 * @var ClinicalData[] $records
 * @var bool $latest
 *
 */

Yii::app()->clientScript->registerCssFile('/css/grid/style.css');

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->assetManager->publish(
        Yii::getPathOfAlias('patient.assets').'/js/profile.js'
    ),
    CClientScript::POS_END
);

?>

<?php if(Yii::app()->user->checkAccess("editClinicalData")):?>
<p class="margin20">
<?=CHtml::link(Yii::t("patient", "Добавить клинические данные"), $this->createUrl(
    $this->id . "/edit",
    array("id" => $patient->uid)
));
?></p>
<?php endif;?>

<?php if ($latestData):?>
    <div class="row">
        <div class="span8">
            <?php if ($latest):?>
                <h3><?=Yii::t("patient", "Последние данные")?></h3>
            <?php else:?>
                <h3><?=Yii::t("patient", "Данные на дату")?></h3>
            <?php endif;?>
        </div>
    </div>
    <div class="row">
        <div class="span4">
            <?=Yii::t("patient", "Дата")?>
        </div>
        <div class="span4">
            <?=$latestData->date_create->format()?>
        </div>
    </div>

        <div class="row margin10">
            <div class="span4">
                <?=Yii::t("patient", $latestData->getAttributeLabel("weight"))?>
            </div>
            <div class="span4">
                <?=$latestData->weight?>
            </div>
            <div class="span1">
                <?=CHtml::link(
                    Yii::t("patient", "история"),
                    $this->createUrl(
                        $this->id . "/history",
                        ['id' => $this->patient->uid, 'parameter' => 'weight']
                    )
                );?>
            </div>
        </div>
        <div class="row margin10">
            <div class="span4">
                <?=Yii::t("patient", $latestData->getAttributeLabel("height"))?>
            </div>
            <div class="span4">
                <?=$latestData->height?>
            </div>
            <div class="span1">
                <?=CHtml::link(
                    Yii::t("patient", "история"),
                    $this->createUrl(
                        $this->id . "/history",
                        ['id' => $this->patient->uid, 'parameter' => 'height']
                    )
                );?>
            </div>
        </div>
        <div class="row margin10">
            <div class="span4">
                <?=Yii::t("patient", $latestData->getAttributeLabel("nyha_class"))?>
            </div>
            <div class="span4">
                <?=$latestData->nyha->name?>
            </div>
            <div class="span1">
                <?=CHtml::link(
                    Yii::t("patient", "история"),
                    $this->createUrl(
                        $this->id . "/history",
                        ['id' => $this->patient->uid, 'parameter' => 'nyha']
                    )
                );?>
            </div>
        </div>
        <div class="row margin10">
            <div class="span4">
                <?=Yii::t("patient", $latestData->getAttributeLabel("icd10_main"))?>
            </div>
            <div class="span4">
                <?php if($latestData->icd10_main){
                    foreach ($latestData->icd10_main as $icd10) {
                        ?>
                        <?=$icd10->name?><br />
                    <?php
                    }
                };?>
            </div>
            <div class="span1">
                <?=CHtml::link(
                    Yii::t("patient", "история"),
                    $this->createUrl(
                        $this->id . "/history",
                        ['id' => $this->patient->uid, 'parameter' => 'icd10_main']
                    )
                );?>
            </div>
        </div>
        <div class="row margin10">
            <div class="span4">
                <?php foreach ($latestData->icd10 as $desease):?>
                    <?=$desease->name?><br />
                <?php endforeach;?>
            </div>
        </div>

        <div class="row margin10">
            <div class="span4">
                <?=$latestData->getParameterName("therapy")?>
            </div>
            <div class="span4">
                <?php foreach ($latestData->therapy as $pharm):?>
                <div class="row">
                <div class="span2">
                    <?=$pharm->pharmProduct->name?>
                </div>
                <div class="span2">
                    <?=$pharm->dose?> <?=$pharm->measureUnit->name?>
                </div>
                </div>
                <?php endforeach;?>
            </div>
            <div class="span1">
                <?=CHtml::link(
                    Yii::t("patient", "история"),
                    $this->createUrl(
                        $this->id . "/history",
                        ['id' => $this->patient->uid, 'parameter' => 'therapy']
                    )
                );?>
            </div>
        </div>

        <div class="row margin10">
            <div class="span4">
                <?=Yii::t("patient", "Перенесенные операции")?>
            </div>
            <div class="span4">
                <?php foreach ($latestData->surgeryRel as $surg):?>
                <div class="row">
                <div class="span2">
                    <?=$surg->surgery->name?>
                </div>
                <div class="span2">
                    <?=$surg->date?>
                </div>
                </div>
                <?php endforeach;?>
            </div>
            <div class="span1">
                <?=CHtml::link(
                    Yii::t("patient", "история"),
                    $this->createUrl(
                        $this->id . "/history",
                        ['id' => $this->patient->uid, 'parameter' => 'surgeryRel']
                    )
                );?>
            </div>
        </div>

        <?php if ($latestData->termination_reason):?>
        <div class="row margin10">
            <div class="span4">
                <?=Yii::t("patient", "Причина окончания мониторинга")?>
            </div>
            <div class="span4">
                <?=ClinicalData::model()->getTerminationReason()[$latestData->termination_reason];?>
            </div>
        </div>
        <?php endif;?>

        <?php if ($latestData->termination_date):?>
        <div class="row margin10">
            <div class="span4">
                <?=Yii::t("patient", "Дата завершения")?>
            </div>
            <div class="span4">
                <?=$latestData->termination_date;?>
            </div>
        </div>
        <?php endif;?>

<?php endif;?>

<?php if ($records):?>

<div class="rh_table_grid">
    <table class="items margin30">
        <tr>
            <th><?=Yii::t("patient", "Дата") ?></th>
            <th><?= Yii::t('patient', 'Исс-ль')?></th>
            <th><?=Yii::t("patient", "Действия") ?></th>
        </tr>
        <?php foreach ($records as $row):?>
            <tr style="text-align: center;">
                <td class="date_create_cell"><?=$row->date_create->format()?> </td>
                <td><?=$row->user->getDisplayName(); ?></td>
                <td>
                    <?=CHtml::link(Yii::t("patient", "Смотреть"), $this->createUrl($this->id . "/index", array("id" => $row->patient_uid, "recordId" => $row->uid)))?>
            <?php if(Yii::app()->user->checkAccess("editClinicalData")):?>
                    | <?=CHtml::link(Yii::t("patient", "Редактировать"), $this->createUrl($this->id . "/edit", array("id" => $row->patient_uid, "recordId" => $row->uid)))?>
                    | <?=CHtml::link(Yii::t("patient", "Удалить"), $this->createUrl($this->id . "/delete", array("id" => $row->patient_uid, "recordId" => $row->uid)), array("class" => "deleteLink"))?>
            <?php endif;?>
                </td>
            </tr>
        <?php endforeach;?>
    </table>
</div>
<?php endif;