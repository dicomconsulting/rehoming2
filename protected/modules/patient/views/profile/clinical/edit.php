<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.11.13
 * Time: 11:05
 */
/* @var $this ClinicalController */
/* @var $model ClinicalData */
/* @var $form CActiveForm */
/* @var $pharmMeasureUnits array */

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->assetManager->publish(
        'js/mustache.js'
    ),
    CClientScript::POS_END
);

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->assetManager->publish(
        Yii::getPathOfAlias('patient.assets') . '/js/profile/clinical.js'
    ),
    CClientScript::POS_END
);

Yii::app()->mustache->templatePathAlias = "patient.views.profile.clinical";

?>
<script type="text/javascript">
    var fieldsPrefix = "ClinicalData_";
    var surgeryTemplate = <?=CJSON::encode(Yii::app()->mustache->getTemplate("surgery")); ?>;
    var etiologyTemplate = <?=CJSON::encode(Yii::app()->mustache->getTemplate("etiology")); ?>;
    var icd10MainTemplate = <?=CJSON::encode(Yii::app()->mustache->getTemplate("main_disease")); ?>;
    var pharmProductTemplate = <?=CJSON::encode(Yii::app()->mustache->getTemplate("pharmProduct")); ?>;
</script>

<?php
$this->widget(
    'directory.widgets.DiseaseSelector',
    ['patient' => $this->patient]
);

$this->widget(
    'directory.widgets.PharmSelector',
    ['patient' => $this->patient]
);
?>

<div class="form">

    <?php
    $form = $this->beginWidget(
        'CActiveForm',
        array(
            'id' => 'patient-clinical-data-edit-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => true,
            'clientOptions' => array("validateOnSubmit" => true, "afterValidate" => "js:afterValidate")
        )
    );
    ?>

    <h3><?= Yii::t('patient', "Клинические данные"); ?></h3>

    <?php echo $form->errorSummary($model); ?>

    <div class="row margin20">
        <div class="span8">
            <div>
                <?php echo $form->labelEx($model, 'date_create'); ?>
                <?php echo CHtml::activeDateField(
                    $model,
                    "date_create[date]",
                    ['value' => $model->date_create->format("Y-m-d")]
                ); ?>
                <?php echo CHtml::activeTimeField(
                    $model,
                    "date_create[time]",
                    array("class" => "input-small", 'value' => $model->date_create->format("H:i"))
                ); ?>
                <?php echo $form->error($model, 'date_create'); ?>
            </div>

            <div class="margin10">
                <?php echo $form->labelEx($model, 'height'); ?>
                <?php echo $form->numberField($model, 'height'); ?>
                <?php echo $form->error($model, 'height'); ?>
            </div>

            <div class="margin10">
                <?php echo $form->labelEx($model, 'weight'); ?>
                <?php echo $form->textField($model, 'weight', ['class' => 'decimal_field']); ?>
                <?php echo $form->error($model, 'weight'); ?>
            </div>


            <div class="margin10">
                <?php echo $form->labelEx($model, 'nyha_class'); ?>
                <?php echo $form->dropDownList(
                    $model,
                    'nyha_class',
                    CHtml::listData(ClinicalData::model()->getNyhaOptions(), "uid", "name"),
                    array("class" => "input-small")
                ); ?>
                <?php echo $form->error($model, 'nyha_class'); ?>
            </div>
            <div class="margin20">
                <strong><?= Yii::t("patient", "Клинический диагноз"); ?></strong>
                <br/>

                <div
                    id="main_container">
                    <?php if($model->icd10_main) {
                        foreach($model->icd10_main as $icd10) {
                            /** @var Icd10 $icd10 */
                            Yii::app()->mustache->render("main_disease", array(
                                    "id" => $icd10->uid,
                                    "name" => $icd10->name,
                                ));
                        };
                    };?></div>
                <?=
                CHtml::link(
                    Yii::t("patient", "Выбрать диагноз из справочника"),
                    '#',
                    array('onclick' => 'DiseaseSelector.getInstance().show(processMainDesease);return false;', 'class' => 'noprint')
                );?>
            </div>
            <div class="margin20">
                <strong><?= Yii::t("patient", "Сопутствующие хронические заболевания"); ?></strong>

                <div id="comorbidities_container">
                    <?php foreach ($model->icd10 as $icd10): ?>
                        <div><input type='hidden' class='custom_etiology' name='ClinicalData[icd10][]'
                                    value='<?= $icd10->uid ?>'><?= $icd10->name ?> <a href='#'
                                                                                      class='remove_etiology_row'>удалить</a>
                        </div>
                    <?php endforeach; ?></div>
                <?=
                CHtml::link(
                    Yii::t("patient", "Выбрать диагноз из справочника"),
                    '#',
                    array('onclick' => 'DiseaseSelector.getInstance().show(processComorbidities);return false;', 'class' => 'noprint')
                );?>
            </div>

            <script>
                var pharmUnits = <?=CJSON::encode($pharmMeasureUnits); ?>;
            </script>

            <div class="row margin10">
                <div class="span10">
                    <div id="otherPharmContainer">
                        <strong><?= Yii::t("patient", "Антиаритмическая терапия") ?>:</strong>
                        <?php

                        foreach ($model->therapy as $pharmRelObject) {
                            $pharmMeasureUnitsForTempl = $this->preparePharmUnits(
                                $pharmMeasureUnits,
                                $pharmRelObject->measure
                            );

                            echo Yii::app()->mustache->render(
                                "pharmProduct",
                                array(
                                    "pharmName" => $pharmRelObject->pharmProduct->name,
                                    "pharmId" => $pharmRelObject->product_uid,
                                    "dose" => $pharmRelObject->dose,
                                    "measure" => $pharmMeasureUnitsForTempl
                                )
                            );
                        }
                        ?>
                    </div>
                    <?=
                    CHtml::link(
                        Yii::t("patient", "Выбрать  препарат из справочника"),
                        '#',
                        array('onclick' => 'PharmSelector.getInstance().show(processPharmSelected);return false;',
                            "class" => "noprint")
                    );?>
                </div>
            </div>

            <div class="margin10">
                <strong><?php echo Yii::t('ProtocolsModule.protocols', "Операции на сердце") ?></strong>
            </div>
            <div>
                <?php
                $surgeryList = Surgery::model()->withI18n("ru")->findAll();
                echo CHtml::dropDownList(
                    "surgeryTypeSelector",
                    0,
                    array(0 => Yii::t("patient", "выберите операцию")) + CHtml::listData($surgeryList, 'uid', 'name')
                ); ?> <a href="#" id="add_surgery"><?= Yii::t("patient", "Добавить") ?></a>
            </div>

            <div id="surgeryContainer">
                <?php if ($model->surgeryRel): ?>
                    <?php foreach ($model->surgeryRel as $surgRel): ?>
                        <?php
                        foreach ($surgeryList as $surgery) {
                            /* @var Surgery $surgery */
                            if ($surgRel->surgery_uid == $surgery->uid) {
                                $rand = uniqid();
                                Yii::app()->mustache->render(
                                    "surgery",
                                    array(
                                        "surgeryName" => $surgRel->surgery->name,
                                        "surgeryId" => $surgRel->surgery_uid,
                                        "rnd" => $rand,
                                        "date" => $surgRel->date,
                                    )
                                );
                            }
                        };?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>


            <div class="margin20">
                <?php echo $form->labelEx($model, 'termination_reason'); ?>
                <?php echo $form->dropDownList(
                    $model,
                    'termination_reason',
                    ClinicalData::model()->getTerminationReason()
                ); ?>
                <?php echo $form->error($model, 'termination_reason'); ?>
            </div>

            <div>
                <?php echo $form->labelEx($model, 'termination_date'); ?>
                <?php echo $form->dateField($model, 'termination_date'); ?>
                <?php echo $form->error($model, 'termination_date'); ?>
            </div>

            <div class="buttons margin30 noprint">
                <?php echo CHtml::submitButton(Yii::t("patient", "Сохранить")); ?>
                <?php echo CHtml::button(
                    Yii::t("patient", "Отменить"),
                    array(
                        "onclick" => 'window.location.href="' . $this->createUrl(
                                $this->id . "/index",
                                array("id" => $this->patient->uid)
                            ) . '"'
                    )
                ); ?>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->
