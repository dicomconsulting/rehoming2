<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.11.13
 * Time: 11:07
 *
 * @var ClinicalController $this
 * @var Patient $patient
 * @var string $parameter
 *
 */

Yii::app()->clientScript->registerCssFile('/css/grid/style.css');

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->assetManager->publish(
        Yii::getPathOfAlias('patient.assets').'/js/profile.js'
    ),
    CClientScript::POS_END
);

?>

<div class="row">
    <div class="span8">
        <h3><?=Yii::t("patient", "История изменения данных по параметру")?>
            &laquo;<?=ClinicalData::model()->getParameterName($parameter)?>&raquo;</h3>
    </div>
</div>
<?php $this->widget("HistoryWidget", ['patient' => $patient, 'parameter' => $parameter])?>