<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 24.10.13
 * Time: 10:26
 *
 * @var $patient Patient
 *
 */
$isAllowedViewPersonalData = Yii::app()->user->checkAccess('viewPersonalData');
?>

<div class="margin30">
    <table class="table table-condensed table-bordered rh-main-table">
        <col width="30%" valign="middle">
        <col width="70%" valign="middle">
        <thead>
        <tr>
            <th colspan="2">
                <?= Yii::t('PatientModule.patient', 'Общие настройки'); ?>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <?= Yii::t('PatientModule.patient', 'ID пациента'); ?>
            </td>
            <td>
                <?= $patient->human_readable_id; ?>
            </td>
        </tr>
        <tr>
            <td>
                <?= Yii::t('PatientModule.patient', 'Группа пациента'); ?>
            </td>
            <td>
                <?= $patient->group->name; ?>
            </td>
        </tr>
        <tr>
            <td>
                <?= Yii::t('PatientModule.patient', 'Серийный номер устройства'); ?>
            </td>
            <td>
                <?= $patient->device->serial_number; ?>
            </td>
        </tr>
        <tr>
            <td>
                <?= Yii::t('PatientModule.patient', 'Модель устройства'); ?>
            </td>
            <td>
                <?= $patient->device->device_model; ?>
            </td>
        </tr>
        <tr>
            <td>
                <?= Yii::t('PatientModule.device', 'Дата имплантации'); ?>
            </td>
            <?php if ($patient->device->implantation_date instanceof RhDateTime) { ?>
                <td>
                    <?php $date = $patient->device->implantation_date; ?>
                    <?= $date->format('d'); ?>
                    <?= $date->getMonthName(); ?>
                    <?= $date->format('Y'); ?>
                </td>
            <?php } else { ?>
                <td>---</td>
            <?php } ?>
        </tr>
        <tr>
            <td>
                <?= Yii::t('PatientModule.patient', 'Комментарий'); ?>
            </td>
            <td>
                <?= $patient->comment ?: '---'; ?>
            </td>
        </tr>
        <?php if ($patient->device->active_since_date instanceof RhDateTime) { ?>
            <tr>
                <td>
                    <?= Yii::t('PatientModule.patient', 'Состояние мониторинга'); ?>
                </td>
                <td>
                    <?= Yii::t('PatientModule.patient', 'Активен с'); ?>
                    <?= $patient->device->active_since_date->format('d'); ?>
                    <?= $patient->device->active_since_date->getMonthName(); ?>
                    <?= $patient->device->active_since_date->format('Y'); ?>
                </td>
            </tr>
        <?php } ?>
        <tr>
            <td>
                <?= Yii::t('PatientModule.patient', 'Трансмиттер № (последнее сообщение)'); ?>
            </td>
            <td>
                <?= $patient->device->transmitter_serial_number; ?>
                <?php if ($patient->existDeviceData() && $patient->device->lastState->create_time instanceof RhDateTime) { ?>
                    (<?= $patient->device->lastState->create_time->format('d'); ?>
                    <?= $patient->device->lastState->create_time->getMonthName(); ?>
                    <?= $patient->device->lastState->create_time->format('Y'); ?>
                    <?= $patient->device->lastState->create_time->format('H:i:s'); ?>)
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td>
                <?= Yii::t('PatientModule.patient', 'Тип трансмиттера'); ?>
            </td>
            <td>
                <?= $patient->device->transmitter_type; ?>
            </td>
        </tr>
        </tbody>
    </table>

    <?php
    if ($isAllowedViewPersonalData) {
        if (Yii::app()->getModule('patient')->depersonalization) {
            echo $patient->getProfileIframe();
        } else { ?>
            <?php if ($patient->consent) { ?>
                <table class="table table-condensed table-bordered rh-main-table">
                    <col width="30%" valign="middle">
                    <col width="70%" valign="middle">
                    <thead>
                    <tr>
                        <th colspan="2">
                            <?= Yii::t('PatientModule.patient', 'Персональные настройки'); ?>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <?= Yii::t('PatientModule.patient', 'Имя'); ?>
                        </td>
                        <td>
                            <?= $patient->name; ?>
                            <?= $patient->middle_name; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= Yii::t('PatientModule.patient', 'Фамилия'); ?>
                        </td>
                        <td>
                            <?= $patient->surname; ?>
                        </td>
                    </tr>
                    <?php if ($patient->birth_date instanceof RhDateTime) { ?>
                        <tr>
                            <td>
                                <?= Yii::t('PatientModule.patient', 'Дата рождения'); ?>
                            </td>
                            <td>
                                <?php $date = $patient->birth_date; ?>
                                <?= $date->format('d'); ?>
                                <?= $date->getMonthName(); ?>
                                <?= $date->format('Y'); ?>
                            </td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td>
                            <?= Yii::t('PatientModule.patient', 'Пол'); ?>
                        </td>
                        <td>
                            <?php if ($patient->sex == Patient::FEMALE) { ?>
                                <?= Yii::t('PatientModule.patient', 'женщина'); ?>
                            <?php } elseif ($patient->sex == Patient::MALE) { ?>
                                <?= Yii::t('PatientModule.patient', 'мужчина'); ?>
                            <?php } else { ?>
                                <?= Yii::t('PatientModule.patient', 'не указан'); ?>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php $numberNum = 0; ?>
                    <?php foreach ($patient->phones as $phone) { ?>
                        <tr>
                            <td>
                                <?= (++$numberNum == 1) ? Yii::t('PatientModule.patient', 'Контактная информация') : '&nbsp'; ?>
                            </td>
                            <td>
                                <?= $phone->number; ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            <?php } ?>
            <?php if ($patient->contact_persons || $patient->family_doctors) { ?>

                <table class="table table-condensed table-bordered rh-main-table">
                    <col width="30%" valign="middle">
                    <col width="70%" valign="middle">
                    <thead>
                    <tr>
                        <th colspan="2">
                            <?= Yii::t('PatientModule.patient', 'Дополнительные данные о пациенте'); ?>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $contactPersonNum = 0; ?>
                    <?php foreach ($patient->contact_persons as $person): ?>
                        <tr>
                            <td>
                                <?= (++$contactPersonNum == 1) ? Yii::t('PatientModule.patient', 'Контактное лицо') : '&nbsp'; ?>
                            <td>
                                <?= Yii::t('PatientModule.patient', 'Имя'); ?>:
                                <?= $person->name ?><br/>
                                <?= Yii::t('PatientModule.patient', 'Телефон'); ?>:
                                <?= $person->phone->number; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php if ($contactPersonNum == 0) { ?>
                        <tr>
                            <td>
                                <?= Yii::t('PatientModule.patient', 'Контактное лицо'); ?>
                            </td>
                            <td>---</td>
                        </tr>
                    <?php } ?>
                    <?php $familyDoctorNum = 0; ?>
                    <?php foreach ($patient->family_doctors as $doctor): ?>
                        <tr>
                            <td>
                                <?= (++$familyDoctorNum == 1) ? Yii::t('PatientModule.patient', 'Семейный доктор') : '&nbsp'; ?>
                            </td>
                            <td>
                                <?= $doctor->name ?>, <?= $doctor->phone->number; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php if ($familyDoctorNum == 0) { ?>
                        <tr>
                            <td>
                                <?= Yii::t('PatientModule.patient', 'Семейный доктор'); ?>
                            </td>
                            <td>---</td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            <?php } ?>
            <?php
        }
    }
    ?>
</div>