<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.11.13
 * Time: 11:07
 *
 * @var EkgController $this
 * @var Patient $patient
 * @var PatientEkg $latestData
 * @var PatientEkg[] $records
 * @var bool $latest
 *
 */

Yii::app()->clientScript->registerCssFile('/css/grid/style.css');

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->assetManager->publish(
        Yii::getPathOfAlias('patient.assets').'/js/profile.js'
    ),
    CClientScript::POS_END
);

?>

<?php if(Yii::app()->user->checkAccess("editEkg")):?>
<p class="margin20">
<?=CHtml::link(Yii::t("PatientModule.patient", "Добавить данные ЭКГ"), $this->createUrl(
    $this->id . "/edit",
    array("id" => $patient->uid)
));
?></p>
<?php endif;?>

<?php if ($latestData):?>
    <div class="row">
        <div class="span8">
            <?php if ($latest):?>
            <h3><?=Yii::t("PatientModule.patient", "Последние данные")?></h3>
            <?php else:?>
            <h3><?=Yii::t("PatientModule.patient", "Данные на дату")?></h3>
            <?php endif;?>
        </div>
    </div>
    <div class="row">
        <div class="span5">
            <?=Yii::t("PatientModule.patient", "Дата")?>
        </div>
        <div class="span3">
            <?=$latestData->date_create->format()?>
        </div>
    </div>

    <?php foreach ($this->getFieldsToShow() as $field):?>

    <div class="row">
        <div class="span5">
            <?=Yii::t("PatientModule.patient", $latestData->getAttributeLabel($field))?>
        </div>
        <div class="span3">
            <?=$latestData->$field?>
        </div>
    </div>
    <?php endforeach;?>
<?php endif;?>

<?php if ($records):?>

<div class="rh_table_grid">
    <table class="items margin30">
        <tr>
            <th><?=Yii::t("PatientModule.patient", "Дата") ?></th>
            <th><?= Yii::t('PatientModule.patient', 'Исс-ль')?></th>
            <th><?=Yii::t("PatientModule.patient", "Действия") ?></th>
        </tr>
        <?php foreach ($records as $row):?>
            <tr style="text-align: center;">
                <td class="date_create_cell"><?=$row->date_create->format()?> </td>
                <td><?=$row->user->getDisplayName(); ?></td>
                <td>
                    <?=CHtml::link(Yii::t("PatientModule.patient", "Смотреть"), $this->createUrl($this->id . "/index", array("id" => $row->patient_uid, "recordId" => $row->uid)))?>
                    <?php if(Yii::app()->user->checkAccess("editEkg")):?>
                        | <?=CHtml::link(Yii::t("PatientModule.patient", "Редактировать"), $this->createUrl($this->id . "/edit", array("id" => $row->patient_uid, "recordId" => $row->uid)))?>
                        | <?=CHtml::link(Yii::t("PatientModule.patient", "Удалить"), $this->createUrl($this->id . "/delete", array("id" => $row->patient_uid, "recordId" => $row->uid)), array("class" => "deleteLink"))?>
                    <?php endif;?>
                </td>
            </tr>
        <?php endforeach;?>
    </table>
</div>
<?php endif;