<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.11.13
 * Time: 11:05
 */
/* @var $this EkgController */
/* @var $model PatientEkg */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'patient-ekg-edit-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,    )
);
    ?>

    <h2><?=Yii::t('patient', "Данные эхокардиографии");?></h2>

    <?php echo $form->errorSummary($model); ?>

    <div class="row margin20">
        <div class="span8">
            <div>
                <?php echo $form->labelEx($model, 'date_create'); ?>
                <?php echo CHtml::activeDateField(
                    $model,
                    "date_create[date]",
                    ['value' => $model->date_create->format("Y-m-d")]
                ); ?>
                <?php echo CHtml::activeTimeField(
                    $model,
                    "date_create[time]",
                    array("class" => "input-small", 'value' => $model->date_create->format("H:i"))
                ); ?>
                <?php echo $form->error($model, 'date_create'); ?>
            </div>

        <div>
            <?php echo $form->labelEx($model, 'ef_lv'); ?>
            <?php echo $form->numberField($model, 'ef_lv'); ?>
            <?php echo $form->error($model, 'ef_lv'); ?>
        </div>

        <div>
            <?php echo $form->labelEx($model, 'edv'); ?>
            <?php echo $form->textField($model, 'edv', ['class' => 'decimal_field']); ?>
            <?php echo $form->error($model, 'edv'); ?>
        </div>

        <div>
            <?php echo $form->labelEx($model, 'csr'); ?>
            <?php echo $form->textField($model, 'csr', ['class' => 'decimal_field']); ?>
            <?php echo $form->error($model, 'csr'); ?>
        </div>

        <div>
            <?php echo $form->labelEx($model, 'regurgitation_degree'); ?>
            <?php echo $form->textField($model, 'regurgitation_degree'); ?>
            <?php echo $form->error($model, 'regurgitation_degree'); ?>
        </div>

        <div>
            <?php echo $form->labelEx($model, 'vti'); ?>
            <?php echo $form->textField($model, 'vti'); ?>
            <?php echo $form->error($model, 'vti'); ?>
        </div>

        <div>
            <?php echo $form->labelEx($model, 'ivmd_intra_spont'); ?>
            <?php echo $form->numberField($model, 'ivmd_intra_spont'); ?>
            <?php echo $form->error($model, 'ivmd_intra_spont'); ?>
        </div>

        <div>
            <?php echo $form->labelEx($model, 'ivmd_intra_bivent'); ?>
            <?php echo $form->numberField($model, 'ivmd_intra_bivent'); ?>
            <?php echo $form->error($model, 'ivmd_intra_bivent'); ?>
        </div>

        <div>
            <?php echo $form->labelEx($model, 'ivmd_inter_spont'); ?>
            <?php echo $form->numberField($model, 'ivmd_inter_spont'); ?>
            <?php echo $form->error($model, 'ivmd_inter_spont'); ?>
        </div>

        <div>
            <?php echo $form->labelEx($model, 'ivmd_inter_bivent'); ?>
            <?php echo $form->numberField($model, 'ivmd_inter_bivent'); ?>
            <?php echo $form->error($model, 'ivmd_inter_bivent'); ?>
        </div>

        <div>
            <?php echo $form->labelEx($model, 'rv_pressure'); ?>
            <?php echo $form->numberField($model, 'rv_pressure'); ?>
            <?php echo $form->error($model, 'rv_pressure'); ?>
        </div>

        <div>
            <?php echo $form->labelEx($model, 'electric_delay'); ?>
            <?php echo $form->numberField($model, 'electric_delay'); ?>
            <?php echo $form->error($model, 'electric_delay'); ?>
        </div>

    <div class="buttons margin30 noprint">
            <?php echo CHtml::submitButton(Yii::t('ProtocolsModule.protocols', "Сохранить")); ?>
            <?php echo CHtml::button(
    Yii::t('ProtocolsModule.protocols', "Отменить"),
    array("onclick" => 'window.location.href="' . $this->createUrl(
        $this->id . "/index",
        array("id" => $this->patient->uid)
    ). '"'
    )
); ?>
    </div>
    </div>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->
