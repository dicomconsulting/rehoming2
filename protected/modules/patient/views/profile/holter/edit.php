<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.11.13
 * Time: 11:05
 */
/* @var $this HolterController */
/* @var $model PatientHolter */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'patient-holter-edit-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,    )
);
    ?>

    <h2><?=Yii::t('patient', "Данные Холтер");?></h2>

    <?php echo $form->errorSummary($model); ?>

    <div class="row margin20">
        <div class="span8">
            <div>
                <?php echo $form->labelEx($model, 'date_create'); ?>
                <?php echo CHtml::activeDateField(
                    $model,
                    "date_create[date]",
                    ['value' => $model->date_create->format("Y-m-d")]
                ); ?>
                <?php echo CHtml::activeTimeField(
                    $model,
                    "date_create[time]",
                    array("class" => "input-small", 'value' => $model->date_create->format("H:i"))
                ); ?>
                <?php echo $form->error($model, 'date_create'); ?>
            </div>

    <?php foreach ($this->getFieldsToShow() as $field): ?>

        <div>
            <?php echo $form->labelEx($model, $field); ?>
            <?php echo $form->numberField($model, $field); ?>
            <?php echo $form->error($model, $field); ?>
        </div>

    <?php endforeach;?>

    <div class="buttons margin30 noprint">
            <?php echo CHtml::submitButton(Yii::t('ProtocolsModule.protocols', "Сохранить")); ?>
            <?php echo CHtml::button(
    Yii::t('ProtocolsModule.protocols', "Отменить"),
    array("onclick" => 'window.location.href="' . $this->createUrl(
        $this->id . "/index",
        array("id" => $this->patient->uid)
    ). '"'
    )
); ?>
    </div>
    </div>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->
