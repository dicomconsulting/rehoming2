<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 24.10.13
 * Time: 10:26
 *
 * @var $patient Patient
 * @var $ps integer
 * @var $dataProvider CDataProvider
 *
 */
Yii::app()->clientScript->registerCssFile('/css/grid/style.css');

?>

<h2><?=Yii::t("PatientModule.patient", "Список записей")?></h2>

<?php $this->widget('LastMessagesWidget', array('patient' => $patient));?>

<div style="float:left;">
    <?=Yii::t("PatientModule.patient", "Выводить")?>:
    <?php if ($ps == 10):?>10<?php else:?><?=CHtml::link(10, array("/patient/recording/list", "ps" => 10, "id" => $patient->uid))?><?php endif;?>
    <?php if ($ps == 20):?>20<?php else:?><?=CHtml::link(20, array("/patient/recording/list", "ps" => 20, "id" => $patient->uid))?><?php endif;?>
    <?php if ($ps == 50):?>50<?php else:?><?=CHtml::link(50, array("/patient/recording/list", "ps" => 50, "id" => $patient->uid))?><?php endif;?>
</div>

<?php $this->widget('RecordingListWidget', ['patient' => $patient, 'dataProvider' => $dataProvider]);
