<?/*@var $patient Patient*/
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$lvLeadExist = isset($parameters['lv_pulse_amplitude'])
    || isset($parameters['lv_pulse_width'])
    || isset($parameters['lv_pacing_polarity'])
    || isset($parameters['lv_sensing_polarity']);
?>
<?php $this->widget('LastMessagesWidget', array('patient' => $patient));?>
<?php $this->widget('DeviceSettingsInfoWidget', array('patient' => $patient));?>
<div class = "span5 left-column">
    <?php $this->widget('DeviceRaLeadWidget', array('patient' => $patient));?>
    <?php if($lvLeadExist) {
        $this->widget('DeviceRvLeadWidget', array('patient' => $patient));
    } ?>
</div>
<div class = "span5 right-column">
     <?php if($lvLeadExist) {
        $this->widget('DeviceLvLeadWidget', array('patient' => $patient));
     } else {
         $this->widget('DeviceRvLeadWidget', array('patient' => $patient));
     } ?>
</div>

<div class="clearfix"></div>
<div class="right noprint" style="text-align: right;">
    <i class="icon-print"></i> <?=CHtml::link(Yii::t("PatientModule.patient", "Печать"), $this->createUrl($this->id . "/print", array("id" => $patient->uid, "action" => $this->action->id)), array("target" => "_blank"))?>
</div>
