<?php
/*@var $patient Patient*/
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
$crtExist = isset($parameters['ven_pacing'])
    || isset($parameters['initially_paced_chamber'])
    || isset($parameters['vv_delay_after_vp'])
    || isset($parameters['triggering'])
    || isset($parameters['maximum_trigger_rate'])
    || isset($parameters['lv_t_wave_protection']);
?>
<?php $this->widget('LastMessagesWidget', array('patient' => $patient));?>

<?php $this->widget('DeviceSettingsInfoWidget', array('patient' => $patient));?>
<div class = "span5 left-column">
    <?php $this->widget('DeviceBradycardiaBasicsWidget', array('patient' => $patient));?>
    <?php if($crtExist) {
        $this->widget('DeviceBradycardiaAvWidget', array('patient' => $patient));
    } ?>
</div>
<div class = "span5 right-column">
     <?php if($crtExist) {
        $this->widget('DeviceBradycardiaCrtWidget', array('patient' => $patient));
     } else {
         $this->widget('DeviceBradycardiaAvWidget', array('patient' => $patient));
     } ?>
</div>

<div class="clearfix"></div>
<div class="right noprint" style="text-align: right;">
    <i class="icon-print"></i> <?=CHtml::link(Yii::t("PatientModule.patient", "Печать"), $this->createUrl($this->id . "/print", array("id" => $patient->uid, "action" => $this->action->id)), array("target" => "_blank"))?>
</div>
