<?php
/*@var $patient Patient*/
/* @var $parameters DeviceParameter[] */
$parameters = $patient->device->model->parameters;
?>
<?php $this->widget('LastMessagesWidget', array('patient' => $patient));?>
<?php $this->widget('DeviceSettingsInfoWidget', array('patient' => $patient));?>
<div class = "span10 left-column">
    <?php $this->widget('DeviceOverviewGeneralWidget', array('patient' => $patient));?>
</div>
<div class = "span10 left-column">
    <?php $this->widget('DeviceTachyWidget', array('patient' => $patient));?>
</div>
<div class = "span10 left-column">
    <?php $this->widget('DeviceBradyWidget', array('patient' => $patient));?>
</div>
<div class="clearfix"></div>
<div class="clearfix"></div>
<div class="right noprint" style="text-align: right;">
    <i class="icon-print"></i> <?=CHtml::link(Yii::t("PatientModule.patient", "Печать"), $this->createUrl($this->id . "/print", array("id" => $patient->uid, "action" => $this->action->id)), array("target" => "_blank"))?>
</div>
