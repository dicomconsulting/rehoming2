<?php
/**
 * @var Patient $patient
 */
?>
<?php $this->widget('LastMessagesWidget', array('patient' => $patient));?>

<?php $this->widget('DeviceSettingsInfoWidget', array('patient' => $patient));?>
<div class = "span5 left-column">
    <?php $this->widget('DeviceAtrialModeWidget', array('patient' => $patient));?>
</div>
<div class = "span5 right-column">
    <?php $this->widget('DeviceAtrialZoneWidget', array('patient' => $patient));?>
</div>
<div class="clearfix"></div>
<div class="right noprint" style="text-align: right;">
    <i class="icon-print"></i> <?=CHtml::link(Yii::t("PatientModule.patient", "Печать"), $this->createUrl($this->id . "/print", array("id" => $patient->uid, "action" => $this->action->id)), array("target" => "_blank"))?>
</div>
