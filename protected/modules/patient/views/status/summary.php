<?php
/**
 * @var Patient $patient
 */
?>
<?php $this->widget('LastMessagesWidget', array('patient' => $patient));?>
<?php $this->widget('QuickViewWidget', array('patient' => $patient));?>
<div class = "span6 left-column">
    <?php $this->widget('DeviceStatusWidget', array('patient' => $patient));?>
</div>
<div class = "span3 right-column">
</div>
<div class="clearfix"></div>
<div class = "span6 left-column">
    <?php $this->widget('TachySettingsWidget', array('patient' => $patient));?>
</div>
<div class = "span4 right-column">
    <?php $this->widget('BradySettingsWidget', array('patient' => $patient));?>
</div>
<div class="clearfix"></div>
<div class = "span6 left-column">
    <?php $this->widget('BradyLeadWidget', array('patient' => $patient));?>
</div>
<div class = "span4 right-column">
    <?php $this->widget('ShockLeadWidget', array('patient' => $patient));?>
</div>
<div class="clearfix"></div>
<div class = "span5 left-column">
    <?php $this->widget('VenArrWidget', array('patient' => $patient));?>
</div>
<div class = "span5 right-column">
    <?php $this->widget('EventEpisodesWidget', array('patient' => $patient));?>
</div>
<div class="clearfix"></div>
<div class = "span6 left-column">
    <?php $this->widget('AtrialArrWidget', array('patient' => $patient));?>
</div>
<div class = "span5 right-column"></div>
<div class="clearfix"></div>
<div class="right noprint" style="text-align: right;">
    <i class="icon-print"></i> <?=CHtml::link(Yii::t("PatientModule.patient", "Печать"), $this->createUrl($this->id . "/print", array("id" => $patient->uid, "action" => $this->action->id)), array("target" => "_blank"))?>
</div>
