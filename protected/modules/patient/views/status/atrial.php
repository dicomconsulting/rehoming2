<?php
/*@var $patient Patient*/
$graphManager = new GraphManager();
?>
<?php $this->widget('LastMessagesWidget', array('patient' => $patient));?>
<div class = "span10 left-column">
    <?php $this->widget('LastMessageInfoWidget', array('patient' => $patient));?>
</div>
<div class = "span10 left-column">
    <?php $this->widget('StatusAtrialMainWidget', ['patient' => $patient, 'graphManager' => $graphManager]);?>
    <?php $this->widget('StatusAtrialAddWidget', ['patient' => $patient, 'graphManager' => $graphManager]);?>
    <?php $this->widget('LastEpisodeWidget', array('patient' => $patient));?>
    <?php if ($graphManager->countGraphs()) { ?>
        <table class = "table table-condensed rh-main-table noprint">
        <tbody>
        <?php $graphManager->resetGraphs(); ?>
        <?php do { ?>
            <tr id = '<?=$graphManager->getAnchor();?>'>
                <th>
                    <?=$graphManager->getName();?>
                </th>
            </tr>
            <tr>
                <td colspan="5">
                    <?php $graphManager->renderGraph(); ?>
                </td>
            </tr>
        <?php } while ($graphManager->nextGraph() !== false); ?>
        </tbody>
        </table>
    <?php } ?>
</div>

<div class="clearfix"></div>
<div class="right noprint" style="text-align: right;">
    <i class="icon-print"></i> <?=CHtml::link(Yii::t("PatientModule.patient", "Печать"), $this->createUrl($this->id . "/print", array("id" => $patient->uid, "action" => $this->action->id)), array("target" => "_blank"))?>
</div>
