<?/*@var $patient Patient*/?>
<?php $this->widget('LastMessagesWidget', array('patient' => $patient));?>
<div class = "span10 left-column">
    <?php $this->widget('LastMessageInfoWidget', array('patient' => $patient));?>
</div>
<div class = "span10 left-column">
    <?php $this->widget('StatusPhysiologicWidget', array('patient' => $patient));?>
</div>
<div class="clearfix"></div>
<div class="right noprint" style="text-align: right;">
    <i class="icon-print"></i> <?=CHtml::link(Yii::t("PatientModule.patient", "Печать"), $this->createUrl($this->id . "/print", array("id" => $patient->uid, "action" => $this->action->id)), array("target" => "_blank"))?>
</div>
