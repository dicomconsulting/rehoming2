<?php
/* @var $this ListController */
/* @var $dataProvider CActiveDataProvider */
/* @var $patientModel Patient */
/* @var $ps integer */
/* @var $savedFilterId integer */
Yii::app()->clientScript->registerCssFile(Yii::app()->request->getBaseUrl(true) . '/css/grid/style.css');
Yii::app()->clientScript->registerScriptFile(
    Yii::app()->assetManager->publish(
        Yii::getPathOfAlias('patient.assets') . '/js/list.js'
    ),
    CClientScript::POS_END
);
Yii::app()->clientScript->registerScriptFile(
    Yii::app()->assetManager->publish(
        Yii::getPathOfAlias('admin.assets') . '/js/savedFilter.js'
    ),
    CClientScript::POS_END
);

ViewFragment::registerScriptOptions('savedFilterObj', 'SavedFilterClass',
    [
        'elementId'    => 'savedFilter',
        'filterValues' => SavedFilter::getFiltersUrls(
            SavedFilter::PLACEMENT_PATIENT,
            'Patient',
            '/patient/list/index/'
        ),
        'modalId'      => 'SavedFilterModal',
        'saveUrl'      => '/admin/savedFilter/saveSavedFilterParams',
        'deleteUrl'    => '/admin/savedFilter/deleteSavedFilter',
        'inputPrefix'  => 'Patient',
        'placement'    => 'patient',
        'urlPrefix'    => '/patient/list/index/',
        'getFilterUrl' => '/admin/savedFilter/getFilterUrl'

    ]
);
Yii::app()->getController()->widget(
    'Modal',
    [
        'id'      => 'SavedFilterModal',
        'header'  => Yii::t('AdminModule.messages', 'Сохраненный фильтр'),
        'content' => $this->renderFile(Yii::getPathOfAlias('application.modules.admin') . '/views/savedFilter/createNewFilter.php', [], true),
    ]
);
?>

<h2 class="noprint">
    <?= Yii::t("PatientModule.patient", "Список пациентов") ?><?php if ($unacknowledged): ?>, <?= Yii::t("PatientModule.patient", "требующих внимания") ?><?php endif; ?>
</h2>

<div class="row noprint">
    <div class="span10">
        <form class="form-search" style="margin: 0;">
            <span class="showPatientsFilter">
                <?= Yii::t('PatientModule.patient', 'Сохраненный фильтр') ?>
            </span>
            <?php
            echo CHtml::dropDownList(
                'savedFilter',
                $savedFilterId,
                SavedFilter::getFiltersList(SavedFilter::PLACEMENT_PATIENT),
                [
                    'onchange' => 'savedFilterObj.handleClickFilter()'
                ]);
            echo '&nbsp;&nbsp;&nbsp;';
            echo CHtml::button(Yii::t('PatientModule.patient', 'Сохранить'), [
                'onclick' => 'savedFilterObj.handleSaveFilter();'
            ]);
            echo '&nbsp;';
            echo CHtml::button(Yii::t('PatientModule.patient', 'Удалить'), [
                'onclick' => 'savedFilterObj.handleDeleteFilter();'
            ])
            ?>
        </form>
    </div>
</div>
<br>

<div class="row noprint">
    <div class="span8">
        <form class="form-search" style="margin: 0;">
            <span class="showPatientsFilter">
                <?= Yii::t('PatientModule.patient', 'Показывать пациентов') ?>
            </span>
            <?php echo CHtml::dropDownList(
                'AdditionalFilter[showPatientFilterValue]',
                $patientModel->showPatientFilterValue,
                [
                    '0' => Yii::t('PatientModule.patient', 'Все'),
                    '1' => Yii::t('PatientModule.patient', 'Активные'),
                    '2' => Yii::t('PatientModule.patient', 'Неактивные'),
                    '3' => Yii::t('PatientModule.patient', 'Включенные в исследование'),
                    '4' => Yii::t('PatientModule.patient', 'Не включенные в исследование')
                ],
                [
                    'onchange' => 'fixSearchPatientFilter(this.value);',
                    'id'       => 'dropDownList'
                ]); ?>
            <?= CHTML::button(Yii::t('PatientModule.patient', 'Сбросить фильтры'), [
                'onclick' => 'resetFilters();'
            ]) ?>
        </form>
    </div>
    <div class="span2" style="text-align: right">
        <?= Yii::t("PatientModule.patient", "Выводить") ?>:
        <?php if ($ps == 10): ?>10<?php else: ?><?= CHtml::link(10, array("/patient/list/" . $this->action->id, "ps" => 10)) ?><?php endif; ?>
        <?php if ($ps == 20): ?>20<?php else: ?><?= CHtml::link(20, array("/patient/list/" . $this->action->id, "ps" => 20)) ?><?php endif; ?>
        <?php if ($ps == 50): ?>50<?php else: ?><?= CHtml::link(50, array("/patient/list/" . $this->action->id, "ps" => 50)) ?><?php endif; ?>
    </div>
</div>

<?php
$this->widget('GridView', [
    'dataProvider' => $dataProvider,
    'htmlOptions'  => array('class' => 'rh_table_grid'),
    'ajaxUpdate'   => false,
    'filter'       => $patientModel,
    'columns'      => [
        [
            "header" => Yii::t("PatientModule.patient", "ID пациента"),
            "name"   => "human_readable_id",
            "type"   => "html",
            "filter" => '<div class="form-search" style="margin: 0;">
                        <div class="input-append">
                        <input type="text" class="span2 search-query" name="' . $dataProvider->id . '[human_readable_id]" style="width: 84px;" value="' . $dataProvider->model->human_readable_id . '">
                            <button type="submit" class="btn"><i class="icon-search"></i></button>
                        </div>
                        </div>',
            "value"  => function ($data, $row, $dataColumn) {
                /** @var $data Patient */
                if ($data->existDeviceData()) {
                    $urlParts = array("status/summary", "id" => $data->uid);
                } else {
                    $urlParts = array("profile/view/index", "id" => $data->uid);
                }
                if (Yii::app()->user->checkAccess('viewPersonalData') && (!Yii::app()->getModule('patient')->depersonalization)) {
                    $personalInfo = $data->surname . " " . $data->name . " " . $data->middle_name;
                } else {
                    $personalInfo = '';
                }
                return CHtml::link($data->human_readable_id, $urlParts) . "<br />" . $personalInfo;
            }
        ],
        [
            "header"      => Yii::t("PatientModule.patient", "Сообщение"),
            "type"        => "html",
            "htmlOptions" => array(
                "class" => "center-column",
                "style" => "padding:0;margin:0;width:auto;vertical-align:top;"
            ),
            "value"       => function ($data, $row, $dataColumn) {
                /** @var $data Patient */
                /** @var $latestStatusMessage Message */
                /** @var $dataColumn CDataColumn */
                $latestStatusMessage = $data->device->getLatestStatusMessage();
                if ($latestStatusMessage) {
                    $message = $latestStatusMessage->formatMessage(true);
                    if (!$latestStatusMessage->isAcknowledged()) {
                        return CHtml::openTag("strong") . $message . CHtml::closeTag("strong");
                    } else {
                        return $message;
                    }
                } else {
                    return "&nbsp;";
                }
            }
        ],
        [
            "header" => Yii::t("PatientModule.patient", "Устройство&nbsp;/ Сер.&nbsp;номер"),
            "filter" => '<div class="form-search" style="margin: 0;">
                        <div class="input-append">
                        <input type="text" class="span2 search-query" name="' . $dataProvider->id . '[device_search]" style="width: 84px;" value="' . $dataProvider->model->device_search . '">
                            <button type="submit" class="btn"><i class="icon-search"></i></button>
                        </div>
                        </div>',
            "type"   => "html",
            "name"   => "device_search",
            "value"  => function ($data, $row, $dataColumn) {
                /** @var $data Patient */
                return $data->device->device_model . "<br />" . $data->device->serial_number;
            }
        ],
        [
            "header" => Yii::t("PatientModule.device", "Дата имплантации"),
            "type"   => "raw",
            "name"   => "implantation_date_search",
            'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                "name"     => $dataProvider->id . "[implantation_date_search]",
                'language' => Yii::app()->language,
                "value"    => $dataProvider->model->implantation_date_search,
                'options'  => array(
                    'dateFormat'  => 'dd-mm-yy',
                    'changeMonth' => 'true',
                    'changeYear'  => 'true',
                ),
            ), true),
            "value"  => function ($data, $row, $dataColumn) {
                /** @var $data Patient */
                return $data->device->implantation_date;
            }
        ],
        [
            "header"            => Yii::t("PatientModule.patient", "Комментарий"),
            "name"              => "comment",
            "headerHtmlOptions" => array("style" => "width:200px;"),
            'filter'            => CHtml::textField($dataProvider->id . "[comment]", $dataProvider->model->comment),
            "value"             => function ($data, $row, $dataColumn) {
                /** @var $data Patient */
                return $data->comment;
            }
        ],
        [
            "header" => Yii::t("PatientModule.patient", "Группа пациента"),
            "name"   => 'group_id',
            "type"   => "raw",
            "filter" => CHtml::listData(PatientGroup::model()->findAll(), 'uid', 'name'),
            "value"  => function ($data, $row, $dataColumn) {
                /** @var $data Patient */
                return $data->group->name;
            }
        ],
        [
            "header" => Yii::t("PatientModule.device", "Последнее сообщение"),
            "type"   => "html",
            'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                "name"     => $dataProvider->id . "[lastMessageDateSearch]",
                'language' => Yii::app()->language,
                "value"    => $dataProvider->model->lastMessageDateSearch,
                'options'  => array(
                    'dateFormat'  => 'dd-mm-yy',
                    'changeMonth' => 'true',
                    'changeYear'  => 'true',
                ),
            ), true),
            "value"  => function ($data, $row, $dataColumn) {
                /** @var $data Patient */
                if ($data->device->lastState instanceof DeviceState) {
                    return $data->device->lastState->create_time->format('d')
                    . ' ' . $data->device->lastState->create_time->getMonthName()
                    . ' ' . $data->device->lastState->create_time->format('Y H:i');
                } else {
                    return "&nbsp;";
                }

            }
        ],
        [
            "header" => Yii::t("PatientModule.patient", "Последний просмотр"),
            "name"   => "last_view_time",
            'filter' => false,
            "type"   => "html",
            'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                "name"     => $dataProvider->id . "[last_view_time]",
                'language' => Yii::app()->language,
                "value"    => $dataProvider->model->last_view_time,
                'options'  => array(
                    'dateFormat'  => 'dd-mm-yy',
                    'changeMonth' => 'true',
                    'changeYear'  => 'true',
                ),
            ), true),
            "value"  => function ($data, $row, $dataColumn) {
                /** @var $data Patient */
                if ($data->last_view_time instanceof RhDateTime) {
                    return $data->last_view_time->format('d')
                    . ' ' . $data->last_view_time->getMonthName()
                    . ' ' . $data->last_view_time->format('Y H:i');
                } else {
                    return "&nbsp;";
                }
            }
        ],
    ],
]);
?>

<div class="right noprint" style="text-align: right;">
    <i class="icon-print"></i> <?= CHtml::link(Yii::t("PatientModule.patient", "Печать"), $this->createUrl($this->id . "/print", $_GET)) ?>
</div>
