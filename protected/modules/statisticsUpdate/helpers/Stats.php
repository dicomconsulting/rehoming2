<?php

/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 27.01.16
 * Time: 17:49
 */
class Stats
{

    /**
     * Выравнивание границ массива нулями, для стандартного отклонения
     *
     * @param $array
     * @param $cnt
     * @return array
     */
    public static function zeroAlign($array, $cnt)
    {
        $z = -999;
        if (count($array) < $cnt) {
            $addZerroCount = $cnt - count($array);
            // Добавляем нули
            for ($i = 0; $i < $addZerroCount; $i++) {
                $array[$z] = 0;
                $z--;
            }
        }
        return $array;
    }

    /**
     * Стандартное отклонение
     *
     * @param $aValues
     * @param bool|false $bSample
     * @return float
     */
    public static function standardDeviation($aValues, $bSample = true)
    {
        $totalCount = count($aValues);
        if (!$totalCount) {
            return null;
        }
        if ($bSample) {
            if ($totalCount == 1) {
                return null;
            }
        }

        $fMean     = array_sum($aValues) / $totalCount;
        $fVariance = 0.0;
        foreach ($aValues as $i) {
            $fVariance += pow($i - $fMean, 2);
        }
        $fVariance /= ($bSample ? $totalCount - 1 : $totalCount);
        return round(sqrt($fVariance), 2);
    }

    /**
     * Минимальное значение, либо null в случае отсутвия значений
     *
     * @param $values
     * @return mixed|null
     */
    public static function min($values)
    {
        if (count($values)) {
            return min($values);
        } else {
            return null;
        }
    }

    /**
     * Максимальное значение, либо null в случае отсутвия значений
     *
     * @param $values
     * @return mixed|null
     */
    public static function max($values)
    {
        if (count($values)) {
            return max($values);
        } else {
            return null;
        }
    }

    /**
     * Сумма значений либо null в случае отсутствия значений
     *
     * @param $values
     * @return float|int|null
     */
    public static function sum($values)
    {
        return count($values) ? array_sum($values) : null;
    }

    /**
     * Максимальное значение, либо null в случае отсутвия значений
     *
     * @param $values
     * @return mixed|null
     */
    public static function average($values)
    {
        if (count($values)) {
            return round(array_sum($values) / count($values), 2);
        } else {
            return null;
        }
    }

    /**
     * Количество совпадающих элементов в массиве
     *
     * @param $values
     * @return int
     */
    public static function equalValuesCount($values, $template)
    {
        $count = 0;
        foreach ($values as $value) {
            if ($value === $template) {
                $count++;
            }
        }
        return $count;
    }

    public static function smartCalcPeriodDays($startDate, $endDate)
    {
        $dateBegin  = new DateTime($startDate);
        $dateEnd    = new DateTime($endDate ? $endDate : 'now');
        $dateInterval = $dateEnd->diff($dateBegin);
        return $dateInterval->days;
    }

    public static function smartCalcPeriod($startDate, $endDate)
    {
        $dateBegin  = new DateTime($startDate);
        $dateEnd    = new DateTime($endDate ? $endDate : 'now');
        $totalMonth = 0;
        // Если в рамках одного месяца
        $startDay   = $dateBegin->format('d');
        $startMonth = $dateBegin->format('m');
        $startYear  = $dateBegin->format('Y');
        $endDay     = $dateEnd->format('d');
        $endMonth   = $dateEnd->format('m');
        $endYear    = $dateEnd->format('Y');

        if (($startMonth == $endMonth) && ($startYear == $endYear)) {
            if (($endDay - $startDay) > 15) {
                return 1;
            } else {
                return 0;
            }
        }
        if ($startDay <= 15) {
            $totalMonth++;
        }
        // Прибавляем месяц
        $dateInterval = new DateInterval('P1M');
        $dateBegin->add($dateInterval);
        // Устанавливаем перове число
        $monthBegin = $dateBegin->format('Y')*12 + $dateBegin->format('m');

        if ($endDay > 15) {
            $totalMonth++;
        }
        // Устанавливаем перове число
        $monthEnd = $dateEnd->format('Y')*12 + $dateEnd->format('m');

        $dateDiff = $monthEnd - $monthBegin;
        $totalMonth += $dateDiff;

        return $totalMonth;
    }


    /**
     * @param DeviceValue[] $vals
     * @return DeviceValue[]
     */
    public static function filterEmpty(array $vals)
    {
        $filtered = [];
        foreach ($vals as $value) {
            if ($value->value) {
                $filtered[] = $value;
            }
        }

        return $filtered;
    }

    /**
     * @param array $values
     * @return array
     */
    public static function getRawValues(array $values)
    {
        $rawValues = [];
        foreach ($values as $value) {
            $rawValues[] = $value->value;
        }
        return $rawValues;
    }
}