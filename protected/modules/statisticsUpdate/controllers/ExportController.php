<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.03.16
 * Time: 13:34
 */

class ExportController extends Controller
{
    public function accessRules()
    {
        return [
            [
                'allow',
                'roles' => ['viewStatistics']
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

    /**
     * Получить шаблон
     *
     * @return static
     * @throws CException
     */
    public function getTemplate()
    {
        $currentGroupId = Yii::app()->user->user_group_id;
        if ($currentGroupId) {
            $statisticsTemplateFile = StatisticsTemplateFile::model()->findByAttributes(['group_id' => $currentGroupId]);
            if (!$statisticsTemplateFile) {
                throw new CException(Yii::t('StatisticsModule.statistics', 'Не задан шаблон статистики'));
            }
            return $statisticsTemplateFile;
        } else {
            throw new CException(Yii::t('StatisticsModule.statistics', 'Неизвестный пользователь'));
        }
    }


    public function actionXls()
    {
        $patientsProvider = new PatientsProvider(array_key_exists('Filter', $_REQUEST) ? $_REQUEST['Filter'] : []);
        $patients         = $patientsProvider->getPatients();
        $template = $this->getTemplate();
        $xlsFile = new XlsOfficeFile();
        $xlsFile->loadTemplate($template);
        $xlsFile->applayData($patients);

        $fileName = substr($template->name, 0, strrpos($template->name, '.'));
        $newName =  date('Y-m-d').' '.$fileName.'.xls';

        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename='.$newName);
        $xlsFile->echoData();
    }
}