<?php

class ViewController extends Controller
{
    public function init()
    {
        $this->pageTitle = Yii::t("StatisticsModule.statistics", "Статистика");
        parent::init();

        //настройки вывода в pdf
        $pdfOptions                = $this->getPdfPageOptions();
        $pdfOptions['header-html'] = Yii::app()->request->getBaseUrl(true) . $this->createUrl(
                $this->id . "/printHeader"
            );
        $pdfOptions['footer-html'] = Yii::app()->request->getBaseUrl(true) . $this->createUrl(
                $this->id . "/printFooter"
            );
        $this->setPdfPageOptions($pdfOptions);
    }

    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => ['printHeader', 'printFooter'],
                'users'   => ['*'],
            ],
            [
                'allow',
                'roles' => ['viewStatistics']
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

    public function getStatisticsData()
    {
        $patientsProvider = new PatientsProvider(array_key_exists('Filter', $_REQUEST) ? $_REQUEST['Filter'] : []);
        $patients         = $patientsProvider->getPatients();

        $resultStatistics = [];
        if ($patients) {
            $statisticsProviders = [
                TotalPatientsStatistics::class,
                PatientSexStatistics::class,
                PatientAgeStatistics::class,
                ObservationPeriodStatistics::class,
                NyhaStatistics::class,
                RhythmDisturbancesStatistics::class,
                SeriousAdverseEvents::class,
                EtiologyStatistics::class,
                PreparationsStatistics::class,
                HeartSurgeryStatistics::class,
                EksStatistics::class,
                IkdCrtStatistics::class,
                ImplantationStimulatingStatistics::class,
                TrendMonitoringStatistics::class,
                AdverseEventsStatistics::class,
                AdverseImplantStatistics::class,
                ShockDischargesStatistics::class,
                HomeMonitoringStatistics::class,
                HomeMonitoringStatisticsPart2::class,
                FunctionRatingsStatistics::class,
                ImplantWithAdverseEventStatistics::class,
                TotalNumberVisitStatistics::class,
                VisitsNumberStatistics::class,
                ClinicLoadStatistics::class,
                DaysBeforeTherapyStatistics::class,
                VentricularArrhythmiasStatistics::class,
                CountershockStatistics::class,
            ];
            $resultStatistics    = [];
            foreach ($statisticsProviders as $classProviderName) {
                $providerName                    = lcfirst($classProviderName);
                $provider                        = new $classProviderName;
                $resultStatistics[$providerName] = $provider->getStatistics($patients);
            };
        }
        return $resultStatistics;
    }

    public function actionIndex()
    {
        $statisticsData = $this->getStatisticsData();
        $renderData     = $statisticsData;
        if (array_key_exists('Filter', $_REQUEST)) {
            $renderData['filter'] = $_REQUEST['Filter'];
        } else {
            $renderData['filter'] = [];
        }
        if (array_key_exists('savedFilterId', $_REQUEST)) {
            $renderData['savedFilterId'] = $_REQUEST['savedFilterId'];
        } else {
            $renderData['savedFilterId'] = 0;
        }

        $this->render('index', $renderData);
    }

    public function actionPrint()
    {
        $this->layout   = '//layouts/print';
        $statisticsData = $this->getStatisticsData();
        $renderData     = $statisticsData;
        if (array_key_exists('Filter', $_REQUEST)) {
            $renderData['filter'] = $_REQUEST['Filter'];
        } else {
            $renderData['filter'] = [];
        }
        if (array_key_exists('savedFilterId', $_REQUEST)) {
            $renderData['savedFilterId'] = $_REQUEST['savedFilterId'];
        } else {
            $renderData['savedFilterId'] = 0;
        }

        $this->renderPdf('index', $renderData);
    }

    public function behaviors()
    {
        return array(
            'pdfable' => array(
                'class'          => 'ext.pdfable.Pdfable',
                // Global default options for wkhtmltopdf
                'pdfOptions'     => array(
                    // Use binary path from module config
                    'bin'            => Yii::app()->getModule("pdfable")->bin,
                    'use-xserver',
                    'dpi'            => 600,
                    'margin-top'     => 19,
                    'margin-right'   => 5,
                    'margin-bottom'  => 15,
                    'margin-left'    => 10,
                    'header-spacing' => 3,
                    'footer-spacing' => 3,
                ),
                // Default page options
                'pdfPageOptions' => array(
                    'print-media-type',
                    'enable-javascript',
                    'javascript-delay' => 500,
                ),
            )
        );
    }

    public function actionPrintHeader(
        $page = 0,
        $title = "",
        $frompage = 0,
        $topage = 0,
        $doctitle = 0
    ) {

        $widgetName = "PrintHeader";
        echo $this->widget($widgetName, array("title" => $title), true);

        Yii::app()->end();
    }

    public function actionPrintFooter(
        $page = 0,
        $title = "",
        $frompage = 0,
        $topage = 0,
        $doctitle = 0
    ) {

        $widgetName = "PrintFooter";
        echo $this->widget($widgetName, array("title" => $title, "page" => $page, "pageTotal" => $topage), true);

        Yii::app()->end();
    }

    /**
     * Подготовить массив моделей для checkboxList
     *
     * @param $filter
     * @param $requestParameterName
     * @param $modelArray
     * @param $attributeKey
     * @param $attributeName
     * @return array
     */
    public function formCheckboxList($filter, $requestParameterName, $modelArray, $attributeKey, $attributeName)
    {
        uasort($modelArray, function ($a, $b) use ($filter, $requestParameterName, $attributeKey, $attributeName) {
            if (array_key_exists($requestParameterName, $filter) && in_array($a->$attributeKey, $filter[$requestParameterName])) {
                if (in_array($b->$attributeKey, $filter[$requestParameterName])) {
                    return $a->$attributeName > $b->$attributeName;
                } else {
                    return false;
                }
            } elseif (array_key_exists($requestParameterName, $filter) && in_array($b->$attributeKey, $filter[$requestParameterName])) {
                if (in_array($a->$attributeKey, $filter[$requestParameterName])) {
                    return $a->$attributeName > $b->$attributeName;
                } else {
                    return true;
                }
            } else {
                return $a->$attributeName > $b->$attributeName;
            }
        });
        $resultArray = CHtml::listData($modelArray, $attributeKey, function ($obj) use ($attributeName) {
            return $obj->$attributeName;
        });
        return $resultArray;
    }

}
