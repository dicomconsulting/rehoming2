<?php

class IkdXlsSource extends BaseXlsSource implements XlsSourceInterface
{
    public function getVarPrefix()
    {
        return 'Ikd';
    }

    public function getVarNames()
    {
        return [
            'Code'              => PatientXlsParameter::class,
            'Sex'               => PatientXlsParameter::class,
            'Age'               => PatientXlsParameter::class,
            'ImplantDate'       => PatientXlsParameter::class,
            'DeviceName'        => PatientXlsParameter::class,
            'ObservationPeriod' => PatientXlsParameter::class,
            'Nyha'              => PatientXlsParameter::class,
            'IsDead'            => PatientXlsParameter::class,
        ];
    }

    /**
     * Фильтруем пациентов по признаку наличия устройств Ikd;
     *
     * @param Patient[] $patients
     * @return mixed
     */
    public function filterPatients($patients)
    {
        $filteredPatients = [];
        foreach ($patients as $patient) {
            if ($patient->device->model->type->alias == 'icd') {
                $filteredPatients[$patient->uid] = $patient;
            }
        }

        return $filteredPatients;
    }

    public function getVarsValues($patients)
    {
        $filteredPatients = $this->filterPatients($patients);
        return $this->baseFormVarsValues($filteredPatients);
    }
}
