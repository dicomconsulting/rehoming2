<?php

class PatientXlsSource extends BaseXlsSource implements XlsSourceInterface
{

    public function getVarPrefix()
    {
        return 'P';
    }

    public function getVarNames()
    {
        return [
            'Id'                               => PatientXlsParameter::class,
            'Code'                             => PatientXlsParameter::class,
            'Nyha'                             => PatientXlsParameter::class,
            'ImplantDate'                      => PatientXlsParameter::class,
            'LastHospDate'                     => PatientXlsParameter::class,
            'ObservationPeriod'                => PatientXlsParameter::class,
            'Comment'                          => PatientXlsParameter::class,
            'Sex'                              => PatientXlsParameter::class,
            'Age'                              => PatientXlsParameter::class,
            'StudyStartAge'                    => PatientXlsParameter::class,
            'IsDead'                           => PatientXlsParameter::class,
            'DieDate'                          => PatientXlsParameter::class,
            'DieCause'                         => PatientXlsParameter::class,
            'DeviceName'                       => PatientXlsParameter::class,
            'RhythmConductanceDisorders'       => PatientXlsParameter::class,
            'Etiology'                         => PatientXlsParameter::class,
            'VentricularTachycardia'           => PatientXlsParameter::class,
            'TypeVentricularTachycardia'       => PatientXlsParameter::class,
            'VentricularFibrillationAnamnesis' => PatientXlsParameter::class,
            'ConsciousnessAtAttack'            => PatientXlsParameter::class,
            'ReasonImplantationEKS'            => PatientXlsParameter::class,
            'ReasonImplantationIKD'            => PatientXlsParameter::class,
            'HeartSurgery'                     => PatientXlsParameter::class,
            'SVT'                              => PatientXlsParameter::class,
            'GT'                               => PatientXlsParameter::class,
            'FG'                               => PatientXlsParameter::class,
            'AtsGTZone'                        => PatientXlsParameter::class,
            'EffectiveAtsGTZone'               => PatientXlsParameter::class,
            'AtsFGZone'                        => PatientXlsParameter::class,
            'EffectiveAtsFGZone'               => PatientXlsParameter::class,
            'NumberShocksReceived'             => PatientXlsParameter::class,
            'ShocksEfficiency'                 => PatientXlsParameter::class,
            'MonthBeforeTherapy'               => PatientXlsParameter::class,
            'SeriousAdverseEvents'             => PatientXlsParameter::class,
            'AdverseImplant'                   => PatientXlsParameter::class,
            'HomeMonitoringMessagesNumber'     => PatientXlsParameter::class,
            'ImplantWithAdverseEvent'          => PatientXlsParameter::class,
            'ImplantWithAdverseEventExplanted' => PatientXlsParameter::class,
            'ClinicTotalVisitsNumber'          => PatientXlsParameter::class,
            'ClinicVisitsNumber'               => PatientXlsParameter::class,
            'ClinicHospitalDaysNumber'         => PatientXlsParameter::class,
            'ClinicDisabilityDaysNumber'       => PatientXlsParameter::class,
            'ClinicAmbulanceCallsNumber'       => PatientXlsParameter::class,
            'DaysBeforeFirstTherapy'           => PatientXlsParameter::class,
        ];
    }

    public function getVarsValues($patients)
    {
        return $this->baseFormVarsValues($patients);
    }
}
