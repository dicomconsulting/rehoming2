<?php

Yii::import('extvendors.phpoffice.phpexcel.Classes' . '/PHPExcel.php');

/**
 * Class XlsOfficeFile
 */
class XlsOfficeFile extends CComponent
{
    /**
     * @var PHPExcel
     */
    public $objPHPExcel;


    public function loadTemplate($template)
    {
        $ext = '';
        if (strrpos($template->name, '.') !== false) {
            $ext = substr($template->name, strrpos($template->name, '.') + 1);
        }
        if (strlen($ext) > 4 || strlen($ext) < 3) {
            $ext = 'xls';
        }
        $data        = base64_decode($template->fileData);
        $tmpFileName = Yii::getPathOfAlias('application.runtime') . DIRECTORY_SEPARATOR . basename(get_class($this)) . '_' . getmypid() . '.' . $ext;
        file_put_contents($tmpFileName, $data);
        $this->objPHPExcel = PHPExcel_IOFactory::load($tmpFileName);
        unlink($tmpFileName);
    }

    public function getXlsSourcesList()
    {
        return [
            'P'   => PatientXlsSource::class,
            'Ikd' => IkdXlsSource::class
        ];

    }

    public function formAllVars($patients)
    {
        $allVars = [];
        $sources = $this->getXlsSourcesList();
        foreach ($sources as $oneSourceName => $sourceClass) {
            /** @var XlsSourceInterface $source */
            $source    = new $sourceClass();
            $varValues = $source->getVarsValues($patients);
            $allVars   = array_merge($allVars, $varValues);
        }
        return $allVars;

    }

    /**
     * В заданную ячейку пишем значения переменных
     *
     * @param $sheet
     * @param $cellCoordinate
     * @param $varData
     */
    public function fixOneCellVarValue(PHPExcel_Worksheet $sheet, $cellCoordinate, $varData)
    {
        $arrayCoordinate = PHPExcel_Cell::coordinateFromString($cellCoordinate);
        $column          = $arrayCoordinate[0];
        $row             = $arrayCoordinate[1];
        foreach ($varData as $oneValue) {
            $sheet->setCellValue($column . $row, $oneValue);
            $row++;
        }
    }


    /**
     * Применить одну переменную на шаблоне
     *
     * @param $varName
     * @param $varData
     * @throws PHPExcel_Exception
     */
    public function applyVar($varName, $varData)
    {
        $checkValue = '{' . $varName . '}';
        foreach ($this->objPHPExcel->getAllSheets() as $sheet) {
            foreach ($sheet->getCellCollection() as $cellCoordinate) {
                $cell      = $sheet->getCell($cellCoordinate);
                $cellValue = $cell->getValue();
                if (($cellValue.'') == $checkValue) {
                    $this->fixOneCellVarValue($sheet, $cellCoordinate, $varData);
                }
            }
        }
    }

    public function applayData($patients)
    {
        $allVars = $this->formAllVars($patients);

        foreach ($allVars as $varName => $varData) {
            $this->applyVar($varName, $varData);
        }
    }

    public function echoData()
    {

        $objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
}
