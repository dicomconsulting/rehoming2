<?php


/**
 * Связь нежелательного явления с имплантатом
 *
 * Class AdverseEventsStatistics
 */
class AdverseEventsStatistics implements StatisticsProviderInterface
{
    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $adverseCount    = 0;
        $allAdverseCount = 0;
        $allAdverse      = [];
        $total           = 0;

        foreach ($patients as $patient) {
            if ($patient->research) {
                $total++;

                $protocols = PatientStats::getAllProtocolAdverse($patient);
                if ($protocols) {
                    $adverseCount++;
                    $allAdverseCount += count($protocols);
                    if (array_key_exists($patient->uid, $allAdverse)) {
                        $allAdverse[$patient->uid] += count($protocols);
                    } else {
                        $allAdverse[$patient->uid] = count($protocols);
                    }

                } else {
                    $allAdverse[$patient->uid] = 0;
                }
            }
        }

        return [
            // Определяется количество пациентов, у которых сохранен протокол «Нежелательное явление» среди пациентов выборки.
            Yii::t('StatisticsUpdateModule.statistics', 'Количество пациентов, у которых зарегистрированы НЯ') => [
                'cnt'      => $adverseCount,
                'totalCnt' => $total
            ],
            // Определяется количество протоколов «Нежелательное явление» среди пациентов выборки.
            Yii::t('StatisticsUpdateModule.statistics', 'Количество НЯ')                                       => [
                'cnt'             => $allAdverseCount,
                'noRelativeValue' => true
            ],
            // Определяется количество пациентов, у которых сохранен протокол «Нежелательное явление» среди пациентов выборки.
            Yii::t('StatisticsUpdateModule.statistics', 'Среднее количество НЯ')                               => [
                'cnt'             => Stats::average($allAdverse),
                'noRelativeValue' => true
            ],
            // Определяется количество пациентов, у которых сохранен протокол «Нежелательное явление» среди пациентов выборки.
            Yii::t('StatisticsUpdateModule.statistics', 'Стандартное отклонение')                              => [
                'cnt'             => Stats::standardDeviation($allAdverse),
                'noRelativeValue' => true
            ],
        ];
    }
}
