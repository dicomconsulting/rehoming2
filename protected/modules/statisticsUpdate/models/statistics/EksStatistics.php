<?php


class EksStatistics implements StatisticsProviderInterface
{
    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $implSssu           = 0;
        $implAvBlockPerm    = 0;
        $implAvBlockTrans   = 0;
        $implBrady          = 0;
        $implBinodalAvPerm  = 0;
        $implBinodalAvTrans = 0;
        $implVasoSyncope    = 0;
        $implCount          = [];
        $total              = 0;

        foreach ($patients as $patient) {
            $protocolEnrolment = PatientStats::getFirstProtocolEnrolment($patient);
            if ($protocolEnrolment) {
                $total++;
                if ($protocolEnrolment->impl_sssu) {
                    $implSssu++;
                    $implCount[$patient->uid] = true;
                }
                if ($protocolEnrolment->impl_av_block_perm) {
                    $implAvBlockPerm++;
                    $implCount[$patient->uid] = true;
                }
                if ($protocolEnrolment->impl_av_block_trans) {
                    $implAvBlockTrans++;
                    $implCount[$patient->uid] = true;
                }
                if ($protocolEnrolment->impl_brady) {
                    $implBrady++;
                    $implCount[$patient->uid] = true;
                }
                if ($protocolEnrolment->impl_binodal_av_perm) {
                    $implBinodalAvPerm++;
                    $implCount[$patient->uid] = true;
                }
                if ($protocolEnrolment->impl_binodal_av_trans) {
                    $implBinodalAvTrans++;
                    $implCount[$patient->uid] = true;
                }
                if ($protocolEnrolment->impl_vaso_syncope) {
                    $implVasoSyncope++;
                    $implCount[$patient->uid] = true;
                }
            }
        }

        return [
            // Определяется количество пациентов, у которых значение параметра «CCCУ, включая тахи-бради синдром» = Да.
            Yii::t('StatisticsUpdateModule.statistics', 'СССУ, включая тахи-бради синдром')                => [
                'cnt'      => $implSssu,
                'totalCnt' => $total
            ],
            // Определяется количество пациентов, у которых значение параметра «Постоянная AV-блокада высокой степени» = Да.
            Yii::t('StatisticsUpdateModule.statistics', 'Постоянная AV-блокада высокой степени')           => [
                'cnt'      => $implAvBlockPerm,
                'totalCnt' => $total
            ],
            // Определяется количество пациентов, у которых значение параметра «Преходящая AV-блокада высокой степени» = Да.
            Yii::t('StatisticsUpdateModule.statistics', 'Преходящая AV-блокада высокой степени')           => [
                'cnt'      => $implAvBlockTrans,
                'totalCnt' => $total
            ],
            // Определяется количество пациентов, у которых значение параметра «Брадикардия с постоянной формой ФП» = Да.
            Yii::t('StatisticsUpdateModule.statistics', 'Брадикардия с постоянной формой ФП')              => [
                'cnt'      => $implBrady,
                'totalCnt' => $total
            ],
            // Определяется количество пациентов, у которых значение параметра «Бинодальное заболевание + постоянная AV блокада» = Да.
            Yii::t('StatisticsUpdateModule.statistics', 'Бинодальное заболевание + постоянная AV блокада') => [
                'cnt'      => $implBinodalAvPerm,
                'totalCnt' => $total
            ],
            // Определяется количество пациентов, у которых значение параметра «Бинодальное заболевание + преходящая AV-блокада» = Да.
            Yii::t('StatisticsUpdateModule.statistics', 'Бинодальное заболевание + преходящая AV-блокада') => [
                'cnt'      => $implBinodalAvTrans,
                'totalCnt' => $total
            ],
            // Определяется количество пациентов, у которых значение параметра «Вазовагальные синкопы» = Да.
            Yii::t('StatisticsUpdateModule.statistics', 'Вазовагальные синкопы')                           => [
                'cnt'      => $implVasoSyncope,
                'totalCnt' => $total
            ],
            Yii::t('StatisticsUpdateModule.statistics', 'Всего')                                           => [
                'cnt'      => count($implCount),
                'totalCnt' => $total
            ],
        ];
    }
}
