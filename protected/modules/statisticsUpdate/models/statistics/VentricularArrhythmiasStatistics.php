<?php

/**
 * Желудочковые нарушения ритма
 *
 * Class VentricularArrhythmiasStatistics
 */
class VentricularArrhythmiasStatistics extends BaseParameterStatistics implements StatisticsProviderInterface
{
    protected $totalCnt = [];

    /**
     * Получить статистику по Желудочковой Аритмии (Общее)
     *
     * @param $statDevices
     * @return array
     */
    public function getVentricularFibrillationTotal($statDevices)
    {
        $vt1ParameterId = $this->getParameterIdByAlias('vt1_episodes');
        $vt2ParameterId = $this->getParameterIdByAlias('vt2_episodes');
        $vfParameterId  = $this->getParameterIdByAlias('vf_episodes');

        $vt1Values = $this->getParameterValues($vt1ParameterId, $statDevices);
        $vt2Values = $this->getParameterValues($vt2ParameterId, $statDevices);
        $vfValues  = $this->getParameterValues($vfParameterId, $statDevices);

        // Объединяем случае, общего случая пришедших данных в одном пучке
        $allValues = [];

        /** @var DeviceValue $oneValue */
        foreach ($vt1Values as $oneValue) {
            if ($oneValue->value) {
                $patientId = $oneValue->deviceState->device->patient_id;
                if (!array_key_exists($patientId, $allValues)) {
                    $allValues[$patientId] = $oneValue->value;
                } else {
                    $allValues[$patientId] += $oneValue->value;
                }
                $this->totalCnt[$patientId] = true;
            }
        }
        foreach ($vt2Values as $oneValue) {
            if ($oneValue->value) {
                $patientId = $oneValue->deviceState->device->patient_id;
                if (!array_key_exists($patientId, $allValues)) {
                    $allValues[$patientId] = $oneValue->value;
                } else {
                    $allValues[$patientId] += $oneValue->value;
                }
                $this->totalCnt[$patientId] = true;
            }
        }
        foreach ($vfValues as $oneValue) {
            if ($oneValue->value) {
                $patientId = $oneValue->deviceState->device->patient_id;
                if (!array_key_exists($patientId, $allValues)) {
                    $allValues[$patientId] = $oneValue->value;
                } else {
                    $allValues[$patientId] += $oneValue->value;
                }
                $this->totalCnt[$patientId] = true;
            }
        }

        $cnt       = count($allValues);
        $average   = $cnt ? round(array_sum($allValues) / $cnt, 2) : 0;
        $deviation = Stats::standardDeviation($allValues);

        return [
            Yii::t('StatisticsUpdateModule.statistics', 'Желудочковая аритмия (общее)') => [
                'cnt'               => $cnt,
                'average'           => $average,
                'standardDeviation' => $deviation
            ]
        ];
    }


    /**
     * Желудочковая тахикардия
     * @param $statDevices
     * @return array
     */
    public function getVentricularTachycardia($statDevices)
    {
        $vt1ParameterId = $this->getParameterIdByAlias('vt1_episodes');
        $vt2ParameterId = $this->getParameterIdByAlias('vt2_episodes');

        $vt1Values = $this->getParameterValues($vt1ParameterId, $statDevices);
        $vt2Values = $this->getParameterValues($vt2ParameterId, $statDevices);

        // Объединяем случае, общего случая пришедших данных в одном пучке
        $allValues = [];

        /** @var DeviceValue $oneValue */
        foreach ($vt1Values as $oneValue) {
            if ($oneValue->value) {
                $patientId = $oneValue->deviceState->device->patient_id;
                if (!array_key_exists($patientId, $allValues)) {
                    $allValues[$patientId] = $oneValue->value;
                } else {
                    $allValues[$patientId] += $oneValue->value;
                }
                $this->totalCnt[$patientId] = true;
            }
        }
        foreach ($vt2Values as $oneValue) {
            if ($oneValue->value) {
                $patientId = $oneValue->deviceState->device->patient_id;
                if (!array_key_exists($patientId, $allValues)) {
                    $allValues[$patientId] = $oneValue->value;
                } else {
                    $allValues[$patientId] += $oneValue->value;
                }
                $this->totalCnt[$patientId] = true;
            }
        }


        $cnt       = count($allValues);
        $average   = $cnt ? round(array_sum($allValues) / $cnt, 2) : 0;
        $deviation = Stats::standardDeviation($allValues);

        return [
            Yii::t('StatisticsUpdateModule.statistics', 'Желудочковая тахикардия') => [
                'cnt'               => $cnt,
                'average'           => $average,
                'standardDeviation' => $deviation
            ]
        ];
    }

    /**
     * Фибрилляция желудочков
     *
     * @param $statDevices
     * @return array
     */
    public function getVentricularFibrillation($statDevices)
    {
        $vfParameterId = $this->getParameterIdByAlias('vf_episodes');
        $vfValues      = $this->getParameterValues($vfParameterId, $statDevices);

        // Объединяем случае, общего случая пришедших данных в одном пучке
        $allValues = [];

        /** @var DeviceValue $oneValue */
        foreach ($vfValues as $oneValue) {
            if ($oneValue->value) {
                $patientId = $oneValue->deviceState->device->patient_id;
                if (!array_key_exists($patientId, $allValues)) {
                    $allValues[$patientId] = $oneValue->value;
                } else {
                    $allValues[$patientId] += $oneValue->value;
                }
                $this->totalCnt[$patientId] = true;
            }
        }


        $cnt       = count($allValues);
        $average   = $cnt ? round(array_sum($allValues) / $cnt, 2) : 0;
        $deviation = Stats::standardDeviation($allValues);

        return [
            Yii::t('StatisticsUpdateModule.statistics', 'Фибрилляция желудочков') => [
                'cnt'               => $cnt,
                'average'           => $average,
                'standardDeviation' => $deviation
            ]
        ];
    }

    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $patientsFiltered = [];
        foreach ($patients as $patient) {
            $protocolEnrolment = PatientStats::getFirstProtocolEnrolment($patient);
            if ($patient->device->model->device_type_uid == 2) {
                $patientsFiltered[] = $patient;
            }
        }

        // Устроойства по которым ведеться статистика
        $statDevicesFiltered = $this->getStatDevices($patientsFiltered);


        // Остальные параметры выделяем в отдельном разделе "Желудочковые нарушения ритма"
        $ventricularFibrillationTotalStats = $this->getVentricularFibrillationTotal($statDevicesFiltered);
        $ventricularTachycardiaStats       = $this->getVentricularTachycardia($statDevicesFiltered);
        $ventricularFibrillation           = $this->getVentricularFibrillation($statDevicesFiltered);
        $total                             = count($patients);

        if (count($patientsFiltered)) {
            $totalCntValue = count($this->totalCnt) . ' / ' . intval(count($this->totalCnt) / count($patientsFiltered) * 100) . '%';
        } else {
            $totalCntValue = '';
        }

        return array_merge(
            $ventricularFibrillationTotalStats,
            $ventricularTachycardiaStats,
            $ventricularFibrillation,
            [
                Yii::t('StatisticsUpdateModule.statistics', 'Пациентов в выборке') => [
                    'cnt'                       => $totalCntValue,
                    'noStandardDeviationСolumn' => true,
                ]
            ]
        );
    }
}
