<?php


class IkdCrtStatistics implements StatisticsProviderInterface
{
    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $implPrimePrevent  = 0;
        $implSecondPrevent = 0;
        $implHeartFailure  = 0;
        $implCount         = [];
        $total             = 0;

        foreach ($patients as $patient) {
            $protocolEnrolment = PatientStats::getFirstProtocolEnrolment($patient);
            if ($protocolEnrolment) {
                $total++;
                if ($protocolEnrolment->impl_prime_prevent) {
                    $implPrimePrevent++;
                    $implCount[$patient->uid] = true;
                }
                if ($protocolEnrolment->impl_second_prevent) {
                    $implSecondPrevent++;
                    $implCount[$patient->uid] = true;
                }
                if ($protocolEnrolment->impl_heart_failure) {
                    $implHeartFailure++;
                    $implCount[$patient->uid] = true;
                }

            }
        }

        return [
            // ИКД, CRT
            // Определяется количество пациентов, у которых значение параметра «ИКД, CRT. Первичная профилактика» = Да.
            Yii::t('StatisticsUpdateModule.statistics', 'Первичная профилактика')    => [
                'cnt'      => $implPrimePrevent,
                'totalCnt' => $total
            ],
            // Определяется количество пациентов, у которых значение параметра «ИКД, CRT. Вторичная профилактика» = Да.
            Yii::t('StatisticsUpdateModule.statistics', 'Вторичная профилактика')    => [
                'cnt'      => $implSecondPrevent,
                'totalCnt' => $total
            ],
            // Определяется количество пациентов, у которых значение параметра «ИКД, CRT. Сердечная недостаточность» = Да.
            Yii::t('StatisticsUpdateModule.statistics', 'Сердечная недостаточность') => [
                'cnt'      => $implHeartFailure,
                'totalCnt' => $total
            ],
            Yii::t('StatisticsUpdateModule.statistics', 'Пациентов в выборке')       => [
                'cnt'      => count($implCount),
                'totalCnt' => $total
            ],
        ];
    }
}
