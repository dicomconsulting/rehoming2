<?php


class NyhaStatistics implements StatisticsProviderInterface
{
    /**
     * Получить значение параметра Nyha
     *
     * @param Patient $patient
     * @return null
     */
    protected function getPatientNyha(Patient $patient)
    {
        $protocolEnrolment = PatientStats::getFirstProtocolEnrolment($patient);

        if ($protocolEnrolment) {
            return $protocolEnrolment->patient_nyha;
        } else {
            return null;
        }
    }

    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $allNyha = [];
        foreach ($patients as $patient) {
            $nyha      = $this->getPatientNyha($patient);
            $allNyha[] = $nyha;
        }
        $total = count($allNyha);

        // Вычисляем средний класс
        $nuhaWithoutNA  = array_diff($allNyha, [6]);
        $nuhaZerroNo    = array_replace($nuhaWithoutNA, array_fill_keys(array_keys($nuhaWithoutNA, '5'), '0')); // Заменяем значения 6 на 0
        $averageClassId = round(Stats::average($nuhaZerroNo), 2);

        return [
            // Определяется % пациентов, у которых значение параметра «NYHA класс CH» = I
            Yii::t('StatisticsUpdateModule.statistics', 'I')                     => [
                'cnt'      => Stats::equalValuesCount($allNyha, '1'),
                'totalCnt' => $total
            ],
            Yii::t('StatisticsUpdateModule.statistics', 'II')                    => [
                'cnt'      => Stats::equalValuesCount($allNyha, '2'),
                'totalCnt' => $total
            ],
            Yii::t('StatisticsUpdateModule.statistics', 'III')                   => [
                'cnt'      => Stats::equalValuesCount($allNyha, '3'),
                'totalCnt' => $total
            ],
            Yii::t('StatisticsUpdateModule.statistics', 'IV')                    => [
                'cnt'      => Stats::equalValuesCount($allNyha, '4'),
                'totalCnt' => $total
            ],
            Yii::t('StatisticsUpdateModule.statistics', 'Нет')                   => [
                'cnt'      => Stats::equalValuesCount($allNyha, '5'),
                'totalCnt' => $total
            ],
            Yii::t('StatisticsUpdateModule.statistics', 'Оценка не проводилась') => [
                'cnt'      => Stats::equalValuesCount($allNyha, '6'),
                'totalCnt' => $total
            ],
            Yii::t('StatisticsUpdateModule.statistics', 'Средний класс по NYHA') => [
                'cnt'             => $averageClassId,
                'noRelativeValue' => true
            ],
            Yii::t('StatisticsUpdateModule.statistics', 'Всего')                 => [
                'cnt'             => $total,
                'noRelativeValue' => true
            ]
        ];
    }
}
