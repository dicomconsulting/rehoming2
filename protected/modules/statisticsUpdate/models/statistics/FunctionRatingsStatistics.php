<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 29.02.16
 * Time: 16:46
 */


/**
 * 15. Оценка функций
 *
 * Class FunctionRatingsStatistics
 */
class FunctionRatingsStatistics implements StatisticsProviderInterface
{

    /**
     * Cформировать статистику по одному элементу
     *
     * @param $elementData
     * @return array
     */
    public function formElementStats($elementData)
    {
        $returnValue = [];
        for ($i = 5; $i > 0; $i--) {
            $cnt             = Stats::equalValuesCount($elementData, $i . '');
            $returnValue[$i] = [
                'cnt'      => $cnt,
                'totalCnt' => count($elementData),
            ];
        }
        $returnValue[Yii::t('StatisticsModule.statistics', 'Средний балл')] = [
            'cnt'             => Stats::average($elementData),
            'noRelativeValue' => true
        ];
        return $returnValue;
    }

    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $rtgTimeHmsc      = [];
        $rtgTimeSemaphore = [];
        $rtgTimeIegm      = [];
        $rtgClHmsc        = [];
        $rtgClSemaphore   = [];
        $rtgClIegm        = [];
        $rtgClDataSuff    = [];

        foreach ($patients as $patient) {
            $protocols = PatientStats::getAllProtocolsAnnual($patient);
            if ($protocols) {
                foreach ($protocols as $protocolAnnual) {
                    $rtgTimeHmsc[]      = $protocolAnnual->rtg_time_hmsc;
                    $rtgTimeSemaphore[] = $protocolAnnual->rtg_time_semaphore;
                    $rtgTimeIegm[]      = $protocolAnnual->rtg_time_iegm;
                    $rtgClHmsc[]        = $protocolAnnual->rtg_cl_hmsc;
                    $rtgClSemaphore[]   = $protocolAnnual->rtg_cl_semaphore;
                    $rtgClIegm[]        = $protocolAnnual->rtg_cl_iegm;
                    $rtgClDataSuff[]    = $protocolAnnual->rtg_cl_data_suff;
                }
            }
        }

        return [
            'rtgTimeHmsc'      => $this->formElementStats($rtgTimeHmsc),
            'rtgTimeSemaphore' => $this->formElementStats($rtgTimeSemaphore),
            'rtgTimeIegm'      => $this->formElementStats($rtgTimeIegm),
            'rtgClHmsc'        => $this->formElementStats($rtgClHmsc),
            'rtgClSemaphore'   => $this->formElementStats($rtgClSemaphore),
            'rtgClIegm'        => $this->formElementStats($rtgClIegm),
            'rtgClDataSuff'    => $this->formElementStats($rtgClDataSuff)
        ];
    }
}