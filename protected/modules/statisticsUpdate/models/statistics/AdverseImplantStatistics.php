<?php


/**
 * Нежелательные явления
 *
 * Class AdverseImplantStatistics
 */
class AdverseImplantStatistics implements StatisticsProviderInterface
{
    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $noLink       = 0;
        $possibleLink = 0;
        $clearLink    = 0;
        $total        = 0;

        foreach ($patients as $patient) {
            $protocols = PatientStats::getAllProtocolAdverse($patient);
            if ($protocols) {
                foreach ($protocols as $protocol) {
                    $total++;
                    /** @var ProtocolAdverse $protocol */
                    if ($protocol->adv_iml_relation == 0) {
                        $noLink++;
                    } else {
                        if ($protocol->adv_iml_relation == 1) {
                            $possibleLink++;
                        } else {
                            if ($protocol->adv_iml_relation == 2) {
                                $clearLink++;
                            }
                        }
                    }
                }
            }
        }

        return [
            // Определяется количество НЯ (по количеству протоколов), у которых указано значение «явное отсутствие связи» = Да.
            Yii::t('StatisticsUpdateModule.statistics', 'Явное отсутствие связи') => [
                'cnt'      => $noLink,
                'totalCnt' => $total
            ],
            // Определяется количество НЯ (по количеству протоколов), у которых указано значение «Связь возможна» = Да.
            Yii::t('StatisticsUpdateModule.statistics', 'Связь возможна')         => [
                'cnt'      => $possibleLink,
                'totalCnt' => $total
            ],
            // Определяется количество НЯ (по количеству протоколов), у которых указано значение «Связь очевидна» = Да.
            Yii::t('StatisticsUpdateModule.statistics', 'Связь очевидна')         => [
                'cnt'      => $clearLink,
                'totalCnt' => $total
            ]
        ];
    }
}
