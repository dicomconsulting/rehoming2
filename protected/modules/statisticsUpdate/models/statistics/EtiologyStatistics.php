<?php

/**
 * Статистика раздела Этиология
 *
 * Class EtiologyStatistics
 */
class EtiologyStatistics implements StatisticsProviderInterface
{
    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $enrolmentEtiology = [];
        $total             = 0;

        foreach ($patients as $patient) {
            $protocolEnrolment = PatientStats::getFirstProtocolEnrolment($patient);
            if ($protocolEnrolment) {
                $total++;
                $protocolEtiology = $protocolEnrolment->etiology;
                foreach ($protocolEtiology as $mkb19Element) {
                    $enrolmentEtiology[] = $mkb19Element->uid;
                }
            }
        }

        return [
            // Определяется количество пациентов, у которых значение параметра «Артериальная гипертензия» = Да.
            Yii::t('StatisticsUpdateModule.statistics', 'Артериальная гипертензия') => [
                'cnt'      => Stats::equalValuesCount($enrolmentEtiology, '14911'),
                'totalCnt' => $total
            ],
            // Определяется количество пациентов, у которых значение параметра «ИБС» = Да.
            Yii::t('StatisticsUpdateModule.statistics', 'ИБС')                      => [
                'cnt'      => Stats::equalValuesCount($enrolmentEtiology, '14802'),
                'totalCnt' => $total
            ],
            // Определяется количество пациентов, у которых значение параметра «Сахарный диабет» = Да
            Yii::t('StatisticsUpdateModule.statistics', 'Сахарный диабет')          => [
                'cnt'      => Stats::equalValuesCount($enrolmentEtiology, '14459'),
                'totalCnt' => $total
            ],
            // Определяется количество пациентов, у которых значение параметра «ВПС» = Да.
            Yii::t('StatisticsUpdateModule.statistics', 'ВПС')                      => [
                'cnt'      => Stats::equalValuesCount($enrolmentEtiology, '14502'),
                'totalCnt' => $total
            ],
            // Определяется количество пациентов, у которых значение параметра «ППС» = Да.
            Yii::t('StatisticsUpdateModule.statistics', 'ППС')                      => [
                'cnt'      => Stats::equalValuesCount($enrolmentEtiology, '14815'),
                'totalCnt' => $total
            ],
            // Определяется количество пациентов, у которых значение параметра «Кардиомиопатия» = Да.
            Yii::t('StatisticsUpdateModule.statistics', 'Кардиомиопатия')           => [
                'cnt'      => Stats::equalValuesCount($enrolmentEtiology, '14837'),
                'totalCnt' => $total
            ],
        ];
    }
}
