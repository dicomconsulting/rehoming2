<?php

/**
 * Электроимпульсная терапия
 *
 * Class CountershockStatistics
 */
class CountershockStatistics extends BaseParameterStatistics implements StatisticsProviderInterface
{
    /**
     * Расчет процентов
     *
     * @param $array
     * @return array
     */
    protected function calculationPercentage($array)
    {
        $resultPercentage = [];
        foreach ($array as $key => $oneElement) {
            if (array_key_exists('start', $oneElement) && array_key_exists('stop', $oneElement) && $oneElement['start']) {
                $resultPercentage[$key] = (100 * $oneElement['stop']) / $oneElement['start'];
            }
        }
        return $resultPercentage;
    }

    public function getEfficiencyofATPinZoneBTP($statDevices)
    {
        $atpStartId = $this->getParameterIdByAlias('atp_in_vt_zones_started');
        $atpStopId  = $this->getParameterIdByAlias('atp_in_vt_zones_successful');

        $atpStartValues = $this->getParameterValues($atpStartId, $statDevices);
        $atpStopValues  = $this->getParameterValues($atpStopId, $statDevices);

        // Объединяем случае, общего случая пришедших данных в одном пучке
        $groupValues = $this->groupValues($atpStartValues, $atpStopValues);
        $patients    = [];

        foreach ($groupValues as $oneValue) {
            $patientUid = $oneValue['startObj']->deviceState->device->patient_id;
            if ($oneValue['start']) {
                $patients[$patientUid] = true;
            }
        }

        // Считаем проценты, в массиве
        $percentage = $this->calculationPercentage($groupValues);

        // По процентам выводм среднее
        $totalCount = count($statDevices);
        $cnt        = $totalCount ? round(array_sum($percentage) / $totalCount, 2) : 0;

        return [
            'EffectivityAtpInVt'         => $cnt,
            'EffectivityAtpInVtMoreZero' => count($patients)
        ];
    }

    public function getEfficiencyofATPinZoneVF($statDevices)
    {
        $atpStartId = $this->getParameterIdByAlias('atp_one_shot_started');
        $atpStopId  = $this->getParameterIdByAlias('atp_one_shot_successful');

        $atpStartValues = $this->getParameterValues($atpStartId, $statDevices);
        $atpStopValues  = $this->getParameterValues($atpStopId, $statDevices);

        // Объединяем случае, общего случая пришедших данных в одном пучке
        $groupValues = $this->groupValues($atpStartValues, $atpStopValues);
        $patients    = [];

        foreach ($groupValues as $oneValue) {
            $patientUid = $oneValue['startObj']->deviceState->device->patient_id;
            if ($oneValue['start']) {
                $patients[$patientUid] = true;
            }
        }

        // Считаем проценты, в массиве
        $percentage = $this->calculationPercentage($groupValues);

        // По процентам выводм среднее
        $patientsCount = count($statDevices);
        $cnt           = $patientsCount ? round(array_sum($percentage) / $patientsCount, 2) : 0;

        return [
            'EffectivityAtpInVf'         => $cnt,
            'EffectivityAtpInVfMoreZero' => count($patients)
        ];
    }

    /**
     * @param $statDevices
     * @return array
     */
    public function getHomeMonitoringStatistics($statDevices)
    {
        $cnt                = 0;
        $maxCount           = 0;
        $totalCount         = 0;
        $patientsShocks     = [];
        $patientsEfficiency = [];

        $startId   = $this->getParameterIdByAlias('shocks_started');
        $abortId   = $this->getParameterIdByAlias('shocks_aborted');
        $successId = $this->getParameterIdByAlias('shocks_successful');

        /** @var Device $statDevice */
        foreach ($statDevices as $statDeviceId) {

            $deviceObj                  = Device::model()->findByPk($statDeviceId);
            $patientId                  = $deviceObj->patient_id;
            $patientsShocks[$patientId] = 0;

            // Перебираем всех пациентов, получаем у них значения параметров
            $startValues   = $this->getParameterValues($startId, [$statDeviceId]);
            $abortValues   = $this->getParameterValues($abortId, [$statDeviceId]);
            $successValues = $this->getParameterValues($successId, [$statDeviceId]);

            if (count($startValues) && count($abortValues)) {
                $cnt = $startValues[0]->value - $abortValues[0]->value; // Количество шоковых разрядов

                if ($maxCount < $cnt) {
                    $maxCount = $cnt;
                }
                $totalCount += $cnt;
                $patientsShocks[$patientId] = $cnt;
            }
            if (count($successValues)) {
                $successCount = $successValues[0]->value;
            } else {
                $successCount = 0;
            }

            if ($cnt) {
                $patientsEfficiency[$patientId] = $successCount / $cnt * 100;
            }
        }

        $effectivity  = Stats::average($patientsEfficiency);
        $deviation    = Stats::standardDeviation($patientsShocks);
        $averageCount = Stats::average($patientsShocks);

        return [
            'MaxCountShockDischarges'          => $maxCount, // Максимальное количество нанесенных шоковых разрядов
            'AverageCountShockDischarges'      => $averageCount, // Среднее количество нанесенных шоковых разрядов
            'StandardDeviationShockDischarges' => $deviation, // Стандартное отклонение по параметру «Количество нанесенных шоковых разрядов»
            'EffectiveShockDischarges'         => $effectivity.'%' // Эффективность нанесенных шоковых разрядов
        ];
    }

    public function getShockStatistics($statDevices)
    {
        $startId      = $this->getParameterIdByAlias('shocks_started');
        $successfulId = $this->getParameterIdByAlias('shocks_successful');

        $startValues      = $this->getParameterValues($startId, $statDevices);
        $successfulValues = $this->getParameterValues($successfulId, $statDevices);

        $patientsWithShock           = [];
        $patientsWithShockSuccessful = [];
        foreach ($startValues as $value) {
            /** @var $value DeviceValue */
            $patientId = $value->deviceState->device->patient_id;
            if ($value->value) {
                $patientsWithShock[$patientId] = true;
            }
        }

        foreach ($successfulValues as $value) {
            /** @var $value DeviceValue */
            $patientId = $value->deviceState->device->patient_id;
            if ($value->value) {
                $patientsWithShock[$patientId]           = true;
                $patientsWithShockSuccessful[$patientId] = true;
            }
        }
        $relative        = null;
        $impulseRelative = null;
        if ($statDevices) {
            $relative        = count($patientsWithShock) / count($statDevices) * 100;
            $impulseRelative = (count($patientsWithShock) + count($patientsWithShockSuccessful)) / count($statDevices) * 100;
        }

        return [
            'TotalPatientsWithShock'           => count($patientsWithShock),
            'TotalPatientsWithShockRelative'   => round($relative, 2).'%',
            'TotalPatientsWithImpulse'         => count($patientsWithShockSuccessful),
            'TotalPatientsWithImpulseRelative' => round($impulseRelative, 2).'%'
        ];
    }

    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $patientsFiltered = [];
        foreach ($patients as $patient) {
            if ($patient->device->model->device_type_uid == 2) {
                $patientsFiltered[] = $patient;
            }
        }
        $statDevices = $this->getStatDevices($patients);

        $data = [];

        // Эффективность АТР в зоне VT
        $data = array_merge(
            $data,
            $this->getEfficiencyofATPinZoneBTP($statDevices)
        );

        // Эффективность АТР в зоне VF
        $data = array_merge(
            $data,
            $this->getEfficiencyofATPinZoneVF($statDevices)
        );

        // Статистика по шоковым разрядам
        $data = array_merge(
            $data,
            $this->getHomeMonitoringStatistics($statDevices)
        );

        // Общее количество пациентов, получивших шоки
        $data = array_merge(
            $data,
            $this->getShockStatistics($statDevices)
        );

        return $data;
    }
}
