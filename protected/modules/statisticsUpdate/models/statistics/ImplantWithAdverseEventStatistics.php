<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 01.03.16
 * Time: 14:23
 */

/**
 * Имплантат, связанный с нежелательным явлением
 *
 * Class ImplantWithAdverseEventStatistics
 */
class ImplantWithAdverseEventStatistics implements StatisticsProviderInterface
{
    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $totalCnt    = 0;
        $eksCnt      = 0;
        $crtP        = 0;
        $crtD        = 0;
        $ikd         = 0;
        $ppElectrode = 0;
        $pgElectrode = 0;
        $ksElectrode = 0;
        $implExpl    = 0;

        foreach ($patients as $patient) {
            $protocols = PatientStats::getAllProtocolAdverse($patient);
            if ($protocols) {
                foreach ($protocols as $protocol) {
                    $totalCnt++;
                    /** @var $protocol ProtocolAdverse */
                    if ($protocol->adv_iml_relation_device == 1) {
                        $ikd++;
                    }
                    if ($protocol->adv_iml_relation_device == 2) {
                        $eksCnt++;
                    }
                    if ($protocol->adv_iml_relation_device == 3) {
                        $crtP++;
                    }
                    if ($protocol->adv_iml_relation_device == 4) {
                        $crtD++;
                    }
                    if ($protocol->adv_iml_relation_electr == 1) {
                        $ppElectrode++;
                    }
                    if ($protocol->adv_iml_relation_electr == 2) {
                        $pgElectrode++;
                    }
                    if ($protocol->adv_iml_relation_electr == 3) {
                        $ksElectrode++;
                    }
                    if ($protocol->impl_expl) {
                        $implExpl++;
                    }
                }
            }
        }

        return [
            Yii::t('StatisticsUpdateModule.statistics', 'ЭКС')                   => [
                'cnt'      => $eksCnt,
                'totalCnt' => $totalCnt
            ],
            Yii::t('StatisticsUpdateModule.statistics', 'СRT-P')                 => [
                'cnt'      => $crtP,
                'totalCnt' => $totalCnt
            ],
            Yii::t('StatisticsUpdateModule.statistics', 'ИКД')                   => [
                'cnt'      => $ikd,
                'totalCnt' => $totalCnt
            ],
            Yii::t('StatisticsUpdateModule.statistics', 'CRT-D')                 => [
                'cnt'      => $crtD,
                'totalCnt' => $totalCnt
            ],
            Yii::t('StatisticsUpdateModule.statistics', 'Электрод ПП')           => [
                'cnt'      => $ppElectrode,
                'totalCnt' => $totalCnt
            ],
            Yii::t('StatisticsUpdateModule.statistics', 'ПЖ')                    => [
                'cnt'      => $pgElectrode,
                'totalCnt' => $totalCnt
            ],
            Yii::t('StatisticsUpdateModule.statistics', 'КС')                    => [
                'cnt'      => $ksElectrode,
                'totalCnt' => $totalCnt
            ],
            Yii::t('StatisticsUpdateModule.statistics', 'Прибор эксплантирован') => [
                'cnt'             => $implExpl,
                'noRelativeValue' => true
            ],
        ];
    }
}
