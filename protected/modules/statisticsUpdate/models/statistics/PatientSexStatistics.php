<?php


class PatientSexStatistics implements StatisticsProviderInterface
{
    /**
     * @param Patient[] $patients
     * @return array
     */
    public function getStatistics($patients)
    {
        $femaleCount = 0;
        $maleCount   = 0;
        $unknown     = 0;
        $total       = count($patients);
        foreach ($patients as $patient) {
            if ($patient->sex == Patient::MALE) {
                $maleCount++;
            }
            if ($patient->sex == Patient::FEMALE) {
                $femaleCount++;
            }
            if ($patient->sex == Patient::UNKNOWN) {
                $unknown++;
            }

        }
        return [
            Yii::t('StatisticsUpdateModule.statistics', 'Женщины')          => [
                'cnt'      => $femaleCount,
                'totalCnt' => $total
            ],
            Yii::t('StatisticsUpdateModule.statistics', 'Мужчины')          => [
                'cnt'      => $maleCount,
                'totalCnt' => $total
            ],
            Yii::t('StatisticsUpdateModule.statistics', 'Пол не определен') => [
                'cnt'      => $unknown,
                'totalCnt' => $total
            ],
            Yii::t('StatisticsUpdateModule.statistics', 'Всего')            => [
                'cnt'             => $total,
                'noRelativeValue' => true,
            ]
        ];
    }
}
