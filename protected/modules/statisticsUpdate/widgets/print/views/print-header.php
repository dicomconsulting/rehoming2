<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 11.11.13
 * Time: 17:04
 *
 * @var string $title
 * @var Patient $patient
 * @var PrintHeader $this
 */
?>
<!DOCTYPE html>
<html lang="en"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" /></head>
<body style="margin: 0; padding: 0;">
<table style="border: 0; border-collapse: collapse; width: 100%; border-bottom: 1px #000000 solid;font-size: 14px;">
    <tr style="font-weight: bold">
        <td style="text-align: left;font-size: 16px;">ReHoming</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td style="text-align: right;"><?=$title?> на <?=$date->format(RhDateTime::RH_DATETIME)?></td>
        <td style="text-align: right;width: 170px;"><img src="<?=Yii::app()->request->getBaseUrl(true)?>/images/logo_vnoa.jpg" width="75" height="63"></td>
    </tr>
</table>
</body>
</html>