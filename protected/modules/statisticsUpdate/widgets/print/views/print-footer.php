<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 11.11.13
 * Time: 17:04
 *
 * @var string $title
 * @var Protocol $protocol
 */
?>
<!DOCTYPE html>
<html lang="en"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" /></head>
<body style="margin: 0; padding: 0;">
<table style="border: 0; border-collapse: collapse; width: 100%; border-top: 1px #000000 solid;font-size: 12px;">
    <tr style="font-weight: bold">
        <td colspan="2">ReHoming - <?=$title?></td>
        <td colspan="2" style="text-align: right;"><?=$page?>/<?=$pageTotal?></td>
    </tr>
</table>
</body>
</html>