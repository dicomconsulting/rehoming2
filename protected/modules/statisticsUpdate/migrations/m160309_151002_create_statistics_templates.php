<?php

class m160309_151002_create_statistics_templates extends EDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{statistics_templates}}',
            [
                'id'       => 'pk',
                'name'     => 'varchar(255) NOT NULL',
                'group_id' => 'integer',
                'fileData' => 'longblob'
            ],
            'ENGINE=InnoDB'
        );
        $this->addForeignKey(
            'fk_statistics_templates_group_id',
            '{{statistics_templates}}',
            'group_id',
            '{{user_group}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_statistics_templates_group_id', '{{statistics_templates}}');
        $this->dropTable('{{statistics_templates}}');
    }
}
