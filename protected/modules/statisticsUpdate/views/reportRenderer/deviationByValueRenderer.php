<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 19.12.13
 * Time: 12:03
 * @var string $header
 * @var array $data
 */
?>

<h5><?= $header; ?></h5>
<table class="table table-condensed table-bordered rh-main-table statistics">
    <col width="40%" valign="middle">
    <col width="20%" valign="middle">
    <col width="20%" valign="middle">
    <col width="20%" valign="middle">
    <thead>
    <tr>
        <td><?= Yii::t('StatisticsModule.statistics', 'Параметр'); ?> </td>
        <td><?php
            if (isset($column1Header)) {
                echo $column1Header;
            } else {
                echo Yii::t('StatisticsModule.statistics', 'Количество');
            }
            ?>
        </td>
        <td><?= Yii::t('StatisticsModule.statistics', 'Среднее значение'); ?></td>
        <td><?= Yii::t('StatisticsModule.statistics', 'Стандартное отклонение'); ?></td>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($data as $name => $val) { ?>
        <tr>
            <td>
                <?= $name; ?>
            </td>
            <td>
                <?= $val['cnt']; ?>
            </td>
            <?php
            if (array_key_exists('noStandardDeviationСolumn', $val)) {
                ?>
                <td colspan="2">
                    <?= $val['average']; ?>
                </td>
                <?php
            } else {
                ?>
                <td>
                    <?= $val['average']; ?>
                </td>
                <td>
                    <?= $val['standardDeviation']; ?>
                </td>
                <?php
            }
            ?>
        </tr>
    <?php } ?>
    </tbody>
</table>
