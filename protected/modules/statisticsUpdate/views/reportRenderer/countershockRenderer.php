<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 24.03.16
 * Time: 11:36
 */
?>
<h5><?= Yii::t('StatisticsModule.statistics', 'Электроимпульсная терапия'); ?></h5>

<table class="table table-condensed table-bordered rh-main-table statistics">
    <col width="40%" valign="middle">
    <col width="35%" valign="middle">
    <col width="25%" valign="middle">
    <thead>
    <tr>
        <td><?= Yii::t('StatisticsModule.statistics', 'Параметр'); ?></td>
        <td><?= Yii::t('StatisticsModule.statistics', 'Значение'); ?></td>
        <td><?= Yii::t('StatisticsModule.statistics', 'Количество пациентов с эффективностью > 0'); ?></td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><?= Yii::t('StatisticsModule.statistics', 'Эффективность АТР в зоне VT'); ?></td>
        <td>
            <?= $data['EffectivityAtpInVt']; ?>
        </td>
        <td>
            <?= $data['EffectivityAtpInVtMoreZero']; ?>
        </td>
    </tr>
    <tr>
        <td><?= Yii::t('StatisticsModule.statistics', 'Эффективность АТР в зоне VF'); ?></td>
        <td>
            <?= $data['EffectivityAtpInVf']; ?>
        </td>
        <td>
            <?= $data['EffectivityAtpInVfMoreZero']; ?>
        </td>
    </tr>
    </tbody>
</table>

<table class="table table-condensed table-bordered rh-main-table statistics">
    <col width="40%" valign="middle">
    <col width="35%" valign="middle">
    <thead>
    <tr>
        <td><?= Yii::t('StatisticsModule.statistics', 'Параметр'); ?></td>
        <td><?= Yii::t('StatisticsModule.statistics', 'Значение'); ?></td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><?= Yii::t('StatisticsModule.statistics', 'Максимальное количество нанесенных шоковых разрядов'); ?></td>
        <td>
            <?= $data['MaxCountShockDischarges']; ?>
        </td>
    </tr>
    <tr>
        <td><?= Yii::t('StatisticsModule.statistics', 'Среднее количество нанесенных шоковых разрядов'); ?></td>
        <td>
            <?= $data['AverageCountShockDischarges']; ?>
        </td>
    </tr>
    <tr>
        <td><?= Yii::t('StatisticsModule.statistics', 'Стандартное отклонение по параметру «Количество нанесенных шоковых разрядов»'); ?></td>
        <td>
            <?= $data['StandardDeviationShockDischarges']; ?>
        </td>
    </tr>
    <tr>
        <td><?= Yii::t('StatisticsModule.statistics', 'Эффективность нанесенных шоковых разрядов'); ?></td>
        <td>
            <?= $data['EffectiveShockDischarges']; ?>
        </td>
    </tr>
    </tbody>
</table>

<table class="table table-condensed table-bordered rh-main-table statistics">
    <col width="40%" valign="middle">
    <col width="35%" valign="middle">
    <thead>
    <tr>
        <td><?= Yii::t('StatisticsModule.statistics', 'Параметр'); ?></td>
        <td><?= Yii::t('StatisticsModule.statistics', 'Количественное значение'); ?></td>
        <td><?= Yii::t('StatisticsModule.statistics', 'Относительное значение'); ?></td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><?= Yii::t('StatisticsModule.statistics', 'Общее количество пациентов, получивших шоки'); ?></td>
        <td>
            <?= $data['TotalPatientsWithShock']; ?>
        </td>
        <td>
            <?= $data['TotalPatientsWithShockRelative']; ?>
        </td>
    </tr>
    <tr>
        <td><?= Yii::t('StatisticsModule.statistics', 'Пациенты, получившие электроимпульсную терапию'); ?></td>
        <td>
            <?= $data['TotalPatientsWithImpulse']; ?>
        </td>
        <td>
            <?= $data['TotalPatientsWithImpulseRelative']; ?>
        </td>
    </tr>
    </tbody>
</table>


