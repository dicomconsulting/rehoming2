<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 26.01.16
 * Time: 17:19
 */
/* @var $this ViewController */
/* @var $filter */
/* @var $totalPatientsStatistics */
/* @var $patientSexStatistics */
/* @var $savedFilterId */

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->assetManager->publish(
        Yii::getPathOfAlias('admin.assets') . '/js/savedFilter.js'
    ),
    CClientScript::POS_END
);

ViewFragment::registerScriptOptions('savedFilterObj', 'SavedFilterClass',
    [
        'elementId'    => 'savedFilter',
        'filterValues' => SavedFilter::getFiltersUrls(
            SavedFilter::PLACEMENT_STATISTICS,
            'Filter',
            '/statisticsUpdate/view/index/'
        ),
        'modalId'      => 'SavedFilterModal',
        'saveUrl'      => '/admin/savedFilter/saveSavedFilterParams',
        'deleteUrl'    => '/admin/savedFilter/deleteSavedFilter',
        'placement'    => SavedFilter::PLACEMENT_STATISTICS,
        'inputPrefix'  => 'Filter',
        'urlPrefix'    => '/statisticsUpdate/view/index/',
        'getFilterUrl' => '/admin/savedFilter/getFilterUrl',
    ]
);
Yii::app()->getController()->widget(
    'Modal',
    [
        'id'      => 'SavedFilterModal',
        'header'  => Yii::t('AdminModule.messages', 'Сохраненный фильтр'),
        'content' => $this->renderFile(Yii::getPathOfAlias('application.modules.admin') . '/views/savedFilter/createNewFilter.php', [], true),
    ]
);
?>
<div class="row noprint">
    <div class="span10">
        <form class="form-search" style="margin: 0;">
            <span class="showPatientsFilter">
                <?= Yii::t('PatientModule.patient', 'Сохраненный фильтр') ?>
</span>
            <?php
            echo CHtml::dropDownList(
                'savedFilter',
                $savedFilterId,
                SavedFilter::getFiltersList(SavedFilter::PLACEMENT_STATISTICS),
                [
                    'onchange' => 'savedFilterObj.handleClickFilter()'
                ]);
            echo '&nbsp;&nbsp;&nbsp;';
            echo CHtml::button(Yii::t('PatientModule.patient', 'Сохранить'), [
                'onclick' => 'savedFilterObj.handleSaveFilter();'
            ]);
            echo '&nbsp;';
            echo CHtml::button(Yii::t('PatientModule.patient', 'Удалить'), [
                'onclick' => 'savedFilterObj.handleDeleteFilter();'
            ])
            ?>
        </form>
    </div>
</div>
<br>
