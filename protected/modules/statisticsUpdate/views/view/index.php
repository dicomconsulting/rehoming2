<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 26.01.16
 * Time: 17:19
 */
/* @var $this ViewController */
/* @var $filter */
/* @var $totalPatientsStatistics */
/* @var $patientSexStatistics */
/* @var $patientAgeStatistics */
/* @var $observationPeriodStatistics */
/* @var $nyhaStatistics */
/* @var $rhythmDisturbancesStatistics */
/* @var $seriousAdverseEvents */
/* @var $etiologyStatistics */
/* @var $preparationsStatistics */
/* @var $heartSurgeryStatistics */
/* @var $eksStatistics */
/* @var $ikdCrtStatistics */
/* @var $implantationStimulatingStatistics */
/* @var $trendMonitoringStatistics */
/* @var $trendMonitoringAdditionalStatistics */
/* @var $adverseEventsStatistics */
/* @var $adverseImplantStatistics */
/* @var $shockDischargesStatistics */
/* @var $homeMonitoringStatistics */
/* @var $homeMonitoringStatisticsPart2 */
/* @var $functionRatingsStatistics */
/* @var $implantWithAdverseEventStatistics */
/* @var $totalNumberVisitStatistics */
/* @var $visitsNumberStatistics */
/* @var $clinicLoadStatistics */
/* @var $daysBeforeTherapyStatistics */
/* @var $ventricularArrhythmiasStatistics */
/* @var $countershockStatistics */


Yii::app()->clientScript->registerCssFile(
    Yii::app()->assetManager->publish(
        Yii::getPathOfAlias('statisticsUpdate.assets') . '/css/statistics.css'
    )
);
?>
<h2 class="noprint"><?= Yii::t('StatisticsUpdateModule.statistics', 'Статистика') ?></h2>

<div class="right noprint" style="text-align: right; margin-bottom: 20px;margin-top: -35px;">
    <i class="icon-xls"></i> <?= CHtml::link(Yii::t('StatisticsUpdateModule.statistics', 'Экспорт'), $this->createUrl('/statisticsUpdate/export/xls', $_GET)) ?>
    &nbsp;&nbsp;&nbsp;
    <i class="icon-print"></i> <?= CHtml::link(Yii::t('StatisticsUpdateModule.statistics', 'Печать'), $this->createUrl($this->id . "/print", $_GET)) ?>
</div>
<?php
echo $this->renderPartial('savedFilter', $_data_);
echo $this->renderPartial('filter', $_data_);

if (array_key_exists('formStatistics', $filter)) {
    if (!isset($totalPatientsStatistics)) {
        echo Yii::t('StatisticsUpdateModule.statistics', 'Нет пациентов удолетворяющих условиям. <br>');
    } else {
        echo Yii::t('StatisticsUpdateModule.statistics', 'Всего пациентов в выборке: ') . $totalPatientsStatistics . '<br>';
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('StatisticsUpdateModule.statistics', 'Пол пациента'),
            'data'   => $patientSexStatistics
        ]);
        echo $this->renderPartial('../reportRenderer/simpleRenderer', [
            'header' => Yii::t('StatisticsUpdateModule.statistics', 'Возраст пациента'),
            'data'   => $patientAgeStatistics
        ]);
        echo $this->renderPartial('../reportRenderer/simpleRenderer', [
            'header' => Yii::t('StatisticsUpdateModule.statistics', 'Период наблюдения (в месяцах)'),
            'data'   => $observationPeriodStatistics
        ]);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('StatisticsUpdateModule.statistics', 'NYHA класс СН'),
            'data'   => $nyhaStatistics
        ]);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('StatisticsUpdateModule.statistics', 'Нарушения ритма и проводимости'),
            'data'   => $rhythmDisturbancesStatistics
        ]);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('StatisticsUpdateModule.statistics', 'Этиология'),
            'data'   => $etiologyStatistics
        ]);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('StatisticsUpdateModule.statistics', 'Препараты, принимаемые к моменту включения в исследование'),
            'data'   => $preparationsStatistics
        ]);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('StatisticsUpdateModule.statistics', 'Операции на сердце'),
            'data'   => $heartSurgeryStatistics
        ]);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('StatisticsUpdateModule.statistics', 'Серьезность нежелательного явления'),
            'data'   => $seriousAdverseEvents
        ]);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('StatisticsUpdateModule.statistics', 'ЭКС'),
            'data'   => $eksStatistics
        ]);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('StatisticsUpdateModule.statistics', 'ИКД, CRT'),
            'data'   => $ikdCrtStatistics
        ]);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('StatisticsUpdateModule.statistics', 'Имплантация стимулирующей системы'),
            'data'   => $implantationStimulatingStatistics
        ]);
        echo $this->renderPartial('../reportRenderer/supraventricularArrhythmiasRenderer', [
            'header' => Yii::t('StatisticsUpdateModule.statistics', 'Наджелудочковые аритмии (по пациентам ИКД)'),
            'column1Header' => Yii::t('StatisticsUpdateModule.statistics', 'Количество пациентов'),
            'data'   => $trendMonitoringStatistics['ikdStats']
        ]);
        echo $this->renderPartial('../reportRenderer/supraventricularArrhythmiasRenderer', [
            'header' => Yii::t('StatisticsUpdateModule.statistics', 'Наджелудочковые аритмии (по всем пациентам)'),
            'column1Header' => Yii::t('StatisticsUpdateModule.statistics', 'Количество пациентов'),
            'data'   => $trendMonitoringStatistics['allStats']
        ]);
        echo $this->renderPartial('../reportRenderer/deviationShortByValueRenderer', [
            'header' => Yii::t('StatisticsUpdateModule.statistics', 'Желудочковые нарушения ритма'),
            'column1Header' => Yii::t('StatisticsUpdateModule.statistics', 'Количественное значение'),
            'data'   => $ventricularArrhythmiasStatistics
        ]);
        echo $this->renderPartial('../reportRenderer/countershockRenderer', [
            'header'        => Yii::t('StatisticsUpdateModule.statistics', 'Электроимпульсная терапия'),
            'data'          => $countershockStatistics
        ]);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('StatisticsUpdateModule.statistics', 'Нежелательные явления'),
            'data'   => $adverseEventsStatistics
        ]);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('StatisticsUpdateModule.statistics', 'Связь нежелательного явления с имплантатом'),
            'data'   => $adverseImplantStatistics
        ]);
        echo $this->renderPartial('../reportRenderer/simpleRenderer', [
            'header' => Yii::t('StatisticsUpdateModule.statistics', 'Нанесенные шоковые разряды'),
            'data'   => $shockDischargesStatistics
        ]);
?>
        <h5><?= Yii::t('StatisticsUpdateModule.statistics', 'Опции Home Monitoring') ?></h5>
<?php
        echo $this->renderPartial('../reportRenderer/homeMonitoringChangeOptionsRenderer', [
            'data'   => $homeMonitoringStatistics
        ]);
        echo $this->renderPartial('../reportRenderer/simpleRenderer', [
            'header' => '',
            'data'   => $homeMonitoringStatisticsPart2
        ]);

        echo '<h5>' . Yii::t('StatisticsModule.statistics', 'Оценка функций (экономия времени)') . '</h5>';
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header'        => '',
            'parameterName' => Yii::t('StatisticsUpdateModule.statistics', 'Home Monitoring Service Center'),
            'data'          => $functionRatingsStatistics['rtgTimeHmsc']
        ]);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header'        => '',
            'parameterName' => Yii::t('StatisticsUpdateModule.statistics', 'Концепция "Светофор"'),
            'data'          => $functionRatingsStatistics['rtgTimeSemaphore']
        ]);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header'        => '',
            'parameterName' => Yii::t('StatisticsUpdateModule.statistics', 'IEGM online'),
            'data'          => $functionRatingsStatistics['rtgTimeIegm']
        ]);

        echo '<h5>' . Yii::t('StatisticsModule.statistics', 'Оценка функций (клиника)') . '</h5>';
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header'        => '',
            'parameterName' => Yii::t('StatisticsUpdateModule.statistics', 'Home Monitoring Service Center'),
            'data'          => $functionRatingsStatistics['rtgClHmsc']
        ]);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header'        => '',
            'parameterName' => Yii::t('StatisticsUpdateModule.statistics', 'Концепция "Светофор"'),
            'data'          => $functionRatingsStatistics['rtgClSemaphore']
        ]);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header'        => '',
            'parameterName' => Yii::t('StatisticsUpdateModule.statistics', 'IEGM online'),
            'data'          => $functionRatingsStatistics['rtgClIegm']
        ]);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header'        => '',
            'parameterName' => Yii::t('StatisticsUpdateModule.statistics', 'Достаточность данных HM'),
            'data'          => $functionRatingsStatistics['rtgClDataSuff']
        ]);
        echo $this->renderPartial('../reportRenderer/dispersionByValueRenderer', [
            'header' => Yii::t('StatisticsUpdateModule.statistics', 'Имплантат, связанный с нежелательным явлением'),
            'data'   => $implantWithAdverseEventStatistics
        ]);
        echo $this->renderPartial('../reportRenderer/deviationShortByValueRenderer', [
            'header'        => Yii::t('StatisticsUpdateModule.statistics', 'Общее количество посещений клиники'),
            'column1Header' => Yii::t('StatisticsUpdateModule.statistics', 'Среднее количество'),
            'data'          => $totalNumberVisitStatistics
        ]);
        echo $this->renderPartial('../reportRenderer/simpleRenderer', [
            'header'        => Yii::t('StatisticsUpdateModule.statistics', 'Количество посещений клиники'),
            'data'          => $visitsNumberStatistics
        ]);
        echo $this->renderPartial('../reportRenderer/deviationShortByValueRenderer', [
            'header'        => Yii::t('StatisticsUpdateModule.statistics', 'Оценка клинической нагрузки'),
            'column1Header' => Yii::t('StatisticsUpdateModule.statistics', 'Среднее количество'),
            'data'          => $clinicLoadStatistics
        ]);
        echo $this->renderPartial('../reportRenderer/simpleRenderer', [
            'header'        => Yii::t('StatisticsUpdateModule.statistics', 'Количество дней до 1 терапии'),
            'data'          => $daysBeforeTherapyStatistics
        ]);

    }
}
?>

