<?php

class TaskModule extends CWebModule
{
    public function init()
    {
        Yii::setPathOfAlias('task', dirname(__FILE__));
        $this->setImport(
            array(
                'task.components.*',
                'task.models.*',
            )
        );
    }
}
