<?php
/* @var $this SettingController */
/* @var $model CronTask */
/* @var $form I18nActiveForm */
?>
<h2 class="offset1">
    <?=Yii::t("AdminModule.messages", "Основные параметры")?>
</h2>

<div class="form">

    <?php $form = $this->beginWidget('I18nActiveForm', array(
        'id' => 'cron-task-form',
        'enableAjaxValidation' => false,
        'stateful' => true
    )); ?>

    <?php if (count($model->parameters) > 0) { ?>
        <p class="note controls">
            <?=Yii::t("AdminModule.messages", "Поля отмеченные");?>
            <span class="required">"*"</span>
            <?=Yii::t("AdminModule.messages", "обязательны для заполнения");?>.
        </p>
    <?php } else { ?>
        <p class="note controls text-info">
            <?=Yii::t("AdminModule.messages", "У данной задачи нет настраеваемых параметров.");?>
        </p>
    <?php } ?>

    <?php foreach($model->parameters as $parameter) { ?>
        <div class="control-group">
            <?=$form->labelEx($parameter, 'value', ['class' => 'control-label']); ?>
            <div class="controls">
                <?php
                    if ($parameter->dataList !== false) {
                        echo $form->dropDownList(
                            $parameter,
                            "[{$parameter->name}]value",
                            $parameter->dataList,
                            [
                                'empty' => Yii::t("DirectoryModule.messages", 'Выберите значение'),
                                'class' => 'input-xlarge'
                            ]
                        );
                    } else {
                        echo $form->textField($parameter, "[{$parameter->name}]value", ['class' => 'input-xlarge']);
                    }
                ?>
                <?=$form->error($parameter, "[{$parameter->name}]value");?>
                <div class="text-info"><?=$parameter->description;?></div>
            </div>
        </div>
    <?php } ?>

    <?=CHtml::link(
        Yii::t('AdminModule.messages', 'Отмена'),
        $this->createUrl('/task/backend/setting/index'),
        ['class' => 'btn']);
    ?>
    <?=CHtml::submitButton(
        '< ' . Yii::t('AdminModule.messages', 'Изменить настройки'),
        ['class' => 'btn btn-primary', 'name' => 'choiceTask']);
    ?>
    <?=CHtml::submitButton(
        Yii::t('AdminModule.messages', 'Создать расписание') . ' >',
        ['class' => 'btn btn-primary', 'name' => 'finish']);
    ?>
    </div>
    <?php $this->endWidget(); ?>
</div><!-- form -->