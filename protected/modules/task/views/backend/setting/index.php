<?php
/* @var $this SettingController */
/* @var $model CronTask */
/* @var $data CronTask */
?>
<?php
    $commandIdentityList = $model->getCommandIdentityList();
    $this->widget('GridView', array(
    'id' => 'cron-setting-grid',
    'dataProvider' => $model->search(),
    'nullDisplay' => '---',
    'columns' => array(
        [
            "header" => Yii::t("TaskModule.messages", "№"),
            "value" => '$data->uid',
        ],
        [
            "header" => Yii::t("TaskModule.messages", "Название"),
            "value" => function ($data) use ($commandIdentityList) {
                return $commandIdentityList[$data->getCommandIdentity()];
            },
        ],
        [
            "header" => Yii::t("TaskModule.messages", "Команда"),
            "value" => '$data->command',
        ],
        [
            "header" => Yii::t("TaskModule.messages", "Действие"),
            "value" => '$data->action',
        ],
        [
            "header" => Yii::t("TaskModule.messages", "Параметры"),
            "type" => "html",
            'htmlOptions' => ['style' => 'text-align: center;'],
            "value" => function ($data) {
                    $content = null;
                    if ($data->parameters) {
                        $content = '<ul>';
                        foreach ($data->parameters as $parameter) {
                            if (!is_null($parameter->value)) {
                                $content .= '<li style="list-style-type: none;">' . $parameter->name .'&nbsp;=&nbsp;' . $parameter->value . '</li>';
                            }
                        }
                        $content .= '</ul>';
                    } else {

                    }
                    return $content;
                },
        ],
        [
            "header" => Yii::t("TaskModule.messages", "Тип"),
            "value" => function ($data) {
                return is_null($data->repeat) ? Yii::t("TaskModule.messages", "Регулярно") : Yii::t("TaskModule.messages", "Разовый запуск");
            },
        ],
        [
            "header" => Yii::t("TaskModule.messages", "Максимальное количество перезапусков"),
            "value" => '$data->retries',
        ],
        [
            "header" => Yii::t("TaskModule.messages", "Приоритет"),
            "value" => function ($data) {
                return CronTaskPriority::getList()[$data->priority];
            },
        ],
        [
            "header" => Yii::t("TaskModule.messages", "Расписание CRON"),
            "value" => '$data->cron_expression',
        ],
        [
            "header" => Yii::t("TaskModule.messages", "Время следующего запуска"),
            "value" => function ($data) {
                    return $data->next_execution_time instanceof RhDateTime ? $data->next_execution_time->format('d.m.Y H:i') : '---';
                },
        ],
        [
            "header" => Yii::t("TaskModule.messages", "Акт."),
            'type' => 'raw',
            'htmlOptions' => ['style' => 'text-align: center;'],
            "value" => '$data->active ? "Да" : "Нет"',
        ],
        array(
            'class'=>'CButtonColumn',
            'template' => '{update} {history} {delete} {restore}',
            'header' => CHtml::dropDownList(
                'pageSize',
                Yii::app()->user->getState('pageSize'),
                [100 => 100, 20 => 20, 50 => 50],
                [
                    'onchange' => "$.fn.yiiGridView.update('cron-setting-grid',{ data:{pageSize: $(this).val() }})",
                    'style' => 'width: 70px;'
                ]
            ),
            'buttons' => [
                'delete' => [
                    'visible' => '$data->active;'
                ],
                'restore' => [
                    'label' => Yii::t("TaskModule.messages", "Запустить в работу"),
                    'imageUrl' => '/images/restore.png',
                    'url' => 'Yii::app()->controller->createUrl("/task/backend/setting/restore", ["id" => $data->uid])',
                    'visible' => '!$data->active;'
                ],
                'history' => [
                    'label' => Yii::t("TaskModule.messages", "История"),
                    'imageUrl' => '/images/list.png',
                    'url' => 'Yii::app()->controller->createUrl("/task/backend/setting/history", ["taskId" => $data->uid])',
                ]
            ]
        ),
    )
));?>

<?=CHtml::link(
    CHtml::tag('i', ['class' => 'icon-plus-sign'], ' ') . Yii::t("TaskModule.messages", "Создать"),
    $this->createUrl('/task/backend/setting/create'),
    ['class' => 'btn btn-primary', 'style' => 'float:right;']
);
