<?php
/* @var $this SettingController */
/* @var $model CronTask */
/* @var $form I18nActiveForm */
Yii::app()->getClientScript()->registerScriptFile(
    Yii::app()->assetManager->publish(
        Yii::getPathOfAlias('task.assets').'/jquery-cron.js'
    ),
    CClientScript::POS_END
);

Yii::app()->clientScript->registerCssFile(
    Yii::app()->assetManager->publish(
        Yii::getPathOfAlias('task.assets').'/jquery-cron.css'
    )
);

Yii::app()->getClientScript()->registerScript(
    'cron-expression',
    '$(function () {
        var initCronValue = $("#CronTask_cron_expression").val();
        var initRepeatValue = $("#CronTask_repeat").val();
        if (initRepeatValue == 1) {
            initCronValue = "*";
        }
        var cronWidget = $("#cron_expression_widget").cron({
            initial: (initCronValue ? initCronValue : "*"),
            customValues: {
                "Разовая" : "*"
            },
            onChange: function() {
                var newValue = $(this).cron("value");
                if (newValue == "*") {
                    $("#CronTask_cron_expression").val("* * * * *");
                    $("#CronTask_repeat").val(1);
                } else {
                    $("#CronTask_cron_expression").val(newValue);
                    $("#CronTask_repeat").val("");
                }
            }
        });
        if (cronWidget.cron("value") == "*") {
            $("#CronTask_cron_expression").val("* * * * *");
        }
    });',
    CClientScript::POS_END
);
?>
<h2 class="offset1">
    <?=Yii::t("AdminModule.messages", "Основные параметры")?>
</h2>

<div class="form">

    <?php $form = $this->beginWidget('I18nActiveForm', array(
        'id' => 'cron-task-form',
        'enableAjaxValidation' => false,
        'stateful' => true
    )); ?>

    <p class="note controls">
        <?=Yii::t("AdminModule.messages", "Поля отмеченные");?>
        <span class="required">"*"</span>
        <?=Yii::t("AdminModule.messages", "обязательны для заполнения");?>.
    </p>

    <div class="controls">
        <?=$form->errorSummary($model); ?>
    </div>

    <div class="control-group">
        <?=$form->labelEx($model, 'commandIdentity', ['class' => 'control-label']); ?>
        <div class="controls">
            <?=$form->dropDownList(
                $model,
                'commandIdentity',
                $model->getCommandIdentityList(),
                [
                    'empty' => Yii::t("DirectoryModule.messages", 'Выберите задачу'),
                    'class' => 'input-xlarge'
                ]
            ); ?>
            <?=$form->error($model, 'commandIdentity');?>
        </div>
    </div>

    <div class="control-group">
        <?=$form->labelEx($model, 'retries', ['class' => 'control-label']); ?>
        <div class="controls">
            <?=$form->textField($model, 'retries', ['class' => 'input-xlarge']); ?>
            <?=$form->error($model, 'retries');?>
        </div>
    </div>

    <div class="control-group">
        <?=$form->labelEx($model, 'priority', ['class' => 'control-label']); ?>
        <div class="controls">
            <?=$form->dropDownList(
                $model,
                'priority',
                CronTaskPriority::getList(),
                [
                    'empty' => Yii::t("DirectoryModule.messages", 'Выберите значение'),
                    'class' => 'input-xlarge'
                ]
            ); ?>
            <?=$form->error($model, 'priority');?>
        </div>
    </div>

    <div class="control-group">
        <label for="CronTask_cron_expression" class="control-label">Расписание</label>
        <div class="controls">
            <div id='cron_expression_widget'></div>
        </div>
    </div>
    <div class="control-group">
        <?=$form->labelEx($model, 'cron_expression', ['class' => 'control-label']); ?>
        <div class="controls">
            <?=$form->textField($model, 'cron_expression', ['class' => 'input-xlarge']); ?>
            <?=$form->error($model, 'cron_expression');?>
        </div>
    </div>

    <?=$form->hiddenField($model, 'repeat'); ?>

    <?=CHtml::link(
        Yii::t('AdminModule.messages', 'Отмена'),
        $this->createUrl('/task/backend/setting/index'),
        ['class' => 'btn']);
    ?>
    <?=CHtml::submitButton(
        Yii::t('AdminModule.messages', 'Задать параметры') . ' >',
        ['class' => 'btn btn-primary', 'name' => 'setParameters']);
    ?>
    </div>
    <?php $this->endWidget(); ?>
</div><!-- form -->