<?php
/* @var $this SettingController */
/* @var $model CronTaskStatus */
/* @var $data CronTaskStatus */
?>
<h2>История выполнения задачи №<?=$taskId;?></h2>
<?php
    $this->widget('GridView', array(
    'id' => 'cron-history-grid',
    'dataProvider' => $model->search(),
    'nullDisplay' => '---',
    'columns' => array(
        [
            "header" => Yii::t("TaskModule.messages", "№"),
            "value" => '$data->uid',
        ],
        [
            "header" => Yii::t("TaskModule.messages", "Статус"),
            "value" => function ($data){
                return CronTaskStatus::getList()[$data->status_id];
            },
        ],
        [
            "header" => Yii::t("TaskModule.messages", "Дата"),
            "value" => '$data->create_time->format(RhDateTime::RH_DATETIME)',
        ],
        [
            'class'=>'CButtonColumn',
            'template' => '<i class="icon-ok"></i>',
            'header' => CHtml::dropDownList(
                    'pageSize',
                    Yii::app()->user->getState('pageSize'),
                    [ 10 => 10, 20 => 20, 50 => 50, 5000 => 5000 ],
                    [
                        'onchange' => "$.fn.yiiGridView.update('cron-history-grid',{ data:{pageSize: $(this).val() }})",
                        'style' => 'width: 70px;'
                    ]
                ),
        ],
    )
));?>

<?=CHtml::link(
    Yii::t("TaskModule.messages", "Вернуться к расписанию"),
    $this->createUrl('/task/backend/setting/index'),
    ['class' => 'btn', 'style' => 'float:right;']
);
