<?php
Yii::import('admin.AdminModule');

class SettingController extends CController
{
    public $layout='//layouts/admin';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = [];

    protected $pageTitlePrefix = "Панель администратора";

    protected $pageTitle = 'Задачи';

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return [
            [
                'allow',
                'roles' => ['taskManagement']
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

    public function actionIndex()
    {
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']);
        }

        $model = new CronTask('search');
        $this->render('index', ['model' => $model]);
    }

    public function actionHistory($taskId)
    {
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']);
        }

        $model = new CronTaskStatus('search');
        $model->cron_task_id = $taskId;
        $this->render('history', ['model' => $model, 'taskId' => $taskId]);
    }

    public function actionCreate()
    {
        $model = new CronTask();
        $this->edit($model);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $this->edit($model);
    }

    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();
        if(!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
    }

    public function actionRestore($id)
    {
        $model = $this->loadModel($id);
        $model->restore();

        if(!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
    }

    protected function edit(CronTask $model)
    {
        if (isset($_POST['choiceTask'])) {
            $model->attributes = $this->getPageState('choiceTask', []);
            $this->render('choiceTask', ['model' => $model]);
        } elseif (isset($_POST['setParameters'])) {
            $model->setScenario('choiceTask');
            $this->setPageState('choiceTask',$_POST['CronTask']);
            $model->attributes = $_POST['CronTask'];
            if($model->validate()) {
                $model->parameters = TaskManager::getCommandParameters($model->command, $model->action, $model->parameters);
                $this->render('setParameters', ['model' => $model]);
            } else {
                $this->render('choiceTask', ['model' => $model]);
            }
        } elseif (isset($_POST['finish'])) {
            $model->attributes = $this->getPageState('choiceTask', []);
            $parametersValues = isset($_POST['CronTaskParameter']) ? $_POST['CronTaskParameter'] : [];
            $result = $this->editParameters($model, $parametersValues);
            if ($result && $model->save()) {
                $this->redirect(['index']);
            } else {
                $this->render('setParameters', ['model' => $model]);
            }
        } else {
            $this->render('choiceTask', ['model' => $model]);
        }
    }

    protected function editParameters(CronTask $model, $values)
    {
        $existParameters = $model->parameters;
        foreach ($values as $name => $value) {
            if (isset($existParameters[$name])) {
                $existParameters[$name]->value = $value['value'];
            } else {
                $cronParameter = new CronTaskParameter();
                $cronParameter->name = $name;
                $cronParameter->value = $value['value'];
                $existParameters[$name] = $cronParameter;
            }
        }
        $valid = true;
        $existParameters = TaskManager::getCommandParameters($model->command, $model->action, $existParameters);
        foreach($existParameters as $existParameter) {
            $valid = $existParameter->validate() && $valid;
        }
        $model->parameters = $existParameters;
        return $valid;
    }

    public function  getPageTitle()
    {
        $pageTitle = Yii::t('AdminModule.messages', $this->pageTitlePrefix);
        if (!empty($this->pageTitle)) {
            $pageTitle .= ' - ' . Yii::t(ucfirst(Yii::app()->controller->module->id) . 'Module.messages', $this->pageTitle);
        }
        return $pageTitle;
    }

    public function loadModel($id)
    {
        $model = CronTask::model()->findByPk($id);
        if($model===null) {
            throw new CHttpException(404,'The requested page does not exist.');
        }
        return $model;
    }
}
