<?php

/**
 * This is the model class for table "{{cron_task}}".
 *
 * The followings are the available columns in table '{{cron_task}}':
 * @property integer $uid
 * @property string $command
 * @property string $action
 * @property integer $repeat
 * @property integer $retries
 * @property integer $priority
 * @property string $cron_expression
 * @property RhDateTime $next_execution_time
 * @property RhDateTime $update_time
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property CronTaskStatus[] $statuses
 * @property CronTaskParameter[] $parameters
 */
class CronTask extends CActiveRecord
{
    const COMMAND_IDENTITY_DELIMITER = ':';

    private $isReachedFailLimit;

    public function setCommandIdentity($identity)
    {
        if (!empty($identity)) {
            list($commandName, $actionName) = explode(self::COMMAND_IDENTITY_DELIMITER, $identity);
            $this->command = $commandName;
            $this->action = $actionName;
        }
    }

    public function getCommandIdentity()
    {
        $commandIdentity = null;
        if (!empty($this->command) && !empty($this->action)) {
            $commandIdentity = implode(self::COMMAND_IDENTITY_DELIMITER, [$this->command, $this->action]);
        }
        return $commandIdentity;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{cron_task}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('retries, priority', 'required'),
            array('commandIdentity', 'required', 'on' => 'choiceTask'),
            array('commandIdentity, command, action', 'required', 'on' => 'insert, update'),
            array('repeat, retries', 'numerical', 'allowEmpty' => true, 'integerOnly' => true, 'min' => 0),
            array('command, action', 'length', 'max' => 100),
            array('cron_expression', 'length', 'max' => 250),
            // The following rule is used by search().
            array(
                'uid, command, action, repeat, retries, priority, cron_expression, next_execution_time, update_time, active',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'statuses'   => array(self::HAS_MANY, 'CronTaskStatus', 'cron_task_id'),
            'parameters' => array(self::HAS_MANY, 'CronTaskParameter', 'cron_task_id', 'index' => 'name'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid'                 => 'Идентификатор',
            'commandIdentity'     => 'Название задачи',
            'command'             => 'Консольная команда',
            'action'              => 'Действие',
            'repeat'              => 'Количество выполнения задачи',
            'retries'             => 'Количество повторов при ошибке',
            'priority'            => 'Приоритет',
            'cron_expression'     => 'Расписание в формате CRON',
            'next_execution_time' => 'Дата выполнения',
            'update_time'         => 'Update Time',
            'active'              => 'Active',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        return new CActiveDataProvider(
            $this,
            array(
                'criteria'   => new CDbCriteria(['order' => 'uid ASC']),
                'pagination' => [
                    'pageSize' => Yii::app()->user->getState('pageSize')
                ]
            )
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CronTask the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        return [
            'RhDateTimeBehavior'  => [
                'class' => 'application.extensions.RhDateTimeBehavior'
            ],
            'EAdvancedArBehavior' => [
                'class' => 'application.extensions.EAdvancedArBehavior'
            ]
        ];
    }

    public function getCommandIdentityList()
    {
        $fullCommandList = TaskManager::getCronCommandList();
        $resultList = [];
        foreach ($fullCommandList as $commandName => $actionArray) {
            foreach ($actionArray as $actionName => $label) {
                $resultList[$commandName . self::COMMAND_IDENTITY_DELIMITER . $actionName] = $label;
            }
        }
        return $resultList;
    }

    public function delete()
    {
        if (!$this->getIsNewRecord()) {
            if ($this->beforeDelete()) {
                $result = $this->updateByPk($this->getPrimaryKey(), ['active' => 0]) == 1;
                $this->afterDelete();
            } else {
                $result = false;
            }
        } else {
            $result = false;
        }
        return $result;
    }

    public function restore()
    {
        if (!$this->getIsNewRecord()) {
            $this->active = 1;
            $this->save();
        }
    }

    public function getNextTask()
    {
        return $this->_getNextTask(CronTaskPriority::LEVEL_LOW);
    }

    public function getNextCriticalTask()
    {
        return $this->_getNextTask(CronTaskPriority::LEVEL_IMMEDIATE);
    }

    public function getConsoleCommand()
    {
        $consoleCommandParts = [$this->command, $this->action];
        foreach ($this->parameters as $parameter) {
            if (!is_null($parameter->value)) {
                $consoleCommandParts[] = '--' . $parameter->name . "='" . $parameter->value . "'";
            }
        }
        return implode(' ', $consoleCommandParts);
    }

    public function setInProgressStatus($pid)
    {
        $this->_createNewStatus($pid, CronTaskStatus::STATUS_IN_PROGRESS);
    }

    public function setFailStatus($pid)
    {
        if (!$this->_isReachedFailLimit()) {
            $pauseTime = CronTaskStatus::PAUSE_AFTER_FAIL;
            $currentTime = new RhDateTime();
            $nextExecutionTime = $currentTime->modify("+{$pauseTime} seconds");
            $this->updateByPk(
                $this->uid,
                ['next_execution_time' => $nextExecutionTime->format(RhDateTime::DB_DATETIME)]
            );
        }
        $this->_createNewStatus($pid, CronTaskStatus::STATUS_FAIL);
        if ($this->_isReachedFailLimit()) {
            $this->_createNewStatus($pid, CronTaskStatus::STATUS_SKIP);
            $this->_deactivateTimeTask();
            $this->save();
        }
        Yii::app()->mailer->send('Ошибка при выполнении задачи №' . $this->uid, 'Сбой в системе регулярных задач.');
    }

    public function setDoneStatus($pid)
    {
        $this->_createNewStatus($pid, CronTaskStatus::STATUS_DONE);
        $this->_deactivateTimeTask();
        $this->save();
    }

    protected function beforeSave()
    {
        if ($this->getIsNewRecord()) {
            $this->active = 1;
        }
        $this->calculatingNextExecutionTime();
        $this->update_time = new RhDateTime();
        return parent::beforeSave();
    }

    protected function calculatingNextExecutionTime()
    {
        if (!$this->next_execution_time instanceof RhDateTime) {
            $this->next_execution_time = new RhDateTime();
        }
        $cron = Cron\CronExpression::factory($this->cron_expression);
        $cron->isDue();
        $iteration = 0;
        while (($nextDateTime = $cron->getNextRunDate('now', $iteration)) < $this->next_execution_time) {
            $iteration++;
        }
        if ($this->getIsNewRecord() && $this->repeat == 1) {
            $nextDateTime->modify("-2 minutes");
        }
        $this->next_execution_time = new RhDateTime($nextDateTime->format(RhDateTime::DB_DATETIME));
    }

    protected function afterSave()
    {
        parent::afterSave();
        foreach ($this->parameters as $parameter) {
            $parameter->cron_task_id = $this->uid;
            $parameter->save();
        }
    }

    private function _getNextTask($minPriority = CronTaskPriority::LEVEL_LOW)
    {
        $sql = 'SELECT ct.* FROM {{cron_task}} AS ct LEFT JOIN
            (SELECT cts_temp.* FROM {{cron_task_status}} AS cts_temp
                GROUP BY cts_temp.cron_task_id ORDER BY cts_temp.create_time DESC LIMIT 1) AS cts ON cts.cron_task_id = ct.uid
            WHERE ct.active = :active AND ct.next_execution_time <= :current_time AND ct.priority >= :priority
            AND (
                cts.status_id IS NULL
                OR cts.status_id = :done_status_id
                OR (cts.status_id = :fail_status_id AND
                    (SELECT COUNT(*) FROM {{cron_task_status}} AS cts_temp2 WHERE cts_temp2.cron_task_id = ct.uid
                        AND cts_temp2.cts.status_id = :fail_status_id
                        AND cts_temp2.create_time > (
                            SELECT COALESCE(MAX(cts_temp3.create_time), "0000-00-00 00:00:00") FROM {{cron_task_status}} AS cts_temp3 WHERE cts_temp3.status_id IN (:done_status_id, :skip_status_id) AND cts_temp3.cron_task_id = ct.uid
                        )) <= ct.retries)
            )
            ORDER BY ct.next_execution_time ASC, ct.priority DESC  LIMIT 1';

        $parameters = [
            'active'         => 1,
            'priority'       => $minPriority,
            'current_time'   => date(RhDateTime::DB_DATETIME),
            'done_status_id' => CronTaskStatus::STATUS_DONE,
            'fail_status_id' => CronTaskStatus::STATUS_FAIL,
            'skip_status_id' => CronTaskStatus::STATUS_SKIP
        ];
        return $this->findBySql($sql, $parameters);
    }

    private function _isReachedFailLimit()
    {
        if (is_null($this->isReachedFailLimit)) {
            $sql = 'SELECT COUNT(*) FROM {{cron_task_status}} AS cts_1 WHERE cts_1.cron_task_id = :cron_task_id
                AND cts_1.status_id = :fail_status_id
                AND cts_1.create_time > (
                    SELECT COALESCE(MAX(cts_2.create_time), "0000-00-00 00:00:00") FROM {{cron_task_status}} AS cts_2
                        WHERE cts_2.status_id IN (:done_status_id, :skip_status_id) AND cts_2.cron_task_id = :cron_task_id
                )';
            $command = Yii::app()->db->createCommand($sql);
            $command->bindValue(":cron_task_id", $this->uid, PDO::PARAM_INT);
            $command->bindValue(":fail_status_id", CronTaskStatus::STATUS_FAIL, PDO::PARAM_INT);
            $command->bindValue(":done_status_id", CronTaskStatus::STATUS_DONE, PDO::PARAM_INT);
            $command->bindValue(":skip_status_id", CronTaskStatus::STATUS_SKIP, PDO::PARAM_INT);
            $failsCounter = $command->queryScalar();
            $this->isReachedFailLimit = $failsCounter >= $this->retries;
        }
        return $this->isReachedFailLimit;
    }

    private function _createNewStatus($pid, $statusId)
    {
        $newStatus = new CronTaskStatus();
        $newStatus->status_id = $statusId;
        $newStatus->cron_task_id = $this->uid;
        $newStatus->pid = $pid;
        $newStatus->save();
    }

    private function _deactivateTimeTask()
    {
        if ($this->repeat == 1) {
            $this->active = 0;
        }
    }
}
