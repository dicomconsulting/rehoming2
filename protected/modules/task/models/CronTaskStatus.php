<?php

/**
 * This is the model class for table "{{cron_task_status}}".
 *
 * The followings are the available columns in table '{{cron_task_status}}':
 * @property integer $uid
 * @property integer $cron_task_id
 * @property integer $status_id
 * @property integer $pid
 * @property string $create_time
 *
 * The followings are the available model relations:
 * @property CronTask $cronTask
 */
class CronTaskStatus extends CActiveRecord
{
    const STATUS_IN_PROGRESS = 1;

    const STATUS_FAIL = 2;

    const STATUS_DONE = 3;

    const STATUS_SKIP = 4;

    const PAUSE_AFTER_FAIL = 30;

    public static function getList()
    {
        return [
            self::STATUS_IN_PROGRESS => Yii::t('TaskModule.messages', 'В работе'),
            self::STATUS_FAIL => Yii::t('TaskModule.messages', 'Перезапуск'),
            self::STATUS_DONE => Yii::t('TaskModule.messages', 'Выполнена'),
            self::STATUS_SKIP => Yii::t('TaskModule.messages', 'Пропущена')
        ];
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{cron_task_status}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cron_task_id', 'required'),
			array('cron_task_id, status_id, pid', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('uid, cron_task_id, status_id, pid, create_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cronTask' => array(self::BELONGS_TO, 'CronTask', 'cron_task_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'uid' => 'Uid',
			'cron_task_id' => 'Cron Task',
			'status_id' => 'Status',
			'pid' => 'Номер процесса в котором был создан статус',
			'create_time' => 'Create Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

//		$criteria->compare('uid',$this->uid);
		$criteria->compare('cron_task_id',$this->cron_task_id);
//		$criteria->compare('status_id',$this->status_id);
//		$criteria->compare('pid',$this->pid);
//		$criteria->compare('create_time',$this->create_time,true);

		return new CActiveDataProvider(
            $this,
            array(
			'criteria' => $criteria,
            'pagination'=> [
                'pageSize' => Yii::app()->user->getState('pageSize')
            ]
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CronTaskStatus the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function behaviors()
    {
        return [
            'RhDateTimeBehavior' => [
                'class' => 'application.extensions.RhDateTimeBehavior'
            ]
        ];
    }

    protected function beforeSave()
    {
        $this->create_time = new RhDateTime();
        return parent::beforeSave();
    }
}
