<?php

/**
 * This is the model class for table "{{cron_task_parameter}}".
 *
 * The followings are the available columns in table '{{cron_task_parameter}}':
 * @property integer $uid
 * @property integer $cron_task_id
 * @property string $name
 * @property string $value
 *
 * The followings are the available model relations:
 * @property CronTask $cronTask
 */
class CronTaskParameter extends CActiveRecord
{
    public $label;

    public $description;

    public $dataList = false;

    public $required = false;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{cron_task_parameter}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
        $rules = [['value', 'safe']];
        if ($this->required) {
            $rules[] = ['value', 'required'];
        }
        if ($this->dataList !== false) {
            $rules[] = ['value', 'in', 'range' => array_keys($this->dataList), 'allowEmpty' => !$this->required];
        }
		return $rules;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cronTask' => array(self::BELONGS_TO, 'CronTask', 'cron_task_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'uid' => 'Uid',
			'cron_task_id' => 'Cron Task',
			'name' => 'Parameter name',
			'value' => $this->label
		);
	}

    protected function beforeSave()
	{

        if (strlen($this->value) == 0) {
            $this->value = null;
        }
        return parent::beforeSave();
    }

    /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('uid',$this->uid);
		$criteria->compare('cron_task_id',$this->cron_task_id);
		$criteria->compare('status_id',$this->status_id);
		$criteria->compare('pid',$this->pid);
		$criteria->compare('create_time',$this->create_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CronTaskParameter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
