<?php

class CronTaskPriority
{
    const LEVEL_LOW = 0;

    const LEVEL_MEDIUM = 3;

    const LEVEL_HIGH = 6;

    const LEVEL_IMMEDIATE = 9;

    public static function getList()
    {
        return [
            self::LEVEL_LOW => Yii::t('TaskModule.messages', 'Низкий'),
            self::LEVEL_MEDIUM => Yii::t('TaskModule.messages', 'Средний'),
            self::LEVEL_HIGH => Yii::t('TaskModule.messages', 'Высокий'),
            self::LEVEL_IMMEDIATE => Yii::t('TaskModule.messages', 'Немедленный')
        ];
    }
}
