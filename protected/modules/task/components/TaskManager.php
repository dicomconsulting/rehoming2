<?php

use Minime\Annotations\Facade;

class TaskManager
{
    const ACTION_PREFIX = 'action';

    public static function getCronCommandList()
    {
        $consoleConfig = self::loadConsoleConfig();
        $cronReadyCommandArray = [];
        foreach ($consoleConfig['commandMap'] as $commandName => $commandConfig) {
            Yii::import($commandConfig['class']);
            $className = substr($commandConfig['class'], strrpos($commandConfig['class'], '.') + 1);
            $annotations = Facade::getClassAnnotations($className);
            if ($annotations->get('cron-ready')) {
                $cronReadyCommandArray[$commandName] = [];
                $class = new ReflectionClass($className);
                foreach($class->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
                    if(!strncasecmp($method->getName(), self::ACTION_PREFIX, 6) && strlen($method->getName()) > 6) {
                        $annotations = Facade::getMethodAnnotations($className, $method->getName());
                        if ($method->getDocComment() !== false && $annotations->get('cron-name')) {
                            $name = lcfirst(substr($method->getName(), 6));
                            $cronReadyCommandArray[$commandName][$name] = $annotations->get('cron-name');
                        }
                    }
                }
                if (empty($cronReadyCommandArray[$commandName])) {
                    unset($cronReadyCommandArray[$commandName]);
                }
            }
        }
        return $cronReadyCommandArray;
    }

    public static function getCommandParameters($commandName, $actionName, $allowParameterArray = [])
    {
        $consoleConfig = self::loadConsoleConfig();
        $classAlias = $consoleConfig['commandMap'][$commandName]['class'];
        Yii::import($classAlias);
        $className = substr($classAlias, strrpos($classAlias, '.') + 1);
        $methodName = self::ACTION_PREFIX . ucfirst($actionName);
        $method = new ReflectionMethod($className, $methodName);
        if ($method->getNumberOfParameters() > 0) {
            $annotations = Facade::getMethodAnnotations($className, $method->getName());
            $parameterArray = $method->getParameters();
            foreach ($parameterArray as $parameter) {
                if (!isset($allowParameterArray[$parameter->getName()])) {
                    $updateParameter = new CronTaskParameter();
                    $updateParameter->name = $parameter->getName();
                    if ($parameter->isOptional()) {
                        $updateParameter->value = $parameter->getDefaultValue();
                    }
                } else {
                    $updateParameter = $allowParameterArray[$parameter->getName()];
                }

                if ($parameter->isOptional()) {
                    $updateParameter->required = false;
                } else {
                    $updateParameter->required = true;
                }
                if ($label = $annotations->get('cron-' . $parameter->getName() . '-label')) {
                    $updateParameter->label = $label;
                } else {
                    $updateParameter->label = ucfirst($parameter->getName());
                }
                if ($description = $annotations->get('cron-' . $parameter->getName() . '-description')) {
                    $updateParameter->description = $description;
                }
                if ($dataList = $annotations->get('cron-' . $parameter->getName() . '-data')) {
                    $updateParameter->dataList = $dataList;
                }
                if ($dataProviderCallback = $annotations->get('cron-' . $parameter->getName() . '-dataProvider')) {
                    $updateParameter->dataList = call_user_func($dataProviderCallback);
                }
                $allowParameterArray[$parameter->getName()] = $updateParameter;
            }
        }
        return $allowParameterArray;
    }

    protected static function loadConsoleConfig()
    {
        $consoleConfigFilePath = Yii::getPathOfAlias('application') . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'console.php';
        return require($consoleConfigFilePath);
    }
} 