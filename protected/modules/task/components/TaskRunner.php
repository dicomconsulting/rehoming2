<?php

class TaskRunner extends CComponent
{
    public $workingDirectoryPath;

    public $lockFileName = 'worker-exist.lock';

    public $stderrFileName = 'cron-stderr.log';

    public $outputFileName = 'cron-output.log';

    public $commandScript;

    public $binPath = 'php';

    public $maxLogFileSize = 1024;

    private $lockFileHandle = null;

    public function init()
    {
        Yii::log('Создан экзепмляр "TaskRunner"');
        $this->_rotateLog($this->_getStderrFullFileName());
        $this->_rotateLog($this->_getOutputFullFileName());
    }

    public function runTask(CronTask $task)
    {
        $descriptorOptions = [
            1 => array("file", $this->_getOutputFullFileName(), "a"),
            2 => array("file", $this->_getStderrFullFileName(), "a"),
        ];
        $process = proc_open($this->getFullCommand($task), $descriptorOptions, $pipes);
        if (is_resource($process)) {
            $status = proc_get_status($process);
            $task->setInProgressStatus($status['pid']);
            while ($status['running']) {
                sleep(5);
                $status = proc_get_status($process);
            }
            proc_close($process);
            if ($status['exitcode'] === 0) {
                $task->setDoneStatus($status['pid']);
            } else {
                $task->setFailStatus($status['pid']);
            }
            Yii::log("Задача №{$task->uid} завершилась с кодом {$status['exitcode']}");
        }
    }

    public function workerBegin()
    {
        if (flock($this->getLockFileHandle(), LOCK_EX|LOCK_NB)) {
            $currentDate = new RhDateTime();
            fwrite($this->getLockFileHandle(), $currentDate->format('d.m.Y H:i:s') . "\n");
        } else {
            fclose($this->getLockFileHandle());
            return false;
        }
    }

    public function workerEnd()
    {
        flock($this->getLockFileHandle(), LOCK_UN);
        fclose($this->getLockFileHandle());
    }

    public function workerExist()
    {
        $lockResult = flock($this->getLockFileHandle(), LOCK_EX|LOCK_NB);
        if ($lockResult) {
            flock($this->getLockFileHandle(), LOCK_UN);
        }
        return !$lockResult;
    }

    public function getFullCommand(CronTask $task)
    {
        return $this->binPath . ' ' . $this->getCommandScript() . ' ' . $task->getConsoleCommand();
    }

    public function getCommandScript()
    {
        if (is_null($this->commandScript)) {
            $this->commandScript = Yii::getPathOfAlias('application') . DIRECTORY_SEPARATOR . 'cron.php';
        }
        return $this->commandScript;
    }

    /**
     * @param $workingDirectoryPath
     *
     * @return $this
     */
    public function setWorkingDirectoryPath($workingDirectoryPath)
    {
        $this->workingDirectoryPath = $workingDirectoryPath;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWorkingDirectoryPath()
    {
        if (is_null($this->workingDirectoryPath)) {
            $this->setWorkingDirectoryPath(Yii::app()->getRuntimePath());
        }
        return $this->workingDirectoryPath;
    }

    private function _getStderrFullFileName()
    {
        return $this->getWorkingDirectoryPath() . DIRECTORY_SEPARATOR . $this->stderrFileName;
    }

    private function _getOutputFullFileName()
    {
        return $this->getWorkingDirectoryPath() . DIRECTORY_SEPARATOR . $this->outputFileName;
    }

    private function _rotateLog($fullFileName)
    {
        if (is_file($fullFileName)) {
            $handler = @fopen($fullFileName, 'a');
            if (@flock($handler, LOCK_EX|LOCK_NB) && @filesize($fullFileName) > $this->maxLogFileSize * 1024) {
                @rename($fullFileName, $fullFileName . '-' . date('d.m.Y-H:i:s'));
                @flock($handler, LOCK_UN);
                fclose($handler);
            }
        }
    }

    private function getFullLockFilePath()
    {
        return $this->getWorkingDirectoryPath() . DIRECTORY_SEPARATOR . $this->lockFileName;
    }

    private function getLockFileHandle()
    {
        if (!is_resource($this->lockFileHandle)) {
            $this->lockFileHandle = fopen($this->getFullLockFilePath(), "w+");
        }
        return $this->lockFileHandle;
    }
}
