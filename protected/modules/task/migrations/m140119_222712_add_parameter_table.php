<?php

class m140119_222712_add_parameter_table extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{cron_task_parameter}}',
            array(
                'uid' => 'pk',
                'cron_task_id' => 'INTEGER NOT NULL',
                'name' => 'VARCHAR(100)',
                'value' => 'VARCHAR(100)',
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );

        $this->addForeignKey(
            'fk_cron_task_parameter_relation_cron_task',
            '{{cron_task_parameter}}',
            'cron_task_id',
            '{{cron_task}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_cron_task_parameter_relation_cron_task', '{{cron_task_parameter}}');
        $this->dropTable('{{cron_task_parameter}}');
    }
}
