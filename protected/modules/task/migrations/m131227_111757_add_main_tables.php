<?php

class m131227_111757_add_main_tables extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{cron_task}}',
            array(
                'uid' => 'pk',
                'command' => 'varchar(100) NOT NULL',
                'action' => 'varchar(100) NOT NULL',
                'repeat' => 'INTEGER DEFAULT NULL COMMENT "Количество выполнения задачи. NULL - регулярно"',
                'retries' => 'INTEGER NOT NULL DEFAULT 0 COMMENT "Количество повторов при ошибке"',
                'priority' => 'INTEGER NOT NULL DEFAULT 0',
                'cron_expression' => 'VARCHAR(250) DEFAULT NULL',
                'next_execution_time' => 'DATETIME NOT NULL',
                'update_time' => 'DATETIME NOT NULL',
                'active' => 'TINYINT(1)'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );

        $this->createTable(
            '{{cron_task_status}}',
            array(
                'uid' => 'pk',
                'cron_task_id' => 'INTEGER NOT NULL',
                'status_id' => 'TINYINT(1) DEFAULT 0',
                'pid' => 'INTEGER COMMENT "Номер процесса в котором был создан статус"',
                'create_time' => 'DATETIME NOT NULL',
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );

        $this->addForeignKey(
            'fk_cron_task_status_relation_cron_task',
            '{{cron_task_status}}',
            'cron_task_id',
            '{{cron_task}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_cron_task_status_relation_cron_task', '{{cron_task_status}}');
        $this->dropTable('{{cron_task}}');
        $this->dropTable('{{cron_task_status}}');
    }
}
