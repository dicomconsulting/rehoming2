<?php
Yii::import('application.modules.task.models.*');

class TaskManagerCommand extends CConsoleCommand
{

    public function actionIndex()
    {
        Yii::log('Менеджер задач запущен.');
        $taskRunner = Yii::app()->taskRunner;
        $cronTaskModel = new CronTask();
        if (!$taskRunner->workerExist()) {
            Yii::log('Запускаем новый процесс для всех типов задач.');
            $taskRunner->workerBegin();
            $this->_runTask($taskRunner, $cronTaskModel->getNextTask());
            $taskRunner->workerEnd();
        } else {
            Yii::log('Запускаем новый процесс для критических задач.');
            $this->_runTask($taskRunner, $cronTaskModel->getNextCriticalTask());
        }
        Yii::log('Менеджер задач закончил работу.');
    }

    private function _runTask($taskRunner, $cronTask)
    {
        if ($cronTask instanceof CronTask) {
            Yii::log('Взята в обработку задача #' . $cronTask->uid);
            $taskRunner->runTask($cronTask);
        } else {
            Yii::log('Задачи для обработки отсутствуют');
        }
    }
}
