<?php

class I18nModule extends CWebModule
{
    public $translatedLanguages = [];

    public $defaultLanguage;

    /**
     * @var string
     */
    public $tableSuffix = '';
    
    /**
     * Initializes the module.
     */
    public function init()
    {
        $this->setImport(
            array(
                'i18n.components.*',
                'i18n.models.*',
            )
        );
    }
}
