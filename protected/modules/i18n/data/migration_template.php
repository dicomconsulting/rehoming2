<?php

class {ClassName} extends I18nDbMigration
{
    public function safeUp()
    {
    }

    public function safeDown()
    {
        echo "{ClassName} does not support migration down.\\n";

        return false;
    }
}
