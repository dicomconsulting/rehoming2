<?php

class I18nDbMigration extends EDbMigration
{
    const COMMAS = ',';
    const LOCALE_COLUMN_NAME = 'locale';
    const PK_PARTS_GLUE = '`,`';
    const COUNT_PLACEHOLDER = '{count}';
    const DEFAULT_LOCALE_PLACEHOLDER = '{default_locale}';

    /**
     * @var string
     */
    private $_tableSuffix = '_translation';

    /**
     * @var string
     */
    private $_foreignKeyPrefix = 'fk_';

    /**
     *
     * @var string
     */
    private $_localeColumnDefinition;

    /**
     *
     * @var integer
     */
    private $_localeColumnCountChars = 2;

    /**
     *
     * @var string
     */
    private $_defaultLocale = 'en';

    /**
     * Parts of pk pseudo type's description from all schemas.
     *
     * @todo Modify this functionality when implementing multi DB's drivers.
     * @var array
     */
    private $_autoIncrementStatements = array(
        'identity' => '',
        'PRIMARY' => '',
        'KEY' => '',
        'AUTO_INCREMENT' => '',
        'serial' => 'integer'
    );

    /**
     *
     * @var array
     */
    private $_allowConfigParamNames = array(
        'localeColumnCountChars',
        'foreignKeyPrefix',
        'tableSuffix',
        'defaultLocale'
    );

    public function __construct()
    {
        $this->_localeColumnDefinition = "CHAR(" . self::COUNT_PLACEHOLDER . ") NOT NULL DEFAULT '"
             . self::DEFAULT_LOCALE_PLACEHOLDER . "'";

        if (is_array(Yii::app()->params['i18n'])) {
            foreach ($this->_allowConfigParamNames as $paramName) {
                $methodName = 'set' . ucfirst($paramName);
                if (isset(Yii::app()->params['i18n'][$paramName])) {
                    $this->$methodName(Yii::app()->params['i18n'][$paramName]);
                }
            }
        }
    }

    /**
     *
     * @param string       $table
     * @param array        $columns
     * @param string       $options
     * @param string|array $i18nColumns
     */
    public function createTableWithI18n($mainTableName, $allColumns, $options = null, $i18nColumnNames = array())
    {
        $i18nTableName = $this->_generateI18nTableName($mainTableName);
        $calculatedColumns = $this->_distributeTablesColumns($allColumns, $i18nColumnNames);

        $this->createTable($mainTableName, $calculatedColumns['main'], $options);
        if (!empty($calculatedColumns['i18n'])) {
            $mainTablePkColumns = $this->_getPrimaryKey($mainTableName, true);
            $i18nTableColumns = $this->_calculateI18nTableColumns(
                $calculatedColumns['main'],
                $calculatedColumns['i18n'],
                $mainTablePkColumns
            );

            $this->createTable($i18nTableName, $i18nTableColumns, $options);
            $this->_addForeignKey($mainTableName, $i18nTableName, $mainTablePkColumns);
        }
    }

    /**
     *
     * @param string $mainTableName
     * @param string $newMainTableName
     */
    public function renameTableWithI18n($mainTableName, $newMainTableName)
    {
        $i18nTableName = $this->_generateI18nTableName($mainTableName);
        $existI18nTable = $this->_existTable($i18nTableName);
        if ($existI18nTable) {
            $newI18nTableName = $this->_generateI18nTableName($newMainTableName);
            $mainTablePkColumns = $this->_getPrimaryKey($mainTableName, true);

            $this->_dropI18nForeignKey($mainTableName, $i18nTableName, $mainTablePkColumns);
            $this->renameTable($i18nTableName, $newI18nTableName);
        }

        $this->renameTable($mainTableName, $newMainTableName);

        if ($existI18nTable) {
            $this->_addForeignKey($newMainTableName, $newI18nTableName, $mainTablePkColumns);
        }
    }

    /**
     * @param string $mainTableName
     */
    public function dropTableWithI18n($mainTableName)
    {
        $i18nTableName = $this->_generateI18nTableName($mainTableName);
        if ($this->_existTable($i18nTableName)) {
            $this->dropTable($i18nTableName);
        }
        $this->dropTable($mainTableName);
    }

    /**
     *
     * @param string $mainTableName
     */
    public function truncateTableWithI18n($mainTableName)
    {
        $i18nTableName = $this->_generateI18nTableName($mainTableName);
        $existI18nTable = $this->_existTable($i18nTableName);
        if ($existI18nTable) {
            $mainTablePkColumns = $this->_getPrimaryKey($mainTableName, true);
            $this->_dropI18nForeignKey($mainTableName, $i18nTableName, $mainTablePkColumns);
            $this->truncateTable($i18nTableName);
        }
        $this->truncateTable($mainTableName);
        if ($existI18nTable) {
            $this->_addForeignKey($mainTableName, $i18nTableName, $mainTablePkColumns);
        }
    }

    /**
     *
     * @param string $mainTableName
     */
    public function refreshTableWithI18nSchema($mainTableName)
    {
        $this->refreshTableSchema($mainTableName);
        $this->refreshTableSchema($this->_generateI18nTableName($mainTableName));
    }

    /**
     * The currently initialized suffix for i18n tables
     *
     * @return string
     */
    public function getTableSuffix()
    {
        return $this->_tableSuffix;
    }

    /**
     * @param string $suffix
     */
    public function setTableSuffix($suffix)
    {
        if (is_string($suffix) && strlen($suffix) > 0) {
            $this->_tableSuffix = $suffix;
        }
    }

    /**
     *
     * @param String $fkPrefix
     */
    public function setForeignKeyPrefix($fkPrefix = '')
    {
        $this->_foreignKeyPrefix = $fkPrefix;
    }

    /**
     *
     * @param string $locale
     */
    public function setDefaultLocale($locale)
    {
        if (is_string($locale) && strlen($locale)) {
            $this->_defaultLocale = $locale;
        }
    }

    /**
     * Set locale column chars' count
     *
     * @param integer $count
     */
    public function setLocaleColumnCountChars($count)
    {
        $count = abs(intval($count));

        if ($count > 0) {
            $this->_localeColumnCountChars = $count;
        }
    }

    /**
     * Return DB column's definition string.
     *
     * @return string
     */
    public function getLocaleColumnDefinition()
    {
        return str_replace(
            array(self::COUNT_PLACEHOLDER, self::DEFAULT_LOCALE_PLACEHOLDER),
            array($this->_localeColumnCountChars, $this->_defaultLocale),
            $this->_localeColumnDefinition
        );
    }

    /**
     * Use safeUp for this class and subclasses
     *
     * @return boolean
     */
    final public function up()
    {
        return parent::up();
    }

    /**
     * Use safeDown for this class and subclasses
     *
     * @return boolean
     */
    final public function down()
    {
        return parent::down();
    }

    /**
     * Generate table name for translate table
     *
     * @param  string $mainTableName
     * @return string
     */
    private function _generateI18nTableName($mainTableName)
    {
        return $mainTableName . $this->getTableSuffix();
    }

    /**
     * Distribute columns for main and translate tables
     *
     * @param  array        $columns
     * @param  string|array $i18nColumnNames
     * @return array
     */
    private function _distributeTablesColumns($allColumns, $i18nColumnNames)
    {
        $i18nColumns = array();

        if (!is_array($i18nColumnNames)) {
            $i18nColumnNames = array($i18nColumnNames);
        }

        foreach ($i18nColumnNames as $columnName) {
            if (array_key_exists($columnName, $allColumns)) {
                $i18nColumns[$columnName] = $allColumns[$columnName];
                unset($allColumns[$columnName]);
            }
        }

        return array(
            'main' => $allColumns,
            'i18n' => $i18nColumns
        );
    }

    /**
     * Get primary key for table
     *
     * @param  string     $tableName
     * @param  boolean    $required
     * @return null|array
     * @throws Exception
     */
    private function _getPrimaryKey($tableName, $required = false)
    {
        $pk = $this->getDbConnection()->getSchema()->getTable($tableName)->primaryKey;

        if ($required && empty($pk)) {
            throw new Exception('The table must have a primary key.');
        } elseif (empty($pk)) {
            return null;
        } else {
            return is_array($pk) ? $pk : array($pk);
        }
    }

    /**
     * Adding columns for foreign key
     *
     * @param  array $mainTableColumns
     * @param  array $i18nTableColumns
     * @param  array $foreignKeyColumnNames
     * @return array
     */
    private function _calculateI18nTableColumns($mainTableColumns, $i18nTableColumns, $foreignKeyColumnNames)
    {
        foreach ($foreignKeyColumnNames as $columnName) {
            if (!array_key_exists($columnName, $i18nTableColumns)) {
                if ($mainTableColumns[$columnName] === 'pk') {
                    $i18nTableColumns[$columnName] = str_ireplace(
                        array_keys($this->_autoIncrementStatements),
                        array_values($this->_autoIncrementStatements),
                        $this->getDbConnection()->getSchema()->columnTypes['pk']
                    );
                } else {
                    $i18nTableColumns[$columnName] = $mainTableColumns[$columnName];
                }
            }
        }

        $i18nTableColumns = $this->_addI18nTablePrimaryKey(
            $this->_addLocaleColumn($i18nTableColumns),
            $foreignKeyColumnNames
        );

        return $i18nTableColumns;
    }

    /**
     * Generate foreign key name by tables' and columns' names
     *
     * @param  string $mainTableName
     * @param  string $i18nTableName
     * @param  string $mainTablePkColumns
     * @return string
     */
    private function _generateForeignKeyName($mainTableName, $i18nTableName, $mainTablePkColumns)
    {
        return $this->_foreignKeyPrefix
            . implode('_', array_merge(array($i18nTableName, $mainTableName), $mainTablePkColumns));
    }

    /**
     *
     * @param string $mainTableName
     * @param string $i18nTableName
     * @param array  $mainTablePkColumns
     */
    private function _addForeignKey($mainTableName, $i18nTableName, $mainTablePkColumns)
    {
        $this->addForeignKey(
            $this->_generateForeignKeyName($mainTableName, $i18nTableName, $mainTablePkColumns),
            $i18nTableName,
            implode(self::COMMAS, $mainTablePkColumns),
            $mainTableName,
            implode(self::COMMAS, $mainTablePkColumns),
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * Drop foreign key between main and i18n tables.
     *
     * @param string $mainTableName
     * @param string $i18nTableName
     * @param array  $mainTablePkColumns
     */
    private function _dropI18nForeignKey($mainTableName, $i18nTableName, $mainTablePkColumns)
    {
        $this->dropForeignKey(
            $this->_generateForeignKeyName($mainTableName, $i18nTableName, $mainTablePkColumns),
            $i18nTableName
        );
    }

    /**
     * Add PRIMARY KEY to i18nTable
     *
     * @param  array $i18nColumns
     * @param  array $foreignKeyColumnNames
     * @return array
     */
    private function _addI18nTablePrimaryKey($i18nColumns, $foreignKeyColumnNames)
    {
        $primaryKeyColumnNames = $foreignKeyColumnNames;
        array_push($primaryKeyColumnNames, self::LOCALE_COLUMN_NAME);

        $primaryKeyString = 'PRIMARY KEY (`' . implode(self::PK_PARTS_GLUE, $primaryKeyColumnNames) . '`)';
        array_push($i18nColumns, $primaryKeyString);

        return $i18nColumns;
    }

    /**
     * Add locale column's definition
     *
     * @param  array $i18nColumns
     * @return array
     */
    private function _addLocaleColumn($i18nColumns)
    {
        $i18nColumns[self::LOCALE_COLUMN_NAME] = $this->getLocaleColumnDefinition();

        return $i18nColumns;
    }

    /**
     * True if table exist. False if the table does not exist
     *
     * @param  string  $tableName
     * @return boolean
     */
    private function _existTable($tableName)
    {
        return $this->getDbConnection()->getSchema()->getTable($tableName, true) instanceof CDbTableSchema;
    }
}
