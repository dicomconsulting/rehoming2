<?php

class LanguageUrlManager extends CUrlManager
{
    public function createUrl ($route, $params = [], $ampersand = '&')
    {
        $url = parent::createUrl($route, $params, $ampersand);
        return MultiLanguageHelper::addLangToUrl($url);
    }
}
