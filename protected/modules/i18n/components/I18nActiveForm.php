<?php
Yii::import('i18n.components.I18nHtml');
Yii::import('i18n.models.I18nActiveRecord');

class I18nActiveForm extends CActiveForm
{
    public $translateFieldWrapper = [
        'tag' => 'span',
        'style' => 'margin-left: 10px'
    ];

    public function init()
    {
        $this->action = Yii::app()->request->getOriginalUrl();
        parent::init();
    }

    public function textField($model, $attribute, $htmlOptions = [])
    {
        if ($this->_isI18nModel($model) && $model->isTranslationAttribute($attribute)) {
            $model->disableDefaultSourceTranslation();
            $htmlOutput = '';
            $wrapperTag = $this->translateFieldWrapper['tag'];
            $wrapperOptions = $this->translateFieldWrapper;
            unset($wrapperOptions['tag']);
            $renderFirstField = false;
            foreach (MultiLanguageHelper::languageList() as $localeName) {
                $newAttributeName = $localeName . I18nActiveRecord::PROPERTY_NAME_DELIMITER . $attribute;
                $htmlOutput .= ($renderFirstField ? CHtml::openTag($wrapperTag, $wrapperOptions) : '')
                    . CHtml::openTag('div', ['class' => 'input-prepend'])
                    . CHtml::openTag('span', ['class' => 'add-on'])
                    . MultiLanguageHelper::generateLanguageFlag($localeName)
                    . CHtml::closeTag('span')
                    . CHtml::closeTag('div')
                    . CHtml::activeTextField($model, $newAttributeName, $htmlOptions)
                    . ($renderFirstField ? CHtml::closeTag($wrapperTag) : '');

                $renderFirstField = true;
            }
            return $htmlOutput;
        } else {
            return parent::textField($model, $attribute, $htmlOptions);
        }
    }

    private function _isI18nModel($model)
    {
        return $model instanceof I18nActiveRecord;
    }
}
