<?php

class LanguageSwitcherWidget extends CWidget
{
    public function run()
    {
        $currentUrl = ltrim(Yii::app()->request->url, '/');
        $links = array();
        foreach (MultiLanguageHelper::languageList() as $language) {
            $isActiveLanguage = Yii::app()->getLanguage() == $language;
            $imgHtml = MultiLanguageHelper::generateLanguageFlag($language);
            $url = '/' . (MultiLanguageHelper::isDefaultLanguage($language) ? '' : $language . '/') . $currentUrl;
            $links[] = CHtml::tag('li', array('class' => $isActiveLanguage ? 'active' : ''), CHtml::link($imgHtml, $url));
        }
        echo CHtml::tag('ul', array('id' => 'languageSwitcher'), implode("\n", $links));
    }
}
