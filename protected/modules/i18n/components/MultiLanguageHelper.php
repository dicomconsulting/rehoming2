<?php

class MultiLanguageHelper
{
    public static function languageList()
    {
        $translatedLanguages = Yii::app()->getModule('i18n')->translatedLanguages;
        if (!is_array($translatedLanguages)) {
            $translatedLanguages = [Yii::app()->sourceLanguage];
            if (Yii::app()->language != Yii::app()->sourceLanguage) {
                $translatedLanguages[] = Yii::app()->language;
            }
        }
        return $translatedLanguages;
    }

    public static function getDefaultLanguage()
    {
        $defaultLanguage = Yii::app()->getModule('i18n')->defaultLanguage;
        if (!in_array($defaultLanguage, self::languageList())) {
            $defaultLanguage = Yii::app()->language;
        }
        return $defaultLanguage;
    }

    public static function isDefaultLanguage($language)
    {
        return $language == self::getDefaultLanguage();
    }

    public static function processLangInUrl($url)
    {
        $urlParts = explode('/', ltrim($url, '/'));
        $languagePart = $urlParts[0];
        $isLangExists = in_array($urlParts[0], self::languageList());

        if ($isLangExists) {
            $lang = array_shift($urlParts);
            Yii::app()->setLanguage($lang);
        } else {
            Yii::app()->setLanguage(self::getDefaultLanguage());
        }

        $url = '/' . implode('/', $urlParts);

        return $url;
    }

    public static function addLangToUrl($url)
    {
        $urlParts = explode('/', ltrim($url, '/'));
        $languagePart = $urlParts[0];
        $isHasLanguage = in_array($languagePart, self::languageList());
        $isDefaultLanguage = Yii::app()->getLanguage() == self::getDefaultLanguage();

        if ($isHasLanguage && $isDefaultLanguage) {
            array_shift($urlParts);
        }

        if (!$isHasLanguage && !$isDefaultLanguage) {
            array_unshift($urlParts, Yii::app()->getLanguage());
        }

        $url = '/' . implode('/', $urlParts);

        return $url;
    }

    public static function generateLanguageFlag($language)
    {
        if (array_search($language, self::languageList()) !== false) {
            return CHtml::image('/images/flag_'. $language .'.png', $language);
        } else {
            return '';
        }
    }
}
