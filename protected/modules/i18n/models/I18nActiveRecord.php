<?php
Yii::import('i18n.models.TranslationObject');


/**
 * Class I18nActiveRecord
 *
 * @TODO пофиксить инстанциирование объектов переводов если они были заджоинены
 */
abstract class I18nActiveRecord extends CActiveRecord
{
    const PROPERTY_NAME_DELIMITER = '__';

    /**
     * @var array Array of translation objects indexed by the locale.
     */
    private $i18n = array();

    /**
     * @var string Current object's locale.
     */
    private $locale = null;

    /**
     * @var string Translation table's alias for sql queries.
     */
    private $i18nTableAlias = 'translate';

    /**
     * @var string Name for locale field in translation table.
     */
    private $localeFieldName = 'locale';

    /**
     * Behavior if the requested translation doesn't exist for this model
     *
     * When is true it will return the source language translation, when requested does not exist.
     *
     * @var boolean
     */
    private $defaultSourceTranslation = true;

    /**
     * Behavior if the suitable translation doesn't exist for this model
     *
     * @var boolean
     */
    private $nullInsteadException = true;

    /**
     * Return null instead of throwing an exception if the suitable translation doesn't exist for this model.
     */
    public function enableNullInsteadException()
    {
        $this->nullInsteadException = true;
        return $this;
    }

    /**
     * Throwing an exception if the suitable translation doesn't exist for this model.
     */
    public function disableNullInsteadException()
    {
        $this->nullInsteadException = false;
        return $this;
    }

    /**
     * Return the source language translation, when requested does not exist.
     */
    public function enableDefaultSourceTranslation()
    {
        $this->defaultSourceTranslation = true;
        return $this;
    }

    /**
     * Return null or throw exception, when requested translation does not exist.
     */
    public function disableDefaultSourceTranslation()
    {
        $this->defaultSourceTranslation = false;
        return $this;
    }

    /**
     * Set the current locale to use.
     *
     * @param  string $locale the CLocale id
     * @return I18nActiveRecord Returns this active record.
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * Return the current locale to use.
     *
     * If the current locale is not specified, set the global application's locale, as the current.
     *
     * @return string CLocale id.
     */
    public function getLocale()
    {
        if (empty($this->locale)) {
            $this->setLocale(Yii::app()->getLocale()->id);
        }
        return $this->locale;
    }

    /**
     * Return translation table locale field's name.
     *
     * @return string
     */
    public function getLocaleFieldName()
    {
        return $this->localeFieldName;
    }

    /**
     * Return alias name for translation table.
     *
     * @return string
     */
    public function getI18nTableAlias()
    {
        return $this->i18nTableAlias;
    }

    /**
     * Return the translation object by locale's name.
     *
     * If the name of the locale is not defined, use current locale's name of main object.
     * If $existOnly equal false and translation doesn't exist, then new translation will be created.
     *
     * @param  null|string $locale CLocale id.
     * @param boolean $existOnly Do not create a new translation if it is not in the model.
     * @return CActiveRecord Translation object.
     */
    public function getTranslation($locale = null, $existOnly = false)
    {
        $locale = $this->_normalizeLocale($locale);
        $this->_loadTranslation($locale, $existOnly);
        return $this->_getTranslation($locale);
    }

    /**
     * Delete translation from main object and DB.
     *
     * If the name of the locale is not defined, use current locale name of main object.
     *
     * @param  null|string  $locale CLocale id.
     * @return boolean Is deletion was successful.
     */
    public function deleteTranslation($locale = null)
    {
        $locale = $this->_normalizeLocale($locale);
        if ($this->_issetTranslation($locale)) {
            $this->_unsetTranslation($locale);
        }

        return $this->_deleteTranslation($locale);
    }

    /**
     * Get the name of the translation class.
     *
     * @return string Class name of the translation record
     */
    public function getTranslationClassName()
    {
        return 'TranslationObject';
    }

    /**
     * Verify the existence of the translation.
     *
     * Verifies the existence of the translation for a given locale name.
     * If $saved equal true, then translation must exist in DB, otherwise must exist in object or DB.
     * If the name of the locale is not defined, use current locale name of main object.
     *
     * @param null|string  $locale CLocale id.
     * @param boolean $saved Default true.
     * @return boolean Exist translation.
     */
    public function translationExists($locale = null, $saved = false)
    {
        $locale = $this->_normalizeLocale($locale);

        if ($saved && $this->getIsNewRecord()) {
            $exist = false;
        } elseif (!$saved) {
            $translationClassName = $this->getTranslationClassName();
            $exist = $this->i18n[$locale] instanceof $translationClassName;
        } else {
            $exist = $this->_createNewTranslationObject($this->getLocale(),$this->_getTranslationTableName())->exists(
                $this->_getTranslationConditionForPk(),
                $this->_getTranslationTablePrimaryKeyParams($locale)
            );
        }

        return $exist;
    }

    /**
     * Calls an anonymous function on each translation
     * The signature of the closure should be <code>function(CActiveRecord $translationObject){}</code>.
     * @param Closure
     */
    public function eachTranslation(Closure $closure)
    {
        foreach ($this->i18n as $translationObject) {
            $closure($translationObject);
        }
    }

    /**
     * A named scope to fetch translation together with this record.
     *
     * If the name of the locale is not defined, use current locale name of main object.
     *
     * @param string $locale CLocale id.
     * @param boolean $onlyWithTranslation
     * use true to get all records including those without translation
     * use false to return only records with translation
     * @return I18nActiveRecord
     */
    public function withI18n($locale = null, $onlyWithTranslation = false)
    {
        if (!$onlyWithTranslation) {
            $joinCondition = 'LEFT OUTER JOIN';
        } else {
            $joinCondition = 'INNER JOIN';
        }
        $schema = $this->dbConnection->schema;
        $pkParts = $this->_getTranslationTablePrimaryKey($locale);
        $onConditionParts = [];
        $parameters = [];
        foreach ($pkParts as $name => $value) {
            if ($name != $this->getLocaleFieldName()) {
                $value = 't.'.$name;
            } else {
                $this->setLocale($value);
                $parameters[':' . $name] = $value;
                $value = ":{$name}";
            }
            $onConditionParts[] = $this->_addTranslationTableAlias($name, true) . ' = ' . $value;
        }
        $i18nTable = $this->_createNewTranslationObject($this->getLocale(),$this->_getTranslationTableName())->getTableSchema();
        $columns = [];
        $alias = $schema->quoteColumnName($this->getI18nTableAlias());
        foreach ($i18nTable->getColumnNames() as $name) {
            if ($name == $this->getLocaleFieldName() || !array_key_exists($name, $pkParts)) {
                $fieldName = $schema->quoteColumnName($name);
                $columns[]="{$alias}.{$fieldName} AS {$name}";
            }
        }
        $this->getDbCriteria()->mergeWith([
            'select' => "t.*, " . implode(', ', $columns),
            'join' => "{$joinCondition} {$i18nTable->name} {$alias} ON (" . implode(' AND ', $onConditionParts) . ")",
            'params' => $parameters
        ]);

        return $this;
    }

    /**
     * Getting attribute values of the translation object.
     *
     * If the mechanisms of the parent entity did not return a value, then there is a search value in the translations.
     * There are two ways to work with this mechanism:
     * $obj->attributeName - get the value of the translation attribute "attributeName" of the main object's current
     * locale.
     * $obj->localeName__attributeName - get the value of the translation attribute "attributeName" of the specified
     *     "localeName", without changing of the main object's current locale.
     *
     * @param string $name
     * @return mixed
     * @throws CException
     */
    public function __get($name)
    {
        $parameters = $this->_parsingPropertyName($name);
        $locale = $parameters['locale'];
        $name = $parameters['name'];

        try {
            $value = parent::__get($name);
        } catch (CException $e) {
            if ($this->getTranslation($locale, true) && $this->getTranslation($locale, true)->hasAttribute($name)) {
                $value = $this->getTranslation($locale)->getAttribute($name);
            } elseif ($this->getTranslation($locale, true)
                && isset($this->getTranslation($locale, true)->getMetaData()->columns[$name])) {
                $value = null;
            } elseif ($this->isTranslationAttribute($name)) {
                $sourceLanguage = Yii::app()->sourceLanguage;
                if ($sourceLanguage != $locale && $this->defaultSourceTranslation) {
                    $value = $this->__get($sourceLanguage . self::PROPERTY_NAME_DELIMITER . $name);
                } elseif ($this->nullInsteadException) {
                    return null;
                } else {
                    throw new CException("Translation for locale '{$locale}' does not exist for this object", null, $e);
                }
            } else {
                throw $e;
            }
        }

        return $value;
    }

    /**
     * Setting attribute value of the translation object.
     *
     * If the attribute refers to the translation object, it is set, otherwise called the parent method.
     * There are two ways to work with this mechanism:
     * $obj->attributeName = value - set the value for the translation attribute "attributeName" of the main object's
     * current locale.
     * $obj->localeNmae__attributeName = value - set the value for the translation attribute "attributeName"
     * of the specified
     *     "localeName", without changing of the main object's current locale.
     *
     * @param string $name
     * @param mixed $value
     * @throws CException
     * @return void
     */
    public function __set($name, $value)
    {
        $parameters = $this->_parsingPropertyName($name);
        $locale = $parameters['locale'];
        $name = $parameters['name'];

        if ($this->isTranslationAttribute($name)) {
            $this->getTranslation($locale);
            $this->getTranslation($locale)->setAttribute($name, $value);
        } else {
            parent::__set($name, $value);
        }
    }

    /**
     * Checks if a property value is null.
     * This method overrides the parent implementation by checking
     * if the named attribute is null or not.
     * @param  string  $name the property name or the event name
     * @return boolean whether the property value is null
     */
    public function __isset($name)
    {
        if ($this->hasAttribute($name)) {
            return true;
        } else {
            $requestTranslation = $this->getTranslation($this->locale, true);
            $defaultTranslation = $this->getTranslation(Yii::app()->sourceLanguage, true);
            if (($requestTranslation && $this->getTranslationClassName() && isset($requestTranslation->getAttributes()[$name]))
                || (
                    $this->defaultSourceTranslation
                    && $defaultTranslation
                    && isset($defaultTranslation->getAttributes()[$name]))) {
                return true;
            } elseif (isset($this->getMetaData()->columns[$name])) {
                return false;
            } elseif ($this->hasRelated($name)) {
                return true;
            } elseif (isset($this->getMetaData()->relations[$name])) {
                return $this->getRelated($name) !== null;
            } else {
                return parent::__isset($name);
            }
        }
    }

    /**
     * Sets a component property to be null.
     * This method overrides the parent implementation by clearing
     * the specified attribute value.
     * @param string $name the property name or the event name
     * @return void
     */
    public function __unset($name)
    {
        if (isset($this->getMetaData()->columns[$name])) {
            unset($this->getAttributes()[$name]);
        }
        if (isset($this->i18n[$this->locale]) && isset($this->i18n[$this->locale]->getMetaData()->columns[$name])) {
            unset($this->i18n[$this->locale]->getAttributes()[$name]);
        } else {
            parent::__unset($name);
        }
    }

    /**
     * Checks whether this AR has the named attribute
     * @param  string  $name attribute name
     * @return boolean whether this AR has the named attribute (table column).
     */
    public function hasAttribute($name)
    {
        return parent::hasAttribute($name);
    }

    /**
     * Returns the named attribute value.
     * If this is a new record and the attribute is not set before,
     * the default column value will be returned.
     * If this record is the result of a query and the attribute is not loaded,
     * null will be returned.
     * You may also use $this->AttributeName to obtain the attribute value.
     * @param  string $name the attribute name
     * @return mixed  the attribute value. Null if the attribute is not set or does not exist.
     * @see hasAttribute
     */
    public function getAttribute($name)
    {
        if (($value = parent::getAttribute($name) ) === null) {
            return $this->_createNewTranslationObject($this->getLocale(),$this->_getTranslationTableName())->getAttribute($name);
        }

        return $value;
    }

    public function setAttribute($name, $value)
    {
        if (!parent::setAttribute($name, $value)) {
            return isset($this->i18n[$this->locale]) && $this->i18n[$this->locale]->setAttribute($name, $value);
        }

        return true;
    }

    /**
     * Returns all column attribute values.
     * Note, related objects are not returned.
     * @param mixed $names names of attributes whose value needs to be returned.
     * If this is true (default), then all attribute values will be returned, including
     * those that are not loaded from DB (null will be returned for those attributes).
     * If this is null, all attributes except those that are not loaded from DB will be returned.
     * @return array attribute values indexed by attribute names.
     */
    public function getAttributes($names = true)
    {
        $i18nAttributes = $this->getTranslationAttributeNames();

        return array_merge($i18nAttributes, parent::getAttributes($names));
    }

    public function setAttributes($values, $safeOnly = true)
    {
        if (!is_array($values)) {
            return;
        }

        $attributes = array_flip($safeOnly ? $this->getSafeAttributeNames() : $this->attributeNames());
        foreach ($values as $name => $value) {
            $parameters = $this->_parsingPropertyName($name);
            $normalizeName = $parameters['name'];

            if (isset($attributes[$normalizeName])) {
                $this->$name=$value;
            } elseif ($safeOnly) {
                $this->onUnsafeAttribute($name, $value);
            }
        }
    }

    /**
     * Returns the list of all attribute names of the model.
     * This would return all column names of the table associated with this AR class.
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        return array_merge(
            array_keys($this->_createNewTranslationObject($this->getLocale(),$this->_getTranslationTableName())->getMetaData()->columns),
            parent::attributeNames()
        );
    }

    /**
     * Returns the text label for the specified attribute.
     * @param  string $attribute the attribute name
     * @return string the attribute label
     * @see generateAttributeLabel
     * @see attributeLabels
     */
    public function getAttributeLabel($attribute)
    {
        $labels = array_merge(
            $this->_createNewTranslationObject($this->getLocale(),$this->_getTranslationTableName())->attributeLabels(),
            $this->attributeLabels()
        );
        if (isset($labels[$attribute])) {
            return $labels[$attribute];
        } else {
            return $this->generateAttributeLabel($attribute);
        }
    }

    public function save($runValidation = true, $attributes = null)
    {
        if (!$runValidation || $this->validate($attributes)) {
            $pdo = $this->dbConnection->getPdoInstance();
            if (!$pdo->inTransaction()) {
                $pdo->beginTransaction();
                try {
                    $this->_saveInternal($attributes);

                    return $pdo->commit();
                } catch (Exception $e) {
                    $pdo->rollback();
                    throw new CDbException(Yii::t('yii', $e->getMessage()));
                }
            } else {
                return $this->_saveInternal($attributes);
            }
        } else {
            return false;
        }
    }

    public function populateRecord($attributes, $callAfterFind = true)
    {
        if ($attributes !== false) {
            if (isset($attributes[$this->getLocaleFieldName()])) {
                return $this->_populateWithI18n($attributes, $callAfterFind);
            }
            return $this->_populateRecordOnly($attributes, $callAfterFind);
        } else {
            return null;
        }
    }

    public function validate($attributes = null, $clearErrors = true)
    {
        if ($clearErrors) {
            $this->clearErrors();
        }
        if (is_array($attributes)) {
            $translationAttributeNames = array_intersect($attributes, $this->getTranslationAttributeNames());
            $baseAttributeNames = array_diff($attributes, $translationAttributeNames);
        } else {
            $translationAttributeNames = $this->getTranslationAttributeNames();
            $baseAttributeNames = $this->getBaseAttributeNames();
        }

        if ($this->beforeValidate()) {
            foreach (parent::getValidators() as $validator) {
                $validator->validate($this, $baseAttributeNames);
                foreach ($this->i18n as $i18nItem) {
                    $validator->validate($i18nItem, $translationAttributeNames);
                }
            }
            $this->afterValidate();

            $this->_collectUniqueErrorsInMainModel();

            return !$this->hasErrors();
        } else {
            return false;
        }
    }

    public function clearErrors($attribute = null)
    {
        parent::clearErrors($attribute);
        foreach ($this->i18n as $i18nItem) {
            $i18nItem->clearErrors($attribute);
        }
    }

    /**
     * Checking that the translation object can include this attribute.
     *
     * @param string $attributeName
     * @return boolean
     */
    public function isTranslationAttribute($attributeName)
    {
        return array_search($attributeName, $this->getTranslationAttributeNames()) !== false;
    }

    public function getTranslationAttributeNames()
    {
        $primaryKey = !is_array($this->primaryKey()) ? [$this->primaryKey()] : $this->primaryKey();

        return array_diff(array_keys($this->_createNewTranslationObject($this->getLocale(),$this->_getTranslationTableName())->getMetaData()->columns), $primaryKey);
    }

    public function getBaseAttributeNames()
    {
        return array_keys($this->getMetaData()->columns);
    }

    public static function model($className = __CLASS__)
    {
        $model = parent::model($className);
        if ($model instanceof I18nActiveRecord) {
            return $model->withI18n();
        } else {
            return $model;
        }
    }

    public function search()
    {
        return new CActiveDataProvider(
            $this,
            array(
                'criteria' => new CDbCriteria(['order' => 'uid ASC']),
                'pagination'=> [
                    'pageSize' => Yii::app()->user->getState('pageSize')
                ]
            )
        );
    }

    /**
     *
     * @param  type          $attributes
     * @param  type          $callAfterFind
     * @return CActiveRecord
     */
    private function _populateRecordOnly($attributes, $callAfterFind)
    {
        $record = $this->instantiate($attributes);
        $record->setScenario('update');
        $record->init();
        $md = $record->getMetaData();
        foreach ($attributes as $name => $value) {
            if (property_exists($record, $name)) {
                $record->$name = $value;
            } elseif (isset($md->columns[$name])) {
                $record->setAttribute($name, $value);
            }
        }
        $record->setOldPrimaryKey($record->getPrimaryKey());
        $record->attachBehaviors($record->behaviors());
        if ($callAfterFind) {
            $record->afterFind();
        }
        return $record;
    }

    /**
     *
     * @param  type          $attributes
     * @param  type          $callAfterFind
     * @return CActiveRecord
     */
    private function _populateWithI18n($attributes, $callAfterFind)
    {
        $translationLocale = $this->_normalizeLocale($attributes[$this->getLocaleFieldName()]);
        $record = $this->instantiate($attributes);
        $record->setScenario('update');
        $record->init();
        $md = $record->getMetaData();

        $i18nItem = $this->_createNewTranslationObject($translationLocale, 'update');
        $mdI18n = $i18nItem->getMetaData();

        foreach ($attributes as $name => $value) {
            if (property_exists($record, $name)) {
                $record->$name = $value;
            } elseif (isset($md->columns[$name])) {
                $record->setAttribute($name, $value);
            } elseif (property_exists($i18nItem, $name)) {
                $i18nItem->$name = $value;
            } elseif (isset($mdI18n->columns[$name])) {
                $i18nItem->setAttribute($name, $value);
            }
        }
        $record->setOldPrimaryKey($record->getPrimaryKey());
        $this->_bindTranslate($i18nItem, $record->getOldPrimaryKey());
        $record->i18n[$translationLocale] = $i18nItem;
        $record->attachBehaviors($record->behaviors());
        if ($callAfterFind) {
            $record->afterFind();
        }
        return $record;
    }

    private function _saveInternal($attributes)
    {
        if ($this->getIsNewRecord()) {
            $result = $this->insert($attributes);
            $pk = $this->getPrimaryKey();
            foreach ($this->i18n as $i18nItem) {
                $this->_bindTranslate($i18nItem, $pk);
                $result = $i18nItem->insert($attributes) && $result;
            }
        } else {
            $result = $this->update($attributes);
            $pk = $this->getPrimaryKey();
            foreach ($this->i18n as $i18nItem) {
                if ($i18nItem->getIsNewRecord()) {
                    $this->_bindTranslate($i18nItem, $pk);
                    $result = $i18nItem->insert($attributes) && $result;
                } else {
                    $result = $i18nItem->update($attributes) && $result;
                }
            }
        }

        return $result;
    }

    private function _bindTranslate($i18nItem, $pk)
    {
        if (is_array($pk)) {
            foreach ($pk as $pkPartName => $pkPartValue) {
                $i18nItem->$pkPartName = $pkPartValue;
            }
        } else {
            $pkPartName = $this->primaryKey();
            $i18nItem->$pkPartName = $pk;
        }
    }

    private function _getAllUniqueErrors()
    {
        $uniqueErrors = parent::getErrors();
        foreach ($this->i18n as $i18nItem) {
            $errors = $i18nItem->getErrors();
            foreach ($errors as $attributeName => $messages) {
                if (isset($uniqueErrors[$attributeName])) {
                    $uniqueErrors[$attributeName] = array_unique(
                        $uniqueErrors[$attributeName] + $messages
                    );
                } else {
                    $uniqueErrors[$attributeName] = $messages;
                }
            }
        }
        return $uniqueErrors;
    }

    private function _collectUniqueErrorsInMainModel()
    {
        $uniqueErrors = $this->_getAllUniqueErrors();
        $this->clearErrors();
        $this->addErrors($uniqueErrors);
    }


    private function _getTranslationTablePrimaryKey($locale = null, $manyLocales = false)
    {
        $primaryKeyParts = [];
        if ($manyLocales) {
            $locale = $this->_normalizeLocaleArray($locale);
        } else {
            $locale = $this->_normalizeLocale($locale);
        }
        $mainTablePrimaryKey = $this->getPrimaryKey();
        if (is_array($mainTablePrimaryKey)) {
            $primaryKeyParts = $mainTablePrimaryKey;
        } else {
            $primaryKeyParts[$this->primaryKey()] = $mainTablePrimaryKey;
        }
        $primaryKeyParts[$this->getLocaleFieldName()] = $locale;
        return $primaryKeyParts;
    }

    private function _getTranslationTablePrimaryKeyParams($locale = null, $manyLocales = true)
    {
        $paramsArray = [];
        $keyPartArray = $this->_getTranslationTablePrimaryKey($locale, $manyLocales);
        foreach ($keyPartArray as $key => $value) {
            $paramsArray[':'.$key] = $value;
        }
        $paramsArray[$this->getLocaleFieldName()] = implode(', ', $paramsArray[$this->getLocaleFieldName()]);
        return $paramsArray;
    }

    private function _normalizeLocale($locale = null)
    {
        if (empty($locale)) {
            $locale = $this->getLocale();
        } elseif (is_array($locale)) {
            $locale = strval(current($locale));
        } else {
            $locale = strval($locale);
        }
        return preg_replace('/[\r|\n]/', '', $locale);
    }

    private function _normalizeLocaleArray($locale = null)
    {
        $normalizeArray = [];
        if (empty($locale)) {
            $normalizeArray[] = $this->getLocale();
        } elseif (is_array($locale)) {
            foreach ($locale as $item) {
                $normalizeArray[] = $this->_normalizeLocale($item);
            }
        } else {
            $normalizeArray[] = $this->_normalizeLocale($locale);
        }
        return $normalizeArray;
    }

    /**
     * Return condition for pk.
     *
     * The condition contains all the attributes of the table's primary key, connected through the operator AND.
     * Field of the locale can be compared with multiple values.
     * If $withAlias equal true, the attributes will contain a translation table's alias.
     *
     * @param boolean $withAlias
     * @param boolean $manyLocales
     * @return string Condition.
     */
    private function _getTranslationConditionForPk($withAlias = false, $manyLocales = false)
    {
        $keyParts = $this->primaryKey();
        $conditionParts = [];
        if (!is_array($keyParts)) {
            $conditionParts[] = $this->_addTranslationTableAlias($keyParts, $withAlias) . ' = :' . $keyParts;
        } else {
            foreach ($keyParts as $part) {
                $conditionParts[] = $this->_addTranslationTableAlias($part, $withAlias) .' = :' . $part;
            }
        }
        $conditionParts[] = $this->_addTranslationTableAlias($this->getLocaleFieldName(), $withAlias)
            . ($manyLocales ? ' IN(:' . $this->getLocaleFieldName().')':' = :' . $this->getLocaleFieldName());
        return implode(' AND ', $conditionParts);
    }

    /**
     * Prepend translation table alias if needed.
     *
     * @param string $fieldName Input field name.
     * @param boolean $needPrepend
     * @return string Result fiend name.
     */
    private function _addTranslationTableAlias($fieldName, $needPrepend = false)
    {
        return ($needPrepend ? $this->getI18nTableAlias() . '.' : '') . $fieldName;
    }

    /**
     * Loads the translation from the database or create a new one, for the requested locale.
     *
     * If the name of the locale is not defined, use current locale name of main object.
     *
     * @param null|string $locale CLocale id.
     * @param boolean $existOnly Do not create a new translation if it is not in the model.
     * @return false|CActiveRecord Translation object.
     */
    private function _loadTranslation($locale = null, $existOnly = false)
    {
        $locale = $this->_normalizeLocale($locale);
        if (!$this->_issetTranslation($locale)) {
            $class = $this->getTranslationClassName();
            if (!$this->getIsNewRecord()) {
                $translation = $this->_createNewTranslationObject($locale)->findByAttributes(
                    $this->_getTranslationTablePrimaryKey($locale)
                );
                if ($translation !== null) {
                    $translation->setScenario($this->getScenario());
                    $this->_setTranslation($translation, $locale);
                } elseif (!$existOnly) {
                    $this->_createNewTranslation($locale);
                }
            } elseif (!$existOnly) {
                $this->_createNewTranslation($locale);
            }
        }
        return $this->_getTranslation($locale);
    }

    private function _createNewTranslation($locale)
    {
        $translation = $this->_createNewTranslationObject($locale);
        $this->_setTranslation($translation, $locale);
    }

    private function _createNewTranslationObject($locale, $scenario = 'insert')
    {
        $className = $this->getTranslationClassName();

        $translation = new $className($scenario, $this->_getTranslationTableName());
        $translation->locale = $locale;
        $translation->setAttributeLabels($this->attributeLabels())
            ->setPrimaryKey(array_keys($this->_getTranslationTablePrimaryKey()));
        return $translation;
    }

    /**
     * Checking that translation was loaded.
     *
     * If the name of the locale is not defined, use current locale name of main object.
     *
     * @param null|string $locale CLocale id.
     * @return boolean
     */
    private function _issetTranslation($locale = null)
    {
        $locale = $this->_normalizeLocale($locale);
        return isset($this->i18n[$locale]);
    }

    /**
     * Unset exist translation object.
     *
     * If the name of the locale is not defined, use current locale name of main object.
     *
     * @param null|string $locale CLocale id.
     */
    private function _unsetTranslation($locale = null)
    {
        $locale = $this->_normalizeLocale($locale);
        unset($this->i18n[$locale]);
    }

    /**
     * Get translation object for current locale.
     *
     * If the name of the locale is not defined, use current locale name of main object.
     *
     * @param null|string $locale CLocale id.
     * @return false|CActiveRecord Translation object, if exist.
     */
    private function _getTranslation($locale = null)
    {
        $locale = $this->_normalizeLocale($locale);
        return ($this->_issetTranslation($locale) ? $this->i18n[$locale] : false);
    }

    /**
     * Set translation object for current locale.
     *
     * If the name of the locale is not defined, use current locale name of main object.
     * If a translation already exists, exception will be generated.
     *
     * @param CActiveRecord $translation Translation object
     * @param null|string $locale CLocale id.
     * @return CActiveRecord Current translation object.
     * @throws CException
     */
    private function _addTranslation($translation, $locale = null)
    {
        $locale = $this->_normalizeLocale($locale);
        if ($this->_issetTranslation($locale)) {
            throw new CException('Translation for locale "' . $locale . '" already exist!');
        }
        return $this->i18n[$locale] = $translation;
    }

    /**
     * Set translation object for current locale.
     *
     * If the name of the locale is not defined, use current locale name of main object.
     * If a translation already exists, it will be replaced.
     *
     * @param CActiveRecord $translation Translation object
     * @param null|string $locale CLocale id.
     * @return CActiveRecord Current translation object.
     * @throws CException
     */
    private function _setTranslation($translation, $locale = null)
    {
        $locale = $this->_normalizeLocale($locale);
        if ($this->_issetTranslation($locale)) {
            if (!$this->_deleteTranslation($locale)) {
                file_put_contents('/tmp/i18n.log', print_r($this, true));
                throw new CException(
                    "Translation for locale '" . $locale . "' already exist!."
                    . "When replacing an existing translation error occurred!"
                );
            }
        } else {
            $this->_addTranslation($translation, $locale);
        }
        return $translation;
    }

    /**
     * Delete translation from DB.
     *
     * If the name of the locale is not defined, use current locale name of main object.
     *
     * @param  null|string  $locale CLocale id.
     * @return boolean Is deletion was successful.
     */
    private function _deleteTranslation($locale = null)
    {
        return (boolean) $this->_createNewTranslationObject($this->getLocale(), $this->_getTranslationTableName())->deleteAllByAttributes(
            $this->_getTranslationTablePrimaryKey($locale)
        );
    }

    /**
     * Parsing property name of the desired locale and the real name of the property.
     *
     * Used to be able to query the values of properties for a specific locale,
     * without having to switch the locale of the main object.
     *
     * @param string $name
     * @return array
     */
    private function _parsingPropertyName($name)
    {
        $returnArray = [];
        $parts = explode(self::PROPERTY_NAME_DELIMITER, $name);
        if (count($parts) == 2) {
            $returnArray['locale'] = $parts[0];
            $returnArray['name'] = $parts[1];
        } else {
            $returnArray['locale'] = $this->getLocale();
            $returnArray['name'] = $name;
        }
        return $returnArray;
    }

    private function _getTranslationTableName()
    {
        return rtrim($this->tableName(), '}') . '_i18n}}';
    }
}
