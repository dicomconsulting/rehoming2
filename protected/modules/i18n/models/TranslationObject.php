<?php

class TranslationObject extends CActiveRecord
{
    public function __construct($scenario = 'insert', $tableName = null)
    {
        $this->setTableName($tableName);
        parent::__construct($scenario);
    }

    private $tableName;

    private $primaryKey;

    private $attributeLabels = [];

    /**
     * @param array $attributeLabels
     * @return TranslationObject
     */
    public function setAttributeLabels($attributeLabels)
    {
        $this->attributeLabels = $attributeLabels;
        return $this;
    }

    /**
     * @param array $primaryKey
     * @return TranslationObject
     */
    public function setPrimaryKey($primaryKey)
    {
        $this->primaryKey = $primaryKey;
        return $this;
    }

    /**
     * @param string $tableName
     * @return TranslationObject
     */
    public function setTableName($tableName)
    {
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * @return string the associated database table name
     * @throws CException
     */
    public function tableName()
    {
        if (empty($this->tableName)) {
            throw new CException('Name for translation table can not be empty.' . $this->tableName);
        }
        return $this->tableName;
    }

    /*public function primaryKey()
    {
        if (empty($this->primaryKey)) {
            throw new CException('Primary key for translation table can not be empty.');
        }
        return $this->primaryKey;
    }*/

    public function attributeLabels()
    {
        return $this->attributeLabels;
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getMetaData()
    {
        $this->refreshMetaData();
        return parent::getMetaData();
    }

    protected function instantiate($attributes)
    {
        $class = get_class($this);
        $model = new $class(null, $this->tableName());
        $model->setAttributeLabels($this->attributeLabels())
            ->setPrimaryKey($this->primaryKey());
        return $model;
    }
}
