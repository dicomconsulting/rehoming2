<?php
Yii::import('dataSource.components.Interfaces.SpiderInterface');
Yii::import('dataSource.components.BiotronikSpiderException');
Yii::import('dataSource.models.Pages.SignInPage');

class BiotronikSpider extends CComponent implements SpiderInterface
{
    const HTTP = 'http://';
    const HTTPS = 'https://';

    private $domainName;

    private $homePage = 'hmsc_guiWeb/user/site/Home.jsf';

    private $signInPage = 'hmsc_guiWeb/user/site/SignIn.jsf';

    private $securitySignOutPage = 'hmsc_guiWeb/security/SecuritySignOut.jsf';

    private $pathForCookies = 'application.runtime.cookies';

    private $cookieJarFilePrefix = 'biotronik.cookies';

    private $userGroup;

    private $userName;

    private $userPassword;

    private $backupUserGroup;

    private $backupUserName;

    private $backupUserPassword;

    public $customConnectionConfig = [];

    private $defaultConfiguration = array(
        'connect_timeout' => 30,
        'adapter'         => 'curl',
        'ssl_verify_peer' => false,
        'ssl_verify_host' => false,
    );

    /** @var HTTP_Request2 */
    private $requestObjectPrototype;

    public function init()
    {
        $this->applyCurrentUserGroup();
    }

    public function applyCurrentUserGroup()
    {
        if ($currentGroup = Yii::app()->userGroupManager->getCurrentGroup()) {
            $this->setUserName($currentGroup->admin_name);
            $this->setUserPassword($currentGroup->admin_pass);
            $this->setUserGroup($currentGroup->name);
        }
    }

    public function authenticate()
    {
        Yii::log('Начало процесса аутентификации');

        if (!$this->isAuthenticated()) {

            Yii::log('Начинаем попытку аутентифицироваться');
            Yii::log('Переход на страницу входа в систему');
            $request = $this->getNewRequestObject();
            $request->setUrl(
                $this->getDomainName(true, true, true) . $this->signInPage
            );
            $response = $request->send();
            $responseBody = $response->getBody();
            $signInPage = new SignInPage($responseBody);

            $request->setMethod(HTTP_Request2::METHOD_POST);
            $request->addPostParameter(
                [
                    'SignInForm'            => 'SignInForm',
                    'SignInForm:userGroup'  => $this->getUserGroup(),
                    'SignInForm:userName'   => $this->getUserName(),
                    'SignInForm:password'   => $this->getUserPassword(),
                    'SignInForm:signIn'     => 'Sign in',
                    'javax.faces.ViewState' => $signInPage->getViewState(),
                ]
            );
            $response = $request->send();

            Yii::log('Отправка данных для аутентификации');

            if ($response->getStatus() == 302
                && stripos($response->getHeader('location'), $this->homePage)
            ) {

                Yii::log('Вход в систему выполнен');

                $this->_saveCookies($request->getCookieJar());

                Yii::log('Сохранение данных для открытой сессии');

            } else {

                Yii::log('Вход в систему провален');

                $signInPage->setContent($response->getBody());
                $errorCode = $signInPage->getErrorCode();
                if (isset($errorCode)) {
                    throw new BiotronikSpiderException(
                        $signInPage->getErrorText(),
                        $errorCode
                    );
                } else {
                    throw new Exception('Authentication filed!');
                }
            }
        }
        return true;
    }

    public function getNewRequestObject()
    {
        $this->_createRequestObject();
        return clone $this->requestObjectPrototype;
    }

    public function setDomainName($domainName)
    {
        $this->domainName = $domainName;
    }

    public function getDomainName($prependProtocol = false, $https = false, $trailingSlash = false)
    {
        return ($prependProtocol ? ($https ? self::HTTPS : self::HTTP) : '')
        . $this->domainName
        . ($trailingSlash ? '/' : '');
    }

    public function setCookieJarFilePrefix($cookieJarFilePrefix)
    {
        $this->cookieJarFilePrefix = $cookieJarFilePrefix;
    }

    public function getCookieJarFilePrefix()
    {
        return $this->cookieJarFilePrefix;
    }

    public function setUserGroup($userGroup)
    {
        if (Yii::app()->userGroupManager->isAllowedUserGroup($userGroup)) {
            $this->userGroup = $userGroup;
        } else {
            throw new BiotronikSpiderException('You are trying to assign an illegal group.');
        }
        $this->_resetRequestObject();
    }

    public function getUserGroup()
    {
        return $this->userGroup;
    }

    public function setUserName($userName)
    {
        $this->userName = $userName;
        $this->_resetRequestObject();
        $this->_clearCookie();
    }

    public function getUserName()
    {
        return $this->userName;
    }

    public function setUserPassword($userPassword)
    {
        $this->userPassword = $userPassword;
    }

    public function getUserPassword()
    {
        return $this->userPassword;
    }

    public function backupAuthenticationCredentials()
    {
        $this->backupUserGroup = $this->userGroup;
        $this->backupUserName = $this->userName;
        $this->backupUserPassword = $this->userPassword;
    }

    public function restoreAuthenticationCredentials()
    {
        $this->setUserGroup($this->backupUserGroup ? : $this->userGroup);
        $this->setUserName($this->backupUserName ? : $this->userName);
        $this->setUserPassword($this->backupUserPassword ? : $this->userPassword);
    }

    private function _createRequestObject()
    {
        if (!$this->requestObjectPrototype instanceof HTTP_Request2) {
            $this->requestObjectPrototype = new HTTP_Request2(
                null,
                HTTP_Request2::METHOD_GET,
                CMap::mergeArray($this->defaultConfiguration, $this->customConnectionConfig)
            );
            $this->_addCookieJar($this->requestObjectPrototype);
        }
    }

    private function _resetRequestObject()
    {
        $this->requestObjectPrototype = null;
    }

    private function _clearCookie()
    {
        if (is_file($this->_getCookieJarFilePath())) {
            unlink($this->_getCookieJarFilePath());
        }
    }

    /**
     * Check exist session.
     *
     * Verifies that the user is already authenticated in the system.
     *
     * @return boolean
     */
    private function isAuthenticated()
    {
        Yii::log('Проверка существующей сессии');
        $request = $this->getNewRequestObject();
        $homePage = $this->getDomainName(true, true, true) . $this->homePage;
        $request->setUrl($homePage);
        $response = $request->send();
        if ($response->getStatus() == 302
            && (stripos($response->getHeader('location'), $this->signInPage)
                || stripos($response->getHeader('location'), $this->securitySignOutPage))
        ) {
            Yii::log('Активная сессия отсутствует');
            return false;
        } else {
            Yii::log('Текущая сессия активна');
            return true;
        }
    }

    /**
     * Add cookie jar file with saved cookies
     *
     * @param HTTP_Request2 $request
     */
    private function _addCookieJar(HTTP_Request2 $request)
    {
        $cookieJar = new HTTP_Request2_CookieJar();
        $cookieJar->serializeSessionCookies(true);
        $this->_loadCookies($cookieJar);
        $request->setCookieJar($cookieJar);
    }

    /**
     * Load cookies from file
     *
     * @param HTTP_Request2_CookieJar $cookieJar
     */
    private function _loadCookies(HTTP_Request2_CookieJar $cookieJar)
    {
        $cookieJarFileHandle = $this->_getCookieJarFileHandle();
        $fileSize = filesize($this->_getCookieJarFilePath());
        if ($fileSize > 0) {
            $cookieJar->unserialize(fread($cookieJarFileHandle, $fileSize));
        }
        fclose($cookieJarFileHandle);
    }

    /**
     * Save cookies to file
     *
     * @param HTTP_Request2_CookieJar $cookieJar
     */
    private function _saveCookies(HTTP_Request2_CookieJar $cookieJar)
    {
        $cookieJarFileHandle = $this->_getCookieJarFileHandle();
        fwrite($cookieJarFileHandle, $cookieJar->serialize());
        fclose($cookieJarFileHandle);
    }

    /**
     * Return file handle with cookies jar serialize data
     *
     * @return resource
     * @throws Exception
     */
    private function _getCookieJarFileHandle()
    {
        $cookieJarFile = $this->_getCookieJarFilePath();
        $cookieJarFileHandle = @fopen($cookieJarFile, 'r+');

        if (!$cookieJarFileHandle) {
            $folderForCookies = $this->_getFolderForCookies();

            if (!file_exists($folderForCookies)) {
                mkdir($folderForCookies);
            }

            if (!is_writable($folderForCookies)) {
                throw new Exception($folderForCookies . ' must be writable');
            }

            $cookieJarFileHandle = @fopen($cookieJarFile, 'w+');
            if (!$cookieJarFileHandle) {
                throw new Exception("Can't create '" . $cookieJarFile . "' with writable access.");
            }
        }
        return $cookieJarFileHandle;
    }

    private function _getFolderForCookies()
    {
        return Yii::getPathOfAlias($this->pathForCookies);
    }

    private function _getCookieJarFilePath()
    {
        return $this->_getFolderForCookies() . DIRECTORY_SEPARATOR . $this->_getCookieJarFileName();
    }

    private function _getCookieJarFileName()
    {
        return $this->cookieJarFilePrefix . '-' . $this->getUserName();
    }
}
