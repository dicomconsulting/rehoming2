<?php

/**
 * Interface for web spiders
 *
 * Each web spider's class must implement this interface
 */
interface SpiderInterface
{
    /**
     * Authenticate spider in the system.
     *
     * Return true, if the authentication was successful.
     * Otherwise false.
     * Throws an exception if an error occurs.
     *
     * @return boolean Authentication's result
     * @throws Exception
     */
    public function authenticate();

    /**
     * Return new request object instance
     *
     * @return HTTP_Request2 Request object
     */
    public function getNewRequestObject();

    /**
     * Return domain name for current spider class
     *
     * Prepend protocol (http|https) $prependProtocol is true.
     * Add https (if $https is true) or http (if $https is false) protocol name,
     * if $prependProtocol equal true. Add trailing slash, if $trailingSlash is true.
     *
     * @param boolean $prependProtocol
     * @param boolean $https
     * @param boolean $trailingSlash
     *
     * @return string Domain full name
     */
    public function getDomainName($prependProtocol = false, $https = false, $trailingSlash = false);
}
