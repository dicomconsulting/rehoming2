<?php

class DataSourceModule extends CWebModule
{
     /**
     * Initializes the module.
     */
    public function init()
    {
        Yii::setPathOfAlias('dataSource', dirname(__FILE__));
        $this->setImport(
            array(
                'dataSource.components.*',
                'dataSource.models.*',
            )
        );
    }
}
