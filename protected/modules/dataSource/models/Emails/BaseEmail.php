<?php

abstract class BaseEmail
{
    /**
     *
     * @var \Ddeboer\Imap\Message
     */
    protected $message;

    public function __construct(\Ddeboer\Imap\Message  $message)
    {
        $this->message = $message;
    }

    public static function getEmailTypeObject(\Ddeboer\Imap\Message  $message)
    {
        if (NewPatientEmail::checkType($message)) {
            return new NewPatientEmail($message);
        } elseif (NewFindingEmail::checkType($message)) {
            return new NewFindingEmail($message);
        }

        return false;
    }

    abstract public function process();
}
