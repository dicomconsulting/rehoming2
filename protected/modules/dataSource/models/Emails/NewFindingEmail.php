<?php
Yii::import('application.modules.dataSource.models.Tasks.*');
Yii::import('patient.models.*');
Yii::import('device.models.*');

class NewFindingEmail extends BaseEmail implements EmailInterface
{
    /**
     * Check type of email message.
     *
     * True, if type of the email message is same, otherwise false
     *
     * @param \Ddeboer\Imap\Message $message
     * @return boolean
     */
    public static function checkType(\Ddeboer\Imap\Message $message)
    {
        echo $message->getSubject() . "\n";
        return preg_match('/^Patient(.*)(New finding:)?/iU', $message->getSubject());
    }

    public function process()
    {
        $statusMessage = new StatusMessage();
        $emailText = $this->message->getBodyText();
        if (($deviceId = $this->_getDeviceId($emailText)) != false) {
            $statusMessage->device_id = $deviceId;
            $statusMessage->type = $statusMessage->getStatusTypeByName(
                $this->_getTypeName($emailText)
            );
            $statusMessage->finding = $this->_getFinding($emailText);
            $statusMessage->create_time = $this->message->getDate()->format('Y-m-d H:i:s');
            $statusMessage->save();
        }
    }

    private function _getDeviceId($emailText)
    {
        preg_match('/Patient:(.*)with (.*)\/(.*)/i', $emailText, $matches);
        $humanReadebleId = trim($matches[1]);
        $deviceModel = trim($matches[2]);
        $serialNumber = trim($matches[3]);
        $patient = Patient::model()->findByAttributes(
            ['human_readable_id' => $humanReadebleId]
        );
        if (!empty($patient)) {
            $device = Device::model()->findByAttributes(
                [
                    'serial_number' => $serialNumber,
                    'patient_id' => $patient->uid,
                    'device_model' => $deviceModel
                ]
            );
            if (!empty($device)) {
                return $device->uid;
            }
        } else {
            echo "Patient does't exist in the system: " . $humanReadebleId;
        }

        return false;
    }

    private function _getTypeName($emailText)
    {
        preg_match('/Status:(.*)/i', $emailText, $matches);
        return trim($matches[1]);
    }

    private function _getFinding($emailText)
    {
        $strings = explode("\n", $emailText);
        preg_match('/^(New finding:)?(.*)\s/i', $strings[4], $matches);
        return trim($matches[2]);
    }
}
