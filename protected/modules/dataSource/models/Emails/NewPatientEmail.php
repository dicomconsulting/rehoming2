<?php
Yii::import('application.modules.dataSource.models.Tasks.*');

class NewPatientEmail extends BaseEmail implements EmailInterface
{
    private static $needleSubject = 'New Home Monitoring patients.';
    /**
     * Check type of email message.
     *
     * True, if type of the email message is same, otherwise false
     *
     * @param \Ddeboer\Imap\Message $message
     * @return boolean
     */
    public static function checkType(\Ddeboer\Imap\Message $message)
    {
        return $message->getSubject() == static::$needleSubject;
    }

    public function process()
    {
        $getPatientIdByPatientUniqueNameTask = new GetPatientIdByPatientUniqueNameTask();
        $updatePatientByPatientIdTask = new UpdatePatientByPatientIdTask();
        $updatePatientByPatientIdTask->run([
            'patientId' => $getPatientIdByPatientUniqueNameTask->run([
                'patientName' => $this->getHumanReadableId()
            ])
        ]);
    }

    public function getHumanReadableId()
    {
        preg_match('/\\n(.*) with/U', $this->message->getBodyText(), $matches);
        return $matches[1];
    }
}
