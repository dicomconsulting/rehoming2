<?php
Yii::import('dataSource.models.Pages.*');
Yii::import('patient.models.*');
Yii::import('device.models.*');

class DownloadECGImageTask extends BiotronikSpiderBaseListTask
{
    protected $requiredFieldNames = ['deviceId', 'episodeNumber'];

    /**
     * @var RecordingListPage
     */
    private $recordingListPage;

    /**
     * @var RecordingViewPage
     */
    private $recordingViewPage;

    public function __construct()
    {
        parent::__construct();
        $this->setListUrl('hmsc_guiWeb/patient/monitoring/DisplayPatientContext.jsf');
    }

    /**
     * Update history information for the patient.
     *
     * The parameter array must contain the following elements:
     * patientId - [string] unique system identifier of the patient
     *
     * @param array $parameters Array of needle parameters.
     */
    public function run($parameters = array())
    {
        $this->_checkRequiredParameters($parameters);
        $this->getSpider()->authenticate();

        $deviceId = $parameters['deviceId'];
        $episodeNumber = $parameters['episodeNumber'];
        $device = Device::model()->findByPk($deviceId);
        $patient = Patient::model()->findByPk($device->patient_id);
        if ($patient) {
            $this->_loadRecordingListPage($patient);
            $this->_findRecordingRow($patient, $episodeNumber);
            $this->_loadRecordingViewPage($patient, $episodeNumber);
            $response = $this->_downloadECGImage();
            $this->_saveImage($response, $deviceId, $episodeNumber);
        }
    }

    /**
     * @param RecordingListPage $recordingListPage
     */
    public function setRecordingListPage(RecordingListPage $recordingListPage)
    {
        $this->recordingListPage = $recordingListPage;
    }

    /**
     * @return RecordingListPage
     */
    public function getRecordingListPage()
    {
        return $this->recordingListPage;
    }

    /**
     * @param RecordingViewPage $recordingViewPage
     */
    public function setRecordingViewPage($recordingViewPage)
    {
        $this->recordingViewPage = $recordingViewPage;
    }

    /**
     * @return RecordingViewPage
     */
    public function getRecordingViewPage()
    {
        return $this->recordingViewPage;
    }

    protected function goToPage(BasePage $page, $postParameters, $parametersPrefix = '')
    {
        $postParameters = $this->mergePostParameters($page, $postParameters, $parametersPrefix);
        unset($postParameters[$parametersPrefix . ':defaultAction']);
        unset($postParameters[$parametersPrefix . ':searchString']);
        $request = $this->getSpider()->getNewRequestObject();
        $request->setConfig('follow_redirects', true);
        $request->setUrl(
            $this->getSpider()->getDomainName(true, true, true)
            . $this->getListUrl()
        );
        $request->setMethod(HTTP_Request2::METHOD_POST);
        $request->addPostParameter($postParameters);
        $this->_injectFilterParametersToRequest(
            $request,
            $this->getRecordingListPage()->getFilterParameters()
        );
        $response = $request->send();
        $page->setContent($response->getBody());
    }

    private function _loadRecordingListPage(Patient $patient)
    {
        $request = $this->getSpider()->getNewRequestObject();
        $request->setUrl($this->getSpider()->getDomainName(true, true, true) . $this->getListUrl());
        $url = $request->getUrl();
        $url->setQueryVariable('PatientIdentifier', $patient->external_uid);
        $url->setQueryVariable('TopTabIdentifier', 'HOLTER');
        $url->setQueryVariable('LowTabIdentifier', 'HOLTER_EPISODES_LIST');
        $response = $request->send();
        $this->setRecordingListPage(
            new RecordingListPage($response->getBody())
        );
    }

    private function _findRecordingRow(Patient $patient, $episodeNumber)
    {
        if (!$this->_isRecordingListPageWasLoaded()) {
            throw new Exception("Recording list page must was loaded previously.");
        }
        $patientId = $patient->external_uid;
        $existNeedleEpisode = $this->getRecordingListPage()->existNeedleEpisode($episodeNumber);
        if ($existNeedleEpisode === false) {
            $this->_setNeedleFilterParameters($patientId);
            $existNeedleEpisode = $this->getRecordingListPage()->existNeedleEpisode($episodeNumber);
            $existNextPage = $this->getRecordingListPage()->existNextPage('DisplayEpisodeList');
        }
        while ($existNeedleEpisode === false && $existNextPage) {
            $this->loadNextPage(
                $this->getRecordingListPage(),
                [
                    'LowTabIdentifier'  => 'HOLTER_EPISODES_LIST',
                    'PatientIdentifier' => $patientId,
                    'TopTabIdentifier'  => 'HOLTER'
                ],
                'DisplayEpisodeList'
            );
            $existNextPage = $this->getRecordingListPage()->existNextPage('DisplayEpisodeList');
            $existNeedleEpisode = $this->getRecordingListPage()->existNeedleEpisode($episodeNumber);
        }
        if ($existNeedleEpisode === false) {
            throw new CException("Episode #{$episodeNumber} for patient #{$patientId}  doesn't exist.");
        }
    }

    private function _loadRecordingViewPage(Patient $patient, $episodeNumber)
    {
        $identification = $this->getRecordingListPage()->getNeedleRecordIdentification($episodeNumber);
        $postParameters = [
            $identification     => '',
            'LowTabIdentifier'  => 'HOLTER_EPISODES_LIST',
            'PatientIdentifier' => $patient->external_uid,
            'TopTabIdentifier'  => 'HOLTER'
        ];
        $parametersPrefix = 'DisplayEpisodeList';
        $postParameters = $this->mergePostParameters($this->getRecordingListPage(), $postParameters, $parametersPrefix);
        unset($postParameters[$parametersPrefix . ':defaultAction']);
        unset($postParameters[$parametersPrefix . ':searchString']);
        $request = $this->getSpider()->getNewRequestObject();
        $request->setConfig('follow_redirects', true);
        $request->setUrl(
            $this->getSpider()->getDomainName(true, true, true)
            . $this->getListUrl()
        );
        $request->setMethod(HTTP_Request2::METHOD_POST);
        $request->addPostParameter($postParameters);
        $this->_injectFilterParametersToRequest(
            $request,
            $this->getRecordingListPage()->getFilterParameters()
        );
        $response = $request->send();
        $this->setRecordingViewPage(
            new RecordingViewPage($response->getBody())
        );
    }

    private function _downloadECGImage()
    {
        $imageUrl = $this->getRecordingViewPage()->getECGImageUrl();
        $request = $this->getSpider()->getNewRequestObject();
        $request->setUrl(
            $this->getSpider()->getDomainName(true, true, true)
            . ltrim($imageUrl, '/')
        );
        $response = $request->send();
        return $response;
    }

    private function _saveImage(HTTP_Request2_Response $response, $deviceId, $episodeNumber)
    {
        $ecgImage = new ECGImage();
        $ecgImage->createImage($response->getBody());
        $ecgImage->device_id = $deviceId;
        $ecgImage->episode_number = $episodeNumber;
        $ecgImage->save();
    }

    private function _setNeedleFilterParameters($patientId)
    {
        $this->goToPage(
            $this->getRecordingListPage(),
            [
                'DisplayEpisodeList:filterViewEpisodeListOverviewButton' => '',
                'LowTabIdentifier'                                       => 'HOLTER_EPISODES_LIST',
                'PatientIdentifier'                                      => $patientId,
                'TopTabIdentifier'                                       => 'HOLTER'
            ],
            'DisplayEpisodeList'
        );
    }

    private function _injectFilterParametersToRequest(HTTP_Request2 $request, $filterParameters)
    {
        $requestBody = $request->getBody();
        foreach ($filterParameters as $parameterName => $filterParameter) {
            foreach ($filterParameter as $parameterValue) {
                $requestBody .= '&' . $parameterName . '=' . $parameterValue;
            }
        }
        $request->setBody($requestBody);
    }

    private function _isRecordingListPageWasLoaded()
    {
        return ($this->recordingListPage instanceof RecordingListPage);
    }
}
