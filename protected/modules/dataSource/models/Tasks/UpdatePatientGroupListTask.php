<?php
Yii::import('dataSource.models.Pages.*');
Yii::import('patient.models.*');

class UpdatePatientGroupListTask extends BiotronikSpiderBaseListTask
{
    /**
     * @var string New patient form page URI.
     */
    private $newPatientFormUrl = 'hmsc_guiWeb/patient/administration/CreatePatient.jsf';

    public function __construct()
    {
        parent::__construct();
        $this->setListUrl('hmsc_guiWeb/patientgroup/administration/DisplayPatientGroupOverview.jsf');
    }

    /**
     * Create/update group's list from biotronik's data.
     *
     * @param [] $parameters
     * @return \PatientGroup[]
     */
    public function run($parameters = [])
    {
        $this->getSpider()->authenticate();
        $request = $this->getSpider()->getNewRequestObject();
        $request->setConfig('follow_redirects', true);
        $request->setUrl($this->getSpider()->getDomainName(true, true, true) . $this->newPatientFormUrl);
        $response = $request->send();

        $newPatientFormPage = new NewPatientFormPage($response->getBody());
        $patientGroupDataArray = $newPatientFormPage->getPatientGroupDataArray();
        $patientGroupList = array();

        foreach ($patientGroupDataArray as $groupUid => $groupName) {
            $patientGroup = $this->_createPatientGroup($groupUid, $groupName);
            $patientGroup->save();
            $patientGroupList[] = $patientGroup;
        }

        return $patientGroupList;
    }

    /**
     * Create patient group entity from unique data.
     *
     * @param string $groupUid
     * @param string $groupName
     * @return \PatientGroup
     */
    private function _createPatientGroup($groupUid, $groupName)
    {
        $patientGroup = PatientGroup::model()->findByAttributes(['external_uid' => $groupUid]);

        if (empty($patientGroup)) {
            $patientGroup = PatientGroup::model()->findByAttributes(['name' => $groupName]);
        }

        if (empty($patientGroup)) {
            $patientGroup = new PatientGroup();
        }

        $patientGroup->name = $groupName;
        $patientGroup->external_uid = $groupUid;
        $this->_addComment($patientGroup);
        return $patientGroup;
    }

    /**
     * Add comment value to patient group object.
     *
     * @param PatientGroup $patientGroup
     */
    private function _addComment(PatientGroup $patientGroup)
    {
        $request = $this->getSpider()->getNewRequestObject();
        $request->setConfig('follow_redirects', true);
        $request->setUrl(
            $this->getSpider()->getDomainName(true, true, true)
            . $this->getListUrl()
        );
        $response = $request->send();

        $patientGroupListPage = new PatientGroupListPage($response->getBody());

        $allowFirstPageButton = $patientGroupListPage->allowFirstPageButton('DisplayPatientGroupOverview');
        if ($allowFirstPageButton) {
            $this->reloadList($patientGroupListPage, null, 'DisplayPatientGroupOverview');
        }
        $comment = $patientGroupListPage->findCommentByPatientGroupName($patientGroup->name);
        $existNextPage = $patientGroupListPage->existNextPage('DisplayPatientGroupOverview');
        while ($comment == null && $existNextPage) {
            $this->loadNextPage($patientGroupListPage, null, 'DisplayPatientGroupOverview');
            $comment = $patientGroupListPage->findCommentByPatientGroupName($patientGroup->name);
            $existNextPage = $patientGroupListPage->existNextPage('DisplayPatientGroupOverview');
        }

        $patientGroup->comment = $comment;
    }
}
