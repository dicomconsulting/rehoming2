<?php
Yii::import('application.modules.dataSource.models.Pages.*');
Yii::import('application.modules.device.models.*');
Yii::import('application.modules.patient.models.*');

class UpdatePatientByPatientIdTask extends BiotronikSpiderBaseTask
{
    protected $requiredFieldNames = ['patientId'];

    private $patientViewPageUrl = 'hmsc_guiWeb/patient/monitoring/DisplayPatientContext.jsf';

    /**
     * @var PatientVewPage
     */
    private $patientViewPage = null;

    private $patientFieldNames = array(
        'human_readable_id',
        'name',
        'middle_name',
        'surname',
        'comment',
        'group_id',
        'consent',
        'sex',
        'title'
    );

    public function run($parameters = [])
    {
        $this->_checkRequiredParameters($parameters);
        $patientId = $parameters['patientId'];
        $this->_loadPatientViewPage($patientId);

        $patient = Patient::model()->find('external_uid = :patientId', array(':patientId' => $patientId));
        if (empty($patient)) {
            $patient = new Patient();
        }

        $this->_fillMainFields($patient, $patientId);
        $this->_addGroup($patient);
        $this->_addBirthDate($patient);
        $this->_modifyPhones($patient);
        $this->_modifyDevice($patient);
        $this->_modifyContacts($patient, $this->_getPatientViewPage()->getContactPersonsInfo(), ContactPerson::TYPE_ID);
        $this->_modifyContacts($patient, $this->_getPatientViewPage()->getFamilyDoctorsInfo(), FamilyDoctor::TYPE_ID);

        $patient->save();
    }

    private function _setPatientViewPage(PatientViewPage $patientViewPage)
    {
        $this->patientViewPage = $patientViewPage;
    }

    private function _getPatientViewPage()
    {
        return $this->patientViewPage;
    }

    private function _loadPatientViewPage($patientId)
    {
        $this->getSpider()->authenticate();
        $request = $this->getSpider()->getNewRequestObject();
        $request->setMethod(HTTP_Request2::METHOD_GET);
        $request->setConfig('follow_redirects', true);
        $request->setUrl($this->getSpider()->getDomainName(true, true, true) . $this->patientViewPageUrl);
        $url = $request->getUrl();
        $url->setQueryVariable('PatientIdentifier', $patientId);
        $url->setQueryVariable('TopTabIdentifier', 'PATIENT_MAIN_DATA');
        $response = $request->send();
        $this->_setPatientViewPage(new PatientViewPage($response->getBody()));
    }

    /**
     * Set values to main fields
     *
     * @param \Patient $patient
     * @param string $patientId
     */
    private function _fillMainFields(\Patient $patient, $patientId)
    {
        $patient->external_uid = $patientId;
        foreach ($this->patientFieldNames as $fieldName) {
            $patient->$fieldName = $this->_getPatientViewPage()->getAttribute($fieldName);
        }
    }

    /**
     * Add contact phones
     *
     * @param Patient $patient
     */
    private function _modifyPhones(\Patient $patient)
    {
        $numbers = $this->_getPatientViewPage()->getPhones();

        foreach ($numbers as $number) {
            $phone         = new Phone();
            $phone->number = $number;
            $patient->addPhone($phone);
        }

        $phoneArray         = $patient->phones;
        $numbersForDeleting = array_diff(
            array_keys($phoneArray),
            $numbers
        );
        foreach ($numbersForDeleting as $number) {
            $phoneArray[$number]->delete();
            unset($phoneArray[$number]);
        }
        $patient->phones = $phoneArray;
    }

    /**
     * Add device and device model entities, if patient is new
     *
     * @param \Patient $patient
     */
    private function _modifyDevice(\Patient $patient)
    {
        if ($patient->isNewRecord) {
            $device = new Device();
        } else {
            $device = $patient->device;
        }
        $deviceModelName = $this->_getPatientViewPage()->getDeviceModel();
        $deviceModel     = DeviceModel::model()->findByPk($deviceModelName);
        if (empty($deviceModel)) {
            $deviceModel = new DeviceModel();
        }

        $transmitterSN = $this->_getPatientViewPage()->getTransmitterSN();

        $deviceModel->name = $deviceModelName;
        $deviceModel->save();
        $device->serial_number             = $this->_getPatientViewPage()->getDeviceSN();
        $device->implantation_date         = $this->_getPatientViewPage()->getImplantationDate();
        $device->model                     = $deviceModel;
        $device->device_model              = $deviceModelName;
        $device->transmitter_type          = $this->_getPatientViewPage()->getTransmitterModel();
        $device->transmitter_serial_number = $transmitterSN ? $transmitterSN : null;
        $device->active_since_date         = $this->_getPatientViewPage()->getMonitoringStateDate();
        $device->detachBehavior('EAdvancedArBehavior');
        $device->save();

        $patient->device = $device;
    }

    /**
     * Add/update contact persons information
     *
     * @param \Patient $patient
     * @param array $contactPersonsInfo
     * @param integer $type
     */
    private function _modifyContacts(\Patient $patient, $contactPersonsInfo, $type = ContactPerson::TYPE_ID)
    {
        if ($type == ContactPerson::TYPE_ID) {
            $propertyName = 'contact_persons';
        } else {
            $propertyName = 'family_doctors';
        }
        $existContactPersons       = $patient->{$propertyName};
        $updatedContactPersonNames = [];
        foreach ($contactPersonsInfo as $contactPersonInfo) {
            if (empty($contactPersonInfo['number'])) {
                continue;
            }

            if (array_key_exists($contactPersonInfo['name'], $existContactPersons) === false) {
                $updatedContactPersonNames[] = $contactPersonInfo['name'];
                if ($type == 1) {
                    $contactPerson = new ContactPerson();
                } else {
                    $contactPerson = new FamilyDoctor();
                }
                $contactPerson->name = $contactPersonInfo['name'];
                $contactPerson->setPhone($contactPersonInfo['number']);
                $existContactPersons[$contactPersonInfo['name']] = $contactPerson;
            } else {
                $updatedContactPersonNames[]                                    = $contactPersonInfo['name'];
                $existContactPersons[$contactPersonInfo['name']]->phone->number =
                    $contactPersonInfo['number'];

            }
            foreach ($existContactPersons as $name => $contactPerson) {
                if (array_search($name, $updatedContactPersonNames) === false) {
                    $contactPerson->delete();
                    unset($existContactPersons[$name]);
                }
            }
        }

        $patient->{$propertyName} = $existContactPersons;
    }

    /**
     * Assign patient with patient group
     *
     * @param \Patient $patient
     */
    private function _addGroup(\Patient $patient)
    {
        $groupName    = $this->_getPatientViewPage()->getGroupName();
        $patientGroup = PatientGroup::model()->findByAttributes(['name' => $groupName]);

        if (empty($patientGroup)) {
            $patientGroup       = new PatientGroup();
            $patientGroup->name = $groupName;
            $patientGroup->save();
        }
        $patient->group = $patientGroup;
    }

    /**
     * Assign patient with patient group
     *
     * @param \Patient $patient
     */
    private function _addBirthDate(\Patient $patient)
    {
        $patient->birth_date = $this->_getPatientViewPage()->getBirthDate();
    }
}
