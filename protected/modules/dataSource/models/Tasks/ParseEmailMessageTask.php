<?php
Yii::import('application.modules.dataSource.models.Emails.*');

class ParseEmailMessageTask
{
    public function run(\Ddeboer\Imap\Message $message)
    {
        $emailType = BaseEmail::getEmailTypeObject($message);
        if ($emailType instanceof BaseEmail) {
            $emailType->process();
        }
    }
}
