<?php
Yii::import('application.modules.dataSource.models.Pages.*');
Yii::import('dataSource.models.Pages.*');


class GetPatientIdByPatientUniqueNameTask extends BiotronikSpiderBaseListTask
{
    protected $requiredFieldNames = ['patientName'];

    /**
     * Patient list page object.
     *
     * @var PatientsListPage
     */
    private $patientListPage;

    public function __construct()
    {
        parent::__construct();
        $this->setListUrl('hmsc_guiWeb/patient/monitoring/overview/DisplayPatientOverview.jsf');

        $this->getSpider()->authenticate();
        $request = $this->getSpider()->getNewRequestObject();
        $request->setConfig('follow_redirects', true);
        $request->setUrl(
            $this->getSpider()->getDomainName(true, true, true)
            . $this->getListUrl()
        );
        $response = $request->send();

        $this->patientListPage = new PatientsListPage($response->getBody());
    }

    /**
     * Return patient list page object
     *
     * @return PatientsListPage
     */
    public function getPatientListPage()
    {
        return $this->patientListPage;
    }

    public function run($parameters = [])
    {
        $this->_checkRequiredParameters($parameters);
        $patientName = $parameters['patientName'];

        $patientListPage      = $this->getPatientListPage();
        $customPostParameters = [
            'PatientsOverview:selectMonitoringStatusFilter' => 'ALL',
            'PatientsOverview:selectPatientFilter'          => 'ALL_PATIENTS',
            'PatientsOverview:selectPatientGroupFilter'     => 'all',
        ];
        $this->loadSearchResultList($patientListPage, $customPostParameters, 'PatientsOverview', $patientName);

        $existNextPage = $patientListPage->existNextPage('PatientsOverview');
        $patientId     = $patientListPage->findIdByUniqueName($patientName);

        while ($patientId == null && $existNextPage) {
            $this->loadNextPage($patientListPage, $customPostParameters, 'PatientsOverview');
            $patientId     = $patientListPage->findIdByUniqueName($patientName);
            $existNextPage = $patientListPage->existNextPage('PatientsOverview');
        }

        if (empty($patientId)) {
            throw new CException("Wrong patient name!");
        }

        return $patientId;
    }
}
