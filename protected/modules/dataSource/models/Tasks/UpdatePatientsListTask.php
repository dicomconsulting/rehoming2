<?php
Yii::import('application.modules.dataSource.models.Pages.*');
Yii::import('dataSource.models.Pages.*');


class UpdatePatientsListTask extends BiotronikSpiderBaseListTask
{
    protected $requiredFieldNames = ['patientName'];

    /**
     * Patient list page object.
     *
     * @var PatientsListPage
     */
    private $patientListPage;

    public function __construct()
    {
        parent::__construct();
        $this->setListUrl('hmsc_guiWeb/patient/monitoring/overview/DisplayPatientOverview.jsf');

        $this->getSpider()->authenticate();

        $request = $this->getSpider()->getNewRequestObject();
        $request->setConfig('follow_redirects', true);
        $request->setUrl(
            $this->getSpider()->getDomainName(true, true, true)
            . $this->getListUrl()
        );
        $response = $request->send();
        $body = $response->getBody();

        $this->patientListPage = new PatientsListPage($body);
    }

    /**
     * Return patient list page object
     *
     * @return PatientsListPage
     */
    public function getPatientListPage()
    {
        return $this->patientListPage;
    }

    public function run($parameters = [])
    {
        $patientListPage = $this->getPatientListPage();
        $customPostParameters = [
            'PatientsOverview:selectMonitoringStatusFilter' => 'ACTIVATED',
            'PatientsOverview:selectPatientFilter'          => 'ALL_PATIENTS',
            'PatientsOverview:selectPatientGroupFilter'     => 'all'
        ];
        $this->loadSearchResultList($patientListPage, $customPostParameters, 'PatientsOverview');

        $existNextPage = $patientListPage->existNextPage('PatientsOverview');
        $patientsList = $patientListPage->getPatientsList();

        $this->processList($patientsList);

        while ($existNextPage) {
            $this->loadNextPage($patientListPage, $customPostParameters, 'PatientsOverview');
            $this->processList($patientListPage->getPatientsList());

            $existNextPage = $patientListPage->existNextPage('PatientsOverview');
        }

        return true;
    }

    private function processList($patientsList)
    {
        foreach ($patientsList as $patientHumanReadableId => $patientId) {
            /** @var Patient $patient */
            $patient = Patient::model()->findByAttributes(['external_uid' => $patientId]);

            if (!$patient || $patient->human_readable_id != $patientHumanReadableId) {
                echo "Updating patient " . $patientHumanReadableId . "\n";
                try {
                    $updatePatientByPatientIdTask = new UpdatePatientByPatientIdTask();
                    $updatePatientByPatientIdTask->run(
                        [
                            'patientId' => $patientId
                        ]
                    );
                } catch (CException $e) {
                    echo $e->getMessage() . ', Patient name - ' . $patientHumanReadableId . "\n";
                }

            }
        }
    }
}
