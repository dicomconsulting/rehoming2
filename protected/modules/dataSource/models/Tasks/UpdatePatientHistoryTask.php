<?php
Yii::import('dataSource.models.Pages.*');
Yii::import('patient.models.HistoryMessage');

class UpdatePatientHistoryTask extends BiotronikSpiderBaseListTask
{
    protected $requiredFieldNames = ['patientId', 'deviceId'];

    private $patientHistoryPageUri = 'hmsc_guiWeb/patient/monitoring/DisplayPatientContext.jsf';

    private $patientHistoryPage;

    const ERROR_MARKER = 'If this error occurs again, please call your Customer Service specialist';

    public function __construct()
    {
        parent::__construct();
        $this->setListUrl('hmsc_guiWeb/patient/monitoring/DisplayPatientContext.jsf');
    }

    /**
     * Update history information for the patient.
     *
     * The parameter array must contain the following elements:
     * patientId - [string] unique system identifier of the patient
     *
     * @param array $parameters Array of needle parameters.
     */
    public function run($parameters = array())
    {
        $this->_checkRequiredParameters($parameters);
        $this->getSpider()->authenticate();

        $patientId = $parameters['patientId'];
        $deviceId = $parameters['deviceId'];
        if ($this->_loadPatientHistoryPage($patientId)) {
            $this->_updatePatientHistory($deviceId, $patientId);
        } else {
            throw new CException("Patient skipped");
        }
    }

    public function setPatientHistoryPage(PatientHistoryPage $patientHistoryPage)
    {
        $this->patientHistoryPage = $patientHistoryPage;
    }

    /**
     * @return PatientHistoryPage
     */
    public function getPatientHistoryPage()
    {
        return $this->patientHistoryPage;
    }

    private function _loadPatientHistoryPage($patientId)
    {
        $request = $this->getSpider()->getNewRequestObject();
        $request->setConfig('follow_redirects', true);
        $request->setUrl($this->getSpider()->getDomainName(true, true, true) . $this->patientHistoryPageUri);
        $url = $request->getUrl();
        $url->setQueryVariable('PatientIdentifier', $patientId);
        $url->setQueryVariable('TopTabIdentifier', 'HISTORY');
        $url->setQueryVariable('LowTabIdentifier', 'HISTORY');
        $response = $request->send();
        $body = $response->getBody();

        if (strpos($body, self::ERROR_MARKER) !== false) {
            return false;
        } else {
            $this->setPatientHistoryPage(
                new PatientHistoryPage($body)
            );
            return true;
        }
    }

    private function _updatePatientHistory($deviceId, $patientId)
    {
        if (!$this->_isHistoryPageWasLoaded()) {
            throw new Exception("Patient's history page must was loaded previously.");
        }

        $this->_clearHistoryMessage($deviceId);
        $existNextPage = $this->getPatientHistoryPage()->existNextPage('DisplayPatientHistory');
        $this->_saveHistoryMassages(
            $this->getPatientHistoryPage()->getHistoryMessageArray(),
            $deviceId
        );
        while ($existNextPage) {
            $this->loadNextPage(
                $this->getPatientHistoryPage(),
                [
                    'LowTabIdentifier' => 'HISTORY',
                    'PatientIdentifier' => $patientId,
                    'TopTabIdentifier' => 'HISTORY',
                    $this->getPatientHistoryPage()->getDatePickerId() => date('m/d/Y'),
                    'DisplayPatientHistory:actionsFilterSelected' => 'on',
                    'DisplayPatientHistory:findingsFilter' => 'ALL',
                    'DisplayPatientHistory:findingsFilterSelected' => 'on',
                    'DisplayPatientHistory:selectedDate.day' => date('d'),
                    'DisplayPatientHistory:selectedDate.month' => date('m'),
                    'DisplayPatientHistory:selectedDate.year' => date('Y'),
                    'DisplayPatientHistory:statusNotesFilterSelected' => 'on'
                ],
                'DisplayPatientHistory'
            );
            $existNextPage = $this->getPatientHistoryPage()->existNextPage('DisplayPatientHistory');
            $this->_saveHistoryMassages(
                $this->getPatientHistoryPage()->getHistoryMessageArray(),
                $deviceId
            );
        }
    }

    protected function mergePostParameters(BasePage $page, $postParameters = null, $parametersPrefix = '')
    {
        $postParameters = parent::mergePostParameters($page, $postParameters, $parametersPrefix);
        unset($postParameters[$parametersPrefix . ':defaultAction']);
        unset($postParameters[$parametersPrefix . ':searchString']);
        return $postParameters;
    }

    private function _clearHistoryMessage($deviceId)
    {
        HistoryMessage::model()->deleteAllByAttributes(['device_id' => $deviceId]);
    }

    private function _saveHistoryMassages($historyMessageArray, $deviceId)
    {
        foreach ($historyMessageArray as $messageInfo) {
            $historyMessage = new HistoryMessage();
            $historyMessage->time = $messageInfo['time'];
            $historyMessage->text = $messageInfo['text'];
            $historyMessage->device_id = $deviceId;
            $historyMessage->save();
        }
    }

    private function _isHistoryPageWasLoaded()
    {
        return ($this->patientHistoryPage instanceof PatientHistoryPage);
    }
}
