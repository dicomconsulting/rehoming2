<?php

class GetDoctorLoginArrayTask extends UpdateDoctorBaseTask
{
    /**
     * Return array of doctor's usernames.
     * 
     * @param array $parameters
     * @return array
     */
    public function run($parameters = array())
    {
        $doctorListPage = $this->getDoctorListPage();
        $existNextPage = $doctorListPage->existNextPage('DisplayUserOverview');
        $doctorLoginArray = $doctorListPage->getDoctorLoginArray();
        while ($existNextPage) {
            $this->loadNextPage($doctorListPage, null, 'DisplayUserOverview');
            $existNextPage = $doctorListPage->existNextPage('DisplayUserOverview');
            $doctorLoginArray = array_merge($doctorLoginArray, $doctorListPage->getDoctorLoginArray());
        }
        return $doctorLoginArray;
    }
}
