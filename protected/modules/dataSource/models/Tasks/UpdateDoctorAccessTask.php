<?php
Yii::import('admin.models.*');
Yii::import('patient.models.*');

class UpdateDoctorAccessTask extends UpdateDoctorBaseTask
{
    /**
     * @var array Map for translate Biotronik Home Monitoring access level to internal values 
     */
    private $accessLevelMap = [
        'noAccess' => PatientGroupAccess::ACCESS_NO,
        'readAccess' => PatientGroupAccess::ACCESS_READ,
        'fullAccess' => PatientGroupAccess::ACCESS_FULL
    ];
            
    /**
     * @var string Uri to access the edit form.
     */
    private $doctorGroupAccessFormUri = 'hmsc_guiWeb/user/administration/DisplayUserOverview.jsf';
    
    /**
     * @var string Uri of doctor access edit form. 
     */
    private $groupAccessFormListUri = 'hmsc_guiWeb/user/administration/ChangePatientGroupAccess.jsf';
    
    public function run($parameters = array())
    {
        $this->_checkRequiredParameters($parameters);
        $doctorLogin = $parameters['doctorLogin'];
        $doctorListPage = $this->getDoctorListPage();
        $this->loadSearchResultList($doctorListPage, null, 'DisplayUserOverview', $doctorLogin);
        $doctorListPage->getEditProfileButtonNameByDoctorLogin($doctorLogin);

        $existNextPage = $doctorListPage->existNextPage('DisplayUserOverview');
        $buttonName = $doctorListPage->getEditGroupAccessButtonNameByDoctorLogin($doctorLogin);
        
        while (empty($buttonName) && $existNextPage) {
            $this->loadNextPage($doctorListPage, null, 'DisplayUserOverview');
            $existNextPage = $doctorListPage->existNextPage('DisplayUserOverview');
            $buttonName = $doctorListPage->getEditGroupAccessButtonNameByDoctorLogin($doctorLogin);
        }
        
        if (empty($buttonName)) {
            throw new Exception("Wrong doctor's login!");
        }
        
        $this->_createDoctorGroupAccess($doctorListPage, $doctorLogin);
    }
    
    /**
     * Load doctor group access list edit form.
     * 
     * Load first page use redirect, set new url as list url.
     * 
     * If you are going to use the same object for parse a list of doctors,
     * do not forget to change the address on the original value.
     * 
     * @param DoctorListPage $page
     * @param string $doctorLogin
     * @return \DoctorGroupAccessListPage
     */
    private function _loadGroupAccessForm(DoctorListPage $page, $doctorLogin)
    {
        $request = $this->getSpider()->getNewRequestObject();
        $request->setConfig('follow_redirects', true);
        $request->setUrl(
            $this->getSpider()->getDomainName(true, true, true)
            . $this->doctorGroupAccessFormUri
        );
        $request->setMethod(HTTP_Request2::METHOD_POST);
        $request->addPostParameter(
            [
                'DisplayUserOverview' => 'DisplayUserOverview',
                'DisplayUserOverview:defaultAction' => 'DisplayUserOverview:userOverviewSearchButton',
                'DisplayUserOverview:searchString' => '',
                $page->getEditGroupAccessButtonNameByDoctorLogin($doctorLogin) => 'Edit',
                'javax.faces.ViewState' => $page->getViewState()
            ]
        );
        $response = $request->send();
        $doctorGroupAccessListPage = new DoctorGroupAccessListPage($response->getBody());
        $this->setListUrl($this->groupAccessFormListUri);
        
        return $doctorGroupAccessListPage;
    }
    
    /**
     * Updates the access level for the doctor for each patient group.
     * 
     * @param DoctorListPage $page
     * @param string $doctorLogin
     */
    private function _createDoctorGroupAccess($page, $doctorLogin)
    {
        $doctorGroupAccessList = $this->_loadGroupAccessForm($page, $doctorLogin);
        $accessLevelArray = $doctorGroupAccessList->getAccessLevelArray();
        
        $user = User::model()->findByAttributes(['username' => $doctorLogin]);
        if (empty($user) || empty($user->doctor)) {
            throw new Exception(
                "Can't create or update access level for " . $doctorLogin
                . " by patient groups, because doctor does't exist"
            );
        }
        
        $existNextPage = $doctorGroupAccessList->existNextPage('ChangePatientGroupAccess');
        while ($existNextPage) {
            $this->loadNextPage($doctorGroupAccessList, null, 'ChangePatientGroupAccess');
            $accessLevelArray = array_merge($accessLevelArray, $doctorGroupAccessList->getAccessLevelArray());
            $existNextPage = $doctorGroupAccessList->existNextPage('ChangePatientGroupAccess');
        }
        
        foreach ($accessLevelArray as $groupName => $accessLevel) {
            $this->_updateAccess($user->doctor, $groupName, $accessLevel);
        }
        
        $this->setListUrl($this->doctorGroupAccessFormUri);
    }
    
    /**
     * Update access level, if patient group exist in the system.
     * 
     * @param Doctor $doctor
     * @param string $groupName
     * @param string $accessLevel
     */
    private function _updateAccess(Doctor $doctor, $groupName, $accessLevel)
    {
        $group = PatientGroup::model()->findByAttributes(['name' => $groupName]);
        if (!empty($group)) {
            $groupAccess = PatientGroupAccess::model()->findByAttributes(
                ['doctor_id' => $doctor->uid, 'patient_group_id' => $group->uid]
            );
            
            if (empty($groupAccess)) {
                $groupAccess = new PatientGroupAccess();
            }
            $groupAccess->patient_group_id = $group->uid;
            $groupAccess->doctor_id = $doctor->uid;
            $groupAccess->access_level = $this->accessLevelMap[trim($accessLevel)];
            $groupAccess->save();
        }
    }
}
