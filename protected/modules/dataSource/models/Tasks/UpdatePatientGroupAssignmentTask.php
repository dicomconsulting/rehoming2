<?php
Yii::import('dataSource.models.Pages.*');
Yii::import('dataSource.models.Pages.*');
Yii::import('patient.models.*');


class UpdatePatientGroupAssignmentTask extends BiotronikSpiderBaseListTask
{
    protected $requiredFieldNames = ['patientIdArray'];

    /**
     * Patient list page object.
     *
     * @var PatientsListPage
     */
    private $patientListPage;

    public function __construct()
    {
        parent::__construct();
        $this->setListUrl('hmsc_guiWeb/patient/monitoring/overview/DisplayPatientOverview.jsf');

        $this->getSpider()->authenticate();
        $request = $this->getSpider()->getNewRequestObject();
        $request->setConfig('follow_redirects', true);
        $request->setUrl(
            $this->getSpider()->getDomainName(true, true, true)
            . $this->getListUrl()
        );
        $response = $request->send();

        $this->patientListPage = new PatientsListPage($response->getBody());
    }

    /**
     * Return patient list page object
     *
     * @return PatientsListPage
     */
    public function getPatientListPage()
    {
        return $this->patientListPage;
    }

    public function run($parameters = [])
    {
        $this->_checkRequiredParameters($parameters);
        $patientIdArray = $parameters['patientIdArray'];

        $patientListPage = $this->getPatientListPage();
        $customPostParameters = [
            'PatientsOverview:selectMonitoringStatusFilter' => 'ACTIVATED',
            'PatientsOverview:selectPatientFilter' => 'ALL_PATIENTS',
            'PatientsOverview:selectPatientGroupFilter' => 'all'
        ];

        $existNextPage = $patientListPage->existNextPage('PatientsOverview');
        $patientsGroupAssignment = $patientListPage->getPatientsGroupAssignment();

        while ($existNextPage) {
            $this->loadNextPage($patientListPage, $customPostParameters, 'PatientsOverview');
            $patientsGroupAssignment += $patientListPage->getPatientsGroupAssignment();
            $existNextPage = $patientListPage->existNextPage('PatientsOverview');
        }

        foreach ($patientsGroupAssignment as $patientId => $groupName) {
            $patient = Patient::model()->find('external_uid = :patientId', array(':patientId' => $patientId));
            if (!empty($patient) && (empty($patientIdArray) || in_array($patientId, $patientIdArray))) {
                $patientGroup = PatientGroup::model()->findByAttributes(['name' => $groupName]);
                if ($patient->group_id != $patientGroup->uid) {
                    $patient->group = $patientGroup;
                    $patient->save();
                }
            }
        }
    }
}
