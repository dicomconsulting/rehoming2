<?php
Yii::import('application.modules.dataSource.models.Pages.*');
Yii::import('application.modules.device.models.*');
Yii::import('application.modules.patient.models.*');

class UpdatePatientOptionsTask extends BiotronikSpiderBaseTask
{
    protected $requiredFieldNames = ['patient'];

    private $optionsViewPageUrl = 'hmsc_guiWeb/patient/monitoring/DisplayPatientContext.jsf';
    const OPTIONS_ERROR_MARKER = 'However, your entries might be lost';

    /**
     * @var OptionsViewPage
     */
    private $optionsViewPage = null;

    /**
     * @var OptionsEditPage
     */
    private $optionsEditPage = null;

    public function run($parameters = [])
    {
        Yii::app()->getModule("option");

        $this->_checkRequiredParameters($parameters);
        $patient = $parameters['patient'];
        $this->getSpider()->authenticate();
        $this->_loadOptionsViewPage($patient);
        if ($this->_loadOptionsEditPage($patient)) {
            $this->_updateOptions($patient);
        } else {
            throw new CException('Patient skipped');
        }
    }

    private function _setOptionsViewPage(OptionsViewPage $optionsViewPage)
    {
        $this->optionsViewPage = $optionsViewPage;
    }

    private function _getOptionsViewPage()
    {
        return $this->optionsViewPage;
    }

    /**
     * @param OptionsEditPage $optionsEditPage
     */
    private function _setOptionsEditPage(OptionsEditPage $optionsEditPage)
    {
        $this->optionsEditPage = $optionsEditPage;
    }

    /**
     * @return OptionsEditPage
     */
    private function _getOptionsEditPage()
    {
        return $this->optionsEditPage;
    }

    private function _loadOptionsViewPage(Patient $patient)
    {
        $request = $this->getSpider()->getNewRequestObject();
        $request->setMethod(HTTP_Request2::METHOD_GET);
        $request->setConfig('follow_redirects', true);
        $request->setUrl($this->getSpider()->getDomainName(true, true, true) . $this->optionsViewPageUrl);
        $url = $request->getUrl();
        $url->setQueryVariable('PatientIdentifier', $patient->external_uid);
        $url->setQueryVariable('TopTabIdentifier', 'PATIENT_OPTIONS');
        $response = $request->send();
        $body = $response->getBody();
        $this->_setOptionsViewPage(new OptionsViewPage($body));
    }

    private function _loadOptionsEditPage(Patient $patient)
    {
        $postParameters = [
            'DisplayPatientOptions' => 'DisplayPatientOptions',
            'DisplayPatientOptions:edit' => 'Edit',
            'DisplayPatientOptions:templateSelection' => '',
            'LowTabIdentifier' => 'PATIENT_OPTIONS',
            'PatientIdentifier' => $patient->external_uid,
            'TopTabIdentifier' => 'PATIENT_OPTIONS',
            'javax.faces.ViewState' => $this->_getOptionsViewPage()->getViewState()
        ];
        $request = $this->getSpider()->getNewRequestObject();
        $request->setConfig('follow_redirects', true);
        $request->setUrl(
            $this->getSpider()->getDomainName(true, true, true)
            . $this->optionsViewPageUrl
        );
        $request->setMethod(HTTP_Request2::METHOD_POST);
        $request->addPostParameter($postParameters);
        $response = $request->send();
        $body = $response->getBody();

        if (strpos($body, self::OPTIONS_ERROR_MARKER) !== false) {
            return false;
        }

        $this->_setOptionsEditPage(
            new OptionsEditPage($body)
        );
        return true;
    }

    /**
     * @return OptionRawData[]
     */
    private function _generateOptionRowDataArray()
    {
        $optionRowDataArray = [];
        $this->_getOptionsEditPage()->resetRow();
        do {
            $optionRowData = new OptionRawData();
            $optionRowData->selectedAlertLevel = $this->_getOptionsEditPage()->getAlertLevel();
            $optionRowData->optionText = $this->_getOptionsEditPage()->getOptionName();
            $optionRowData->selectedVal1 = $this->_getOptionsEditPage()->getFirstValue();
            $optionRowData->selectedVal2 = $this->_getOptionsEditPage()->getSecondValue();
            $optionRowDataArray[] = $optionRowData;
        } while ($this->_getOptionsEditPage()->nextRow());
        $this->_getOptionsEditPage()->resetRow();
        return $optionRowDataArray;
    }

    private function _updateOptions(Patient $patient)
    {
        $data = $this->_generateOptionRowDataArray();

        foreach ($data as $row) {
            try {
                $option = OptionFactory::createOptionByData($row);
                $parser = $option->createParser();
                $parsedData = $parser->parse($row);
                $option->saveValue($parsedData, $patient->device);
            } catch (OptionException $e) {
                echo "Patient: {$patient->human_readable_id}, " . $e->getMessage() . "\n";
            }
        }

    }
}
