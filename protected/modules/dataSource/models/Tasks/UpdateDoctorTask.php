<?php
Yii::import('admin.models.*');

class UpdateDoctorTask extends UpdateDoctorBaseTask
{
    /**
     * @var string Uri of doctor profile page.
     */
    private $doctorProfileUri = 'hmsc_guiWeb/user/administration/DisplayUserOverview.jsf';

    /**
     * @var array Doctor profile fields names.
     */
    private $doctorFieldNames = ['title', 'name', 'surname', 'external_role'];

    public function run($parameters = [])
    {
        $this->_checkRequiredParameters($parameters);
        $doctorLogin = $parameters['doctorLogin'];
        $doctorListPage = $this->getDoctorListPage();
        $this->loadSearchResultList($doctorListPage, null, 'DisplayUserOverview', $doctorLogin);
        $doctorListPage->getEditProfileButtonNameByDoctorLogin($doctorLogin);

        $existNextPage = $doctorListPage->existNextPage('DisplayUserOverview');
        $buttonName = $doctorListPage->getEditProfileButtonNameByDoctorLogin($doctorLogin);

        while (empty($buttonName) && $existNextPage) {
            $this->loadNextPage($doctorListPage, null, 'DisplayUserOverview');
            $existNextPage = $doctorListPage->existNextPage('DisplayUserOverview');
            $buttonName = $doctorListPage->getEditProfileButtonNameByDoctorLogin($doctorLogin);
        }

        if (empty($buttonName)) {
            throw new Exception("Wrong doctor's login!");
        }

        $doctorViewPage = $this->_loadDoctorProfile($doctorListPage, $doctorLogin);

        $this->_createDoctor($doctorViewPage, $doctorLogin);
    }

    /**
     * Load doctor profile page.
     *
     * Return doctor profile page.
     *
     * @param DoctorListPage $page
     * @param string $doctorLogin
     * @return DoctorViewPage Doctor profile page object.
     */
    private function _loadDoctorProfile(DoctorListPage $page, $doctorLogin)
    {
        $request = $this->getSpider()->getNewRequestObject();
        $request->setConfig('follow_redirects', true);
        $request->setUrl(
            $this->getSpider()->getDomainName(true, true, true)
            . $this->doctorProfileUri
        );
        $request->setMethod(HTTP_Request2::METHOD_POST);
        $request->addPostParameter(
            [
                'DisplayUserOverview' => 'DisplayUserOverview',
                'DisplayUserOverview:defaultAction' => 'DisplayUserOverview:userOverviewSearchButton',
                'DisplayUserOverview:searchString' => '',
                $page->getEditProfileButtonNameByDoctorLogin($doctorLogin) => $doctorLogin,
                'javax.faces.ViewState' => $page->getViewState()
            ]
        );
        $response = $request->send();
        $doctorViewPage = new DoctorViewPage($response->getBody());

        return $doctorViewPage;
    }

    /**
     * Create or update doctor and user information in DB.
     *
     * @param DoctorViewPage $doctorViewPage
     * @param string $doctorLogin
     */
    private function _createDoctor(DoctorViewPage $doctorViewPage, $doctorLogin)
    {
        $user = User::model()->findByAttributes(['username' => $doctorLogin]);
        if (empty($user)) {
            $user = new BiotronikUser();
            $user->doctor = new Doctor();
            $user->username = $doctorLogin;

            $groupName = $this->getSpider()->getUserGroup();
            $groupId = Yii::app()->userGroupManager->getGroupIdByName($groupName);
            $user->user_group_id = $groupId;
        }
        $doctor = $user->doctor;
        foreach ($this->doctorFieldNames as $fieldName) {
            $doctor->{$fieldName} = $doctorViewPage->getAttribute($fieldName);
        }
        $doctor->save(false);
        $user->doctor_id = $doctor->uid;
        $user->save();
    }
}
