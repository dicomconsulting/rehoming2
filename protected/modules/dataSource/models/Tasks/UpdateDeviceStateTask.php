<?php
Yii::import('patient.models.*');
Yii::import('device.models.*');
Yii::import('device.models.DeviceParameterSynonym.*');

class UpdateDeviceStateTask
{
    public function run(Patient $patient, CsvFile $csvFile, $returnUpdatedPatient = false)
    {
        $device = $patient->device;
        $parameters = $device->model->parameters;
        $rows = $csvFile->getBody()->getRows();
        if ($returnUpdatedPatient) {
            $stateArray = [];
        }
        foreach ($rows as $row) {
            $deviceState = new DeviceState();
            $valueArray = [];
            foreach ($parameters as $parameter) {
                $parameterCsvName = $parameter->csv_name;
                if ($device->model->getParameterSynonym($parameter) !== false) {
                    $parameterCsvName = $device->model->getParameterSynonym($parameter);
                    $isSynonym = true;
                } else {
                    $isSynonym = false;
                }
                if ($csvFile->getHeader()->existColumn($parameterCsvName)) {
                    $value = new DeviceValue();
                    $value->value = $parameter->transformationFromCsv(
                        $row->getCellValue(
                            $csvFile->getHeader()->getColumnNumber($parameterCsvName)
                        )
                    );
                    $value->parameter_id = $parameter->uid;
                    $valueArray[$parameter->system_name] = $value;
                } else {
                    $createTime = isset($patient->device->lastState) ? $patient->device->lastState->create_time : null;
                    $this->_wrongParameter($parameter, $device->model, $createTime, $isSynonym);
                }
            }
            $this->_assignInformation($deviceState, $valueArray);
            if ($this->_existDeviceState($device, $deviceState)) {
                continue;
            }

            $deviceState->device_id = $device->uid;
            $deviceState->save();
            if ($returnUpdatedPatient) {
                $stateArray[] = $deviceState;
            } else {
                unset($deviceState);
            }
        }
        if ($returnUpdatedPatient) {
            $device->states = $stateArray;
            $patient->device = $device;
        }
        unset($rows);
        unset($stateArray);
        unset($valueArray);
        unset($csvFile);
    }

    private function _wrongParameter(DeviceParameter $parameter, DeviceModel $deviceModel, $lastCurrentTimestamp, $isSynonym)
    {
        if ($isSynonym) {
            $synonym = DeviceParameterSynonym::model()->findByAttributes(
                ['device_model_name' => $deviceModel->name, 'parameter_id' => $parameter->uid]
            );
            $synonymState = SynonymStateBase::createState(SynonymStateChanged::ID, $synonym);
        } else {
            $synonym = new DeviceParameterSynonym();
            $synonym->parameter = $parameter;
            $synonym->deviceModel = $deviceModel;
            $synonymState = SynonymStateBase::createState(SynonymStateNew::ID, $synonym);
        }
        $synonymState->freeze();
        $synonym->start_conflict_time = $lastCurrentTimestamp;
        $synonym->setState($synonymState);
        $synonym->save();
    }

    private function _assignInformation(DeviceState $deviceState, $valueArray)
    {
        $deviceState->values = $valueArray;

        if (isset($valueArray['device_message_created_on'])) {
            $deviceState->create_time = $valueArray['device_message_created_on']->value;
        } else {
            $deviceState->create_time = null;
        }

        if (isset($valueArray['last_follow_up'])) {
            $deviceState->last_follow_up = $valueArray['last_follow_up']->value;
        } else {
            $deviceState->last_follow_up = null;
        }
    }

    private function _existDeviceState(Device $device, DeviceState $deviceState)
    {
        return $deviceState->create_time instanceof RhDateTime && DeviceState::model()->exists(
            'device_id = :device_id AND create_time = :create_time',
            ['device_id' => $device->uid, 'create_time' => $deviceState->create_time->format('Y-m-d H:i:s')]
        );
    }
}
