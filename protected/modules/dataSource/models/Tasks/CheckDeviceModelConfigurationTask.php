<?php
Yii::import('patient.models.*');
Yii::import('device.models.*');

class CheckDeviceModelConfigurationTask
{
    public function run(Patient $patient, CsvFile $csvFile)
    {
        $deviceModel = $patient->device->model;
        if (empty($deviceModel->parameters)) {
            $deviceModel = $this->_createDeviceModelProfile($deviceModel, $csvFile);
        }
        return $deviceModel;
    }

    private function _createDeviceModelProfile(DeviceModel $deviceModel, CsvFile $csvFile)
    {
        $allParameterArray = DeviceParameter::model()->withI18n('en')->findAll();
        $existParameters = [];
        foreach ($allParameterArray as $parameter) {
            $synonym = $deviceModel->getParameterSynonym($parameter);
            if ($csvFile->getHeader()->existColumn($parameter->csv_name)
                || ($synonym !== false && $csvFile->getHeader()->existColumn($synonym))) {
                $existParameters[] = $parameter;
            }
        }
        $deviceModel->parameters = $existParameters;
        $deviceModel->save();
        return $deviceModel;
    }
}
