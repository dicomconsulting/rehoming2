<?php
Yii::import('dataSource.components.Interfaces.SpiderInterface');
Yii::import('dataSource.models.Pages.BasePage');

abstract class BiotronikSpiderBaseListTask extends BiotronikSpiderBaseTask
{
    /**
     * @var string Uri of list page.
     */
    private $listUrl;
    
    /**
     * @var string Current search string's value.
     */
    private $searchString = '';
    
    /**
     * Return current search string.
     * 
     * @return string
     */
    public function getSearchString()
    {
        return $this->searchString;
    }

    /**
     * Set current search string's value.
     * 
     * @param string $searchString
     */
    public function setSearchString($searchString = '')
    {
        $this->searchString = $searchString;
    }
    
    /**
     * Clear search string's value.
     */
    public function clearSearchString()
    {
        $this->setSearchString();
    }

    /**
     * Set uri of list page for current type entity.
     *
     * @param string $listUrl
     */
    public function setListUrl($listUrl)
    {
        $this->listUrl = $listUrl;
    }

    /**
     * Get uri of list page for current type entity.
     *
     * @return null|string
     */
    public function getListUrl()
    {
        return $this->listUrl;
    }

    /**
     * Load next page from list view.
     *
     * @param BasePage $page Page object.
     * @param array $postParameters Array of parameters for request.
     * @param string $parametersPrefix Prefix for all parameters in current page.
     */
    protected function loadNextPage(BasePage $page, $postParameters, $parametersPrefix)
    {
        $postParameters[$parametersPrefix . ':defaultAction'] = $parametersPrefix . ':submitSearchButton';
        $postParameters[$parametersPrefix . ':gotoNextButton'] = '';
        
        $this->goToPage($page, $postParameters, $parametersPrefix);
    }

    /**
     * Reload current list view to first page.
     *
     * @param BasePage $page Page object.
     * @param null|array $postParameters Array of parameters for request.
     * @param string $parametersPrefix Prefix for all parameters in current page.
     */
    protected function reloadList(BasePage $page, $postParameters, $parametersPrefix)
    {
        $postParameters[$parametersPrefix . ':defaultAction'] = $parametersPrefix . ':submitSearchButton';
        $postParameters[$parametersPrefix . ':gotoFirstButton'] = '';
        
        $this->goToPage($page, $postParameters, $parametersPrefix);
    }

    /**
     * Load search result list view.
     *
     * @param BasePage $page Page object.
     * @param null|array $postParameters Array of parameters for request.
     * @param string $parametersPrefix Prefix for all parameters in current page.
     * @param string $searchString New search string's value.
     */
    protected function loadSearchResultList(BasePage $page, $postParameters, $parametersPrefix, $searchString = '')
    {
        $postParameters = $this->mergePostParameters($page, $postParameters, $parametersPrefix);

        $postParameters[$parametersPrefix . ':defaultAction'] = $parametersPrefix . ':userOverviewSearchButton';
        $postParameters[$parametersPrefix . ':userOverviewSearchButton'] = '';
        
        $this->setSearchString($searchString);
        
        $this->goToPage($page, $postParameters, $parametersPrefix);
    }
    
    /**
     * Load need page of list view.
     *
     * @param BasePage $page Page object.
     * @param null|array $postParameters Array of parameters for request.
     * @param string $parametersPrefix Prefix for all parameters in current page.
     */
    protected function goToPage(BasePage $page, $postParameters, $parametersPrefix = '')
    {
        $postParameters = $this->mergePostParameters($page, $postParameters, $parametersPrefix);

        $request = $this->getSpider()->getNewRequestObject();
        $request->setConfig('follow_redirects', true);
        $request->setUrl(
            $this->getSpider()->getDomainName(true, true, true)
            . $this->getListUrl()
        );
        $request->setMethod(HTTP_Request2::METHOD_POST);
        $request->addPostParameter($postParameters);
        $response = $request->send();
        $page->setContent($response->getBody());
    }

    /**
     * Merge parameters with default parameters.
     *
     * @param BasePage $name Page's class.
     * @param array|string|null $postParameters.
     * @param string $parametersPrefix Prefix for all parameters in current page.
     * @return array
     */
    protected function mergePostParameters(BasePage $page, $postParameters = null, $parametersPrefix = '')
    {
        if (!isset($postParameters)) {
            $postParameters = array();
        } elseif (!is_array($postParameters)) {
            $postParameters = [$postParameters];
        }
        
        $postParameters[$parametersPrefix] = $parametersPrefix;
        $postParameters[$parametersPrefix . ':searchString'] = $this->getSearchString();
        $postParameters['javax.faces.ViewState'] = $page->getViewState();
        
        return $postParameters;
    }
}
