<?php
Yii::import('dataSource.models.Pages.*');

abstract class UpdateDoctorBaseTask extends BiotronikSpiderBaseListTask
{
    protected $requiredFieldNames = array('doctorLogin');

    /**
     * Doctor list page object.
     *
     * @var DoctorListPage
     */
    private $doctorListPage;

    public function __construct()
    {
        parent::__construct();
        $this->setListUrl('hmsc_guiWeb/user/administration/DisplayUserOverview.jsf');

        $this->getSpider()->authenticate();
        $request = $this->getSpider()->getNewRequestObject();
        $request->setConfig('follow_redirects', true);
        $request->setUrl(
            $this->getSpider()->getDomainName(true, true, true)
            . $this->getListUrl()
        );
        $response = $request->send();

        $this->doctorListPage = new DoctorListPage($response->getBody());
    }

    /**
     * Return doctor list page object
     *
     * @return DoctorListPage
     */
    public function getDoctorListPage()
    {
        return $this->doctorListPage;
    }
}
