<?php
Yii::import('dataSource.components.BiotronikSpider');

abstract class BiotronikSpiderBaseTask
{
    /**
     * @var BiotronikSpider
     */
    private $spider = null;

    /**
     * Array of required parameter' names.
     *
     * @var array
     */
    protected $requiredFieldNames = array();

    public function __construct()
    {
        $spider = Yii::app()->biotronikSpider;
        $this->setSpider($spider);
    }

    /**
     * Set object, that implemented SpiderInterface
     *
     * @param SpiderInterface $spider
     */
    public function setSpider(SpiderInterface $spider)
    {
        $this->spider = $spider;
    }

    /**
     * Return object, that implemented SpiderInterface
     *
     * @return BiotronikSpider
     * @throws Exception
     */
    public function getSpider()
    {
        if (!$this->spider instanceof SpiderInterface) {
            throw new Exception('Set spider before any actions');
        }

        return $this->spider;
    }

    /**
     * Method for executing current task
     *
     * @param array $parameters Array of need parameters
     */
    abstract public function run($parameters = []);

    /**
     * Check exist of required parameters.
     *
     * Throw Exception with list of lost parameters.
     *
     * @param array $parameters
     * @throws Exception
     */
    protected function _checkRequiredParameters($parameters = array())
    {
        $lostParameterNames = [];

        if (!empty($this->requiredFieldNames) && is_array($this->requiredFieldNames)) {
            $lostParameterNames = array_diff(
                $this->requiredFieldNames,
                array_keys($parameters)
            );
        }

        if (!empty($lostParameterNames)) {
            throw new Exception(
                'Missing required parameters: '
                . implode(', ', $lostParameterNames)
            );
        }
    }
}
