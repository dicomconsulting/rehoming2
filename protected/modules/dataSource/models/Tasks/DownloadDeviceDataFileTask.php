<?php
Yii::import('dataSource.models.Pages.*');

class DownloadDeviceDataFileTask extends BiotronikSpiderBaseListTask
{
    const CSV_FILE_CONTENT_TYPE = 'application/octet-stream';

    protected $requiredFieldNames = ['patientId', 'startDate', 'endDate'];

    private $patientHistoryPageUri = 'hmsc_guiWeb/patient/monitoring/DisplayPatientContext.jsf';

    private $patientHistoryPage;

    private $downloadDataFormPage;

    /**
     * Retrieves data from the patient. Returns a csv file.
     *
     * The parameter array must contain the following elements:
     * patientId - [string] unique system identifier of the patient
     * startDate - [DateTime] start date of monitoring data period
     * endDate - [DateTime] end date of monitoring data period
     *
     * @param array $parameters Array of needle parameters.
     * @return CsvFile Object of Csv file with data.
     */
    public function run($parameters = array())
    {
        $this->_checkRequiredParameters($parameters);
        $this->getSpider()->authenticate();

        $patientId = $parameters['patientId'];
        $startDate = $parameters['startDate'];
        $endDate = $parameters['endDate'];

        $this->_loadPatientHistoryPage($patientId);
        $this->_loadDownloadDeviceFormPage($patientId, $startDate, $endDate);
        return $this->_getDataFile($patientId, $startDate, $endDate);
    }

    public function setPatientHistoryPage(PatientHistoryPage $patientHistoryPade)
    {
        $this->patientHistoryPage = $patientHistoryPade;
    }

    public function getPatientHistoryPage()
    {
        return $this->patientHistoryPage;
    }

    public function setDownloadDataFormPage(DownloadMonitoringDataPage $downloadDataFormPage)
    {
        $this->downloadDataFormPage = $downloadDataFormPage;
    }

    public function getDownloadDataFormPage()
    {
        return $this->downloadDataFormPage;
    }

    private function _loadPatientHistoryPage($patientId)
    {
        $request = $this->getSpider()->getNewRequestObject();
        $request->setConfig('follow_redirects', true);
        $request->setUrl($this->getSpider()->getDomainName(true, true, true) . $this->patientHistoryPageUri);
        $url = $request->getUrl();
        $url->setQueryVariable('PatientIdentifier', $patientId);
        $url->setQueryVariable('TopTabIdentifier', 'HISTORY');
        $url->setQueryVariable('LowTabIdentifier', 'HISTORY');
        $response = $request->send();
        $this->setPatientHistoryPage(
            new PatientHistoryPage($response->getBody())
        );
    }

    private function _loadDownloadDeviceFormPage($patientId)
    {
        if (!$this->_isHistoryPageWasLoaded()) {
            throw new Exception("Patient's history page must was loaded previously.");
        }

        $request = $this->getSpider()->getNewRequestObject();
        $request->setConfig('follow_redirects', true);
        $request->setUrl(
            $this->getSpider()->getDomainName(true, true, true)
            . $this->patientHistoryPageUri
        );
        $request->setMethod(HTTP_Request2::METHOD_POST);
        $request->addPostParameter(
            [
                'DisplayPatientHistory' => 'DisplayPatientHistory',
                'DisplayPatientHistory:displayDownloadMonitoringDataButton' => 'Download monitoring data',
                'LowTabIdentifier' => 'HISTORY',
                'TopTabIdentifier' => 'HISTORY',
                'PatientIdentifier' => $patientId,
                'javax.faces.ViewState' => $this->getPatientHistoryPage()->getViewState()
            ]
        );
        $response = $request->send();
        $this->setDownloadDataFormPage(
            new DownloadMonitoringDataPage($response->getBody())
        );
    }

    private function _getDataFile($patientId, DateTime $startDate, DateTime $endDate)
    {
        if (!$this->_isDownloadDataFormPageWasLoaded()) {
            throw new Exception("Download monitoring data form page must was loaded previously.");
        }
        $request = $this->getSpider()->getNewRequestObject();
        $request->setConfig('follow_redirects', true);
        $request->setUrl(
            $this->getSpider()->getDomainName(true, true, true)
            . $this->patientHistoryPageUri
        );
        $request->setMethod(HTTP_Request2::METHOD_POST);
        $request->addPostParameter(
            [
                'DisplayDownloadMonitoringData'=>'DisplayDownloadMonitoringData',
                'DisplayDownloadMonitoringData:defaultAction'=>'DisplayDownloadMonitoringData:downloadMonitoringDataButton',
                'TopTabIdentifier'=>'HISTORY',
                'LowTabIdentifier'=>'HISTORY',
                'PatientIdentifier'=> $patientId,
                'DisplayDownloadMonitoringData:selectedStartDate.day' => $startDate->format('d'),
                'DisplayDownloadMonitoringData:selectedStartDate.month' => $startDate->format('m'),
                'DisplayDownloadMonitoringData:selectedStartDate.year' => $startDate->format('Y'),
                'DisplayDownloadMonitoringData:selectedEndDate.day' => $endDate->format('d'),
                'DisplayDownloadMonitoringData:selectedEndDate.month' => $endDate->format('m'),
                'DisplayDownloadMonitoringData:selectedEndDate.year' => $endDate->format('Y'),
                'DisplayDownloadMonitoringData:downloadMonitoringDataButton' => 'Apply',
                'javax.faces.ViewState' => $this->getDownloadDataFormPage()->getViewState()

            ]
        );
        $response = $request->send();

        if (!empty($response->getHeader('content-disposition'))
            && $response->getHeader('content-type') == self::CSV_FILE_CONTENT_TYPE) {
            return new CsvFile($response->getBody());
        }
        return false;
    }


    private function _isHistoryPageWasLoaded()
    {
        return ($this->patientHistoryPage instanceof PatientHistoryPage);
    }

    private function _isDownloadDataFormPageWasLoaded()
    {
        return ($this->downloadDataFormPage instanceof DownloadMonitoringDataPage);
    }
}
