<?php
class CsvHeader
{
    private $columns = [];
    
    public function __construct($rowString)
    {
        $this->_processData($rowString);
    }
    
    private function _processData($data)
    {
        $this->setColumns(str_getcsv(trim($data, ' ;'), ';'));
    }
    
    public function setColumns($columns)
    {
        $this->columns = $columns;
        return $this;
    }
    
    public function getColumns()
    {
        return $this->columns;
    }
    
    public function getColumnNumber($columnName)
    {
        return array_search($columnName, $this->getColumns());
    }
    
    public function existColumn($columnName)
    {
        return array_search($columnName, $this->getColumns()) !== false;
    }
}
