<?php
class CsvFile
{
    /**
     * Headers of CSV file
     * 
     * @var CsvHeader
     */
    private $header;
    
    /**
     * Body of Csv file. Collection of CsvRow objects
     * .
     * @var CsvBody
     */
    private $body;
    
    public function __construct($data)
    {
        $this->_processData($data);
    }
    
    public function setHeader(CsvHeader $header)
    {
        $this->header = $header;
    }
    
    public function getHeader()
    {
        return $this->header;
    }

    public function setBody(CsvBody $body)
    {
        $this->body = $body;
    }
    
    public function getBody()
    {
        return $this->body;
    }
        
    private function _processData($data)
    {
        $rows = explode("\n", $data);
        $this->setHeader(new CsvHeader(array_shift($rows)));
        $this->setBody(new CsvBody($rows));
    }
}
