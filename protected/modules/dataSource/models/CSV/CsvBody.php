<?php
class CsvBody implements ArrayAccess, Countable
{
    /**
     * Array of CSV rows.
     * 
     * @var CsvRow[]
     */
    private $csvRows = array();
    
    /**
     * @param array Array of string, that represent csv rows.
     */
    public function __construct($rows)
    {
        $this->_processData($rows);
    }
    
    private function _processData($rows)
    {
        foreach ($rows as $rowString) {
            $this->_createNewRow($rowString);
        }
    }
    
    private function _createNewRow($string)
    {
        if (strlen($string)) {
            $csvRow = new CsvRow($string);
            $this->addRow($csvRow);
        }
    }
    
    /**
     * Add Csv row object to collection.
     * 
     * @param CsvRow $csvRow
     * @param integer $keyNumber
     * @return \CsvBody
     */
    public function addRow(CsvRow $csvRow, $keyNumber = null)
    {
        if (empty($keyNumber)) {
            $this->csvRows[] = $csvRow;
        } else {
            $this->csvRows[intval($keyNumber)] = $csvRow;
        }
        
        return $this;
    }
    
    public function setRow(CsvRow $csvRow, $keyNumber)
    {
        if (!key_exists($keyNumber, $this->getRows())) {
            throw new Exception('Row with the specified address not available. The value can not be specified.');
        }
    }
    
    public function getRows()
    {
        return $this->csvRows;
    }
    
    public function offsetExists($offset)
    {
        key_exists(($offset-1), $this->getRows());
    }

    public function offsetGet($offset)
    {
        return $this->getRows()[$offset+1];
    }

    public function offsetSet($rowNumber, $row)
    {
        if ($row instanceof CsvRow) {
            throw new Exception('Csv body. Can only contain objects of lines');
        }
        return $this->setRow($row, $rowNumber);
    }

    public function offsetUnset($offset)
    {
        $rows = $this->getRows();
        array_splice($rows, $offset-1, 1);
    }

    public function count()
    {
        return count($this->getRows());
    }
}
