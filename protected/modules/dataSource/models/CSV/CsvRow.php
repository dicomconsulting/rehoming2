<?php
class CsvRow
{
    /**
     * @var array of cell's values 
     */
    private $columns = array();
    
    /**
     * Csv row in text format.
     * 
     * @param string $string
     */
    public function __construct($string)
    {
        $this->_processData($string);
    }

    /**
     * Get the value of the cell to the column number of the documen.
     * 
     * @param integer $number Number of cell
     * @throws Exception
     */
    public function getCellValue($number)
    {
        if (key_exists($number, $this->getColumns())) {
            return $this->columns[$number];
        } else {
            throw new Exception('Trying to view a nonexistent cell.');
        }
    }

    /**
     * @param array $columns
     */
    public function setColumns($columns)
    {
        $this->columns = $columns;
    }
    
    /**
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * Parse string from csv file to array of cells.
     * 
     * @param string $string
     */
    private function _processData($string)
    {
        $this->setColumns(
            str_getcsv(
                trim($string, ' ;'),
                ';'
            )
        );
    }
}
