<?php

class PatientHistoryPage extends BaseListPage
{
    public function getHistoryMessageArray()
    {
        $historyMessageTimestampArray = $this->simpleXmlDocument->xpath(
            '//xhtml:span[starts-with(string(@id), "DisplayPatientHistory:entries:")]'
        );
        $historyMessageArray = [];
        if (!empty($historyMessageTimestampArray)) {
            $count = count($historyMessageTimestampArray);
            for ($i = 0; $i < $count-1; $i+=2) {
                $historyMessageArray[] = [
                    'time' => new RhDateTime($historyMessageTimestampArray[$i]->__toString()),
                    'text' => $historyMessageTimestampArray[$i+1]->__toString()
                ];
            }
        }
        return $historyMessageArray;
    }

    public function getDatePickerId()
    {
        $datePickerElement = $this->simpleXmlDocument->xpath(
            '//xhtml:input[starts-with(string(@id), "Datepicker_")]'
        );
        $id = null;
        if (!empty($datePickerElement)) {
            $id = $datePickerElement[0]->attributes()['id']->__toString();
        }
        return $id;
    }
}
