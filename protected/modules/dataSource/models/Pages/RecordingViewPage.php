<?php

class RecordingViewPage extends BasePage
{
    public function getECGImageUrl()
    {
        $imageElement = $this->simpleXmlDocument->xpath(
            '//xhtml:table[@class="ChartData"]/descendant::xhtml:img/@src'
        );
        if (!empty($imageElement)) {
            $url = $imageElement[0]->__toString();
        } else {
            throw new CException("ECG image url doesn't exist!");
        }
        return $url;
    }
}
