<?php

class DoctorViewPage extends BasePage
{
    public function getTitle()
    {
        $title = $this->getElementText('span[@id="UserProfileShow:userTitle"]');
        
        return trim($title, ' -');
    }

    public function getName()
    {
        return $this->getElementText('span[@id="UserProfileShow:userFirstName"]');
    }
    
    public function getSurname()
    {
        return $this->getElementText('span[@id="UserProfileShow:userLastName"]');
    }
    
    public function getExternalRole()
    {
        $element = $this->simpleXmlDocument->xpath('//xhtml:table[@id="UserProfileShow"]');

        $roleName = null;

        if (!empty($element)) {
            $roleName = $element[0]->tr[5]->td[1]->__toString();
        }
        return $roleName;
    }
}
