<?php

class DoctorListPage extends BaseListPage
{
    /**
     * Return array of doctor's login from current page.
     * 
     * @return array 
     */
    public function getDoctorLoginArray()
    {
        $doctorLoginElements = $this->simpleXmlDocument->xpath(
            '//xhtml:input[starts-with(string(@id), "DisplayUserOverview:users:") and @class = "TxtSubmit"]'
        );
        $doctorLoginArray = array();
        if (!empty($doctorLoginElements)) {
            foreach ($doctorLoginElements as $doctorLoginElement) {
                $doctorLoginArray[] = $doctorLoginElement['value']->__toString();
            }
        }
        return $doctorLoginArray;
    }
    
    /**
     * Return edit doctor's profile button name for specified doctor login.
     *   
     * @param string $doctorLogin
     * @return null|string
     */
    public function getEditProfileButtonNameByDoctorLogin($doctorLogin)
    {
        $buttonElement = $this->simpleXmlDocument->xpath(
            '//xhtml:input[starts-with(string(@id), "DisplayUserOverview:users:") and @class = "TxtSubmit"'
            . ' and string(@value) = "' . trim($doctorLogin) . '"]'
        );
        $buttonName = null;
        if (!empty($buttonElement)) {
            $buttonName = $buttonElement[0]['name']->__toString();
        }
        return $buttonName;
    }
    
    /**
     * Return edit patient group access button name for specified doctor login.
     *   
     * @param string $doctorLogin
     * @return null|string
     */
    public function getEditGroupAccessButtonNameByDoctorLogin($doctorLogin)
    {
        $editProfileButtonName = $this->getEditProfileButtonNameByDoctorLogin($doctorLogin);
        $namePrefix = substr($editProfileButtonName, 0, (strrpos($editProfileButtonName, ':')+1));
        $buttonElement = $this->simpleXmlDocument->xpath(
            '//xhtml:input[starts-with(string(@id), "' . $namePrefix . '") and @class = "SubmitEdit"]'
        );
        $buttonName = null;
        if (!empty($buttonElement)) {
            $buttonName = $buttonElement[0]['name']->__toString();
        }
        return $buttonName;
    }
}
