<?php

class PatientGroupListPage extends BaseListPage
{
    /**
     * Find comment for patient group by group's name
     *
     * Return null if not exist on current page.
     * Return strind, when group exist.
     * @return null|string
     */
    public function findCommentByPatientGroupName($patientGroupName)
    {
        $comment = null;
        $commentElement = $this->simpleXmlDocument
            ->xpath(
                '//xhtml:input[starts-with(string(@id), "DisplayPatientGroupOverview:patientGroupOverviewTable:")'
                .' and @type = "submit" and @value ="' . trim($patientGroupName) . '"]/../../xhtml:td[5]/xhtml:span'
            );

        if (!empty($commentElement)) {
            $comment = $commentElement[0]->__toString();
        }

        return $comment;
    }
}
