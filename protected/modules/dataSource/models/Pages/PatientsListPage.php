<?php

class PatientsListPage extends BasePage
{
    public function findIdByUniqueName($patientName)
    {
        $patientId = null;
        $links = $this->simpleXmlDocument
            ->xpath(
                '//xhtml:a[starts-with(string(@id), "PatientsOverview:patientOverviewTable:")'
                . ' and normalize-space(text()) = "' . trim($patientName) . '"]'
            );

        if ($links) {
            $href = $links[0]->attributes()['href'];
            $urlQuery = parse_url($href, PHP_URL_QUERY);
            parse_str($urlQuery, $urlQueryParts);
            if (isset($urlQueryParts['PatientIdentifier'])) {
                $patientId = $urlQueryParts['PatientIdentifier'];
            }
        }

        return $patientId;
    }

    public function existNextPage()
    {
        $nextPageButton = $this->simpleXmlDocument->xpath('//xhtml:input[@id="PatientsOverview:gotoNextButton"]');

        if ($nextPageButton && !isset($nextPageButton[0]->attributes()['disabled'])) {
            return true;
        } else {
            return false;
        }
    }

    public function allowFirstPageButton()
    {
        $allowFirstPageButton = $this->simpleXmlDocument->xpath(
            '//xhtml:input[@id="PatientsOverview:gotoFirstButton"]'
        );

        if ($allowFirstPageButton && !isset($allowFirstPageButton[0]->attributes()['disabled'])) {
            return true;
        } else {
            return false;
        }
    }

    public function existPatientOverviewTable()
    {
        return is_array($this->simpleXmlDocument->xpath('//xhtml:table[@id="PatientsOverview:patientOverviewTable"]'));
    }

    public function getPatientsGroupAssignment()
    {
        $resultDataArray = [];
        $patientIdArray = $this->simpleXmlDocument
            ->xpath('//xhtml:a[starts-with(string(@id), "PatientsOverview:patientOverviewTable:")]');
        $patientGroupArray = $this->simpleXmlDocument
            ->xpath(
                '//xhtml:tbody[@id="PatientsOverview:patientOverviewTabl:tbody_element"]/'
                . 'descendant::xhtml:input[starts-with(string(@id), "PatientsOverview:patientOverviewTable:")]/@value'
            );
        foreach ($patientIdArray as $key => $element) {
            if (isset($patientGroupArray[$key])) {
                $resultDataArray[$element->__toString()] = $patientGroupArray[$key]->__toString();
            }
        }
        return $resultDataArray;
    }

    public function getPatientsList()
    {
        $resultDataArray = [];
        $patientIdArray = $this->simpleXmlDocument
            ->xpath('//xhtml:a[starts-with(string(@id), "PatientsOverview:patientOverviewTable:")]');

        foreach ($patientIdArray as $key => $element) {
            $href = $element->attributes()['href'];
            $urlQuery = parse_url($href, PHP_URL_QUERY);
            parse_str($urlQuery, $urlQueryParts);
            $patientId = $urlQueryParts['PatientIdentifier'];
            $resultDataArray[$element->__toString()] = $patientId;
        }

        return $resultDataArray;
    }
}
