<?php

class NewPatientFormPage extends BasePage
{
    /**
     * Return array of patient group data.
     * 
     * Return array of <unique_id> => <patient_group_name>.
     * 
     * @return array
     */
    public function getPatientGroupDataArray()
    {
        $patientGroupList = array();
        
        $patientGroupElements = $this->simpleXmlDocument->xpath('//xhtml:option[parent::xhtml:select[@id="CreatePatient:patientGroup"]]');
        
        if (!empty($patientGroupElements)) {
            foreach ($patientGroupElements as $patientGroupElement) {
                $patientGroupList[$patientGroupElement['value']->__toString()] = $patientGroupElement->__toString();
            }
        }
        
        return $patientGroupList;
    }
}
