<?php

class RecordingListPage extends BaseListPage
{
    public function existNeedleEpisode($episodeNumber)
    {
        $needleEpisodeNumberElement = $this->simpleXmlDocument->xpath(
            '//xhtml:td[@class="EpisodeNumberCol"]/xhtml:span[text()="' . $episodeNumber . '"]'
        );
        return !empty($needleEpisodeNumberElement);
    }
    public function getFilterParameters()
    {
        $filterElementArray = $this->simpleXmlDocument->xpath(
            '//xhtml:input[starts-with(string(@id), "DisplayEpisodeList:viewFilter:")  and @value != "Since_Last_Follow_Up"]'
        );
        $filterParameters = [];
        if (!empty($filterElementArray)) {
            foreach ($filterElementArray as $key => $filterElement) {
                if (!isset($filterParameters[$filterElement->attributes()['name']->__toString()])) {
                    $filterParameters[$filterElement->attributes()['name']->__toString()] = [];
                }
                $filterParameters[
                    $filterElement->attributes()['name']->__toString()
                ][] = $filterElement->attributes()['value']->__toString();
            }
        }
        return $filterParameters;
    }

    public function getNeedleRecordIdentification($episodeNumber)
    {
        $needleEpisodeIdentificationElement = $this->simpleXmlDocument->xpath(
            '//xhtml:td[@class="EpisodeNumberCol"]/xhtml:span[text()="' . $episodeNumber . '"]/../../xhtml:td[@class="EpisodeIconCol"]/xhtml:input'
        );
        if (!empty($needleEpisodeIdentificationElement)) {
            $identification = $needleEpisodeIdentificationElement[0]->attributes()['name']->__toString();
        } else {
            throw new CException("Record's identification doesn't exist!");
        }
        return $identification;
    }
}
