<?php
Yii::import('dataSource.models.Pages.BasePage');

class SignInPage extends BasePage
{
    public function getErrorText()
    {
        return !empty($this->_getErrorTextParts()) ?
            trim($this->_getErrorTextParts()[1]) : null;
    }

    public function getErrorCode()
    {
        return !empty($this->_getErrorTextParts()) ?
            $this->_getErrorTextParts()[2] : null;
    }

    private function _getErrorTextParts()
    {
        $errorText = $this->getElementText('tr[@class="MessageError"]/xhtml:td');
        preg_match('/^([^#]*)#(\d+).*/', $errorText, $errorTextParts);
        return $errorTextParts;
    }
}
