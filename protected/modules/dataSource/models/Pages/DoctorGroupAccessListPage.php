<?php

/**
 * Class DoctorGroupAccessListPage
 *
 * Класс страницы пользователя со списком доступов к группам пациентов
 */
class DoctorGroupAccessListPage extends BaseListPage
{
    /**
     * Return array of doctor's access level by groups.
     * 
     * @return array 
     */
    public function getAccessLevelArray()
    {
        $groupNameElements = $this->simpleXmlDocument->xpath('//xhtml:td[@class="Group"]');
        $accessLevelElements = $this->simpleXmlDocument->xpath(
            '//xhtml:input[starts-with(string(@name), "ChangePatientGroupAccess:accessType_") and @checked]'
        );
        $accessLevelArray = array();
        if (!empty($groupNameElements)) {
            foreach ($groupNameElements as $key => $groupNameElement) {
                $accessLevelArray[(string) $groupNameElement->span] = (string) $accessLevelElements[$key]['value'];
            }
        }
        return $accessLevelArray;
    }
}
