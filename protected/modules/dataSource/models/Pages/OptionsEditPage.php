<?php
Yii::import('application.modules.dataSource.models.Pages.BasePage');
Yii::import('application.modules.option.models.*');

class OptionsEditPage extends BasePage
{
    private $alertLevelMap = [
        0 => AbstractOption::ALERT_LEVEL_OFF,
        1 => AbstractOption::ALERT_LEVEL_YELLOW,
        2 => AbstractOption::ALERT_LEVEL_YELLOW_WITH_NOTICE,
        3 => AbstractOption::ALERT_LEVEL_RED_WITH_NOTICE
    ];

    private $rows;

    public function existRows()
    {
        return $this->_getRows()->valid();
    }

    public function getAlertLevel()
    {
        return $this->alertLevelMap[
            intval($this->_getCurrentRow()->xpath('.//td[@class="OptionsRadio"]/input[@checked="checked"]/@value')[0]->__toString())
        ];
    }

    public function resetRow()
    {
        $this->_getRows()->rewind();
    }

    public function nextRow()
    {
        if ($this->_getRows()->key() < ($this->_getRows()->getSize()-1)) {
            $this->_getRows()->next();
            return true;
        } else {
            return false;
        }
    }

    public function getOptionName()
    {
        return $this->_getCurrentRow()->xpath('.//td[5]')[0]->__toString();
    }

    public function getFirstValue()
    {
        return $this->_getSelectOption('td[6]/select/option[@selected="selected"]');
    }

    public function getSecondValue()
    {
        return $this->_getSelectOption('td[8]/select/option[@selected="selected"]');
    }

    private function _getCurrentRow()
    {
        return new SimpleXMLElement($this->_getRows()->current()->asXML());
    }

    private function _getRows()
    {
        if (is_null($this->rows)) {
            $this->rows = SplFixedArray::fromArray($this->_findAllRows());
        }

        return $this->rows;
    }

    private function _findAllRows()
    {
        return $this->simpleXmlDocument
            ->xpath('//xhtml:td[@class="OptionsRadio"]/xhtml:input[@checked="checked"]/../..');
    }

    private function _getSelectOption($xpathExpression)
    {
        $element = $this->_getCurrentRow()->xpath('.//' . $xpathExpression);

        $value = null;

        if (!empty($element)) {
            $value = $element[0]->__toString();
        }

        return $value;
    }
}
