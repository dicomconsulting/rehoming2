<?php

abstract class BaseListPage extends BasePage
{
    public function existNextPage($parametersPrefix)
    {
        $nextPageButton = $this->simpleXmlDocument->xpath('//xhtml:input[@id="' . $parametersPrefix . ':gotoNextButton"]');

        if ($nextPageButton && !isset($nextPageButton[0]->attributes()['disabled'])) {
            return true;
        } else {
            return false;
        }
    }

    public function allowFirstPageButton($parametersPrefix)
    {
        $allowFirstPageButton = $this->simpleXmlDocument->xpath('//xhtml:input[@id="' . $parametersPrefix . ':gotoFirstButton"]');

        if ($allowFirstPageButton && !isset($allowFirstPageButton[0]->attributes()['disabled'])) {
            return true;
        } else {
            return false;
        }
    }
}
