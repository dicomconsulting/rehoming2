<?php

class PatientViewPage extends BasePage
{
    public function getHumanReadableId()
    {
        return $this->getElementText('span[@id="DisplayPatientMainData:patientId"]');
    }

    public function getName()
    {
        $fullName = $this->getElementText('span[@id="DisplayPatientMainData:patientFirstName"]');
        $nameParts = explode(' ', $fullName, 2);
        return isset($nameParts[0]) ? $nameParts[0] : null;
    }

    public function getMiddleName()
    {
        $fullName = $this->getElementText('span[@id="DisplayPatientMainData:patientFirstName"]');
        $nameParts = explode(' ', $fullName, 2);
        return isset($nameParts[1]) ? $nameParts[1] : null;
    }

    public function getSurname()
    {
        return $this->getElementText('span[@id="DisplayPatientMainData:patientLastName"]');
    }

    public function getComment()
    {
        return $this->getElementText('span[@id="DisplayPatientMainData:patientComment"]');
    }

    public function getConsent()
    {
        return empty($this->getElementText('span[@id="DisplayPatientMainData:patientPersonalDataLabel"]')) ? 0 : 1;
    }

    /**
     * Return birth date.
     *
     * @return null | RhDateTime
     */
    public function getBirthDate()
    {
        $birthDateString = $this->getElementText('span[@id="DisplayPatientMainData:dateOfBirth"]');
        return !empty($birthDateString) ? new RhDateTime($birthDateString) : null;
    }

    public function getImplantationDate()
    {
        $implantationDateString = $this->getElementText('span[@id="DisplayPatientMainData:implantationDate"]');
        return !empty($implantationDateString) ? new RhDateTime($implantationDateString) : null;
    }

    public function getMonitoringStateDate()
    {
        $monitoringStateString = $this->getElementText('span[@id="DisplayPatientMainData:monitoringStatus"]');
        preg_match('/^([^\d]*)(.*)$/', $monitoringStateString, $monitoringStateParts);
        return !empty($monitoringStateParts[2]) ? new RhDateTime($monitoringStateParts[2]) : null;
    }

    public function getTitle()
    {
        $titleValue = $this->getElementText('span[@id="DisplayPatientMainData:patientTitle"]');
        return $titleValue != '-' ? : null;
    }

    public function getSex()
    {
        $sexTextValue = $this->getElementText('span[@id="DisplayPatientMainData:sex"]');
        if (is_string($sexTextValue)) {
            $sexIntToNameMap = array(
                'unknown' => Patient::UNKNOWN,
                'male' => Patient::MALE,
                'female' => Patient::FEMALE
            );
            $sexIntValue = $sexIntToNameMap[strtolower($sexTextValue)];
        } else {
            $sexIntValue = null;
        }

        return $sexIntValue;
    }

    public function getDeviceModel()
    {
        return $this->getElementText('span[@id="DisplayPatientMainData:implantName"]');
    }

    public function getDeviceSN()
    {
        return $this->getElementText('span[@id="DisplayPatientMainData:implantType"]');
    }

    public function getTransmitterModel()
    {
        return $this->getElementText('span[@id="DisplayPatientMainData:transmitterType"]');
    }

    public function getTransmitterSN()
    {
        preg_match('/^\d+/', $this->getElementText('span[@id="DisplayPatientMainData:transmitterSerial"]'), $found);
        return current($found);
    }

    /**
     * Parse contact phone's numbers
     *
     * @return array
     */
    public function getPhones()
    {
        $numberArray = [];
        $elements = $this->simpleXmlDocument
            ->xpath('//xhtml:span[starts-with(string(@id), "DisplayPatientMainData:contactPhonesTable:") and ":phone_" = substring(string(@id), string-length(string(@id))-6)]');

        if (is_array($elements)) {
            foreach ($elements as $element) {
                $numberArray[] = $this->filterHtmlText($element->__toString());
            }
        }

        return $numberArray;
    }

    public function getContactPersonsInfo()
    {
        return $this->getContactsInfo(
            'DisplayPatientMainData:contactPersonName_',
            'DisplayPatientMainData:contactPersonPhone_'
        );
    }

    public function getFamilyDoctorsInfo()
    {
        return $this->getContactsInfo(
            'DisplayPatientMainData:familyDoctorsName_',
            'DisplayPatientMainData:familyDoctorsPhone_'
        );
    }

    public function getContactsInfo($nameElementsIdPrefix, $phoneElementsIdPrefix)
    {
        $contactPersonArray = [];
        $nameElements = $this->simpleXmlDocument
            ->xpath('//xhtml:span[starts-with(string(@id), "' . $nameElementsIdPrefix . '")]');
        $phoneElements = $this->simpleXmlDocument
            ->xpath('//xhtml:span[starts-with(string(@id), "' . $phoneElementsIdPrefix . '")]');

        if (is_array($nameElements)) {
            foreach ($nameElements as $key => $element) {
                $contactPersonArray[$key] = [
                    'name' => $this->filterHtmlText($element->__toString()),
                    'number' => $this->filterHtmlText($phoneElements[$key]->__toString())
                ];
            }
        }

        return $contactPersonArray;
    }

    public function getGroupName()
    {
        return $this->getElementText('span[@id="DisplayPatientMainData:patientGroup"]');
    }

    /**
     * Get text of node by xpath query
     *
     * @param string $xpathExpression
     * @return null | string
     */
    protected function getElementText($xpathExpression)
    {
        $element = $this->simpleXmlDocument
            ->xpath('//xhtml:' . $xpathExpression);

        $value = null;

        if (!empty($element)) {
            $value = $element[0]->__toString();
        }

        return $value;
    }
}
