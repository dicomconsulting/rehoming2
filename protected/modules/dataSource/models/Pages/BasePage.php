<?php

/**
 * Base class for all biotronik pages.
 */
abstract class BasePage
{
    /**
     * @var SimpleXMLElement Storage container body for loaded page.
     */
    protected $simpleXmlDocument;

    public function __construct($xhtml = null)
    {
        if (!empty($xhtml)) {
            $this->setContent($xhtml);
        }
    }

    /**
     * Set page's source code.
     *
     * @param string $xhtml Source code string.
     * @throws Exception
     */
    public function setContent($xhtml)
    {
        try {
            $xhtml = $this->cleanUpHtml($xhtml);
            @$this->simpleXmlDocument = new SimpleXMLElement($xhtml);
        } catch (Exception $e) {
            throw new Exception("Patients xhtml document parsing failed", 0, $e);
        }

        $this->simpleXmlDocument->registerXPathNamespace("xhtml", "http://www.w3.org/1999/xhtml");
    }

    /**
     * Get JSF client view state's value from current loaded page.
     *
     * @return string
     */
    public function getViewState()
    {
        $viewState      = null;
        $viewStateInput = $this->simpleXmlDocument->xpath('//xhtml:input[@name = "javax.faces.ViewState"]');
        $inputField     = $viewStateInput[0];

        foreach ($inputField->attributes() as $name => $attribute) {
            if ($name == "value") {
                $viewState = (string)$attribute;
            }
        }

        return $viewState;
    }

    /**
     * Getter for use in the cycle.
     *
     * Return value, if corresponding method exist in model.
     * Otherwise return null.
     *
     * @param string $name Attribute's name.
     * @return null|value
     */
    public function getAttribute($name)
    {
        $methodName = 'get' . $this->underscoreToCamelCase($name, true);

        if (method_exists($this, $methodName)) {
            $value = $this->$methodName();
        } else {
            $value = null;
        }
        $value = $this->filterHtmlText($value);
        return $value;
    }

    /**
     * Сформировать текстовую строку на основе html style кодированной строки
     *
     * @param string $text
     * @return mixed|string
     */
    public function filterHtmlText($text)
    {
        if (is_string($text)) {
            $text = str_replace('AmPeRsAnD', '&', $text);
            $text = html_entity_decode($text);
        }
        return $text;
    }

    /**
     * Transform string from underscore format to camel case.
     *
     * If $ucfirst is true, then first character of the returned string is uppercase.
     * Otherwise first character of the returned string is lowercase.
     *
     * @param string $name String in underscore format.
     * @param boolean $ucfirst
     * @return string
     */
    protected function underscoreToCamelCase($name, $ucfirst = false)
    {
        $name          = str_replace('_', ' ', $name);
        $ucwords       = ucwords(strtolower($name));
        $camelCaseName = str_replace(' ', '', $ucwords);
        if (!$ucfirst) {
            $camelCaseName = lcfirst($camelCaseName);
        }
        return $camelCaseName;
    }

    /**
     * Get text of node by xpath query
     *
     * @param string $xpathExpression
     * @return null | string
     */
    protected function getElementText($xpathExpression)
    {
        $element = $this->simpleXmlDocument
            ->xpath('//xhtml:' . $xpathExpression);

        $value = null;

        if (!empty($element)) {
            $value = $element[0]->__toString();
        }

        return $value;
    }

    protected function cleanUpHtml($xhtml)
    {
        return preg_replace("/&(?!amp;|gt;|lt;|quot;)/", "AmPeRsAnD", $xhtml);
    }
}
