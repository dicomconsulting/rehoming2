<?php
Yii::import('application.modules.dataSource.models.Tasks.*');

/**
 * @cron-ready
 */
class MailCommand extends RhConsoleCommand
{
    /**
     * Email server's address for connection by IMAP protocol
     *
     * @var string
     */
    public $server;

    /**
     * Email server's username for connection by IMAP protocol
     *
     * @var string
     */
    public $username;

    /**
     * Email server's password for connection by IMAP protocol
     *
     * @var string
     */
    public $password;

    /**
     * Email address for "from email" filter
     *
     * @var string
     */
    public $biotronikEmail;

    /**
     * Default mailbox name
     *
     * @var string
     */
    private $defaultMailbox = 'INBOX';

    /**
     * Default encoding for messages
     *
     * @var string
     */
    public $defaultInternalEncoding = 'KOI8-R';

    /**
     * Connection to email account
     *
     * @var \Ddeboer\Imap\Connection
     */
    private $connection = null;

    /**
     * Mailbox name for processed email messages
     *
     * @var string
     */
    private $processedMailboxName = 'PROCESSED';

    /**
     * Flag file to determine the client's readiness to work
     *
     * @var string
     */
    private $installFile = 'application.runtime.mailBoxInstalled';

    public function init()
    {
        parent::init();
        mb_internal_encoding($this->defaultInternalEncoding);
    }

    public function beforeAction($action, $params)
    {
        parent::beforeAction($action, $params);

        if (empty($this->biotronikEmail)) {
            echo "You must set biotronik email address!\n ['biotronikEmail' => '<email>']";
            return false;
        }

        $groups = Yii::app()->userGroupManager->getAvailableGroups();
        $cnt = 0;
        $methodName = 'action' . $action;

        foreach ($groups as $group) {
            echo 'Processing of group "' . $group->name . '" started' . "\n";
            Yii::app()->userGroupManager->setCurrentGroupId($group->uid);
            Yii::app()->biotronikSpider->applyCurrentUserGroup();
            $this->initConnection();

            if (count($groups) == $cnt + 1) {
                //пропускаем последний
                continue;
            }

            $method = new ReflectionMethod($this, $methodName);
            $method->invokeArgs($this, $params);
            $cnt++;
        }

        return true;
    }

    protected function initConnection()
    {
        $currentGroup = Yii::app()->userGroupManager->getCurrentGroup();
        $this->username = $currentGroup->mail_user;
        $this->password = $currentGroup->mail_pass;
        $this->server = $currentGroup->mail_server;

        $server = new \Ddeboer\Imap\Server($this->server);
        $this->connection = $server->authenticate($this->username, $this->password);
        $this->checkConfiguration();
    }

    /**
     * @cron-name Обработка полученных писем
     */
    public function actionIndex()
    {
        $mailbox = $this->connection->getMailbox($this->defaultMailbox);
        $processedMailbox = $this->connection->getMailbox($this->processedMailboxName);

        $messages = $this->getMessages($mailbox);
        while ($messages->count() > 0) {
            echo '*' . "\n";
            $message = $messages->current();
            try {
                $task = new ParseEmailMessageTask();
                $task->run($message);
            } catch (CException $e) {
                echo $e->getMessage() . "\n";
                Yii::app()->mailer->send(
                    'Ошибка при выполнении задачи:' . $e->getMessage(),
                    'Сбой в системе регулярных задач.'
                );
            }
            $message->move($processedMailbox);
            $messages = $this->getMessages($mailbox);
        }
        echo "\n";
        $this->connection->close();
    }

    public function actionTestSMTP()
    {
        Yii::app()->mailer->send('Тестовое письмо для проверки работы SMTP протокола.');
    }

    private function checkConfiguration()
    {
        $fileName = Yii::getPathOfAlias($this->installFile);
        if (!file_exists($fileName)) {
            $mailboxes = $this->connection->getMailboxes();
            $existMailboxForProcessedMails = false;
            foreach ($mailboxes as $mailbox) {
                if ($mailbox->getName() == $this->processedMailboxName) {
                    $existMailboxForProcessedMails = true;
                    break;
                }
            }

            if (!$existMailboxForProcessedMails) {
                $this->connection->createMailbox($this->processedMailboxName);
            }
            file_put_contents($fileName, $this->processedMailboxName);
        }
    }

    private function getMessages(\Ddeboer\Imap\Mailbox $mailbox)
    {
        $searchExpression = new \Ddeboer\Imap\SearchExpression();
        $searchExpression->addCondition(
            new \Ddeboer\Imap\Search\Email\FromAddress($this->biotronikEmail)
        );

        return $mailbox->getMessages($searchExpression);
    }
}
