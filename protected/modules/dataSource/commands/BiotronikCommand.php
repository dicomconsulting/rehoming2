<?php
Yii::import('dataSource.models.Tasks.*');
Yii::import('dataSource.models.CSV.*');

Yii::import('patient.models.*');
Yii::import('device.models.*');
Yii::app()->getModule("option");

/**
 * @cron-ready
 */
class BiotronikCommand extends RhConsoleCommand
{
    /**
     * Нам необходимо прогнать экшены под каждой группой пользователей. Для этого экшены будут вызываться столько раз,
     * сколько у нас групп пользователей. Данный метод менят контекст группы пользователей.
     *
     * @param string $action
     * @param array $params
     * @return bool
     */
    protected function beforeAction($action, $params)
    {
        $groups = Yii::app()->userGroupManager->getAvailableGroups();
        $cnt = 0;
        $methodName = 'action' . $action;

        foreach ($groups as $group) {
            echo 'Processing of group "' . $group->name . '" started' . "\n";
            Yii::app()->userGroupManager->setCurrentGroupId($group->uid);
            Yii::app()->biotronikSpider->applyCurrentUserGroup();

            if (count($groups) == $cnt + 1) {
                //пропускаем последний
                continue;
            }

            $method = new ReflectionMethod($this, $methodName);
            $method->invokeArgs($this, $params);
            $cnt++;
        }
        return parent::beforeAction($action, $params);
    }


    /**
     * @cron-name Загрузка данных устройств
     * @cron-startDaysAgo-label Прошедшие дни
     * @cron-startDaysAgo-description Загрузка данных за последние n дней
     */
    public function actionIndex($startDaysAgo = 1)
    {
        echo "Основные команды получения данных с сайта Biotronik Home Monitoring\n\n";
        $checkDeviceModelConfigurationTask = new CheckDeviceModelConfigurationTask();
        $updateDeviceStateTask = new UpdateDeviceStateTask();
        $startDate = new RhDateTime();
        $startDate->sub(
            DateInterval::createFromDateString(intval($startDaysAgo) . ' day')
        );
        $endDate = new RhDateTime();

        $patients = Patient::model()->findAll();
        foreach ($patients as $patient) {
            if ($patient) {
                if  ((!$patient->group) || (!$patient->group->userGroup) || (!$patient->group->userGroup->admin_name)) {
                    continue;
                }
            }
            if ($patient->device->lastState instanceof DeviceState
                && $startDate < $patient->device->lastState->create_time
            ) {
                $requestStartDate = $patient->device->lastState->create_time;
            } else {
                $requestStartDate = $startDate;
            }
            echo "Loading data of " . $patient->human_readable_id . " began\n";
            $task = new DownloadDeviceDataFileTask();
            $csvFile = $task->run(
                [
                    'patientId' => $patient->external_uid,
                    'startDate' => $requestStartDate,
                    'endDate'   => $endDate
                ]
            );

            if ($csvFile instanceof CsvFile) {
                $checkDeviceModelConfigurationTask->run($patient, $csvFile);
                $updateDeviceStateTask->run($patient, $csvFile);
            } else {
                echo "The resulting file is invalid or missing required data...\n";
            }

            echo $patient->human_readable_id . " done...\n";
        }

        if (empty($patients)) {
            echo "No patients in the system.\n"
                . "You need to download them.\n"
                . "See: yiic spider help\n";
        }
    }

    /**
     * @cron-name Обновление группы у пациентов
     */
    public function actionUpdatePatientsGroupAssignment()
    {
        $firstTask = new UpdatePatientGroupListTask();
        $firstTask->run();
        $secondTask = new UpdatePatientGroupAssignmentTask();
        $secondTask->run(['patientIdArray' => []]);
    }

    public function actionLoadFromArchive($pathToArchive)
    {
        $checkDeviceModelConfigurationTask = new CheckDeviceModelConfigurationTask();
        $updateDeviceStateTask = new UpdateDeviceStateTask();
        $pathToArchive = rtrim($pathToArchive, '/ ') . DIRECTORY_SEPARATOR;

        $archiveDir = opendir($pathToArchive);
        echo "Read dir '" . $pathToArchive . "' ...\n";
        while ($filename = readdir($archiveDir)) {
            if (($filename != ".") && ($filename != "..")) {
                echo "Processing '" . $filename . "'\n";
                $filenameParts = explode('-', $filename);
                $startDate = array_pop($filenameParts);
                $endDate = array_pop($filenameParts);
                $patientName = str_replace('_', ' ', implode('-', $filenameParts));
                echo "Update profile'" . $patientName . "'\n";
                $this->actionLoadPatient($patientName);
                $patient = Patient::model()->findByAttributes(['human_readable_id' => $patientName]);
                $csvFile = new CsvFile(file_get_contents($pathToArchive . $filename));
                $checkDeviceModelConfigurationTask->run($patient, $csvFile);
                $updateDeviceStateTask->run($patient, $csvFile);
                echo $patient->human_readable_id . " done...\n";
            }
        }
        closedir($archiveDir);
    }

    /**
     * @cron-name Обработка статусов пациентов
     * @cron-startDaysAgo-label Прошедшие дни
     * @cron-startDaysAgo-description Загрузка данных за последние n дней
     */
    public function actionUpdatePatientStatuses($startDaysAgo = 1)
    {
        Yii::app()->getModule("option");
        $patients = Patient::model()->findAll();

        $startDate = new RhDateTime();
        $startDate->sub(
            DateInterval::createFromDateString(intval($startDaysAgo) . ' day')
        );
        $endDate = new RhDateTime();

        foreach ($patients as $patient) {
            echo "Processing '" . $patient->human_readable_id . "'\n";
            /** @var Patient $patient */
            $states = $patient->device->getStatesByPeriod($startDate, $endDate);

            foreach ($states as $state) {
                $patient->device->assignStateValuesToDeviceModelParameters($state);
                $op = new OptionsProcessor($patient);
                $op->process();
                unset($patient->device);
            }
            echo $patient->human_readable_id . " done...\n";
            unset($row);
        }
    }

    /**
     * @cron-name Обновление данных по врачам
     */
    public function actionLoadDoctors()
    {
        $firstTask = new GetDoctorLoginArrayTask();
        $loginArray = $firstTask->run();
        $secondTask = new UpdatePatientGroupListTask();
        $secondTask->run();
        foreach ($loginArray as $login) {
            $thirdTask = new UpdateDoctorTask();
            $thirdTask->run(['doctorLogin' => $login]);

            $fourthTask = new UpdateDoctorAccessTask();
            $fourthTask->run(['doctorLogin' => $login]);
        }
    }

    public function actionLoadDoctor($login)
    {
        $thirdTask = new UpdateDoctorTask();
        try {
            $thirdTask->run(['doctorLogin' => $login]);

            $fourthTask = new UpdateDoctorAccessTask();
            $fourthTask->run(['doctorLogin' => $login]);
        } catch (Exception $e) {
            echo "skipping... \n";
        }
    }

    /**
     * @cron-name Обновление данных по группам пациентов
     */
    public function actionLoadGroups()
    {
        $secondTask = new UpdatePatientGroupListTask();
        $secondTask->run();
    }
    /**
     * @cron-name Обновление данных пациента - загрузка отсутствующих, обновление существующих
     */
    public function actionLoadPatients()
    {
        $secondTask = new UpdatePatientsListTask();
        $secondTask->run();
    }

    /**
     * @cron-name Загрузка данных пациента
     * @cron-uniquePatientName-label Уникальный идентификатор пациента
     * @ cron-uniquePatientName-dataProvider json ["BiotronikCommand", "getPatientIdList"]
     */
    public function actionLoadPatient($uniquePatientName)
    {
        $getPatientIdByPatientUniqueNameTask = new GetPatientIdByPatientUniqueNameTask();
        try {
            $updatePatientByPatientIdTask = new UpdatePatientByPatientIdTask();
            $updatePatientByPatientIdTask->run(
                [
                    'patientId' => $getPatientIdByPatientUniqueNameTask->run(['patientName' => $uniquePatientName])
                ]
            );
        } catch (CException $e) {
            echo $e->getMessage() . ', Patient name - ' . $uniquePatientName . "\n";
        }
    }

    /**
     * @cron-name Обновление истории пациентов
     */
    public function actionUpdateHistory()
    {
        $patients = Patient::model()->findAll();
        foreach ($patients as $patient) {
            echo "Loading history for " . $patient->human_readable_id . " began\n";
            $task = new UpdatePatientHistoryTask();
            try {
                $task->run(
                    [
                        'patientId' => $patient->external_uid,
                        'deviceId'  => $patient->device->uid
                    ]
                );

                echo "Loading history for " . $patient->human_readable_id . " done...\n";

            } catch (CException $e) {
                echo $patient->human_readable_id . " {$e->getMessage()}\n";
            }
        }

        if (empty($patients)) {
            echo "No patients in the system.\n"
                . "You need to download them.\n"
                . "See: yiic spider help\n";
        }
    }

    /**
     * @cron-name Загрузка ЭКГ изображений
     * @cron-deviceId-label Идентификатор устройства
     * @cron-episodeNumber-label Номер эпизода
     * @cron-all-label Все
     * @cron-all-description Загрузить отсутствующие изображения по всем эпизодам
     * @cron-deviceId-dataProvider json ["BiotronikCommand", "getDeviceIdList"]
     * @cron-all-data json ["Нет", "Да"]
     */
    public function actionDownloadECG($deviceId = null, $episodeNumber = null, $all = false)
    {
        if ($all == false && (!isset($deviceId) || !isset($episodeNumber))) {
            echo 'You must specify the device number and the episode number, or pass a parameter --all is equal to true.';
            echo "\n";
            return 1;
        }

        if ($all) {
            $identificationArray = Recording::getIdentificationWithoutECG();
        } else {
            $identificationArray = [
                ['device_id' => $deviceId, 'episode_number' => $episodeNumber]
            ];
        }
        $task = new DownloadECGImageTask();
        foreach ($identificationArray as $item) {
            $task->run(['deviceId' => $item['device_id'], 'episodeNumber' => $item['episode_number']]);
        }
        echo "Processed " . count($identificationArray) . " items.\n";
    }

    /**
     * @cron-name Обновление настроек устройств
     */
    public function actionUpdateOptions($patientHumanReadableId = null, $all = false)
    {
        if ($all == false && (!isset($patientHumanReadableId))) {
            echo "You must specify the patient human readable id or pass a parameter --all is equal to true.\n";
            return 1;
        }

        if ($all) {
            $patientArray = Patient::model()->findAll();
        } else {
            /** @var Patient $patient */
            $patient = Patient::model()->findByAttributes(['human_readable_id' => $patientHumanReadableId]);
            if ($patient instanceof Patient) {
                $patientArray = [$patient];
            } else {
                echo "Incorrect patient human readable id.\n";
                echo "You must specify the correct patient human readable id or " .
                    "pass a parameter --all is equal to true.\n";
                return 1;
            }
        }
        $task = new UpdatePatientOptionsTask();
        foreach ($patientArray as $patient) {
            try {
                $task->run(['patient' => $patient]);
            } catch (CException $e) {
                echo($patient->human_readable_id . ': ' . $e->getMessage() . "\n");
            }
        }
        echo "Processed " . count($patientArray) . " patient.\n";
    }

    public function getHelp()
    {
        return "Available commands:\n"
        . "\tloadFromArchive - for loading patient device's data from csv files.\n"
        . "\tParameters: --pathToArchive=<string>\n\n"
        . "\tloadDoctors - for load doctors, users, patient groups and acces rights for doctors by groups.\n"
        . "\tParameters: none\n\n"
        . "\tloadGroups  - for load patient groups.\n"
        . "\tParameters: none\n\n"
        . "\tloadPatient - for load patient, devices and device models.\n"
        . "\tParameters: --uniquePatientName=<string>\n\n"
        . "\tupdateHistory - for reload patient's history.\n"
        . "\tParameters: none\n\n"
        . "\tdownloadECG - for download ECG image for all records by all patients (if all=true) or for needle record.\n"
        . "\tParameters:\n"
        . "\t\t--deviceId=<integer>\n"
        . "\t\t--episodeNumber=<integer>\n"
        . "\t\t--all=<boolean>\n\n"
        . "\tupdateOptions - for update options for all exist patients (if all=true) or for needle exist patient.\n"
        . "\tParameters:\n"
        . "\t\t--patientHumanReadableId=<string>\n"
        . "\t\t--all=<boolean>\n\n";
    }

    public static function getDeviceIdList()
    {
        return CHtml::listData(
            Device::model()->findAll(),
            "uid",
            function ($device) {
                return $device->patient->human_readable_id . ' (' . $device->serial_number . ')';
            }
        );
    }

    public static function getPatientIdList()
    {
        return CHtml::listData(
            Patient::model()->findAll(),
            "human_readable_id",
            "human_readable_id"
        );
    }
}
