<?php

return CMap::mergeArray(
    array(
        'components'=>array(
            'fixture'=>array(
                'class'=>'system.test.CDbFixtureManager',
            )
        ),
    ),
    require(
        $applicationPath
        . DIRECTORY_SEPARATOR . 'config'
        . DIRECTORY_SEPARATOR . 'test.php'
    )
);
