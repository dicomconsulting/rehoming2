<?php

$yiit = implode(
    DIRECTORY_SEPARATOR,
    [
        dirname(__FILE__),
        '..',
        '..',
        '..',
        '..',
        'vendor',
        'yiisoft',
        'yii',
        'framework',
        'yiit.php'
    ]
);
/*
 * Var use in config/test.php
 * Path to application main folder
 */
$applicationPath = implode(
    DIRECTORY_SEPARATOR,
    [
        dirname(__FILE__),
        '..',
        '..',
        '..'
    ]
);

require_once($yiit);
$config =  require_once dirname(__FILE__).'/config/test.php';

Yii::$enableIncludePath = false;
Yii::setPathOfAlias('tests', dirname(__FILE__));
Yii::import('tests.*');

Yii::createWebApplication($config);
