<?php

class DataSourceTestSuite extends PHPUnit_Framework_TestSuite
{
    public function __construct($theClass = '', $name = '')
    {
        parent::__construct($theClass, $name);
        $customCaseClassName = 'CustomTestCase';
        if (!class_exists($customCaseClassName)) {
            require_once $customCaseClassName . '.php';
        }
    }
    public static function suite()
    {
        $suite = new self();

        return $suite;
    }
}
