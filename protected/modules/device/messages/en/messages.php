<?php

return [
    'Параметр' => 'Parameter',
    'Синоним' => 'Synonym',
    'Модель' => 'Model',
    'Статус' => 'Status',
    'Последняя рассинхронизация данных' => 'The latest data out of sync',
    'Подготовлен' => 'Ready',
    'Новый' => 'New',
    'Изменённый' => 'Changed',
    'Вернуться к списку' => 'Back to list',
    'Редактировать' => 'Edit',
    'Включить в обработку' => 'Add to processing',
    'Отмена' => 'Cancel',
    'Удалить' => 'Delete',
    'Поля отмеченные' => 'Fields with',
    'обязательны для заполнения' => 'are required',
    'Единицы измерения параметров различны' => 'Parameter measurement units are not equal'
];
