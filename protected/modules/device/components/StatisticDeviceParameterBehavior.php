<?php

class StatisticDeviceParameterBehavior extends CBehavior
{
    public function getMaxValue()
    {
        /** @var DeviceParameter $owner */
        $owner = $this->getOwner();
        return $owner->transformationFromDb($this->_calculateStatisticValue('MAX(ds_alias4.value) as max'));
    }

    public function getMeanValue()
    {
        /** @var DeviceParameter $owner */
        $owner = $this->getOwner();
        return $owner->transformationFromDb($this->_calculateStatisticValue('AVG(ds_alias4.value) as mean'));
    }

    public function getMinValue()
    {
        /** @var DeviceParameter $owner */
        $owner = $this->getOwner();
        return $owner->transformationFromDb($this->_calculateStatisticValue('MIN(ds_alias4.value) as min'));
    }

    public function getMaxByCountValue($precision = 0)
    {
        $rows = $this->getCountStatisticByValue();
        $numberMax = null;
        $stringMax = null;
        foreach ($rows as $row) {
            if ($row['number'] == false) {
                $stringMax = empty($stringMax) ?
                    $row['value'] : ($stringMax > $row['value'] ? $stringMax : $row['value']);
            } else {
                $numberMax = empty($numberMax) ?
                    $row['value'] : ($numberMax > $row['value'] ? $numberMax : $row['value']);
            }
        }

        return !empty($numberMax) ? round($numberMax, intval($precision)) : $stringMax;
    }

    public function getMeanByCountValue($precision = 0)
    {
        $rows = $this->getCountStatisticByValue();
        $sum = 0;
        $amount = 0;
        foreach ($rows as $row) {
            if ($row['number'] == false) {
                return $row['value'];
            } else {
                $sum += ($row['value'] * $row['count']);
                $amount += $row['count'];
            }
        }
        return $amount > 0 ? round($sum/$amount, intval($precision)) : $sum;
    }

    public function getMinByCountValue($precision = 0)
    {
        $rows = $this->getCountStatisticByValue();
        $numberMin = null;
        $stringMin = null;
        foreach ($rows as $row) {
            if ($row['number'] == false) {
                $stringMin = empty($stringMin) ?
                    $row['value'] : ($stringMin < $row['value'] ? $stringMin : $row['value']);
            } else {
                $numberMin = empty($numberMin) ?
                    $row['value'] : ($numberMin < $row['value'] ? $numberMin : $row['value']);
            }
        }

        return !empty($numberMin) ? round($numberMin, intval($precision)) : $stringMin;
    }

    public function getCountStatisticByValue()
    {
        $sql = 'SELECT ds_alias4.value, COUNT(ds_alias4.value) as count FROM
            (SELECT value FROM {{device_value}} as dv WHERE dv.parameter_id = :parameter_id AND dv.device_state_id IN
                (SELECT ds.uid FROM {{device_state}} as ds WHERE
                    ds.device_id = (
                        SELECT ds_alias1.device_id FROM {{device_state}} as ds_alias1 WHERE ds_alias1.uid = :device_state_id
                    ) AND
                    ds.last_follow_up = (
                        SELECT ds_alias2.last_follow_up FROM {{device_state}} as ds_alias2 WHERE ds_alias2.uid = :device_state_id
                    ) AND
                    ds.create_time <= (
                        SELECT ds_alias3.create_time FROM {{device_state}} as ds_alias3 WHERE ds_alias3.uid = :device_state_id
                    )
                )
           ) as ds_alias4 WHERE ds_alias4.value is not null GROUP BY ds_alias4.value';
        $command = Yii::app()->db->createCommand($sql);
        $command->bindValue(":parameter_id", $this->getOwner()->uid, PDO::PARAM_INT);
        $command->bindValue(":device_state_id", $this->getOwner()->getCurrentValue()->device_state_id, PDO::PARAM_INT);
        $rows = $command->queryAll();
        foreach ($rows as $key => $row) {
            if (preg_match('/^\d+(.\d*)?$/', $row['value'])) {
                $rows[$key]['number'] = true;
            } else {
                $rows[$key]['number'] = false;
            }
        }
        return $rows;
    }

    public function getCountByCurrentPeriod()
    {
        $sql = 'SELECT ABS(dv_last.value - dv_first.value)
            FROM {{device_value}} as dv_last, {{device_value}} as dv_first WHERE
            dv_last.parameter_id = :parameter_id
            AND dv_first.parameter_id = :parameter_id
            AND dv_last.device_state_id = :device_state_id
            AND  dv_first.device_state_id = (
                SELECT ds.uid FROM {{device_state}} as ds WHERE ds.last_follow_up = (
                    SELECT ds_alias.last_follow_up FROM {{device_state}} as ds_alias WHERE ds_alias.uid = :device_state_id
                )  ORDER BY ds.create_time ASC LIMIT 1
            );';
        $command = Yii::app()->db->createCommand($sql);
        $command->bindValue(":parameter_id", $this->getOwner()->uid, PDO::PARAM_INT);
        $command->bindValue(":device_state_id", $this->getOwner()->getCurrentValue()->device_state_id, PDO::PARAM_INT);
        return $command->queryScalar();
    }

    public function getAllValues()
    {
        $sql = 'SELECT ds.`create_time` as `time`, dv.`value` as `value` FROM (
            SELECT dv_alias.`device_state_id`, dv_alias.`value` FROM {{device_value}} as dv_alias WHERE `parameter_id` = :parameter_id AND
              EXISTS (
                  SELECT 1 FROM {{device_state}} as ds_alias WHERE ds_alias.`uid` = dv_alias.`device_state_id`
                  AND ds_alias.`device_id` = (
                      SELECT `device_id` FROM {{device_state}} WHERE `uid` = :device_state_id
                  )
              )
        ) as dv INNER JOIN (
            SELECT `uid`, `create_time` FROM {{device_state}} WHERE device_id = (
                SELECT `device_id` FROM {{device_state}} WHERE `uid` = :device_state_id
            )
        ) as ds ON dv.device_state_id = ds.uid ORDER BY `time` ASC';
        $command = Yii::app()->db->createCommand($sql);
        $command->bindValue(":parameter_id", $this->getOwner()->uid, PDO::PARAM_INT);
        $command->bindValue(":device_state_id", $this->getOwner()->getCurrentValue()->device_state_id, PDO::PARAM_INT);

        return $command->queryAll();
    }

    private function _calculateStatisticValue($selectPart)
    {
        $sql = 'SELECT ' . $selectPart . ' FROM
            (SELECT value FROM {{device_value}} as dv WHERE dv.parameter_id = :parameter_id AND dv.device_state_id IN
                (SELECT ds.uid FROM {{device_state}} as ds WHERE
                    ds.device_id = (
                        SELECT ds_alias1.device_id FROM {{device_state}} as ds_alias1 WHERE ds_alias1.uid = :device_state_id
                    ) AND
                    ds.last_follow_up = (
                        SELECT ds_alias2.last_follow_up FROM {{device_state}} as ds_alias2 WHERE ds_alias2.uid = :device_state_id
                    ) AND
                    ds.create_time <= (
                        SELECT ds_alias3.create_time FROM {{device_state}} as ds_alias3 WHERE ds_alias3.uid = :device_state_id
                    )
                )
           ) as ds_alias4';
        $command = Yii::app()->db->createCommand($sql);
        $command->bindValue(":parameter_id", $this->getOwner()->uid, PDO::PARAM_INT);
        $command->bindValue(":device_state_id", $this->getOwner()->getCurrentValue()->device_state_id, PDO::PARAM_INT);
        return $command->queryScalar();
    }

    public function getMaxByAllStates()
    {
        /** @var DeviceParameter $owner */
        $owner = $this->getOwner();

        $sql = 'SELECT MAX(CAST(`value` AS SIGNED INTEGER))
            FROM {{device_value}} as dv
            JOIN  {{device_state}} AS ds ON ds.uid = dv.device_state_id
            WHERE
              dv.parameter_id = :parameter_id
              AND ds.device_id = (
                SELECT device_id
                FROM {{device_state}}
                WHERE uid = :device_state_id );';
        $command = Yii::app()->db->createCommand($sql);
        $command->bindValue(":parameter_id", $this->getOwner()->uid, PDO::PARAM_INT);
        $command->bindValue(":device_state_id", $owner->getCurrentValue()->device_state_id, PDO::PARAM_INT);

        return $owner->transformationFromDb($command->queryScalar());
    }
}
