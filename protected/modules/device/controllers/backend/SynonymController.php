<?php

class SynonymController extends BaseAdminController
{
    protected $pageTitle = 'Синонимы';

    public function accessRules()
    {
        return [
            [
                'allow',
                'roles' => ['deviceManagement'],
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

    public function getModelClassName()
    {
        return 'DeviceParameterSynonym';
    }

    public function actionCreate()
    {
        throw new CException('Illegal action.');
    }
}
