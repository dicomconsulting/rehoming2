<?php

class m131121_053922_add_ecg_image_table extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{ecg_image}}',
            array(
                'device_id' => 'INTEGER',
                'episode_number' => 'INTEGER',
                'image_name' => 'VARCHAR(100)',
                'PRIMARY KEY (`device_id`, `episode_number`)'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );
        $this->addForeignKey(
            'fk_ecg_image_relation_device',
            '{{ecg_image}}',
            'device_id',
            '{{device}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_ecg_image_relation_device', '{{ecg_image}}');
        $this->dropTable('{{ecg_image}}');
    }
}
