<?php

class m131021_051024_add_last_followup_datetime_field extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{device_state}}', 'last_follow_up', 'DATETIME');
    }

    public function safeDown()
    {
        $this->dropColumn('{{device_state}}', 'last_follow_up');
    }
}
