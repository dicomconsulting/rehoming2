<?php

class m150111_102131_change_parameter_i18n extends I18nDbMigration
{

    /**
     * Переименовать параметры по старому имени
     *
     * @param string $newName
     * @param string $oldName
     * @throws \CException
     */
    protected function renameParameterByOldName($newName, $oldName)
    {
        $this->update(
            '{{parameter_i18n}}',
            [
                'name' => $newName
            ],
            'name = ' . $this->getDbConnection()->quoteValue($oldName)
        );

    }

    /**
     * Переименовать параметр по его системному имени
     *
     * @param string $newName
     * @param string $systemName
     * @throws \CException
     */
    protected function renameParameterBySystemName($newName = '', $systemName = '')
    {
        $uid = $this->getDbConnection()->createCommand()->select('uid')->from('{{parameter}}')->where(
            'system_name=:system_name',
            ['system_name' => $systemName]
        )->queryScalar();
        if ($uid) {
            $this->update(
                '{{parameter_i18n}}',
                [
                    'name' => $newName
                ],
                'uid = ' . $this->getDbConnection()->quoteValue($uid)
            );
        }
    }


    protected function getRenameByOldNameList()
    {
        return [
            'Время последней зарядки'                            => 'Время последнего набора заряда',
            'Чувствительность ПП (средняя за день)'              => 'Амплитуда сигнала ПП (средняя за день)',
            'Чувствительность ПП (минимальная за день)'          => 'Амплитуда сигнала ПП (минимальная за день)',
            'Чувствительность ПЖ (средняя за день)'              => 'Амплитуда сигнала ПЖ (средняя за день)',
            'Чувствительность ПЖ (минимальная за день)'          => 'Амплитуда сигнала ПЖ (минимальная за день)',
            'Чувствительность ЛЖ (средняя за день)'              => 'Амплитуда сигнала ЛЖ (средняя за день)',
            'Чувствительность ЛЖ (минимальная за день)'          => 'Амплитуда сигнала ЛЖ (минимальная за день)',
            'Дата последней трансмиссии'                         => 'Дата последней передачи данных',
            'Эффективность предсердных стимулов'                 => 'Стимуляция предсердий',
            'Эффективность  желудочковых стимулов'               => 'Стимуляция желудочков',
            'CRT стимуляция'                                     => 'CRT стимуляция',
            'Время трансмиссии'                                  => 'Время передачи данных',
            'Зафиксирован длительные эпизод предсердной аритмии' => 'Зафиксирован длительный эпизод предсердной аритмии'
        ];
    }

    public function safeUp()
    {
        $renameByOldName = $this->getRenameByOldNameList();
        foreach ($renameByOldName as $oldName => $newName) {
            $this->renameParameterByOldName($newName, $oldName);
        }
        $this->renameParameterBySystemName('Переключение режимов', 'mode_switching_mode');

    }

    public function safeDown()
    {
        $renameByOldName = $this->getRenameByOldNameList();
        foreach ($renameByOldName as $oldName => $newName) {
            $this->renameParameterByOldName($oldName, $newName);
        }
        $this->renameParameterBySystemName('Режим', 'mode_switching_mode');
    }
}
