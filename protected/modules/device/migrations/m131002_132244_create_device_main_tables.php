<?php

class m131002_132244_create_device_main_tables extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTableWithI18n(
            '{{device_model}}',
            array(
                'name' => 'VARCHAR(100)',
                'short_name' => 'VARCHAR(50)',
                'PRIMARY KEY (`name`)'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci',
            array('short_name')
        );

        $this->createTableWithI18n(
            '{{parameter}}',
            array(
                'uid' => 'pk',
                'name' => 'VARCHAR(50)',
                'csv_name' => 'VARCHAR(100)',
                'measurement_unit_id' => 'INTEGER(11)'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci',
            array('name')
        );
        $this->addForeignKey(
            'fk_parameter_measurement_unit_uid',
            '{{parameter}}',
            'measurement_unit_id',
            '{{measurement_unit}}',
            'uid',
            'RESTRICT',
            'CASCADE'
        );

        $this->createTable(
            '{{parameter_model_device_relation}}',
            array(
                'parameter_id' => 'INTEGER(11)',
                'device_model_name' => 'VARCHAR(100)',
                'UNIQUE `relation` (`parameter_id`, `device_model_name`)'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );
        $this->addForeignKey(
            'fk_parameter_model_device_relation_parameter_uid',
            '{{parameter_model_device_relation}}',
            'parameter_id',
            '{{parameter}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_parameter_model_device_relation_device_model',
            '{{parameter_model_device_relation}}',
            'device_model_name',
            '{{device_model}}',
            'name',
            'CASCADE',
            'CASCADE'
        );

        $this->createTableWithI18n(
            '{{parameter_group}}',
            array(
                'uid' => 'pk',
                'name' => 'VARCHAR(100)',
                'system_name' => 'VARCHAR(30)',
                'parent_group_id' => 'INTEGER(11)',
                'UNIQUE `system_name` (`system_name`)'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci',
            array('name')
        );

        $this->createTableWithI18n(
            '{{parameter_group_parameter_relation}}',
            array(
                'parameter_id' => 'INTEGER(11)',
                'parameter_group_id' => 'INTEGER(11)',
                'UNIQUE `relation` (`parameter_id`, `parameter_group_id`)'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci',
            array('name')
        );
        $this->addForeignKey(
            'fk_parameter_group_parameter_relation_parameter_uid',
            '{{parameter_group_parameter_relation}}',
            'parameter_id',
            '{{parameter}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_parameter_group_parameter_relation_parameter_group_id',
            '{{parameter_group_parameter_relation}}',
            'parameter_group_id',
            '{{parameter_group}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_parameter_measurement_unit_uid',
            '{{parameter}}'
        );
        $this->dropForeignKey(
            'fk_parameter_model_device_relation_parameter_uid',
            '{{parameter_model_device_relation}}'
        );
        $this->dropForeignKey(
            'fk_parameter_model_device_relation_device_model',
            '{{parameter_model_device_relation}}'
        );
        $this->dropForeignKey(
            'fk_parameter_group_parameter_relation_parameter_uid',
            '{{parameter_group_parameter_relation}}'
        );
        $this->dropForeignKey(
            'fk_parameter_group_parameter_relation_parameter_group_id',
            '{{parameter_group_parameter_relation}}'
        );

        $this->truncateTableWithI18n('{{device_model}}');
        $this->truncateTableWithI18n('{{parameter}}');
        $this->truncateTableWithI18n('{{parameter_model_device_relation}}');
        $this->truncateTableWithI18n('{{parameter_group}}');
        $this->truncateTableWithI18n('{{parameter_group_parameter_relation}}');

        $this->dropTableWithI18n('{{device_model}}');
        $this->dropTableWithI18n('{{parameter}}');
        $this->dropTableWithI18n('{{parameter_model_device_relation}}');
        $this->dropTableWithI18n('{{parameter_group}}');
        $this->dropTableWithI18n('{{parameter_group_parameter_relation}}');
    }
}
