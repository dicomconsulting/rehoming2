<?php

class m131210_094822_add_device_state_transformer_table extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{device_state_transformer}}',
            array(
                'uid' => 'pk',
                'device_model' => 'VARCHAR(100) NOT NULL',
                'parameter_system_name' => 'VARCHAR(100) NOT NULL',
                'class_name' => 'VARCHAR(100)',
                'method_name' => 'VARCHAR(100) NOT NULL'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );
        $this->addForeignKey(
            'fk_device_state_transformer_relation_parameter',
            '{{device_state_transformer}}',
            'parameter_system_name',
            '{{parameter}}',
            'system_name',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_device_state_transformer_relation_parameter', '{{device_state_transformer}}');
        $this->dropTable('{{device_state_transformer}}');
    }
}
