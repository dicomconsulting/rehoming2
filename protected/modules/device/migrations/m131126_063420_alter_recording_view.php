<?php

class m131126_063420_alter_recording_view extends I18nDbMigration
{
    public function safeUp()
    {
        $this->execute('CREATE OR REPLACE
SQL SECURITY DEFINER
VIEW {{vw_recording_state}}
AS
SELECT
state.uid AS state_uid, device_id, last_follow_up, state.`create_time` AS state_create_time
	, val_ep_num.`value` AS episode_number
	, val_ep_type.`value` AS episode_type
	, val_detection.value AS detection
	, val_episode_duration.value AS episode_duration
	, val_mean_pp_at_initial_detection.value AS mean_pp_at_initial_detection
	, val_mean_rr_at_initial_detection.value AS mean_rr_at_initial_detection
	, val_onset.value AS onset
	, val_stability.value AS stability
	, val_redetection.value AS redetection
	, val_atp_one_shot_delivered.value AS atp_one_shot_delivered
	, val_episode_shocks_delivered.value AS episode_shocks_delivered
	, val_episode_shocks_aborted.value AS episode_shocks_aborted
	, val_maximum_energy.value AS maximum_energy
	, val_mean_pp_at_termination.value AS mean_pp_at_termination
	, val_mean_rr_at_termination.value AS mean_rr_at_termination
	, val_remark_detection.value AS remark_detection
	, val_remark_therapy.value AS remark_therapy
	, val_remark_termination.value AS remark_termination
	, val_atp_in_vt_vf_delivered.value AS atp_in_vt_vf_delivered

	FROM rh_device_value AS val
	JOIN `rh_device_state` AS state ON val.`device_state_id` = `state`.`uid`
LEFT JOIN rh_device_value AS val_ep_num ON val_ep_num.`device_state_id` = `state`.`uid` AND val_ep_num.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "episode_number")
LEFT JOIN rh_device_value AS val_ep_type ON val_ep_type.`device_state_id` = `state`.`uid` AND val_ep_type.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "episode_type")
LEFT JOIN rh_device_value AS val_detection ON val_detection.`device_state_id` = `state`.`uid` AND val_detection.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "detection")
LEFT JOIN rh_device_value AS val_episode_duration ON val_episode_duration.`device_state_id` = `state`.`uid` AND val_episode_duration.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "episode_duration")
LEFT JOIN rh_device_value AS val_mean_pp_at_initial_detection ON val_mean_pp_at_initial_detection.`device_state_id` = `state`.`uid` AND val_mean_pp_at_initial_detection.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "mean_pp_at_initial_detection")
LEFT JOIN rh_device_value AS val_mean_rr_at_initial_detection ON val_mean_rr_at_initial_detection.`device_state_id` = `state`.`uid` AND val_mean_rr_at_initial_detection.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "mean_rr_at_initial_detection")
LEFT JOIN rh_device_value AS val_onset ON val_onset.`device_state_id` = `state`.`uid` AND val_onset.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "onset")
LEFT JOIN rh_device_value AS val_stability ON val_stability.`device_state_id` = `state`.`uid` AND val_stability.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "stability")
LEFT JOIN rh_device_value AS val_redetection ON val_redetection.`device_state_id` = `state`.`uid` AND val_redetection.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "redetection")
LEFT JOIN rh_device_value AS val_atp_in_vt_vf_delivered ON val_atp_in_vt_vf_delivered.`device_state_id` = `state`.`uid` AND val_atp_in_vt_vf_delivered.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "atp_in_vt_vf_delivered")
LEFT JOIN rh_device_value AS val_atp_one_shot_delivered ON val_atp_one_shot_delivered.`device_state_id` = `state`.`uid` AND val_atp_one_shot_delivered.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "atp_one_shot_delivered")
LEFT JOIN rh_device_value AS val_episode_shocks_delivered ON val_episode_shocks_delivered.`device_state_id` = `state`.`uid` AND val_episode_shocks_delivered.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "episode_shocks_delivered")
LEFT JOIN rh_device_value AS val_episode_shocks_aborted ON val_episode_shocks_aborted.`device_state_id` = `state`.`uid` AND val_episode_shocks_aborted.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "episode_shocks_aborted")
LEFT JOIN rh_device_value AS val_maximum_energy ON val_maximum_energy.`device_state_id` = `state`.`uid` AND val_maximum_energy.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "maximum_energy")
LEFT JOIN rh_device_value AS val_mean_pp_at_termination ON val_mean_pp_at_termination.`device_state_id` = `state`.`uid` AND val_mean_pp_at_termination.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "mean_pp_at_termination")
LEFT JOIN rh_device_value AS val_mean_rr_at_termination ON val_mean_rr_at_termination.`device_state_id` = `state`.`uid` AND val_mean_rr_at_termination.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "mean_rr_at_termination")
LEFT JOIN rh_device_value AS val_remark_detection ON val_remark_detection.`device_state_id` = `state`.`uid` AND val_remark_detection.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "remark_detection")
LEFT JOIN rh_device_value AS val_remark_therapy ON val_remark_therapy.`device_state_id` = `state`.`uid` AND val_remark_therapy.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "remark_therapy")
LEFT JOIN rh_device_value AS val_remark_termination ON val_remark_termination.`device_state_id` = `state`.`uid` AND val_remark_termination.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "remark_termination")
	WHERE
		val.`parameter_id` =  (SELECT uid FROM rh_parameter WHERE system_name = "episode_number")
		AND val.`value` IS NOT NULL
UNION
SELECT
  NULL                              AS `state_uid`,
  `state`.`device_id`                        AS `device_id`,
  `state`.`last_follow_up`                   AS `last_follow_up`,
  `state`.`last_follow_up`                   AS `state_create_time`,
  0                                          AS `episode_number`,
  \'Follow Up\'                              AS `episode_type`,
  `state`.`last_follow_up`                   AS `detection`,
  0             AS `episode_duration`,
  0 AS `mean_pp_at_initial_detection`,
  0 AS `mean_rr_at_initial_detection`,
  0                        AS `onset`,
  0                    AS `stability`,
  0                  AS `redetection`,
  0       AS `atp_one_shot_delivered`,
  0     AS `episode_shocks_delivered`,
  0       AS `episode_shocks_aborted`,
  0               AS `maximum_energy`,
  0       AS `mean_pp_at_termination`,
  0       AS `mean_rr_at_termination`,
  0           AS `remark_detection`,
  0               AS `remark_therapy`,
  0           AS `remark_termination`,
  0       AS `atp_in_vt_vf_delivered`

FROM rh_device_state AS state
  GROUP BY state.`device_id`, state.`last_follow_up`');
    }

    public function safeDown()
    {
        $this->execute('CREATE OR REPLACE
SQL SECURITY DEFINER
VIEW {{vw_recording_state}}
AS
SELECT
state.uid AS state_uid, device_id, last_follow_up, state.`create_time` AS state_create_time
	, val_ep_num.`value` AS episode_number
	, val_ep_type.`value` AS episode_type
	, val_detection.value AS detection
	, val_episode_duration.value AS episode_duration
	, val_mean_pp_at_initial_detection.value AS mean_pp_at_initial_detection
	, val_mean_rr_at_initial_detection.value AS mean_rr_at_initial_detection
	, val_onset.value AS onset
	, val_stability.value AS stability
	, val_redetection.value AS redetection
	, val_atp_one_shot_delivered.value AS atp_one_shot_delivered
	, val_episode_shocks_delivered.value AS episode_shocks_delivered
	, val_episode_shocks_aborted.value AS episode_shocks_aborted
	, val_maximum_energy.value AS maximum_energy
	, val_mean_pp_at_termination.value AS mean_pp_at_termination
	, val_mean_rr_at_termination.value AS mean_rr_at_termination
	, val_remark_detection.value AS remark_detection
	, val_remark_therapy.value AS remark_therapy
	, val_remark_termination.value AS remark_termination
	, val_atp_in_vt_vf_delivered.value AS atp_in_vt_vf_delivered

	FROM rh_device_value AS val
	JOIN `rh_device_state` AS state ON val.`device_state_id` = `state`.`uid`
LEFT JOIN rh_device_value AS val_ep_num ON val_ep_num.`device_state_id` = `state`.`uid` AND val_ep_num.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "episode_number")
LEFT JOIN rh_device_value AS val_ep_type ON val_ep_type.`device_state_id` = `state`.`uid` AND val_ep_type.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "episode_type")
LEFT JOIN rh_device_value AS val_detection ON val_detection.`device_state_id` = `state`.`uid` AND val_detection.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "detection")
LEFT JOIN rh_device_value AS val_episode_duration ON val_episode_duration.`device_state_id` = `state`.`uid` AND val_episode_duration.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "episode_duration")
LEFT JOIN rh_device_value AS val_mean_pp_at_initial_detection ON val_mean_pp_at_initial_detection.`device_state_id` = `state`.`uid` AND val_mean_pp_at_initial_detection.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "mean_pp_at_initial_detection")
LEFT JOIN rh_device_value AS val_mean_rr_at_initial_detection ON val_mean_rr_at_initial_detection.`device_state_id` = `state`.`uid` AND val_mean_rr_at_initial_detection.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "mean_rr_at_initial_detection")
LEFT JOIN rh_device_value AS val_onset ON val_onset.`device_state_id` = `state`.`uid` AND val_onset.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "val_onset")
LEFT JOIN rh_device_value AS val_stability ON val_stability.`device_state_id` = `state`.`uid` AND val_stability.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "stability")
LEFT JOIN rh_device_value AS val_redetection ON val_redetection.`device_state_id` = `state`.`uid` AND val_redetection.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "redetection")
LEFT JOIN rh_device_value AS val_atp_in_vt_vf_delivered ON val_atp_in_vt_vf_delivered.`device_state_id` = `state`.`uid` AND val_atp_in_vt_vf_delivered.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "atp_in_vt_vf_delivered")
LEFT JOIN rh_device_value AS val_atp_one_shot_delivered ON val_atp_one_shot_delivered.`device_state_id` = `state`.`uid` AND val_atp_one_shot_delivered.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "atp_one_shot_delivered")
LEFT JOIN rh_device_value AS val_episode_shocks_delivered ON val_episode_shocks_delivered.`device_state_id` = `state`.`uid` AND val_episode_shocks_delivered.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "episode_shocks_delivered")
LEFT JOIN rh_device_value AS val_episode_shocks_aborted ON val_episode_shocks_aborted.`device_state_id` = `state`.`uid` AND val_episode_shocks_aborted.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "episode_shocks_aborted")
LEFT JOIN rh_device_value AS val_maximum_energy ON val_maximum_energy.`device_state_id` = `state`.`uid` AND val_maximum_energy.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "maximum_energy")
LEFT JOIN rh_device_value AS val_mean_pp_at_termination ON val_mean_pp_at_termination.`device_state_id` = `state`.`uid` AND val_mean_pp_at_termination.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "mean_pp_at_termination")
LEFT JOIN rh_device_value AS val_mean_rr_at_termination ON val_mean_rr_at_termination.`device_state_id` = `state`.`uid` AND val_mean_rr_at_termination.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "mean_rr_at_termination")
LEFT JOIN rh_device_value AS val_remark_detection ON val_remark_detection.`device_state_id` = `state`.`uid` AND val_remark_detection.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "remark_detection")
LEFT JOIN rh_device_value AS val_remark_therapy ON val_remark_therapy.`device_state_id` = `state`.`uid` AND val_remark_therapy.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "remark_therapy")
LEFT JOIN rh_device_value AS val_remark_termination ON val_remark_termination.`device_state_id` = `state`.`uid` AND val_remark_termination.`parameter_id` = (SELECT uid FROM rh_parameter WHERE system_name = "remark_termination")
	WHERE
		val.`parameter_id` =  (SELECT uid FROM rh_parameter WHERE system_name = "episode_number")
		AND val.`value` IS NOT NULL
UNION
SELECT
  NULL                              AS `state_uid`,
  `state`.`device_id`                        AS `device_id`,
  `state`.`last_follow_up`                   AS `last_follow_up`,
  `state`.`last_follow_up`                   AS `state_create_time`,
  0                                          AS `episode_number`,
  \'Follow Up\'                              AS `episode_type`,
  `state`.`last_follow_up`                   AS `detection`,
  0             AS `episode_duration`,
  0 AS `mean_pp_at_initial_detection`,
  0 AS `mean_rr_at_initial_detection`,
  0                        AS `onset`,
  0                    AS `stability`,
  0                  AS `redetection`,
  0       AS `atp_one_shot_delivered`,
  0     AS `episode_shocks_delivered`,
  0       AS `episode_shocks_aborted`,
  0               AS `maximum_energy`,
  0       AS `mean_pp_at_termination`,
  0       AS `mean_rr_at_termination`,
  0           AS `remark_detection`,
  0               AS `remark_therapy`,
  0           AS `remark_termination`,
  0       AS `atp_in_vt_vf_delivered`

FROM rh_device_state AS state
  GROUP BY state.`device_id`, state.`last_follow_up`');
    }
}
