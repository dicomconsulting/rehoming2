<?php

class m150114_130921_create_new_device_model extends I18nDbMigration
{
    public function safeUp()
    {
        $this->insertMultiple('{{device_model}}', [
            [
                'name'            => 'Iforia 3 HF-T',
                'device_type_uid' => '2'
            ],
            [
                'name'            => 'Iforia 3 DR-T',
                'device_type_uid' => '2'
            ]
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{device_model}}', 'name="Iforia 3 HF-T" or name="Iforia 3 DR-T"');
    }
}
