<?php

class m131224_200604_change_parameter_synonym_table extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{parameter_synonym}}', 'uid', 'pk');
        $this->addColumn('{{parameter_synonym}}', 'state_id', 'INTEGER NOT NULL DEFAULT 0');
        $this->addColumn('{{parameter_synonym}}', 'start_conflict_time', 'DATETIME DEFAULT NULL');
    }

    public function safeDown()
    {
        $this->alterColumn('{{parameter_synonym}}', 'uid', 'INTEGER');
        $this->dropPrimaryKey('uid', '{{parameter_synonym}}');
        $this->dropColumn('{{parameter_synonym}}', 'uid');
        $this->dropColumn('{{parameter_synonym}}', 'state_id');
        $this->dropColumn('{{parameter_synonym}}', 'start_conflict_time');
    }
}
