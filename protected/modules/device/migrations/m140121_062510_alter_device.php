<?php

class m140121_062510_alter_device extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addForeignKey(
            'fk_device_patient_id_patient_uid',
            '{{device}}',
            'patient_id',
            '{{patient}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_device_patient_id_patient_uid',
            '{{device}}'
        );
    }
}
