<?php

class m131030_111604_add_active_since_date_column extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{device}}', 'active_since_date', 'DATE DEFAULT NULL');
    }

    public function safeDown()
    {
        $this->dropColumn('{{device}}', 'active_since_date');
    }
}
