<?php

class m131015_111225_change_device_parameter_name_length extends I18nDbMigration
{
    public function safeUp()
    {
        $this->alterColumn('{{parameter_i18n}}', 'name', 'VARCHAR(100)');
    }

    public function safeDown()
    {
        $this->alterColumn('{{parameter_i18n}}', 'name', 'VARCHAR(50)');
    }
}
