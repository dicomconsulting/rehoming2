<?php

class m131216_113805_transmitter extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            "{{transmitter}}",
            [
                "uid" => "pk",
                "name" => "varchar(255) NOT NULL"
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );
    }

    public function safeDown()
    {
        $this->dropTable("{{transmitter}}");
    }
}
