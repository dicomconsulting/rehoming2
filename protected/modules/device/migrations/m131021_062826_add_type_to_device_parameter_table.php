<?php

class m131021_062826_add_type_to_device_parameter_table extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{parameter}}', 'type', 'INTEGER');
    }

    public function safeDown()
    {
        $this->dropColumn('{{parameter}}', 'type');
    }
}
