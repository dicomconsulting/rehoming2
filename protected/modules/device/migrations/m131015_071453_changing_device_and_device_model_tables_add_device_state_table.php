<?php

class m131015_071453_changing_device_and_device_model_tables_add_device_state_table extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{device_state}}',
            array(
                'uid' => 'pk',
                'device_id' => 'INTEGER',
                'create_time' => 'DATETIME'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );
        $this->addForeignKey(
            'fk_device_state_device_id_relation_device_uid',
            '{{device_state}}',
            'device_id',
            '{{device}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
        $this->dropForeignKey(
            'fk_device_value_parameter_id_relation_parameter_uid',
            '{{device_value}}'
        );
        $this->dropForeignKey(
            'fk_device_value_device_id_relation_device_uid',
            '{{device_value}}'
        );
        $this->execute('ALTER TABLE {{device_value}} DROP PRIMARY KEY');
        $this->renameColumn('{{device_value}}', 'device_id', 'device_state_id');
        $this->dropColumn('{{device_value}}', 'create_time');
        $this->addPrimaryKey('device_value_uid', '{{device_value}}', 'device_state_id, parameter_id');
        $this->addForeignKey(
            'fk_device_value_parameter_id_relation_parameter_uid',
            '{{device_value}}',
            'parameter_id',
            '{{parameter}}',
            'uid',
            'RESTRICT',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_device_value_device_id_relation_device_state_uid',
            '{{device_value}}',
            'device_state_id',
            '{{device_state}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );

        $this->addColumn('{{parameter}}', 'system_name', 'VARCHAR(100) NOT NULL');
        $this->createIndex('parameter_system_name_unique', '{{parameter}}', 'system_name', true);
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_device_state_device_id_relation_device_uid',
            '{{device_state}}'
        );
        $this->dropForeignKey(
            'fk_device_value_parameter_id_relation_parameter_uid',
            '{{device_value}}'
        );
        $this->dropForeignKey(
            'fk_device_value_device_id_relation_device_state_uid',
            '{{device_value}}'
        );
        $this->dropTable('{{device_state}}');
        $this->execute('ALTER TABLE {{device_value}} DROP PRIMARY KEY');
        $this->renameColumn('{{device_value}}', 'device_state_id', 'device_id');
        $this->addColumn('{{device_value}}', 'create_time', 'DATETIME');
        $this->addPrimaryKey('device_value_uid', '{{device_value}}', 'device_id, parameter_id');
        $this->addForeignKey(
            'fk_device_value_parameter_id_relation_parameter_uid',
            '{{device_value}}',
            'parameter_id',
            '{{parameter}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_device_value_device_id_relation_device_uid',
            '{{device_value}}',
            'device_id',
            '{{device}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
        $this->dropIndex('parameter_system_name_unique', '{{parameter}}');
        $this->dropColumn('{{parameter}}', 'system_name');
    }
}
