<?php

class m131106_075037_add_disable_tab_table extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{disable_tab}}',
            array(
                'tab_name' => 'VARCHAR(100)',
                'device_model_name' => 'VARCHAR(100)',
                'UNIQUE `relation` (`tab_name`, `device_model_name`)'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );
        $this->addForeignKey(
            'fk_disable_tab_relation_model_device',
            '{{disable_tab}}',
            'device_model_name',
            '{{device_model}}',
            'name',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_disable_tab_relation_model_device', '{{disable_tab}}');
        $this->dropTable('{{disable_tab}}');
    }
}
