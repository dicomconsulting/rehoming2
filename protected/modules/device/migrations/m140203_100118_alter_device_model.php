<?php

class m140203_100118_alter_device_model extends I18nDbMigration
{
    public function safeUp()
    {
        $this->addColumn(
            "{{device_model}}",
            'is_delete',
            "tinyint(1) NOT NULL DEFAULT '0'"
        );
    }

    public function safeDown()
    {
        $this->dropColumn(
            "{{device_model}}",
            'is_delete'
        );
    }
}
