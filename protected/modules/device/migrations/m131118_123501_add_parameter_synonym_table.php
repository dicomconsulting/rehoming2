<?php

class m131118_123501_add_parameter_synonym_table extends I18nDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{parameter_synonym}}',
            array(
                'parameter_id' => 'INTEGER',
                'csv_synonym' => 'VARCHAR(100)',
                'device_model_name' => 'VARCHAR(100)',
                'UNIQUE `relation` (`parameter_id`, `csv_synonym`, `device_model_name`)'
            ),
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );
        $this->addForeignKey(
            'fk_parameter_synonym_relation_parameter',
            '{{parameter_synonym}}',
            'parameter_id',
            '{{parameter}}',
            'uid',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_parameter_synonym_relation_parameter', '{{parameter_synonym}}');
        $this->dropTable('{{parameter_synonym}}');
    }
}
