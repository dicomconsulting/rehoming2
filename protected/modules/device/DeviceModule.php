<?php

class DeviceModule extends CWebModule
{
    /**
     * Initializes the module.
     */
    public function init()
    {
        Yii::app()->getModule("option");

        $this->setImport(
            array(
                'device.components.*',
                'device.models.*',
            )
        );
    }
}
