<?php
Yii::import('device.models.DeviceParameter');
Yii::import('device.models.DeviceModel');
/* @var $model DeviceParameterSynonym */
/* @var $form CActiveForm */

?>
<h2 class="offset1">
    <?=Yii::t("DeviceModule.messages", "Синоним")?>
    <?=$model->getIsNewRecord() ? '' : '№' . $model->uid;?>
</h2>
<div class="form">

    <?php $form = $this->beginWidget('I18nActiveForm', array(
        'id' => 'doctor-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => [
            'class' => 'form-horizontal'
        ]
    )); ?>

    <p class="note controls">
        <?=Yii::t("DeviceModule.messages", "Поля отмеченные");?>
        <span class="required">"*"</span>
        <?=Yii::t("DeviceModule.messages", "обязательны для заполнения");?>.
    </p>

    <div class="controls">
        <?=$form->errorSummary($model); ?>
    </div>

    <div class="control-group">
        <?=$form->labelEx($model, 'parameter_id', ['class' => 'control-label']); ?>
        <div class="controls">
            <?=$form->dropDownList(
                $model,
                'parameter_id',
                CHtml::listData(
                    DeviceParameter::model()->findAll(["order" => "name"]),
                    "uid" ,
                    function($parameter){
                        return $parameter->name;
                    }
                ),
                ['disabled' => 'disabled', 'class' => 'input-xxlarge']
            ); ?>
            <?=$form->error($model, 'parameter_id');?>
        </div>
    </div>

    <?php
        $htmlOptions = ['class' => 'input-xxlarge'];
        if (!$model->isEditable()) {
            $htmlOptions['disabled'] = 'disabled';
        }
    ?>
    <div class="control-group">
        <?=$form->labelEx($model, 'csv_synonym', ['class' => 'control-label']); ?>
        <div class="controls">
            <?=$form->textField($model, 'csv_synonym', $htmlOptions); ?>
            <?=$form->error($model, 'csv_synonym');?>
        </div>
    </div>

    <div class="control-group">
        <?=$form->labelEx($model, 'device_model_name', ['class' => 'control-label']); ?>
        <div class="controls">
            <?=$form->dropDownList(
                $model,
                'device_model_name',
                CHtml::listData(
                    DeviceModel::model()->findAll(["order" => "name"]),
                    "name" ,
                    function($deviceModel){
                        return $deviceModel->name;
                    }
                ),
                ['disabled' => 'disabled', 'class' => 'input-xxlarge']
            ); ?>
            <?=$form->error($model, 'device_model_name');?>
        </div>
    </div>

    <div class="control-group">
        <?=$form->labelEx($model, 'start_conflict_time', ['class' => 'control-label']); ?>
        <div class="controls">
            <?=$form->dateField($model, 'start_conflict_time', ['disabled' => 'disabled', 'class' => 'input-xxlarge']); ?>
            <?=$form->error($model, 'start_conflict_time');?>
        </div>
    </div>

    <div class="row buttons controls">
        <?=$model->isEditable() ? CHtml::submitButton(
            Yii::t('DeviceModule.messages', 'Включить в обработку'),
            ['class' => 'btn btn-primary']
        ) : '';?>
        <?=CHtml::link(
            Yii::t('DeviceModule.messages', 'Отмена'),
            $this->createUrl('/device/backend/synonym/index'),
            ['class' => 'btn']
        );?>
    </div>
    <?php $this->endWidget(); ?>
</div>