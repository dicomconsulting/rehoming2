<?php
/* @var $this SynonymController */
/* @var $model DeviceParameterSynonym */
/* @var $data DeviceParameterSynonym */
?>
<?php $this->widget('GridView', array(
    'id' => 'synonym-grid',
    'dataProvider' => $model->search(),
    'nullDisplay' => '---',
    'columns' => array(
        [
            "header" => Yii::t("DeviceModule.messages", "№"),
            "value" => '$data->uid',
        ],
        [
            "header" => Yii::t("DeviceModule.messages", "Параметр"),
            'type' => 'html',
            "value" => '$data->parameter->name . "<br/>(" . $data->parameter->csv_name . ")"',
        ],
        [
            "header" => Yii::t("DeviceModule.messages", "Синоним"),
            "value" => '$data->csv_synonym',
        ],
        [
            "header" => Yii::t("DeviceModule.messages", "Модель"),
            "value" => '$data->device_model_name',
        ],
        [
            "header" => Yii::t("DeviceModule.messages", "Статус"),
            'type' => 'raw',
            'htmlOptions' => ['style' => 'text-align: center;'],
            "value" => '$data->getState()->getName()',
        ],
        [
            "header" => Yii::t("DeviceModule.messages", "Последняя рассинхронизация данных"),
            'type' => 'raw',
            'htmlOptions' => ['style' => 'text-align: center;'],
            "value" => function ($data, $row) {
                $date = $data->start_conflict_time;
                $rowValue = null;
                if ($date instanceof RhDateTime) {
                    $rowValue = $date->format('d ') . $date->getMonthName() . $date->format(' Y H:i:s');
                }
                return $rowValue;
            }
        ],
        array(
            'class'=>'CButtonColumn',
            'header' => CHtml::dropDownList(
                'pageSize',
                Yii::app()->user->getState('pageSize'),
                [10 => 10, 20 => 20, 50 => 50],
                [
                    'onchange' => "$.fn.yiiGridView.update('synonym-grid',{ data:{pageSize: $(this).val() }})",
                    'style' => 'width: 70px;'
                ]
            ),
            'buttons' => [
                'update' => [
                    'visible' => '$data->isEditable();'
                ],
                'delete' => [
                   'visible' => '$data->isEditable();'
                ]
            ]
        ),
    ),
));
