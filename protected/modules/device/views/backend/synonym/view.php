<?php
/* @var $model DeviceParameterSynonym */
?>

    <h2><?=Yii::t("DeviceModule.messages", "Синоним")?> №<?php echo $model->uid;?></h2>
    <?=CHtml::link(
        CHtml::tag('i', ['class' => 'icon-list'], ' ') . Yii::t("DeviceModule.messages", "Вернуться к списку"),
        $this->createUrl('/device/backend/synonym/index'),
        ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 10px;']
    )?>
    <?=$model->isEditable() ? CHtml::link(
        CHtml::tag('i', ['class' => 'icon-edit'], ' ') . Yii::t("DeviceModule.messages", "Редактировать"),
        $this->createUrl('/device/backend/synonym/update/', ['id' => $model->uid]),
        ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 10px;']
    ) : '';?>
    <?=$model->isEditable() ? CHtml::link(
        CHtml::tag('i', ['class' => 'icon-trash'], ' ') . Yii::t("DeviceModule.messages", "Удалить"),
        $this->createUrl('/device/backend/synonym/delete/', ['id' => $model->uid]),
        ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 10px;']
    ) : '';?>
    <?php $this->widget('zii.widgets.CDetailView', [
        'data' => $model,
        'attributes' => [
            [
                "name" => Yii::t("DeviceModule.messages", "№"),
                "value" => $model->uid,
            ],
            [
                "name" => Yii::t("DeviceModule.messages", "Параметр"),
                "value" => $model->parameter->name . " (" . $model->parameter->csv_name . ")",
            ],
            [
                "name" => Yii::t("DeviceModule.messages", "Синоним"),
                "value" => $model->csv_synonym,
            ],
            [
                "name" => Yii::t("DeviceModule.messages", "Модель"),
                "value" => $model->deviceModel->name,
            ],
            [
                "name" => Yii::t("DeviceModule.messages", "Статус"),
                "value" => $model->getState()->getName(),
            ],
            [
                "name" => Yii::t("DeviceModule.messages", "Последняя рассинхронизация данных"),
                "value" => function ($model) {
                        $date = $model->start_conflict_time;
                        $rowValue = null;
                        if ($date instanceof RhDateTime) {
                            $rowValue = $date->format('d ') . $date->getMonthName() . $date->format(' Y H:i:s');
                        }
                        return $rowValue;
                    }
            ]
        ]
    ]);


