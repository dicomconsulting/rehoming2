<?php
Yii::import('device.models.DeviceParameter.I18NTransformation');
Yii::import('device.models.*');

class I18NValue implements ToStringInterface
{
    private $dbValue;

    private $viewValue;

    public function __construct($inputString)
    {
        $this->dbValue = $inputString;
    }

    /**
     * Initial value from DB
     *
     * @return string
     */
    public function getDbValue()
    {
        return $this->dbValue;
    }

    /**
     * Value after translate
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getViewValue();
    }

    /**
     * Value after translate
     *
     * @return string
     */
    public function getViewValue()
    {
        if (is_null($this->viewValue)) {
            $this->_prepareViewValue();
        }
        return $this->viewValue;
    }

    /**
     * @return string
     */
    public function toString()
    {
        return $this->getViewValue();
    }


    private function _prepareViewValue()
    {
        $this->viewValue = $this->dbValue;
        if (preg_match_all('/[a-zA-Z.\s]+/', $this->dbValue, $parts)) {
            $messageSource = I18NTransformation::getMessageSource();
            $search = [];
            $replace = [];
            foreach ($parts[0] as $part) {
                $message = trim($part, '. ');
                if (strlen($message)) {
                    $translateMessage = $messageSource->translate('dictionary', $message);
                    if ($translateMessage != $message) {
                        $search[] = $message;
                        $replace[] = $translateMessage;
                    }
                }
            }
            if (!empty($search)) {
                $this->viewValue = str_replace($search, $replace, $this->dbValue);
            }
        }
    }
}
