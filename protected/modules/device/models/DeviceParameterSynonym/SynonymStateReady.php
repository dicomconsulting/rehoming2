<?php

class SynonymStateReady extends SynonymStateBase
{
    const ID = 0;

    public function getNextStateId()
    {
        return SynonymStateChanged::ID;
    }

    public function getName()
    {
        return Yii::t('DeviceModule.messages', 'Подготовлен');
    }

    public function isEditable()
    {
        return false;
    }

    public function rules()
    {
        return [];
    }
}
