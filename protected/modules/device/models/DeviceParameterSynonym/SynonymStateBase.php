<?php
Yii::import('device.models.DeviceParameterSynonym');
Yii::import('device.models.DeviceParameterSynonym.*');
Yii::import('device.DeviceModule');

abstract class SynonymStateBase implements SynonymStateInterface
{
    private $context;

    private $frozen;

    /**
     * @param integer $stateId
     * @param DeviceParameterSynonym $context
     * @return SynonymStateBase
     */
    public static function createState($stateId, DeviceParameterSynonym $context)
    {
        $stateId = intval($stateId);
        if ($stateId == SynonymStateNew::ID) {
            $newState = new SynonymStateNew($context);
        } elseif ($stateId == SynonymStateChanged::ID) {
            $newState = new SynonymStateChanged($context);
        } else {
            $newState = new SynonymStateReady($context);
        }
        return $newState;
    }

    public function freeze()
    {
        $this->frozen = true;
    }

    public function unfreeze()
    {
        $this->frozen = false;
    }

    /**
     * @param DeviceParameterSynonym $context
     */
    public function __construct(DeviceParameterSynonym $context)
    {
        $this->setContext($context);
    }

    /**
     * @param DeviceParameterSynonym $context
     */
    public function setContext(DeviceParameterSynonym $context)
    {
        $this->context = $context;
    }

    /**
     * @return DeviceParameterSynonym
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['csv_synonym', 'checkEqualMeasureUnit'],
            ['csv_synonym', 'required']
        ];
    }

    /**
     * @return bool
     */
    public function isEditable()
    {
        return true;
    }

    /**
     * @return integer
     */
    public function getNextStateId()
    {
        return SynonymStateReady::ID;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return static::ID;
    }

    public function toggleState()
    {
        if (!$this->frozen) {
            $nextState = self::createState($this->getNextStateId(), $this->getContext());
            $this->getContext()->setState($nextState);
        }
    }

    abstract public function getName();
}
