<?php

class SynonymStateChanged extends SynonymStateBase
{
    const ID = 2;

    public function getNextStateId()
    {
        return SynonymStateReady::ID;
    }

    public function getName()
    {
        return Yii::t('DeviceModule.messages', 'Изменённый');
    }
}
