<?php

class SynonymStateNew extends SynonymStateBase
{
    const ID = 1;

    public function getNextStateId()
    {
        return SynonymStateReady::ID;
    }

    public function getName()
    {
        return Yii::t('DeviceModule.messages', 'Новый');
    }
}
