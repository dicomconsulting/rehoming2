<?php

interface SynonymStateInterface
{
    /**
     * @return boolean
     */
    public function isEditable();

    public function toggleState();
}
