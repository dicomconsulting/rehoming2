<?php

/**
 * This is the model class for table "{{ecg_image}}".
 *
 * The followings are the available columns in table '{{ecg_image}}':
 * @property integer $device_id
 * @property integer $episode_number
 * @property string $image_name
 */
class ECGImage extends CActiveRecord
{
    const FILE_EXTENSION = 'png';

    const BASE_DIR_ALIAS = 'application.documents.ecg';

    private $imageResource;

    public function createImage($content)
    {
        if ($this->getIsNewRecord()) {
            $this->imageResource = imagecreatefromstring($content);
        } else {
            throw new CException('You can create an image from the string only for the new entity.');
        }
    }

    public function getRealImagePath()
    {
        return Yii::getPathOfAlias(self::BASE_DIR_ALIAS) . DIRECTORY_SEPARATOR . $this->image_name;
    }

    public function send()
    {
        header("Content-type: image/png");
        imagepng(imagecreatefrompng($this->getRealImagePath()));
    }

    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->getIsNewRecord()) {
                imagepng($this->imageResource, $this->_createImagePath());
                $this->image_name = $this->_createImageName();
            }
            return true;
        } else {
            return false;
        }
    }


    private function _createImageName()
    {
        return $this->device_id . '-' . $this->episode_number . '.' . self::FILE_EXTENSION;
    }

    private function _createImagePath()
    {
        return Yii::getPathOfAlias(self::BASE_DIR_ALIAS) . DIRECTORY_SEPARATOR . $this->_createImageName();
    }


    /**
     * The associated database table name.
     *
     * @return string
     */
    public function tableName()
    {
        return '{{ecg_image}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param  string  $className active record class name.
     * @return Patient the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function primaryKey()
    {
        return ['device-id', 'episode_number'];
    }
}
