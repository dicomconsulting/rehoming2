<?php
Yii::import('i18n.models.I18nActiveRecord');
Yii::import('device.models.DeviceParameter.*');
Yii::import('directory.models.MeasurementUnit');
Yii::import('device.components.*');
/**
 * This is the model class for table "{{parameter}}".
 *
 * The followings are the available columns in table '{{parameter}}':
 * @property integer $uid
 * @property string $csv_name
 * @property string $system_name
 * @property integer $measurement_unit_id
 * @property string $name
 * @property integer $type
 * @property MeasurementUnit $unit
 */
class DeviceParameter extends I18nActiveRecord implements AppDataTransformationInterface, CsvDataTransformationInterface
{
    /**
     * When receiving the value from CSV,
     * all placeholders must be replaced by null.
     */
    const TYPE_NORMALIZE_NULL = 1;

    /**
     * The value of this parameter should be processed as a date.
     */
    const TYPE_DATE = 2;

    /**
     * The value of this parameter should be processed as a date and time.
     */
    const TYPE_DATETIME = 4;

    /**
     * At this parameter should be available calculation
     * of the mean, minimum and maximum values.
     */
    const TYPE_STATISTIC = 8;

    /**
     * At this parameter should be processed as time interval
     */
    const TYPE_TIME_INTERVAL = 16;

    /**
     * At this parameter should be processed i18n
     */
    const TYPE_I18N = 32;

    /**
     * Current assign value from device state.
     *
     * @var DeviceValue
     */
    private $currentValue;

    /**
     * Handlers are applied to the values when picked of CSV.
     *
     * Each object must implement CsvDataTransformationInterface.
     * @var CsvDataTransformationInterface[]
     */
    private $csvDataStrategies = [];

    /**
     * Handlers are applied to the values when loading from the DB and saved in the DB.
     *
     * Each object must implement AppDataTransformationInterface.
     * @var AppDataTransformationInterface[]
     */
    private $appDataStrategies = [];

    /**
     * Assign device value from device state.
     *
     * @param DeviceValue $currentValue
     */
    public function assignCurrentValue(DeviceValue $currentValue)
    {
        $this->currentValue = $currentValue;
    }

    /**
     * Getter for current assign device's value.
     *
     * @return DeviceValue
     */
    public function getCurrentValue()
    {
        return $this->currentValue;
    }

    /**
     * Performs the necessary transformation of the value of this parameter
     * when retrieving data from CSV.
     *
     * @param mixed $value The input value.
     * @return mixed The processed value.
     */
    public function transformationFromCsv($value)
    {
        foreach ($this->csvDataStrategies as $strategy) {
            $value = $strategy->transformationFromCsv($value);
        }
        return $value;
    }

    /**
     * Performs the necessary transformation of the value of this parameter
     * when loading data from DB.
     *
     * @param mixed $value The input value.
     * @return mixed The processed value.
     */
    public function transformationFromDb($value)
    {
        foreach ($this->appDataStrategies as $strategy) {
            $value = $strategy->transformationFromDb($value);
        }
        return $value;
    }

    /**
     * Performs the necessary transformation of the value of this parameter
     * when saving data to DB.
     *
     * @param mixed $value The input value.
     * @return mixed The processed value.
     */
    public function transformationToDb($value)
    {
        foreach ($this->appDataStrategies as $strategy) {
            $value = $strategy->transformationToDb($value);
        }
        return $value;
    }

    public function afterFind()
    {
        $this->_customizeBehavior();
        parent::afterFind();
    }

    /**
     * Verifies that the parameter refers to a type of "normalize null".
     *
     * @return boolean
     */
    public function isTypeNormalizeNull()
    {
        return $this->type & self::TYPE_NORMALIZE_NULL;
    }

    /**
     * Verifies that the parameter refers to a type of "date".
     *
     * @return boolean
     */
    public function isTypeDate()
    {
        return $this->type & self::TYPE_DATE;
    }

    /**
     * Verifies that the parameter refers to a type of "date & time".
     *
     * @return boolean
     */
    public function isTypeDateTime()
    {
        return $this->type & self::TYPE_DATETIME;
    }

    /**
     * Verifies that the parameter refers to a type of "statistic".
     *
     * @return boolean
     */
    public function isTypeStatistic()
    {
        return $this->type & self::TYPE_STATISTIC;
    }

    /**
     * Verifies that the parameter refers to a type of "time interval".
     *
     * @return boolean
     */
    public function isTypeTimeInterval()
    {
        return $this->type & self::TYPE_TIME_INTERVAL;
    }

    /**
     * Verifies that the parameter refers to a type of "i18n".
     *
     * @return boolean
     */
    public function isTypeI18N()
    {
        return $this->type & self::TYPE_I18N;
    }

    public function primaryKey()
    {
        return 'uid';
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{parameter}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param  string $className active record class name.
     * @return DeviceParameter  the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function relations()
    {
        return array(
            'unit' => array(self::BELONGS_TO, 'MeasurementUnit', 'measurement_unit_id')
        );
    }

    public function __toString()
    {
        return $this->name;
    }

    private function _customizeBehavior()
    {
        if ($this->isTypeNormalizeNull()) {
            $this->csvDataStrategies[] = new NormalizeNullTransformation();
        }

        if ($this->isTypeDate()) {
            $dateTransformation = new DateTransformation();
            $this->csvDataStrategies[] = $dateTransformation;
            $this->appDataStrategies[] = $dateTransformation;
        }

        if ($this->isTypeDateTime()) {
            $dateTimeTransformation = new DateTimeTransformation();
            $this->csvDataStrategies[] = $dateTimeTransformation;
            $this->appDataStrategies[] = $dateTimeTransformation;
        }

        if ($this->isTypeStatistic()) {
            $this->attachBehavior('StatisticBehavior', new StatisticDeviceParameterBehavior());
        }

        if ($this->isTypeTimeInterval()) {
            $timeIntervalTransformation = new TimeIntervalTransformation();
            $this->csvDataStrategies[] = $timeIntervalTransformation;
            $this->appDataStrategies[] = $timeIntervalTransformation;
        }

        if ($this->isTypeI18N()) {
            $i18nTransformation = new I18NTransformation();
            $this->csvDataStrategies[] = $i18nTransformation;
            $this->appDataStrategies[] = $i18nTransformation;
        }
    }
}
