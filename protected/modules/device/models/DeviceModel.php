<?php
Yii::import('device.models.DeviceParameter');
Yii::import('admin.components.DeactivationDataTrait');

/**
 * This is the model class for table "{{device_model}}".
 *
 * The followings are the available columns in table '{{device_model}}':
 * @property string $name
 * @property integer $device_type_uid
 * @property DeviceParameter[] $parameters
 * @property DeviceType[] $type
 */
class DeviceModel extends CActiveRecord
{
    use DeactivationDataTrait;

    private $parameterSynonyms;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{device_model}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('device_type_uid', 'numerical', 'integerOnly' => true),
            array('device_type_uid', 'required'),
            array('name', 'length', 'max'=>100),
            array('name', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'parameters'=>array(
                self::MANY_MANY,
                'DeviceParameter',
                '{{parameter_model_device_relation}}(device_model_name, parameter_id)',
                'index' => 'system_name'
            ),
            'type'=>array(
                self::BELONGS_TO,
                'DeviceType',
                'device_type_uid',
            ),
        );
    }

    public function behaviors()
    {
        return array( 'CAdvancedArBehavior' => array(
            'class' => 'application.extensions.CAdvancedArBehavior')
        );
    }


    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'name' => Yii::t("DeviceModule.messages", "Название"),
            'device_type_uid' => Yii::t("DeviceModule.messages", "Тип устройства"),
        );
    }

    /**
     * @param DeviceParameter $parameter
     * @return false|string
     */
    public function getParameterSynonym(DeviceParameter $parameter)
    {
        return array_key_exists($parameter->uid, $this->_getParameterSynonyms()) ? $this->_getParameterSynonyms()[$parameter->uid] : false;
    }

    private function _getParameterSynonyms()
    {
        if (!is_array($this->parameterSynonyms)) {
            $command = Yii::app()->db->createCommand();
            $command->select(['parameter_id', 'csv_synonym'])
                ->from('{{parameter_synonym}}')
                ->where(
                    "device_model_name = :device_model_name AND state_id = :state_id",
                    [":device_model_name" => $this->name, ":state_id" => SynonymStateReady::ID]);
            $rows = $command->queryAll();
            $this->parameterSynonyms = [];
            foreach ($rows as $row) {
                $this->parameterSynonyms[$row['parameter_id']] = $row['csv_synonym'];
            }
        }
        return $this->parameterSynonyms;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return DeviceModel the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function primaryKey()
    {
        return 'name';
    }

    public function search()
    {
        return new CActiveDataProvider(
            $this,
            array(
                'criteria' => new CDbCriteria(['order' => 'name ASC']),
                'pagination'=> [
                    'pageSize' => Yii::app()->user->getState('pageSize')
                ]
            )
        );
    }
}
