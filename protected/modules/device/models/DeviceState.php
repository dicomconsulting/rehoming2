<?php

/**
 * This is the model class for table "{{device_state}}".
 *
 * The followings are the available columns in table '{{device_state}}':
 * @property integer $uid
 * @property integer $device_id
 * @property RhDateTime $create_time
 * @property RhDateTime $last_follow_up
 * @property DeviceValue[] $values
 * @property Device $device
 */
class DeviceState extends CActiveRecord
{
    /**
     * Assign value to parameter.
     *
     * @param DeviceParameter $parameter
     * @return DeviceValue
     */
    public function assignValue(DeviceParameter $parameter)
    {
        if ($this->existValue($parameter)) {
            $parameter->assignCurrentValue($this->values[$parameter->uid]);
        } else {
            $this->_assignNullDeviceValue($parameter);
        }
        return ;
    }

    /**
     * Checking the existence of a value for the parameter.
     *
     * @param DeviceParameter $parameter
     * @return boolean
     */
    public function existValue($parameter)
    {
        $values = $this->values;
        return (isset($values[$parameter->uid]) && $values[$parameter->uid] instanceof DeviceValue);
    }

    public function beforeSave()
    {
        $transformerArray = DeviceStateTransformer::findAllByDeviceId($this->device_id);
        foreach ($transformerArray as $transformer) {
            if (!isset($this->values[$transformer->parameter_system_name])) {
                $this->_reindexValueArrayByParameterSystemName();
            }
            $transformer->transform($this);
        }
        return parent::beforeSave();
    }


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{device_state}}';
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'values' => array(self::HAS_MANY, 'DeviceValue', 'device_state_id', 'index' => 'parameter_id'),
            'device' => array(self::BELONGS_TO, 'Device', 'device_id'),
        );
    }

    protected function afterSave()
    {
        parent::afterSave();

//        $processor = new OptionsProcessor($this->device->patient);
//        $processor->process();
    }


    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return DeviceState the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function primaryKey()
    {
        return 'uid';
    }

    public function behaviors()
    {
        return [
            'RhDateTimeBehavior' => [
                'class' => 'application.extensions.RhDateTimeBehavior'
            ],
            'EAdvancedArBehavior' => [
                'class' => 'application.extensions.EAdvancedArBehavior'
            ]
        ];
    }

    /**
     * Return start time of current value period.
     *
     * @param DeviceParameter $parameter
     * @param bool $uniqueValue
     * @return RhDateTime
     */
    public function getStartTimeOfCurrentValuePeriod(DeviceParameter $parameter, $uniqueValue = false)
    {
        $sql = 'SELECT MAX(tmp.create_time) as max, MIN(tmp.create_time) as min, tmp.value FROM (
            SELECT dv.value, ds.create_time  FROM {{device_value}} as dv INNER JOIN {{device_state}} as ds
            ON ds.uid = dv.device_state_id AND ds.device_id = :device_id
                AND dv.parameter_id = (SELECT p.uid FROM {{parameter}} as p WHERE p.system_name = :system_name)
                AND ds.create_time BETWEEN (
                    SELECT MIN(ds_alias.create_time) FROM {{device_value}} as dv_alias INNER JOIN {{device_state}} as ds_alias
                    ON ds_alias.uid = dv_alias.device_state_id AND ds_alias.device_id = :device_id
                        AND dv_alias.value = :value GROUP BY dv_alias.value)
                AND  :create_time
            ) as tmp GROUP BY tmp.value ORDER BY max';
        $command = Yii::app()->db->createCommand($sql);
        $value = $parameter->transformationToDb($parameter->getCurrentValue()->value);
        $command->bindValue(":device_id", $this->device_id, PDO::PARAM_INT);
        $command->bindValue(":system_name", $parameter->system_name, PDO::PARAM_STR);
        $command->bindValue(":value", $value, PDO::PARAM_STR);
        $command->bindValue(":create_time", $this->create_time->format('Y-m-d H:i:s'), PDO::PARAM_STR);
        $rows = $command->queryAll();

        if (count($rows) > 1) {
            $timeString = current($rows)['max'];
        } else {
            $timeString = current($rows)['min'];
        }

        if ($uniqueValue) {
            foreach ($rows as $row) {
                if ($row['value'] == $value) {
                    $timeString = $row['min'];
                    break;
                }
            }
        }
        return new RhDateTime($timeString);
    }

    /**
     * Возвращает состояние устройства по дате
     *
     * @param RhDateTime $dateTime
     * @param Device $device
     * @return DeviceState
     */
    public static function getStateByDate(RhDateTime $dateTime, Device $device)
    {
        $criteria = new CDbCriteria();
        $criteria->condition = "device_id = :device_id AND create_time = :create_time";
        $criteria->params = array(":device_id" => $device->uid, ":create_time" => $dateTime->format('Y-m-d H:i:s'));
        return self::model()->find($criteria);
    }

    /**
     * Возвращает самое первое состояние устройства
     *
     * @param Device $device
     * @return CActiveRecord
     */
    public static function getFirstStateByDevice(Device $device)
    {
        $criteria = new CDbCriteria();
        $criteria->condition = "device_id = :device_id";
        $criteria->params = array(":device_id" => $device->uid);
        $criteria->order = "create_time ASC";
        return self::model()->find($criteria);
    }

    /**
     * Возвращает самое последнее состояние устройства
     *
     * @param Device $device
     * @return CActiveRecord
     */
    public static function getLastStateByDevice(Device $device)
    {
        $criteria = new CDbCriteria();
        $criteria->condition = "device_id = :device_id";
        $criteria->params = array(":device_id" => $device->uid);
        $criteria->order = "create_time DESC";
        return self::model()->find($criteria);
    }

    private function _reindexValueArrayByParameterSystemName()
    {
        $valueArray = $this->values;
        $reindexValueArray = [];
        foreach ($valueArray as $value) {
            $reindexValueArray[$value->parameter->system_name] = $value;
        }
        $this->values = $reindexValueArray;
    }

    private function _assignNullDeviceValue(DeviceParameter $parameter)
    {
        $nullDeviceValue = new DeviceValue();
        $nullDeviceValue->value = null;
        $nullDeviceValue->device_state_id = $this->uid;
        $nullDeviceValue->parameter_id = $parameter->uid;
        $parameter->assignCurrentValue($nullDeviceValue);
    }

    public static function getStateDateByUid($stateUid)
    {
        return self::model()->findByPk($stateUid)->create_time;
    }
}
