<?php
Yii::import('device.models.*');

class NullNotUniqueByDevice
{
    /**
     * @param string $parameterSystemName
     * @param DeviceState $deviceState
     * @return DeviceValue
     */
    public function normalize($parameterSystemName, DeviceState $deviceState)
    {
        $deviceValue = $deviceState->values[$parameterSystemName];
        if (!DeviceValue::isUniqueByDevice($deviceValue->value, $parameterSystemName, $deviceState->device_id)) {
            $deviceValue->value = null;
        }
        return $deviceValue;
    }
}
