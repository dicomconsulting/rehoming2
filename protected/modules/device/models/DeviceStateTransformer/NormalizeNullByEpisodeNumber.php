<?php
Yii::import('device.models.*');

class NormalizeNullByEpisodeNumber
{
    const EPISODE_NUMBER = 'episode_number';

    /**
     * @param string $parameterSystemName
     * @param DeviceState $deviceState
     * @return DeviceValue
     */
    public function normalize($parameterSystemName, DeviceState $deviceState)
    {
        $valueForProcessing = $deviceState->values[$parameterSystemName];
        $episodeNumberValue = $deviceState->values[self::EPISODE_NUMBER];
        if (is_null($episodeNumberValue->value)
            || !DeviceValue::isUniqueByDevice($episodeNumberValue->value, self::EPISODE_NUMBER, $deviceState->device_id)) {
            $valueForProcessing->value = null;
        }
        return $valueForProcessing;
    }
}
