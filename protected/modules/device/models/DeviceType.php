<?php
Yii::import('i18n.models.I18nActiveRecord');
Yii::import('admin.components.DeactivationDataTrait');

/**
 * This is the model class for table "{{device_type}}".
 *
 * The followings are the available columns in table '{{device_type}}':
 * @property integer $uid
 *
 * The followings are the available model relations:
 */
class DeviceType extends I18nActiveRecord
{
    use DeactivationDataTrait;
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return DeviceType the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function primaryKey()
    {
        return 'uid';
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{device_type}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('name, name_full', 'safe'),
            array('uid', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid' => 'Uid',
            'name' => Yii::t("DeviceModule.messages", "Название"),
            'name_full' => Yii::t("DeviceModule.messages", "Полное название"),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('uid', $this->uid);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}
