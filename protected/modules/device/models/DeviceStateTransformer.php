<?php
Yii::import('device.models.*');
Yii::import('device.models.DeviceStateTransformer.*');

/**
 * This is the model class for table "{{device_state_transformer}}".
 *
 * The followings are the available columns in table '{{device_state_transformer}}':
 * @property integer $uid
 * @property string $device_model
 * @property string $parameter_system_name
 * @property string $class_name
 * @property string $method_name
 */
class DeviceStateTransformer extends CActiveRecord
{

    /**
     * @param integer $deviceId
     * @return DeviceStateTransformer[]
     */
    public static function findAllByDeviceId($deviceId)
    {
        $device = Device::model()->findByPk($deviceId);
        return self::model()->findAllByAttributes(['device_model' => $device->device_model]);
    }

    /**
     * @param DeviceState $deviceState
     */
    public function transform(DeviceState $deviceState)
    {
        $values = $deviceState->values;
        $values[$this->parameter_system_name] =
            call_user_func_array($this->_createCallback(), [$this->parameter_system_name, $deviceState]);
        $deviceState->values = $values;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{device_state_transformer}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return DeviceStateTransformer the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function primaryKey()
    {
        return 'uid';
    }

    private function _createCallback()
    {
        if (!empty($this->class_name)) {
            $callback = [new $this->class_name(), $this->method_name];
        } else {
            $callback = $this->method_name;
        }
        return $callback;
    }
}
