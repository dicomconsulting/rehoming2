<?php
Yii::import('i18n.models.I18nActiveRecord');
Yii::import('device.models.DeviceParameterSynonym.*');

/**
 * This is the model class for table "{{parameter_synonym}}".
 *
 * The followings are the available columns in table '{{parameter_synonym}}':
 * @property integer $uid
 * @property integer $parameter_id
 * @property string $csv_synonym
 * @property string $device_model_name
 * @property integer $measurement_unit_id
 * @property integer $state_id
 * @property DateTime $start_conflict_time
 * @property DeviceParameter parameter
 * @property DeviceModel deviceModel
 */
class DeviceParameterSynonym extends CActiveRecord implements SynonymStateInterface
{
    private $state;

    /**
     * @param SynonymStateBase $state
     */
    public function setState(SynonymStateBase $state)
    {
        $this->state = $state;
        $this->state_id = $this->getState()->getId();
    }

    /**
     * @return SynonymStateBase
     */
    public function getState()
    {
        return $this->state;
    }

    public function rules()
    {
        return $this->getState()->rules();
    }

    public function isEditable()
    {
        return $this->getState()->isEditable();
    }

    public function toggleState()
    {
        $this->getState()->toggleState();
    }

    public function afterFind()
    {
        $this->setState(
            SynonymStateBase::createState($this->state_id, $this)
        );
        parent::afterFind();
    }

    protected function beforeSave()
    {
        if (!$this->getIsNewRecord()) {
            $this->toggleState();
        }
        return parent::beforeSave();
    }

    protected function afterSave()
    {
        parent::afterSave();
        $subject = 'Изменения конфигурации модели ' . $this->deviceModel->name;
        $username = Yii::app()->hasComponent('user') ? Yii::app()->user->name : 'auto';
        $message = '<ul>'
            . '<li>Параметр: ' . $this->parameter->name .' (' . $this->parameter->csv_name . ')' . '</li>'
            . '<li>Статус: ' . $this->state->getName() . '</li>'
            . '<li>Пользователь: ' . $username .'</li>'
            . '</ul>';
        Yii::app()->mailer->send($message, $subject);
    }


    public function behaviors()
    {
        return [
            'RhDateTimeBehavior' => [
                'class' => 'application.extensions.RhDateTimeBehavior'
            ],
            'EAdvancedArBehavior' => [
                'class' => 'application.extensions.EAdvancedArBehavior'
            ]
        ];
    }

    public function relations()
    {
        return [
            'parameter' => [self::BELONGS_TO, 'DeviceParameter', 'parameter_id'],
            'deviceModel' => [self::BELONGS_TO, 'DeviceModel', 'device_model_name'],
        ];
    }

    public function search()
    {
        return new CActiveDataProvider(
            $this,
            array(
                'criteria' => new CDbCriteria,
                'pagination'=> [
                    'pageSize' => Yii::app()->user->getState('pageSize')
                ]
            )
        );
    }

    public function primaryKey()
    {
        return 'uid';
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{parameter_synonym}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param  string $className active record class name.
     * @return DeviceParameterSynonym  the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function attributeLabels()
    {
        return array(
            'uid' => Yii::t("DeviceModule.messages", 'Номер'),
            'parameter_id' => Yii::t("DeviceModule.messages", 'Параметр'),
            'csv_synonym' => Yii::t("DeviceModule.messages", 'Синоним'),
            'device_model_name' => Yii::t("DeviceModule.messages", 'Модель'),
            'start_conflict_time' => Yii::t("DeviceModule.messages", 'Последняя рассинхронизация данных')
        );
    }

    public function checkEqualMeasureUnit()
    {
        $parts = explode('[', $this->csv_synonym);
        $equalUnits = true;
        if (isset($parts[1])) {
            $measurementUnitEnName = preg_replace('/\].*/s', '', $parts[1]);
            $measurementUnit = MeasurementUnit::model()->withI18n('en')->find('name = :name', [':name' => $measurementUnitEnName]);
            if ($measurementUnit && $this->parameter->measurement_unit_id && $this->parameter->measurement_unit_id != $measurementUnit->uid) {
                $equalUnits = false;
            }
        } elseif ($this->parameter->measurement_unit_id) {
            $equalUnits = false;
        }
        if (!$equalUnits) {
            $this->addError(
                'csv_synonym',
                Yii::t(
                    'DeviceModule.messages',
                    'Единицы измерения параметров различны'
                )
            );
        }
    }
}
