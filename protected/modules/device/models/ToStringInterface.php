<?php

interface ToStringInterface
{
    /**
     * @return string
     */
    public function toString();
}
