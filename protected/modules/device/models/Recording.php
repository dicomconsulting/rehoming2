<?php

/**
 * This is the model class for table "{{vw_recording_state}}".
 *
 * The followings are the available columns in table '{{vw_recording_state}}':
 * @property integer $state_uid
 * @property integer $device_id
 * @property RhDateTime $last_follow_up
 * @property RhDateTime $state_create_time
 * @property string $episode_number
 * @property string $episode_type
 * @property RhDateTime $detection
 * @property string $episode_duration
 * @property string $mean_pp_at_initial_detection
 * @property string $mean_rr_at_initial_detection
 * @property string $onset
 * @property string $stability
 * @property string $redetection
 * @property string $atp_one_shot_delivered
 * @property string $atp_in_vt_vf_delivered
 * @property string $episode_shocks_delivered
 * @property string $episode_shocks_aborted
 * @property string $maximum_energy
 * @property string $mean_pp_at_termination
 * @property string $mean_rr_at_termination
 * @property string $remark_detection
 * @property string $remark_therapy
 * @property string $remark_termination
 * @property Device $device
 * @property ECGImage $ecg
 * @property RhDateTime $termination
 * @property integer $device_settings_no
 */

class Recording extends CActiveRecord
{

    protected $dateColumns = ['detection', 'termination'];

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Recording the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{vw_recording_state}}';
    }

    public function primaryKey()
    {
        return 'state_uid';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('state_uid, device_id', 'numerical', 'integerOnly' => true),
            array(
                'episode_number, episode_type, detection, episode_duration, mean_pp_at_initial_detection, mean_rr_at_initial_detection, onset, stability, redetection, atp_one_shot_delivered, episode_shocks_delivered, episode_shocks_aborted, maximum_energy, mean_pp_at_termination, mean_rr_at_termination, remark_detection, remark_therapy, remark_termination, atp_in_vt_vf_delivered',
                'length',
                'max' => 100
            ),
            array('last_follow_up, state_create_time', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array(
                'state_uid, device_id, last_follow_up, state_create_time, episode_number, episode_type, detection, episode_duration, mean_pp_at_initial_detection, mean_rr_at_initial_detection, onset, stability, redetection, atp_one_shot_delivered, episode_shocks_delivered, episode_shocks_aborted, maximum_energy, mean_pp_at_termination, mean_rr_at_termination, remark_detection, remark_therapy, remark_termination, atp_in_vt_vf_delivered',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'device' => array(self::BELONGS_TO, 'Device', 'device_id'),
            'ecg' => array(self::HAS_ONE, 'ECGImage', ['device_id' => 'device_id', 'episode_number' => 'episode_number'])
        );
    }

    public function existECGImage()
    {
        return $this->ecg instanceof ECGImage;
    }

    public static function getIdentificationWithoutECG()
    {
        $sql = 'SELECT vwrs.`device_id`, vwrs.`episode_number` FROM {{vw_recording_state}} as vwrs'
            . ' LEFT JOIN {{ecg_image}} as ei ON ei.device_id = vwrs.device_id AND ei.episode_number = vwrs.episode_number'
            . ' WHERE ei.`image_name` IS NULL  AND vwrs.`episode_number` > 0 GROUP BY vwrs.`device_id`, vwrs.`episode_number`';
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryAll();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'state_uid' => 'State Uid',
            'device_id' => 'Device',
            'last_follow_up' => 'Last Follow Up',
            'state_create_time' => 'State Create Time',
            'episode_number' => 'Episode Number',
            'episode_type' => 'Episode Type',
            'detection' => 'Detection',
            'episode_duration' => 'Episode Duration',
            'mean_pp_at_initial_detection' => 'Mean Pp At Initial Detection',
            'mean_rr_at_initial_detection' => 'Mean Rr At Initial Detection',
            'onset' => 'Onset',
            'stability' => 'Stability',
            'redetection' => 'Redetection',
            'atp_one_shot_delivered' => 'Atp One Shot Delivered',
            'episode_shocks_delivered' => 'Episode Shocks Delivered',
            'episode_shocks_aborted' => 'Episode Shocks Aborted',
            'maximum_energy' => 'Maximum Energy',
            'mean_pp_at_termination' => 'Mean Pp At Termination',
            'mean_rr_at_termination' => 'Mean Rr At Termination',
            'remark_detection' => 'Remark Detection',
            'remark_therapy' => 'Remark Therapy',
            'remark_termination' => 'Remark Termination',
            'termination' => 'Termination',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('state_uid', $this->state_uid);
        $criteria->compare('device_id', $this->device_id);
        $criteria->compare('last_follow_up', $this->last_follow_up, true);
        $criteria->compare('state_create_time', $this->state_create_time, true);
        $criteria->compare('episode_number', $this->episode_number, true);
        $criteria->compare('episode_type', $this->episode_type, true);
        $criteria->compare('detection', $this->detection, true);
        $criteria->compare('episode_duration', $this->episode_duration, true);
        $criteria->compare('mean_pp_at_initial_detection', $this->mean_pp_at_initial_detection, true);
        $criteria->compare('mean_rr_at_initial_detection', $this->mean_rr_at_initial_detection, true);
        $criteria->compare('onset', $this->onset, true);
        $criteria->compare('stability', $this->stability, true);
        $criteria->compare('redetection', $this->redetection, true);
        $criteria->compare('atp_one_shot_delivered', $this->atp_one_shot_delivered, true);
        $criteria->compare('episode_shocks_delivered', $this->episode_shocks_delivered, true);
        $criteria->compare('episode_shocks_aborted', $this->episode_shocks_aborted, true);
        $criteria->compare('maximum_energy', $this->maximum_energy, true);
        $criteria->compare('mean_pp_at_termination', $this->mean_pp_at_termination, true);
        $criteria->compare('mean_rr_at_termination', $this->mean_rr_at_termination, true);
        $criteria->compare('remark_detection', $this->remark_detection, true);
        $criteria->compare('remark_therapy', $this->remark_therapy, true);
        $criteria->compare('remark_termination', $this->remark_termination, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function behaviors()
    {
        return [
            'RhDateTimeBehavior' => [
                'class' => 'application.extensions.RhDateTimeBehavior'
            ],
            'EAdvancedArBehavior' => [
                'class' => 'application.extensions.EAdvancedArBehavior'
            ]
        ];
    }

    protected function afterFind()
    {
        parent::afterFind();

        foreach ($this->dateColumns as $column) {
            if ($this->$column) {
                $this->$column = new RhDateTime($this->$column);
            }
        }
    }

    public function isFollowUp()
    {
        return $this->episode_type == "Follow Up";
    }
}
