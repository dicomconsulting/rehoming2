<?php

/**
 * Class Transmitter
 * @property integer $uid
 * @property string $name
 */
class Transmitter extends CActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{transmitter}}';
    }


    public static function getList()
    {
        $rows = self::model()->findAll();
        $res = [];

        /** @var $row Transmitter */
        foreach ($rows as $row) {
            $res[] = array("uid" => $row->uid, "name" => $row->name);
        }

        return $res;
    }

    /**
     * @param string $className
     * @return Transmitter
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Returns the list of attribute names of the model.
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        return array("uid", "name");
    }
}