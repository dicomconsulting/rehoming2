<?php

class DeviceParameterI18n extends CActiveRecord
{
    /**
    * @return string the associated database table name
    */
    public function tableName()
    {
        return '{{parameter_i18n}}';
    }
    /**
    *@return array primary key column
    */
    public function primaryKey()
    {
        return array('uid','locale');
    }
}
