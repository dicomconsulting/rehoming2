<?php

interface AppDataTransformationInterface
{
    public function transformationFromDb($value);

    public function transformationToDb($value);
}
