<?php

class NormalizeNullTransformation implements CsvDataTransformationInterface
{
    /**
     * The placeholder is used in CSV to indicate that value is NULL.
     */
    const NULL_PLACEHOLDER = '---';

    public function transformationFromCsv($value)
    {
        $value = preg_replace('/^\(not for Lumax.*/', self::NULL_PLACEHOLDER, $value);
        return ($value == self::NULL_PLACEHOLDER ? null : $value);
    }
}
