<?php

class DateTransformation extends AbstractDateTransformation
{
    public function getOutputStringFormat()
    {
        return 'Y-m-d';
    }
}
