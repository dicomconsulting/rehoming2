<?php
Yii::import('device.models.I18NValue');

class I18NTransformation implements AppDataTransformationInterface, CsvDataTransformationInterface
{
    private static $messageSource;

    /**
     * @return CPhpMessageSource
     */
    public static function getMessageSource()
    {
        if (empty(self::$messageSource)) {
            self::$messageSource = new CPhpMessageSource();
            self::$messageSource->basePath = __DIR__ . DIRECTORY_SEPARATOR . '..'
                . DIRECTORY_SEPARATOR . '..'
                . DIRECTORY_SEPARATOR . 'messages';
            self::$messageSource->forceTranslation = true;
        }
        return self::$messageSource;
    }

    public function transformationFromDb($value)
    {
        return new I18NValue($value);
    }

    public function transformationToDb($value)
    {
        return $value->getDbValue();
    }

    public function transformationFromCsv($value)
    {
        return new I18NValue($value);
    }
}
