<?php

abstract class AbstractDateTransformation implements CsvDataTransformationInterface, AppDataTransformationInterface
{

    /**
     * Must return output string format.
     */
    abstract public function getOutputStringFormat();

    public function transformationFromDb($value)
    {
        if (!empty($value)) {
            $value = new RhDateTime($value);
        }
        return $value;
    }

    public function transformationToDb($value)
    {
        if ($value instanceof RhDateTime) {
            $value = $value->format($this->getOutputStringFormat());
        }
        return $value;
    }

    public function transformationFromCsv($value)
    {
        if (!empty($value)) {
            $value = new RhDateTime($value);
        }
        return $value;
    }
}
