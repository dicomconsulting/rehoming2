<?php

interface CsvDataTransformationInterface
{
    public function transformationFromCsv($value);
}
