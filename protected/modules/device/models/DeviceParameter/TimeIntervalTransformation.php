<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 29.10.13
 * Time: 16:27
 */

class TimeIntervalTransformation implements CsvDataTransformationInterface, AppDataTransformationInterface
{
    /**
     * Source data examples:
     * 57s
     * 4min 00s
     * 10h 45min 35s
     * 1d 5h 35min
     *
     * @param $value
     * @return int|null
     */
    public function transformationFromCsv($value)
    {
        if (!empty($value)) {
            preg_match("/((\d+)d )?((\d+)h )?((\d+)min\w?)?((\d+)s)?/i", $value, $matches);
            $intervalInText = "P0000-00-".sprintf("%1$02d", (!empty($matches[2]) ? $matches[2] : 0))."T" . sprintf("%1$02d:%2$02d:%3$02d", (!empty($matches[4]) ? $matches[4] : 0), (!empty($matches[6]) ? $matches[6] : 0), (!empty($matches[8]) ? $matches[8] : 0));

            return new DateInterval($intervalInText);
        } else {
            return null;
        }
    }

    public function transformationFromDb($value)
    {
        if (!empty($value)) {
            return DateIntervalHelper::createDateIntervalFromSeconds($value);
        } else {
            return null;
        }
    }

    public function transformationToDb($value)
    {
        if ($value instanceof DateInterval) {
            return DateIntervalHelper::toSeconds($value);
        } else {
            return null;
        }
    }
}