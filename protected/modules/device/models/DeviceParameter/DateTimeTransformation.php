<?php

class DateTimeTransformation extends AbstractDateTransformation
{
    public function getOutputStringFormat()
    {
        return 'Y-m-d H:i:s';
    }
}
