<?php

/**
 * This is the model class for table "{{device}}".
 *
 * The followings are the available columns in table '{{device}}':
 * @property integer $uid
 * @property string $serial_number
 * @property RhDateTime $implantation_date
 * @property string $device_model
 * @property integer $patient_id
 * @property string $transmitter_type
 * @property string $transmitter_serial_number
 * @property DeviceState[] $states
 * @property StatusMessage[] $status_message
 * @property DeviceState $lastState
 * @property DeviceModel $model
 * @property Patient $patient
 */
class Device extends CActiveRecord
{
    private $disableTabNames;

    private $cachedLastState;

    protected $stateUid;

    public $wasAssignStateValues = false;


    /**
     * Assigns to each device model's parameter corresponding value of the device state.
     * @param null|DeviceState $deviceState
     */
    public function assignStateValuesToDeviceModelParameters(DeviceState $deviceState = null)
    {
        $this->wasAssignStateValues = true;
        if (empty($deviceState)) {
            $deviceState = $this->lastState;
        }
        $model = $this->model;
        $parameters = $model->parameters;
        if ($parameters && !$deviceState) {
            throw new CException('Не установлен deviceState');
        }
        foreach ($parameters as $parameter) {
            $deviceState->assignValue($parameter);
        }

        $this->stateUid = $deviceState->uid;
    }

    /**
     * @return array Array of tab names.
     */
    public function getDisableTabNames()
    {
        if (!is_array($this->disableTabNames)) {
            $command = Yii::app()->db->createCommand();
            $command->select("tab_name")
                ->from('{{disable_tab}}')
                ->where("device_model_name = :device_model_name", [":device_model_name" => $this->device_model]);
            $rows = $command->queryAll();
            $this->disableTabNames = [];
            foreach ($rows as $row) {
                $this->disableTabNames[] = $row['tab_name'];
            }
        }

        return $this->disableTabNames;
    }

    /**
     * @param string $tabName
     * @return bool
     */
    public function isActiveTab($tabName)
    {
        return array_search($tabName, $this->getDisableTabNames()) === false;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{device}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('patient_id', 'numerical', 'integerOnly'=>true),
            array('serial_number, transmitter_serial_number', 'length', 'max'=>30),
            array('device_model, transmitter_type', 'length', 'max'=>100),
            array('implantation_date, active_since_date', 'safe'),
            array('uid, serial_number, implantation_date, device_model, patient_id, transmitter_type, transmitter_serial_number', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'patient' => array(self::BELONGS_TO, 'Patient', 'patient_id'),
            'model' => array(self::BELONGS_TO, 'DeviceModel', 'device_model'),
            'states' => array(self::HAS_MANY, 'DeviceState', 'device_id', 'index' => 'create_time'),
            'recordings' => array(self::HAS_MANY, 'Recording', 'device_id', 'index' => 'state_create_time'),
            'status_message' => array(self::HAS_MANY, 'StatusMessage', 'device_id'),
            'first_state_from_period' => array(self::HAS_ONE, 'DeviceState', 'device_id', 'condition' => 'last_follow_up = :follow_up_date', 'order' => 'create_time ASC'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'uid' => 'Uid',
            'serial_number' => 'Serial Number',
            'implantation_date' => 'Implantation Date',
            'device_model' => 'Device Model',
            'patient_id' => 'Patient',
            'transmitter_type' => 'Transmitter Type',
            'transmitter_serial_number' => 'Transmitter Serial Number',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('uid', $this->uid);
        $criteria->compare('serial_number', $this->serial_number, true);
        $criteria->compare('implantation_date', $this->implantation_date, true);
        $criteria->compare('device_model', $this->device_model, true);
        $criteria->compare('patient_id', $this->patient_id);
        $criteria->compare('transmitter_type', $this->transmitter_type, true);
        $criteria->compare('transmitter_serial_number', $this->transmitter_serial_number, true);

        return new CActiveDataProvider($this, ['criteria'=>$criteria]);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Device the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function primaryKey()
    {
        return 'uid';
    }

    public function behaviors()
    {
        return array(
            'RhDateTimeBehavior' => array(
                'class' => 'application.extensions.RhDateTimeBehavior'
            ),
            'EAdvancedArBehavior' => array(
                'class' => 'application.extensions.EAdvancedArBehavior'
            )
        );
    }

    public function getLatestStatusMessage ()
    {
        return Message::getLatestByDevice($this);
    }

    /**
     * Применяет самые первые значения параметров устройства.
     * Подразумевается что это данные на мемомент инплантации.
     *
     */
    public function assignFirstDeviceState()
    {
        /** @var DeviceState $state */
        $state = DeviceState::getFirstStateByDevice($this);
        
        if (!$state) {
            throw new CException('No device data where found.');
        }

        $this->assignStateValuesToDeviceModelParameters($state);
    }

    /**
     * @return DeviceState
     */
    public function getLastState()
    {
        if (!$this->cachedLastState) {
            $this->cachedLastState = DeviceState::getLastStateByDevice($this);
        }

        return $this->cachedLastState;
    }

    /**
     * @param RhDateTime $from
     * @param RhDateTime $to
     * @return DeviceState[]
     */
    public function getStatesByPeriod(RhDateTime $from, RhDateTime $to)
    {
        $format = "Y-m-d H:i:s";
        $criteria = new CDbCriteria();
        $criteria->condition = "(create_time BETWEEN :date_from AND :date_to) AND device_id = :device_id";
        $criteria->params = [
            ":date_from" => $from->format($format),
            ":date_to" => $to->format($format),
            ":device_id" => $this->uid
        ];

        return DeviceState::model()->findAll($criteria);
    }

    /**
     * Возвращает $n последних стейтов
     *
     * @param int $n
     * @return DeviceState[]
     */
    public function getStatesDownTo($n)
    {
        $criteria = new CDbCriteria();
        $criteria->condition = "device_id = :device_id";
        $criteria->params = [
            ":device_id" => $this->uid
        ];
        $criteria->order = "create_time DESC";
        $criteria->limit = $n;

        return DeviceState::model()->findAll($criteria);
    }

    /**
     * @return mixed
     */
    public function getStateUid()
    {
        return $this->stateUid;
    }
}
