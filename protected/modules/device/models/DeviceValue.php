<?php
Yii::import('device.models.*');

/**
 * This is the model class for table "{{device_value}}".
 *
 * The followings are the available columns in table '{{device_value}}':
 * @property integer $device_state_id
 * @property integer $parameter_id
 * @property string $value
 * @property DeviceParameter $parameter
 * @property DeviceState $deviceState
 */
class DeviceValue extends CActiveRecord
{

    public $maxCreateTime; // Необходимо для работы статистки и выборки максимальной даты

    const NULL_PLACEHOLDER = '---';

    public static function isUniqueByDevice($value, $parameterSystemName, $deviceId)
    {
        $criteria = new CDbCriteria([
            'alias'     => 'dv',
            'condition' => '`dv`.`value` = :value'
                . ' AND `ds`.`device_id` = :device_id'
                . ' AND `p`.`system_name` = :parameter_system_name',
            'join'      => 'INNER JOIN `{{device_state}}` as `ds` ON `ds`.`uid` = `dv`.`device_state_id`'
                . 'INNER JOIN `{{parameter}}` as `p` ON `p`.`uid` = `dv`.`parameter_id`',
            'params'    => [
                'value'                 => $value,
                'device_id'             => $deviceId,
                'parameter_system_name' => $parameterSystemName
            ]
        ]);
        return !self::model()->exists($criteria);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{device_value}}';
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'parameter'   => [self::BELONGS_TO, 'DeviceParameter', 'parameter_id'],
            'deviceState' => [self::BELONGS_TO, 'DeviceState', 'device_state_id']
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Device the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function primaryKey()
    {
        return array('device_state_id', 'parameter_id');
    }

    public function behaviors()
    {
        return [
            'EAdvancedArBehavior' => [
                'class' => 'application.extensions.EAdvancedArBehavior'
            ]
        ];
    }

    public function beforeSave()
    {
        $this->value = $this->parameter->transformationToDb($this->value);
        return parent::beforeSave();
    }

    public function afterFind()
    {
        $this->value = $this->parameter->transformationFromDb($this->value);
        parent::afterFind();
    }


    public function __toString()
    {
        if ($this->value instanceof ToStringInterface) {
            $returnValue = $this->value->toString();
        } elseif (!empty($this->value)) {
            $returnValue = $this->value;
        }
        if (empty($this->value) || is_null($returnValue)) {
            $returnValue = self::NULL_PLACEHOLDER;
        }
        return $returnValue;
    }
}
