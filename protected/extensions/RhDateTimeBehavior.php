<?php

/**
 * RhDateTimeBehavior
 *
 * Automatically converts date and datetime fields to RhDateTime object.
 */
class RhDateTimeBehavior extends CActiveRecordBehavior
{
    /**
     * @var string Db format for columns with date type.
     */
    public $dateDbFormat = 'Y-m-d';

    /**
     * @var string Db format for columns with datetime type.
     */
    public $datetimeDbFormat = 'Y-m-d H:i:s';

    /**
     * Converts RhDateTime object to string.
     *
     * Converts RhDateTime object to a string.
     * Uses the respective formats for date and datetime attributes.
     * If the column has the another type, converts to the default string format
     * for the current object, if this is set, otherwise to datetime format.
     *
     * @param CEvent $event
     * @return boolean
     */
    public function beforeSave($event)
    {
        foreach ($event->sender->tableSchema->columns as $columnName => $column) {
            $value = $event->sender->$columnName;
            if ($value instanceof RhDateTime) {
                if ($this->_isDateColumn($column)) {
                    $value = $value->format($this->dateDbFormat);
                } elseif ($this->_isDatetimeColumn($column)
                    || !$value->isSetDefaultOutputFormat()
                ) {
                    $value = $value->format($this->datetimeDbFormat);
                } else {
                    $value = $value->format();
                }
                $event->sender->$columnName = $value;
            }
        }
        return true;
    }

    /**
     * Converts a string to RhDateTime object.
     *
     * Converts a string to a RhDateTime object
     * and sets the default output format, use the default locale.
     * If the value is blank, then sets the null.
     *
     * @param CEvent $event
     * @return boolean
     */
    public function afterFind($event)
    {
        foreach ($event->sender->tableSchema->columns as $columnName => $column) {
            if ($this->_isDateColumn($column) || $this->_isDatetimeColumn($column)) {
                $value = $event->sender->$columnName;
                if (empty($value)) {
                    $event->sender->$columnName = null;
                } else {
                    if ($this->_isDateColumn($column)) {
                        $outputFormat = Yii::app()->getLocale()->getDateFormat();
                    } else {
                        $outputFormat = Yii::app()->getLocale()->getDateFormat()
                            . ' ' . Yii::app()->getLocale()->getTimeFormat();
                    }
                    $event->sender->$columnName = new RhDateTime(
                        $value,
                        new DateTimeZone(Yii::app()->getTimeZone())
                    );
                    $event->sender->$columnName->setDefaultOutputFormat($outputFormat);
                }
            }
        }
        return true;
    }

    private function _isDateColumn(CDbColumnSchema $column)
    {
        return $column->dbType == 'date';
    }

    private function _isDatetimeColumn(CDbColumnSchema $column)
    {
        return $column->dbType == 'datetime';
    }
}
