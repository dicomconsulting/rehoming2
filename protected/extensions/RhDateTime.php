<?php

class RhDateTime extends DateTime
{
    private $defaultOutputFormat;

    const RH_DATE = "d-m-Y";
    const RH_DATETIME = "d-m-Y H:i:s";

    const DB_DATE = 'Y-m-d';
    const DB_DATETIME = 'Y-m-d H:i:s';

    /**
     * A string representation of the value in the specified format.
     *
     * If no format is specified, the output will be produced
     * in the default format when you set it.
     * Otherwise an error.
     *
     * @param string $format
     * @return string
     */
    public function format($format = null)
    {
        if (empty($format) && $this->isSetDefaultOutputFormat()) {
            $format = $this->getDefaultOutputFormat();
        }

        return parent::format($format);
    }

    /**
     * Magic method for automatically converting an object into a string.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->format();
    }

    /**
     * Setter for default output format.
     *
     * @param string $defaultOutputFormat
     * @return \RhDateTime
     */
    public function setDefaultOutputFormat($defaultOutputFormat)
    {
        $this->defaultOutputFormat = strval($defaultOutputFormat);
        return $this;
    }

    /**
     * Getter for default output format.
     *
     * @return string
     */
    public function getDefaultOutputFormat()
    {
        return $this->defaultOutputFormat;
    }

    /**
     * Method to check whether the default output format is specified.
     *
     * @return boolean
     */
    public function isSetDefaultOutputFormat()
    {
        return !empty($this->getDefaultOutputFormat());
    }

    /**
     * Return month name in current locale.
     *
     * @param string $width
     * @param boolean $standAlone
     * @return string
     */
    public function getMonthName($width = 'wide', $standAlone = false)
    {
        $month = $this->format('n');
        return Yii::app()->getLocale()->getMonthNames($width, $standAlone)[$month];
    }
}
