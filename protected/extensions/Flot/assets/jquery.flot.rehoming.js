(function ($) {
    var options = {
        rehoming: {
            active: true
        }
    };

    function init(plot) {
        var rehoming = {
            x: -1,
            lastPointsNumber:[],
            loaded: false,
            series: [],
            legends: [],
            previousMouseX: -1
        };

        function onMouseOut(e) {
            if (rehoming.x != -1) {
                rehoming.x = -1;
            }
            plot.unhighlight();
            resetLegends();
        }

        function findNearestPoints(inputMouseX) {
            var i, j, pointSize, axisX,
                nearestPoints = [],
                maxDistance = plot.getOptions().grid.mouseActiveRadius,
                series = plot.getData();
            for (i = 0; i < series.length; i++) {
                var x,
                    axisX = series[i].xaxis,
                    points = series[i].datapoints.points,
                    mouseX = axisX.c2p(inputMouseX),
                    maxX = maxDistance / axisX.scale,
                    pointSize = series[i].datapoints.pointsize,
                    startPointNumber = 0,
                    endPointNumber = points.length;

                if (i in rehoming.lastPointsNumber) {
                    if (rehoming.previousMouseX <= mouseX) {
                        startPointNumber = rehoming.lastPointsNumber[i] * pointSize;
                    } else if((endPointNumber = (rehoming.lastPointsNumber[i] + 1) * pointSize) > points.length) {
                        endPointNumber = points.length;
                    }
                    rehoming.lastPointsNumber.splice(i, 1);
                }

                for (j = startPointNumber; j < endPointNumber; j += pointSize) {
                    x = points[j];

                    if (x == null || Math.abs(x - mouseX) > maxX) {
                        continue;
                    }

                    nearestPoints.push({
                        seriesNumber: i,
                        pointNumber: parseInt(j/pointSize),
                        timestamp: x,
                        pointValue: points[j+1]
                    });
                    rehoming.lastPointsNumber[i] = parseInt(j/pointSize);
                    break;
                }
            }
            rehoming.previousMouseX = mouseX;

            return nearestPoints.length > 0 ? nearestPoints : null;
        }

        function resetLegends() {
            for (var i = 0; i < rehoming.series.length; i++) {
                rehoming.legends.eq(i).text(rehoming.series[i].label);
            }
        }

        function cacheGraphData()
        {
            if (!rehoming.loaded) {
                rehoming.series  = plot.getData();
                rehoming.loaded = true;
            }
        }

        function leadingZero(inputValue)
        {
            inputValue = inputValue.toString();
            return inputValue.length < 2 ? '0' + inputValue : inputValue;
        }

        function formatDateTime(timestamp)
        {
            var date = new Date(timestamp);

            return leadingZero(date.getDate()) + '/' + leadingZero(date.getMonth() + 1) + '/' + date.getFullYear() + ' '
                + leadingZero(date.getHours()) + ':' + leadingZero(date.getMinutes()) + ':' + leadingZero(date.getSeconds());
        }

        function onMouseMove(e) {
            plot.unhighlight();
            resetLegends();

            if (plot.getSelection && plot.getSelection()) {
                rehoming.x = -1;
                return;
            }
            cacheGraphData();
            rehoming.x = Math.max(0, Math.min(e.pageX - plot.offset().left, plot.width()));
            var points = findNearestPoints(rehoming.x);
            var newLabel;
            if (points !== null) {
                for (var i = 0; i < points.length; i++) {
                    plot.highlight(points[i].seriesNumber, points[i].pointNumber);
                    newLabel = rehoming.series[points[i].seriesNumber].label + ': ' + points[i].pointValue.toFixed(2)
                        + ' - ' + formatDateTime(points[i].timestamp);
                    rehoming.legends.eq(points[i].seriesNumber).text(newLabel);
                }
            }
        }

        plot.hooks.bindEvents.push(function (plot, eventHolder) {
            if (!plot.getOptions().rehoming.active) {
                return;
            }

            eventHolder.mouseout(onMouseOut);
            eventHolder.mousemove(onMouseMove);
        });

        plot.hooks.drawOverlay.push(function (plot, ctx) {
            rehoming.legends = plot.getPlaceholder().find('.legendLabel');
        });

        plot.hooks.shutdown.push(function (plot, eventHolder) {
            eventHolder.unbind("mouseout", onMouseOut);
            eventHolder.unbind("mousemove", onMouseMove);
        });
    }

    $.plot.plugins.push({
        init: init,
        options: options,
        name: 'rehoming.x',
        version: '0.1'
    });
})(jQuery);
