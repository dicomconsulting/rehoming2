<?php
class FlotGraphWidget extends CWidget
{
    public $scriptUrl;

    public $themeUrl;

    public $theme = 'base';

    public $scriptFile = [
        'excanvas.min.js',
        'jquery.flot.min.js',
        'jquery.flot.time.min.js',
        'jquery.flot.navigate.min.js',
        'jquery.flot.symbol.min.js',
        'jquery.flot.crosshair.js',
        'jquery.flot.rehoming.js'
    ];

    public $cssFile = false;

    public $data = [];

    public $options = [];

    public $htmlOptions = [];

    public $tagName = 'div';

    public function init()
    {
        $this->resolvePackagePath();
        $this->registerCoreScripts();
        parent::init();
    }

    public function run()
    {
        $id = $this->getId();
        $this->htmlOptions['id']=$id;

        echo CHtml::openTag('div', ['class' => 'chartWrap']);
        echo CHtml::openTag($this->tagName, $this->htmlOptions);
        echo CHtml::closeTag($this->tagName);
        echo CHtml::tag('div', ['class' => 'axisLabel yAxisLabel'], '');
        echo CHtml::tag('div', ['class' => 'axisLabel xAxisLabel'], '');
        echo CHtml::closeTag('div');

        $graphData = CJavaScript::encode($this->data);
        $graphOptions = CJavaScript::encode($this->options);

        $placeholder = "$('#{$id}')";
        Yii::app()->getClientScript()->registerScript(
            __CLASS__ . '#' . $id,
            "$.plot({$placeholder}, {$graphData}, {$graphOptions});"
        );

        return $id;
    }

    protected function resolvePackagePath()
    {
        if ($this->scriptUrl === null || $this->themeUrl === null) {
            $basePath = Yii::getPathOfAlias('application.extensions.Flot.assets');
            $baseUrl = Yii::app()->getAssetManager()->publish($basePath);
            if ($this->scriptUrl === null) {
                $this->scriptUrl = $baseUrl . '';
            }
            if ($this->themeUrl === null) {
                $this->themeUrl = $baseUrl . '/css';
            }
        }
    }

    protected function registerCoreScripts()
    {
        if (is_string($this->cssFile)) {
            Yii::app()->getClientScript()->registerCssFile($this->themeUrl.'/'.$this->theme.'/'.$this->cssFile);
        } elseif (is_array($this->cssFile)) {
            foreach ($this->cssFile as $cssFile) {
                Yii::app()->getClientScript()->registerCssFile($this->themeUrl.'/'.$this->theme.'/'.$cssFile);
            }
        }

        Yii::app()->getClientScript()->registerCoreScript('jquery');
        if (is_string($this->scriptFile)) {
            $this->registerScriptFile($this->scriptFile);
        } elseif (is_array($this->scriptFile)) {
            foreach ($this->scriptFile as $scriptFile) {
                $this->registerScriptFile($scriptFile);
            }
        }
    }

    protected function registerScriptFile($fileName, $position = CClientScript::POS_HEAD)
    {
        Yii::app()->getClientScript()->registerScriptFile($this->scriptUrl . '/' . $fileName, $position);
    }
}
